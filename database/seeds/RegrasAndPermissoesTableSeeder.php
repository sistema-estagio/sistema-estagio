<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\PermissionRegistrar;

class RegrasAndPermissoesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('permissions')->truncate();
        Schema::enableForeignKeyConstraints();

        app(PermissionRegistrar::class)->forgetCachedPermissions();
        // Reset cached roles and permissions
        app()['cache']->forget('spatie.permission.cache');
        // create permissions
        //Instituicao
        Permission::create(['name' => 'Listar Instituicao']);
        Permission::create(['name' => 'Criar Instituicao']);
        Permission::create(['name' => 'Ver Instituicao']);
        Permission::create(['name' => 'Editar Instituicao']);
        Permission::create(['name' => 'Deletar Instituicao']);
        //Polos
        Permission::create(['name' => 'Listar Polo']);
        Permission::create(['name' => 'Criar Polo']);
        Permission::create(['name' => 'Ver Polo']);
        Permission::create(['name' => 'Editar Polo']);
        Permission::create(['name' => 'Deletar Polo']);
        //Curso
        Permission::create(['name' => 'Listar Curso']);
        Permission::create(['name' => 'Criar Curso']);
        Permission::create(['name' => 'Ver Curso']);
        Permission::create(['name' => 'Editar Curso']);
        Permission::create(['name' => 'Deletar Curso']);
        //Concedente
        Permission::create(['name' => 'Listar Concedente']);
        Permission::create(['name' => 'Criar Concedente']);
        Permission::create(['name' => 'Ver Concedente']);
        Permission::create(['name' => 'Editar Concedente']);
        Permission::create(['name' => 'Deletar Concedente']);
        //Estudantes
        Permission::create(['name' => 'Listar Estudante']);
        Permission::create(['name' => 'Criar Estudante']);
        Permission::create(['name' => 'Ver Estudante']);
        Permission::create(['name' => 'Editar Estudante']);
        Permission::create(['name' => 'Deletar Estudante']);
        //Tce
        Permission::create(['name' => 'Listar Tce']);
        Permission::create(['name' => 'Criar Tce']);
        Permission::create(['name' => 'Ver Tce']);
        Permission::create(['name' => 'Editar Tce']);
        Permission::create(['name' => 'Cancelar Tce']);
        //Aditivo
        Permission::create(['name' => 'Listar Aditivo']);
        Permission::create(['name' => 'Criar Aditivo']);
        Permission::create(['name' => 'Ver Aditivo']);
        Permission::create(['name' => 'Editar Aditivo']);
        Permission::create(['name' => 'Cancelar Aditivo']);
        //Usuario
        Permission::create(['name' => 'Listar Usuario']);
        Permission::create(['name' => 'Criar Usuario']);
        Permission::create(['name' => 'Ver Usuario']);
        Permission::create(['name' => 'Editar Usuario']);
        Permission::create(['name' => 'Deletar Usuario']);
        //Unidade
        Permission::create(['name' => 'Listar Unidade']);
        Permission::create(['name' => 'Criar Unidade']);
        Permission::create(['name' => 'Ver Unidade']);
        Permission::create(['name' => 'Editar Unidade']);
        Permission::create(['name' => 'Deletar Unidade']);
        //Relatorios
        Permission::create(['name' => 'Relatorios']);

        // create roles and assign existing permissions
        Role::create(['name' => 'admin']);

    }
}
