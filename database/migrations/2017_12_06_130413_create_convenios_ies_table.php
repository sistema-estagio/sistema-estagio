<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConveniosIesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('convenios_ies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('instituicao_id')->unsigned();
            $table->foreign('instituicao_id')
                ->references('id')
                ->on('instituicoes')
                ->onDelete('cascade');
            $table->date('dtConvenio')->nullable();
            $table->date('dtConvenioRetorno')->nullable();
            $table->integer('usuario_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('convenios_ies');
    }
}
