<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAtividadeAditivosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('atividade_aditivos', function (Blueprint $table) {
            //$table->increments('id');
            $table->integer('aditivo_id')->unsigned();
            $table->foreign('aditivo_id')
                ->references('id')
                ->on('aditivos')
                ->onDelete('cascade');
            $table->integer('atividade_id')->unsigned();
            $table->foreign('atividade_id')
                ->references('id')
                ->on('atividades')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('atividade_aditivos');
    }
}
