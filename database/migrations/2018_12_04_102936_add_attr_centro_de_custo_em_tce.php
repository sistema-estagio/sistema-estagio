<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAttrCentroDeCustoEmTce extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tces', function (Blueprint $table) {
            //
            $table->string('centroDeCusto')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tces', function (Blueprint $table) {
            //
            $table->dropColumn('centroDeCusto');
        });
    }
}
