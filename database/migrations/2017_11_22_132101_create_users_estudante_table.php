<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersEstudanteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_estudante', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('cdCPF')->unique();;
            $table->date('dtNascimento');
            $table->string('email')->unique();
            $table->string('password');
            $table->integer('usuario_id')->unsigned()->nullable();
            $table->integer('estudante_id')->unsigned()->nullable();
            $table->foreign('estudante_id')
            ->references('id')
            ->on('estudantes');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_estudante');
    }
}
