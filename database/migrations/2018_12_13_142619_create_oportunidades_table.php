<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOportunidadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oportunidades', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('tipo', array('obrigatorio', 'normal', 'jAprendiz'))->default('normal');
            $table->enum('turno', array('0', '1', '2', '3'))->default('0');
            $table->enum('status', array('disponivel', 'encerrada', 'oculta', 'encerradaOculta'))->default('oculta');
            $table->string('titulo');
            $table->string('code');
            $table->string('obs')->nullable();
            $table->string('local')->nullable();
            $table->string('horarioInicio')->nullable();
            $table->string('horarioFim')->nullable();
            $table->integer('cargaHorariaSemanal')->nullable();
            $table->integer('vagas')->default(0);
            $table->decimal('bolsa',10,2)->nullable();
            $table->decimal('auxTransp',10,2)->nullable();
            $table->decimal('auxAlimentacao',10,2)->nullable();
            $table->integer('user_id');
            $table->date('dtEncerrada')->nullable();
            $table->integer('concedente_id')->unsigned();
            $table->foreign('concedente_id')->references('id')->on('concedentes');
            $table->integer('secretaria_id')->unsigned()->nullable();
            $table->foreign('secretaria_id')->references('id')->on('sec_concedentes');
            $table->integer('cidade_id')->unsigned()->nullable();
            $table->foreign('cidade_id')->references('id')->on('cidades');
            $table->integer('estado_id')->unsigned()->nullable();
            $table->foreign('estado_id')->references('id')->on('estados');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oportunidades');
    }
}
