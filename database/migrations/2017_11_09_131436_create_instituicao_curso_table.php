<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstituicaoCursoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instituicoes_cursos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('instituicao_id')->unsigned();
            $table->foreign('instituicao_id')
                    ->references('id')
                    ->on('instituicoes')
                    ->onDelete('cascade');
            $table->integer('curso_id')->unsigned();
            $table->foreign('curso_id')
                    ->references('id')
                    ->on('cursos')
                    ->onDelete('cascade');
            $table->integer('curso_duracao_id')->unsigned();
            $table->foreign('curso_duracao_id')
                    ->references('id')
                    ->on('curso_duracoes')
                    ->onDelete('cascade');
            $table->integer('qtdDuracao',false);
            $table->integer('qtdEstagio', false);
            $table->integer('usuario_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instituicoes_cursos');
    }
}
