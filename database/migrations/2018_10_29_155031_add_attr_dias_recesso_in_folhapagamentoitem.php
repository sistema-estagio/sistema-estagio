<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAttrDiasRecessoInFolhapagamentoitem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('folha_pagamento_item', function (Blueprint $table) {
            $table->integer('diasRecesso')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('folha_pagamento_item', function (Blueprint $table) {
            $table->dropColumn('diasRecesso');
        });
    }
}
