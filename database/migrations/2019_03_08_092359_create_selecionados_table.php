<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSelecionadosTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('selecionados', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('nome');
            $table->date('dtNascimento')->nullable();
            $table->date('selecao_at');
            $table->string('cdCPF');
            $table->string('email')->nullable();
            $table->integer('inscricao');
            $table->integer('classificacao');
            $table->float('nota');
            $table->enum('selecionado', array(0, 1))->default(0);
            $table->string('telefone')->nullable();
            $table->integer('semestreAno');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('concedente_id')->unsigned();
            $table->foreign('concedente_id')->references('id')->on('concedentes');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('selecionados');
    }
}
