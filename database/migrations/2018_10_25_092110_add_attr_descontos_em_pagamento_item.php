<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAttrDescontosEmPagamentoItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('folha_pagamento_item', function (Blueprint $table) {
            $table->decimal('descontoVlAuxTransporte', 10, 2)->nullable();
            $table->decimal('descontoVlAuxMensal', 10, 2)->nullable();
            $table->decimal('descontoVlAuxAlimentacao', 10, 2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('folha_pagamento_item', function (Blueprint $table) {
            $table->dropColumn('descontoVlAuxTransporte');
            $table->dropColumn('descontoVlAuxMensal');
            $table->dropColumn('descontoVlAuxAlimentacao');
        });
    }
}
