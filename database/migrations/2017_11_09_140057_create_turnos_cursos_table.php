<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTurnosCursosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('turnos_cursos', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('curso_instituicao_id')->unsigned();
            $table->foreign('curso_instituicao_id')
                ->references('id')
                ->on('instituicoes_cursos')
                ->onDelete('cascade');
            $table->integer('turno_id')->unsigned();
            $table->foreign('turno_id')
                ->references('id')
                ->on('turnos')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('turnos_cursos');
    }
}
