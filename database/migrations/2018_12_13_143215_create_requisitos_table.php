<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequisitosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requisitos', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('tipo', array('educacional','outro'))->default('outro');
            $table->string('titulo');
            $table->string('descricao')->nullable();
            $table->integer('concedente_id')->unsigned();
            $table->foreign('concedente_id')->references('id')->on('concedentes');
            $table->integer('secretaria_id')->unsigned()->nullable();
            $table->foreign('secretaria_id')->references('id')->on('sec_concedentes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requisitos');
    }
}
