<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFolhasPagamentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('folhas_pagamentos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('concedente_id')->unsigned();
            $table->foreign('concedente_id')
                    ->references('id')
                    ->on('concedentes')
                    ->onDelete('cascade');
            $table->integer('secConcedente_id')->nullable();
            $table->enum('referenciaMes', ['01-JANEIRO','02-FEVEREIRO','03-MARÇO','04-ABRIL','05-MAIO','06-JUNHO','07-JULHO','08-AGOSTO','09-SETEMBRO','10-OUTUBRO','11-NOVEMBRO','12-DEZEMBRO']);
            $table->integer('referenciaAno');
            $table->string('hash');
            $table->integer('user_id')->unsigned()->nullable();
            $table->text('obs')->nullable();
            $table->enum('fechado', ['S','N'])->default('N');
            $table->timestamp('fechado_at')->nullable();
            $table->integer('fechado_user')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('folhas_pagamentos');
    }
}
