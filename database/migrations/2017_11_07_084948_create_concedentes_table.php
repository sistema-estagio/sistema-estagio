<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConcedentesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('concedentes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nmRazaoSocial');
            $table->enum('flTipo', ['PF', 'PJ']);
            $table->string('nmFantasia')->nullable();
            $table->string('cdCnpjCpf')->nullable();
            $table->string('dsOrgaoRegulador')->nullable();
            $table->string('nmResponsavel')->nullable();
            $table->string('dsResponsavelCargo')->nullable();
            $table->string('nmAdministrador')->nullable();
            $table->string('dsAdministradorCargo')->nullable();
            $table->string('dsEmail')->nullable();
            $table->string('dsEndereco')->nullable();
            $table->string('nmBairro')->nullable();
            $table->string('nnNumero')->nullable();
            $table->string('dsComplemento')->nullable();
            $table->string('cdCEP')->nullable();
            $table->integer('cidade_id')->unsigned();
            $table->foreign('cidade_id')
                ->references('id')
                ->on('cidades')
                ->onDelete('cascade');
            $table->integer('estado_id')->unsigned();
            $table->foreign('estado_id')
                ->references('id')
                ->on('estados')
                ->onDelete('cascade');
            $table->string('dsFone')->nullable();
            $table->string('dsOutroFone')->nullable();
            $table->date('dtInicioContrato')->nullable();
            $table->date('dtFimContrato')->nullable();
            $table->decimal('vlEstagio',10,2)->nullable();
            $table->decimal('vlAlimentacaoDia',10,2)->nullable();
            $table->decimal('vlTransporteDia',10,2)->nullable();
            $table->integer('usuario_id')->nullable();
            $table->enum('logo', ['S', 'N']);
            $table->string('assUpaTCE', 1)->nullable($value = true);
            $table->string('assUpaADITIVO', 1)->nullable($value = true);
            $table->string('opCancelamento', 3)->default('NAO');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('concedentes');
    }
}
