<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAjusteItemFolhasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ajuste_item_folhas', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('valor', 10, 2)->default(0);
            $table->string('obs')->nullable();
            $table->integer('user_id');
            $table->integer('item_id')->unsigned();
            $table->foreign('item_id')->references('id')->on('folha_pagamento_item');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ajuste_item_folhas');
    }
}
