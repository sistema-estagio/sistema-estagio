<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstudantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estudantes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nmEstudante');
            $table->string('cdCPF');
            $table->string('cdRG');
            $table->string('dsOrgaoRG');
            $table->enum('dsEstadoCivil', ['SOLTEIRO(A)', 'CASADO(A)', 'VIUVO(A)', 'DIVORCIADO(A)'])->nullable();
            $table->enum('dsSexo', ['MASCULINO', 'FEMININO'])->nullable();
            $table->date('dtNascimento');
            $table->integer('tce_id')->nullable();
            $table->integer('instituicao_id')->unsigned()->nullable();
            $table->foreign('instituicao_id')
                ->references('id')
                ->on('instituicoes')
                ->onDelete('cascade');
            $table->integer('polo_id')->unsigned()->nullable();
            $table->foreign('polo_id')
                ->references('id')
                ->on('polos')
                ->onDelete('cascade');
            $table->integer('curso_id')->unsigned()->nullable();
            $table->foreign('curso_id')
                ->references('id')
                ->on('instituicoes_cursos')
                ->onDelete('cascade');
            $table->integer('nnSemestreAno')->nullable();
            $table->integer('nivel_id')->unsigned()->nullable();
            $table->foreign('nivel_id')
                ->references('id')
                ->on('niveis')
                ->onDelete('cascade');
            $table->integer('turno_id')->unsigned()->nullable();
            $table->foreign('turno_id')
                ->references('id')
                ->on('turnos')
                ->onDelete('cascade');
            $table->integer('estado_id')->unsigned();
            $table->foreign('estado_id')
                ->references('id')
                ->on('estados')
                ->onDelete('cascade');
            $table->integer('cidade_id')->unsigned();
            $table->foreign('cidade_id')
                ->references('id')
                ->on('cidades')
                ->onDelete('cascade');
            $table->string('dsEndereco')->nullable();
            $table->string('nnNumero')->nullable();
            $table->string('nmBairro')->nullable();
            $table->string('dsComplemento')->nullable();
            $table->string('cdCEP')->nullable();
            $table->string('dsFone')->nullable();
            $table->string('dsOutroFone')->nullable();
            $table->string('dsEmail')->nullable();
            $table->string('nmPai')->nullable();
            $table->string('nmMae')->nullable();
            $table->text('dsBanco')->nullable();
            $table->string('dsAgencia', 30)->nullable();
            $table->string('dsTipoConta', 15)->nullable();
            $table->string('dsConta', 30)->nullable();
            $table->text('dsFavorecido')->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estudantes');
    }
}
