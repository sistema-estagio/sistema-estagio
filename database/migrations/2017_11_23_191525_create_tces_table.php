<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tces', function (Blueprint $table) {
            $table->increments('id');
            $table->string('migracao')->nullable();
            $table->integer('estudante_id')->unsigned();
            $table->foreign('estudante_id')
                ->references('id')
                ->on('estudantes');
            $table->integer('instituicao_id')->unsigned();
            $table->foreign('instituicao_id')
                ->references('id')
                ->on('instituicoes');
            $table->integer('concedente_id')->unsigned();
            $table->foreign('concedente_id')
                ->references('id')
                ->on('concedentes');
            $table->integer('sec_conc_id')->unsigned()->nullable();
            $table->foreign('sec_conc_id')
                ->references('id')
                ->on('sec_concedentes');
            $table->integer('curso_id')->unsigned();
            $table->foreign('curso_id')
            ->references('id')
            ->on('instituicoes_cursos');
            $table->integer('nnSemestreAno');
            $table->string('nmSupervisor')->nullable();
            $table->string('dsSupervisorCargo')->nullable();
            $table->string('cdCPFSupervisor')->nullable();
            $table->string('nmSupervisorIe')->nullable();
            $table->string('dsSupervisorIeCargo')->nullable();
            $table->date('dtInicio');
            $table->date('dtFim');
            $table->integer('periodo_id')->unsigned();
            $table->foreign('periodo_id')
                ->references('id')
                ->on('periodos') ;
            $table->string('dsPeriodo')->nullable();
            $table->time('hrInicio')->nullable();
            $table->time('hrFim')->nullable();
            $table->decimal('vlAuxilioMensal', 10, 2)->nullable();
            $table->decimal('vlAuxilioAlimentacao', 10, 2)->nullable();
            $table->decimal('vlAuxilioTransporte', 10, 2)->nullable();
            $table->text('auxDescriminado')->nullable();
            $table->string('dsApolice')->nullable();
            $table->string('dsLotacao')->nullable();
            $table->date('dtDeclaracao')->nullable();
            $table->date('dt4Via')->nullable();
            $table->dateTime('dt4via_at')->nullable();
            $table->integer('userDt4Via_id')->nullable();
            $table->integer('relatorio_id')->unsigned();
            $table->foreign('relatorio_id')
                ->references('id')
                ->on('relatorios')
                ->onDelete('cascade');
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('motivoCancelamento_id')->unsigned()->nullable();
            $table->foreign('motivoCancelamento_id')
                ->references('id')
                ->on('motivo_cancelamentos')
                ->onDelete('cascade');
            $table->text('dsMotivoCancelamento')->nullable();
            $table->integer('usuarioCancelamento_id')->nullable();
            $table->integer('usuarioConcedenteCancelamento_id')->nullable();
            $table->date('dtCancelamento')->nullable();
            $table->date('cancelado_at')->nullable();
            $table->integer('estado_id')->unsigned();
            $table->foreign('estado_id')
                ->references('id')
                ->on('estados')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tces');
    }
}
