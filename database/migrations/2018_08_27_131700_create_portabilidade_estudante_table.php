<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePortabilidadeEstudanteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portabilidade_estudantes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('estudante_id')->unsigned();
            $table->foreign('estudante_id')
                    ->references('id')
                    ->on('estudantes')
                    ->onDelete('cascade');
            $table->date('dtInicio');
            $table->date('dtFim');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('folha_pagamento_item');
    }
}
