<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSecConcedentesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sec_concedentes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nmSecretaria');
            $table->string('cdCnpjCpf')->nullable();
            $table->string('nmResponsavel')->nullable();
            $table->string('dsResponsavelCargo')->nullable();
            $table->string('nmAdministrador')->nullable();
            $table->string('dsAdministradorCargo')->nullable();
            $table->string('dsEmail')->nullable();
            $table->string('dsEndereco')->nullable();
            $table->string('nmBairro')->nullable();
            $table->string('nnNumero')->nullable();
            $table->string('dsComplemento')->nullable();
            $table->string('cdCEP')->nullable();
            $table->string('dsFone')->nullable();
            $table->string('dsOutroFone')->nullable();
            $table->integer('concedente_id')->unsigned();
            $table->foreign('concedente_id')
            ->references('id')
            ->on('concedentes')
            ->onDelete('cascade');
            $table->integer('usuario_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sec_concedentes');
    }
}
