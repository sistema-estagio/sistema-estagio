<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFolhaPagamentoItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('folha_pagamento_item', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('folha_id')->unsigned();
            $table->foreign('folha_id')
                    ->references('id')
                    ->on('folhas_pagamentos')
                    ->onDelete('cascade');
            $table->integer('tce_id')->unsigned();
            $table->foreign('tce_id')
                    ->references('id')
                    ->on('tces')
                    ->onDelete('cascade');
            $table->integer('concedente_id')->unsigned();
            $table->foreign('concedente_id')
                    ->references('id')
                    ->on('concedentes')
                    ->onDelete('cascade');
            $table->integer('secConcedente_id')->nullable();
            $table->date('dtPagamento')->nullable();
            $table->enum('referenciaMes', ['01-JANEIRO','02-FEVEREIRO','03-MARÇO','04-ABRIL','05-MAIO','06-JUNHO','07-JULHO','08-AGOSTO','09-SETEMBRO','10-OUTUBRO','11-NOVEMBRO','12-DEZEMBRO']);
            $table->string('hash');
            $table->integer('referenciaAno');
            $table->integer('mesesPropRecesso')->nullable();
            $table->decimal('vlRecesso', 10, 2)->nullable();
            $table->integer('diasBaseEstagiados');
            $table->decimal('vlAuxilioMensal', 10, 2)->nullable();
            $table->decimal('vlAuxilioMensalReceber', 10, 2)->nullable();
            $table->decimal('vlAuxilioMensalRecesso', 10, 2)->nullable();
            $table->integer('diasTrabalhados');
            $table->decimal('vlAuxilioAlimentacaoDia', 10, 2)->nullable();
            $table->decimal('vlAuxilioAlimentacaoReceber', 10, 2)->nullable();
            $table->decimal('vlAuxilioTransporteDia', 10, 2)->nullable();
            $table->decimal('vlAuxilioTransporteReceber', 10, 2)->nullable();
            $table->decimal('vlTotal', 10, 2)->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('tipo_tce')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('folha_pagamento_item');
    }
}
