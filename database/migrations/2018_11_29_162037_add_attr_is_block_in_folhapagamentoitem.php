<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAttrIsBlockInFolhapagamentoitem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('folha_pagamento_item', function (Blueprint $table) {
            //
            $table->integer('is_block')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('folha_pagamento_item', function (Blueprint $table) {
            //
            $table->dropColumn('is_block');
        });
    }
}
