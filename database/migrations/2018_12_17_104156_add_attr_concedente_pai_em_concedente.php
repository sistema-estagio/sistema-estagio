<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAttrConcedentePaiEmConcedente extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('concedentes', function (Blueprint $table) {
            //
            $table->integer('concedente_pai')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('concedentes', function (Blueprint $table) {
            //
            $table->dropColumn('concedente_pai');
        });
    }
}
