<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAditivosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aditivos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tce_id')->unsigned();
            $table->foreign('tce_id')->references('id')                ->on('tces')                ->onDelete('cascade');
            $table->integer('nnAditivo');
            $table->integer('nnSemestreAno')->nullable();
            $table->string('nmSupervisor')->nullable();
            $table->string('dsSupervisorCargo')->nullable();
            $table->string('cdCPFSupervisor')->nullable();
            $table->string('nmSupervisorIe')->nullable();
            $table->string('dsSupervisorIeCargo')->nullable();
            $table->date('dtInicio')->nullable();
            $table->date('dtFim')->nullable();
            $table->integer('periodo_id')->unsigned()->nullable();
            $table->foreign('periodo_id')                ->references('id')               ->on('periodos')                ->onDelete('cascade');
            $table->string('dsPeriodo')->nullable();
            $table->time('hrInicio')->nullable();
            $table->time('hrFim')->nullable();
            $table->decimal('vlAuxilioMensal', 10, 2)->nullable();
            $table->decimal('vlAuxilioAlimentacao', 10, 2)->nullable();
            $table->decimal('vlAuxilioTransporte', 10, 2)->nullable();
            $table->string('auxDescriminado')->nullable();
            $table->string('aditivoOutro')->nullable();
            $table->string('dsLotacao')->nullable();
            $table->date('dtDeclaracao')->nullable();
            $table->date('dt4Via')->nullable();
            $table->date('dtAditivo');
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('estado_id')->unsigned();
            $table->foreign('estado_id')                ->references('id')                ->on('estados')                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aditivos');
    }
}
