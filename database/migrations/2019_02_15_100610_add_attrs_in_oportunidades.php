<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAttrsInOportunidades extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('oportunidades', function (Blueprint $table) {
            $table->enum('showBolsa', array(0, 1))->default(0);
            $table->enum('showTransp', array(0, 1))->default(0);
            $table->enum('showAlimentacao', array(0, 1))->default(0);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('oportunidades', function (Blueprint $table) {
            $table->dropColumn('showBolsa');
            $table->dropColumn('showTransp');
            $table->dropColumn('showAlimentacao');
        });
    }
}
