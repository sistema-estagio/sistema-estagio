<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTipoEmUsersConcedente extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_concedente', function (Blueprint $table) {
            $table->enum('tipo', array('a','m'))->default('a');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_concedente', function (Blueprint $table) {
            $table->dropColumn('tipo');
        });
    }
}
