<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePolosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('polos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nmPolo');
            $table->string('cdCNPJ');
            $table->string('nmDiretor')->nullable();
            $table->string('nmDiretorCargo')->nullable();
            $table->string('nmSupervisor')->nullable();
            $table->string('nmSupervisorCargo')->nullable();
            $table->string('nmReitor')->nullable();
            $table->string('nmReitorCargo')->nullable();
            $table->string('dsEmail')->nullable();
            $table->string('dsEndereco')->nullable();
            $table->string('nnNumero')->nullable();
            $table->string('nmBairro')->nullable();
            $table->string('cdCEP')->nullable();
            $table->string('dsFone')->nullable();
            $table->string('dsOutro')->nullable();
            $table->integer('estado_id')->unsigned();
            $table->foreign('estado_id')
                ->references('id')
                ->on('estados')
                ->onDelete('cascade');
            $table->integer('cidade_id')->unsigned();
            $table->foreign('cidade_id')
                ->references('id')
                ->on('cidades')
                ->onDelete('cascade');
            $table->integer('usuario_id')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('polos');
    }
}
