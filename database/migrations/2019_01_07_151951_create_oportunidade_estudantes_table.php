<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOportunidadeEstudantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oportunidade_estudantes', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('selecionado', array('s','n'))->default('n');
            $table->integer('estudante_id')->unsigned();
            $table->foreign('estudante_id')->references('id')->on('estudantes');
            $table->integer('oportunidade_id')->unsigned()->nullable();
            $table->foreign('oportunidade_id')->references('id')->on('oportunidades');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oportunidade_estudantes');
    }
}
