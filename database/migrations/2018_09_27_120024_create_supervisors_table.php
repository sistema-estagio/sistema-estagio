<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupervisorsTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::create('supervisors', function (Blueprint $table) {
      $table->increments('id');
      $table->string('nome');
      $table->string('cpf');
      $table->string('email')->nullable();
      $table->string('cargo');
      $table->string('telefone')->nullable();
      $table->integer('concedente_id')->unsigned();
      $table->foreign('concedente_id')
      ->references('id')
      ->on('concedentes')
      ->onDelete('cascade');
      $table->integer('secConcedente_id')->unsigned()->nullable();
      $table->foreign('secConcedente_id')
      ->references('id')
      ->on('sec_concedentes')
      ->onDelete('cascade');
      $table->timestamps();
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::dropIfExists('supervisors');
  }
}
