<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOportunidadeRequisitosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oportunidade_requisitos', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('isObrigatorio', [true, false])->default(true);
            $table->integer('semestre')->nullable();
            $table->integer('oportunidade_id')->unsigned();
            $table->foreign('oportunidade_id')->references('id')->on('oportunidades');
            $table->integer('requisito_id')->unsigned();
            $table->foreign('requisito_id')->references('id')->on('requisitos');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oportunidade_requisitos');
    }
}
