<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersConcedenteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_concedente', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('email')->unique();
            $table->string('password');
            $table->integer('usuario_id')->unsigned()->nullable();
            $table->integer('concedente_id')->unsigned()->nullable();
            $table->foreign('concedente_id')
            ->references('id')
            ->on('concedentes');
            $table->integer('sec_concedente_id')->unsigned()->nullable();
            $table->foreign('sec_concedente_id')
            ->references('id')
            ->on('sec_concedentes');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_concedente');
    }
}
