<?php

namespace App\Http\Controllers\Estudante;

use App\Models\UserEstudante;
use App\Models\FolhaPagamentoItem;
use App\Models\Tce;
use App\Models\Oportunidade;
use App\Models\Aditivo;
use App\Models\RelatorioSemestralAtividade;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PDF;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:web_estudante');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $estudante = UserEstudante::find(Auth::id())->estudante()->first();
      //$estudante = UserEstudante::find(Auth::id());
      $tces = Tce::where('estudante_id','=',$estudante->id)->orderBy('id', 'desc')->get();
      $tce = Tce::find($estudante->tce_id);
      //dd($tce);
      //$tcesCancelados = $estudante->estudante->tce()->where('estudante_id','=',$estudante->estudante->id);
      //$tcesCancelados = Tce::where('estudante_id','=',$estudante->estudante->id);
      /*
      if(!$tce){
        $tce = $estudante->estudante->tce()->where('dtCancelamento','<>',null);
      } else {
      $folhas = FolhaPagamentoItem::where('tce_id',$tce->id)->get();
      $aditivos = Aditivo::where('tce_id',$tce->id)->get();
      }*/
      $oportunidades = Oportunidade::all();
      return view('painel.estudante.home.index',compact('estudante','tces','tce','folhas','aditivos', 'oportunidades'));
    }

    public function downloadRelatorioSemestralPDF(){
      $estudante = UserEstudante::find(Auth::id());
      $tce = $estudante->estudante->tce()->whereNull('dtCancelamento')->first();
      $getAditivo = $tce->aditivos->where('dtInicio','<>',null)->last();
      $check = RelatorioSemestralAtividade::create([
        'tce_id' => $tce->id
      ]);
      $pdf = PDF::loadView('painel.estudante.relatorio.relatorioAtividadesSemestral', compact('dtHoje','tce','getAditivo','check'))
              ->setPaper('a4')
              ->setOptions(['dpi' => 100]);
      return $pdf->stream('Relatório de Atividades Semestral '.$tce->id.' '.$tce->estudante->nmEstudante.'.pdf');
    }
}
