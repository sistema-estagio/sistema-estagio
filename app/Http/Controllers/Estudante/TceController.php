<?php

namespace App\Http\Controllers\Estudante;

use App\Models\UserEstudante;
use App\Models\FolhaPagamentoItem;
use App\Models\Tce;
use App\Models\Aditivo;
use App\Models\RelatorioSemestralAtividade;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PDF;
use Auth;

class TceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web_estudante');
    }

    public function show($idTce)
    {
        $id = base64_decode($idTce);
        $estudante = UserEstudante::find(Auth::id())->estudante()->first();
        //$estudante = UserEstudante::find(Auth::id());
        $tce = Tce::find($id);
        $folhas = FolhaPagamentoItem::where('tce_id',$id)->get();
        $aditivos = Aditivo::where('tce_id',$id)->get();
        //dd($estudante->estudante_id);
        if($tce){
            $tce;
        }else if($estudante->estudante_id == $tce->estudante_id){
            $tce;
        }else{
            return redirect()->back();
        }
        return view('painel.estudante.tce.index', compact('estudante','tce','folhas','aditivos'));
    }

    public function downloadRelatorioSemestralPDF(){
        $estudante = UserEstudante::find(Auth::id())->estudante()->first();
      //$estudante = UserEstudante::find(Auth::id());
      $tce = $estudante->estudante->tce()->whereNull('dtCancelamento')->first();
      $getAditivo = $tce->aditivos->where('dtInicio','<>',null)->last();
      $check = RelatorioSemestralAtividade::create([
        'tce_id' => $tce->id
      ]);
      $pdf = PDF::loadView('painel.estudante.relatorio.relatorioAtividadesSemestral', compact('dtHoje','tce','getAditivo','check'))
              ->setPaper('a4')
              ->setOptions(['dpi' => 100]);
      return $pdf->stream('Relatório de Atividades Semestral '.$tce->id.' '.$tce->estudante->nmEstudante.'.pdf');
    }
}
