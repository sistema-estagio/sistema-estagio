<?php

namespace App\Http\Controllers\Estudante;

use App\Models\Estudante;
use App\Models\EstudantePainel;
use App\Models\UserEstudante;
use App\Models\Estado;
use App\Models\Cidade;
use App\Models\Instituicao;
use App\Models\CursoInstituicao;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Auth;
use Validator;

class PerfilController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:web_estudante');
    }

    public function index()
    {
        //
        //$estudante = Estudante::find(Auth::user()->estudante_id);
        //dd($estudante);
        $estudante = UserEstudante::find(Auth::id())->estudante()->first();
        //dd($estudante);
        return view('painel.estudante.perfil.index', compact('estudante'));
    }

    public function editar()
    {
        $estados = Estado::all();
        $estudante = UserEstudante::find(Auth::id())->estudante()->first();
        $cidades = Cidade::where('estado_id', $estudante->estado_id)->orderBy('nmCidade')->get();
        $instituicoes = Instituicao::where('estado_id', $estudante->estado_id)->orderBy('nmInstituicao')->get();
        $cursos = CursoInstituicao::where('instituicao_id', $estudante->instituicao_id)->get();
        $niveis = DB::table('instituicoes_cursos')
        ->join('cursos', 'instituicoes_cursos.curso_id', '=', 'cursos.id')
        ->join('niveis', 'cursos.nivel_id', '=', 'niveis.id')
        ->select('niveis.id', 'niveis.nmNivel')
        ->where('instituicoes_cursos.id',$estudante->curso_id)
        ->get();
        $turnos = DB::table('turnos_cursos')
        ->join('turnos', 'turnos_cursos.turno_id', '=', 'turnos.id')
        ->select('turnos.id', 'turnos.nmTurno')
        ->where('turnos_cursos.curso_instituicao_id',$estudante->curso_id)
        ->orderBy('turnos.id')
        ->get();
        //dd($cursos);
        return view('painel.estudante.perfil.editar', compact('estudante','estados','cidades','instituicoes','cursos','niveis','turnos'));
    }

    public function update(Request $request)
    {
        $formulario = $request->all();
        $validar= Validator::make($request->all(), [
            'nmEstudante' => 'required',
            'cdRG' => 'required',
            'dsEstadoCivil' => 'required',
            'dsSexo' => 'required',
            'dtNascimento' => 'required',
            'instituicao_id' => 'required',
            'curso_id' => 'required',
            'nivel_id' => 'required',
            'turno_id' => 'required',
            'nnSemestreAno' => 'required',
            'cdCEP' => 'required',
            'dsEndereco' => 'required',
            'nnNumero' => 'required',
            'nmBairro' => 'required',
            'estado_id' => 'required',
            'cidade_id' => 'required',
            'dsEmail' => 'required',
        ],[
            'nmEstudante.required' => ' O campo Nome é Obrigatório.',
            'cdRG.required' => ' O campo RG é obrigatório.',
            'dsEmail.required' => ' O campo E-MAIL é obrigatorio.',
            'dsEmail.unique' => ' Já existe estudante com esse E-MAIL.',
            'dsEstadoCivil.required' => ' O campo Estado Civil é obrigatório.',
            'dsSexo.required' => ' O campo Gênero é obrigatório.',
            'dtNascimento.required' => ' O campo Data de Nascimento é obrigatória.',
            'instituicao_id.required' => ' O campo Instituição de Ensino é obrigatória.',
            'curso_id.required' => ' O campo Curso é obrigatório.',
            'nivel_id.required' => ' O campo Nivel é obrigatório.',
            'turno_id.required' => ' O campo Turno é obrigatório.',
            'nnSemestreAno.required' => ' O campo Semestre/Ano é obrigatório.',
            'cdCEP.required' => ' O campo CEP é obrigatório.',
            'dsEndereco.required' => ' O campo Endereço é obrigatório.',
            'nnNumero.required' => ' O campo Numero é obrigatório.',
            'nmBairro.required' => ' O campo Bairro é obrigatório.',
            'estado_id.required' => ' O campo Estado é obrigatório.',
            'cidade_id.required' => ' O campo Cidade é obrigatório.',
        ]);
        
        if($validar->fails()) {
            return
            redirect()
            ->back()
            ->withErrors($validar)
            ->withInput();
        }
        $estudante = UserEstudante::find(Auth::id())->estudante()->first();
        $estudanteAtualiza = EstudantePainel::find($estudante->id);
        //dd($estudante, $estudanteAtualiza,$estudanteAcesso);
        //dd(UserEstudante::find(Auth::id())->estudante()->first());
        
        if($estudante AND $estudanteAtualiza){

          $estudanteAtualiza->update($formulario);
          $atualizar = $estudante->update(['name' => $formulario['nmEstudante'], 'phone' => $formulario['dsFone'], 'email' =>  $formulario['dsEmail']]);          
          //$formulario['dsEmail'] = $request['email'];
          //$estudante->name = $formulario['nmEstudante'];
          //$estudante->phone = $formulario['dsFone'];
          //$estudante->email = $formulario['dsEmail'];
          //$estudante->save();

          return redirect('painel/estudante/perfil')->with(['status'=>'success','msg'=>'Dados atualizados com sucesso']);
        }
        return redirect('painel/estudante/perfil')->with(['status'=>'error','msg'=>'Não foi possível atualizar os dados ']);
    }

    public function updateAcesso(Request $request)
    {
        //dd($request->all());
        $estudante = UserEstudante::find(Auth::id())->estudante()->first();
        //$estudante = Estudante::find(Auth::estudante_id());
        if($estudante){
          if($request['new_password'] != null){
            $validator = Validator::make($request->all(), [
              'old_password' => 'required',
              'new_password' => 'required|string|min:8',
            ]);

            if(!$validator->fails()){
              if(\Hash::check($request['old_password'], $estudante->password)){
                $estudante->password = bcrypt($request['new_password']);
              }
            }
          }

          $estudante->save();
          return redirect('painel/estudante/perfil')->with(['status'=>'success','msg'=>'Sua Senha Foi Atualizada com Sucesso!']);
        }
        return redirect('painel/estudante/perfil')->with(['status'=>'error','msg'=>'Não foi possível atualizar os dados ']);
    }

}