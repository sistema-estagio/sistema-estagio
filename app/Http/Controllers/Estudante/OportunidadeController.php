<?php

namespace App\Http\Controllers\Estudante;

use App\Models\Oportunidade;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OportunidadeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Oportunidade  $oportunidade
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $code)
    {
        $oportunidade = Oportunidade::where('code', $code)->first();
        if($oportunidade){
          if($oportunidade->status != 'oculta'){
            return view('painel.estudante.oportunidade.visualizar')->with('oportunidade');
          }
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Oportunidade  $oportunidade
     * @return \Illuminate\Http\Response
     */
    public function edit(Oportunidade $oportunidade)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Oportunidade  $oportunidade
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Oportunidade $oportunidade)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Oportunidade  $oportunidade
     * @return \Illuminate\Http\Response
     */
    public function destroy(Oportunidade $oportunidade)
    {
        //
    }
}
