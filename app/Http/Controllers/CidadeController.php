<?php 

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Models\Estado;
use App\Http\Requests;

class CidadeController extends Controller
{
    private $estadoModel;
    public function __construct(Estado $estado)
    {
        $this->estadoModel = $estado;
    }
    public function index()
    {
        $estados = $this->estadoModel->lists('nmEstado', 'id');
        return view('concedente.cadastro', compact('estados'));
        
    }
    public function getCidades($idEstado)
    {
        $estado = $this->estadoModel->find($idEstado);
        $cidades = $estado->cidades()->getQuery()->orderBy('nmCidade','ASC')->get(['id', 'nmCidade']);
        return Response::json($cidades);
    }
}