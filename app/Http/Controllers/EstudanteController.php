<?php

namespace App\Http\Controllers;

//use Illuminate\Support\Facades\Response;

use Illuminate\Http\Request;
use App\Models\Estudante;
use App\Models\Estudante2;
use App\Models\Tce;
use App\Models\Estado;
use App\Models\Cidade;
use App\Models\Instituicao;
use App\Models\CursoInstituicao;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use Auth;

use Validator;

class EstudanteController extends Controller
{

    private $estudante;
    private $tce;
    private $cursoInstituicao;

    public function __construct(Estudante $estudante, Tce $tce, CursoInstituicao $cursoInstituicao)
    {
        $this->estudante = $estudante;
        $this->tce = $tce;
        $this->cursoInstituicao = $cursoInstituicao;
    }

    public function index(Request $request)
    {
        //dd($request->tce);
        $titulo = "Listagem de Estudantes";

        //Verificação de Estado do Usuario
        if(Auth::user()->unidade_id != null){
            $relEstado = Auth::user()->unidade->estado->pluck('id')->toArray();

            /*$estudantes = $this->estudante
                            //->WhereIn('estado_id',$relEstado)
                            ->WhereIn('estado_id',$relEstado)->get();
                            //->paginate(config('sysGlobal.Paginacao'));
            */
            if($request->tce == "sem"){
                    $estudantes = $this->estudante->
                    WhereIn('estado_id',$relEstado)->
                    whereNull('tce_id')->
                    get();
            } elseif($request->tce == "com"){
                    $estudantes = $this->estudante->
                    WhereIn('estado_id',$relEstado)->
                    whereNotNull('tce_id')->
                    get();
            } else {
                    $estudantes = $this->estudante->WhereIn('estado_id',$relEstado)->get();
            }

        } else {

                if($request->tce == "sem"){
                    $estudantes = $this->estudante->
                    whereNull('tce_id')->
                    get();
                } elseif($request->tce == "com"){
                    $estudantes = $this->estudante->
                    whereNotNull('tce_id')->
                    get();
                } else {
                    $estudantes = $this->estudante->get();
                }

           // $estudantes = $this->estudante->chunk(300);
            //$estudantes = $this->estudante->select('id','nmEstudante','cdCPF','cdRG','cidade_id','created_at')->get();
            //->orderBy('nmEstudante')
            //->paginate(config('sysConfig.Paginacao'));
            //dd($estudantes);
        }
        return view('estudante.index', compact('estudantes','titulo'));
    }

    public function indexJson(Request $request)
    {
        $student = new Collection;
        $est = [];
        //Verificação de Estado do Usuario
        if(Auth::user()->unidade_id != null){
            $relEstado = Auth::user()->unidade->estado->pluck('id')->toArray();
            if($request->tce == "sem"){
                    $estudantes = $this->estudante->
                    WhereIn('estado_id',$relEstado)->
                    whereNull('tce_id')->
                    get();
            } elseif($request->tce == "com"){
                    $estudantes = $this->estudante->
                    WhereIn('estado_id',$relEstado)->
                    whereNotNull('tce_id')->
                    get();
            } else {
                    $estudantes = $this->estudante->WhereIn('estado_id',$relEstado)->get();
            }
        } else {
                if($request->tce == "sem"){
                    $estudantes = $this->estudante->
                    whereNull('tce_id')->
                    get();
                } elseif($request->tce == "com"){
                    $estudantes = $this->estudante->
                    whereNotNull('tce_id')->
                    get();
                } else {
                    $estudantes = $this->estudante->get();
                }

        }
        foreach($estudantes as $estudante){
            $est['id'] = $estudante->id;
            $est['nmEstudante'] = $estudante->nmEstudante;
            $est['cdCPF'] = $estudante->cdCPF;
            $est['cdRG'] = $estudante->cdRG;
            $est['cidadeUF'] = $estudante->cidade->nmCidade.'/'.$estudante->cidade->estado->cdUF;
            $est['created_at'] = $estudante->created_at->format('d/m/Y');
            $student->push($est);
        }

        return response()->json(
            $student
          , JSON_UNESCAPED_UNICODE);
    }

        public function create(Request $request)
        {
            $estados = Estado::all();
            //Pega instituições e ordena por nome de instituições
            //Verificação de Estado do Usuario
            if(Auth::user()->unidade_id != null){
                $relEstado = Auth::user()->unidade->estado->pluck('id')->toArray();
                $instituicoes = Instituicao::WhereIn('estado_id',$relEstado)
                    ->orWhere('isEad', 'S')
                    ->orderBy('nmInstituicao')->get();
                    //->paginate(config('sysGlobal.Paginacao'));
            }else{
                //$instituicoes = $this->instituicao->paginate(config('sysConfig.Paginacao'));
                $instituicoes = Instituicao::all()->sortBy('nmInstituicao');
            }
            //$instituicoes = Instituicao::all()->sortBy('nmInstituicao');
            //dd($instituicoes);
            $titulo = "Cadastro de Estudantes";
            //$cursos = $this->curso->all();
            //$instituicoes = $this->instituicao->paginate($this->totalPage);
            $cidades = [];
            $cursos = [];
            $niveis = [];
            $turnos = [];
            return View('estudante.cadastro', [
                'estados' => $estados,
                'instituicoes' => $instituicoes,
                'titulo' => $titulo,
                'cidades' => $request->session()->get('cidades') ?? [],
                'cursos' => $request->session()->get('cursos') ?? [],
                'niveis' => $request->session()->get('niveis') ?? [],
                'turnos' => $request->session()->get('turnos') ?? []
            ]);
            //return view('estudante.cadastro', compact('titulo','estados','instituicoes'));
        }

    public function store(Request $request)
    {
        $formulario = $request->all();
        $formulario['user_id'] = Auth::user()->id;
        //$dt = $formulario['dtNascimento'];
        //Pega idade digitada e soma +16 anos

        $validar= Validator::make($request->all(), [
            'nmEstudante' => 'required',
            'cdRG' => 'required',
            //'cdCPF' => 'required|unique:estudantes|cpf',
            'cdCPF' => 'required|unique:estudantes',
            'dsEstadoCivil' => 'required',
            'dsSexo' => 'required',
            'dtNascimento' => 'required',
            'instituicao_id' => 'required',
            'curso_id' => 'required',
            'nivel_id' => 'required',
            'turno_id' => 'required',
            'nnSemestreAno' => 'required',
            'cdCEP' => 'required',
            'dsEndereco' => 'required',
            'nnNumero' => 'required',
            'nmBairro' => 'required',
            'estado_id' => 'required',
            'cidade_id' => 'required',
        ],[
            'nmEstudante.required' => ' O campo Nome é Obrigatório.',
            'cdRG.required' => ' O campo RG é obrigatório.',
            'cdCPF.required' => ' O campo CPF é obrigatório.',
            'cdCPF.unique' => ' Já existe estudante com esse numero de CPF.',
            'cdCPF.cpf' => ' CPF Inválido',
            'dsEstadoCivil.required' => ' O campo Estado Civil é obrigatório.',
            'dsSexo.required' => ' O campo Gênero é obrigatório.',
            'dtNascimento.required' => ' O campo Data de Nascimento é obrigatória.',
            'instituicao_id.required' => ' O campo Instituição de Ensino é obrigatória.',
            'curso_id.required' => ' O campo Curso é obrigatório.',
            'nivel_id.required' => ' O campo Nivel é obrigatório.',
            'turno_id.required' => ' O campo Turno é obrigatório.',
            'nnSemestreAno.required' => ' O campo Semestre/Ano é obrigatório.',
            'cdCEP.required' => ' O campo CEP é obrigatório.',
            'dsEndereco.required' => ' O campo Endereço é obrigatório.',
            'nnNumero.required' => ' O campo Numero é obrigatório.',
            'nmBairro.required' => ' O campo Bairro é obrigatório.',
            'estado_id.required' => ' O campo Estado é obrigatório.',
            'cidade_id.required' => ' O campo Cidade é obrigatório.',
        ]);

        if($validar->fails()) {
            $cidades = Cidade::where('estado_id', $formulario['estado_id'])->get();
            $cursos = Instituicao::findOrFail($formulario['instituicao_id']);
            $niveis = DB::table('instituicoes_cursos')
            ->join('cursos', 'instituicoes_cursos.curso_id', '=', 'cursos.id')
            ->join('niveis', 'cursos.nivel_id', '=', 'niveis.id')
            ->select('niveis.id', 'niveis.nmNivel')
            ->where('instituicoes_cursos.id',$formulario['curso_id'])
            ->get();
            $turnos = DB::table('turnos_cursos')
            ->join('turnos', 'turnos_cursos.turno_id', '=', 'turnos.id')
            ->select('turnos.id', 'turnos.nmTurno')
            ->where('turnos_cursos.curso_instituicao_id',$formulario['curso_id'])
            ->orderBy('turnos.id')
            ->get();
            //$this->instituicao->findOrFail($id);
            //$cursos = CursoInstituicao::where('instituicao_id', $formulario['instituicao_id'])->get();
            //dd($formulario);
            return
            //dd($validar, $validar->any());
            redirect()
            ->back()
            //->route('estudante.create')
            ->with('cidades', $cidades)
            ->with('cursos', $cursos)
            ->with('niveis', $niveis)
            ->with('turnos', $turnos)
            ->withErrors($validar)
            ->withInput();
        }

        $CursoInstituicao = $this->cursoInstituicao->findOrFail($formulario['curso_id']);
        $nascimento = Carbon::createFromFormat('d/m/Y', $formulario['dtNascimento'])->addYears(16)->toDateString();
        $hoje = Carbon::now()->toDateString();
        //dd($CursoInstituicao);
        //Compara a data passada + 16 se é menor que a data de hoje, se for não deixa passar.
        if($nascimento > $hoje){
            $cidades = Cidade::where('estado_id', $formulario['estado_id'])->get();
            $cursos = Instituicao::findOrFail($formulario['instituicao_id']);
            $niveis = DB::table('instituicoes_cursos')
            ->join('cursos', 'instituicoes_cursos.curso_id', '=', 'cursos.id')
            ->join('niveis', 'cursos.nivel_id', '=', 'niveis.id')
            ->select('niveis.id', 'niveis.nmNivel')
            ->where('instituicoes_cursos.id',$formulario['curso_id'])
            ->get();
            $turnos = DB::table('turnos_cursos')
            ->join('turnos', 'turnos_cursos.turno_id', '=', 'turnos.id')
            ->select('turnos.id', 'turnos.nmTurno')
            ->where('turnos_cursos.curso_instituicao_id',$formulario['curso_id'])
            ->orderBy('turnos.id')
            ->get();
            return redirect()
            ->back()
            ->with('error', 'Só é permitido o cadastro de pessoas maiores de 16 anos.')
            ->with('cidades', $cidades)
            ->with('cursos', $cursos)
            ->with('niveis', $niveis)
            ->with('turnos', $turnos)
            ->withInput();
        }
        if($formulario['nnSemestreAno'] > $CursoInstituicao->qtdDuracao){
            $cidades = Cidade::where('estado_id', $formulario['estado_id'])->get();
            $cursos = Instituicao::findOrFail($formulario['instituicao_id']);
            $niveis = DB::table('instituicoes_cursos')
            ->join('cursos', 'instituicoes_cursos.curso_id', '=', 'cursos.id')
            ->join('niveis', 'cursos.nivel_id', '=', 'niveis.id')
            ->select('niveis.id', 'niveis.nmNivel')
            ->where('instituicoes_cursos.id',$formulario['curso_id'])
            ->get();
            $turnos = DB::table('turnos_cursos')
            ->join('turnos', 'turnos_cursos.turno_id', '=', 'turnos.id')
            ->select('turnos.id', 'turnos.nmTurno')
            ->where('turnos_cursos.curso_instituicao_id',$formulario['curso_id'])
            ->orderBy('turnos.id')
            ->get();
            return redirect()
            ->back()
            ->with('error', 'Semestre ou Ano do estudante é maior que o do curso.')
            ->with('cidades', $cidades)
            ->with('cursos', $cursos)
            ->with('niveis', $niveis)
            ->with('turnos', $turnos)
            ->withInput();
        }
        //dd($formulario);
        $new = $this->estudante->create($formulario);
        if($new){
            if(($formulario['dtInicio'] != NULL) AND ($formulario['dtFim'] != NULL)){
                $new->portabilidade()->create($request->all());
            }
            return redirect()
                        //Redireciona para o cadastro da instituicao quando a mesma é cadastrada.
                        ->route('estudante.show', ['estudante' => $new->id])
                        //
                        ->with('success', 'Cadastro Realizado com Sucesso!');
        /**/
        }else {
            $cidades = Cidade::where('estado_id', $formulario['estado_id'])->get();
            $cursos = Instituicao::findOrFail($formulario['instituicao_id']);
            $niveis = DB::table('instituicoes_cursos')
            ->join('cursos', 'instituicoes_cursos.curso_id', '=', 'cursos.id')
            ->join('niveis', 'cursos.nivel_id', '=', 'niveis.id')
            ->select('niveis.id', 'niveis.nmNivel')
            ->where('instituicoes_cursos.id',$formulario['curso_id'])
            ->get();
            $turnos = DB::table('turnos_cursos')
            ->join('turnos', 'turnos_cursos.turno_id', '=', 'turnos.id')
            ->select('turnos.id', 'turnos.nmTurno')
            ->where('turnos_cursos.curso_instituicao_id',$formulario['curso_id'])
            ->orderBy('turnos.id')
            ->get();
            return redirect()
                        ->back()
                        ->with('error', 'Falha ao Cadastrar!')
                        ->with('cidades', $cidades)
                        ->with('cursos', $cursos)
                        ->with('niveis', $niveis)
                        ->with('turnos', $turnos)
                        ->withInput();
        }
        /*else {
            $cidades = Cidade::where('estado_id', $formulario['estado_id'])->get();
            return view('estudante.create', [
                 'error' => 'Falha ao Cadastrar!',
     'cidades' => $cidades
            ]);
}*/
    }

    public function show($id)
    {
        $titulo = "Estudantes";
        $estudante = $this->estudante->findOrFail($id);
        $tces = $this->tce->whereNotNull('dtCancelamento')->where('estudante_id',$id)->get();
        //dd($tces);
        //Verificação de Estado do Usuario
        if(Auth::user()->unidade_id != null){//Verificando se recebe unidade
            $relEstado = Auth::user()->unidade->estado->pluck('id')->toArray();
            if(in_array($estudante->estado_id,$relEstado)){//verificando se estado é igual ao estado da unidad
                $estudante;
                $tces;
            }else{
                return redirect()->back();
            }
        }
        //$oi = $estudante->CursoDaInstituicao->curso->nivel;
        //dd($oi);
        //dd($estudante->portabilidade()->count());
        return view('estudante.visualizar', compact('estudante','tces','titulo'));
    }

    //teste copiar
    // public function copiar($id)
    // {
    //     $estudante = $this->estudante->findOrFail($id)->toArray();
    //     //$tces = $this->tce->whereNotNull('dtCancelamento')->where('estudante_id',$id)->get();
    //     $estudante['qqq'] = Auth::user()->id;
    //     $estudante['dtNascimento'] = Carbon::parse($estudante['dtNascimento'])->format('d/m/Y');
    //     //$t = Carbon::createFromFormat('Y-m-d',$t);
    //     //$estudante['dtNascimento'] = Carbon::createFromFormat('Y-m-d', $estudante['dtNascimento'])->toDateString();
    //     //dd($estudante);

    //     //$oi = $estudante->CursoDaInstituicao->curso->nivel;
    //     //dd($oi);
    //     //dd($estudante->portabilidade()->count());
    //     Estudante2::create($estudante);
    //     return view('estudante.visualizar', compact('estudante','tces','titulo'));
    // }
    //teste copiar

    public function edit($id)
    {
        //Verificação de Estado do Usuario
        if(Auth::user()->unidade_id != null){
            $relEstado = Auth::user()->unidade->estado->pluck('id')->toArray();
            $instituicoes = Instituicao::WhereIn('estado_id',$relEstado)
                ->orWhere('isEad', 'S')
                ->orderBy('nmInstituicao')->get();
                //->paginate(config('sysGlobal.Paginacao'));
        }else{
            //$instituicoes = $this->instituicao->paginate(config('sysConfig.Paginacao'));
            $instituicoes = Instituicao::all()->sortBy('nmInstituicao');
        }
        //$instituicoes = Instituicao::all()->sortBy('nmInstituicao');
        $estados = Estado::all();

        $estudante = $this->estudante->findOrFail($id);
        $portabilidade = $estudante->portabilidade;
        //Verificação de Estado do Usuario
        if(Auth::user()->unidade_id != null){//Verificando se recebe unidade
            $relEstado = Auth::user()->unidade->estado->pluck('id')->toArray();
            if(in_array($estudante->estado_id,$relEstado)){//verificando se estado é igual ao estado da unidad
                $estudante;
            }else{
                return redirect()->route('estudante.index');
            }
        }
        return view('estudante.editar', compact('estudante','estados', 'instituicoes','portabilidade'));
    }

    public function update(Request $request, $id)
    {
        $estudante= $this->estudante->findOrFail($id);
        //chama função de validar formulario
        //$this->_validate($request);

        //dd($request->all());
        $formulario = $request->all();
        //dd($formulario);
        $atualizar = $estudante->update($formulario);
        if($atualizar){
             //Update estado_id tce
            $tce = $this->tce->where('estudante_id',$id)->update(['estado_id' => $estudante->estado_id]);
            if($estudante->tce_id != NULL)
            {
                $alterainstituicaodeensinoecursonotceativo = $this->tce->where('id',$estudante->tce_id)->update(['instituicao_id' => $estudante->instituicao_id, 'curso_id' => $estudante->curso_id]);
            }
            if(($formulario['dtInicio'] != NULL) AND ($formulario['dtFim'] != NULL)){
                if($estudante->portabilidade){
                    $estudante->portabilidade->update($request->all());
                } else {
                    $estudante->portabilidade()->create($request->all());
                }
            }
            //dd($alterainstituicaodeensinoecursonotceativo);
            return redirect()
                        ->route('estudante.show', ['estudante' => $id])
                        //->route('estudante.index')
                        ->with('success', 'Cadastro Atualizado com Sucesso!');
        } else {
            return redirect()
                        ->back()
                        ->with('error', 'Falha ao Atualizar!');
        }
    }

    public function historico($id)
    {
        $titulo = "Estudantes";

        $estudante = $this->estudante->findOrFail($id);
        $logs = $estudante->audits()->orderBy('created_at', 'desc')->paginate(config('sysConfig.Paginacao'));
        //dd($logs);
        if(Auth::user()->unidade_id != null){//Verificando se recebe unidade
            $relEstado = Auth::user()->unidade->estado->pluck('id')->toArray();
            if(in_array($instituicao->estado_id,$relEstado)){//verificando se estado é igual ao estado da unidad
                $estudante;
            }else{
                return redirect()->back();
            }
        }

        //dd($instituicao->cursoDaInstituicao);
        //$curso = $instituicao->cursoDaInstituicao->first();

        //dd($curso->curso);

        return view('estudante.historico', compact('titulo','estudante','logs'));

    }

    public function destroy(Estudante $estudante)
    {
        //
    }

    public function search(Request $request)
    {
       $dataForm = $request->except('_token');
       $estudantes = $this->estudante->search($request->key_search, config('sysConfig.Paginacao'));
       //$texto = Input::get('texto');
       //$status = Input::get('status');
    //    $instituicoes = $this->instituicao->where('', 'like', '%'.$status.'%')
    //   ->orWhere('erp_cost','like','%'.$texto.'%')
    //   ->orWhere('erp_productid','like','%'.$texto.'%')
    //   ->orWhereHas('descricao', function ($query) use ($texto) {
    //       $query->where('erp_name', 'like', '%'.$texto.'%');
    //   })
    //   ->orderBy('erp_status')
    //   ->paginate(20);

       return view('estudante.index', compact('estudantes', 'dataForm'));

    }

    public function verify($cpf){
      $estudantes = Estudante::all()->where('cdCPF',$cpf);
      if($estudantes->count() > 0){
        return response()->json(['status'=>'fail','nmEstudante'=>$estudantes->first()->nmEstudante,'id'=>$estudantes->first()->id]);
      }
      return response()->json(['status'=>'success']);
    }

    public function detalhes($hash)
    {
        $oportunidade = Oportunidade::where('code', $hash)->first();
        $cidade = Cidade::find($oportunidade->cidade_id);
        $estado = Estado::find($cidade->estado_id);
        $oportunidade_requisito = OportunidadeRequisito::where('oportunidade_id', $oportunidade->id)->get();
        
        for ($i=0; $i < count($oportunidade_requisito); $i++) {
          $requisitos = Requisito::where('id', $oportunidade_requisito[$i]->requisito_id)->get();
        }
        
        $oportunidade_tarefa = TarefaOportunidade::where('oportunidade_id', $oportunidade->id)->get();

        for ($i=0; $i < count($oportunidade_tarefa); $i++) {
          $tarefas = Tarefa::where('id', $oportunidade_tarefa[$i]->tarefa_id)->get();
        }
        
        dd($requisitos);
        return view('painel.concedente.oportunidade.vaga', compact('oportunidade','requisitos','tarefas', 'cidade', 'estado'));
    }
 
}
