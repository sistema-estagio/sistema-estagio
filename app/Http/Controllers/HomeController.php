<?php

namespace App\Http\Controllers;

use App\Models\Instituicao;
use App\Models\Estado;
use App\Models\Concedente;
use App\Models\SecConcedente;
use App\Models\UserConcedente;
use App\Models\Supervisor;
use App\Models\Estudante;
use App\Models\Tce;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Verificação de Estado do Usuario
        if (Auth::user()->unidade_id != null) {
            $relEstado = Auth::user()->unidade->estado->pluck('id')->toArray();
            //Mostrar na Box
            $todasInstituicoes = Instituicao::
                WhereIn('estado_id', $relEstado)
                ->get();
            $todosEstudantes = Estudante::
                WhereIn('estado_id', $relEstado)
                ->get();
            $todosConcedentes = Concedente::
                WhereIn('estado_id', $relEstado)
                ->get();
            $todosTces = Tce::
                WhereIn('estado_id', $relEstado)
                ->get();
            //Mostrar listagem
            $estudantes = Estudante::
                WhereIn('estado_id', $relEstado)
                ->orderBy('id', 'desc')
                //->take(config('sysGlobal.Paginacao'))
                ->limit(30)
                ->get();
            $concedentes = Concedente::
                WhereIn('estado_id', $relEstado)
                ->orderBy('id', 'desc')
                //->take(config('sysGlobal.Paginacao'))
                ->limit(30)
                ->get();
            $tces = Tce::
                where('dtCancelamento', '=', null)
                ->WhereIn('estado_id', $relEstado)
                ->orderBy('id', 'desc')
                //->take(config('sysGlobal.Paginacao'))
                ->limit(30)
                ->get();
        } else {
            //Mostrar na Box
            $todasInstituicoes = Instituicao::all();
            $todosEstudantes = Estudante::all();
            $todosConcedentes = Concedente::all();
            $todasSecretarias = SecConcedente::all();
            $todosSupervisores = Supervisor::all();
            $todosUsersConcedente = UserConcedente::all();

            $todosTces = Tce::all();
            //Mostrar listagem
            $estudantes = Estudante::
                orderBy('id', 'desc')
                //->take(config('sysGlobal.Paginacao'))
                ->limit(15)
                ->get();
            $concedentes = Concedente::
                orderBy('id', 'desc')
                //->take(config('sysGlobal.Paginacao'))
                ->limit(15)
                ->get();
            $tces = Tce::
                where('dtCancelamento', '=', null)
                ->orderBy('id', 'desc')
                //->take(config('sysGlobal.Paginacao'))
                ->limit(15)
                ->get();
        }
        $titulo = 'Inicial';
        //dd($tces);
        return view('home', compact('todosUsersConcedente', 'todasInstituicoes', 'todosSupervisores', 'todasSecretarias', 'instituicoes', 'todosEstudantes', 'estudantes', 'todosConcedentes', 'concedentes', 'todosTces', 'tces', 'titulo'));
    }
}
