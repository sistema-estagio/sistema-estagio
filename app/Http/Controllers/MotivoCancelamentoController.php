<?php

namespace App\Http\Controllers;

use App\Models\MotivoCancelamento;
use Illuminate\Http\Request;

class MotivoCancelamentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MotivoCancelamento  $motivoCancelamento
     * @return \Illuminate\Http\Response
     */
    public function show(MotivoCancelamento $motivoCancelamento)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MotivoCancelamento  $motivoCancelamento
     * @return \Illuminate\Http\Response
     */
    public function edit(MotivoCancelamento $motivoCancelamento)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MotivoCancelamento  $motivoCancelamento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MotivoCancelamento $motivoCancelamento)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MotivoCancelamento  $motivoCancelamento
     * @return \Illuminate\Http\Response
     */
    public function destroy(MotivoCancelamento $motivoCancelamento)
    {
        //
    }
}
