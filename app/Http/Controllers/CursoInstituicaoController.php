<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Http\Requests;
use App\Models\CursoInstituicao;
use App\Models\Turno;
use App\Models\Curso;
use App\Models\Instituicao;
use App\Models\CursoDuracao;

class CursoInstituicaoController extends Controller
{
    private $cursoInstituicao;

    public function __construct(CursoInstituicao $cursoInstituicao)
    {
        $this->cursoInstituicao = $cursoInstituicao;
    }

    //Não uso essa função
    public function index()
    {

    }
    //

    public function store(Request $request, $instituicao)
    {
        $data = $request->all();
        $data['instituicao_id'] = $instituicao;
        
        //$A = ;
        if ($this->cursoInstituicao->where('curso_id', $data['curso_id'])->where('instituicao_id', $data['instituicao_id'])->count() == 0) {
            $cursoInstituicao = $this->cursoInstituicao->create($data);

            if($cursoInstituicao){
                
                if(isset($data['turnos']))
                //cadastrar turnos selecionados para este curso
                $cursoInstituicao->turnos()->attach(array_values($data['turnos']));
                return redirect()
                            //Redireciona para o cadastro da instituicao quando a mesma é cadastrada.
                            ->route('instituicao.show', ['instituicao' => $instituicao])
                            ->with('successCurso', 'Curso Cadastrado com Sucesso!');
            } else {
                return redirect()
                            ->back()
                            ->with('errorCurso', 'Falha ao Cadastrar!');
            }
        } else {
                return redirect()
                            ->back()
                            ->with('errorCurso', 'Falha ao Cadastrar! Esse curso jà existe nesta instituição.');
        }
    }

    public function search(Request $request)
    {
       // dd($request->all());
       $dataForm = $request->except('_token');
       
       //$cursos = $this->curso->search($request->key_search, $this->totalPage); 
       $cursos = $this->curso->search($request->key_search, config('sysGlobal.Paginacao'));
       return view('curso.index', compact('cursos', 'dataForm'));
    }

    public function edit($instituicao_id, $curso_id)
    {
        $turnos = Turno::all();
        $cursoDuracoes = CursoDuracao::all();
        $instituicao = Instituicao::findOrFail($instituicao_id);
        $CursoInstituicao = $this->cursoInstituicao->findOrFail($curso_id);
        //$userRoles->lists('name','id')->toArray()
        $cursoTurnos = $CursoInstituicao->turnos->toArray();
        //dd($CursoInstituicao->qtdDuracao);
       // $cursoDaInstituicao->turnos->contains($curso->id)
       
       //dd($CursoInstituicao->turnos->contains(3));
       //dd($CursoInstituicao);
       //dd($CursoInstituicao->turnos->curso_instituicao_id);
       return view('instituicao.curso.editar', compact('turnos','CursoInstituicao','cursoDuracoes','instituicao'));
    }

    public function update(Request $request, $instituicao_id, $curso_id)
    {
        $CursoInstituicao = $this->cursoInstituicao->findOrFail($curso_id);

        //chama função de validar formulario
        //$this->_validate($request);

        
        $formulario = $request->all();
        //dd($formulario, $CursoInstituicao);
        $atualizar = $CursoInstituicao->update($formulario);
        if($atualizar){
            if(isset($formulario['turnos'])){
                //cadastrar turnos selecionados para este curso
                $CursoInstituicao->turnos()->sync(array_values($formulario['turnos']));
            } else {
                $CursoInstituicao->turnos()->detach();
            }
            return redirect()
                       ->route('instituicao.show', ['instituicao' => $instituicao_id])
                       ->with('successCurso', 'Curso Atualizado com Sucesso!');
        } else
            return redirect()
                        ->back()
                        ->with('errorCurso', 'Falha ao Atualizar!');
    }

}
