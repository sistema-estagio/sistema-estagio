<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Atividade;
use App\Models\Curso;
use Auth;

class AtividadeController extends Controller
{
    private $atividade;
    protected $totalPage = 2;

    public function __construct(Atividade $atividade)
    {
        $this->atividade = $atividade;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($curso)
    {
        $titulo = "Listagem das Atividades do Curso";
        
        $ocurso = Curso::findOrFail($curso);
        //dd($curso);
        $atividades = Atividade::where('curso_id', $curso)->orderBy('atividade')->get();
        //dd($atividades);
        if(!$atividades)
            return redirect()->back();
        return view('curso.atividade.index', compact('titulo','ocurso','atividades'));
        
    }


    public function store(Request $request, $curso)
    {
           //dd($request->all());
           $data = $request->all();
           $data['curso_id'] = $curso;
           $data['usuario_id'] = Auth::user()->id;
                    

           if($this->atividade->create($data))
   
               return redirect()
                           //->route('curso.index')
                           //Redireciona para o cadastro do curso quando a
                           ->route('curso.atividade.index', ['curso' => $curso])
                           //
                           ->with('success', 'Atividade Cadastrada com Sucesso!');
           else
               return redirect()
                           ->back()
                           ->with('error', 'Falha ao Cadastrar!');
            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
