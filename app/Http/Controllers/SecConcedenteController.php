<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SecConcedente;
use App\Models\UserConcedente;
use App\Models\Supervisor;
use App\Models\FolhaPagamento;
use App\Models\FolhaPagamentoItem;
use Carbon\Carbon;
use App\Models\Concedente;
use App\Models\Estado;
use App\Models\Cidade;
use App\Models\Tce;
use Validator;
use Illuminate\Support\Facades\Response;
use Auth;
use PDF;
use Illuminate\Support\Collection;
use App\Models\MotivoCancelamento;
//use App\Http\Requests\ConcedenteStoreUpdateFormRequest;


class SecConcedenteController extends Controller
{
  private $secConcedente;
  private $estadoModel;
  private $folhapagamento;

  public function __construct(SecConcedente $secConcedente, FolhaPagamento $folhapagamento)
  {
    $this->secConcedente = $secConcedente;
    $this->folhapagamento = $folhapagamento;
  }



  public function store(Request $request, $concedente)
  {
    $formulario = $request->all();
    $formulario['concedente_id'] = $concedente;
    //Pega id do usuario logado pra deixar o rastro no cadastro
    $formulario['usuario_id'] = Auth::user()->id;
    //dd($formulario);
    if($this->secConcedente->create($formulario))
    return redirect()
    ->route('concedente.show', ['concedente' => $concedente])
    ->with('successSecretaria', 'Cadastro Realizado com Sucesso!');
    else
    return redirect()
    ->back()
    ->with('errorSecretaria', 'Falha ao Cadastrar!');
  }

  public function show($idConcedente, $id)
  {
    $titulo = "Secretaria de Concedente";
    $secConcedente = $this->secConcedente->findOrFail($id);
    //usuario do concedente
    $usersConcedente = UserConcedente::where('sec_concedente_id', $id)->orderBy('id')->get();
    $tcesAtivos = Tce::
    where('sec_conc_id', $id)
    ->where('dtCancelamento', '=', NULL)
    ->get();
    $tcesCancelados = Tce::
    where('sec_conc_id', $id)
    ->where('dtCancelamento', '<>', null)
    ->get();
    $folhas = FolhaPagamento::
    where('secConcedente_id', $id)
    ->get();
    $supervisors = Supervisor::
    where('secConcedente_id', $id)
    ->get()->count();
    $aditivos = 0;
    foreach($tcesAtivos as $tce){
      if($tce->aditivos()->count() > 0){
        $aditivos += $tce->aditivos()->count();
      }
    }
    return view('concedente.secretaria.visualizar', compact('secConcedente','usersConcedente','titulo', 'tcesAtivos','tcesCancelados','folhas','supervisors', 'aditivos'));
  }

  public function edit($concedente_id, $id)
  {
    $titulo = "Edição de Secretaria de Concedente";
    $concedente = Concedente::findOrFail($concedente_id);
    $secConcedente = $this->secConcedente->findOrFail($id);

    return view('concedente.secretaria.editar', compact('concedente','secConcedente','titulo'));
  }

  public function showTcesAtivos($concedente_id,$id)
  {
    $titulo = "Tces Ativos Secretaria";
    $tces = Tce::where('sec_conc_id', $id)->where('dtCancelamento',NULL)->whereNotNull('dt4Via')->orderBy('id', 'desc')->get();
    return view('tce.index', compact('tces','titulo'));
  }

  public function showTcesPendentes($concedente_id,$id)
  {
    $titulo = "Tces Pendentes Secretaria";
    $tces = Tce::where('sec_conc_id', $id)->where('dtCancelamento',NULL)->whereNull('dt4Via')->orderBy('id', 'desc')->get();
    return view('tce.index', compact('tces','titulo'));
  }

  public function showTcesCancelados($concedente_id,$id)
  {
    $titulo = "Tces Cancelados Secretaria";
    $tcesCancelados = Tce::where('sec_conc_id', $id)->whereNotNull('dtCancelamento')->orderBy('dtCancelamento', 'desc')->paginate(config('sysGlobal.Paginacao'));
    $concedente = Concedente::find($tcesCancelados->first()->concedente_id);
    $secretaria = null;
    if($tcesCancelados->first()->sec_conc_id != null){
      $secretaria = SecConcedente::find($tcesCancelados->first()->sec_conc_id);
    }
    return view('tce.index', compact('tcesCancelados','titulo', 'concedente', 'secretaria'));
  }

  public function showTcesVencidos($concedente_id,$id)
  {
    $titulo = "Tces Ativos Secretaria";
    $tces = Tce::getVencidos($concedente_id, $id);
    $concedente = Concedente::find($concedente_id);
    $secretaria = null;
    $motivosCancelamento = MotivoCancelamento::all();
    if($id != null){
      $secretaria = SecConcedente::find($id);
    }
    return view('concedente.tce.index', compact('tces','titulo', 'secretaria', 'concedente', 'motivosCancelamento'));
  }

  public function update(Request $request, $concedente_id, $secretaria_id)
  {
    $concedente = Concedente::findOrFail($concedente_id);
    $secConcedente = $this->secConcedente->findOrFail($secretaria_id);
    //chama função de validar formulario
    //$this->_validate($request);
    $formulario = $request->all();
    $atualizar = $secConcedente->update($formulario);
    if($atualizar)
    return redirect()
    ->route('concedente.show', ['concedente' => $concedente_id])
    ->with('successSecretaria', 'Cadastro Atualizado com Sucesso!');
    else
    return redirect()
    ->back()
    ->with('error', 'Falha ao Atualizar!');
  }

  public function getSecretarias($idConcedente)
  {
    $concedente = Concedente::find($idConcedente);
    $secretarias = $concedente->secConcedente()->orderBy('nmSecretaria','ASC')->get(['id', 'nmSecretaria']);
    return Response::json($secretarias);
  }

  public function financeiroIndex($idConcedente, $id){
    $titulo = "Financeiro da Concedente";
    $secConcedente = $this->secConcedente->findOrFail($id);
    $folhas = $this->folhapagamento->where('secConcedente_id','=',$id)->orderBy('id')->get();
    return view('concedente.secretaria.financeiro.index', compact('secConcedente','titulo','folhas'));
  }

  public function financeiroStore(Request $request, $idConcedente, $id){
    $secConcedente = $this->secConcedente->findOrFail($id);
    $formulario = $request->all();
    $formulario['user_id'] = Auth::user()->id;
    $formulario['concedente_id'] = $secConcedente->concedente_id;
    $formulario['secConcedente_id'] = $id;
    $formulario['hash'] = base64_encode($formulario['referenciaMes'].$formulario['referenciaAno'].$formulario['secConcedente_id']);
    //dd($formulario, $concedente);
    $validar= Validator::make($request->all(), [
      'referenciaMes' => 'required',
      'referenciaAno' => 'required',

    ],[
      'referenciaMes.required' => ' O campo Mês Referência é Obrigatório.',
      'referenciaAno.required' => ' O campo Ano é Obrigatório.',
    ]);

    if($validar->fails()) {
      return
      redirect()
      ->route('concedente.secretaria.financeiro',['secConcedente' => $id,'idConcedente' => $secConcedente->concedente_id])
      ->withErrors($validar)
      ->withInput();
    }
    $hash =  base64_encode($formulario['referenciaMes'].$formulario['referenciaAno'].$formulario['secConcedente_id']);
    $teste = FolhaPagamento::where('secConcedente_id', $formulario['secConcedente_id'])
    ->where('hash', $hash)
    ->get();
    //dd($teste);
    if($teste->count() == 0){
    $folha = $this->folhapagamento->create($formulario);
    if($folha){
      if($request['alimentada'] === 'sim'){
        $iFolha = FolhaPagamentoItem::where('folha_id',$folha->id)->pluck('tce_id')->toArray();
        FolhaPagamentoItem::alimentaFolha($secConcedente->concedente_id, $secConcedente->id, $folha, $iFolha, $request['diasBaseEstagiados'], $request['diasTrabalhados']);
      }
    return redirect()
    ->route('concedente.secretaria.financeiro',['secConcedente' => $id,'idConcedente' => $secConcedente->concedente_id])
    ->with('success', 'Folha Criada com Sucesso!');
    }else{
      return redirect()
      ->back()
      ->with('error', 'Falha ao Cadastrar!');
    }
    }else{
      return redirect()
      ->route('concedente.secretaria.financeiro',['secConcedente' => $id,'idConcedente' => $secConcedente->concedente_id])
      ->with('error', 'Falha ao Cadastrar! Já existe uma Folha de Pagamento para esta competência.');
    }
  }

  public function financeiroVerFolha($idConcedente, $id, $folha)
  {
    $titulo = "Secretaria";
    $folha = FolhaPagamento::findOrFail($folha);

    $itensFolha = FolhaPagamentoItem::where('folha_id',$folha->id)->orderBy('created_at','desc')->get();
    // $itensFolha = $itensFolha->sortBy(function($folhapagamentoitem){
    //     //Definir qual coluna vai ser usada na ordenação, evidenciando o UTF8 pra nomes iniciados com letras acentuadas
    //     return iconv('UTF-8', 'ASCII//TRANSLIT', $folhapagamentoitem->tce->estudante->nmEstudante);
    // });

    //pega lista com os ids dos tces que estão na folha até o momento, pra conseguir tira-los das proximas listagens
    $iFolha = FolhaPagamentoItem::where('folha_id',$folha->id)->pluck('tce_id')->toArray();
    $ativos = new Collection;
    $ativo = [];
    //Lista com os TCES ativos sem os que já estão na folha
    $tcesAtivos = Tce::where('sec_conc_id',$id)//->orderBy(function($tce){
      //     //Definir qual coluna vai ser usada na ordenação, evidenciando o UTF8 pra nomes iniciados com letras acentuadas
      //     return iconv('UTF-8', 'ASCII//TRANSLIT', $tce->estudante->nmEstudante);
      // })
      ->where('dtCancelamento',NULL)
      ->where('dt4Via','<>',null)
      ->whereNotIn('id', $iFolha)
      ->get();
      //
      if($tcesAtivos->count() > 0){
        foreach($tcesAtivos as $tce){
          if($tce->dtFim > \Carbon\Carbon::now()->format('Y-m-d')){
            $ativo['id'] = $tce->id;
            $ativo['relatorio_id'] = $tce->tipo_tce;
            $ativo['nmEstudante'] = $tce->estudante->nmEstudante;
            $ativo['cdCPF'] = $tce->estudante->cdCPF;

            if(\Carbon\Carbon::parse($tce->dtInicio)->year === \Carbon\Carbon::now()->year && abs((\Carbon\Carbon::parse($tce->dtInicio)->month - \Carbon\Carbon::now()->month)) == 0){
              $ativo['diasTrabalhados'] = abs((\Carbon\Carbon::parse($tce->dtInicio)->day - 23));
              $ativo['diasBaseEstagiados'] = abs((\Carbon\Carbon::parse($tce->dtInicio)->day - 31));
            }elseif(\Carbon\Carbon::parse($tce->dtFim)->year === \Carbon\Carbon::now()->year && abs((\Carbon\Carbon::parse($tce->dtFim)->month - \Carbon\Carbon::now()->month)) == 0){
              $ativo['diasTrabalhados'] = abs(22 - (\Carbon\Carbon::parse($tce->dtFim)->day - 23));
              $ativo['diasBaseEstagiados'] = abs((\Carbon\Carbon::parse($tce->dtFim)->day - 31));
            }else{
              $ativo['diasTrabalhados'] = 22;
              $ativo['diasBaseEstagiados'] = 30;
            }
            $ativo['vlAuxilioTransporteReceber'] = $tce->vlAuxilioTransporte;
            $ativo['vlAuxilioTransporteDia'] = $tce->vlAuxilioTransporte / 30;

            $ativo['dtInicio'] = \Carbon\Carbon::parse($tce['dtInicio'])->format('d/m/Y');
            $ativo['dtFim'] = \Carbon\Carbon::parse($tce['dtFim'])->format('d/m/Y');

            if($tce->aditivos->count() > 0 && $tce->aditivos()->latest()->first()->vlAuxilioMensal != Null){
              $ativo['vlAuxilioMensal'] = number_format($tce->aditivos()->latest()->first()->vlAuxilioMensal, 2, ',', '.');
            }else{
              $ativo['vlAuxilioMensal'] = number_format($tce->vlAuxilioMensal, 2, ',', '.');
            }
            $ativos->push($ativo);
          }
        }
        $tcesAtivos = $ativos;
      }

      //Lista com os Tces Cancelados sem os que já estão na folha
      $tcesCancelados = Tce::whereNotNull('dtCancelamento')
      ->where('sec_conc_id',$id)
      ->orderBy('dtCancelamento', 'desc')
      ->whereNotIn('id', $iFolha)
      ->get();
      return view('concedente.secretaria.financeiro.visualizar', compact('folha','titulo','itensFolha', 'tcesAtivos','tcesCancelados','id', 'idConcedente'));
    }

    public function financeiroCriarItem($idConcedente, $id, $folha, $tce_id)
    {
      $titulo = "Inclusão de TCE na Folha de Pagamento da Secretaria";
      $folha = FolhaPagamento::findOrFail($folha);
      $tce = Tce::findOrFail($tce_id);
      //$concedente = $this->concedente->findOrFail($id);
      return view('concedente.secretaria.financeiro.folha.create', compact('titulo','folha','tce'));
    }

    public function financeiroItemStore(Request $request)
    {
      $formulario = $request->all();
      $validar= Validator::make($request->all(), [
        'diasBaseEstagiados' => 'required',
        'diasTrabalhados' => 'required',

      ],[
        'diasBaseEstagiados.required' => ' O campo Dias base Estágio é Obrigatório.',
        'diasTrabalhados.required' => ' O campo Dias Trabahados é Obrigatório.',
      ]);

      if($validar->fails()) {
        return
        redirect()
        ->back()
        //->route('concedente.financeiro',['concedente' => $concedente])
        ->withErrors($validar)
        ->withInput();
      }
      $formulario['user_id'] = Auth::user()->id;
      $formulario['hash'] = base64_encode($formulario['referenciaMes'].$formulario['referenciaAno'].$formulario['secConcedente_id']);

      if($formulario['diasRecesso'] != NULL){
        $formulario['vlRecesso'] = number_format($formulario['diasRecesso'] * str_replace(array(".",","), array("","."), round($formulario['vlAuxilioMensal'], 2) / 30), 2, '.', '');
        $vlRecesso = $formulario['vlRecesso'];
      } else {
        $vlRecesso = 0;
      }

      if($formulario['vlAuxilioTransporteDia'] != NULL){
        $vlAuxilioTransporteReceber = number_format(round($formulario['vlAuxilioTransporteDia']) * 30, 2, '.', '');
      } else {
        $vlAuxilioTransporteReceber = 0;
      }


      $formulario['descontoVlAuxTransporte'] = 0;
      $formulario['descontoVlAuxMensal'] = 0;
      if($formulario['faltas'] > 0){
        $formulario['descontoVlAuxTransporte'] = number_format(($formulario['faltas'] + $formulario['justificadas']) * $formulario['vlAuxilioTransporteDia'], 2, '.', '');
        $formulario['descontoVlAuxMensal'] = number_format((str_replace(array(".",","), array("","."),$formulario['vlAuxilioMensal']) / $formulario['diasBaseEstagiados'] * ($formulario['faltas'])), 2, '.', '');
      }

      if($formulario['justificadas'] > 0){
        $formulario['descontoVlAuxTransporte'] = $formulario['descontoVlAuxTransporte'] + abs(number_format($formulario['justificadas'] * $formulario['vlAuxilioTransporteDia'], 2, '.', ''));
      }

      $formulario['vlAuxilioMensalReceber'] = (number_format(str_replace(array(".",","), array("","."), $formulario['vlAuxilioMensal']), 2, '.', '') / 30) * $formulario['diasBaseEstagiados'] - $formulario['descontoVlAuxMensal'];

      $formulario['vlTotal'] = abs($vlRecesso + $vlAuxilioTransporteReceber + $formulario['vlAuxilioMensalReceber'] - ($formulario['descontoVlAuxMensal'] + $formulario['descontoVlAuxTransporte']));

      $hash =  base64_encode($formulario['referenciaMes'].$formulario['referenciaAno']);
      $teste = FolhaPagamentoItem::where('secConcedente_id', $formulario['secConcedente_id'])
      ->where('hash', $hash)
      ->where('folha_id', $formulario['folha_id'])
      ->where('tce_id', $formulario['tce_id'])
      ->get();
      //dd($teste->count());
      if($teste->count() == 0)
      if(FolhaPagamentoItem::create($formulario))
      return redirect()
      ->route('concedente.secretaria.financeiro.show',['idConcedente' => $formulario['concedente_id'], 'id'=>$formulario['secConcedente_id'], 'folha' => $formulario['folha_id']])
      ->with('success', 'TCE Adicionado na folha com Sucesso!');
      else
      return redirect()
      ->back()
      ->with('error', 'Falha ao Cadastrar!');
      else
      return redirect()
      //->back()
      ->route('concedente.secretaria.financeiro.show',['idConcedente' => $formulario['concedente_id'], 'id'=>$formulario['secConcedente_id'], 'folha' => $formulario['folha_id']])
      ->with('error', 'Falha ao Cadastrar! Já existe este lançamento para esta competência.');
    }
    // teste editar item folha
    public function financeiroEditarItem($idConcedente, $id, $folha, $itemFolhaId)
    {
      $titulo = "Edição de Item na Folha de Pagamento da Secretaria";
      //$folha = FolhaPagamento::findOrFail($id);
      $itemFolha = FolhaPagamentoItem::findOrFail($itemFolhaId);
      $tce = Tce::findOrFail($itemFolha->tce_id);
      //$concedente = $this->concedente->findOrFail($id);
      return view('concedente.secretaria.financeiro.folha.edit', compact('titulo','itemFolha','tce'));
    }

    public function financeiroUpdateItem(Request $request, $idConcedente, $id, $idfolha, $idItem)
    {
      $itemFolha = FolhaPagamentoItem::findOrFail($idItem);
      $formulario = $request->all();
      if($formulario['dtPagamento'] == NULL){
        $formulario['dtPagamento'] = null;

      }
      $formulario['hash'] = base64_encode($formulario['referenciaMes'].$formulario['referenciaAno']);
      $formulario['vlAuxilioMensalReceber'] = $itemFolha->vlAuxilioMensalReceber / 30 *  $formulario['diasBaseEstagiados'];

      if($formulario['diasRecesso'] != NULL){
        $formulario['vlRecesso'] = number_format($formulario['diasRecesso'] * str_replace(array(".",","), array("","."), $formulario['vlAuxilioTransporteReceber']) / 30, 2, '.', '');
        $vlRecesso = $formulario['vlRecesso'];
      } else {
        $vlRecesso = 0;
        $formulario['vlRecesso'] = null;
      }

      $vlAuxilioTransporteReceber = $itemFolha->vlAuxilioTransporteReceber;
      //round é um metódo usado para arredondar números
      $formulario['vlAuxilioTransporteDia'] = round($itemFolha->tce->vlAuxilioTransporte / 30, 2);


      $formulario['descontoVlAuxTransporte'] = 0;
      $formulario['descontoVlAuxMensal'] = 0;
      if($formulario['faltas'] > 0){
        $formulario['descontoVlAuxTransporte'] = $formulario['faltas'] * $formulario['vlAuxilioTransporteDia'];
        $formulario['descontoVlAuxMensal'] = $itemFolha->vlAuxilioMensalReceber / $formulario['diasBaseEstagiados'] * $formulario['faltas'];
      }

      if($formulario['justificadas'] > 0){
        $formulario['descontoVlAuxTransporte'] = $formulario['descontoVlAuxTransporte'] + abs(number_format($formulario['justificadas'] * $formulario['vlAuxilioTransporteDia'], 2, '.', ''));
      }

      $formulario['vlTotal'] = abs($vlRecesso + $vlAuxilioTransporteReceber + $formulario['vlAuxilioMensalReceber'] - ($formulario['descontoVlAuxMensal'] + $formulario['descontoVlAuxTransporte']));

      $hash =  base64_encode($formulario['referenciaMes'].$formulario['referenciaAno']);
      $teste = FolhaPagamentoItem::whereNotIn('id', [$idItem])
      ->where('secConcedente_id', $formulario['secConcedente_id'])
      ->where('hash', $hash)
      ->where('folha_id', $formulario['folha_id'])
      ->where('tce_id', $formulario['tce_id'])
      ->get();
      //dd($teste->count());
      if($teste->count() == 0){
        $atualizar = $itemFolha->update($formulario);
        if($atualizar)
        return redirect()
        ->route('concedente.secretaria.financeiro.show',['idConcedente' => $formulario['concedente_id'], 'id' => $id,'folha' => $formulario['folha_id']])
        ->with('success', 'Item Editado da folha com Sucesso!');
        else
        return redirect()
        ->back()
        ->with('error', 'Falha ao Cadastrar!');
      } else {
        return redirect()
        //->back()
        ->route('concedente.secretaria.financeiro.show',['idConcedente' => $formulario['concedente_id'], 'id' => $id,'folha' => $formulario['folha_id']])
        ->with('error', 'Falha ao Cadastrar! Já existe este lançamento para esta competência.');

      }
    }

    // teste deletar item folha
    public function financeiroDeletarItem($idConcedente, $id, $folha, Request $request)
    {
      //$titulo = "Edição de Item na Folha de Pagamento da Concedente";
      //$folha = FolhaPagamento::findOrFail($id);
      $itemFolha = FolhaPagamentoItem::find($request['id']);
      if($itemFolha){
        $itemFolha->delete();
      }
      return redirect()
      ->back()
      ->with('success', 'Item Deletado da folha com Sucesso!');
      //$concedente = $this->concedente->findOrFail($id);
      //return view('concedente.financeiro.editFolha', compact('titulo','itemFolha','tce'))
      //->with('success', 'Item Deletado da folha com Sucesso!');
    }

    //Folha em PDF
    public function financeiroVerFolhaPDF($idConcedente, $id, $folha, Request $request)
    {
      //modelo de relatorio
      $modelo = $request->query('modelo');
      //Querys Principal
      $folha = FolhaPagamento::findOrFail($folha);

      $itensFolha = FolhaPagamentoItem::where('folha_id',$folha->id)->get();
      $itensFolha = $itensFolha->sortBy(function($folhapagamentoitem){
        //Definir qual coluna vai ser usada na ordenação
        return preg_replace('/[`^~\'"]/', null, iconv('UTF-8', 'ASCII//TRANSLIT', $folhapagamentoitem->tce->estudante->nmEstudante));
      });
      //Gets
      $dtHoje = Carbon::today()->format('d/m/Y');
      // $pdf = PDF::loadView('concedente.secretaria.financeiro.relatorio.folhaPDF', compact('folha','itensFolha','dtHoje','modelo'))
      //     ->setPaper('a4','landscape')
      //     ->setOptions(['dpi' => 100,'enable_php'=>true]);
      // return $pdf->stream();
      return view('concedente.secretaria.financeiro.relatorio.recifeFolhaPDF', compact('folha','itensFolha','dtHoje','modelo'));
    }

    public function faturaPDF($id){

      $folha = Folha::find($id);

      if($folha){
        $vlTotal = $folha->sum($folha->folhaItem()->vlTotal);
        $dtFolha = $folha->referenciaMes.'/'.$folha->referenciaAno;
        $vlExtenso = NumberExtension::valorPorExtenso($vlTotal, false, null);
        $pdf = PDF::loadView('concedente.financeiro.relatorio.faturaPDF', compact('concedente', 'vlTotal', 'vlExtenso',
        'dtFolha', 'banco', 'agencia', 'conta', 'endereco', 'competencia', 'nTermo', 'cnpj', 'fone'))
            ->setPaper('a4',$orientacao)
            ->setOptions(['dpi' => 100,'enable_php'=>true]);
        return $pdf->stream();
      }
      return null;
    }

    public function reciboPDF(){

      $pdf = PDF::loadView('concedente.financeiro.relatorio.reciboPDF', compact('Concedente','tces','dtHoje'))
          ->setPaper('a4',$orientacao)
          ->setOptions(['dpi' => 100,'enable_php'=>true]);
      return $pdf->stream();
    }

    public function downloadRelatorioPDF(Request $request, $idConcedente, $id){

      $formulario = $request->all();
      $tcesCancelados;
      $dataFinal;
      $dataInicial;

      if ($formulario['dataInicial'] && $formulario['dataFinal']) {
        $dataInicial = explode('/', $formulario['dataInicial'])[2].'-'.explode('/', $formulario['dataInicial'])[1].'-'.explode('/', $formulario['dataInicial'])[0];
        $dataFinal = explode('/', $formulario['dataFinal'])[2].'-'.explode('/', $formulario['dataFinal'])[1].'-'.explode('/', $formulario['dataFinal'])[0];
        $tcesCancelados = Tce::where('sec_conc_id', $id)->whereNotNull('dtCancelamento')->where('cancelado_at', '<=', $dataFinal)->where('cancelado_at', '>=', $dataInicial)->orderBy('cancelado_at', 'desc')->get();
      } else if ($formulario['dataInicial'] == null) {
        $dataFinal = explode('/', $formulario['dataFinal'])[2].'-'.explode('/', $formulario['dataFinal'])[1].'-'.explode('/', $formulario['dataFinal'])[0];
        $tcesCancelados = Tce::where('sec_conc_id', $id)->whereNotNull('dtCancelamento')->where('cancelado_at', '<=', $dataFinal)->orderBy('cancelado_at', 'desc')->get();
      } else {
        $dataInicial = explode('/', $formulario['dataInicial'])[2].'-'.explode('/', $formulario['dataInicial'])[1].'-'.explode('/', $formulario['dataInicial'])[0];
        $tcesCancelados = Tce::where('sec_conc_id', $id)->whereNotNull('dtCancelamento')->where('cancelado_at', '>=', $dataInicial)->orderBy('cancelado_at', 'desc')->get();
      }

      $pdf = PDF::loadView('tce.relatorios.tcesCancelados', compact('request', 'tcesCancelados'))
      ->setPaper('a4','landscape')
      ->setOptions(['dpi' => 100,'enable_php'=>true]);
      return $pdf->download();

      // return view('tce.relatorios.tcesCancelados', compact('request', 'tcesCancelados'));
    }
  }
