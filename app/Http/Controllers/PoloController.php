<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Models\Instituicao;
use App\Models\Polo;
use App\Models\Estado;
use App\Models\Cidade;
use Auth;
use Validator;

class PoloController extends Controller
{
    private $polo;

    public function __construct(Polo $polo)
    {
        $this->polo = $polo;
    }

    public function index()
    {
        $titulo = "Listagem de Polos Matriz das Instituições de Ensino";
        $polos = $this->polo->paginate(config('sysConfig.Paginacao'));
            return view('instituicao.polo.index', compact('polos','titulo'));
    }

    public function create(Request $request)
    {
        $estados = Estado::all();
        $titulo = "Cadastro de Polo Matriz de Instituição de Ensino";
        //$cursos = $this->curso->all();
        //$instituicoes = $this->instituicao->paginate($this->totalPage);
        $cidades = [];
        return View('instituicao.polo.cadastro', [
            'estados' => $estados,
            'titulo' => $titulo,
            'cidades' => $request->session()->get('cidades') ?? []
        ]);
        //return view('instituicao.polo.cadastro', compact('estados','titulo'));
    }

    public function store(Request $request)
    {
        $formulario = $request->all();
        //dd($formulario);

        //pega usuario logado e seta o id do usuario ao cadastro da instituição, para sabermos quem cadastrou a instituição
        $formulario['usuario_id'] = Auth::user()->id;
        $validar= Validator::make($request->all(), [
            'nmPolo' => 'required',
            'cdCNPJ' => 'required|unique:polos|cnpj',
            'cdCEP' => 'required',
            'dsEndereco' => 'required',
            'nnNumero' => 'required',
            'nmBairro' => 'required',
            'estado_id' => 'required',
            'cidade_id' => 'required',
            //'dsEmail' => 'required',
        ],[
            'nmPolo.required' => ' O campo Nome do Polo é Obrigatório.',
            'cdCNPJ.required' => ' O campo CNPJ é obrigatório.',
            'cdCNPJ.unique' => ' Já existe um Polo com esse numero de CNPJ.',
            'cdCNPJ.cnpj' => ' CNPJ Inválido',
            'cdCEP.required' => ' O campo CEP é obrigatório.',
            'dsEndereco.required' => ' O campo Endereço é obrigatório.',
            'nnNumero.required' => ' O campo Numero é obrigatório.',
            'nmBairro.required' => ' O campo Bairro é obrigatório.',
            'estado_id.required' => ' O campo Estado é obrigatório.',
            'cidade_id.required' => ' O campo Cidade é obrigatório.',
            //'dsEmail.required' => ' O campo Email é obrigatório.',
        ]);

        if($validar->fails()) {
            $cidades = Cidade::where('estado_id', $formulario['estado_id'])->get();
            return 
            //dd($validar, $validar->any());
            redirect()
            ->back()
            //->route('estudante.create')
            ->with('cidades', $cidades)
            ->withErrors($validar)
            ->withInput();
        }

        $new = $this->polo->create($formulario);
        if($new)
            return redirect()
                    //Redireciona para o cadastro da instituicao quando a mesma é cadastrada.
                    ->route('polo.show', ['polo' => $new->id])
                    //
                    ->with('success', 'Cadastro do Polo Realizado com Sucesso!');
        else
            $cidades = Cidade::where('estado_id', $formulario['estado_id'])->get();
            return redirect()
                    ->back()
                    ->with('error', 'Falha ao Cadastrar Polo!')
                    ->with('cidades', $cidades)
                    ->withInput();
    }

    public function show($id)
    {
        $titulo = "Polo de Instituição de Ensino";
        //para formulario de cadastro de novo curso na instituição
        //dd($cursosInstituicao->curso);
        $polo = $this->polo->findOrFail($id);
        //dd($curso->curso);
        
        return view('instituicao.polo.visualizar', compact('titulo','polo'));
    
    }

    public function edit($id)
    {
        $titulo = "Edição de Polo de Instituição de Ensino";
        $estados = Estado::all();
        $polo = $this->polo->findOrFail($id);
       
        return view('instituicao.polo.editar', compact('titulo','polo','estados'));
    }

    public function update(Request $request, $id)
    {
        $polo = $this->polo->findOrFail($id);
        //chama função de validar formulario
        //$this->_validate($request);

        //dd($request->all());
        $formulario = $request->all();
        //dd($formulario);
        $atualizar = $polo->update($formulario);
        if($atualizar)
            return redirect()
                        ->route('polo.index')
                        ->with('success', 'Cadastro Atualizado com Sucesso!');
        else
            return redirect()
                        ->back()
                        ->with('error', 'Falha ao Atualizar!');

    }

    public function historico($id)
    {
        $titulo = "Polo da Instituição de Ensino";
        
        $polo = $this->polo->findOrFail($id);
        $logs = $polo->audits()->orderBy('created_at', 'desc')->paginate(config('sysConfig.Paginacao'));
        //dd($log);
        if(Auth::user()->unidade_id != null){//Verificando se recebe unidade
            $relEstado = Auth::user()->unidade->estado->pluck('id')->toArray();
            if(in_array($polo->estado_id,$relEstado)){//verificando se estado é igual ao estado da unidad
                $polo;
            }else{
                return redirect()->back();
            }
        }

        //dd($instituicao->cursoDaInstituicao);
        //$curso = $instituicao->cursoDaInstituicao->first();

        //dd($curso->curso);
        
        return view('instituicao.polo.historico', compact('titulo','polo','logs'));
    
    }

    //PEGAR POLO DA INSTITUICAO
    public function getInstituicoesPolo($idPolo)
    {        
        $polo = $this->polo->find($idPolo);
        $instituicoes = $polo->instituicoes;
        dd($instituicoes);
        //return Response::json($polo);
    }

}
