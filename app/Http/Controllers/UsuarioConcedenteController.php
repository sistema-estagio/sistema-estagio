<?php

namespace App\Http\Controllers;

use App\Models\Concedente;
use App\Models\UserConcedente;
use Illuminate\Http\Request;
use DB;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Auth;
use Validator;

class UsuarioConcedenteController extends Controller
{
    /**
     * @var UserConcedente
     */
    private $usuario;
    private $concedente;

    public function __construct(UserConcedente $usuario, Concedente $concedente)
    {
        $this->usuario      = $usuario;
        $this->concedente      = $concedente;
    }

    public function create($id)
    {
        $titulo = "Cadastro de Usuário da Concedente";

        $concedente = $this->concedente->findOrFail($id);

        return view('concedente.usuario.cadastro', compact('Permissoes','concedente'));
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $Campos = $request->all();
            //Encriptando a Senha
            $Campos['password'] = bcrypt($Campos['password']);
            //Pega usuario logado para gravar quem cadastrou
            $Campos['tipo'] = 'a';
            $Campos['usuario_id'] = Auth::user()->id;
            //dd($Campos);
            //Adicionando o Usuario ao bd
            $validar= Validator::make($request->all(), [
                'name' => 'required',
                'password' => 'required',
                'email' => 'required|unique:users_concedente',
            ],[
                'name.required' => ' O campo NOME USUARIO é Obrigatório.',
                'senha.required' => ' O campo SENHA é obrigatório.',
                'password.required' => ' O campo EMAIL é obrigatório.',
                'email.unique' => ' Já existe um usuario de alguma concedende usando este Email.',
            ]);

            if($validar->fails()) {
                return
                redirect()
                ->back()
                ->withErrors($validar)
                ->withInput();
            }
            $Usuario = $this->usuario->create($Campos);

        } catch (Exception $e) {
            DB::rollback();
            echo $e->getMessage();
        }
        DB::commit();

        if ($Usuario) {
            return redirect()
                //Redireciona para o cadastro da instituicao quando a mesma é cadastrada.
                ->route('concedente.show', ['concedente' => $Usuario->concedente_id])
                //
                ->with('successUser', 'Usuário Cadastrado com Sucesso!');
        } else {
        return redirect()
            ->back()
            ->with('errorUser', 'Falha ao Cadastrar Usuários!')
            ->withInput();
        }

    }

    public function edit($id)
    {
        $titulo = "Usuário da Concedente";
        $Usuario = $this->usuario->findOrFail($id);
        $concedente = $this->concedente->findOrFail($Usuario->concedente_id);
        //dd($Concedente);
        return view('concedente.usuario.editar', compact('Usuario','titulo','concedente'));
    }

    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $Usuario = $this->usuario->findOrfail($id);
            $Campos = $request->all();
            if($Campos['password'] != null){
                $Campos['password'] = bcrypt($Campos['password']);//Nova Senha se for diferente de '' Vazio
            }else{
                $Campos['password'] = $Usuario->password;//Se não mudar a senha fica antiga
            }
            //Atualizando o Usuario no bd
            $Usuario->update($Campos);

        } catch (Exception $e) {
            DB::rollback();
            echo $e->getMessage();
        }
        DB::commit();

        if ($Usuario) {
            return redirect()
                //Redireciona para o cadastro da instituicao quando a mesma é cadastrada.
                ->route('concedente.show', ['concedente' => $Usuario->concedente_id])
                //
                ->with('successUser', 'Usuário atualizado com Sucesso!');
        } else {
            return redirect()
                ->back()
                ->with('errorUser', 'Falha ao Atualizar Usuário!')
                ->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
