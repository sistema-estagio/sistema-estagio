<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use App\Models\ConveniosIe;
use App\Models\Instituicao;
use Auth;
use PDF;


class ConvenioIeController extends Controller
{
    public function __construct(ConveniosIe $convenioIe)
    {
        $this->convenioIe = $convenioIe;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {	                
        $formulario = $request->all();                   
        $formulario['usuario_id'] = Auth::user()->id;
        $instituicao_id = $formulario['instituicao_id'];
        //dd($formulario);
        $new = $this->convenioIe->create($formulario);

        //$convenio = $this->convenioIe->create(['instituicao_id'=>$instituicao_id, 'usuario_id'=>Auth::user()->id]);
        if($new)
            return redirect()
                        //Redireciona para o cadastro da instituicao quando a mesma é cadastrada.
                        ->route('instituicao.show', ['instituicao' => $instituicao_id])
                        //
                        ->with('Conveniosuccess', 'Convênio gerado com Sucesso!');
        else
            return redirect()
                        ->back()
                        ->with('Convenioerror', 'Falha ao Gerar Convênio!')
                        ->withInput();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, $convenio)
    {
        $id_convenio = $convenio;
        $instituicao = Instituicao::findOrFail($id);
        //$instituicao = $this->instituicao->findOrFail($id);
        return view('instituicao.convenio', compact('instituicao','id_convenio'));
        
        //$pdf = PDF::loadView('tce.relatorio.' . $relatorio->dsUrl . 'Final', compact('tce'))
        //->setPaper('a4')
        //->setOptions(['dpi' => 100]);
        //return $pdf->stream();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
