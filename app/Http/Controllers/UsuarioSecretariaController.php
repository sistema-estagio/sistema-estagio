<?php

namespace App\Http\Controllers;

use App\Models\Concedente;
use App\Models\SecConcedente;
use App\Models\UserConcedente;
use Illuminate\Http\Request;
use DB;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Auth;
use Validator;

class UsuarioSecretariaController extends Controller
{
    /**
     * @var UserConcedente
     */
    private $usuario;
    private $concedente;

    public function __construct(UserConcedente $usuario, Concedente $concedente)
    {
        $this->usuario      = $usuario;
        $this->concedente      = $concedente;
    }

    public function create($idConcedente, $id)
    {
        $concedente = $this->concedente->findOrFail($idConcedente);
        $secretaria = SecConcedente::find($id);
        if($secretaria){
          return view('concedente.secretaria.usuario.cadastro', compact('concedente', 'secretaria'));
        }
        return redirect()->back();
    }

    public function store(Request $request, $idConcedente, $id)
    {
        DB::beginTransaction();
        try {
            $Campos = $request->all();
            //Encriptando a Senha
            $Campos['password'] = bcrypt($Campos['password']);
            //Pega usuario logado para gravar quem cadastrou
            $Campos['tipo'] = 'a';
            $Campos['usuario_id'] = Auth::user()->id;
            $Campos['concedente_id'] = $idConcedente;
            $Campos['sec_concedente_id'] = $id;
            //dd($Campos);
            //Adicionando o Usuario ao bd
            $validar= Validator::make($request->all(), [
                'name' => 'required',
                'password' => 'required',
                'email' => 'required|unique:users_concedente',
            ],[
                'name.required' => ' O campo NOME USUARIO é Obrigatório.',
                'senha.required' => ' O campo SENHA é obrigatório.',
                'password.required' => ' O campo EMAIL é obrigatório.',
                'email.unique' => ' Já existe um usuario de alguma concedende usando este Email.',
            ]);

            if($validar->fails()) {
                return
                redirect()
                ->back()
                ->withErrors($validar)
                ->withInput();
            }
            $Usuario = $this->usuario->create($Campos);

        } catch (Exception $e) {
            DB::rollback();
            echo $e->getMessage();
        }
        DB::commit();

        if ($Usuario) {
            return redirect()
                //Redireciona para o cadastro da instituicao quando a mesma é cadastrada.
                ->route('concedente.secretaria.show', ['idConcedente' => $idConcedente, 'id'=>$id])
                //
                ->with('successUser', 'Usuário Cadastrado com Sucesso!');
        } else {
        return redirect()
            ->back()
            ->with('errorUser', 'Falha ao Cadastrar Usuários!')
            ->withInput();
        }

    }

    public function edit($idConcedente, $id, $user)
    {
        $titulo = "Usuário da Secretaria";
        $Usuario = $this->usuario->findOrFail($user);
        $concedente = $this->concedente->findOrFail($idConcedente);
        $secretaria = SecConcedente::find($id);
        if($secretaria && $Usuario){
          return view('concedente.secretaria.usuario.editar', compact('Usuario','titulo','concedente', 'secretaria'));
        }
        return redirect()->back();
    }

    public function update(Request $request, $idConcedente, $id, $user)
    {
        DB::beginTransaction();
        try {
            $Usuario = $this->usuario->findOrfail($user);
            $Campos = $request->all();
            if($Campos['password'] != null){
                $Campos['password'] = bcrypt($Campos['password']);//Nova Senha se for diferente de '' Vazio
            }else{
                $Campos['password'] = $Usuario->password;//Se não mudar a senha fica antiga
            }
            //Atualizando o Usuario no bd
            $Usuario->update($Campos);

        } catch (Exception $e) {
            DB::rollback();
            echo $e->getMessage();
        }
        DB::commit();

        if ($Usuario) {
            return redirect()
                //Redireciona para o cadastro da instituicao quando a mesma é cadastrada.
                ->route('concedente.secretaria.show', ['idConcedente' => $idConcedente, 'id'=> $id])
                //
                ->with('successUser', 'Usuário atualizado com Sucesso!');
        } else {
            return redirect()
                ->back()
                ->with('errorUser', 'Falha ao Atualizar Usuário!')
                ->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
