<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use App\Models\Curso;
use App\Models\CursoInstituicao;
use App\Models\Nivel;
use Illuminate\Support\Facades\DB;
use Auth;

class CursoController extends Controller
{
    private $curso;
    //protected $totalPage = config('sysConfig.Paginacao');

    public function __construct(Curso $curso)
    {
        $this->curso = $curso;
    }

    public function index()
    {
        //$niveis = Nivel::pluck('nmNivel', 'id')->all();
        $niveis = Nivel::all();
        //return view('sistema.modelos.create', compact('marcas',$marcas));
        //dd($niveis);
        $titulo = "Listagem de Cursos";
        //$cursos = $this->curso->all();
        //$cursos = $this->curso->orderBy('nmCurso', 'ASC')->paginate(config('sysConfig.Paginacao'));
        $cursos = $this->curso->orderBy('nmCurso', 'ASC')->get();
        
        return view('curso.index', compact('cursos','titulo','niveis'));

    }
    //
    public function store(Request $request)
    {
        //dd($request->all());
        $formulario = $request->all();
        $request->validate([
            'nmCurso' => 'required|unique:cursos',
        ]);
        $formulario['usuario_id'] = Auth::user()->id;
        //dd(Curso::create($formulario));
        
        if($this->curso->create($formulario))
            return redirect()
                        ->route('curso.index')
                        ->with('success', 'Cadastro Realizado com Sucesso!');
        else
            return redirect()
                        ->back()
                        ->with('error', 'Falha ao Cadastrar!');

    }

    public function search(Request $request)
    {
        
       // dd($request->all());
       $niveis = Nivel::all();
       $dataForm = $request->except('_token');
       $cursos = $this->curso->search($request->key_search, config('sysConfig.Paginacao')); 
       return view('curso.index', compact('cursos','niveis','dataForm'));
       
    }

    public function getNivelCursoInstituicao($idCurso)
    {
        $nivel = DB::table('instituicoes_cursos')
        ->join('cursos', 'instituicoes_cursos.curso_id', '=', 'cursos.id')
        ->join('niveis', 'cursos.nivel_id', '=', 'niveis.id')
        ->select('niveis.id', 'niveis.nmNivel')
        ->where('instituicoes_cursos.id',$idCurso)
        ->get();

       // $cursoInstituicao = CursoInstituicao::findOrFail($idCurso);
        //$curso = $cursoInstituicao->curso;
        //$nivel = $cursoInstituicao ->curso->nivel->toArray();
      
        //dd($nivel);
        return Response::json($nivel);
    }

    public function getTurnoCursoInstituicao($idCurso)
    {
        $turno = DB::table('turnos_cursos')
        ->join('turnos', 'turnos_cursos.turno_id', '=', 'turnos.id')
        ->select('turnos.id', 'turnos.nmTurno')
        ->where('turnos_cursos.curso_instituicao_id',$idCurso)
        ->orderBy('turnos.id')
        ->get();

       // $cursoInstituicao = CursoInstituicao::findOrFail($idCurso);
        //$curso = $cursoInstituicao->curso;
        //$nivel = $cursoInstituicao ->curso->nivel->toArray();
      
        //dd($nivel);
        return Response::json($turno);
    }
    /*
    public function getCidades($idEstado)
    {
        $estado = $this->estadoModel->find($idEstado);
        $cidades = $estado->cidades()->getQuery()->orderBy('nmCidade','ASC')->get(['id', 'nmCidade']);
        return Response::json($cidades);
    }
    */
    public function getAtividades($idCurso)
    {
        $curso = $this->curso->findOrFail($idCurso);
        //dd($curso);
        $atividades = $curso->atividades()->getQuery()->orderBy('atividade','ASC')->get(['id', 'atividade']);
        return Response::json($atividades);
    }
}
