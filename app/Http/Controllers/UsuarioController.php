<?php

namespace App\Http\Controllers;

use App\Models\Unidade;
use App\Models\User;
use Illuminate\Http\Request;
use DB;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Auth;

class UsuarioController extends Controller
{
    /**
     * @var User
     */
    private $usuario;
    private $permissoes;
    private $unidade;

    public function __construct(User $usuario, Unidade $unidade)
    {
        $this->usuario      = $usuario;
        $this->unidade      = $unidade;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->unidade_id == NULL){
            $Usuarios = $this->usuario->paginate(config('sysConfig.Paginacao'));
        } else {
            $Usuarios = $this->usuario->where('unidade_id',Auth::user()->unidade_id)->paginate(config('sysConfig.Paginacao'));
        }
        //dd($Usuarios);
        return view('usuario.index',compact('Usuarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Permissoes = Permission::all();
        $Unidades   = $this->unidade->all();
        return view('usuario.cadastro', compact('Permissoes','Unidades'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $Campos = $request->all();
            $Campos['password'] = bcrypt($Campos['password']);//Encriptando a Senha
            //Adicionando o Usuario ao bd
            $Usuario = $this->usuario->create($Campos);

            ///Definindo Grupo de Permissao///
            // Grupo de Permissao
            $GrupoP = Role::findByName('' . $Campos['nivel'] . '');
            $Usuario->assignRole($GrupoP);

            if ($Campos['nivel'] == 'admin') {
                //Regas de Permissao 'CAN'
                $PermissoesUsuario = $Campos['permissao_id'];
                $Usuario->givePermissionTo($PermissoesUsuario);
                // .FIM Regas de Permissao 'CAN'
            }
            /// .FIM Grupo de Permissao ///

        } catch (Exception $e) {
            DB::rollback();
            echo $e->getMessage();
        }
        DB::commit();

        if ($Usuario) {
            return redirect()
                //Redireciona para o cadastro da instituicao quando a mesma é cadastrada.
                ->route('usuario.show', ['usuario' => $Usuario->id])
                //
                ->with('success', 'Usuário Cadastrado com Sucesso!');
        } else {
        return redirect()
            ->back()
            ->with('error', 'Falha ao Cadastrar Usuários!')
            ->withInput();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Usuario = User::findOrFail($id);
        $Permissoes = Permission::all();
        $relacionamentoPermissoes   = $Usuario->permissions();
        return view('usuario.visualizar', compact('Usuario','Permissoes','relacionamentoPermissoes'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $Usuario = User::findOrFail($id);
        $Permissoes = Permission::all();
        $relacionamentoPermissoes   = $Usuario->permissions();
        $Unidades   = $this->unidade->all();
        return view('usuario.editar', compact('Usuario','Permissoes','relacionamentoPermissoes','Unidades'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $Usuario = User::findOrfail($id);
            $Campos = $request->all();
            if($Campos['password'] != null){
                $Campos['password'] = bcrypt($Campos['password']);//Nova Senha se for diferente de '' Vazio
            }else{
                $Campos['password'] = $Usuario->password;//Se não mudar a senha fica antiga
            }
            //Atualizando o Usuario no bd
            $Usuario->update($Campos);

            ///Inserindo na tabela de relacionamento Permissões///
            $Usuario->syncPermissions($Campos['permissao_id']);
            /// FIM. Inserindo na tabela de relacionamento Permissões///

        } catch (Exception $e) {
            DB::rollback();
            echo $e->getMessage();
        }
        DB::commit();

        if ($Usuario) {
            return redirect()
                //Redireciona para o cadastro da instituicao quando a mesma é cadastrada.
                ->route('usuario.show', ['usuario' => $Usuario->id])
                //
                ->with('success', 'Usuário atualizado com Sucesso!');
        } else {
            return redirect()
                ->back()
                ->with('error', 'Falha ao Atualizar Usuário!')
                ->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function search(Request $request)
    {
        // dd($request->all());
        $dataForm = $request->except('_token');
        $Usuarios = $this->usuario->search($request->key_search, config('sysConfig.Paginacao'));
        return view('usuario.index', compact('Usuarios', 'dataForm'));

    }
}
