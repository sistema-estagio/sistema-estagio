<?php

namespace App\Http\Controllers;

use App\Models\Supervisor;
use App\Models\SecConcedente;
use App\Models\SupervisorConcedenteSecretaria;
use App\Models\Concedente;
use App\Models\Tce;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;
use App\SRoco\RemoverAcentos;
use Auth;
use Validator;

class SupervisorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($concedente, $secretaria = null)
    {
        $titulo = "Listagem de Supervisores";
        //Verificação de Estado do Usuario
        $supervisors = new Collection;
        $concedente = Concedente::find($concedente);
        $supervisors = Supervisor::Where('concedente_id',$concedente->id)->get();
        if($secretaria != null){
          $secConcedente = SecConcedente::find($secretaria);
          $supervisors = $supervisors->where('secConcedente_id',$secretaria);
          return view('concedente.secretaria.supervisor.index', compact('supervisors','titulo','secConcedente'));
        }
        return view('concedente.supervisor.index', compact('supervisors','titulo','concedente'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($concedente, $secretaria = null)
    {
        $supervisors = new Collection;
        if($secretaria != null){
          $secConcedente = SecConcedente::find($secretaria);
          return view('concedente.secretaria.supervisor.create', compact('supervisors','titulo','secConcedente'));
        }else{
          $concedente = Concedente::find($concedente);
          return view('concedente.supervisor.create', compact('supervisors','titulo','concedente'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $concedente, $secretaria = null)
    {
        $validar = Validator::make($request->all(), [
            'nome' => 'required',
            'cpf' => 'required',
            'cargo' => 'required',
        ],[
            'nome.required' => ' O campo Nome é Obrigatório.',
            'cargo.required' => ' O campo Cargo é Obrigatório.',
            'cpf.required' => ' O campo CPF é obrigatório.',
            'cpf.cpf' => ' CPF Inválido',
        ]);

        $supervisors = new Collection;

        if($concedente != null){
          $supervisors = Supervisor::where(['cpf'=>$request->cpf],['concedente_id'=>$concedente]);
        }

        if($secretaria != null){
          $supervisors = Supervisor::where(['cpf'=>$request->cpf],['secConcedente_id'=>$secretaria]);
        }

        if($validar->fails()){
            return
            redirect()
            ->back()
            ->withErrors($validar)
            ->withInput();
        }else if($supervisors->count() > 0){
            return
            redirect()
            ->back()
            //->withErrors('Já existe supervisor com esse numero de CPF.')
            ->withInput();
        }

        $request['concedente_id'] = $concedente;
        $request['secConcedente_id'] = $secretaria;
        $supervisor = Supervisor::create($request->all());
        if($secretaria != null){
          $secConcedente = SecConcedente::find($secretaria);
          $supervisors = Supervisor::Where('secConcedente_id',$secretaria)->get();
          return view('concedente.secretaria.supervisor.index', compact('supervisors','titulo','secConcedente'))->with('success', 'Cadastro Realizado com Sucesso!');
        }else{
          $concedente = Concedente::find($concedente);
          $supervisors = Supervisor::Where('concedente_id',$concedente->id)->get();
          return view('concedente.supervisor.index', compact('supervisors','titulo','concedente'))->with('success', 'Cadastro Realizado com Sucesso!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Supervisor  $supervisor
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $supervisor = Supervisor::find($id);
        if($supervisor){
          $tces = Tce::all()->where('supervisor_id',$supervisor->id);
          // return dd($tces);
          if($supervisor->secConcedente_id != null){
            $secConcedente = SecConcedente::find($supervisor->secConcedente_id);
            return view('concedente.secretaria.supervisor.show', compact('secConcedente', 'supervisor','tces'));
          }else{
            $concedente = Concedente::find($supervisor->concedente_id);
            return view('concedente.supervisor.show', compact('concedente', 'supervisor','tces'));
          }
        }
        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Supervisor  $supervisor
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $supervisor = Supervisor::find($id);
        if($supervisor){
          if($supervisor->secConcedente_id != null){
            $secConcedente = SecConcedente::find($supervisor->secConcedente_id);
            return view('concedente.secretaria.supervisor.edit', compact('supervisor', 'secConcedente'));
          }else{
            $concedente = Concedente::find($supervisor->concedente_id);
            return view('concedente.supervisor.edit', compact('supervisor', 'concedente'));
          }
        }
        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Supervisor  $supervisor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $supervisor = Supervisor::find($id);
        if($supervisor){
          $supervisorAttached = Supervisor::where('cpf',$request['cpf']);
          if($supervisorAttached->where('concedente_id',$supervisor->concedente_id)->count() > 1 && $supervisorAttached->where('secConcedente_id',$supervisor->secConcedente_id)->count() > 1){
            return redirect()->back();
          }
            $supervisor->nome = $request['nome'];
            $supervisor->cpf = $request['cpf'];
            $supervisor->cargo = $request['cargo'];
            $supervisor->telefone = $request['telefone'];
            $supervisor->email = $request['email'];
            $supervisor->save();

          if($supervisor->secConcedente_id != null){
            $secConcedente = SecConcedente::find($supervisor->secConcedente_id);
            return redirect()->route('concedente.show.supervisor', ['idConcedente'=>$supervisor->concedente_id]);
          }else{
            $concedente = Concedente::find($supervisor->concedente_id);
            return redirect()->route('concedente.secretaria.show.supervisor', ['idConcedente'=>$supervisor->concedente_id, 'id'=>$supervisor->secConcedente_id]);
          }
        }
        return redirect()->back();
    }

    /**
     *
     *
     * @param  \App\Supervisor  $supervisor
     * @return \Illuminate\Http\Response
     */
    public function getConcedenteSecretaria($idConcedente, $secretaria = null)
    {
        //
        $supervisors = new Collection;
        $supervisorsArray = new Collection;
        if($secretaria != null){
          $supervisors = Supervisor::where('secConcedente_id',$secretaria)->get();
        }else{
          $supervisors = Supervisor::where('concedente_id',$idConcedente)->get();
        }

        foreach($supervisors as $supervisor){
          if(Supervisor::validate($supervisor) == true){
            $supervisorsArray->push($supervisor);
          }
        }

        $supervisorsArray = $supervisorsArray->orderBy(function($supervisor){
          return iconv('UTF-8', 'ASCII//TRANSLIT', RemoverAcentos::tirarAcentos($supervisor->nome));
        });

        return response()->json($supervisorsArray);
    }
}
