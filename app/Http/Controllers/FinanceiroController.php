<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\NumberExtension;
use App\Models\Concedente;
use App\Models\Supervisor;
use App\Models\Estado;
use App\Models\Cidade;
use App\Models\UserConcedente;
use App\Models\SecConcedente;
use App\Models\FolhaPagamento;
use App\Models\FolhaPagamentoItem;
use App\Models\Tce;
use Auth;
use Carbon\Carbon;
use Validator;
use PDF;
use Illuminate\Support\Collection;

class FinanceiroController extends Controller
{

  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {
    $this->middleware('auth:web');
  }
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function showConcedente(Request $request)
  {
    return $this->show($request);
  }

  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function showSecretaria(Request $request)
  {
    return $this->show($request);
  }

  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function show($hash)
  {
    $titulo = "Concedente";
    $folha = FolhaPagamento::where('hash',$hash)->get()->first();


    if($folha){
      $itensFolha = FolhaPagamentoItem::where('folha_id',$folha->id)->get();

      //pega lista com os ids dos tces que estão na folha até o momento, pra conseguir tira-los das proximas listagens
      $iFolha = FolhaPagamentoItem::where('folha_id',$folha['id'])->pluck('tce_id')->toArray();
      //Lista com os TCES ativos sem os que já estão na folha
      $tcesAtivos = Tce::where('dtCancelamento',NULL)->whereNotIn('id', $iFolha)->whereNotNull('dt4Via')->get();
      $tcesCancelados = Tce::whereNotNull('dtCancelamento')->orderBy('dtCancelamento', 'desc')->whereNotIn('id', $iFolha)->get();
      if($folha->secConcedente_id != null){
        $tcesCancelados = $tcesCancelados->where('sec_conc_id',$folha->secConcedente_id);
        $tcesAtivos = $tcesAtivos->where('sec_conc_id',$folha->secConcedente_id);
      }else{
        $tcesCancelados = $tcesCancelados->where('concedente_id',$folha->concedente_id);
        $tcesAtivos = $tcesAtivos->where('concedente_id',$folha->concedente_id);
      }

      //Lista com os Tces Cancelados sem os que já estão na folha

      $ativos = new Collection;
      $ativo = [];
      //
      $mesesAnos = \App\SRoco\Meses::getLatestMonth();
      //
      if($tcesAtivos->count() > 0){
        foreach($tcesAtivos as $tce){
          $ativo['id'] = $tce->id;
          $ativo['relatorio_id'] = $tce->tipo_tce;
          $ativo['nmEstudante'] = $tce->estudante->nmEstudante;
          $ativo['cdCPF'] = $tce->estudante->cdCPF;
          if(\Carbon\Carbon::parse($tce->dtInicio)->year === \Carbon\Carbon::now()->year && abs((\Carbon\Carbon::parse($tce->dtInicio)->month - \Carbon\Carbon::now()->month)) < 1){
            $ativo['diasTrabalhados'] = abs(\Carbon\Carbon::parse($tce->dtInicio)->day - 23);
            $ativo['diasBaseEstagiados'] = abs(\Carbon\Carbon::parse($tce->dtInicio)->day - 31);
          }elseif(\Carbon\Carbon::parse($tce->dtFim)->year === \Carbon\Carbon::now()->year && abs((\Carbon\Carbon::parse($tce->dtFim)->month - \Carbon\Carbon::now()->month)) < 1){
            $ativo['diasTrabalhados'] = abs(\Carbon\Carbon::parse($tce->dtFim)->day - 23);
            $ativo['diasBaseEstagiados'] = abs(\Carbon\Carbon::parse($tce->dtFim)->day - 31);
          }else{
            $ativo['diasTrabalhados'] = 22;
            $ativo['diasBaseEstagiados'] = 30;
          }

          $ativo['vlAuxilioTransporteReceber'] = $tce->vlAuxilioTransporte;
          $ativo['vlAuxilioTransporteDia'] = $tce->vlAuxilioTransporte / 30;

          $ativo['dtInicio'] = \Carbon\Carbon::parse($tce->dtInicio)->format('d/m/Y');
          $ativo['dtFim'] = \Carbon\Carbon::parse($tce->dtFim)->format('d/m/Y');
          if($tce->aditivos()->count() > 0){
            $ativo['dtFim'] = \Carbon\Carbon::parse($tce->aditivos()->latest()->first()->dtFim)->format('d/m/Y');
          }

          if($tce->aditivos->count() > 0 && $tce->aditivos()->latest()->first()->vlAuxilioMensal != Null){
            $ativo['vlAuxilioMensal'] = number_format($tce->aditivos()->latest()->first()->vlAuxilioMensal, 2, ',', '.');
          }else{
            $ativo['vlAuxilioMensal'] = number_format($tce->vlAuxilioMensal, 2, ',', '.');
          }

          $ativo['presenteEmOutrasFolhas'] = FolhaPagamentoItem::presenteNasFolhasAnteriores($tce->id, $mesesAnos['beforeMonth1'], $mesesAnos['beforeMonth2'], $mesesAnos['year1'], $mesesAnos['year2']);

          $ativos->push($ativo);
        }
        $tcesAtivos = $ativos;
      }

      if($folha->fechado == 'N'){
        return view('financeiro.folha.visualizar', compact('folha','titulo','itensFolha','tcesAtivos','tcesCancelados', 'concedente'));
      }else{
        return view('financeiro.folha.visualizar-fechada', compact('folha','titulo','itensFolha','tcesCancelados', 'concedente'));
      }
    }
    return redirect()->back();
  }

  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function concedenteReciboPDF(Request $request, $concedente, $idfolha)
  {
    return $this->reciboPDF($request, $concedente, null, $idfolha);
  }

  public function concedenteFaturaPDF(Request $request, $concedente, $idfolha)
  {
    return $this->faturaPDF($request, $concedente, null, $idfolha);
  }


  public function store(Request $request)
  {
    $formulario = $request->all();
    $folha = FolhaPagamento::find($request['folha_id']);
    if($folha){
      $validar= Validator::make($request->all(), [
        'diasBaseEstagiados' => 'required',
      ],[
        'diasBaseEstagiados.required' => ' O campo Dias base Estágio é Obrigatório.',
      ]);

      if($validar->fails()) {
        return
        redirect()
        ->back()
        //->route('concedente.financeiro',['concedente' => $concedente])
        ->withErrors($validar)
        ->withInput();
      }
      $formulario['user_id'] = Auth::user()->id;
      if($folha->secConcedente_id != null){
        $formulario['hash'] = base64_encode($formulario['referenciaMes'].$formulario['referenciaAno'].$folha->secConcedente_id);
      }else{
        $formulario['hash'] = base64_encode($formulario['referenciaMes'].$formulario['referenciaAno'].$folha->concedente_id);
      }

      $formulario['diasTrabalhados'] = 0;
      $formulario['vlAuxilioTransporteDia'] = (float) $formulario['vlAuxilioTransporteDia'];
      $formulario['diasBaseEstagiados'] = (int) $formulario['diasBaseEstagiados'];
      $formulario['faltas'] = (int) $formulario['faltas'];
      $formulario['justificadas'] = (int) $formulario['justificadas'];
      $formulario['diasRecesso'] = (int) $formulario['diasRecesso'];

      if($formulario['diasRecesso'] != NULL){
        $formulario['vlRecesso'] = (float) number_format($formulario['diasRecesso'] * $formulario['vlAuxilioTransporteDia'], 2, '.', '');
        $vlRecesso = $formulario['vlRecesso'];
      } else {
        $vlRecesso = 0;
      }

      $formulario['vlAuxilioMensal'] = (float) $formulario['vlAuxilioMensal'];
      $formulario['vlAuxilioTransporteReceber'] = (float) $formulario['vlAuxilioTransporteReceber'];

      if($formulario['vlAuxilioTransporteDia'] != NULL){
        $vlAuxilioTransporteReceber = number_format(($formulario['vlAuxilioTransporteDia'] * 30), 2, '.', '');
      } else {
        $vlAuxilioTransporteReceber = 0;
      }


      $formulario['descontoVlAuxTransporte'] = 0;
      $formulario['descontoVlAuxMensal'] = 0;
      if($formulario['faltas'] > 0){
        $formulario['descontoVlAuxTransporte'] = number_format(($formulario['faltas'] + $formulario['justificadas']) * $formulario['vlAuxilioTransporteDia'], 2, '.', '');
        $formulario['descontoVlAuxMensal'] = (($formulario['vlAuxilioMensal'] / 30) * $formulario['faltas']);
      }else if($formulario['justificadas'] > 0){
        $formulario['descontoVlAuxTransporte'] = $formulario['descontoVlAuxTransporte'] + abs(number_format($formulario['justificadas'] * $formulario['vlAuxilioTransporteDia'], 2, '.', ''));
      }

      $formulario['vlAuxilioMensalReceber'] = (($formulario['vlAuxilioMensal'] / 30) * $formulario['diasBaseEstagiados']) - $formulario['descontoVlAuxMensal'];

      $formulario['vlTotal'] = round(($vlAuxilioTransporteReceber + $formulario['vlAuxilioMensalReceber']) - ($formulario['descontoVlAuxMensal'] + $formulario['descontoVlAuxTransporte'] + $vlRecesso));

      $formulario['secConcedente_id'] = $folha->secConcedente_id;
      $formulario['concedente_id'] = $folha->concedente_id;

      $hash =  base64_encode($formulario['referenciaMes'].$formulario['referenciaAno']);
      if($folha->secConcedente_id != null){
        $teste = FolhaPagamentoItem::where('secConcedente_id', $folha->secConcedente_id)
        ->where('hash', $hash)
        ->where('folha_id', $formulario['folha_id'])
        ->where('tce_id', $formulario['tce_id'])
        ->get();
      }else{
        $teste = FolhaPagamentoItem::where('concedente_id', $folha->concedente_id)
        ->where('hash', $hash)
        ->where('folha_id', $formulario['folha_id'])
        ->where('tce_id', $formulario['tce_id'])
        ->get();
      }

      //dd($teste->count());
      if($teste->count() == 0)
      if(FolhaPagamentoItem::create($formulario))
      return redirect()
      ->back()
      ->with('success','TCE Adicionado na folha com Sucesso!');
      else
      return redirect()->back()->with('error','Falha ao Cadastrar! Já existe este lançamento para esta competência.');
    }
    return redirect()->back()->with('error', 'Falha ao Cadastrar!');
  }


  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function reciboPDF(Request $request, $concedente, $secretaria = null, $idfolha)
  {
    //
    $folha = FolhaPagamento::find($idfolha);
    $itensFolha = FolhaPagamentoItem::where('folha_id', $idfolha)->get();
    $vlTotal = 0;
    $vlTMensal = 0;
    $vlTTrans = 0;
    if($itensFolha->count() > 0){
      foreach($itensFolha as $item){
        $vlTotal += $item->vlTotal;
        $vlTMensal += $item->vlAuxilioMensalReceber;
        $vlTTrans += $item->vlAuxilioTransporteReceber;
      }
    }

    $concedente = Concedente::find($concedente);
    $unidade = $concedente->cidade->nmCidade;
    $estado = $concedente->cidade->estado->cdUF;
    if($secretaria != null){
      $secretaria = SecConcedente::find($secretaria);
      $unidade = $secretaria->cidade->nmCidade;
    }
    $dtFolha = $folha->referenciaMes.'/'.$folha->referenciaAno;
    $vlExtenso = NumberExtension::valorPorExtenso($itensFolha->sum('vlTotal'), true, null);
    $pdf = PDF::loadView('financeiro.relatorio.reciboPDF', compact('folha', 'itensFolha', 'unidade', 'estado', 'concedente', 'secretaria', 'vlTMensal', 'vlTTrans', 'vlTotal', 'vlExtenso',
    'dtFolha', 'banco', 'agencia', 'conta', 'endereco', 'competencia', 'nTermo', 'cnpj', 'fone'))
    ->setPaper('a4','paisagem')
    ->setOptions(['dpi' => 100,'enable_php'=>true]);
    return $pdf->stream();
  }

  public function faturaPDF(Request $request, $concedente, $secretaria = null, $idfolha)
  {
    $folha = FolhaPagamento::find($idfolha);
    $itensFolha = FolhaPagamentoItem::where('folha_id', $idfolha)->get();
    $vlTotal = 0;
    $vlTMensal = 0;
    $vlTTrans = 0;
    $descAux = 0;
    $descTrans = 0;
    if($itensFolha->count() > 0){
      foreach($itensFolha as $item){
        $vlTotal += $item->vlTotal;
        $descTrans += $item->descontoAuxilioTransporte;
        $descAux += $item->descontoVlAuxTransporte;
        $vlTMensal += $item->vlAuxilioMensalReceber;
        $vlTTrans += $item->vlAuxilioTransporteReceber;
      }
    }

    $concedente = Concedente::find($concedente);
    $unidade = $concedente->cidade->nmCidade;
    $estado = $concedente->cidade->estado->cdUF;
    if($secretaria != null){
      $secretaria = SecConcedente::find($secretaria);
      $unidade = $secretaria->cidade->nmCidade;
    }
    $dtFolha = $folha->referenciaMes.'/'.$folha->referenciaAno;
    $vlExtenso = NumberExtension::valorPorExtenso(($itensFolha->sum('vlTotal') + ($itensFolha->count() * $folha->concedente->vlEstagio) + ($itensFolha->where('diasBaseEstagiados','>',30)->count() * $folha->concedente->vlEstagio)), true, null);
    $pdf = PDF::loadView('financeiro.relatorio.faturaPDF', compact('folha', 'itensFolha', 'unidade', 'estado', 'concedente', 'descAux', 'descTrans','secretaria', 'vlTMensal', 'vlTTrans', 'vlTotal', 'vlExtenso',
    'dtFolha', 'banco', 'agencia', 'conta', 'endereco', 'competencia', 'nTermo', 'cnpj', 'fone'))
    ->setPaper('a4','paisagem')
    ->setOptions(['dpi' => 100,'enable_php'=>true]);
    return $pdf->stream();
  }

  // teste deletar item folha
  public function financeiroDeletarItem(Request $request)
  {

    $itemFolha = FolhaPagamentoItem::find($request['id']);
    if($itemFolha){
      $itemFolha->delete();
    }else{
      return redirect()->back()->with('error', 'Não foi possível excluir o item!');
    }
    return redirect()->back()->with('success', 'Item Deletado da folha com Sucesso!');
  }
  //Folha em PDF
  public function folhaPDF(Request $request)
  {
    $folha = FolhaPagamento::find($request['id']);
    if($folha){
      $itensFolha = FolhaPagamentoItem::where('folha_id', $request['id'])->get();
      $itensFolha = FolhaPagamentoItem::filterItens($itensFolha, $request);
      $vlTotal = 0;
      $vlTMensal = 0;
      $vlTTrans = 0;
      $descAux = 0;
      $descTrans = 0;
      $folhaAnterior = null;
      $itensFolhaAnterior = null;
      if($request['comparacao'] === 'sim'){
        $folhaAnterior = FolhaPagamento::where('concedente_id',$folha->concedente_id)->where('referenciaMes', FolhaPagamento::getReferenciaMesAnterior($folha->referenciaMes))->where('referenciaAno', $folha->referenciaAno)->get();
        if($folha->secConcedente_id != null){
          $folhaAnterior = $folhaAnterior->where('secConcedente_id',$folha->secConcedente_id)->first();
        }else{
          $folhaAnterior = $folhaAnterior->first();
        }
        if($folhaAnterior){
          $itensFolhaAnterior = FolhaPagamentoItem::where('folha_id', '=',$folhaAnterior->id)->get();
        }
      }
      if($itensFolha->count() > 0){
        foreach($itensFolha as $item){
          $item->exist = false;
          if($folhaAnterior != null){
            if($itensFolhaAnterior->where('tce_id',$item->tce_id)->count() == 0){
              $item->exist = true;
            }
          }
          $vlTotal += $item->vlTotal;
          $descTrans += $item->descontoAuxilioTransporte;
          $descAux += $item->descontoVlAuxTransporte;
          $vlTMensal += $item->vlAuxilioMensalReceber;
          $vlTTrans += $item->vlAuxilioTransporteReceber;
        }
      }

      if($request['result'] == 'view'){
        if($request['modelo'] == 'upa'){
          return view('financeiro.relatorio.upa', compact('folha','itensFolha','dtHoje','modelo'));
        }

        if($request['modelo'] == 'recife'){

          return view('financeiro.relatorio.recife', compact('folha','itensFolha','dtHoje','modelo'));
        }

        if($request['modelo'] == 'simplificado'){

          return view('financeiro.relatorio.simplificado', compact('folha','itensFolha','dtHoje','modelo','folhaAnterior','itensFolhaAnterior'));
        }

        $concedente = Concedente::find($folha->concedente_id);
        $unidade = $concedente->cidade->nmCidade;
        $estado = $concedente->cidade->estado->cdUF;
        $secretaria = null;
        if($folha->secConcedente_id != null){
          $secretaria = SecConcedente::find($folha->secConcedente_id);
        }

        $dtFolha = $folha->referenciaMes.'/'.$folha->referenciaAno;

        $vlExtenso = NumberExtension::valorPorExtenso(($itensFolha->sum('vlTotal') + ($itensFolha->count() * $folha->concedente->vlEstagio) + ($itensFolha->where('diasBaseEstagiados','>',30)->count() * $folha->concedente->vlEstagio)), true, null);
        if($request['modelo'] == 'recibo'){

          return view('financeiro.relatorio.recibo', compact('folha', 'concedente', 'secretaria','itensFolha', 'unidade', 'estado', 'concedente', 'descAux', 'descTrans','secretaria', 'vlTMensal', 'vlTTrans', 'vlTotal', 'vlExtenso',
          'dtFolha', 'banco', 'agencia', 'conta', 'endereco', 'competencia', 'nTermo', 'cnpj', 'fone'));
        }

        if($request['modelo'] == 'fatura'){

          return view('financeiro.relatorio.fatura', compact('folha', 'concedente', 'secretaria', 'itensFolha', 'unidade', 'estado', 'concedente', 'descAux', 'descTrans','secretaria', 'vlTMensal', 'vlTTrans', 'vlTotal', 'vlExtenso',
          'dtFolha', 'banco', 'agencia', 'conta', 'endereco', 'competencia', 'nTermo', 'cnpj', 'fone'));
        }
      }else{
        if($request['modelo'] == 'upa'){
          $pdf = PDF::LoadView('financeiro.relatorio.upa', compact('folha','itensFolha','dtHoje','modelo'))
          ->setPaper('a4','landscape')
          ->setOptions(['dpi' => 100,'enable_php'=>true]);
          return $pdf->download();
        }

        if($request['modelo'] == 'recife'){
          $pdf = PDF::LoadView('financeiro.relatorio.recife', compact('folha','itensFolha','dtHoje','modelo'))
          ->setPaper('a4','landscape')
          ->setOptions(['dpi' => 100,'enable_php'=>true]);
          return $pdf->download();
        }

        if($request['modelo'] == 'recibo'){
          return $this->reciboPDF();
        }

        if($request['modelo'] == 'fatura'){
          return $this->faturaPDF();
        }
      }
    }

    return redirect()->back();
  }


  public function fecharFolha($hash){
    $folha = FolhaPagamento::where('hash',$hash)->get()->first();
    if($folha){
      $folha->fechado = 'S';
      $folha->fechado_at = \Carbon\Carbon::now();
      $folha->fechado_user = Auth::id();
      $folha->save();
      return redirect()->back()->with('success','Folha foi fechada com sucesso');
    }
    return redirect()->back()->with('error','Não foi possível fechar a folha');
  }

  public function relatorioGeral(Request $request, $id){
    $concedente = Concedente::find($id);
    $folhas = FolhaPagamento::where('concedente_id',$id)->get();

    $folhas = $folhas->where('referenciaMes', $request['referenciaMes']);
    $folhas = $folhas->where('referenciaAno', $request['referenciaAno']);

    if($folhas->count() > 0){
      if($request['modelo'] == 'pdf'){

        $pdf = PDF::loadView('financeiro.relatorio.geral', compact('folhas', 'concedente'))
        ->setPaper('a4','paisagem')
        ->setOptions(['dpi' => 100,'enable_php'=>true]);
        return $pdf->stream();
      }else{

        return view('financeiro.relatorio.geral', compact('folhas', 'concedente'));
      }
    }else{
      return redirect()->back()->with('error','Nenhuma folha foi gerada com as referências informadas');
    }
    return redirect()->back()->with('error','Não foi possível gerar o relatório');
  }


}
