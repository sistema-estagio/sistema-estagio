<?php

namespace App\Http\Controllers\Concedente;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Estudante;
use App\Models\Tce;
use App\Models\MotivoCancelamento;
use App\Models\Concedente as ConcedenteModel;
use App\Models\SecConcedente;
use App\Models\UserConcedente;
use App\Models\Relatorio;
use App\Models\Aditivo;
use App\Models\FolhaPagamentoItem;
use App\Models\FolhaPagamento;
use App\Models\Supervisor;
use Carbon\Carbon;
use Auth;
use DB;
use PDF;

class HomeConcedenteController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('auth:web_concedente');
    }

    /**
     * show dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $response = UserConcedente::verifyUserReturn(Auth::id(), 'm');

        if ($response === true) {
            return redirect()->route('painel.mediador.index');
        }
        $titulo = 'Pagina Inicial';
        //Pegar dados da concedente através do usuario logado
        $concedente = ConcedenteModel::findOrFail(Auth::user()->concedente_id);
        $secConcedente = SecConcedente::find(Auth::user()->sec_concedente_id);
        /*
        $tces = Tce::with(
        [
        'contents' => function ($query) {
        $query->where('contents.status', 2);
  }
]
)->get();
*/
        if (!$secConcedente) {
            //Pegar TCEs do concedente que não estão cancelados
            $tces = Tce::where('concedente_id', Auth::user()->concedente_id)->where('dtCancelamento', null)->orderBy('dtInicio', 'desc')->limit(20)->get();

            $tcesAtivos = Tce::where('concedente_id', Auth::user()->concedente_id)->where('dtCancelamento', null)->where('dt4Via', '!=', null);

            $tcesPendentes = Tce::where('concedente_id', Auth::user()->concedente_id)->where('dtCancelamento', null)->where('dt4Via', null);

            $tcesCancelados = Tce::where('concedente_id', Auth::user()->concedente_id)->where('dtCancelamento', '<>', null);

            $supervisors = Supervisor::where('concedente_id', Auth::user()->concedente_id);
        } else {
            //Pegar TCEs do concedente que não estão cancelados
            $tcesAtivos = Tce::where('sec_conc_id', Auth::user()->sec_concedente_id)->where('dtCancelamento', null);

            $tcesPendentes = Tce::where('sec_conc_id', Auth::user()->sec_concedente_id)->where('dtCancelamento', null)->where('dt4Via', null);

            $tces = Tce::where('sec_conc_id', Auth::user()->sec_concedente_id)->where('dtCancelamento', null)->orderBy('dtInicio', 'desc')->limit(20)->get();

            $tcesCancelados = Tce::where('sec_conc_id', Auth::user()->sec_concedente_id)->where('dtCancelamento', '<>', null);

            $supervisors = Supervisor::where('secConcedente_id', Auth::user()->sec_concedente_id);
        }

        return view('painel.concedente.index', compact('concedente', 'titulo', 'tces', 'tcesAtivos', 'secConcedente', 'tcesCancelados', 'tcesPendentes', 'supervisors'))->with(['active' => 'dashboard']);
    }

    public function tce($idTce)
    {
        $hoje = Carbon::today()->format('d/m/Y');
        $id = base64_decode($idTce);
        //dd($idTce, $id);
        $titulo = 'Tce';
        $tce = Tce::find($id);
        $folhaPagamentoItem = FolhaPagamentoItem::where('tce_id', $id)->orderBy('id')->get();
        //dd($folhaPagamentoItem);
        $aditivos = Aditivo::where('tce_id', $id)->orderBy('id', 'desc')->get();
        //Lista dos motivos cancelamentos
        $motivosCancelamento = motivoCancelamento::all();
        //Verifica se o TCE é realmente desta concedente
        if (Auth::user()->concedente_id != $tce->concedente_id) {
            return redirect()
    ->route('painel.concedente.dashboard')
    ->with('error', 'Você não pode ver um TCE que não pertence a sua CONCEDENTE!');
        } else {
            $tce;
            //return redirect()->route('tce.index');
        }
        //dd($tce);

        return view('painel.concedente.tce.show', compact('motivosCancelamento', 'hoje', 'titulo', 'tce', 'aditivos', 'folhaPagamentoItem'))->with(['active' => 'tce']);
    }

    public function aditivo($idTce, $idAditivo)
    {
        $hoje = Carbon::today()->format('d/m/Y');
        $id = base64_decode($idTce);
        //dd($idTce, $id);
        $titulo = 'Aditivo';
        $aditivo = Aditivo::find($idAditivo);
        //$folhaPagamentoItem = FolhaPagamentoItem::where('tce_id', $id)->orderBy('id')->get();
        dd($aditivo);
        $aditivos = Aditivo::where('tce_id', $id)->orderBy('id', 'desc')->get();
        //Lista dos motivos cancelamentos
        $motivosCancelamento = motivoCancelamento::all();
        //Verifica se o TCE é realmente desta concedente
        if (Auth::user()->concedente_id != $tce->concedente_id) {
            return redirect()
    ->route('painel.concedente.dashboard')
    ->with('error', 'Você não pode ver um TCE que não pertence a sua CONCEDENTE!');
        } else {
            $tce;
            //return redirect()->route('tce.index');
        }
        //dd($tce);
        return view('painel.concedente.tce.aditivo.show', compact('motivosCancelamento', 'hoje', 'titulo', 'tce', 'aditivos', 'folhaPagamentoItem'))->with(['active' => 'tce']);
    }

    public function showFolha($idTce)
    {
        $hoje = Carbon::today()->format('d/m/Y');
        $id = base64_decode($idTce);
        //dd($idTce, $id);
        $titulo = 'Folha de Pagamento do TCE';
        $tce = Tce::find($id);
        $folhasPagamentoItem = FolhaPagamentoItem::where('tce_id', $id)->orderBy('referenciaMes')->get();
        //dd($folhaPagamentoItem);

        //dd($tce);
        return view('painel.concedente.folhaPagamentoTce', compact('hoje', 'titulo', 'tce', 'folhasPagamentoItem'))->with(['active' => 'tce']);
    }

    public function financeiro()
    {
        $hoje = Carbon::today()->format('d/m/Y');
        $titulo = 'Folhas de Pagamento';
        if (Auth::user()->sec_concedente_id == null) {
            $folhasPagamento = FolhaPagamento::where('concedente_id', Auth::user()->concedente_id)->orderBy('referenciaMes')->get();
        } else {
            $folhasPagamento = FolhaPagamento::where('secConcedente_id', Auth::user()->sec_concedente_id)->orderBy('referenciaMes')->get();
        }
        // dd($folhasPagamento);
        return view('painel.concedente.folhasPagamento', compact('titulo', 'folhasPagamento'))->with(['active' => 'financeiro']);
    }

    public function financeiroVerFolhaPDF($id)
    {
        //Querys Principal
        $folha = FolhaPagamento::findOrFail($id);

        if ($folha->secConcedente_id != Auth::user()->sec_concedente_id && $folha->concedente_id != Auth::user()->concedente_id) {
            return redirect()
    ->route('painel.concedente.dashboard')
    ->with('error', 'Você não pode ver uma folha de pagamento que não pertence a sua CONCEDENTE!');
        }

        //$itensFolha = FolhaPagamentoItem::all()->where('folha_id',$id);
        $itensFolha = FolhaPagamentoItem::all()->sortBy(function ($folhapagamentoitem) {
            //Definir qual coluna vai ser usada na ordenação
            return $folhapagamentoitem->tce->estudante->nmEstudante;
        })
  ->where('folha_id', $id);
        //Gets
        $dtHoje = Carbon::today()->format('d/m/Y');
        $pdf = PDF::loadView('concedente.financeiro.relatorio.pconcedentefolhaPDF', compact('folha', 'itensFolha', 'dtHoje'))
  ->setPaper('a4', 'landscape')
  ->setOptions(['dpi' => 100, 'enable_php' => true]);

        return $pdf->stream();
    }

    public function cancel(Request $request, $tce_id)
    {
        $id = base64_decode($tce_id);
        DB::beginTransaction();
        try {
            //Pegando o TCE
            $tce = Tce::findOrFail($tce_id);
            $formulario = $request->all();
            $request->validate([
      'dtCancelamento' => 'required',
      'motivoCancelamento_id' => 'required',
    ]);
            //Auth::user()->name
            //Pega ususario logado pra setar o ususario que cancelou já nos campos para update
            //$usuario = Auth::user()->id;
            $formulario['usuarioConcedenteCancelamento_id'] = Auth::user()->id;
            //dd($formulario,$id);

            //Atualizando o TCE para cancelado
            $atualizar = $tce->update($formulario);
        } catch (Exception $e) {
            DB::rollback();
            echo $e->getMessage();
        }
        DB::commit();

        if ($atualizar) {
            //Atualizando campo de TCE na tabela estudante
            Estudante::where(['id' => $tce->estudante_id])->update(['tce_id' => null]);

            return redirect()
    ->route('painel.concedente.tce', ['idTce' => base64_encode($tce->id)])
    ->with('success', 'TCE Cancelado com Sucesso!');
        } else {
            return redirect()
    ->back()
    ->with('error', 'Falha ao Cancelar!');
        }
    }

    public function declaracaoCancelado($idTce)
    {
        $id = base64_decode($idTce);
        //dd($idTce, $id);
        //$titulo = "Tce";
        $tce = Tce::find($id);
        $aditivos = Aditivo::where('tce_id', $id)->orderBy('id', 'desc')->get();
        //Verifica se o TCE é realmente desta concedente
        $dtHoje = Carbon::today()->format('d/m/Y');
        //dd($tce, $aditivos);

        //return view('painel.concedente.declaracaoCanceladoPDF', compact('tce','aditivos','dtHoje'));

        $pdf = PDF::loadView('painel.concedente.declaracaoCanceladoPDF', compact('tce', 'aditivos', 'dtHoje'))
  ->setPaper('a4')
  ->setOptions(['dpi' => 100]);

        return $pdf->stream('Declaração TCE '.($tce->migracao == null ? $tce->id : $tce->migracao).' '.$tce->estudante->nmEstudante.'.pdf');
    }

    public function estudante($idTce)
    {
        $id = base64_decode($idTce);
        //dd($idTce, $id);
        $titulo = 'Estudante';
        $tce = Tce::find($id);
        $tces = Tce::whereNotNull('dtCancelamento')->where('estudante_id', $tce->estudante_id)->where('concedente_id', $tce->concedente_id)->get();
        //dd($tces);
        //$aditivos = Aditivo::where('tce_id', $id)->orderBy('id', 'desc')->get();
        //Verifica se o TCE é realmente desta concedente
        if (Auth::user()->concedente_id != $tce->concedente_id && Auth::user()->sec_concedente_id != $tce->sec_conc_id) {
            return redirect()
    ->route('painel.concedente.dashboard')
    ->with('error', 'Você não pode ver um ESTUDANTE que não pertence a sua CONCEDENTE!');
        } else {
            $tce;
            //return redirect()->route('tce.index');
        }
        //dd($tce);
        return view('painel.concedente.estudante', compact('titulo', 'tce', 'tces'))->with(['active' => 'tce']);
    }

    public function instituicao($idTce)
    {
        $id = base64_decode($idTce);
        //dd($idTce, $id);
        $titulo = 'Instituicao';
        $tce = Tce::find($id);
        //dd($tce);
        //$aditivos = Aditivo::where('tce_id', $id)->orderBy('id', 'desc')->get();
        //Verifica se o TCE é realmente desta concedente
        if (Auth::user()->concedente_id != $tce->concedente_id && Auth::user()->sec_concedente_id != $tce->sec_conc_id) {
            return redirect()
    ->route('painel.concedente.dashboard')
    ->with('error', 'Você não pode ver um ESTUDANTE que não pertence a sua CONCEDENTE!');
        } else {
            $tce;
            //return redirect()->route('tce.index');
        }
        //dd($tce);
        return view('painel.concedente.instituicao', compact('titulo', 'tce'))->with(['active' => 'tce']);
    }

    public function tcesAtivos()
    {
        $titulo = 'Tces Ativos';
        //Pegar dados da concedente através do usuario logado
        $concedente = ConcedenteModel::findOrFail(Auth::user()->concedente_id);
        //Pegar TCEs do concedente que não estão cancelados
        if (Auth::user()->sec_concedente_id == null) {
            $tces = Tce::where('concedente_id', Auth::user()->concedente_id)->whereNull('dtCancelamento')->orderBy('id', 'desc')->get();
        } else {
            $tces = Tce::where('sec_conc_id', Auth::user()->sec_concedente_id)->whereNull('dtCancelamento')->orderBy('id', 'desc')->get();
        }
        //$tces = ConcedenteModel::find(Auth::user()->concedente_id)->tce()->where('dtCancelamento','=', NULL)->get();
        //dd($tces);
        return view('painel.concedente.tcesativos', compact('titulo', 'tces'))->with(['active' => 'dashboard']);
        //Auth::user()->name
    }

    public function tces()
    {
        $titulo = 'Tces';
        //Pegar dados da concedente através do usuario logado
        $concedente = ConcedenteModel::findOrFail(Auth::user()->concedente_id);
        //Pegar TCEs do concedente que não estão cancelados
        if (Auth::user()->sec_concedente_id == null) {
            $tces = Tce::where('concedente_id', Auth::user()->concedente_id)->orderBy('id', 'desc')->get();
        } else {
            $tces = Tce::where('sec_conc_id', Auth::user()->sec_concedente_id)->orderBy('id', 'desc')->get();
        }

        //$tces = ConcedenteModel::find(Auth::user()->concedente_id)->tce()->where('dtCancelamento','=', NULL)->get();
        //dd($tces);
        return view('painel.concedente.tces', compact('titulo', 'tces'));
        //Auth::user()->name
    }

    public function tcesCancelados()
    {
        $titulo = 'Tces Cancelados';
        //Pegar dados da concedente através do usuario logado
        $concedente = ConcedenteModel::findOrFail(Auth::user()->concedente_id);
        //Pegar TCEs do concedente que não estão cancelados
        if (Auth::user()->sec_concedente_id == null) {
            $tces = Tce::where('concedente_id', Auth::user()->concedente_id)->whereNotNull('dtCancelamento')->orderBy('dtCancelamento', 'desc')->get();
        } else {
            $tces = Tce::where('sec_conc_id', Auth::user()->sec_concedente_id)->whereNotNull('dtCancelamento')->orderBy('dtCancelamento', 'desc')->get();
        }

        return view('painel.concedente.tcescancelados', compact('titulo', 'tces'))->with(['active' => 'dashboard']);
        //Auth::user()->name
    }

    public function showRelatorio($idTce, $idRelatorio)
    {
        $id = base64_decode($idTce);
        $tce = Tce::findOrFail($id);
        $relatorio = Relatorio::findOrFail($idRelatorio);

        //$valor = extenso($tce->vlAuxilioMensal);
        //dd($tce->secConcedente);
        switch ($relatorio->id) {
    case $relatorio->id:
    //dd($relatorio->nmRelatorio);
    //return view('tce.relatorio.' . $relatorio->dsUrl. 'Div', compact('tce'));

    $pdf = PDF::loadView('tce.relatorio.'.$relatorio->dsUrl, compact('tce'))
    ->setPaper('a4')
    ->setOptions(['dpi' => 100]);

    return $pdf->stream();
    break;
  }
    }

    public function showRecisao($idTce)
    {
        $id = base64_decode($idTce);
        $tce = Tce::findOrFail($id);
        //pegando o ultimo aditivo de data para colocar sua data fim na recisão
        $getAditivo = $tce->aditivos->where('dtInicio', '<>', null)->last();
        //return view('tce.relatorio.recisao', compact('tce','getAditivo'));
        //dd($getAditivos->id);

        $pdf = PDF::loadView('tce.relatorio.recisao', compact('tce', 'getAditivo'))
  ->setPaper('a4')
  ->setOptions(['dpi' => 100]);

        return $pdf->stream('Recisão TCE '.$tce->id.' '.$tce->estudante->nmEstudante.'.pdf');
    }

    public function showRelatorioAtividades($idTce)
    {
        $dtHoje = Carbon::today()->format('d/m/Y');
        $id = base64_decode($idTce);
        $tce = Tce::findOrFail($id);
        //pegando o ultimo aditivo de data para colocar sua data fim na recisão
        $getAditivo = $tce->aditivos->where('dtInicio', '<>', null)->last();
        //return view('tce.relatorio.recisao', compact('tce','getAditivo'));
        //dd($getAditivos->id);

        $pdf = PDF::loadView('tce.relatorio.relatorioAtividadesSemestral', compact('dtHoje', 'tce', 'getAditivo'))
  ->setPaper('a4')
  ->setOptions(['dpi' => 100]);

        return $pdf->stream('Relatório de Atividades Semestral '.$tce->id.' '.$tce->estudante->nmEstudante.'.pdf');
    }

    //RELATORIOS
    public function relatorioTce()
    {
        $titulo = 'Relatório';

        return view('painel.concedente.relatoriotce', compact('titulo'))->with(['active' => 'relatorio']);
    }

    public function relatorioTce1PDF(Request $request)
    {
        /*
        if($request->input('concedente_id') == 'todos'){

        $Concedente = (object) $this->concedente->all();
        dd($Concedente);
} else {

}*/
        //Querys Principal
        $Concedente = ConcedenteModel::find(Auth::user()->concedente_id);
        //dd($Concedente);

        //Gets
        /*$Ordenacao = $request->input('ordenacao');
        
        if($request->input('modelo') == 'sim'){
        $paginaPDF = 'tce.relatorios.concedenteTcePDF';
        $orientacao = 'portrait';
        }else{
        $paginaPDF = 'tce.relatorios.concedenteTceOutrosPDF';
        $orientacao = 'landscape';
        }
        */
        $dtHoje = Carbon::today()->format('d/m/Y');

        return view('painel.concedente.relatoriotce1PDF', compact('Concedente', 'dtHoje'));
        /*
        $pdf = PDF::loadView('painel.concedente.relatoriotce1PDF', compact('Concedente','dtHoje'))
        ->setPaper('a4','portrait')
        ->setOptions(['dpi' => 100,'enable_php'=>true]);
        return $pdf->stream();
        */
    }

    public function relatorioTce2PDF(Request $request)
    {
        //Querys Principal
        $Concedente = ConcedenteModel::find(Auth::user()->concedente_id);
        $dtHoje = Carbon::today()->format('d/m/Y');
        //return view('painel.concedente.relatoriotce2PDF', compact('Concedente','dtHoje'));
        /*
        $pdf = PDF::loadView('painel.concedente.relatoriotce2PDF', compact('Concedente','dtHoje'))
        ->setPaper('a4','portrait')
        ->setOptions(['dpi' => 100,'enable_php'=>true]);
        return $pdf->stream();
        */
        return view('painel.concedente.relatoriotce2PDF', compact('Concedente', 'dtHoje'));
    }

    //RELATORIOS
}
