<?php

namespace App\Http\Controllers\Concedente;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\FolhaPagamentoItem;
use App\Models\FolhaPagamento;
use App\Models\UserConcedente;
use Carbon\Carbon;
use Auth;

class FolhaPagamentoController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:web_concedente');
    }

    public function show($hash) {

      $user = UserConcedente::find(Auth::id());

      $folhas = FolhaPagamento::where('hash', $hash);
      $folha = 0;
      if($user->sec_concedente_id != null) {
        $folha = $folhas->where('secConcedente_id', $user->sec_concedente_id)->first();
      } else if ($user->concedente_id != null){
        $folha = $folhas->where('concedente_id', $user->concedente_id)->first();
      }

      if($folha) {
        $items = FolhaPagamentoItem::where('folha_id', $folha->id)->get();
        return view('painel.concedente.faltas.show', compact('items', 'folha','nmEstudante','diasBaseEstagiados','faltas', 'justificadas'));
      }

      return redirect()->back();
    }

    public function index() {
      if(Auth::user()->sec_concedente_id != null){
        $folhas = FolhaPagamento::where('secConcedente_id', Auth::user()->sec_concedente_id)->orderByDesc('created_at')->take(2)->get();
      }else{
        $folhas = FolhaPagamento::where('concedente_id', Auth::user()->concedente_id)->orderByDesc('created_at')->take(2)->get();
      }
      return view('painel.concedente.folhas.index', compact('folhas'));
    }


    public function store(Request $request){

      $user = Auth::user();
      if($user->sec_concedente_id != null){
        $hash =  base64_encode($request['referencia'].$request['referencia'].'secretaria'.$user->sec_concedente_id);
      }else{
        $hash =  base64_encode($request['referencia'].$request['referencia'].'concedente'.$user->concedente_id);
        $user->sec_concedente_id = null;
      }
      $teste = FolhaPagamento::where('concedente_id', $user->concedente_id)
      ->where('hash', $hash)
      ->get();
      //return dd(explode('/', $request['referencia']));
      try{
        if($teste->count() == 0){
          $folha = FolhaPagamento::create([
            'concedente_id' => $user->concedente_id,
            'referenciaMes' => explode('/', $request['referencia'])[0],
            'referenciaAno' => explode('/', $request['referencia'])[1],
            'hash' => $hash,
            'secConcedente_id' => $user->sec_concedente_id,
            'user_id' => Auth::id(),
            'obs' => 'Cadastrada pela concedente/secretaria'
          ]);
          $iFolha = FolhaPagamentoItem::where('folha_id',$folha->id)->pluck('tce_id')->toArray();
          FolhaPagamentoItem::alimentaFolha($user->concedente_id, $user->sec_concedente_id, $folha, $iFolha, 30, 0);
          return redirect()
          ->route('painel.concedente.folha.show',['hash' => $folha->hash])
          ->with('success', 'Folha cadastrada com sucesso');
        }else{
          return redirect()
          ->back()
          ->with('error', 'Falha ao cadastrar folha');
        }
      }catch(\Exception $e){
        return redirect()->back()->with('error', 'Falha ao cadastrar folha');
      }
    }

    public function updateItem(Request $request, $hash){

      $folha = FolhaPagamento::where('hash',$hash)->get();
      if($folha->count() > 0){
        $folha = $folha->first();
        if($folha->concedente_id === Auth::user()->concedente_id){
          $item = FolhaPagamentoItem::find($request['item']['id']);
          if($item->folha_id == $folha->id){
            $item->diasBaseEstagiados = $request['item']['dias'];
            $item->faltas = $request['item']['faltas'];
            $item->justificadas = $request['item']['justificadas'];
            $item->diasRecesso = $request['item']['recesso'];
            $item->save();

            return response()->json(['status'=>'success']);
          }
        }
      }
      return response()->json(['status'=>'fail']);
    }

    public function fechar($hash) {

      $user = Auth::user();
      $folha = FolhaPagamento::where('hash', $hash)->first();

      if($folha) {
        if ($user->sec_concedente_id != null) {
          if($user->sec_concedente_id === $folha->secConcedente_id) {
            $folha->fechado = 'S';
            $folha->fechado_at = \Carbon\Carbon::now();
            $folha->fechado_user = $user->id;
            $folha->save();

            return redirect()->back()->with(['success' => 'Folha fechada com sucesso!']);
          } else {
            dd($user->sec_concedente_id);
            return redirect()->back()->with(['error' => 'Permissão negada!']);
          }
        }

        if ($user->concedente_id != null) {
          if ($user->concedente_id === $folha->concedente_id) {
            $folha->fechado = 'S';
            $folha->fechado_at = \Carbon\Carbon::now();
            $folha->fechado_user = $user->id;
            $folha->save();

            return redirect()->back()->with(['success' => 'Folha fechada com sucesso!']);
          } else {
            return redirect()->back()->with(['error' => 'Permissão negada!']);
          }
        }
      }

      return redirect()->back()->with(['error'=>'Folha não encontrada.']);
    }

}
