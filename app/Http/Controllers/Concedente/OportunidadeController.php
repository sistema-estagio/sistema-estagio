<?php

namespace App\Http\Controllers\Concedente;

use App\Models\Oportunidade;
use App\Models\OportunidadeRequisito;
use App\Models\OportunidadeEstudante;
use App\Models\TarefaOportunidade;
use App\Models\Requisito;
use App\Models\Instituicao;
use App\Models\CursoInstituicao;
use App\Models\Tarefa;
use App\Models\Concedente;
use App\Models\Curso;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Validator;
use DB;

class OportunidadeController extends Controller
{
  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {
    $this->middleware('auth:web_concedente');
  }
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    //
    $user = Auth::user();

    if($user->sec_concedente_id != null){
      $oportunidades = Oportunidade::where('secretaria_id', $user->sec_concedente_id)->get();
    }else{
      $oportunidades = Oportunidade::where('concedente_id', $user->concedente_id)->get();
    }

    return view('painel.concedente.oportunidade.index', compact('oportunidades'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    $user = Auth::user();
    $instituicoes = Instituicao::where('estado_id',$user->concedente->estado_id)->get();

    $cursos = new Collection;

    foreach ($instituicoes as $instituicao) {
      $cus = CursoInstituicao::where('instituicao_id', $instituicao->id)->get();
      foreach($cus as $curso){
        if($cursos->where('id', $curso->curso_id)->count() == 0){
          $cursos->push(Curso::find($curso->curso_id));
        }
      }
    }

    $tarefas = Tarefa::where('concedente_id', $user->concedente_id)->get();
    $requisitos = Requisito::where('concedente_id', $user->concedente_id)->get();

    if($user->sec_concedente_id != null){
      $requisitos = $requisitos->where('secretaria_id',$user->sec_concedente_id);
      $tarefas = $tarefas->where('secretaria_id', $user->sec_concedente_id);
    }

    return view('painel.concedente.oportunidade.criar', compact('requisitos', 'cursos', 'tarefas'));
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    $user = Auth::user();
    $concedente = Concedente::find($user->concedente_id);

    $validar = Validator::make($request->all(), [
      'titulo' => 'required',
      'vagas' => 'required',
      'cargaHorariaSemanal' => 'required',
    ],[
      'cargaHorariaSemanal.required' => ' O campo Carga Horaria é Obrigatório.',
    ]);
    if(!$validar->fails()){
      try{
        $oportunidade = Oportunidade::create([
          'titulo'=>$request['titulo'],
          'code'=>Oportunidade::hasCode(),
          'vagas'=>$request['vagas'],
          'cargaHorariaSemanal'=>$request['cargaHorariaSemanal'],
          'status'=>$request['status'],
          'obs'=>$request['obs'],
          'local'=>$request['local'],
          'horarioInicio'=>$request['horarioInicio'],
          'horarioFim'=>$request['horarioFim'],
          'bolsa'=>$request['bolsa'],
          'showBolsa'=>$request['showBolsa'],
          'auxTransp'=>$request['auxTransp'],
          'showTransp'=>$request['showTransp'],
          'auxAlimentacao'=>$request['auxAlimentacao'],
          'showAlimentacao'=>$request['showAlimentacao'],
          'curso_id'=>$request['curso_id'],
          'user_id'=>$user->id,
          'concedente_id'=>$user->concedente_id,
          'secretaria_id'=>$user->secretaria_id,
          'cidade_id'=>$concedente->cidade_id,
        ]);

          $e = $request->educacional;
          $s = $request->semestre;
          for ($i=0; $i < count($e); $i++) {
            $formacao = Requisito::find($e[$i]);
            if($formacao){
              if($formacao->concedente_id == $user->concedente_id){
                $oportunidade->requisitos()->create([
                  'tipo' => $formacao->tipo,
                  'semestre' => $s[$i],
                  'oportunidade_id' => $oportunidade->id,
                  'requisito_id' =>  $formacao->id,
                ]);
              }
            }
          }

          $r = $request->requisitos;
          for ($i=0; $i < count($r); $i++) {
            $requisito = Requisito::find($r[$i]);
            if($requisito){
              if($requisito->concedente_id == $user->concedente_id){
                $oportunidade->requisitos()->create([
                  'tipo' => $requisito->tipo,
                  'oportunidade_id' => $oportunidade->id,
                  'requisito_id' =>  $requisito->id,
                ]);
              }
            }
          }

          $a = $request->atividades;
          for ($i=0; $i < count($a); $i++) {
            $tarefa = Tarefa::find($a[$i]);
            if($tarefa){
              if($tarefa->concedente_id == $user->concedente_id){
                $oportunidade->tarefas()->create([
                  'tarefa_id' =>  $tarefa->id,
                  'oportunidade_id' => $oportunidade->id,
                ]);
              }
            }
          }

        $request->session()->flash('success', 'Vaga aberta com sucesso');
        return redirect()->route('painel.concedente.oportunidade.index');
      }catch(\Exception $e){
        $request->session()->flash('fail', 'Não foi possível abrir a vaga');
        return dd($e);//redirect()->back()->withInput($request->all());
      }
    }
    $request->session()->flash('fail', 'Não foi possível abrir a vaga');
    return redirect()->back()->withInput($request->all());
  }

  /**
  * Display the specified resource.
  *
  * @param  \App\Oportunidade  $oportunidade
  * @return \Illuminate\Http\Response
  */
  public function show($hash)
  {
    //
    return view('painel.concedente.oportunidade.show');
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  \App\Oportunidade  $oportunidade
  * @return \Illuminate\Http\Response
  */
  public function edit(Request $request, Oportunidade $oportunidade)
  {
    $user = Auth::user();

    $oportunidade = Oportunidade::where('code', '=', $request->code)->first();
    $instituicoes = Instituicao::where('estado_id',$user->concedente->estado_id)->get();

    $cursos = new Collection;

    foreach ($instituicoes as $instituicao) {
      $cus = CursoInstituicao::where('instituicao_id', $instituicao->id)->get();
      foreach($cus as $curso){
        if($cursos->where('id', $curso->curso_id)->count() == 0){
          $cursos->push(Curso::find($curso->curso_id));
        }
      }
    }
    $tars = Tarefa::where('concedente_id', $user->concedente_id)->get();
    $reqs = Requisito::where('concedente_id', $user->concedente_id)->get();
    //
    $oportunidadeRequisito = OportunidadeRequisito::where('oportunidade_id', $oportunidade->id)->get();
    $oportunidadeTarefa = TarefaOportunidade::where('oportunidade_id', $oportunidade->id)->get();

    $requisitos = new Collection;
    $formacoes = new Collection;
    $tarefas = new Collection;

    foreach($reqs->where('tipo', 'outro') as $requisito){
      if($oportunidadeRequisito->where('requisito_id', $requisito->id)->count() == 0){
        $requisitos->push($requisito);
      }
    }

    foreach($reqs->where('tipo', 'educacional') as $requisito){
      if($oportunidadeRequisito->where('requisito_id', $requisito->id)->count() == 0){
        $formacoes->push($requisito);
      }
    }

    foreach($tars->where('tipo', 'outro') as $tarefa){
      if($oportunidadeTarefa->where('requisito_id', $tarefa->id)->count() == 0){
        $tarefas->push($tarefa);
      }
    }


    // if($user->sec_concedente_id != null){
    //   $requisitos = $requisitos->where('secretaria_id',$user->sec_concedente_id);
    //   $tarefas = $tarefas->where('secretaria_id', $user->sec_concedente_id);
    // }

    return view('painel.concedente.oportunidade.editar', compact('oportunidade', 'requisitos', 'cursos', 'tarefas', 'oportunidadeRequisito', 'oportunidadeTarefa'));
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  \App\Oportunidade  $oportunidade
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, Oportunidade $oportunidade)
  {
    $user = Auth::user();
    $concedente = Concedente::find($user->concedente_id);

    $validar = Validator::make($request->all(), [
      'titulo' => 'required',
      'vagas' => 'required',
      'cargaHorariaSemanal' => 'required',
    ],[
      'diasBaseEstagiados.required' => ' O campo Dias base Estágio é Obrigatório.',
    ]);

    if(!$validar->fails()){
      $oportunidade = Oportunidade::where('code', $request->code)->get()->first();
      $oportunidade->update([
        'titulo' => $request['titulo'],
        'code' => Oportunidade::generateRandomStr(),
        'vagas' => $request['vagas'],
        'cargaHorariaSemanal' => $request['cargaHorariaSemanal'],
        'status' => $request['status'],
        'obs' => $request['obs'],
        'local' => $request['local'],
        'horarioInicio' => $request['horarioInicio'],
        'horarioFim' => $request['horarioFim'],
        'bolsa' => $request['bolsa'],
        'auxTransp' => $request['auxTransp'],
        'auxAlimentacao' => $request['auxAlimentacao'],
        'curso_id' => $request['curso_id'],
        'user_id' => $user->id,
        'concedente_id' => $user->concedente_id,
        'secretaria_id' => $user->secretaria_id,
        'cidade_id' => $concedente->cidade_id,
      ]);

      $oportunidade_requisito = OportunidadeRequisito::where('oportunidade_id', $oportunidade->id)->get();
      $oportunidade_tarefa = TarefaOportunidade::where('oportunidade_id', $oportunidade->id)->get();

      if($user->sec_concedente_id != null){

        for ($i=0; $i < count($request->requisitos); $i++) {

          if($oportunidade_requisito) {
            for ($j=0; $j < count($oportunidade_requisito) ; $j++) {
              $oportunidade_requisito[$j]->delete();
            }
          }

          $r = $request->requisitos;
          $requisito = Requisito::find($r[$i]);
          if($requisito){
            OportunidadeRequisito::create([
              'oportunidade_id' => $oportunidade->id,
              'requisito_id' =>  $requisito->id,
            ]);
          }

        }

        for ($i=0; $i < count($request->atividades); $i++) {

          if($oportunidade_tarefa) {
            for ($k=0; $k < count($oportunidade_tarefa) ; $k++) {
              $oportunidade_tarefa[$k]->delete();
            }
          }

          $r = $request->atividades;
          $tarefa = Tarefa::find($r[$i]);
          if($tarefa){
            TarefaOportunidade::create([
              'oportunidade_id' => $oportunidade->id,
              'tarefa_id' =>  $tarefa->id,
            ]);
          }
        }
      } else {
        for ($i=0; $i < count($request->requisitos); $i++) {

          if($oportunidade_requisito) {
            for ($j=0; $j < count($oportunidade_requisito) ; $j++) {
              $oportunidade_requisito[$j]->delete();
            }
          }

          $r = $request->requisitos;
          $requisito = Requisito::find($r[$i]);
          if($requisito){
            OportunidadeRequisito::create([
              'oportunidade_id' => $oportunidade->id,
              'requisito_id' =>  $requisito->id,
            ]);
          }

        }

        for ($i=0; $i < count($request->atividades); $i++) {

          if($oportunidade_tarefa) {
            for ($k=0; $k < count($oportunidade_tarefa) ; $k++) {
              $oportunidade_tarefa[$k]->delete();
            }
          }

          $r = $request->atividades;
          $tarefa = Tarefa::find($r[$i]);
          if($tarefa){
            TarefaOportunidade::create([
              'oportunidade_id' => $oportunidade->id,
              'tarefa_id' =>  $tarefa->id,
            ]);
          }
        }
      }
    }

    return redirect()->route('painel.concedente.oportunidade.index')->with('oportunidade');
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  \App\Oportunidade  $oportunidade
  * @return \Illuminate\Http\Response
  */
  public function destroy(Request $request, $id)
  {
    $oportunidade = Oportunidade::where('code',$id)->first(); //destroy($id);
    if($oportunidade){
      if($oportunidade->secretaria_id != null){
        if($oportunidade->secretaria_id != Auth::user()->secretaria_id){
          $request->session()->flash('error', 'Não foi possível excluir a vaga');
          return redirect()->back();
        }
      }else{
        if($oportunidade->concedente_id != Auth::user()->concedente_id){
          $request->session()->flash('error', 'Não foi possível excluir a vaga');
          return redirect()->back();
        }
      }
      $oportunidade->delete();

      if($oportunidade->candidatos()->count() > 0){
        $request->session()->flash('success', 'Vaga possuia candidatos, mas foi excluída com sucesso');
      }else{
        $request->session()->flash('success', 'Vaga excluída com sucesso');
      }
      return redirect()->back();
    }
    $request->session()->flash('error', 'Vaga não encontrada!');
    return redirect()->back();
  }
}
