<?php

namespace App\Http\Controllers\Concedente;

use App\Models\UserConcedente;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Validator;
use Auth;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:web_concedente');
    }

    public function changePassword(Request $request){
      $validate = Validator::make($request->all(), [
        'newpassword' => 'required|min:8|max:20',
        'password' => 'required',
      ]);
      if($validate->fails()){
        return response()->json($validate->fails());
      }

      if(Hash::check($request['password'], Auth::user()->password) == true){
        $user = UserConcedente::find(Auth::id());
        $user->password = bcrypt($request['newpassword']);
        $user->save();
        return response()->json(['status' => 'success', 'msg' => 'Senha alterada com sucesso']);
      }else{
        return response()->json(['status' => 'error', 'msg' => 'Senha atual informada não coincide com a da nossa base']);
      }

      return response()->json(['status' => 'error', 'msg' => 'Erro ao alterar a senha']);
    }
}
