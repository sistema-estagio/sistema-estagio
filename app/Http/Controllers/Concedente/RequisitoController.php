<?php

namespace App\Http\Controllers\Concedente;

use App\Models\Requisito;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class RequisitoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:web_concedente');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $requisitos = Requisito::where('concedente_id',$user->concedente_id)->get();
        if($user->secretaria_id == null){
          $user->secretaria_id = null;
        }

        $requisitos = $requisitos->where('titulo',$request['titulo']);
        if($requisitos->count() == 0){
          $requisito = Requisito::create([
            'tipo' => $request['tipo'],
            'titulo' => $request['titulo'],
            'descricao' => $request['descricao'],
            'concedente_id' => $user->concedente_id,
            'secretaria_id' => $user->secretaria_id,
          ]);
          return response()->json(['status'=>'success', 'requisito'=>$requisito]);
        }else{
          return response()->json(['status'=>'fail', 'msg'=>'Já existe um requisito com o título informado']);
        }

        return response()->json(null);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Requisitos  $requisitos
     * @return \Illuminate\Http\Response
     */
    public function show(Requisitos $requisitos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Requisitos  $requisitos
     * @return \Illuminate\Http\Response
     */
    public function edit(Requisitos $requisitos)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Requisitos  $requisitos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Requisitos $requisitos)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Requisitos  $requisitos
     * @return \Illuminate\Http\Response
     */
    public function destroy(Requisitos $requisitos)
    {
        //
    }
}
