<?php

namespace App\Http\Controllers\Concedente;

use App\Models\Tarefa;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class AtividadeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:web_concedente');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $tarefas = Tarefa::where('concedente_id',$user->concedente_id)->get();
        if($user->secretaria_id == null){
          $user->secretaria_id = null;
        }

        $tarefas = $tarefas->where('descricao',$request['descricao']);
        if($tarefas->count() == 0){
          $tarefa = Tarefa::create([
            'descricao'=> $request['descricao'],
            'concedente_id'=> $user->concedente_id,
            'secretaria_id'=> $user->secretaria_id,
          ]);
          return response()->json(['status'=>'success', 'tarefa'=>$tarefa]);
        }else{
          return response()->json(['status'=>'fail', 'msg'=>'Já existe um requisito com o título informado']);
        }

        return response()->json(null);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tarefa  $tarefa
     * @return \Illuminate\Http\Response
     */
    public function show(Tarefa $tarefa)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tarefa  $tarefa
     * @return \Illuminate\Http\Response
     */
    public function edit(Tarefa $tarefa)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tarefa  $tarefa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tarefa $tarefa)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tarefa  $tarefa
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tarefa $tarefa)
    {
        //
    }
}
