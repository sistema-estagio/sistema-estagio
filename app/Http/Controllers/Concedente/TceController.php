<?php

namespace App\Http\Controllers\Concedente;

use App\Models\Tce;
use Illuminate\Http\Request;
use App\Models\Estudante;
use App\Models\Concedente;
use App\Models\Relatorio;
use App\Models\Periodo;
use App\Models\Supervisor;
use App\Models\Atividade;
use App\Models\Aditivo;
use App\Models\MotivoCancelamento;
use App\Models\SecConcedente;
use Carbon\Carbon;
use PDF;
use Auth;
use DB;
use Validator;

class TceController extends Controller
{

  private $tce;
  private $concedente;
  private $estudante;
  private $motivoCancelamento;

  public function __construct(Tce $tce, Concedente $concedente, Estudante $estudante, MotivoCancelamento $motivoCancelamento)
  {
    $this->tce = $tce;
    $this->concedente = $concedente;
    $this->estudante = $estudante;
    $this->motivoCancelamento = $motivoCancelamento;
  }

  public function showRelatorio($idTce, $idRelatorio)
  {
    $idTce = base64_decode($idTce);
    $tce = $this->tce->findOrFail($idTce);
    $relatorio = Relatorio::findOrFail($idRelatorio);
    switch ($relatorio->id) {
      case $relatorio->id:
      //dd($relatorio->nmRelatorio);
      //return view('tce.relatorio.' . $relatorio->dsUrl. 'Div', compact('tce'));

      // $pdf = app('dompdf.wrapper');
      //$pdf->getDomPDF()->set_option("enable_php", true);
      //$pdf->loadView('your.view.here', $data);

      if ($relatorio->dsUrl == 'mtpac') {
        return view('painel.concedente.modelos.'.$relatorio->dsUrl, compact('tce'));
      } else {
        $pdf = PDF::loadView('painel.concedente.modelos.'.$relatorio->dsUrl, compact('tce'))
        ->setPaper('a4')
        ->setOptions(['dpi' => 100, 'enable_php' => true]);

        return $pdf->stream();
      }

      //return view('tce.relatorio.' . $relatorio->dsUrl. 'Div', compact('tce'));
      break;
    }
  }

}
