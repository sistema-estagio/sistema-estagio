<?php

namespace App\Http\Controllers\Concedente;

use App\Models\UserConcedente;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use Validator;

class MediadorController extends Controller
{
  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {
    $this->middleware('auth:web_concedente');
  }
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    //
    $user = UserConcedente::find(Auth::id());
    if($user){
      $mediadors = UserConcedente::where('concedente_id',$user->concedente_id)->where('tipo', 'm')->where('deleted_at',null)->get();
      if($user->sec_concedente_id != null){
        $mediadors = $mediadors->where('sec_concedente_id', $user->sec_concedente_id);
      }
      return view('painel.concedente.mediador.index', compact('mediadors'));
    }
    return redirect()->back();
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    DB::beginTransaction();
    try {
      $user = UserConcedente::find(Auth::id());
      //Encriptando a Senha
      $request['password'] = bcrypt($request['password']);
      //Pega usuario logado para gravar quem cadastrou
      $request['usuario_id'] = Auth::id();
      $request['tipo'] = 'm';
      $request['concedente_id'] = $user->concedente_id;
      $request['sec_concedente_id'] = null;
      if($user->sec_concedente_id != null){
        $request['sec_concedente_id'] = $user->sec_concedente_id;
      }
      //dd($Campos);
      //Adicionando o Usuario ao bd
      $validar = Validator::make($request->all(), [
        'name' => 'required',
        'password' => 'required',
        'email' => 'required|unique:users_concedente',
      ],[
        'name.required' => ' O campo NOME é obrigatório.',
        'senha.required' => ' O campo SENHA é obrigatório.',
        'password.required' => ' O campo EMAIL é obrigatório.',
        'email.unique' => ' Já existe um usuario usando o email informado.',
      ]);

      if($validar->fails()) {
        return response()->json($validar);
      }

      $user = UserConcedente::create([
        'tipo' => $request['tipo'],
        'name' => $request['name'],
        'email' => $request['email'],
        'password' => $request['password'],
        'usuario_id' => Auth::id(),
        'concedente_id' => $request['concedente_id'],
        'sec_concedente_id' => $request['sec_concedente_id'],
      ]);

    } catch (Exception $e) {
      DB::rollback();
      return response()->json($e->getMessage());
    }
    DB::commit();

    if($user){
      return response()->json(['status' => 'success', 'msg' => 'Mediador cadastrado com sucesso!', 'user'=> $user, 'created_at'=> $user->created_at->format('d/m/Y')]);
    }else{
      return response()->json(['status' => 'error', 'msg' => 'Falha ao cadastrar mediador!']);
    }
    return null;
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  \App\UserConcedente  $userConcedente
  * @return \Illuminate\Http\Response
  */
  public function destroy(Request $request)
  {
    $mediador = UserConcedente::find($request['id']);
    $user = UserConcedente::find(Auth::id());
    if($mediador){
      if($mediador->tipo == 'm'){
        if($mediador->concedente_id === $user->concedente_id){
          if($user->sec_concedente_id != null){
            if($mediador->sec_concedente_id != $user->sec_concedente_id){
              return response()->json(['status'=>'error', 'msg'=> 'Não é possível excluir o mediador']);
            }
          }
          $mediador->deleted_at = \Carbon\Carbon::now();
          $mediador->save();
          return response()->json(['status'=>'success', 'msg'=> 'Mediador excluído com sucesso']);
        }
      }
    }
    return response()->json(['status'=>'error', 'msg'=> 'Não é possível excluir o mediador']);
  }
}
