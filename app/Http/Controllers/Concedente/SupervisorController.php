<?php

namespace App\Http\Controllers\Concedente;

use App\Models\Supervisor;
use App\Models\UserConcedente;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;

class SupervisorController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('auth:web_concedente');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = UserConcedente::find(Auth::id());
        if ($user) {
            $supervisors = Supervisor::where('concedente_id', $user->concedente_id)->get();
            if ($user->sec_concedente_id != null) {
                $supervisors = $supervisors->where('secConcedente_id', $user->sec_concedente_id);
            }
        }

        return view('painel.concedente.supervisor.index', compact('supervisors'));
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Supervisor $supervisor
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $supervisor = Supervisor::find($id);
        $user = UserConcedente::find(Auth::id());
        if ($supervisor) {
            if ($user->sec_concedente_id != null) {
                if ($user->secConcedente_id != $supervisor->sec_concedente_id) {
                    return redirect()->back();
                }
            }

            if ($user->concedente_id != $supervisor->concedente_id) {
                return redirect()->back();
            }

            return view('painel.concedente.supervisor.show', compact('supervisor'));
        }

        return redirect()->back();
    }

    public function store(Request $request)
    {
        $user = UserConcedente::find(Auth::id());
        $supervisor = Supervisor::where('cpf', $request->cpf)->get();
        $supervisor = $supervisor->where('concedente_id', $user->concedente_id);

        if ($supervisor->count() == 0) {
            Supervisor::create([
                'nome' => $request->nome,
                'cpf' => $request->cpf,
                'email' => $request->email,
                'telefone' => $request->telefone,
                'cargo' => $request->cargo,
                'concedente_id' => $user->concedente_id,
                'secConcedente_id' => $user->secConcedente_id,
            ]);

            return redirect()->route('painel.concedente.supervisor.index');
        } else {
            if ($user->secConcedente_id != null) {
                $supervisor = $supervisor->where('secConcedente_id', $user->sec_concedente_id);

                if ($supervisor->count() == 0) {
                    Supervisor::create([
                        'nome' => $request->nome,
                        'cpf' => $request->cpf,
                        'email' => $request->email,
                        'telefone' => $request->telefone,
                        'cargo' => $request->cargo,
                        'concedente_id' => $user->concedente_id,
                        'secConcedente_id' => $user->secConcedente_id,
                    ]);
                } else {
                    return redirect()->back();
                }
            } else {
                Supervisor::create([
                    'nome' => $request->nome,
                    'cpf' => $request->cpf,
                    'email' => $request->email,
                    'telefone' => $request->telefone,
                    'cargo' => $request->cargo,
                    'concedente_id' => $user->concedente_id,
                    'secConcedente_id' => $user->secConcedente_id,
                ]);
            }
        }

        // DB::beginTransaction();
        // try {
        //     $user = UserConcedente::find(Auth::id());
        //     $request['usuario_id'] = Auth::id();
        //     $request['concedente_id'] = $user->concedente_id;
        //     $request['sec_concedente_id'] = null;
        //     if ($user->sec_concedente_id != null) {
        //         $request['sec_concedente_id'] = $user->sec_concedente_id;
        //     }

        //     dd(Supervisor::find($user->concedente_id));

        //     //dd($Campos);
        //     //Adicionando o Usuario ao bd
        //     $validar = Validator::make($request->all(), [
        //         'name' => 'required',
        //         'email' => 'required|unique:users_concedente',
        //         'cpf' => 'required',
        //         'telefone' => 'required',
        //         'formacao' => 'required',
        //         'cargo' => 'required',
        //     ], [
        //         'name.required' => ' O campo NOME é obrigatório.',
        //         'email.unique' => ' Já existe um usuario usando o email informado.',
        //         'cpf.required' => ' O campo CPF é obrigatório.',
        //         'telefone.required' => ' O campo TELEFONE é obrigatório.',
        //         'formacao.required' => ' O campo FORMAÇÃO é obrigatório.',
        //         'cargo.required' => ' O campo CARGO é obrigatório.',
        //         ]);

        //     if ($validar->fails()) {
        //         return response()->json($validar);
        //     }
        // } catch (Exception $e) {
        //     DB::rollback();

        //     return response()->json($e->getMessage());
        // }
        // DB::commit();

        // if ($user) {
        //     return response()->json(['status' => 'success', 'msg' => 'Supervisor cadastrado com sucesso!', 'user' => $user, 'created_at' => $user->created_at->format('d/m/Y')]);
        // } else {
        //     return response()->json(['status' => 'error', 'msg' => 'Falha ao cadastrar supervisor!']);
        // }

        // return null;
    }
}
