<?php

namespace App\Http\Controllers\Concedente;

use App\Models\Tce;
use App\Models\Concedente;
use Illuminate\Support\Collection;
use App\Models\Estudante;
use App\Models\SecConcedente;
use App\Models\UserConcedente;
use App\Models\Supervisor;
use App\Models\Aditivo;
use App\Http\Controllers\Controller;
use PDF;
use Auth;

class RelatorioController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('auth:web_concedente');
    }

    public function index()
    {
        $titulo = 'Relatórios';
        $tces = Tce::where('concedente_id', Auth::user()->concedente_id)->get();
        $estudantes = new Collection();
        $supervisores = new Collection();
        $mediadores = UserConcedente::where('tipo', 'm')->where('concedente_id', Auth::user()->concedente_id)->get();

        if (Auth::user()->sec_concedente_id != null) {
            $tces = $tces->where('sec_conc_id', Auth::user()->sec_concedente_id);
            $mediadores = $mediadores->where('sec_conc_id', Auth::user()->sec_concedente_id);
        }

        foreach ($tces as $tce) {
            $estudantes->push(Estudante::find($tce->estudante_id));
            $supervisores->push(Supervisor::find($tce->supervisor_id));
        }

        return view('painel.concedente.relatorios.index', compact('tces', 'titulo', 'estudantes', 'supervisores', 'mediadores'));
    }

    public function pendentes($tipo)
    {
        $concedente = Concedente::find(Auth::user()->concedente_id);
        $tces = Tce::where('concedente_id', $concedente->id)->where('dt4Via', null)->get();
        $secretaria = SecConcedente::find(Auth::user()->sec_concedente_id);
        if ($secretaria != null) {
            $tces = $tces->where('sec_conc_id', Auth::user()->sec_concedente_id)->sortBy('id');
        }
        //Ordenação por nome
        $tces = $tces->sortBy(function ($tce) {
            //Definir qual coluna vai ser usada na ordenação
            return $tce->estudante->nmEstudante;
        });

        $tces = $tces->where('dtCancelamento', '==', null);
        $aditivo = new Aditivo();

        foreach ($tces as $tce) {
            $tce->id = $tce->id;
            $aditivo = $tce->aditivos()->latest()->first();
            if ($tce->aditivos->count() > 0) {
                if ($aditivo->nnSemestreAno != null) {
                    $tce->nnSemestreAno = $aditivo->nnSemestreAno;
                }
                if ($aditivo->periodo_id != null) {
                    $tce->periodo->nmPeriodo = $aditivo->periodo->nmPeriodo;
                }
                if ($aditivo->vlAuxilioMensal != null) {
                    $tce->vlAuxilioMensal = $aditivo->vlAuxilioMensal;
                }
                if ($aditivo->vlAuxilioTransporte != null) {
                    $tce->vlAuxilioTransporte = $aditivo->vlAuxilioTransporte;
                }
                if ($aditivo->vlAuxilioAlimentacao != null) {
                    $tce->vlAuxilioAlimentacao = $aditivo->vlAuxilioAlimentacao;
                }
                if ($aditivo->dtInicio != null) {
                    $tce->dtFim = date('d/m/Y', strtotime($aditivo->dtFim));
                }
            }
        }

        $pdf = PDF::loadView('painel.concedente.relatorios.tce.pendentes', compact('concedente', 'tces', 'dtHoje', 'secretaria', 'tipo'))
    ->setPaper('a4')
    ->setOptions(['dpi' => 100, 'enable_php' => true]);

        return $pdf->stream();
    }

    public function ativos($tipo)
    {
        $concedente = Concedente::find(Auth::user()->concedente_id);
        $tces = Tce::where('concedente_id', $concedente->id)->where('dt4Via', '!=', null)->get();
        $secretaria = SecConcedente::find(Auth::user()->sec_concedente_id);
        if ($secretaria != null) {
            $tces = $tces->where('sec_conc_id', Auth::user()->sec_concedente_id)->sortBy('id');
        }
        //Ordenação por nome
        $tces = $tces->sortBy(function ($tce) {
            //Definir qual coluna vai ser usada na ordenação
            return $tce->estudante->nmEstudante;
        });

        $tces = $tces->where('dtCancelamento', '==', null);
        $aditivo = new Aditivo();

        foreach ($tces as $tce) {
            $tce->id = $tce->id;
            $aditivo = $tce->aditivos()->latest()->first();
            if ($tce->aditivos->count() > 0) {
                if ($aditivo->nnSemestreAno != null) {
                    $tce->nnSemestreAno = $aditivo->nnSemestreAno;
                }
                if ($aditivo->periodo_id != null) {
                    $tce->periodo->nmPeriodo = $aditivo->periodo->nmPeriodo;
                }
                if ($aditivo->vlAuxilioMensal != null) {
                    $tce->vlAuxilioMensal = $aditivo->vlAuxilioMensal;
                }
                if ($aditivo->vlAuxilioTransporte != null) {
                    $tce->vlAuxilioTransporte = $aditivo->vlAuxilioTransporte;
                }
                if ($aditivo->vlAuxilioAlimentacao != null) {
                    $tce->vlAuxilioAlimentacao = $aditivo->vlAuxilioAlimentacao;
                }
                if ($aditivo->dtInicio != null) {
                    $tce->dtFim = date('d/m/Y', strtotime($aditivo->dtFim));
                }
            }
        }

        $pdf = PDF::loadView('painel.concedente.relatorios.tce.ativos', compact('concedente', 'tces', 'dtHoje', 'secretaria', 'tipo'))
    ->setPaper('a4', 'landscape')
    ->setOptions(['dpi' => 100, 'enable_php' => true]);

        return $pdf->stream();
    }

    public function cancelados($tipo)
    {
        $concedente = Concedente::find(Auth::user()->concedente_id);
        $tces = Tce::where('concedente_id', $concedente->id)->get();
        $secretaria = SecConcedente::find(Auth::user()->sec_concedente_id);
        if ($secretaria != null) {
            $tces = $tces->where('sec_conc_id', Auth::user()->sec_concedente_id)->sortBy('id');
        }
        //Ordenação por nome
        $tces = $tces->sortBy(function ($tce) {
            //Definir qual coluna vai ser usada na ordenação
            return $tce->estudante->nmEstudante;
        });

        $tces = $tces->where('dtCancelamento', '!=', null);
        $aditivo = new Aditivo();

        foreach ($tces as $tce) {
            $tce->id = $tce->id;
            $aditivo = $tce->aditivos()->latest()->first();
            if ($tce->aditivos->count() > 0) {
                if ($aditivo->nnSemestreAno != null) {
                    $tce->nnSemestreAno = $aditivo->nnSemestreAno;
                }
                if ($aditivo->periodo_id != null) {
                    $tce->periodo->nmPeriodo = $aditivo->periodo->nmPeriodo;
                }
                if ($aditivo->vlAuxilioMensal != null) {
                    $tce->vlAuxilioMensal = $aditivo->vlAuxilioMensal;
                }
                if ($aditivo->vlAuxilioTransporte != null) {
                    $tce->vlAuxilioTransporte = $aditivo->vlAuxilioTransporte;
                }
                if ($aditivo->vlAuxilioAlimentacao != null) {
                    $tce->vlAuxilioAlimentacao = $aditivo->vlAuxilioAlimentacao;
                }
                if ($aditivo->dtInicio != null) {
                    $tce->dtFim = date('d/m/Y', strtotime($aditivo->dtFim));
                }
            }
        }

        $pdf = PDF::loadView('painel.concedente.relatorios.tce.cancelados', compact('concedente', 'tces', 'dtHoje', 'secretaria', 'tipo'))
    ->setPaper('a4', 'landscape')
    ->setOptions(['dpi' => 100, 'enable_php' => true]);

        return $pdf->stream();
    }

    public function supervisores($tipo)
    {
        $concedente = Concedente::find(Auth::user()->concedente_id);
        $supervisores = Supervisor::where('concedente_id', $concedente->id)->get();
        $secretaria = SecConcedente::find(Auth::user()->sec_concedente_id);
        $supervTces = new Collection();

        if ($secretaria != null) {
            $supervisores = $supervisores->where('secConcedente_id', Auth::user()->sec_concedente_id)->sortBy('id');
        }

        $pdf = PDF::loadView('painel.concedente.relatorios.supervisores.PDF', compact('supervTces', 'concedente', 'supervisores', 'dtHoje', 'secretaria', 'tipo'))
            ->setPaper('a4', 'portrait')
            ->setOptions(['dpi' => 100, 'enable_php' => true]);

        return $pdf->stream();
    }

    public function mediadores($tipo)
    {
        return null;
    }

    public function estudantes($tipo)
    {
        $concedente = Concedente::find(Auth::user()->concedente_id);
        $secretaria = SecConcedente::find(Auth::user()->sec_concedente_id);
        $tces = Tce::where('concedente_id', $concedente->id)->get();

        if ($secretaria != null) {
            $tces = $tces->where('sec_conc_id', $secretaria->id);
        }

        $estudantes = new Collection();

        foreach ($tces as $tce) {
            if ($estudantes->exists()) {
                $estudantes->push(Estudante::where('tce_id', $tce->id));
            }
        }

        $pdf = PDF::loadView('painel.concedente.relatorios.estudantes.PDF', compact('concedente', 'tces', 'estudantes', 'secretaria', 'tipo'))
            ->setPaper('a4', 'portrait')
            ->setOptions(['dpi' => 100, 'enable_php' => true]);

        return $pdf->stream();
    }
}
