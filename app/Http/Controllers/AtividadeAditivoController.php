<?php

namespace App\Http\Controllers;

use App\Models\AtividadeAditivo;
use Illuminate\Http\Request;

class AtividadeAditivoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AtividadeAditivo  $atividadeAditivo
     * @return \Illuminate\Http\Response
     */
    public function show(AtividadeAditivo $atividadeAditivo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AtividadeAditivo  $atividadeAditivo
     * @return \Illuminate\Http\Response
     */
    public function edit(AtividadeAditivo $atividadeAditivo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AtividadeAditivo  $atividadeAditivo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AtividadeAditivo $atividadeAditivo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AtividadeAditivo  $atividadeAditivo
     * @return \Illuminate\Http\Response
     */
    public function destroy(AtividadeAditivo $atividadeAditivo)
    {
        //
    }
}
