<?php

namespace App\Http\Controllers;

use App\Models\Tce;
use Illuminate\Http\Request;
use App\Models\Estudante;
use App\Models\Concedente;
use App\Models\Relatorio;
use App\Models\Periodo;
use App\Models\Supervisor;
use App\Models\Atividade;
use App\Models\Aditivo;
use App\Models\MotivoCancelamento;
use App\Models\SecConcedente;
use Carbon\Carbon;
use PDF;
use Auth;
use DB;
use Validator;

class TceController extends Controller
{
    private $tce;
    private $concedente;
    private $estudante;
    private $motivoCancelamento;

    public function __construct(Tce $tce, Concedente $concedente, Estudante $estudante, MotivoCancelamento $motivoCancelamento)
    {
        $this->tce = $tce;
        $this->concedente = $concedente;
        $this->estudante = $estudante;
        $this->motivoCancelamento = $motivoCancelamento;
    }

    public function index()
    {
        $titulo = 'Listagem de Tces';
        if (Auth::user()->unidade_id != null) {
            $relEstado = Auth::user()->unidade->estado->pluck('id')->toArray();
            $tces = $this->tce->where('dtCancelamento', null)
                ->orderBy('id', 'desc')
                ->WhereIn('estado_id', $relEstado)
                //->paginate(config('sysGlobal.Paginacao'));
                ->get();
        } else {
            $tces = $this->tce->where('dtCancelamento', null)->orderBy('id', 'desc')
            //->paginate(config('sysGlobal.Paginacao'));
            ->get();
        }
        //dd($tces);
        return view('tce.index', compact('tces', 'titulo'));
    }

    public function indexCancelados()
    {
        $titulo = 'Listagem de Tces Cancelados';

        if (Auth::user()->unidade_id != null) {
            $relEstado = Auth::user()->unidade->estado->pluck('id')->toArray();
            $tcesCancelados = $this->tce->where('dtCancelamento', '<>', null)
                ->orderBy('id', 'desc')
                ->WhereIn('estado_id', $relEstado)
                ->paginate(config('sysGlobal.Paginacao'));
        } else {
            $tcesCancelados = $this->tce->where('dtCancelamento', '<>', null)->orderBy('dtCancelamento', 'desc')->paginate(config('sysGlobal.Paginacao'));
        }
        //dd($tces);
        return view('tce.index', compact('tcesCancelados', 'titulo'));
    }

    public function search(Request $request)
    {
        $titulo = 'Busca de Tces';
        //Gets Input
        $Tipo = $request->get('tipo');
        $key_search = $request->get('key_search');

        if (!empty($Tipo == null)) {
            return redirect()->route('tce.index');
        }

        //Se usuario for de alguma unidade entra no laço
        if (Auth::user()->unidade_id != null) {
            //Pega estados que a unidade do usuario pode ver
            $relEstado = Auth::user()->unidade->estado->pluck('id')->toArray();

            //Busca Tce pelo numero tce antigo
            if (!empty($Tipo == 'migracao')) {
                $tces = $this->tce->where('dtCancelamento', null)
                        ->where('migracao', '=', $key_search)
                        ->WhereIn('estado_id', $relEstado)
                        //->paginate(config('sysGlobal.Paginacao'));
                        ->get();
            }
            //Busca Tce pelo numero
            if (!empty($Tipo == 'tce')) {
                $tces = $this->tce->where('dtCancelamento', null)
                        ->where('id', '=', $key_search)
                        ->WhereIn('estado_id', $relEstado)
                        //->paginate(config('sysGlobal.Paginacao'));
                        ->get();
            }
            //Busca TCE pelo nome do estudante
            if (!empty($Tipo == 'estudante')) {
                //Pega todos os TCES que não estão cancelados dtCancelamento = NULL
                $tces = $this->tce->where('dtCancelamento', null)
                        ->WhereIn('estado_id', $relEstado)
                        ->whereHas('estudante', function ($query) use ($key_search) {
                            $query->where('nmEstudante', 'LIKE', '%'.$key_search.'%');
                        })
                    //->paginate(config('sysGlobal.Paginacao'));
                    ->get();
            }
            //Busca Tce pelo nome da concedente
            if (!empty($Tipo == 'concedente')) {
                //Pega todos os TCES que não estão cancelados dtCancelamento = NULL
                $tces = $this->tce->where('dtCancelamento', null)
                        ->WhereIn('estado_id', $relEstado)
                        ->whereHas('concedente', function ($query) use ($key_search) {
                            $query->where('nmRazaoSocial', 'LIKE', '%'.$key_search.'%');
                        })
                    //->paginate(config('sysGlobal.Paginacao'));
                    ->get();
            }
        } else {
            //Busca TCE pelo numero TCE antigo
            if (!empty($Tipo == 'migracao')) {
                //$tces = $this->tce->where('id','=',$key_search)->get();
                $tces = $this->tce->where('dtCancelamento', null)
                    ->where('migracao', '=', $key_search)
                    //->paginate(config('sysGlobal.Paginacao'));
                    ->get();
            }
            //Busca TCE pelo numero
            if (!empty($Tipo == 'tce')) {
                //$tces = $this->tce->where('id','=',$key_search)->get();
                $tces = $this->tce->where('dtCancelamento', null)
                    ->where('id', '=', $key_search)
                    //->paginate(config('sysGlobal.Paginacao'));
                    ->get();
            }
            //Busca TCE pelo nome do estudante
            if (!empty($Tipo == 'estudante')) {
                $tces = $this->tce->where('dtCancelamento', null)
                            ->whereHas('estudante', function ($query) use ($key_search) {
                                $query->where('nmEstudante', 'LIKE', '%'.$key_search.'%');
                            })
                    //->paginate(config('sysGlobal.Paginacao'));
                    ->get();
            }
            //Busca Tce pelo nome da concedente
            if (!empty($Tipo == 'concedente')) {
                //Pega todos os TCES que não estão cancelados dtCancelamento = NULL
                $tces = $this->tce->where('dtCancelamento', null)
                        ->whereHas('concedente', function ($query) use ($key_search) {
                            $query->where('nmRazaoSocial', 'LIKE', '%'.$key_search.'%');
                        })
                    //->paginate(config('sysGlobal.Paginacao'));
                    ->get();
            }
        }

        return view('tce.index', compact('tces', 'titulo'));
    }

    public function searchCancelados(Request $request)
    {
        $titulo = 'Busca de Tces Cancelados';
        //Gets Input
        $Tipo = $request->get('tipo');
        $key_search = $request->get('key_search');

        //Se formulario vinher com tipo não selecionado volta pra index de cancelados.
        if (!empty($Tipo == null)) {
            return redirect()->route('tce.index.cancelados');
        }

        //Se usuario for de alguma unidade entra no laço
        if (Auth::user()->unidade_id != null) {
            //Pega estados que a unidade do usuario pode ver
            $relEstado = Auth::user()->unidade->estado->pluck('id')->toArray();

            //Busca Tce pelo numero tce antigo
            if (!empty($Tipo == 'migracao')) {
                //$tces = $this->tce->where('id','=',$key_search)->get();
                $tcesCancelados = $this->tce->where('dtCancelamento', '<>', null)
                        ->where('migracao', '=', $key_search)
                        ->WhereIn('estado_id', $relEstado)
                        ->paginate(config('sysGlobal.Paginacao'));
            }
            //Busca Tce pelo numero
            if (!empty($Tipo == 'tce')) {
                //$tces = $this->tce->where('id','=',$key_search)->get();
                $tcesCancelados = $this->tce->where('dtCancelamento', '<>', null)
                        ->where('id', '=', $key_search)
                        ->WhereIn('estado_id', $relEstado)
                        ->paginate(config('sysGlobal.Paginacao'));
            }
            //Busca TCE pelo nome do estudante
            if (!empty($Tipo == 'estudante')) {
                //Pega todos os TCES que não estão cancelados dtCancelamento = NULL
                $tcesCancelados = $this->tce->where('dtCancelamento', '<>', null)
                        ->WhereIn('estado_id', $relEstado)
                        ->whereHas('estudante', function ($query) use ($key_search) {
                            $query->where('nmEstudante', 'LIKE', '%'.$key_search.'%');
                        })
                    ->paginate(config('sysGlobal.Paginacao'));
            }
            //Busca Tce pelo nome da concedente
            if (!empty($Tipo == 'concedente')) {
                //Pega todos os TCES que não estão cancelados dtCancelamento = NULL
                $tcesCancelados = $this->tce->where('dtCancelamento', '<>', null)
                        ->WhereIn('estado_id', $relEstado)
                        ->whereHas('concedente', function ($query) use ($key_search) {
                            $query->where('nmRazaoSocial', 'LIKE', '%'.$key_search.'%');
                        })
                    ->paginate(config('sysGlobal.Paginacao'));
            }
        } else {
            //Busca TCE pelo numero tce antigo
            if (!empty($Tipo == 'migracao')) {
                //$tces = $this->tce->where('id','=',$key_search)->get();
                $tcesCancelados = $this->tce->where('dtCancelamento', '<>', null)
                    ->where('migracao', '=', $key_search)
                    ->paginate(config('sysGlobal.Paginacao'));
            }
            //Busca TCE pelo numero
            if (!empty($Tipo == 'tce')) {
                //$tces = $this->tce->where('id','=',$key_search)->get();
                $tcesCancelados = $this->tce->where('dtCancelamento', '<>', null)
                    ->where('id', '=', $key_search)
                    ->paginate(config('sysGlobal.Paginacao'));
            }
            //Busca TCE pelo nome do estudante
            if (!empty($Tipo == 'estudante')) {
                $tcesCancelados = $this->tce->where('dtCancelamento', '<>', null)
                            ->whereHas('estudante', function ($query) use ($key_search) {
                                $query->where('nmEstudante', 'LIKE', '%'.$key_search.'%');
                            })
                    ->paginate(config('sysGlobal.Paginacao'));
            }
            //Busca Tce pelo nome da concedente
            if (!empty($Tipo == 'concedente')) {
                //Pega todos os TCES que não estão cancelados dtCancelamento = NULL
                $tcesCancelados = $this->tce->where('dtCancelamento', '<>', null)
                        ->whereHas('concedente', function ($query) use ($key_search) {
                            $query->where('nmRazaoSocial', 'LIKE', '%'.$key_search.'%');
                        })
                    ->paginate(config('sysGlobal.Paginacao'));
            }
        }

        return view('tce.index', compact('tcesCancelados', 'titulo'));
    }

    /*FORMULARIO DE CRIAR TCE*/
    public function create(Request $request, $id)
    {
        //dd($id);
        $dtHoje = Carbon::today()->format('d/m/Y');
        $estudante = Estudante::findOrFail($id);
        $curso = $estudante->cursoDaInstituicao->curso->id;
        $atividades = Atividade::whereIn('curso_id', [$curso])->orderBy('atividade', 'ASC')->get();
        //Verificação de Estado do Usuario
        if (Auth::user()->unidade_id != null) {
            $relEstado = Auth::user()->unidade->estado->pluck('id')->toArray();
            $concedentes = $this->concedente
                    ->WhereIn('estado_id', $relEstado)->get();
        } else {
            $concedentes = Concedente::all();
        }
        //$concedentes = Concedente::all();
        $relatorios = Relatorio::all();
        $periodos = Periodo::all()->sortBy('id');

        //Verificação de Estado do Usuario
        /*if(Auth::user()->unidade_id != null){//Verificando se recebe unidade
            if($estudante->estado_id == Auth::user()->unidade->estado_id){//verificando se estado é igual ao estado da unidad
                $estudante;
            }else{
                return redirect()->route('tce.index');
            }
        }
        */
        //dd($curso,$atividades->all());
        //dd($estudante);
        $secretarias = [];

        return View('tce.cadastro', [
            'dtHoje' => $dtHoje,
            'estudante' => $estudante,
            'concedentes' => $concedentes,
            'atividades' => $atividades,
            'relatorios' => $relatorios,
            'periodos' => $periodos,
            'secretarias' => $request->session()->get('secretarias') ?? [],
        ]);
        //return view('tce.cadastro', compact('estudante','concedentes','relatorios','periodos','dtHoje','atividades'));
    }

    /* LOGICA CRIAÇÃO TCE */
    public function store(Request $request)
    {
        DB::beginTransaction();

        try {
            $formulario = $request->all();
            $formulario['user_id'] = Auth::user()->id;

            $validar = Validator::make($request->all(), [
                'dtInicio' => 'required',
                'dtFim' => 'required',
                'atividades' => 'required',
                'concedente_id' => 'required',
                'relatorio_id' => 'required',
                'periodo_id' => 'required',
                'dsApolice' => 'required',
            ], [
                'dtInicio.required' => ' O campo INICIO Obrigatório.',
                'dtFim.required' => ' O campo FIM é obrigatório.',
                'atividades.required' => ' Escolha pelomenos 1 ATIVIDADE.',
                'concedente_id.required' => ' O campo CONCEDENTE é obrigatório.',
                'relatorio_id.required' => ' O campo TIPO TCE é obrigatório.',
                'periodo_id.required' => ' O campo PERIODO é obrigatório.',
                'dsApolice.required' => ' O Campo Apolice é obrigatório',
            ]);

            if ($validar->fails()) {
                $secretarias = SecConcedente::where('concedente_id', $formulario['concedente_id'])->get();

                return
                redirect()
                ->back()
                ->with('secretarias', $secretarias)
                ->withErrors($validar)
                ->withInput();
            }
            $dtFim = Carbon::createFromFormat('d/m/Y', $formulario['dtFim']);
            $dtInicio = Carbon::createFromFormat('d/m/Y', $formulario['dtInicio']);
            //Verifica se data fim do TCE é menor que a data inicio do TCE. Tem gente doida meu amigo(a).
            if ($dtFim->toDateString() < $dtInicio->toDateString()) {
                return redirect()
                    ->back()
                    ->with('error', 'O FIM DO TCE('.$dtFim->format('d/m/Y').') É ANTES DO INICIO DO TCE('.$dtInicio->format('d/m/Y').') Linha 338.')
                    ->withInput();
            }
            //Pegando o Estudante do TCE e o Concedente
            $getEstudante = $this->estudante->findOrFail($formulario['estudante_id']);
            $getConcedente = $this->concedente->findOrFail($formulario['concedente_id']);
            //Pega a quantidade de semestres do curso e o do aluno
            $qtdCurso = $getEstudante->cursoDaInstituicao->qtdDuracao;
            $qtdEstudante = $getEstudante->nnSemestreAno;
            //Verifica se concedente tem dtFimContrato
            if ($getConcedente->dtFimContrato) {
                $fimContrato = Carbon::parse($getConcedente->dtFimContrato);
            }

            if ($request['cdCPFSupervisor'] != null) {
                $supervisors = Supervisor::where('cpf', $request['cdCPFSupervisor']);
                if ($supervisors->count() > 0) {
                    if ($request['sec_conc_id'] != null) {
                        $supervisor = $supervisors->where('secConcedente_id', $request['sec_conc_id'])->first();
                        $formulario['sec_conc_id'] = $request['sec_conc_id'];
                    } else {
                        $supervisor = $supervisors->where('concedente_id', $request['concedente_id'])->first();
                    }

                    if ($supervisor) {
                        if (Supervisor::validate($supervisor) == true) {
                            $formulario['supervisor_id'] = $supervisor->id;
                        } else {
                            return redirect()
                      ->back()
                      ->with('error', 'O SUPERVISOR SELECIONADO JÁ ATINGIU O LIMITE DE SUPERVISÃO DE ESTÁGIO.')
                      ->withInput();
                        }
                    } else {
                        $supervisor = Supervisor::create([
                  'nome' => $request['nmSupervisor'],
                  'cpf' => $request['cdCPFSupervisor'],
                  'cargo' => $request['dsSupervisorCargo'],
                  'concedente_id' => $request['concedente_id'],
                  'secConcedente_id' => $request['sec_conc_id'],
                ]);
                        $formulario['supervisor_id'] = $supervisor->id;
                    }
                } else {
                    $supervisor = Supervisor::create([
                'nome' => $request['nmSupervisor'],
                'cpf' => $request['cdCPFSupervisor'],
                'cargo' => $request['dsSupervisorCargo'],
                'concedente_id' => $request['concedente_id'],
                'secConcedente_id' => $request['sec_conc_id'],
              ]);
                    $formulario['supervisor_id'] = $supervisor->id;
                }
            }

            //verificando a data do contrato se for null ou maior que data limite
            //if($getConcedente->dtFimContrato == NULL OR $fimContrato->toDateString() > $dtFim->toDateString()) {
            if ($getConcedente->dtFimContrato == null or $fimContrato->toDateString() >= $dtFim->toDateString()) {
                switch ($getEstudante->cursoDaInstituicao->curso_duracao_id) {
                    //ROTINA PARA CURSOS COM DURACÃO ANUAL
                    case '1':
                        $totCurso = $qtdCurso * 12;
                        $totEstudante = $qtdEstudante * 12;
                        $dif = $totCurso - $totEstudante;
                        $final = $dtInicio->addMonth($dif);

                        //Caso o estudante esteja no ultimo Ano
                        if ($qtdCurso == $qtdEstudante) {
                            $dtInicio = Carbon::createFromFormat('d/m/Y', $formulario['dtInicio']);
                            $incioAdd = Carbon::create($dtInicio->year, 12, 31, 0);
                        } else {
                            $incioAdd = Carbon::create($final->year, 12, 31, 0);
                        }

                        //Se datafim do tce é maior que o fim calculado acima
                        if ($dtFim->toDateString() > $incioAdd) {
                            return redirect()
                                ->back()
                                ->with('error', 'O FIM DO TCE('.$dtFim->format('d/m/Y').') É DEPOIS DO FIM DO CURSO('.$incioAdd->format('d/m/Y').') Linha 375.')
                                ->withInput();
                        } else {
                            //verificando se existe tce com o mesmo concedente
                            if ($getEstudante->tce->where('concedente_id', $formulario['concedente_id'])->all()) {
                                $dtInicio = Carbon::createFromFormat('d/m/Y', $formulario['dtInicio']);

                                $dataTotTrabalhado = 0;
                                foreach ($getEstudante->tce->where('concedente_id', $formulario['concedente_id']) as $tces) {
                                    //Pegando a diferença de tempo entre o cancelamento e o incio dos tces
                                    $dataTotTrabalhado += $tces->dtInicio->diffInMonths($tces->dtCancelamento);
                                }
                                //Pega diferença em meses da dtInicio do formulario e dtFim
                                $difInicioFim = $dtInicio->diffInMonths($dtFim);
                                //Soma total trabalhado em todos tces encontrados com difInicioFim
                                $totalFinal = $dataTotTrabalhado + $difInicioFim;
                                //Verificando dif fim com limite
                                if ($totalFinal > 24) {
                                    return redirect()
                                        ->back()
                                        ->with('error', 'ERRO, O Limite é de 24 Meses. | Totalizou '.$totalFinal.' Meses. Linha: 396.')
                                        ->withInput();
                                } else {
                                    //Criando o TCE
                                    $new = $this->tce->create($formulario);
                                    //Criando relacionamento de atividades com tce
                                    $new->atividades()->attach(array_values($formulario['atividades']));
                                }
                                //Não houve tce com a mesma concedente
                            } else {
                                $dtInicio = Carbon::createFromFormat('d/m/Y', $formulario['dtInicio']);
                                //Pegando a diferença de tempo entre datainicio e datafim do formulario
                                $dataTotPeriodo = $dtInicio->diffInMonths($dtFim);
                                //Verificando dif fim com limite
                                if ($dataTotPeriodo > 24) {
                                    return redirect()
                                        ->back()
                                        ->with('error', 'ERRO, O Limite é de 24 Meses. O Estudante já estagiou nesta concedente. | Totalizou '.$dataTotPeriodo.' Meses. Linha 414.')
                                        ->withInput();
                                } else {
                                    //Criando o TCE
                                    $new = $this->tce->create($formulario);
                                    //Criando relacionamento de atividades com tce
                                    $new->atividades()->attach(array_values($formulario['atividades']));
                                }//Fim else > 24
                            }
                        }//fim verificar datafim do tce é maior que o fim calculado
                        break;

                    //ROTINA PARA CURSOS COM DURACÃO SEMESTRE
                    case '2':
                        $totCurso = $qtdCurso * 6;
                        $totEstudante = $qtdEstudante * 6;
                        $dif = $totCurso - $totEstudante;
                        $final = $dtInicio->addMonth($dif);

                        //Caso o estudante esteja no ultimo Semestre
                        if ($qtdCurso == $qtdEstudante) {
                            if ($dtInicio->month <= 6) {//Pega dt inicio pq o cara só tem o semestre corrente pra estagio
                                $dtInicio = Carbon::createFromFormat('d/m/Y', $formulario['dtInicio']);
                                //Primeiro Semestre do Ano
                                $incioAdd = Carbon::create($dtInicio->year, 6, 30, 0);
                            } else {
                                $dtInicio = Carbon::createFromFormat('d/m/Y', $formulario['dtInicio']);
                                //Segundo Semestre do Ano
                                $incioAdd = Carbon::create($dtInicio->year, 12, 31, 0);
                            }
                        } else {
                            //Se o mês for menor ou igual 6 entra no 1 semestre do ano
                            if ($final->month <= 6) {//pega dtfinal pois o cara tem mais semestres pra estagio
                                //Primeiro Semestre do Ano
                                $incioAdd = Carbon::create($final->year, 6, 30, 0);
                            } else {
                                //Segundo Semestre do Ano
                                $incioAdd = Carbon::create($final->year, 12, 31, 0);
                            }
                        }

                        $dtInicio = Carbon::createFromFormat('d/m/Y', $formulario['dtInicio']);
                        //verifica se data fim do formulario é maior que o calculo feito acima
                        if ($dtFim->toDateString() > $incioAdd) {
                            return redirect()
                                ->back()
                                ->with('error', 'O FIM DO TCE('.$dtFim->format('d/m/Y').') É DEPOIS DO FIM DO CURSO('.$incioAdd->format('d/m/Y').'). Linha 462.')
                                ->withInput();
                        } else {
                            //Verifica se tem tce com a mesma concedente e entra no calculo de limite
                            if ($getEstudante->tce->where('concedente_id', $formulario['concedente_id'])->all()) {
                                $dtInicio = Carbon::createFromFormat('d/m/Y', $formulario['dtInicio']);
                                $dataTotTrabalhado = 0;
                                foreach ($getEstudante->tce->where('concedente_id', $formulario['concedente_id']) as $tces) {
                                    //Pegando a diferença de tempo entre o cancelamento e o incio dos tces
                                    $dataTotTrabalhado += $tces->dtInicio->diffInMonths($tces->dtCancelamento);
                                }
                                //pega diferença em meses da dtInicio do formulario e dtFim
                                $difInicioFim = $dtInicio->diffInMonths($dtFim);
                                //Soma total trabalhado em todos tces encontrados com difInicioFim
                                $totalFinal = $dataTotTrabalhado + $difInicioFim;

                                if ($totalFinal > 24) {//Verificando dif fim com limite
                                    return redirect()
                                        ->back()
                                        ->with('error', 'ERRO, O total de meses permitido nesta concedente é de 24 Meses. O Estagiario já teve TCE desta Concedente. | Total de '.$totalFinal.' Meses. Linha 481.')
                                        ->withInput();
                                } else {
                                    //Criando o TCE
                                    $new = $this->tce->create($formulario);
                                    //Montando relacionamento de TCe com Atividades
                                    $new->atividades()->attach(array_values($formulario['atividades']));
                                }
                            } else { //não houve TCE com a concedente
                                $dtFim = Carbon::createFromFormat('d/m/Y', $formulario['dtFim']);
                                $dtInicio = Carbon::createFromFormat('d/m/Y', $formulario['dtInicio']);
                                //Pegando a diferença de tempo entre o fim e o incio vindo do formulario
                                $dataTotPeriodo = $dtInicio->diffInMonths($dtFim);
                                if ($dataTotPeriodo > 24) {//Verificando dif fim com limite
                                    return redirect()
                                        ->back()
                                        ->with('error', 'ERRO, O Periodo maximo é de 24 Meses | Totalizou '.$dataTotPeriodo.' Meses. Linha 499.')
                                        ->withInput();
                                } else {
                                    //Criando o TCE
                                    $new = $this->tce->create($formulario);
                                    //Montando relacionamento de TCE com Atividades
                                    $new->atividades()->attach(array_values($formulario['atividades']));
                                }//Fim else > 24
                            }
                        }//fim verificação

                        break;
                    /*
                    default:
                        $dtFim      = Carbon::createFromFormat('d/m/Y', $formulario['dtFim']) ;
                        $dtInicio   = Carbon::createFromFormat('d/m/Y', $formulario['dtInicio']);
                        //verificando se existe tce com o mesmo concedente
                        if ($getEstudante->tce && $getEstudante->tce->concedente_id == $formulario['concedente_id']) {

                            //Pegando a diferença de tempo entre o cancelamento e o incio
                            $dataTotTrabalhado = $getEstudante->tce->dtInicio->diffInMonths($getEstudante->tce->dtCancelamento);
                            //Verificando se a dataFim é maior do a Limite e mair que 24
                            $difInicioFim = $getEstudante->tce->dtInicio->diffInMonths($dtFim);
                            //Somando o total de meses que tem entre Inicio e Cancelamento + Total de meses do Formulario
                            $totalFinal = $dataTotTrabalhado + $difInicioFim;

                            if ($totalFinal > 24) {//Verificando dif fim com limite
                                //dd('ERRO, A Data Limite é maior | Total é de - ' . $totalFinal . ' Meses');
                                return redirect()
                                    ->back()
                                    ->with('error', 'ERRO, O periodo maximo é de 24 Meses. | Totalizou ' . $totalFinal . ' Meses. Linha 524')
                                    ->withInput();

                            } else {
                                //Criando o TCE
                                $new = $this->tce->create($formulario);
                                $new->atividades()->attach(array_values($formulario['atividades']));
                            }

                        } else { //não houve TCE com este concedente

                            //Pegando a diferença de tempo entre o cancelamento e o incio
                            $dataTotPeriodo = $dtInicio->diffInMonths($dtFim);

                            if ($dataTotPeriodo > 24) {//Verificando dif fim com limite
                                //dd('ERRO, A Data Limite é maior | Total é de - ' . $dataTotPeriodo . ' Meses');
                                return redirect()
                                    ->back()
                                    ->with('error', 'ERRO, O periodo maximo é de 24 Meses. | Totalizou ' . $dataTotPeriodo . ' Meses. Linha 542')
                                    ->withInput();
                            } else {
                                //Criando o TCE
                                $new = $this->tce->create($formulario);
                                $new->atividades()->attach(array_values($formulario['atividades']));
                            }//Fim else > 24

                        }

                        BREAK;
                        */
                }
                //1 ANO / 2 SEMESTRE
            } elseif ($fimContrato->toDateString() < $dtFim->toDateString()) {// a data do contrato é finalizado antes
                return redirect()
                    ->back()
                    ->with('error', 'O FIM DO CONTRATO COM A CONCEDENTE ('.$fimContrato->format('d/m/Y').') É ANTES DO FIM DO TCE ('.$dtFim->format('d/m/Y').'). Linha 565.')
                    ->withInput();
            }
        } catch (Exception $e) {
            DB::rollback();
            echo $e->getMessage();
        }
        DB::commit();
        //dd($fimContrato->toDateString(), $dtFim->toDateString());
        if ($new) {
            Estudante::where(['id' => $formulario['estudante_id']])->update(['tce_id' => $new->id]);
            //Redireciona para ver tce quando o mesmo é cadastrado.
            return redirect()
                ->route('tce.show', ['tce' => $new->id])
                ->with('success', 'Cadastro Realizado com Sucesso!');
        } else {
            return redirect()
                ->back()
                ->with('error', 'Falha ao Cadastrar! Linha 561.')
                ->withInput();
        }
    }

    public function show($idTce)
    {
        $titulo = 'Tce';
        $tce = $this->tce->findOrFail($idTce);
        //Verificação de Estado do Usuario
        if (Auth::user()->unidade_id != null) {//Verificando se recebe unidade
            $relEstado = Auth::user()->unidade->estado->pluck('id')->toArray();
            if (in_array($tce->estudante->estado_id, $relEstado)) {//verificando se estado é igual ao estado da unidad
                $tce;
            } else {
                return redirect()->route('tce.index');
            }
        }
        $aditivos = Aditivo::where('tce_id', $idTce)->orderBy('id', 'desc')->get();

        return view('tce.visualizar', compact('titulo', 'tce', 'aditivos'));
    }

    public function edit(Request $request, $tce_id)
    {
        $titulo = 'Edição de Tce';
        $tce = $this->tce->findOrFail($tce_id);
        $estudante = Estudante::findOrFail($tce->estudante_id);
        //$curso = $estudante->cursoDaInstituicao->curso->id;
        //$atividades = Atividade::whereIn('curso_id', [$curso]);
        $concedentes = Concedente::all();
        $relatorios = Relatorio::all();
        $periodos = Periodo::all()->sortBy('id');

        //Verificação de Estado do Usuario
        if (Auth::user()->unidade_id != null) {//Verificando se recebe unidade
            $relEstado = Auth::user()->unidade->estado->pluck('id')->toArray();
            if (in_array($tce->estudante->estado_id, $relEstado)) {//verificando se estado é igual ao estado da unidad
                $tce;
            } else {
                return redirect()->route('tce.index');
            }
        }
        $secretarias = $tce->concedente->secConcedente()->get(['id', 'nmSecretaria']);

        if ($tce->sec_conc_id != null) {
            $supervisors = Supervisor::all()->where('secConcedente_id', $tce->sec_conc_id);
        } else {
            $supervisors = Supervisor::all()->where('concedente_id', $tce->concedente_id);
        }

        //return dd($supervisors);
        //$secretarias = [];
        return View('tce.editar', [
            'titulo' => $titulo,
            'tce' => $tce,
            'estudante' => $estudante,
            'concedentes' => $concedentes,
            'relatorios' => $relatorios,
            'periodos' => $periodos,
            'supervisors' => $supervisors,
            //'secretarias' => $request->session()->get('secretarias') ?? []
            'secretarias' => $secretarias,
        ]);
        //return view('tce.editar', compact('tce','estudante','concedentes','relatorios','periodos'));
    }

    public function update(Request $request, $tce_id)
    {
        DB::beginTransaction();
        try {
            //Pegando o TCE
            $tce = $this->tce->findOrFail($tce_id);
            $formulario = $request->all();
            //dd($formulario);
            $validar = Validator::make($request->all(), [
                'dtInicio' => 'required',
                'dtFim' => 'required',
                'atividades' => 'required',
                'concedente_id' => 'required',
                'relatorio_id' => 'required',
                'periodo_id' => 'required',
            ], [
                'dtInicio.required' => ' O campo INICIO Obrigatório.',
                'dtFim.required' => ' O campo FIM é obrigatório.',
                'atividades.required' => ' Escolha pelomenos 1 ATIVIDADE.',
                'concedente_id.required' => ' O campo CONCEDENTE é obrigatório.',
                'relatorio_id.required' => ' O campo TIPO TCE é obrigatório.',
                'periodo_id.required' => ' O campo PERIODO é obrigatório.',
            ]);

            if ($validar->fails()) {
                $secretarias = SecConcedente::where('concedente_id', $formulario['concedente_id'])->get();

                return
                redirect()
                ->back()
                ->with('secretarias', $secretarias)
                ->withErrors($validar)
                ->withInput();
            }
            $dtFim = Carbon::createFromFormat('d/m/Y', $formulario['dtFim']);
            $dtInicio = Carbon::createFromFormat('d/m/Y', $formulario['dtInicio']);
            //Verifica se data fim do TCE é menor que a data inicio do TCE. Tem gente doida meu amigo(a).
            if ($dtFim->toDateString() < $dtInicio->toDateString()) {
                return redirect()
                    ->back()
                    ->with('error', 'O FIM DO TCE('.$dtFim->format('d/m/Y').') É ANTES DO INICIO DO TCE('.$dtInicio->format('d/m/Y').') Linha 682.')
                    ->withInput();
            }
            //Pegando o Estudante do TCE e Concedente
            $getEstudante = $this->estudante->findOrFail($formulario['estudante_id']);
            //$getConcedente = $this->concedente->findOrFail($formulario['concedente_id']);
            $getConcedente = $this->concedente->findOrFail($tce->concedente_id);
            //Pega a quantidade de semestres do curso e o do aluno
            $qtdCurso = $getEstudante->cursoDaInstituicao->qtdDuracao;
            $qtdEstudante = $getEstudante->nnSemestreAno;
            //Verifica se concedente tem dtFimContrato
            if ($getConcedente->dtFimContrato) {
                //$fimContrato = $getConcedente->dtFimContrato->toDateString();
                $fimContrato = $getConcedente->dtFimContrato;
            }

            $supervisors = Supervisor::where('cpf', $request['cdCPFSupervisor']);
            if ($supervisors->count() > 0) {
                if ($request['sec_conc_id'] != null) {
                    $supervisor = $supervisors->where('secConcedente_id', $request['sec_conc_id'])->first();
                    $formulario['sec_conc_id'] = $request['sec_conc_id'];
                } else {
                    $supervisor = $supervisors->where('concedente_id', $request['concedente_id'])->first();
                }

                //   if($supervisor){
            //     if(Supervisor::validate($supervisor) == true){
            //       $formulario['supervisor_id'] = $supervisor->id;
            //     }else{
            //       return redirect()
            //           ->back()
            //           ->with('error', 'O SUPERVISOR SELECIONADO JÁ É ATINGIU O LIMITE DE SUPERVISÃO DE ESTÁGIO.')
            //           ->withInput();
            //     }
            //   }
            }
            //verificando a data do contrato se for null ou maior que data limite
            if ($getConcedente->dtFimContrato == null or $fimContrato->toDateString() >= $dtFim->toDateString()) {//verificando a data do contrato se for null, igual ou maior que data fim
                switch ($getEstudante->cursoDaInstituicao->curso_duracao_id) {
                    //ROTINA PARA CURSOS COM DURACÃO ANUAL
                    case '1':
                    $totCurso = $qtdCurso * 12;
                    $totEstudante = $qtdEstudante * 12;
                    $dif = $totCurso - $totEstudante;
                    $final = $dtInicio->addMonth($dif);

                    //Caso o estudante esteja no ultimo Ano
                    if ($qtdCurso == $qtdEstudante) {
                        $dtInicio = Carbon::createFromFormat('d/m/Y', $formulario['dtInicio']);
                        $incioAdd = Carbon::create($dtInicio->year, 12, 31, 0);
                    } else {
                        $incioAdd = Carbon::create($final->year, 12, 31, 0);
                    }
                        //Se datafim do tce é maior que o fim calculado acima
                        if ($dtFim->toDateString() > $incioAdd) {
                            //dd('O FIM DO TCE É DEPOIS DO FIM DO CURSO');
                            return redirect()
                                ->back()
                                ->with('error', 'O FIM DO TCE('.$dtFim->format('d/m/Y').') É DEPOIS DO FIM DO CURSO('.$incioAdd->format('d/m/Y').') Linha 721.')
                                ->withInput();
                        } else {
                            //verificando se existe tce com o mesmo concedente
                            if ($getEstudante->tce->where('concedente_id', $formulario['concedente_id'])->whereNotIn('id', $tce_id)->all()) {
                                $dtInicio = Carbon::createFromFormat('d/m/Y', $formulario['dtInicio']);

                                $dataTotTrabalhado = 0;
                                foreach ($getEstudante->tce->where('concedente_id', $formulario['concedente_id']) as $tces) {
                                    //Pegando a diferença de tempo entre o cancelamento e o incio dos tces
                                    $dataTotTrabalhado += $tces->dtInicio->diffInMonths($tces->dtCancelamento);
                                }
                                //Pega diferença em meses da dtInicio do formulario e dtFim
                                $difInicioFim = $dtInicio->diffInMonths($dtFim);
                                //Soma total trabalhado em todos tces encontrados com difInicioFim
                                $totalFinal = $dataTotTrabalhado + $difInicioFim;
                                //Verificando dif fim com limite
                                if ($totalFinal >= 24) {//Verificando dif fim com limite
                                    //dd('ERRO, A Data Limite é maior | Total é de - ' . $totalFinal . ' Meses');
                                    return redirect()
                                        ->back()
                                        ->with('error', 'ERRO, O total de meses permitido nesta concedente é de 24 meses. O estagiario já teve TCE desta Concedente. | Total é de - '.$totalFinal.' Meses. Linha 743.')
                                        ->withInput();
                                } else {
                                    //Atualizando o TCE
                                    $atualizar = $tce->update($formulario);
                                }
                                //Não houve tce com a mesma concedente
                            } else {
                                $dtInicio = Carbon::createFromFormat('d/m/Y', $formulario['dtInicio']);
                                //Pegando a diferença de tempo entre o cancelamento e o incio
                                $dataTotPeriodo = $dtInicio->diffInMonths($dtFim);

                                if ($dataTotPeriodo > 24) {//Verificando dif fim com limite
                                    return redirect()
                                        ->back()
                                        ->with('error', 'ERRO, O Limite é de 24 Meses. | Totalizou '.$dataTotPeriodo.' Meses. Linha 758.')
                                        ->withInput();
                                } else {
                                    //Atualizando o TCE
                                    $atualizar = $tce->update($formulario);
                                }//Fim else > 24
                            }
                        }//fim verificação $totEstudante > $totCurso
                        break;

                    //ROTINA PARA CURSOS COM DURACÃO SEMESTRE
                    case '2':

                        $totCurso = $qtdCurso * 6;
                        $totEstudante = $qtdEstudante * 6;
                        $dif = $totCurso - $totEstudante;
                        $final = $dtInicio->addMonth($dif);

                        //Caso o estudante esteja no ultimo Semestre
                        if ($qtdCurso == $qtdEstudante) {
                            if ($dtInicio->month <= 6) {//pega dtinicio pois o cara só tem o semestre corrente pra estagiar
                                $dtInicio = Carbon::createFromFormat('d/m/Y', $formulario['dtInicio']);
                                //Primeiro Semestre do Ano
                                $incioAdd = Carbon::create($dtInicio->year, 6, 30, 0);
                            } else {
                                $dtInicio = Carbon::createFromFormat('d/m/Y', $formulario['dtInicio']);
                                //Segundo Semestre do Ano
                                $incioAdd = Carbon::create($dtInicio->year, 12, 31, 0);
                            }
                        } else {
                            //Se o mês for menor ou igual 6 entra no 1 semestre do ano
                            if ($final->month <= 6) {//pega o dtfinal pois o cara tem mais tempo pra estagiar
                                //Primeiro Semestre do Ano
                                $incioAdd = Carbon::create($final->year, 6, 30, 0);
                            } else {
                                //Segundo Semestre do Ano
                                $incioAdd = Carbon::create($final->year, 12, 31, 0);
                            }
                        }

                        $dtInicio = Carbon::createFromFormat('d/m/Y', $formulario['dtInicio']);
                        //verifica se data fim do formulario é maior que o calculo feito acima
                        if ($dtFim->toDateString() > $incioAdd) {
                            return redirect()
                                ->back()
                                ->with('error', 'O FIM DO TCE('.$dtFim->format('d/m/Y').') É DEPOIS DO FIM DO CURSO('.$incioAdd->format('d/m/Y').'). Linha 805.')
                                ->withInput();
                        } else {
                            //Verifica se tem tce com a mesma concedente e entra no calculo de limite
                            if ($getEstudante->tce->where('concedente_id', $formulario['concedente_id'])->whereNotIn('id', $tce_id)->all()) {
                                $dtInicio = Carbon::createFromFormat('d/m/Y', $formulario['dtInicio']);
                                $dataTotTrabalhado = 0;
                                foreach ($getEstudante->tce->where('concedente_id', $formulario['concedente_id'])->whereNotIn('id', $tce_id) as $tces) {
                                    //Pegando a diferença de tempo entre o cancelamento e o incio dos tces
                                    $dataTotTrabalhado += $tces->dtInicio->diffInMonths($tces->dtCancelamento);
                                }
                                //pega diferença em meses da dtInicio do formulario e dtFim
                                $difInicioFim = $dtInicio->diffInMonths($dtFim);
                                //Soma total trabalhado em todos tces encontrados com difInicioFim
                                $totalFinal = $dataTotTrabalhado + $difInicioFim;

                                if ($totalFinal > 24) {//Verificando dif fim com limite
                                    return redirect()
                                        ->back()
                                        ->with('error', 'ERRO, O total de meses permitido nesta concedente é de 24 Meses. O Estagiario já teve TCE desta Concedente. | Total de '.$totalFinal.' Meses. Linha 824.')
                                        ->withInput();
                                } else {
                                    //Atualizando o TCE
                                    $atualizar = $tce->update($formulario);
                                }
                            } else { //não houve concedente
                                $dtInicio = Carbon::createFromFormat('d/m/Y', $formulario['dtInicio']);
                                //Pegando a diferença de tempo entre o cancelamento e o incio
                                $dataTotPeriodo = $dtInicio->diffInMonths($dtFim);

                                if ($dataTotPeriodo > 24) {//Verificando dif fim com limite
                                    //dd('ERRO, A Data Limite é maior | Total é de - ' . $dataTotPeriodo . ' Meses');
                                    return redirect()
                                        ->back()
                                        ->with('error', 'ERRO, O Periodo maximo é de 24 Meses | Totalizou '.$dataTotPeriodo.' Meses. Linha 842.')
                                        ->withInput();
                                } else {
                                    //Atualizando o TCE
                                    $atualizar = $tce->update($formulario);
                                }//Fim else > 24
                            }
                        }//fim verificação $totEstudante > $totCurso

                        break;
                    /*
                    default:
                        //verificando se existe tce com o mesmo concedente
                        if ($getEstudante->tce && $getEstudante->tce->concedente_id == $formulario['concedente_id']) {

                            //Pegando a diferença de tempo entre o cancelamento e o incio
                            $dataTotTrabalhado = $getEstudante->tce->dtInicio->diffInMonths($getEstudante->tce->dtCancelamento);
                            //Verificando se a dataFim é maior do a Limite e mair que 24
                            $difInicioFim = $getEstudante->tce->dtInicio->diffInMonths($dtFim);
                            //Somando o total de meses que tem entre Inicio e Cancelamento + Total de meses do Formulario
                            $totalFinal = $dataTotTrabalhado + $difInicioFim;

                            if ($totalFinal >= 24) {//Verificando dif fim com limite
                                //dd('ERRO, A Data Limite é maior | Total é de - ' . $totalFinal . ' Meses');
                                return redirect()
                                    ->back()
                                    ->with('error', 'ERRO, A Data Limite é maior | Total é de - ' . $totalFinal . ' Meses')
                                    ->withInput();

                            } else {
                                //Atualizando o TCE
                                $atualizar = $tce->update($formulario);
                            }


                        } else { //não houve concedente

                            //Pegando a diferença de tempo entre o cancelamento e o incio
                            $dataTotPeriodo = $dtInicio->diffInMonths($dtFim);

                            if ($dataTotPeriodo > 24) {//Verificando dif fim com limite
                                //dd('ERRO, A Data Limite é maior | Total é de - ' . $dataTotPeriodo . ' Meses');
                                return redirect()
                                    ->back()
                                    ->with('error', 'ERRO, A Data Limite é maior | Total é de - ' . $dataTotPeriodo . ' Meses')
                                    ->withInput();
                            } else {
                                //Atualizando o TCE
                                $atualizar = $tce->update($formulario);
                            }//Fim else > 24

                        }

                    BREAK;
                    */
                }
                //1 ANO / 2 SEMESTRE
            } elseif ($fimContrato->toDateString() < $dtFim->toDateString()) {// a data do contrato é finaliza antes
                //$fimContrato = Carbon::createFromFormat('Y-m-d', $getConcedente->dtFimContrato);
                return redirect()
                    ->back()
                    ->with('error', 'O FIM DO CONTRATO COM A CONCEDENTE ('.$fimContrato->format('d/m/Y').') É ANTES DO FIM DO TCE ('.$dtFim->format('d/m/Y').'). Linha 905.')
                    ->withInput();
            }
            //-----//FIM. VERIFICANDO INSERT//---//
        } catch (Exception $e) {
            DB::rollback();
            echo $e->getMessage();
        }
        DB::commit();

        if ($atualizar) {
            $tce->update(['nnSemestreAno' => $qtdEstudante]);

            if (isset($formulario['atividades'])) {
                //atualizar atividades selecionados para este tce
                $tce->atividades()->sync(array_values($formulario['atividades']));
            }

            return redirect()
                ->route('tce.show', ['tce' => $tce_id])
                ->with('success', 'TCE Atualizado com Sucesso!');
        } else {
            return redirect()
                ->back()
                ->with('error', 'Falha ao Atualizar!');
        }
    }

    public function destroy(Tce $tce)
    {
    }

    public function showRelatorio($idTce, $idRelatorio)
    {
        $idTce = base64_decode($idTce);
        $tce = $this->tce->findOrFail($idTce);
        $relatorio = Relatorio::findOrFail($idRelatorio);
        switch ($relatorio->id) {
            case $relatorio->id:
                //dd($relatorio->nmRelatorio);
                //return view('tce.relatorio.' . $relatorio->dsUrl. 'Div', compact('tce'));

               // $pdf = app('dompdf.wrapper');
                //$pdf->getDomPDF()->set_option("enable_php", true);
                //$pdf->loadView('your.view.here', $data);

               if ($relatorio->dsUrl == 'mtpac') {
                   return view('painel.concedente.modelos.'.$relatorio->dsUrl, compact('tce'));
               } else {
                   $pdf = PDF::loadView('painel.concedente.modelos.'.$relatorio->dsUrl, compact('tce'))
                    ->setPaper('a4')
                    ->setOptions(['dpi' => 100, 'enable_php' => true]);

                   return $pdf->stream();
               }

                //return view('tce.relatorio.' . $relatorio->dsUrl. 'Div', compact('tce'));
            break;
        }
    }

    ////////
    public function cancelar($tce_id)
    {
        $titulo = 'Cancelamento de Tce';
        $hoje = Carbon::today()->format('d/m/Y');
        $tce = $this->tce->findOrFail($tce_id);
        $motivosCancelamento = $this->motivoCancelamento->all();

        return view('tce.cancelar', compact('titulo', 'tce', 'motivosCancelamento', 'hoje'));
    }

    public function cancel(Request $request, $tce_id)
    {
        DB::beginTransaction();
        try {
            //Pegando o TCE
            $tce = $this->tce->findOrFail($tce_id);
            $formulario = $request->all();
            $request->validate([
                'dtCancelamento' => 'required',
                'motivoCancelamento_id' => 'required',
            ]);
            //Auth::user()->name
            //Pega ususario logado pra setar o ususario que cancelou já nos campos para update
            //$usuario = Auth::user()->id;
            $formulario['usuarioCancelamento_id'] = Auth::user()->id;
            $formulario['cancelado_at'] = Carbon::now()->toDateTimeString();
            //dd($formulario);
            //Atualizando o TCE para cancelado
            $atualizar = $tce->update($formulario);
        } catch (Exception $e) {
            DB::rollback();
            echo $e->getMessage();
        }
        DB::commit();

        if ($atualizar) {
            //Atualizando campo de TCE na tabela estudante
            Estudante::where(['id' => $tce->estudante_id])->update(['tce_id' => null]);

            return redirect()
                ->route('tce.show', ['tce' => $tce_id])
                ->with('success', 'TCE Cancelado com Sucesso!');
        } else {
            return redirect()
                ->back()
                ->with('error', 'Falha ao Cancelar!');
        }
    }

    /////

    public function showRecisao($idTce)
    {
        $tce = $this->tce->findOrFail($idTce);
        //pegando o ultimo aditivo de data para colocar sua data fim na recisão
        $getAditivo = $tce->aditivos->where('dtInicio', '<>', null)->last();
        //return view('tce.relatorio.recisao', compact('tce','getAditivo'));
        //dd($getAditivos->id);

        $pdf = PDF::loadView('tce.relatorio.recisao', compact('tce', 'getAditivo'))
                ->setPaper('a4')
                ->setOptions(['dpi' => 100]);

        return $pdf->stream('Recisão TCE '.$tce->id.' '.$tce->estudante->nmEstudante.'.pdf');
    }

    public function showDeclaracao($idTce)
    {
        $tce = $this->tce->findOrFail($idTce);
        //pegando o ultimo aditivo de data para colocar sua data fim na recisão
        $getAditivo = $tce->aditivos->where('dtInicio', '<>', null)->last();
        $dtHoje = Carbon::today()->format('d/m/Y');
        //return view('tce.relatorio.recisao', compact('tce','getAditivo'));
        //dd($getAditivos->id);

        $pdf = PDF::loadView('tce.relatorio.declaracaoCancelado', compact('tce', 'getAditivo', 'dtHoje'))
                ->setPaper('a4')
                ->setOptions(['dpi' => 100]);

        return $pdf->stream('Declaração de estágio '.$tce->id.' - '.$tce->estudante->nmEstudante.'.pdf');
    }

    public function validar()
    {
        $titulo = 'Validação de Tce';
        $hoje = Carbon::today()->format('d/m/Y');

        return view('tce.validar', compact('titulo', 'tce', 'hoje'));
    }

    public function searchValidar(Request $request)
    {
        $titulo = 'Validação de Tce';
        $tce = $this->tce->findOrFail($request->get('tce'));

        return view('tce.validar_form', compact('titulo', 'tce'));
    }

    public function validando(Request $request, $tce_id)
    {
        //dd($request->all(), $tce_id);
        DB::beginTransaction();
        try {
            //Pegando o TCE
            $tce = $this->tce->findOrFail($tce_id);
            $formulario = $request->all();
            //Pega ususario logado pra setar o ususario que validou já no campo para validação
            $formulario['userDt4Via_id'] = Auth::user()->id;
            $formulario['dt4Via_at'] = Carbon::now();
            //Atualizando o TCE para validado
            $atualizar = $tce->update($formulario);
        } catch (Exception $e) {
            DB::rollback();
            echo $e->getMessage();
        }
        DB::commit();

        if ($atualizar) {
            return redirect()
                ->route('tce.validar')
                ->with('success', 'TCE '.$tce->id.' Validado com Sucesso!');
        } else {
            return redirect()
                ->back()
                ->with('error', 'Falha ao Validar Tce, tente novamente! Se o erro persistir contate o Suporte de TI.');
        }
    }

    //teste server side
    public function allTces(Request $request)
    {
        $columns = array(
                            0 => 'migracao',
                            1 => 'tce',
                            2 => 'estudante',
                            3 => 'concedente',
                            4 => 'inicio',
                            5 => 'fim',
                        );
        if (Auth::user()->unidade_id != null) {
            $relEstado = Auth::user()->unidade->estado->pluck('id')->toArray();
            $tces = $this->tce->where('dtCancelamento', null)
                ->orderBy('id', 'desc')
                ->WhereIn('estado_id', $relEstado)
                ->get();
        } else {
            $tces = $this->tce->where('dtCancelamento', null)->orderBy('id', 'desc')
            ->get();
        }
        $totalData = $tces->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            if (Auth::user()->unidade_id != null) {
                $relEstado = Auth::user()->unidade->estado->pluck('id')->toArray();
                $tces = $this->tce->offset($start)
                    ->limit($limit)
                    ->where('dtCancelamento', null)
                    ->WhereIn('estado_id', $relEstado)
                    ->orderBy($order, $dir)
                    ->get();
            } else {
                $tces = $this->tce->offset($start)
                ->limit($limit)
                ->where('dtCancelamento', null)
                ->orderBy($order, $dir)
                ->get();
            }
        } else {
            $search = $request->input('search.value');

            $tcesCancelados = $this->tce->where('dtCancelamento', '<>', null)
            ->WhereIn('estado_id', $relEstado)
            ->orWhere('title', 'LIKE', "%{$search}%")
            ->offset($start)
            ->limit($limit)
            ->orderBy($order, $dir)
            ->whereHas('concedente', function ($query) use ($search) {
                $query->where('nmRazaoSocial', 'LIKE', '%'.$search.'%');
            })->get();
            $posts = Post::where('id', 'LIKE', "%{$search}%")
                            ->orWhere('title', 'LIKE', "%{$search}%")
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order, $dir)
                            ->get();

            $totalFiltered = Post::where('id', 'LIKE', "%{$search}%")
                             ->orWhere('title', 'LIKE', "%{$search}%")
                             ->count();
        }

        $data = array();
        if (!empty($posts)) {
            foreach ($posts as $post) {
                $show = route('posts.show', $post->id);
                $edit = route('posts.edit', $post->id);

                $nestedData['id'] = $post->id;
                $nestedData['title'] = $post->title;
                $nestedData['body'] = substr(strip_tags($post->body), 0, 50).'...';
                $nestedData['created_at'] = date('j M Y h:i a', strtotime($post->created_at));
                $nestedData['options'] = "&emsp;<a href='{$show}' title='SHOW' ><span class='glyphicon glyphicon-list'></span></a>
                                          &emsp;<a href='{$edit}' title='EDIT' ><span class='glyphicon glyphicon-edit'></span></a>";
                $data[] = $nestedData;
            }
        }

        $json_data = array(
                    'draw' => intval($request->input('draw')),
                    'recordsTotal' => intval($totalData),
                    'recordsFiltered' => intval($totalFiltered),
                    'data' => $data,
                    );

        echo json_encode($json_data);
    }
}
