<?php

namespace App\Http\Controllers;

use App\Models\Estado;
use App\Models\Unidade;
use Illuminate\Http\Request;
use DB;

class UnidadeController extends Controller
{
    /**
     * @var Unidade
     */
    private $unidade;
    private $estado;

    public function __construct(Unidade $unidade, Estado $estado)
    {
        $this->unidade  = $unidade;
        $this->estado = $estado;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Unidades = $this->unidade->paginate(config('sysConfig.Paginacao'));
        return view('unidade.index',compact('Unidades'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Estado = $this->estado->all();
        return view('unidade.cadastro', compact('Estado'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $formulario = $request->all();
            //Adicionando o Unidade ao bd
            $Unidade = $this->unidade->create($formulario);
            ///Inserindo na tabela de relacionamento unidades_estados///
            $Unidade->estado()->sync($formulario['estado_id'],true);
            ///FIM. Inserindo na tabela de relacionamento unidades_estados///

        } catch (Exception $e) {
            DB::rollback();
            echo $e->getMessage();
        }
        DB::commit();

        if ($Unidade) {
            return redirect()
                //Redireciona para o cadastro da instituicao quando a mesma é cadastrada.
                ->route('unidade.show', ['uniade' => $Unidade->id])
                //
                ->with('Unidadesuccess', 'Unidade Cadastrado com Sucesso!');
        } else {
        return redirect()
            ->back()
            ->with('Unidadeerror', 'Falha ao Cadastrar Unidade!')
            ->withInput();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Unidade = Unidade::findOrFail($id);
        return view('unidade.visualizar', compact('Unidade'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Unidade = Unidade::findOrFail($id);
        $Estado = $this->estado->all();
        return view('unidade.editar', compact('Unidade','Estado'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $formulario = $request->all();
            $Unidade = Unidade::findOrFail($id);
            //Adicionando o Unidade ao bd
            $Unidade->update($formulario);
            ///Inserindo na tabela de relacionamento unidades_estados///
            $Unidade->estado()->sync($formulario['estado_id'],true);
            ///FIM. Inserindo na tabela de relacionamento unidades_estados///

        } catch (Exception $e) {
            DB::rollback();
            echo $e->getMessage();
        }
        DB::commit();

        if ($Unidade) {
            return redirect()
                //Redireciona para o cadastro da unidade quando a mesma é cadastrada.
                ->route('unidade.show', ['unidade' => $Unidade->id])
                //
                ->with('Unidadesuccess', 'Unidade gerado com Sucesso!');
        } else {
            return redirect()
                ->back()
                ->with('Unidadeerror', 'Falha ao Gerar Unidade!')
                ->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function search(Request $request)
    {
        // dd($request->all());
        $dataForm = $request->except('_token');
        $Usuarios = $this->usuario->search($request->key_search, config('sysConfig.Paginacao'));
        return view('usuario.index', compact('Usuarios', 'dataForm'));

    }
}
