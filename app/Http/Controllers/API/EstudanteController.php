<?php

namespace App\Http\Controllers\API;

use App\Models\Estudante;
use App\Models\UserEstudante;
use App\Models\UserInteressado;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EstudanteController extends Controller
{
    /**
     * Validate if exist estudante by CPF
     *
     * @return \Illuminate\Http\Response
     */
    public function verifyCPF($cpf)
    {
        $estudante = Estudante::where('cdCPF',$cpf)->first();
        if($estudante){
          $user = UserEstudante::where('estudante_id',$estudante->id)->first();
          //$interessado = UserInteressado::where('estudante_id',$estudante->id)->first();
          if(!$user){
            return response()->json([
              'status' => true, 'nmEstudante' => $estudante->nmEstudante
            ], JSON_UNESCAPED_UNICODE);
          /*}else if(!$interessado){
            return response()->json([
              'status' => true, 'nmEstudante' => $estudante->nmEstudante
            ], JSON_UNESCAPED_UNICODE);
            */
          }else{
            return response()->json([
              'status' => false
            ], JSON_UNESCAPED_UNICODE);
          }
        }
        return response()->json([
          'status' => null
        ], JSON_UNESCAPED_UNICODE);
    }

    public function verifydtNascimento($cpf, $dtNascimento)
    {
        $estudante = Estudante::all()->where('cdCPF',$cpf)->first();
        if($estudante){
          $user = UserEstudante::all()->where('estudante_id',$estudante->id)->first();
          if(!$user){
            if($estudante->dtNascimento->format('d/m/Y') == (\Carbon\Carbon::parse($dtNascimento)->format('d/m/Y'))){
              return response()->json([
                'status' => true
              ], JSON_UNESCAPED_UNICODE);
            }else{
              return response()->json([
                'status' => 'error',
                'msg' => 'Data de nascimento informada diverge da data registrada'
              ], JSON_UNESCAPED_UNICODE);
            }
          }else{
            return response()->json([
              'status' => false
            ], JSON_UNESCAPED_UNICODE);
          }
        }
        return response()->json([
          'status' => null
        ], JSON_UNESCAPED_UNICODE);
    }
}
