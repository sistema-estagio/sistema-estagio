<?php

namespace App\Http\Controllers\API;

use App\Models\FolhaPagamento;
use App\Models\FolhaPagamentoItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Supervisor;
use App\Models\Estado;
use App\Models\Cidade;
use App\Models\UserConcedente;
use App\Models\SecConcedente;
use App\Models\Tce;
use Auth;
use Carbon\Carbon;
use Validator;
use PDF;
use Illuminate\Support\Collection;

class FinanceiroController extends Controller
{
  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {
    $this->middleware('auth:web');
  }

  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function getAtivos($idConcedente, $id, $folha)
  {
    //
    $folha = FolhaPagamento::findOrFail($folha);
    if($folha){
      //pega lista com os ids dos tces que estão na folha até o momento, pra conseguir tira-los das proximas listagens
      $iFolha = FolhaPagamentoItem::where('folha_id',$folha->id)->pluck('tce_id')->toArray();
      $ativos = new Collection;
      $ativo = [];
      //Lista com os TCES ativos sem os que já estão na folha
      $tcesAtivos = Tce::all()->sortBy(function($tce){
        //Definir qual coluna vai ser usada na ordenação, evidenciando o UTF8 pra nomes iniciados com letras acentuadas
        return iconv('UTF-8', 'ASCII//TRANSLIT', $tce->estudante->nmEstudante);
      })
      ->where('dtCancelamento',NULL)
      ->where('sec_conc_id',$id)
      ->whereNotIn('id', $iFolha);
      //
      foreach($tcesAtivos as $tce){
        $ativo['id'] = $tce->id;
        $ativo['relatorio_id'] = $tce->tipo_tce;
        $ativo['nmEstudante'] = $tce->estudante->nmEstudante;

        if(\Carbon\Carbon::parse($tce->dtInicio)->year === \Carbon\Carbon::now()->year && abs((\Carbon\Carbon::parse($tce->dtInicio)->month - \Carbon\Carbon::now()->month)) <= 1){
          $ativo['diasTrabalhados'] = abs((\Carbon\Carbon::parse($tce->dtInicio)->day - \Carbon\Carbon::parse($tce->dtInicio)->daysInMonth));
          $ativo['diasBaseEstagiados'] = abs((\Carbon\Carbon::parse($tce->dtInicio)->day - \Carbon\Carbon::parse($tce->dtInicio)->daysInMonth));
        }elseif(\Carbon\Carbon::parse($tce->dtFim)->year === \Carbon\Carbon::now()->year && abs((\Carbon\Carbon::parse($tce->dtFim)->month - \Carbon\Carbon::now()->month)) < 1){
          $ativo['diasTrabalhados'] = abs((\Carbon\Carbon::parse($tce->dtFim)->day - \Carbon\Carbon::parse($tce->dtFim)->daysInMonth));
          $ativo['diasBaseEstagiados'] = abs((\Carbon\Carbon::parse($tce->dtFim)->day - \Carbon\Carbon::parse($tce->dtFim)->daysInMonth));
        }else{
          $ativo['diasTrabalhados'] = 22;
          $ativo['diasBaseEstagiados'] = 30;
        }

        if($tce->aditivos->count() > 0 && $tce->aditivos()->latest()->first()->vlAuxilioMensal != Null){
          $ativo['vlAuxilioMensal'] = number_format($tce->aditivos()->latest()->first()->vlAuxilioMensal, 2, ',', '.');
        }else{
          $ativo['vlAuxilioMensal'] = number_format($tce->vlAuxilioMensal, 2, ',', '.');
        }
        $ativos->push($ativo);
      }
    }


    return response()->json($ativos);
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function getCancelados($idConcedente, $id, $folha)
  {
    //

    //Lista com os Tces Cancelados sem os que já estão na folha
    $tcesCancelados = Tce::whereNotNull('dtCancelamento')
    ->where('sec_conc_id',$id)
    ->orderBy('dtCancelamento', 'desc')
    ->whereNotIn('id', $iFolha)
    ->get();
    //dd($tcesCancelados);
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function editItem(Request $request)
  {
    //
    $itemFolha = FolhaPagamentoItem::find($request['item']['id']);
    if($itemFolha){
      $itemR = $request['item'];
      $formulario = $request['item'];

      $formulario['hash'] = base64_encode($itemFolha->folha->referenciaMes.$itemFolha->folha->referenciaAno);
      // $formulario['dtPagamento'] = FolhaPagamentoItem::setDtPagamentoAttribute($formulario['dtPagamento']);
      $formulario['dtPagamento'] = null;
      $formulario['descontoVlAuxMensal'] = $itemR['descontoVlAuxMensal'];
      $formulario['descontoVlAuxTransporte'] = $itemR['descontoVlAuxTransporte'];
      if($formulario['vlAuxilioTransporte'] === $itemFolha->tce->vlAuxilioTransporte){
        $formulario['vlAuxilioTransporteReceber'] = ($itemR['vlAuxilioTransporte'] / 30) * $itemR['diasBaseEstagiados'];
      }else{
        $formulario['vlAuxilioTransporteReceber'] = $itemR['vlAuxilioTransporte'];
      }
      $itemR['vlRecessoTransp'] = number_format($itemR['diasRecesso'] * ($itemR['vlAuxilioTransporte'] / 30), 2, '.', '');

      $formulario['descontoVlAuxTransporte'] = $formulario['descontoVlAuxTransporte'];
      $formulario['vlAuxilioMensalReceber'] = number_format($itemR['vlAuxilioMensal'], 2, '.', '');
      $formulario['vlAuxilioMensal'] = $itemFolha->tce->vlAuxilioMensal;
      Tce::find($itemFolha->tce_id)->update([
        'centroDeCusto' => $itemR['centroDeCusto']
      ]);
      $formulario['vlTotal'] = $itemR['vlTotal'];

      $hash =  base64_encode($itemFolha->folha->referenciaMes.$itemFolha->folha->referenciaAno);
      if($itemFolha->secConcedente_id != null){
        $teste = FolhaPagamentoItem::whereNotIn('id', [$request['id']])
        ->where('secConcedente_id', $itemFolha->secConcedente_id)
        ->where('hash', $hash)
        ->where('folha_id', $itemFolha->folha_id)
        ->where('tce_id', $itemFolha->tce_id)
        ->get();
      }else{
        $teste = FolhaPagamentoItem::whereNotIn('id', [$request['id']])
        ->where('concedente_id', $itemFolha->concedente_id)
        ->where('hash', $hash)
        ->where('folha_id', $itemFolha->folha_id)
        ->where('tce_id', $itemFolha->tce_id)
        ->get();
      }

      //dd($teste->count());
      if($teste->count() == 0){
        $atualizar = $itemFolha->update($formulario);
        if($atualizar){
          $msg = [
            'status' => 'success',
            'msg' => 'Item Editado da folha com Sucesso!'
          ];
          $itemFolha = FolhaPagamentoItem::find($request['item']['id']);
          $item = [];
          $item['id'] = $itemFolha->id;
          $item['is_block'] = $itemFolha->is_block;
          $item['nmEstudante'] = $itemFolha->tce->estudante->nmEstudante;
          $item['diasRecesso'] = $formulario['diasRecesso'];
          $item['diasBaseEstagiados'] = $formulario['diasBaseEstagiados'];
          $item['diasTrabalhados'] = $itemFolha->diasTrabalhados;
          $item['faltas'] = $formulario['faltas'];
          $item['justificadas'] = $formulario['justificadas'];
          $item['vlAuxilioMensalReceber'] = $formulario['vlAuxilioMensalReceber'];
          $item['vlAuxilioTransporteReceber'] = $formulario['vlAuxilioTransporte'];
          $item['descontoVlAuxTransporte'] = $formulario['descontoVlAuxTransporte'];
          $item['descontoVlAuxMensal'] = $formulario['descontoVlAuxMensal'];
          $item['centroDeCusto'] = $formulario['centroDeCusto'];
          $item['vlTotal'] = $itemFolha['vlTotal'];
          return response()
          ->json(['msg'=>$msg, 'item'=> $item]);
        }else{
          $msg = [
            'status' => 'error',
            'msg' => 'Falha ao Cadastrar!'
          ];
          return response()
          ->json(['msg'=>$msg]);
        }
      } else {
        $msg = [
          'status' => 'error',
          'msg' => 'Falha ao Cadastrar!'
        ];
        return response()
        ->json(['msg'=>$msg]);
      }
    }
    $msg = [
      'status' => 'error',
      'msg' => 'Item inexistente!'
    ];
    return response()->json(['msg'=> $msg]);
  }

  public function itemsStore(Request $request)
  {
    $success = array();
    $errors = array();
    $invalid = array();

    if(count($request['items']) > 0){
      for($i = 0; $i < count($request['items']);$i++){

        $validar = Validator::make($request['items'][$i], [
          'diasBaseEstagiados' => 'required',
        ]);

        if($validar->fails()){
          array_push($invalid,$request['items'][$i]['id']);
        }else{
          try{
            $tce = Tce::find($request['items'][$i]['id']);
            $formulario['vlTotal'] = 0;
            $formulario['referenciaMes'] = $request['items'][$i]['referenciaMes'];
            $formulario['referenciaAno'] = $request['items'][$i]['referenciaAno'];
            $formulario['diasBaseEstagiados'] = $request['items'][$i]['diasBaseEstagiados'];
            $formulario['diasTrabalhados'] = 0;
            $formulario['diasRecesso'] = $request['items'][$i]['diasRecesso'];
            $formulario['vlAuxilioTransporteDia'] = $request['items'][$i]['vlAuxilioTransporteDia'];
            $formulario['vlAuxilioTransporteReceber'] = $request['items'][$i]['vlAuxilioTransporteReceber'];
            $formulario['vlAuxilioMensal'] = (float) $request['items'][$i]['vlAuxilioMensal'];
            $formulario['vlAuxilioMensalReceber'] = $request['items'][$i]['vlAuxilioMensalReceber'];
            $formulario['faltas'] = $request['items'][$i]['faltas'];
            $formulario['justificadas'] = $request['items'][$i]['justificadas'];
            $formulario['folha_id'] = $request['folha'];
            $formulario['tce_id'] = $tce->id;
            $formulario['concedente_id'] = $tce->concedente_id;
            $formulario['user_id'] = Auth::user()->id;
            $formulario['hash'] = base64_encode($formulario['referenciaMes'].$formulario['referenciaAno'].$tce->secConcedente_id);
            $formulario['descontoVlAuxTransporte'] = 0;
            $formulario['descontoVlAuxMensal'] = 0;
            //
            if($formulario['vlAuxilioMensal'] != 0 || $formulario['vlAuxilioMensal'] != 0){
              if($formulario['diasRecesso'] != NULL){
                $formulario['vlRecesso'] = (float) $formulario['diasRecesso'] * number_format($formulario['vlAuxilioTransporteDia'], 2, '.', '');
                $vlRecesso = $formulario['vlRecesso'];
              } else {
                $vlRecesso = 0;
              }

              $vlAuxilioTransporteReceber = $formulario['vlAuxilioTransporteReceber'];

              $formulario['descontoVlAuxTransporte'] = 0;
              $formulario['descontoVlAuxMensal'] = 0;
              if($formulario['faltas'] > 0){
                $formulario['descontoVlAuxTransporte'] = number_format(($formulario['faltas'] + $formulario['justificadas']) * $formulario['vlAuxilioTransporteDia'], 2, '.', '');
                $formulario['descontoVlAuxMensal'] = number_format(($formulario['vlAuxilioMensal'] / $formulario['diasBaseEstagiados']) * $formulario['faltas'], 2, '.', '');
              }

              if($formulario['justificadas'] > 0){
                $formulario['descontoVlAuxTransporte'] = $formulario['descontoVlAuxTransporte'] + abs(number_format($formulario['justificadas'] * $formulario['vlAuxilioTransporteDia'], 2, '.', ''));
              }

              $formulario['vlAuxilioMensalReceber'] = number_format(($formulario['vlAuxilioMensal'] / 30) * $formulario['diasBaseEstagiados'], 2, '.', '') - $formulario['descontoVlAuxMensal'];

              $desc = ($formulario['descontoVlAuxTransporte'] + $formulario['descontoVlAuxMensal']);

              $formulario['vlTotal'] = abs(($vlRecesso + $vlAuxilioTransporteReceber + $formulario['vlAuxilioMensalReceber']) - $desc);

            }

            $hash =  base64_encode($formulario['referenciaMes'].$formulario['referenciaAno']);
            $teste = FolhaPagamentoItem::where('secConcedente_id', $tce->secConcedente_id)
            ->where('hash', $hash)
            ->where('folha_id', $request['folha_id'])
            ->where('tce_id', $tce->id)
            ->get();
            if($teste->count() == 0){
              if(FolhaPagamentoItem::create($formulario)){
                array_push($success,$tce->id);
              }else{
                array_push($errors,['tce_id'=>$tce->id, 'error'=>'Não foi possível adicionar o tce 1']);
              }
            }else{
              array_push($errors,['tce_id'=>$tce->id, 'error'=>'Não foi possível adicionar o tce 2']);
            }
          }catch(\Exception $e){
            $e->tce_id = $tce->id;
            array_push($errors, ['e'=>$e->getMessage().' /line: '.$e->getLine(), 'tce_id'=>$tce->id, 'error'=>'Não foi possível adicionar o tce']);
          }
        }
      }
      return response()->json(['result'=>true, 'success'=>$success, 'errors'=>$errors, 'invalid'=>$invalid]);
    }
    return response()->json(['result'=>false, 'success'=>$success, 'errors'=>$errors, 'invalid'=>$invalid]);
  }

  public function itemsRemove(Request $request, $id)
  {
    $folha = FolhaPagamento::where('hash',$id)->get()->first();
    if($folha){
      if($folha->fechado == "N"){
        if(count($request['items']) > 0){
          for($i = 0; $i < count($request['items']);$i++){
            FolhaPagamentoItem::destroy($request['items'][$i]);
          }
          return response()->json(['result'=>true]);
        }
      }
    }
    return response()->json(['result'=>false]);
  }
}
