<?php

namespace App\Http\Controllers\API;

use App\Models\UserConcedente;
use App\Models\Supervisor;
use App\Models\FolhaPagamento;
use App\Models\FolhaPagamentoItem;
use Carbon\Carbon;
use App\Models\Concedente;
use App\Models\Estado;
use App\Models\Cidade;
use App\Models\Tce;
use Illuminate\Support\Facades\Response;
use App\Models\SecConcedente;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Auth;

class ConcedenteController extends Controller
{
  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {
    $this->middleware('auth:web');
  }

  public function itemsStore(Request $request)
  {
    $success = array();
    $errors = array();
    $invalid = array();

    if(count($request['items']) > 0){
      for($i = 0; $i < count($request['items']);$i++){
        $validar = Validator::make($request['items'][$i], [
          'diasBaseEstagiados' => 'required',
          'diasTrabalhados' => 'required',
        ]);

        if($validar->fails()){
          array_push($invalid,$request['items'][$i]['id']);
        }else{
          try{
            $tce = Tce::find($request['items'][$i]['id']);
            $formulario['vlTotal'] = 0;
            $formulario['referenciaMes'] = $request['items'][$i]['referenciaMes'];
            $formulario['referenciaAno'] = $request['items'][$i]['referenciaAno'];
            $formulario['diasBaseEstagiados'] = $request['items'][$i]['diasBaseEstagiados'];
            $formulario['diasTrabalhados'] = $request['items'][$i]['diasTrabalhados'];
            $formulario['diasRecesso'] = $request['items'][$i]['diasRecesso'];
            $formulario['vlAuxilioTransporteDia'] = $request['items'][$i]['vlAuxilioTransporteDia'];
            $formulario['vlAuxilioTransporteReceber'] = $request['items'][$i]['vlAuxilioTransporteReceber'];
            $formulario['vlAuxilioMensal'] = (float) $request['items'][$i]['vlAuxilioMensal'];
            $formulario['vlAuxilioMensalReceber'] = $request['items'][$i]['vlAuxilioMensalReceber'];
            $formulario['faltas'] = $request['items'][$i]['faltas'];
            $formulario['justificadas'] = $request['items'][$i]['justificadas'];
            $formulario['folha_id'] = $request['folha'];
            $formulario['tce_id'] = $tce->id;
            $formulario['concedente_id'] = $tce->concedente_id;
            $formulario['user_id'] = Auth::user()->id;
            $formulario['hash'] = base64_encode($formulario['referenciaMes'].$formulario['referenciaAno'].$tce->secConcedente_id);
            $formulario['descontoVlAuxTransporte'] = 0;
            $formulario['descontoVlAuxMensal'] = 0;
            //
            if($formulario['vlAuxilioMensal'] != 0 || $formulario['vlAuxilioMensal'] != 0){
              if($formulario['diasRecesso'] != NULL){
                $formulario['vlRecesso'] = (float) $formulario['diasRecesso'] * number_format($formulario['vlAuxilioTransporteDia'], 2, '.', '');
                $vlRecesso = $formulario['vlRecesso'];
              } else {
                $vlRecesso = 0;
              }

              $vlAuxilioTransporteReceber = $formulario['vlAuxilioTransporteReceber'];

              $formulario['descontoVlAuxTransporte'] = 0;
              $formulario['descontoVlAuxMensal'] = 0;
              if($formulario['faltas'] > 0){
                $formulario['descontoVlAuxTransporte'] = number_format(($formulario['faltas'] + $formulario['justificadas']) * $formulario['vlAuxilioTransporteDia'], 2, '.', '');
                $formulario['descontoVlAuxMensal'] = number_format(($formulario['vlAuxilioMensal'] / $formulario['diasBaseEstagiados']) * $formulario['faltas'], 2, '.', '');
              }

              if($formulario['justificadas'] > 0){
                $formulario['descontoVlAuxTransporte'] = $formulario['descontoVlAuxTransporte'] + abs(number_format($formulario['justificadas'] * $formulario['vlAuxilioTransporteDia'], 2, '.', ''));
              }

              $formulario['vlAuxilioMensalReceber'] = number_format(($formulario['vlAuxilioMensal'] / 30) * $formulario['diasBaseEstagiados'], 2, '.', '') - $formulario['descontoVlAuxMensal'];

              $desc = ($formulario['descontoVlAuxTransporte'] + $formulario['descontoVlAuxMensal']);

              $formulario['vlTotal'] = abs(($vlRecesso + $vlAuxilioTransporteReceber + $formulario['vlAuxilioMensalReceber']) - $desc);

            }

            $hash =  base64_encode($formulario['referenciaMes'].$formulario['referenciaAno']);
            $teste = FolhaPagamentoItem::where('secConcedente_id', $tce->secConcedente_id)
            ->where('hash', $hash)
            ->where('folha_id', $request['folha_id'])
            ->where('tce_id', $tce->id)
            ->get();
            if($teste->count() == 0){
              if(FolhaPagamentoItem::create($formulario)){
                array_push($success,$tce->id);
              }else{
                array_push($errors,['tce_id'=>$tce->id, 'error'=>'Não foi possível adicionar o tce 1']);
              }
            }else{
              array_push($errors,['tce_id'=>$tce->id, 'error'=>'Não foi possível adicionar o tce 2']);
            }
          }catch(\Exception $e){
            $e->tce_id = $tce->id;
            array_push($errors, ['e'=>$e->getMessage().' /line: '.$e->getLine(), 'tce_id'=>$tce->id, 'error'=>'Não foi possível adicionar o tce']);
          }
        }
      }
      return response()->json(['result'=>true, 'success'=>$success, 'errors'=>$errors, 'invalid'=>$invalid]);
    }
    return response()->json(['result'=>false, 'success'=>$success, 'errors'=>$errors, 'invalid'=>$invalid]);
  }

  public function itemsRemove(Request $request)
  {
    if(count($request['items']) > 0){
      for($i = 0; $i < count($request['items']);$i++){
        FolhaPagamentoItem::destroy($request['items'][$i]);
      }
      return response()->json(['result'=>true]);
    }
    return response()->json(['result'=>false]);
  }
}
