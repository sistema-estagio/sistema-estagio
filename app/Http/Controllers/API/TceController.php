<?php

namespace App\Http\Controllers\API;

use App\Models\Tce;
use App\Models\SecConcedente;
use App\Models\User;
use App\Models\Concedente;
use App\Models\MotivoCancelamento;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Auth;
use Validator;
use DB;

class TceController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function cancelLote(Request $request)
    {
        $tces = $request['tces'];
        $validate = Validator::make($request->all(), [
      'dtCancelamento' => 'required',
      'motivoCancelamento_id' => 'required',
      ]);

        if ($validate->fails()) {
            return response()->json(['status' => 'error']);
        }

        foreach ($tces as $id) {
            DB::beginTransaction();
            try {
                //Pegando o TCE
                $tce = Tce::find($id);
                if ($tce) {
                    $request['usuarioCancelamento_id'] = Auth::id();
                    $request['cancelado_at'] = Carbon::now()->toDateTimeString();

                    $tce->update($request->all());
                }
            } catch (Exception $e) {
                DB::rollback();
            }
            DB::commit();
        }

        return response()->json(['status' => 'success', 'msg' => 'Cancelamento realizado com sucesso']);
    }

    public function getTces(Request $request)
    {
        $tipo = $_GET['tipo'];
        $tces = DB::table('estudantes')
          ->join('tces', 'estudantes.id', '=', 'tces.estudante_id')
          ->orderBy('tces.dtInicio', 'DESC');

        switch ($tipo) {
        case 'ativos':
        $tces = $tces->where('dtCancelamento', null)->where('dt4Via', '!=', null)->take(10)->get();
        foreach ($tces as $tce) {
            if ($tce->sec_conc_id != null) {
                $tce->nmSecretaria = SecConcedente::find($tce->sec_conc_id)->nmSecretaria;
            } else {
                $tce->nmFantasia = Concedente::find($tce->concedente_id)->nmFantasia;
            }
        }

        return response()->json($tces);
        break;
        case 'cancelados':
        $tces = $tces->where('dtCancelamento', '<>', null)->take(10)->get();

        foreach ($tces as $tce) {
            if ($tce->sec_conc_id != null) {
                $tce->nmSecretaria = SecConcedente::find($tce->sec_conc_id)->nmSecretaria;
            } else {
                $tce->nmFantasia = Concedente::find($tce->concedente_id)->nmFantasia;
            }

            if ($tce->usuarioConcedenteCancelamento_id != null) {
                $tce->name = 'CONCEDENTE';
            } else {
                $tce->name = User::find($tce->usuarioCancelamento_id)->name;
            }
            $tce->dsMotivoCancelamento = MotivoCancelamento::find($tce->motivoCancelamento_id)->dsMotivoCancelamento;
        }

        return response()->json($tces);
        break;
        case 'pendentes':
        $tces = $tces->where('dtCancelamento', null)->where('dt4Via', null);

        return response()->json($tces, 200);

        break;
        case 'vencidos':
        $tces = Tce::getVencidos($concedente_id, $id);

        return $tces;
        break;
        case 'proxVencimento':

        break;
        case 'valMes':

        break;

        default:

        break;
      }
    }
}
