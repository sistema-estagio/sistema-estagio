<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
//use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Models\Estudante;
use App\Models\UserEstudante;
use App\Models\Estado;
use App\Models\Curso;
use App\Models\Cidade;
use App\Models\Instituicao;
use App\Models\CursoInstituicao;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Validator;
use Auth;
use Route;

class EstudanteLoginController extends Controller
{

  public function __construct()
  {
    //$this->middleware('guest:web_estudante')->except(['logout']);
    //$this->middleware('guest:web_estudante', ['except' => ['logout']]);
  }

  public function showLoginForm()
  {
    return view('auth.estudante.login');
    //return 'Login estudante';
  }

  public function showPrimeiroAcessoForm(Request $request)
  {
    $estados = Estado::all();
    $cidades = [];
    $cursos = Curso::all();
    $niveis = [];
    $turnos = [];
    return view('auth.estudante.primeiro-acesso', [
      'estados' => $estados,
      'instituicoes' => $request->session()->get('instituicoes') ?? [],
      'cidades' => $request->session()->get('cidades') ?? [],
      'cursos' => $request->session()->get('cursos') ?? [],
      'niveis' => $request->session()->get('niveis') ?? [],
      'turnos' => $request->session()->get('turnos') ?? []
    ]);
    //return 'Login estudante';
  }

  public function showRegisterForm(Request $request)
  {
    $titulo = "Cadastro de Estudante - Sistema de Estágio - UPA";
    $estados = Estado::all();
    $cidades = [];
    $cursos = Curso::all();
    $niveis = [];
    $turnos = [];
    return View('auth.estudante.register', [
      'estados' => $estados,
      'instituicoes' => $request->session()->get('instituicoes') ?? [],
      'titulo' => $titulo,
      'cidades' => $request->session()->get('cidades') ?? [],
      'cursos' => $request->session()->get('cursos') ?? [],
      'niveis' => $request->session()->get('niveis') ?? [],
      'turnos' => $request->session()->get('turnos') ?? []
    ]);
    //return view('auth.estudante.register');
    //return 'Login estudante';
  }

  public function register(Request $request){
    //dd($request->all());
    $formulario = $request->all();

    $validar= Validator::make($request->all(), [
      'nmEstudante' => 'required',
      'cdRG' => 'required',
      //'cdCPF' => 'required|unique:estudantes|cpf',
      'cdCPF' => 'required|unique:estudantes',
      'dsEstadoCivil' => 'required',
      'dsSexo' => 'required',
      'dtNascimento' => 'required',
      'cdCEP' => 'required',
      'dsEndereco' => 'required',
      'nnNumero' => 'required',
      'nmBairro' => 'required',
      'estado_id' => 'required',
      'cidade_id' => 'required',
      'email' => 'required|unique:users_estudante',
      'password' => 'required|string|min:8',
    ],[
      'nmEstudante.required' => ' O campo Nome é Obrigatório.',
      'cdRG.required' => ' O campo RG é obrigatório.',
      'cdCPF.required' => ' O campo CPF é obrigatório.',
      'cdCPF.unique' => ' Já existe estudante com esse numero de CPF.',
      'cdCPF.cpf' => ' CPF Inválido',
      'email.required' => ' O campo E-MAIL é obrigatorio.',
      'email.unique' => ' Já existe estudante com esse E-MAIL.',
      'password.required' => 'O campo SENHA é obrigatorio.',
      'password.min' => 'Sua senha deve ter no minimo 8 caracteres.',
      'dsEstadoCivil.required' => ' O campo Estado Civil é obrigatório.',
      'dsSexo.required' => ' O campo Gênero é obrigatório.',
      'dtNascimento.required' => ' O campo Data de Nascimento é obrigatória.',
      'cdCEP.required' => ' O campo CEP é obrigatório.',
      'dsEndereco.required' => ' O campo Endereço é obrigatório.',
      'nnNumero.required' => ' O campo Numero é obrigatório.',
      'nmBairro.required' => ' O campo Bairro é obrigatório.',
      'estado_id.required' => ' O campo Estado é obrigatório.',
      'cidade_id.required' => ' O campo Cidade é obrigatório.',
    ]);

    if($validar->fails()) {
      $cidades = Cidade::where('estado_id', $formulario['estado_id'])->orderBy('nmCidade')->get();
      $instituicoes = Instituicao::where('estado_id', $formulario['estado_id'])->orderBy('nmInstituicao')->get();
      $cursos = Instituicao::findOrFail($formulario['instituicao_id']);
      $niveis = DB::table('instituicoes_cursos')
      ->join('cursos', 'instituicoes_cursos.curso_id', '=', 'cursos.id')
      ->join('niveis', 'cursos.nivel_id', '=', 'niveis.id')
      ->select('niveis.id', 'niveis.nmNivel')
      ->where('instituicoes_cursos.id',$formulario['curso_id'])
      ->get();
      $turnos = DB::table('turnos_cursos')
      ->join('turnos', 'turnos_cursos.turno_id', '=', 'turnos.id')
      ->select('turnos.id', 'turnos.nmTurno')
      ->where('turnos_cursos.curso_instituicao_id',$formulario['curso_id'])
      ->orderBy('turnos.id')
      ->get();
      //$this->instituicao->findOrFail($id);
      //$cursos = CursoInstituicao::where('instituicao_id', $formulario['instituicao_id'])->get();
      //dd($formulario);
      return
      //dd($validar, $validar->any());
      redirect()
      ->back()
      //->route('estudante.create')
      ->with('cidades', $cidades)
      ->with('instituicoes', $instituicoes)
      ->with('cursos', $cursos)
      ->with('niveis', $niveis)
      ->with('turnos', $turnos)
      ->withErrors($validar)
      ->withInput();
    }

    $formulario['dsEmail'] = $request['email'];

    // if($request['nmInstituicao'] != null){
    //   $instituicao = Instituicao::create([
    //     'nmInstituicao' => $request['nmInstituicao'],
    //     'nmFantasia' => $request['nmInstituicao'],
    //     'cdCNPJ' => '	99.999.999/9999-99',
    //     'estado_id' => $request['estado_id'],
    //     'cidade_id' => $request['cidade_id'],
    //   ]);
    //
    //   $curso = Curso::where('nmCurso', 'like', '%', $request['nmCurso'], '%')->get();
    //
    //   if($curso){
    //     $curso = Curso::create([
    //       'instituicao_id' => $instituicao->id,
    //       'nmFantasia' => $request['nmInstituicao'],
    //       'cdCNPJ' => '	99.999.999/9999-99',
    //       'estado_id' => $request['estado_id'],
    //       'cidade_id' => $request['cidade_id'],
    //     ]);
    //   }
    //
    //   $formulario['instituicao_id'] = $instituicao->id;
    //   $formulario['curso_id'] = $curso->id;
    // }

    $new = Estudante::create($formulario);
    if($new){
      UserEstudante::create([
        'name' => $formulario['nmEstudante'],
        'email'   => $formulario['dsEmail'],
        'phone'   => $formulario['dsFone'],
        'cdCPF' => $formulario['cdCPF'],
        'dtNascimento' => Carbon::createFromFormat('d/m/Y', $formulario['dtNascimento'])->format('Y-m-d'),
        //'dtNascimento' => Carbon::parse($formulario['dtNascimento'])->format('d/m/Y'),
        'password' => bcrypt($formulario['password']),
        'estudante_id' => $new->id
      ]);
      return redirect()
      //Redireciona para tela de login quando o mesma é cadastrado.
      //->route('estudante.show', ['estudante' => $new->id])
      ->route('painel.estudante.login')
      //
      ->with('success', 'Cadastro Realizado com Sucesso!')
      ->withInput();
      /**/
    }else {
      $cidades = Cidade::where('estado_id', $formulario['estado_id'])->get();
      $cursos = Instituicao::findOrFail($formulario['instituicao_id']);
      $niveis = DB::table('instituicoes_cursos')
      ->join('cursos', 'instituicoes_cursos.curso_id', '=', 'cursos.id')
      ->join('niveis', 'cursos.nivel_id', '=', 'niveis.id')
      ->select('niveis.id', 'niveis.nmNivel')
      ->where('instituicoes_cursos.id',$formulario['curso_id'])
      ->get();
      $turnos = DB::table('turnos_cursos')
      ->join('turnos', 'turnos_cursos.turno_id', '=', 'turnos.id')
      ->select('turnos.id', 'turnos.nmTurno')
      ->where('turnos_cursos.curso_instituicao_id',$formulario['curso_id'])
      ->orderBy('turnos.id')
      ->get();
      return redirect()
      ->back()
      ->with('error', 'Falha ao Cadastrar!')
      ->with('cidades', $cidades)
      ->with('cursos', $cursos)
      ->with('niveis', $niveis)
      ->with('turnos', $turnos)
      ->withInput();
    }

  }

  public function registerPrimeiroAcesso(Request $request){
    $validate = Validator::make($request->all(), [
      'email'   => 'required|email',
      'cpf' => 'required',
      'password' => 'required|string|min:8',
      'user_term' => 'required'
    ]);

    if($validate->fails()){
      return redirect()->back()->with(['status' => 'error', 'msg' =>'Falha no cadastro'])->withInput()->withErrors($validate->errors());
    }
    $estudante = Estudante::where('cdCPF',$request['cpf'])->first();
    if($estudante){
      $user = UserEstudante::where('estudante_id',$estudante->id)->first();
      if(!$user){
        UserEstudante::create([
          'name' => $request['nome'],
          'email'   => $request['email'],
          'cdCPF' => $request['cpf'],
          'dtNascimento' => \Carbon\Carbon::parse($request['dtNascimento']),
          'password' => bcrypt($request['password']),
          'estudante_id' => $estudante->id
        ]);
        return redirect('painel/estudante/login')->with(['status' => 'success', 'msg' => 'Conta criada com sucesso!'])->withInput();
      }
    }
    return redirect('painel/estudante/login')->with(['status' => 'warning', 'msg' => 'Não foi possível criar a conta']);
  }

  public function login(Request $request)
  {
    // Validate the form data
    $this->validate($request, [
      'email'   => 'required|email',
      'password' => 'required'
    ]);

    // Attempt to log the user in
    if (Auth::guard('web_estudante')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
      // if successful, then redirect to their intended location
      return redirect()->to(route('painel.estudante.home'));
      //dd(Auth::guard());
      //return 'logado estudante';
    } else {
      //dd(Auth::guard());
      $errors = ['email' => ['E-mail ou Senha incorreto!']];
      return redirect()->back()->withErrors($errors)->withInput($request->only('email'));
    }
    // if unsuccessful, then redirect back to the login with the form data
    return redirect()->back()->withInput($request->only('email', 'remember'))->withErrors();
  }

  public function logout()
  {
    Auth::guard('web_estudante')->logout();
    //return redirect('/painel');
    return redirect()->intended(route('painel.estudante.login'));
  }

  public function index()
  {
    return view('painel.estudante.home.index');
  }
}
