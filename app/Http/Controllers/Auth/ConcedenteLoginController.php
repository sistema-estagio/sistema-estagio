<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
//use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use Route;

class ConcedenteLoginController extends Controller
{

    public function __construct()
    {
        //$this->middleware('guest:web_concedente')->except(['logout']);
        $this->middleware('guest:web_concedente', ['except' => ['logout']]);
    }

    public function showLoginForm()
    {
      return view('auth.concedente.login');
      //return 'Login Concedente';
    }

    public function login(Request $request)
    {
      // Validate the form data
      $this->validate($request, [
        'email'   => 'required|email',
        'password' => 'required'
      ]);

      // Attempt to log the user in
      if (Auth::guard('web_concedente')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
        // if successful, then redirect to their intended location
        return redirect()->route('painel.concedente.dashboard');
        //dd(Auth::guard());
        //return 'logado concedente';
      }
      // if unsuccessful, then redirect back to the login with the form data
      return redirect()->back()->withInput($request->only('email', 'remember'));
    }

    public function logout()
    {
        Auth::guard('web_concedente')->logout();
        //return redirect('/painel');
        return view('auth.concedente.login');//redirect()->intended(route('painel.concedente.login'));
    }

    public function index()
    {
        return view('painel.concedente.index');
    }
}
