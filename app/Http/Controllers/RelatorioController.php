<?php

namespace App\Http\Controllers;

use App\Models\Cidade;
use App\Models\Concedente;
use App\Models\SecConcedente;
use App\Models\Curso;
use App\Models\Aditivo;
use App\Models\Estado;
use App\Models\Estudante;
use App\Models\Instituicao;
use App\Models\Tce;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PDF;
use Excel;
use Auth;

class RelatorioController extends Controller
{
    private $estado;
    private $cidade;
    private $curso;
    private $instituicao;
    private $estudante;
    private $concedente;
    private $secConcedente;
    private $tce;

    public function __construct(Estado $estado,
                                Cidade $cidade,
                                Curso $curso,
                                Instituicao $instituicao,
                                Estudante $estudante,
                                Concedente $concedente,
                                SecConcedente $secConcedente,
                                Tce $tce)
    {
        $this->estado = $estado;
        $this->cidade = $cidade;
        $this->curso = $curso;
        $this->instituicao = $instituicao;
        $this->estudante = $estudante;
        $this->concedente = $concedente;
        $this->secConcedente = $secConcedente;
        $this->tce = $tce;
    }

    public function concedente()
    {
        $Estados = $this->estado->orderBy('nmEstado', 'Asc')->get();

        return view('concedente.relatorio.concedente', compact('Estados'));
    }

    public function concedentePDF(Request $request)
    {
        //Querys Principal
        $estadoBusca = $this->estado->orderBy('nmEstado', 'Asc');

        //GETS
        $contrato = $request->input('contrato');
        $estado = $request->input('estado_id');
        $cidadeGet = $request->input('cidade_id');

        //QUERYS Buscas
        if ($contrato == 1) {
            $estadoBusca->whereHas('concedentes', function ($query) {
                $query->where('dtFimContrato', '=', null);
            });
        } elseif ($contrato == 2) {
            $estadoBusca->whereHas('concedentes', function ($query) {
                $query->where('dtFimContrato', '<>', null);
            });
        }//Busca por Contrato

        if (!empty($estado)) {
            $estadoBusca->where('id', '=', $estado);
        }//Busca por estado

        //Query Final
        $Estados = $estadoBusca->get();

        //return view('relatorio.concedentePDF', compact('Concedentes'));
        $pdf = PDF::loadView('concedente.relatorio.concedentePDF', compact('Estados', 'cidadeGet'))
            ->setPaper('a4')
            ->setOptions(['dpi' => 100, 'enable_php' => true]);

        return $pdf->stream();
    }

    /**
     * RELATORIOS TCES.
     */
    //Relatorios > Tces
    public function tce()
    {
        return view('tce.relatorios.tce');
    }

    //Relatorios > Tces
    public function tcePDF(Request $request)
    {
        $dtHoje = Carbon::today();
        //Gets
        //$Tipo       = $request->input('tipo');
        $Ordenacao = $request->input('ordenacao');
        //Querys Principal
        //dd($request);
        if ($Ordenacao == 'estudante') {
            //Verificação dos Estados da Unidade do Usuario
            if (Auth::user()->unidade_id != null) {
                $relEstado = Auth::user()->unidade->estado->pluck('id')->toArray();
                $Estudante = $this->estudante
                    //->where('tce_id', '<>', null)
                    ->orderBy('nmEstudante')
                    ->WhereIn('estado_id', $relEstado)->get();
            } else {
                $Estudante = $this->estudante
                //->where('tce_id', '<>', null)
                ->orderBy('nmEstudante')
                ->get();
            }
            //    $Estudante = $this->estudante->orderBy('nmEstudante')->get();

        /*}elseif($Ordenacao == 'ativo'){
            $Estudante = $this->estudante->whereHas('tce', function ($query){
                $query->orderBy('dtFim', 'ASC')->get();
            });
        */
        } elseif ($Ordenacao == 'tce') {
            //Verificação dos Estados da Unidade do Usuario
            if (Auth::user()->unidade_id != null) {
                $relEstado = Auth::user()->unidade->estado->pluck('id')->toArray();
                $Estudante = $this->estudante
                    ->orderBy('tce_id')
                    ->WhereIn('estado_id', $relEstado)->get();
            } else {
                $Estudante = $this->estudante
                ->orderBy('tce_id')
                ->get();
            }
            //$Estudante = $this->estudante->orderBy('tce_id')->get();
        } elseif ($Ordenacao == 'concedente') {
            $Estudante = $this->estudante->all()->sortBy(function ($tce) {
                //Definir qual coluna vai ser usada na ordenação
                //return $tce->estudante->nmEstudante;
                return iconv('UTF-8', 'ASCII//TRANSLIT', $tce->concedente->nmRazaoSocial);
            })->get();
        } else {
            $Estudante = $this->estudante
            ->orderBy('tce_id')
            ->get();
        }
        //dd($Estudante);
        //return view('tce.relatorios.tcePDF', compact('Estudante','dtHoje'));

        if ($request['tipo_relatorio'] == 'pdf') {
            $pdf = PDF::loadView('tce.relatorios.tcePDF', compact('Estudante', 'dtHoje'))
                ->setPaper('a4')
                ->setOptions(['dpi' => 100, 'enable_php' => true]);
            //return $pdf->stream();
            return $pdf->download();
        }
        if ($request['tipo_relatorio'] == 'html') {
            return view('tce.relatorios.tceHTML', compact('Estudante', 'dtHoje'));
            //return view('tce.relatorios.tcePDF', compact('Estudante','dtHoje'));
        }

        if ($request['tipo_relatorio'] == 'excel') {
            /*Excel::create('tces'.$dtHoje->format('d-m-Y'), function($excel) use ($Estudante, $dtHoje){
                $excel->sheet('tces'.$dtHoje->format('d-m-Y'), function($sheet) use ($Estudante, $dtHoje){
                    //$sheet->loadView('estudante.relatorio.estudanteConcedenteCSV', compact('Concedente','Tces'));

                    //$sheet->loadView('tce.relatorios.tceCSV')->with(["Estudante" => $Estudante, "dtHoje" => $dtHoje]);
                    $sheet->loadView('tce.relatorios.tceDadosCompletosCSV')->with(["Estudante" => $Estudante, "dtHoje" => $dtHoje]);
                    //$sheet->loadView('estudante.relatorio.estudanteConcedenteCSV')->with("Tces", $Tces)->with("Concedente", $Concedente);
                    //$sheet->loadView('view', array('key' => 'value'));
                    $sheet->setOrientation('landscape');
                });
            })->export('xlsx');*/

            /*Funfando Legal*/
            //     $query = Estudante::where('tce_id', '!=', NULL);

            //         return Excel::create("relatorio-", function ($excel) use($query) {

            //             $excel->setTitle('Usuários')->sheet('usuarios', function ($sheet) use($query) {

            //                 $sheet->appendRow([
            //                     //Concedente
            //                     'CNPJ',
            //                     'RAZÃO SOCIAL',
            //                     'REP.',
            //                     'CIDADE/UF',
            //                     'END.',
            //                     'BAIRRO',
            //                     'CEP',
            //                     'TELEFONE',
            //                     //IE
            //                     'CNPJ IE',
            //                     'INSTITUIÇÃO',
            //                     'CURSO',
            //                     'RESP.',
            //                     'CIDADE/UF',
            //                     'END.',
            //                     'BAIRRO',
            //                     'CEP',
            //                     'TELEFONE',
            //                     //ESTUDANTE
            //                     'CPF',
            //                     'RG',
            //                     'ESTUDANTE',
            //                     'NASCIMENTO',
            //                     'TELEFONE',
            //                     'CIDADE/UF',
            //                     'ENDEREÇO',
            //                     'BAIRRO',
            //                     'CEP',
            //                     //TCE
            //                     'TCE',
            //                     //'ATIVIDADES',
            //                     'DT.INICIO',
            //                     'DT.FIM',
            //                     'SUPERVISOR',

            //                 ]);

            //                 // Os dados são carregados de 150 em 150, para não sobrecarregar a memória
            //                 $query->chunk(150, function ($usuarios) use ($sheet) {

            //                     foreach ($usuarios as $usuario) {
            //                         foreach ($usuario->tce->where('dtCancelamento',NULL) as $tce){
            //                             //Verifica tce, se for migração exibe nº tce migração
            //                             if($tce->migracao){
            //                                 $tceid=$tce->migracao;
            //                             } else {
            //                                 $tceid=$tce->id;
            //                             }
            //                             //verifica se tem aditivo de data
            //                             if($tce->aditivos->count() > 0){
            //                                 if($tce->aditivos()->latest()->first()->dtInicio != NULL){
            //                                     $dtFim=$tce->aditivos()->latest()->first()->dtFim->format('d/m/Y');
            //                                 } else {
            //                                     $dtFim=$tce->dtFim->format('d/m/Y');
            //                                 }
            //                             } else {
            //                                 $dtFim=$tce->dtFim->format('d/m/Y');
            //                             }
            //                             //Verifica se tem aditivo para supervisor
            //                             if($tce->aditivos->count() > 0){
            //                                 if($tce->aditivos()->latest()->first()->nmSupervisor != ""){
            //                                     $nmSupervisor=$tce->aditivos()->latest()->first()->nmSupervisor;
            //                                 } else {
            //                                     $nmSupervisor=$tce->nmSupervisor;
            //                                 }
            //                             } else{
            //                                 $nmSupervisor=$tce->nmSupervisor;
            //                             }
            //                             $sheet->appendRow([
            //                             //CONCEDENTE
            //                             $tce->concedente->cdCnpjCpf,
            //                             $tce->concedente->nmRazaoSocial,
            //                             $tce->concedente->nmResponsavel,
            //                             $tce->concedente->cidade->nmCidade."/".$tce->concedente->cidade->estado->cdUF,
            //                             $tce->concedente->dsEndereco.", ".$tce->concedente->nnNumero,
            //                             $tce->concedente->nmBairro,
            //                             $tce->concedente->cdCEP,
            //                             $tce->concedente->dsFone,
            //                             //IE
            //                             $tce->instituicao->cdCNPJ,
            //                             $tce->instituicao->nmInstituicao,
            //                             $tce->cursoDaInstituicao->curso->nmCurso,
            //                             $tce->instituicao->nmDiretor,
            //                             $tce->instituicao->cidade->nmCidade."/".$tce->instituicao->cidade->estado->cdUF,
            //                             $tce->instituicao->dsEndereco.", ".$tce->instituicao->nnNumero,
            //                             $tce->instituicao->nmBairro,
            //                             $tce->instituicao->cdCEP,
            //                             $tce->instituicao->dsFone,
            //                             //ESTUDANTE
            //                             $tce->estudante->cdCPF,
            //                             $tce->estudante->cdRG,
            //                             $tce->estudante->nmEstudante,
            //                             $tce->estudante->dtNascimento->format('d/m/Y'),
            //                             $tce->estudante->dsFone,
            //                             $tce->estudante->cidade->nmCidade."/".$tce->estudante->cidade->estado->cdUF,
            //                             $tce->estudante->dsEndereco.", ".$tce->estudante->nnNumero,
            //                             $tce->estudante->nmBairro,
            //                             $tce->estudante->cdCEP,
            //                             //TCE
            //                             $tceid,
            //                             /*foreach($tce->atividades as $atividade)	{
            //                                 $atividade->atividade.";"
            //                             }
            //                             ,
            //                             */
            //                             $tce->dtInicio->format('d/m/Y'),
            //                             $dtFim,
            //                             $nmSupervisor,

            //                             ]);
            //                         }
            //                     }
            //                 });

            //                 $sheet->row(1, function ($row) {
            //                     $row->setFontColor('#ffffff')->setBackground('#00458B');
            //                 });
            //             });

            //         })->download('xlsx');

            $query = Estudante::where('tce_id', '!=', null);

            return Excel::create('relatorio-', function ($excel) use ($query) {
                $excel->setTitle('Usuários')->sheet('usuarios', function ($sheet) use ($query) {
                    $sheet->appendRow([
                        'TCE',
                        'ESTUDANTE',
                        'INSTITUIÇÃO',
                        'CURSO',
                        'SEMESTRE/ANO',
                        'DURAÇÃO',
                        'PERIODO',
                        'AUX-MÊS',
                        'AUX-TRANS',
                        'AUX-ALIM',
                        'DT.INICIO',
                        'DT.FIM',
                    ]);

                    // Os dados são carregados de 150 em 150, para não sobrecarregar a memória
                    $query->chunk(150, function ($usuarios) use ($sheet) {
                        foreach ($usuarios as $usuario) {
                            foreach ($usuario->tce->where('dtCancelamento', null) as $tce) {
                                //Verifica tce, se for migração exibe nº tce migração
                                if ($tce->migracao) {
                                    $tceid = $tce->migracao;
                                } else {
                                    $tceid = $tce->id;
                                }
                                //verifica se tem aditivo de data
                                if ($tce->aditivos->count() > 0) {
                                    if ($tce->aditivos()->latest()->first()->dtInicio != null) {
                                        $dtFim = $tce->aditivos()->latest()->first()->dtFim->format('d/m/Y');
                                    } else {
                                        $dtFim = $tce->dtFim->format('d/m/Y');
                                    }
                                } else {
                                    $dtFim = $tce->dtFim->format('d/m/Y');
                                }
                                //Verifica aditivo de semestre/ano
                                if ($tce->aditivos->count() > 0) {
                                    if ($tce->aditivos()->latest()->first()->nnSemestreAno != null) {
                                        $semestreAno = $tce->aditivos()->latest()->first()->nnSemestreAno;
                                    } else {
                                        $semestreAno = $tce->nnSemestreAno;
                                    }
                                } else {
                                    $semestreAno = $tce->nnSemestreAno;
                                }
                                //Verifica aditivo de periodo
                                if ($tce->aditivos->count() > 0) {
                                    if ($tce->aditivos()->latest()->first()->periodo_id != null) {
                                        $periodo = $tce->aditivos()->latest()->first()->periodo->nmPeriodo;
                                    } else {
                                        $periodo = $tce->periodo->nmPeriodo;
                                    }
                                } else {
                                    $periodo = $tce->periodo->nmPeriodo;
                                }
                                //verifica aditivo aux mensal
                                if ($tce->aditivos->count() > 0) {
                                    if ($tce->aditivos()->latest()->first()->vlAuxilioMensal != null) {
                                        $vlAux = $tce->aditivos()->latest()->first()->vlAuxilioMensal;
                                    } else {
                                        $vlAux = $tce->vlAuxilioMensal;
                                    }
                                } else {
                                    $vlAux = $tce->vlAuxilioMensal;
                                }
                                //verifica aditivo aux transporte
                                if ($tce->aditivos->count() > 0) {
                                    if ($tce->aditivos()->latest()->first()->vlAuxilioTransporte != null) {
                                        $vlTrans = $tce->aditivos()->latest()->first()->vlAuxilioTransporte;
                                    } else {
                                        $vlTrans = $tce->vlAuxilioTransporte;
                                    }
                                } else {
                                    $vlTrans = $tce->vlAuxilioTransporte;
                                }
                                //Verifica aditivo alimentação
                                if ($tce->aditivos->count() > 0) {
                                    if ($tce->aditivos()->latest()->first()->vlAuxilioAlimentacao != null) {
                                        $vlAlimen = $tce->aditivos()->latest()->first()->vlAuxilioAlimentacao;
                                    } else {
                                        $vlAlimen = $tce->vlAuxilioAlimentacao;
                                    }
                                } else {
                                    $vlAlimen = $tce->vlAuxilioAlimentacao;
                                }
                                $nome = explode(' ', $tce->estudante->nmEstudante);
                                $nomeInst = explode(' ', $tce->instituicao->nmInstituicao);
                                $sheet->appendRow([
                                    $tceid,
                                    $nome[0],
                                    $nomeInst[0],
                                    //$tce->estudante->nmEstudante,
                                    //$tce->instituicao->nmInstituicao,
                                    $tce->cursoDaInstituicao->curso->nmCurso,
                                    $semestreAno,
                                    $tce->CursoDaInstituicao->qtdDuracao,
                                    $periodo,
                                    $vlAux,
                                    $vlTrans,
                                    $vlAlimen,
                                    $tce->dtInicio->format('d/m/Y'),
                                    $dtFim,
                                ]);
                            }
                        }
                    });

                    $sheet->row(1, function ($row) {
                        $row->setFontColor('#ffffff')->setBackground('#00458B');
                    });
                });
            })->download('xlsx');
        }
    }

    public function concedenteTce()
    {
        //$Concedentes = $this->concedente->all();
        $Concedentes = $this->concedente->whereHas('tce', function ($query) {
            $query->where('dtCancelamento', null);
        })->orderBy('nmRazaoSocial', 'asc')->get();
        $ConcedentesSec = $this->concedente->whereHas('tce', function ($query) {
            $query->where('dtCancelamento', null)->whereNotNull('sec_conc_id');
        })->get();
        //dd($ConcedentesSec);
        return view('tce.relatorios.concedenteTce', compact('Concedentes', 'ConcedentesSec'));
    }

    // public function concedenteTcePDF(Request $request){

    //     /*
    //     if($request->input('concedente_id') == 'todos'){

    //         $Concedente = (object) $this->concedente->all();
    //         dd($Concedente);
    //     } else {

    //     }*/
    //     $dtHoje = Carbon::today()->format('d/m/Y');
    //     //Querys Principal
    //     $Concedente = $this->concedente->find($request->input('concedente_id'));
    //     //dd($Concedente);

    //     //Gets
    //     $Ordenacao = $request->input('ordenacao');

    //     if($request->input('modelo') == 'sim'){
    //         $paginaPDF = 'tce.relatorios.concedenteTcePDF';
    //         $orientacao = 'portrait';
    //     }else{
    //         $paginaPDF = 'tce.relatorios.concedenteTceOutrosPDF';
    //         $orientacao = 'landscape';
    //     }

    //     $pdf = PDF::loadView($paginaPDF, compact('Concedente','Ordenacao','dtHoje'))
    //         ->setPaper('a4',$orientacao)
    //         ->setOptions(['dpi' => 100,'enable_php'=>true]);
    //     return $pdf->stream();
    // }

    public function concedenteTcePDF(Request $request)
    {
        $dtHoje = Carbon::today()->format('d/m/Y');
        //Querys Principal
        $Concedente = $this->concedente->find($request->input('concedente_id'));
        //dd($Concedente);
        //Gets
        $Ordenacao = $request->input('ordenacao');
        if ($Ordenacao == 'id') {
            $tces = $Concedente->tce->sortBy('id');
        } elseif ($Ordenacao == 'estudante_id') {
            $tces = $Concedente->tce->sortBy(function ($tce) {
                //Definir qual coluna vai ser usada na ordenação
                return iconv('UTF-8', 'ASCII//TRANSLIT', $tce->estudante->nmEstudante);
                //return $tce->estudante->nmEstudante;
            });
        } elseif ($Ordenacao == 'dtFim') {
            $tces = $Concedente->tce->sortBy(function ($tce) {
                //Verifica se o tce tem aditivos e se a data de início é diferente de nula
                if ($tce->aditivos->count() > 0 && $tce->aditivos()->latest()->first()->dtInicio != null) {
                    //retorna objeto baseado na data final do último aditivo
                    return $tce->aditivos()->latest()->first()->dtFim;
                } else {
                    //retorna objeto baseado na data final do tce
                    return $tce->dtFim;
                }
            });
        }
        if ($request->input('modelo') == 'sim') {
            $paginaPDF = 'tce.relatorios.concedenteTcePDF';
            $orientacao = 'portrait';
        } else {
            $paginaPDF = 'tce.relatorios.concedenteTceOutrosPDF';
            $orientacao = 'landscape';
        }

        /*
        $pdf = PDF::loadView($paginaPDF, compact('Concedente','tces','dtHoje'))
            ->setPaper('a4',$orientacao)
            ->setOptions(['dpi' => 100,'enable_php'=>true]);
        return $pdf->stream();*/
        return view($paginaPDF, compact('Concedente', 'tces', 'dtHoje'));
    }

    //Concedente Secretaria
    public function concedenteSecTcePDF(Request $request)
    {
        $dtHoje = Carbon::today()->format('d/m/Y');

        $Concedente = $this->concedente->find($request->input('sec_concedente_id'));
        $tces = Tce::where('concedente_id', $request['sec_concedente_id'])->get();
        if ($request->input('sec_conc_id') != null) {
            $tces = $tces->where('sec_conc_id', $request->input('sec_conc_id'))->sortBy('id');
        }
        $secretaria = $this->secConcedente->find($request->input('sec_conc_id'));
        //dd($Concedente->secConcedente);
        //Gets
        $Ordenacao = $request->input('ordenacao');
        if ($Ordenacao == 'id') {
            $tces = $tces->sortBy('id');
        } elseif ($Ordenacao == 'estudante_id') {
            //Ordenação por nome
            $tces = $tces->sortBy(function ($tce) {
                //Definir qual coluna vai ser usada na ordenação
                return $tce->estudante->nmEstudante;
            });
        } elseif ($Ordenacao == 'dtFim') {
            $tces = $tces->sortBy(function ($tce) {
                //Verifica se o tce tem aditivos e se a data de início é diferente de nula
                if ($tce->aditivos->count() > 0 && $tce->aditivos()->latest()->first()->dtInicio != null) {
                    //retorna objeto baseado na data final do último aditivo
                    return $tce->aditivos()->latest()->first()->dtFim;
                } else {
                    //retorna objeto baseado na data final do tce
                    return $tce->dtFim;
                }
            });
        }
        $tces = $tces->where('dtCancelamento', '==', null);
        $aditivo = new Aditivo();
        foreach ($tces as $tce) {
            $tce->id = $tce->id;
            $aditivo = $tce->aditivos()->latest()->first();
            if ($tce->aditivos->count() > 0) {
                if ($aditivo->nnSemestreAno != null) {
                    $tce->nnSemestreAno = $aditivo->nnSemestreAno;
                }
                if ($aditivo->periodo_id != null) {
                    $tce->periodo->nmPeriodo = $aditivo->periodo->nmPeriodo;
                }
                if ($aditivo->vlAuxilioMensal != null) {
                    $tce->vlAuxilioMensal = $aditivo->vlAuxilioMensal;
                }
                if ($aditivo->vlAuxilioTransporte != null) {
                    $tce->vlAuxilioTransporte = $aditivo->vlAuxilioTransporte;
                }
                if ($aditivo->vlAuxilioAlimentacao != null) {
                    $tce->vlAuxilioAlimentacao = $aditivo->vlAuxilioAlimentacao;
                }
                if ($aditivo->dtInicio != null) {
                    $tce->dtFim = date('d/m/Y', strtotime($aditivo->dtFim));
                }
            }
        }

        if ($request->dataFinal === $request->dataInicial) {
            $dataFinal = explode('/', $request['dataFinal'])[2].'-'.explode('/', $request['dataFinal'])[1].'-'.explode('/', $request['dataFinal'])[0];
            $tces = $tces->where('created_at', $dataFinal);
        } else {
            if ($request->dataFinal != null) {
                $dataFinal = explode('/', $request['dataFinal'])[2].'-'.explode('/', $request['dataFinal'])[1].'-'.explode('/', $request['dataFinal'])[0];
                $tces = $tces->where('created_at', '<=', $dataFinal);
            }

            if ($request->dataInicial != null) {
                $dataInicial = explode('/', $request['dataInicial'])[2].'-'.explode('/', $request['dataInicial'])[1].'-'.explode('/', $request['dataInicial'])[0];
                $tces = $tces->where('created_at', '>=', $dataInicial);
            }
        }

        if ($request->input('protocolo') != 'protocolo') {
            if ($request->input('modelo') == 'sim') {
                $paginaPDF = 'tce.relatorios.concedenteTcePDF';
                $orientacao = 'portrait';
            } elseif ($request->input('modelo') == 'seguro') {
                $paginaPDF = 'tce.relatorios.concedenteTceSeguro';
                $orientacao = 'landscape';
            } else {
                $paginaPDF = 'tce.relatorios.concedenteTceOutrosPDF';
                $orientacao = 'landscape';
            }

            // $pdf = PDF::loadView($paginaPDF, compact('Concedente','tces','dtHoje','secretaria'))
            //     ->setPaper('a4',$orientacao)
            //     ->setOptions(['dpi' => 100,'enable_php'=>true]);
            // return $pdf->stream();
            return view($paginaPDF, compact('Concedente', 'tces', 'dtHoje', 'secretaria'));
        } else {
            //return $pdf->download('document.pdf');
            //return dd($pdf);
            //return view($paginaPDF, compact('Concedente','tces','dtHoje','secretaria'));
            return view('tce.relatorios.concedenteTceProtocolo', compact('Concedente', 'tces', 'dtHoje', 'secretaria'));
        }
    }

    public function tceDesValidado()
    {
        //$Concedentes = $this->concedente->all();
        $Concedentes = $this->concedente->whereHas('tce', function ($query) {
            $query->where('dtCancelamento', null);
        })->orderBy('nmRazaoSocial', 'asc')->get();
        $ConcedentesSec = $this->concedente->whereHas('tce', function ($query) {
            $query->where('dtCancelamento', null)->whereNotNull('sec_conc_id');
        })->get();
        //dd($ConcedentesSec);
        return view('tce.relatorios.desvalidadoTce', compact('Concedentes', 'ConcedentesSec'));
    }

    //Concedente Secretaria
    public function tceDesValidadoPDF(Request $request)
    {
        $dtHoje = Carbon::today()->format('d/m/Y');
        //dd($request->all());
        //Querys Principal
        $Concedente = $this->concedente->find($request->input('concedente_id'));
        //dd($Concedente);
        if ($request->input('sec_conc_id') == null) {
            $tces = $Concedente->tce->where('dtCancelamento', '==', null)->where('dt4Via', '==', null);
        } else {
            $tces = $Concedente->tce->where('sec_conc_id', $request->input('sec_conc_id'))->where('dtCancelamento', '==', null)->where('dt4Via', '==', null)->sortBy('id');
        }
        //dd($tces);
        $secretaria = $this->secConcedente->find($request->input('sec_conc_id'));
        //dd($Concedente->secConcedente);
        //Gets
        $Ordenacao = $request->input('ordenacao');
        if ($Ordenacao == 'id') {
            $tces->sortBy('id');
        } elseif ($Ordenacao == 'estudante_id') {
            //Ordenação por nome
            $tces = $tces->sortBy(function ($tce) {
                //Definir qual coluna vai ser usada na ordenação
                return iconv('UTF-8', 'ASCII//TRANSLIT', $tce->estudante->nmEstudante);
                //return $tce->estudante->nmEstudante;
            });
        } elseif ($Ordenacao == 'dtFim') {
            $tces->sortBy(function ($tce) {
                //Verifica se o tce tem aditivos e se a data de início é diferente de nula
                if ($tce->aditivos->count() > 0 && $tce->aditivos()->latest()->first()->dtInicio != null) {
                    //retorna objeto baseado na data final do último aditivo
                    return $tce->aditivos()->latest()->first()->dtFim;
                } else {
                    //retorna objeto baseado na data final do tce
                    return $tce->dtFim;
                }
            });
        }
        //return $pdf->download('document.pdf');
        //return dd($pdf);
        //return view($paginaPDF, compact('Concedente','tces','dtHoje','secretaria'));
        return view('tce.relatorios.desvalidadoTcePDF', compact('Concedente', 'tces', 'dtHoje', 'secretaria'));
    }

    //Inicio Relatorio Validações
    public function concedenteTceValidado()
    {
        //Pega só concedentes que tem TCE não cancelados
        $Concedentes = $this->concedente->whereHas('tce', function ($query) {
            $query->whereNull('dtCancelamento');
        })->get();

        return view('tce.relatorios.concedenteTceValidado', compact('Concedentes'));
    }

    public function concedenteTceValidadoPDF(Request $request)
    {
        $dtHoje = Carbon::today()->format('d/m/Y');
        if ($request->input('dtInicio')) {
            $dtInicio = Carbon::createFromFormat('d/m/Y', $request->input('dtInicio'))->toDateString();
            $dtFim = Carbon::createFromFormat('d/m/Y', $request->input('dtFim'))->toDateString();
        }
        //Querys Principal
        $Concedente = $this->concedente->find($request->input('concedente_id'));
        $secretaria = $this->secConcedente->find($request->input('sec_conc_id'));
        if ($secretaria == null) {
            if ($request->input('dtInicio')) {
                $tces = $this->tce->where('dtCancelamento', null)->whereBetween('dt4Via', [$dtInicio, $dtFim])->where('concedente_id', '=', $request->input('concedente_id'))->get();
                $tces = $tces->sortBy(function ($tce) {
                    //Definir qual coluna vai ser usada na ordenação
                    return iconv('UTF-8', 'ASCII//TRANSLIT', $tce->estudante->nmEstudante);
                });
            } else {
                $tces = $this->tce->where('dtCancelamento', null)->where('dt4Via', '<>', null)->where('concedente_id', '=', $request->input('concedente_id'))->get();
                $tces = $tces->sortBy(function ($tce) {
                    return iconv('UTF-8', 'ASCII//TRANSLIT', $tce->estudante->nmEstudante);
                });
            }
        } else {
            if ($request->input('dtInicio')) {
                $tces = $this->tce->where('dtCancelamento', null)->whereBetween('dt4Via', [$dtInicio, $dtFim])->where('concedente_id', '=', $request->input('concedente_id'))->where('sec_conc_id', $request->input('sec_conc_id'))->get();
                $tces = $tces->sortBy(function ($tce) {
                    return iconv('UTF-8', 'ASCII//TRANSLIT', $tce->estudante->nmEstudante);
                });
            } else {
                $tces = $this->tce->where('dtCancelamento', null)->where('dt4Via', '<>', null)->where('concedente_id', '=', $request->input('concedente_id'))->where('sec_conc_id', $request->input('sec_conc_id'))->get();
                $tces = $tces->sortBy(function ($tce) {
                    return iconv('UTF-8', 'ASCII//TRANSLIT', $tce->estudante->nmEstudante);
                });
            }
        }

        return view('tce.relatorios.concedenteTceValidadoPDF', compact('tces', 'Concedente', 'Ordenacao', 'secretaria', 'dtHoje'));
    }

    //Fim Relatorio Validações

    public function concedenteTceCancelado()
    {
        //Pega só concedentes que tem TCE CANCELADOS
        $Concedentes = $this->concedente->whereHas('tce', function ($query) {
            $query->whereNotNull('dtCancelamento');
        })->get();
        //dd($Concedentes, $Concedentes2);
        return view('tce.relatorios.concedenteTceCancelado', compact('Concedentes'));
    }

    public function concedenteTceCanceladoPDF(Request $request)
    {
        if ($request->dataInicial) {
            // Replacing slashes
            $dataInicial = str_replace('/', '-', $request->dataInicial);
            //Converting date format
            $fmtDataInicial = date('Y-m-d 00:00:00', strtotime($dataInicial));
        }

        if ($request->dataFinal) {
            // Replacing slashes
            $dataFinal = str_replace('/', '-', $request->dataFinal);
            //Converting date format
            $fmtDataFinal = date('Y-m-d 00:00:00', strtotime($dataFinal));
        }

        $dtHoje = Carbon::today()->format('d/m/Y');
        //Querys Principal
        $Concedente = $this->concedente->find($request->input('concedente_id'));
        $secretaria = $this->secConcedente->find($request->input('sec_conc_id'));
        //Gets
        $tces = Tce::where('concedente_id', $Concedente->id)->whereNotNull('dtCancelamento')->get();

        if ($request->input('sec_conc_id') != null) {
            $tces = $tces->where('sec_conc_id', $secretaria->id);
        }

        if ($request->dataInicial === $request->dataFinal) {
            $tces = $tces->where('cancelado_at', $fmtDataFinal);
        } else {
            if ($request->dataInicial != null) {
                $tces = $tces->where('cancelado_at', '>=', date($fmtDataInicial));
            }

            if ($request->dataFinal != null) {
                $tces = $tces->where('cancelado_at', '<=', date($fmtDataFinal));
            }
        }
        //$tces = $Concedente->tce->where('dtCancelamento','<>',null);
        $Ordenacao = $request->input('ordenacao');

        if ($Ordenacao == 'id') {
            $tces = $tces->sortBy('id');
        } elseif ($Ordenacao == 'estudante_id') {
            //Ordenação por nome
            $tces = $tces->sortBy(function ($tce) {
                //Definir qual coluna vai ser usada na ordenação
                return iconv('UTF-8', 'ASCII//TRANSLIT', $tce->estudante->nmEstudante);
                //return $tce->estudante->nmEstudante;
            });
        }
        /*else if ($Ordenacao == "dtFim"){
            $tces = $tces->sortBy(function($tce){
                //Verifica se o tce tem aditivos e se a data de início é diferente de nula
                if($tce->aditivos->count() > 0 && $tce->aditivos()->latest()->first()->dtInicio != Null){
                    //retorna objeto baseado na data final do último aditivo
                    return $tce->aditivos()->latest()->first()->dtFim;
                } else {
                    //retorna objeto baseado na data final do tce
                    return $tce->dtFim;
                }
            });
        }*/
        /*
        $pdf = PDF::loadView('tce.relatorios.concedenteTceCanceladoPDF', compact('Concedente','secretaria',dtHoje','tces'))
            ->setPaper('a4')
            ->setOptions(['dpi' => 100,'enable_php'=>true]);
        return $pdf->stream();
        */
        //dd($tces);
        return view('tce.relatorios.concedenteTceCanceladoPDF', compact('Concedente', 'secretaria', 'dtHoje', 'tces'));
    }

    public function instituicao()
    {
        $Estados = $this->estado->orderBy('nmEstado', 'Asc')->get();

        //return view('relatorio.instituicao', compact('Estados'));
        return view('instituicao.relatorio.instituicao', compact('Estados'));
    }

    public function instituicaoPDF(Request $request)
    {
        //Querys Principal
        $estadoBusca = $this->estado->orderBy('nmEstado', 'Asc');

        //GETS
        $estado = $request->input('estado_id');
        $cidade = $request->input('cidade_id');

        //QUERYS Buscas
        if (!empty($estado)) {
            $estadoBusca->where('id', '=', $estado)->whereHas('instituicao', function ($query) use ($estado) {
                $query->where('estado_id', '=', $estado);
            });
        }//Busca por estado

        //Query Final
        $Estados = $estadoBusca->get();

        $dtHoje = Carbon::today()->format('d/m/Y');
        //$pdf = PDF::loadView('relatorio.instituicaoPDF', compact('Estados','dtHoje'))
        $pdf = PDF::loadView('instituicao.relatorio.instituicaoPDF', compact('Estados', 'dtHoje', 'cidade', 'estado'))
            ->setPaper('a4')
            ->setOptions(['dpi' => 100, 'enable_php' => true]);

        return $pdf->stream();
    }

    public function curso()
    {
        $Estados = $this->estado->orderBy('nmEstado', 'Asc')->get();

        return view('instituicao.relatorio.curso', compact('Estados'));
    }

    public function cursoPDF(Request $request)
    {
        //Querys Principal
        $estadoBusca = $this->estado->orderBy('nmEstado', 'Asc');

        //GETS
        $estado = $request->input('estado_id');
        $cidadeGet = $request->input('cidade_id');

        //QUERYS Buscas
        if (!empty($estado)) {
            $estadoBusca->where('id', '=', $estado)->whereHas('instituicao', function ($query) use ($estado) {
                $query->where('estado_id', '=', $estado);
            });
        }//Busca por estado

        //Query Final
        $Estados = $estadoBusca->get();
        $dtHoje = Carbon::today()->format('d/m/Y');
        //return view('instituicao.relatorio.cursoPDF', compact('Estados','dtHoje'));

        $pdf = PDF::loadView('instituicao.relatorio.cursoPDF', compact('Estados', 'dtHoje', 'cidadeGet'))
            ->setPaper('a4')
            ->setOptions(['dpi' => 100, 'enable_php' => true]);

        return $pdf->stream();
    }

    public function cursoInstituicao()
    {
        $Curso = $this->curso->orderBy('nmCurso', 'Asc')->get();

        return view('instituicao.relatorio.cursoInstituicao', compact('Curso'));
    }

    public function cursoInstituicaoPDF(Request $request)
    {
        $dtHoje = Carbon::today()->format('d/m/Y');

        //Querys Principal
        $cursoBusca = $this->curso->orderBy('nmCurso', 'Asc');
        $instituicaoBUsca = $this->instituicao->orderBy('nmInstituicao', 'Asc');

        //GETS
        $curso = $request->input('curso_id');

        //QUERYS Buscas
        if (!empty($curso)) {
            $cursoBusca->where('id', '=', $curso);
        }//Busca por estado

        //Query Final
        $Cursos = $cursoBusca->get();
        //dd($Cursos);
        $Instituicao = $instituicaoBUsca->get();
        //ini_set('max_execution_time', 0); // Also you can increase memory
        //ini_set('memory_limit','2048M');

        $pdf = PDF::loadView('instituicao.relatorio.cursoInstituicaoPDF', compact('Cursos', 'Instituicao', 'dtHoje'))
            ->setPaper('a4')
            ->setOptions(['dpi' => 100, 'enable_php' => true]);

        return $pdf->stream();
        /*
        return view('instituicao.relatorio.cursoInstituicaoPDF', compact('Cursos','Instituicao','dtHoje'));
        */
    }

    /**
     * RELATORIOS DE ESTUDANTE.
     */
    public function estudante()
    {
        $Estados = $this->estado->orderBy('nmEstado', 'Asc')->get();
        $Cursos = $this->curso->all();

        return view('estudante.relatorio.estudante', compact('Cursos', 'Estados'));
    }

    public function estudantePDF(Request $request)
    {
        $dtHoje = Carbon::today()->format('d/m/Y');

        //GETS
        $estado = $request->input('estado_id');
        $cidade = $request->input('cidade_id');
        $curso = $request->input('curso');
        $semestre = $request->input('semestre');
        $idade = $request->input('idade');
        $sexo = $request->input('sexo');
        $ordenacao = $request->input('ordenacao');

        //Querys Principal
        $estudanteBusca = $this->estudante;

        //QUERYS Buscas
        if (!empty($estado)) {
            $estudanteBusca = $this->estudante->where('estado_id', '=', $estado);
        }
        if (!empty($cidade)) {
            $estudanteBusca = $this->estudante->where('cidade_id', '=', $cidade);
        }
        if (!empty($curso)) {
            $estudanteBusca = $this->estudante->whereHas('CursoDaInstituicao', function ($query) use ($curso) {
                $query->where('curso_id', '=', $curso);
            });
        }
        if (!empty($semestre)) {
            $estudanteBusca = $this->estudante->where('nnSemestreAno', '=', $semestre);
        }

        if (!empty($idade)) {
            $dtInicial = Carbon::now()->subYears($idade)->subYear(1)->addDay(1)->toDateString();
            $dtFinal = Carbon::now()->subYears($idade)->toDateString();

            //Query
            $estudanteBusca = $this->estudante
                ->where('dtNascimento', '>=', $dtInicial)
                ->where('dtNascimento', '<=', $dtFinal);

            //dd($estudanteBusca);
        }

        if (!empty($sexo)) {
            $estudanteBusca = $this->estudante->where('dsSexo', '=', $sexo);
        }

        //Ordenacao Final
        if ($ordenacao == 'id') {
            $Estudantes = $estudanteBusca->orderBy('id', 'Asc')->get();
        } elseif ($ordenacao == 'nmEstudante') {
            $Estudantes = $estudanteBusca->orderBy('nmEstudante', 'Asc')->get();
        } elseif ($ordenacao == 'curso_id') {
            $Estudantes = $estudanteBusca->select(DB::raw('estudantes.*'))
                ->leftJoin('instituicoes_cursos', 'instituicoes_cursos.id', '=', 'estudantes.curso_id')
                ->leftJoin('cursos', 'cursos.id', '=', 'instituicoes_cursos.curso_id')
                ->orderBy('cursos.nmCurso', 'asc')
                ->get();
        } elseif ($ordenacao == 'nnSemestreAno') {
            $Estudantes = $$estudanteBusca = $this->estudante->orderBy('nnSemestreAno', 'Asc')->get();
        }

        /*
        $pdf = PDF::loadView('estudante.relatorio.estudantePDF', compact('Estudantes','dtHoje'))
            ->setPaper('a4')
            ->setOptions(['dpi' => 100,'enable_php'=>true]);
        return $pdf->stream();
        */
        return view('estudante.relatorio.estudanteHTML', compact('Estudantes', 'dtHoje'));
    }

    public function estudanteInstituicao()
    {
        $Instituicao = $this->instituicao->all();

        return view('estudante.relatorio.estudanteInstituicao', compact('Instituicao'));
    }

    public function estudanteInstituicaoPDF(Request $request)
    {
        $dtHoje = Carbon::today()->format('d/m/Y');

        //GETS
        $instituicao = $request->input('instituicao_id');
        $tce = $request->input('tce');
        $ordenacao = $request->input('ordenacao');

        //Querys Principal
        $estudanteBusca = $this->estudante->orderBy($ordenacao, 'Asc');

        if ($tce == 'no') {
            $estudanteBusca->where('tce_id', '=', null);
        } elseif ($tce == 'ativo') {
            $estudanteBusca->where('tce_id', '<>', null)->whereHas('tce', function ($query) {
                $query->where('dtCancelamento', '=', null);
            });
        } else {
            $estudanteBusca->whereHas('tce', function ($query) {
                $query->where('dtCancelamento', '<>', null);
            });
        }
        //Query Final
        if (!empty($instituicao)) {
            $Estudantes = $estudanteBusca->where('instituicao_id', $instituicao)->get();
        }//Busca por cidade
        else {
            $Estudantes = $estudanteBusca->get();
        }
        //$Estudantes = $estudanteBusca->where('instituicao_id',$instituicao)->get();

        $pdf = PDF::loadView('estudante.relatorio.estudanteInstituicaoPDF', compact('Estudantes', 'dtHoje'))
            ->setPaper('a4')
            ->setOptions(['dpi' => 100, 'enable_php' => true]);

        return $pdf->stream();
    }

    public function estudanteConcedente()
    {
        $Concedentes = $this->concedente->whereHas('tce', function ($query) {
            $query->where('dtCancelamento', null);
        })->get();
        //dd($Concedentes);
        return view('estudante.relatorio.estudanteConcedente', compact('Concedentes'));
    }

    public function estudanteConcedenteCSV(Request $request)
    {
        //Querys Principal
        $Concedente = $this->concedente->find($request->input('concedente_id'));
        $dtNascimento = $request->query('dtNascimento');
        $cdCPF = $request->query('cdCPF');
        $endereco = $request->query('endereco');
        $contato = $request->query('contato');
        $ensino = $request->query('ensino');
        //dd(var_dump($opcoes));
        //Gets
        $Tces = $Concedente->tce->sortBy(function ($tce) {
            //Definir qual coluna vai ser usada na ordenação
            return $tce->estudante->nmEstudante;
        })->where('dtCancelamento', null);
        //dd($Tces);
        //foreach()

        //Excel::create('estudantes', function($excel) use ($Tces, $Concedente){
        //$excel->sheet('estudantes', function($sheet) use ($Tces, $Concedente){
        Excel::create('estudantes', function ($excel) use ($Tces, $Concedente,$dtNascimento,$cdCPF,$endereco,$ensino,$contato) {
            $excel->sheet('estudantes', function ($sheet) use ($Tces, $Concedente,$dtNascimento,$cdCPF,$endereco,$ensino,$contato) {
                //$sheet->loadView('estudante.relatorio.estudanteConcedenteCSV', compact('Concedente','Tces'));
                $sheet->loadView('estudante.relatorio.estudanteConcedenteCSV')->with(['Tces' => $Tces, 'Concedente' => $Concedente, 'dtNascimento' => $dtNascimento,  'cdCPF' => $cdCPF, 'endereco' => $endereco, 'contato' => $contato, 'ensino' => $ensino]);
                //$sheet->loadView('estudante.relatorio.estudanteConcedenteCSV')->with("Tces", $Tces)->with("Concedente", $Concedente);
                //$sheet->loadView('view', array('key' => 'value'));
                $sheet->setOrientation('landscape');
            });
        })->export('xlsx');

        //return view('estudante.relatorio.estudanteConcedenteCSV',compact('Tces','Concedente','dtNascimento','cdCPF','endereco','contato','ensino'));

        // if($request->input('modelo') == 'sim'){
        //     $paginaPDF = 'tce.relatorios.concedenteTcePDF';
        //     $orientacao = 'portrait';
        // } else {
        //     $paginaPDF = 'tce.relatorios.concedenteTceOutrosPDF';
        //     $orientacao = 'landscape';
        // }

        // $pdf = PDF::loadView($paginaPDF, compact('Concedente','tces','dtHoje'))
        //     ->setPaper('a4',$orientacao)
        //     ->setOptions(['dpi' => 100,'enable_php'=>true]);
        // return $pdf->stream();
    }
}
/*$estudanteBusca->where('tce_id','<>',Null)->whereHas('tce', function ($query){
    $query->where('dtCancelamento','=',NULL);
});
*/
