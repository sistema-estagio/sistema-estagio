<?php

namespace App\Http\Controllers\Mediador;

use App\Models\UserConcedente;
use App\Models\FolhaPagamento;
use App\Models\FolhaPagamentoItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class FaltaController extends Controller
{

  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {
    $this->middleware('auth:web_concedente');
  }

  /**
  * Display the specified resource.
  *
  * @param  \App\UserConcedente  $userConcedente
  * @return \Illuminate\Http\Response
  */
  public function show() {

    $user = UserConcedente::find(Auth::id());

    $folhas = FolhaPagamento::where('concedente_id',$user->concedente_id)->where('fechado','N')->get();
    $folha = null;
    $items = null;
    if($folhas->count() > 0){
      if($user->sec_concedente_id != null) {
        $folha = $folhas->where('secConcedente_id', $user->sec_concedente_id)->latest()->first();
      }else{
        $folha = $folhas->first();
      }
    }

    if($folha){
      $items = FolhaPagamentoItem::where('folha_id', $folha->id)->get();
    }

    return view('painel.mediador.faltas.index', compact('items', 'folha'));
  }


  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  \App\UserConcedente  $userConcedente
  * @return \Illuminate\Http\Response
  */
  public function updateItem(Request $request, $hash){

    $folha = FolhaPagamento::where('hash',$hash)->get();
    if($folha->count() > 0){
      $folha = $folha->first();
      if($folha->concedente_id === Auth::user()->concedente_id){
        $item = FolhaPagamentoItem::find($request['item']['id']);
        if($item->folha_id == $folha->id){
          $item->diasBaseEstagiados = $request['item']['dias'];
          $item->faltas = $request['item']['faltas'];
          $item->justificadas = $request['item']['justificadas'];
          $item->save();

          return response()->json(['status'=>'success']);
        }
      }
    }
    return response()->json(['status'=>'fail']);
  }

}
