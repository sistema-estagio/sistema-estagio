<?php

namespace App\Http\Controllers;

use App\Models\Aditivo;
use Illuminate\Http\Request;
use App\Models\Estudante;
use App\Models\Tce;
use Illuminate\Database\Eloquent\Collection;
use App\Models\Concedente;
use App\Models\Supervisor;
use App\Models\Relatorio;
use App\Models\Periodo;
use App\Models\Atividade;
use App\Models\AtividadeTce;
use Carbon\Carbon;
use PDF;
use Auth;
use DB;
use Validator;

class AditivoController extends Controller
{

    private $aditivo;
    private $tce;
    private $concedente;
    private $estudante;
    protected $totalPage = 2;

    /**
     * AditivoController constructor.
     * @param Aditivo $aditivo
     * @param Tce $tce
     * @param Concedente $concedente
     */
    public function __construct(Aditivo $aditivo, Tce $tce, Concedente $concedente, Estudante $estudante)
    {
        $this->aditivo = $aditivo;
        $this->tce = $tce;
        $this->concedente = $concedente;
        $this->estudante = $estudante;
    }

    public function create($id)
    {
            $titulo = "Cadastro de Aditivo";
            $tce = Tce::findOrFail($id);
            $dtHoje = Carbon::today()->format('d/m/Y');
            $curso = $tce->cursoDaInstituicao->curso->id;
            //$collection->where('price', 100);
            $atividades = Atividade::whereIn('curso_id', [$curso]);
            $aditivosSemestre = Aditivo::where('tce_id', $id)->where('nnSemestreAno','<>', NULL)->get();
            //dd($aditivosSemestre);
            $relatorios = Relatorio::all();
            $periodos = Periodo::all()->sortBy('id');
            $supervisorsCollection = new Collection;
            if($tce->sec_conc_id != null){
              $supervisors = Supervisor::where('secConcedente_id',$tce->sec_conc_id)->get();
            }else{
              $supervisors = Supervisor::where('concedente_id',$tce->concedente_id)->get();
            }
            foreach($supervisors as $supervisor){
              if(Supervisor::validate($supervisor) === true){
                $supervisorsCollection->push($supervisor);
              }
            }
            $supervisors = $supervisorsCollection;
        //Verificação de Estado do Usuario
        /*if(Auth::user()->unidade_id != null){//Verificando se recebe unidade
            if($tce->estado_id == Auth::user()->unidade->estado_id){//verificando se estado é igual ao estado da unidad
                $tce;
            }else{
                return redirect()->route('tce.index');
            }
        }
        */
        return view('tce.aditivo.cadastro', compact('titulo','tce','aditivosSemestre','dtHoje','atividades','relatorios','periodos', 'supervisors'));
    }

    public function store(Request $request)
    {
        DB::beginTransaction();

        try {
            $formulario = $request->all();
            $formulario['user_id'] = Auth::user()->id;
            $validar= Validator::make($request->all(), [
                'dtAditivo' => 'required',
            ],[
                'dtAditivo.required' => ' O campo DATA DO ADITIVO é Obrigatório.',
            ]);
            //dd($formulario);
            if($validar->fails()) {
                //$secretarias = SecConcedente::where('concedente_id', $formulario['concedente_id'])->get();
                return
                redirect()
                ->back()
                //->with('secretarias', $secretarias)
                ->withErrors($validar)
                ->withInput();
            }
            //Pegando o Estudante do TCE
            $getEstudante = $this->estudante->findOrFail($formulario['estudante_id']);
            $getTce = $this->tce->findOrFail($formulario['tce_id']);
            $getConcedente = $getTce->concedente;
            //if ($getEstudante->tce->id != $getTce->id  && $getEstudante->tce->concedente_id == $getTce->concedente_id) {
            //dd($getEstudante->tce);
            //Pega duração de curso o semestre ano do estagiario
            $qtdCurso = $getTce->cursoDaInstituicao->qtdDuracao;
            $qtdEstudante = $getTce->estudante->nnSemestreAno;
            //-----//VERIFICANDO INSERT//-----//
            if($getConcedente->dtFimContrato){
                //$fimContrato = Carbon::createFromFormat('Y-m-d', $getConcedente->dtFimContrato);
                $fimContrato = $getConcedente->dtFimContrato;
                //dd($fimContrato);
            }
            if($formulario['dtFim'] != ''){
                $dtFim      = Carbon::createFromFormat('d/m/Y', $formulario['dtFim']) ;
            } else {
                $dtFim = $getTce->dtFim->toDateString();
            }
            if($formulario['dtInicio'] != ''){
                $dtInicio   = Carbon::createFromFormat('d/m/Y', $formulario['dtInicio']);
            }

            $supervisor = Supervisor::find($request['supervisor_id']);
            if($supervisor){
              if(Supervisor::validate($supervisor) == true){
                $formulario['supervisor_id'] = $supervisor->id;
              }else{
                return redirect()
                    ->back()
                    ->with('error', 'O SUPERVISOR SELECIONADO JÁ ATINGIU O LIMITE DE SUPERVISÃO DE ESTÁGIO.')
                    ->withInput();
              }
            }

            //verifica contrato concedente se tiver ver se data final é menor que dtFim do contrato
            if($getConcedente->dtFimContrato == null OR $fimContrato->toDateString() >= $getTce->dtFim->toDateString()) {//verificando a data do contrato se for null ou maior que data limite
                //dd($getConcedente->dtFimContrato, $getTce->dtFim->toDateString());
                switch($getTce->cursoDaInstituicao->curso_duracao_id)
                {
                    CASE '1':
                        //Verfica se data está vazia no formulario de aditivo
                        if($formulario['dtInicio'] == ''){
                            //Pegando o total de tempo do curso em meses
                            $totCurso = $qtdCurso*12;
                            if($formulario['nnSemestreAno'] != ''){
                                $totEstudante = $formulario['nnSemestreAno']*12;
                                //Atualizando o semestre do Estudante
                                //$idEstudanteTce = $getTce->estudante->id;
                                //$dbEstudante = $this->estudante->find($idEstudanteTce);
                                //$dbEstudante->update(['nnSemestreAno' => $formulario['nnSemestreAno']]);
                            }else{
                                $totEstudante = $qtdEstudante*12;
                            }
                            // FIM.  Verificando se há nsemestre no aditivo//

                            $dif = $totCurso - $totEstudante;
                            $final = $getTce->dtInicio->addMonth($dif);
                            //$incioAdd = $final;

                            //Caso o estudante esteja no ultimo Ano
                            if($totCurso == $totEstudante){
                                //$dtInicio = Carbon::createFromFormat('d/m/Y', $formulario['dtInicio']);
                                $dtInicio = $getTce->dtInicio;
                                $incioAdd = Carbon::create($dtInicio->year, 12, 31, 0);//pega dtinicio pois o cara só tem o ano corrente para estágio.
                            } else {
                                $incioAdd = Carbon::create($final->year, 12, 31, 0);//pega dtfinal pois o cara tem mais tempo pra estagio.
                            }

                            //if($getTce->dtFim > $incioAdd){
                            //    return redirect()
                            //        ->back()
                            //        ->with('error', 'O FIM DO TCE('.$getTce->dtFim->format('d/m/Y').') É DEPOIS DO FIM DO CURSO('.$incioAdd->format('d/m/Y').') Linha 168.')
                            //        ->withInput();
                            //}else{
                                //cadastro do aditivo
                                $qtdAditivos = Aditivo::where('tce_id', $formulario['tce_id'])->count();
                                $qtdAditivos++;
                                $formulario['nnAditivo'] = $qtdAditivos;

                                $new = $this->aditivo->create($formulario);
                                //Se Aditivo tiver atividades
                                if(isset($formulario['atividades'])){
                                    $new->atividades()->attach(array_values($formulario['atividades']));
                                }
                            //}
                        //Se houver data no aditivo calculos abaixo
                        }else{
                            //Pegando o total de tempo do curso
                            $totCurso = $qtdCurso*12;
                            if($formulario['nnSemestreAno'] != ''){
                                $totEstudante = $formulario['nnSemestreAno']*12;
                                //Atualizando o semestre do Estudante
                                //$idEstudanteTce = $getTce->estudante->id;
                                //$dbEstudante = $this->estudante->find($idEstudanteTce);
                                //$dbEstudante->update(['nnSemestreAno' => $formulario['nnSemestreAno']]);
                            }else{
                                $totEstudante = $qtdEstudante*12;
                            }
                            // FIM.  Verificando se há nsemestre no aditivo//

                            $dif = $totCurso - $totEstudante;
                            $final = $dtInicio->addMonth($dif);
                            //$incioAdd = $final->addMonth(12);
                            $incioAdd = $final;

                            //dd($final, $dtInicio, $dtFim, $incioAdd);
                            //Caso o estudante esteja no ultimo Ano
                            if($totCurso == $totEstudante){
                                $dtInicio = Carbon::createFromFormat('d/m/Y', $formulario['dtInicio']);
                                $incioAdd = Carbon::create($dtInicio->year, 12, 31, 0);//pega dtinicio pois o cara está no ultimo ano de estagio.
                            } else {
                                $incioAdd = Carbon::create($final->year, 12, 31, 0);//pega final pois o cara tem mais tempo para estagio.
                            }
                            //fim caso o estudante esteja no ultimo ano
                            //dd($final, $dtInicio, $dtFim, $incioAdd);
                            if($dtFim->toDateString() > $incioAdd->toDateString()){
                                return redirect()
                                    ->back()
                                    ->with('error', 'O FIM DO ADITIVO É DEPOIS DO FIM DO CURSO. Linha 215 Aditivo.')
                                    ->withInput();
                            }else{
                                $dtFimAditivo      = Carbon::createFromFormat('d/m/Y', $formulario['dtFim']) ;
                                $dtInicioAditivo   = Carbon::createFromFormat('d/m/Y', $formulario['dtInicio']);
                                //$getEstudanteAditivo = $getEstudante->tce->first();

                                //verificando se existe tce com o mesmo concedente
                                if ($getEstudante->tce->where('concedente_id', $getConcedente->id)->all()) {
                                    $dtInicio   = Carbon::createFromFormat('d/m/Y', $formulario['dtInicio']);
                                    //dd($formulario, $getEstudante->tce->where('concedente_id', $formulario['concedente_id']));
                                    //$totalFinal = 0;
                                    $dataTotTrabalhado = 0;

                                    if($getEstudante->tce->where('concedente_id', $getConcedente->id)->whereNotIn('id', $getTce->id)->count() > 0){
                                        foreach ($getEstudante->tce->where('concedente_id', $getConcedente->id)->whereNotIn('id', $getTce->id) as $tces) {
                                            //Pegando a diferença de tempo entre o cancelamento e o incio dos tces
                                            $dataTotTrabalhado += $tces->dtInicio->diffInMonths($tces->dtCancelamento);
                                        }
                                    //fim if se tem mais tce que o do aditivo atual que seja do mesmo concedente
                                    }
                                    $esseTce =  $getEstudante->tce->where('id', $getTce->id);
                                    foreach ($esseTce as $eTce) {
                                        //Pegando dtInicio e dtFim do TCE atual e soma com os outros Tces se houver
                                        $dataTotTrabalhado += $eTce->dtInicio->diffInMonths($eTce->dtFim);
                                    }
                                    //pega diferença em meses da dtInicio do formulario e dtFim
                                    $difInicioFim = $dtInicio->diffInMonths($dtFim);
                                    //Soma total trabalhado em todos tces encontrados com difInicioFim
                                    $totalFinal = $dataTotTrabalhado + $difInicioFim;

                                    //dd($formulario, $dtInicio, $dtFim, $dataTotTrabalhado,$difInicioFim, $totalFinal);

                                    if ($totalFinal > 24) {//Verificando dif fim com limite
                                        //dd('ERRO, A Data Limite é maior | Total é de - ' . $totalFinal . ' Meses');
                                        return redirect()
                                            ->back()
                                            ->with('error', 'ERRO, O total de meses permitido nesta concedente é de 24 Meses | Total de ' . $totalFinal . ' Meses. Linha 252 Aditivo.')
                                            ->withInput();

                                    } else {
                                        //Pegando Aditivos de datas desse TCE
                                        $getAditivos = $getTce->aditivos->where('dtInicio','<>',null);//Pegando Aditivo
                                        if($getAditivos->count() > 1){
                                            $vlFinal = 0;
                                            foreach ($getAditivos as $adt => $valor){
                                                $vlFinal += $valor['dtInicio']->diffInMonths($valor['dtFim']);
                                            }

                                            //pegar diferença do ultimo aditivo
                                            $difIncioFimOutrosAditivos = $vlFinal;
                                            //Somando a difirença data aditivo + total do tce cancelado
                                            $totalAdTce = $difIncioFimOutrosAditivos + $totalFinal;
                                            //verificando diferença em mes do inicio aditivo e do fim do aditivo
                                            //$difDataAtivo = $dtInicioAditivo->diffInMonths($dtFimAditivo);
                                            //somando todos
                                            //$totalFinalAditivo = $totalAdTce +$difDataAtivo;
                                            $totalFinalAditivo = $difIncioFimOutrosAditivos + $totalFinal;
                                            //dd('Meses TCE: '.$totalFinal.' Meses ADITIVO: '.$difIncioFimOutrosAditivos.'Total:'.$totalFinalAditivo);
                                            //dd('Meses ADITIVO: '.$difIncioFimOutrosAditivos);

                                            if ($totalFinalAditivo > 24) {//Verificando dif fim com limite
                                                //dd('ERRO, A Data Limite é maior Aditivo | Total é de - ' . $totalFinalAditivo . ' Meses');
                                                return redirect()
                                                    ->back()
                                                    ->with('error', 'ERRO, O total de meses permitido neste concedente é de 24 Meses. | Totalizou ' . $totalFinalAditivo . ' Meses. Linha 280 Aditivo.')
                                                    ->withInput();
                                            }else{
                                                //cadastro de aditivo
                                                $qtdAditivos = Aditivo::where('tce_id', $formulario['tce_id'])->count();
                                                $qtdAditivos++;
                                                $formulario['nnAditivo']= $qtdAditivos;

                                                $new = $this->aditivo->create($formulario);
                                                if(isset($formulario['atividades'])){
                                                    $new->atividades()->attach(array_values($formulario['atividades']));
                                                }
                                            }

                                        }else{ //VERIFICAÇÃO SEM ADITIVOS

                                            //verificando diferença em mes do inicio aditivo e do fim do aditivo
                                            //$difDataAtivo = $dtInicioAditivo->diffInMonths($dtFimAditivo);
                                            //Somando a difirença data aditivo + total do tce cancelado
                                            //$totalFinalAditivo = $difDataAtivo + $totalFinal;
                                            //dd($totalFinal, $totalFinalAditivo);
                                            if ($totalFinal > 24) {//Verificando Total Final
                                                //dd('ERRO, A Data Limite é maior Aditivo | Total é de - ' . $totalFinalAditivo . ' Meses');
                                                return redirect()
                                                    ->back()
                                                    ->with('error', 'ERRO, O total de meses permitido neste concedente é de 24 Meses. | Totalizou ' . $totalFinalAditivo . ' Meses. Linha 305 Aditivo.')
                                                    ->withInput();
                                            }else{
                                                //cadastro de aditivo
                                                $qtdAditivos = Aditivo::where('tce_id', $formulario['tce_id'])->count();
                                                $qtdAditivos++;
                                                $formulario['nnAditivo']= $qtdAditivos;

                                                $new = $this->aditivo->create($formulario);
                                                if(isset($formulario['atividades'])){
                                                    $new->atividades()->attach(array_values($formulario['atividades']));
                                                }
                                            }

                                        }//if Aditivo

                                    }

                                } else { //não houve TCE concedente
                                    //ACHO QUE NUNCA CAIRÁ NESSA CONDIÇÃO, POIS TRATEI OS ADITIVOS NOS CODIGOS ACIMA.
                                    return redirect()
                                    ->back()
                                    ->with('error', 'AÇÃO NÃO EXECUTADA. CONTATE O SETOR DE TECNOLOGIA. Linha 327.')
                                    ->withInput();

                                    $getAditivos = $getTce->aditivos->where('dtInicio','<>',null);//Pegando Aditivo
                                    if($getAditivos->count() > 1){
                                        $vlFinal = 0;
                                        foreach ($getAditivos as $adt => $valor){
                                            $vlFinal += $valor['dtInicio']->diffInMonths($valor['dtFim']);
                                        }

                                        //Pegando Diferença da data Tce
                                        $getDifTce = $getTce->dtInicio->diffInMonths($getTce->dtFim);
                                        //Pegando Diferença da data Aditivo ja Cadastrado
                                        $getDifAditivo = $vlFinal;
                                        //Pegando a diferança form ativo atual
                                        $getDifAditivoNovo = $dtInicioAditivo->diffInMonths($dtFimAditivo);
                                        //Somando as datas DifTCE + difAditivo
                                        $totalData = $getDifTce + $getDifAditivo + $getDifAditivoNovo;

                                        if ($totalData > 24) {//Verificando dif fim com limite
                                            //dd('ERRO, A Data Limite é maior Aditivo | Total é de - ' . $totalData . ' Meses');
                                            return redirect()
                                                ->back()
                                                ->with('error', 'ERRO, A Data Limite é maior | Total é de - ' . $totalFinal . ' Meses')
                                                ->withInput();
                                        }else{
                                            //cadastro de aditivo
                                            $qtdAditivos = Aditivo::where('tce_id', $formulario['tce_id'])->count();
                                            $qtdAditivos++;
                                            $formulario['nnAditivo']= $qtdAditivos;

                                            $new = $this->aditivo->create($formulario);
                                            if(isset($formulario['atividades'])){
                                                $new->atividades()->attach(array_values($formulario['atividades']));
                                            }
                                        }


                                    }else{

                                        //Pegando Diferença da data Tce
                                        $getDifTce = $getTce->dtInicio->diffInMonths($getTce->dtFim);
                                        //Pegando Diferença da data Aditivo
                                        $getDifAditivo = $dtInicioAditivo->diffInMonths($dtFimAditivo);
                                        //Somando as datas DifTCE + difAditivo
                                        $totalData = $getDifTce + $getDifAditivo;

                                        if ($totalData > 24) {//Verificando dif fim com limite
                                            //dd('ERRO, A Data Limite é maior Aditivo | Total é de - ' . $totalData . ' Meses');
                                            return redirect()
                                                ->back()
                                                ->with('error', 'ERRO, A Data Limite é maior | Total é de - ' . $totalFinal . ' Meses')
                                                ->withInput();
                                        }else{
                                            //cadastro de aditivo
                                            $qtdAditivos = Aditivo::where('tce_id', $formulario['tce_id'])->count();
                                            $qtdAditivos++;
                                            $formulario['nnAditivo']= $qtdAditivos;

                                            $new = $this->aditivo->create($formulario);
                                            if(isset($formulario['atividades'])){
                                                $new->atividades()->attach(array_values($formulario['atividades']));
                                            }
                                        }
                                    }
                                }

                            }//fim verificação $totEstudante > $totCurso

                        }
                        BREAK;

                    CASE '2':
                        //Se Aditivo Vinher com Data Inicio vazia, quer dizer que não é aditivo de data, então calcula pelas datas do tce.
                        if($formulario['dtInicio'] == ''){
                            //Pegando o total de tendo do curso
                            $totCurso = $qtdCurso*6;
                            if($formulario['nnSemestreAno'] != ''){
                                $totEstudante = $formulario['nnSemestreAno']*6;
                                //Atualizando o semestre do Estudante
                                //$idEstudanteTce = $getTce->estudante->id;
                                //$dbEstudante = $this->estudante->find($idEstudanteTce);
                                //$dbEstudante->update(['nnSemestreAno' => $formulario['nnSemestreAno']]);
                            }else{
                                $totEstudante = $qtdEstudante*6;
                            }
                            // FIM.  Verificando se há nsemestre no aditivo//

                            $dif = $totCurso - $totEstudante;
                            $dataInicioTCE = $getTce->dtInicio;
                            $final = $dataInicioTCE->addMonth($dif);
                            //$incioAdd = $final;
                            //Caso o estudante esteja no ultimo Semestre
                            if($totCurso == $totEstudante){
                                if($getTce->dtInicio->month <= 6){//pega dtinicio pois é o ultimo semestre pra estagio.
                                    $dtInicio   = Carbon::createFromFormat('d/m/Y', $getTce->dtInicio);
                                    //Primeiro Semestre do Ano
                                    $incioAdd = Carbon::create($dtInicio->year, 6, 30, 0);
                                } else {
                                    //dd($getTce->dtInicio->year);
                                    //$dtInicio   = Carbon::createFromFormat('d/m/Y', $getTce->dtInicio);
                                    //Segundo Semestre do Ano
                                    $incioAdd = Carbon::create($getTce->dtInicio->year, 12, 31, 0);
                                }
                            } else {
                                //Se o mês for menor ou igual 6 entra no 1 semestre do ano
                                if($final->month <= 6){//pega final pois o cara tem mais tempo pra estagio.
                                    //Primeiro Semestre do Ano
                                    $incioAdd = Carbon::create($final->year, 6, 30, 0);
                                } else {
                                    //Segundo Semestre do Ano
                                    $incioAdd = Carbon::create($final->year, 12, 31, 0);
                                }
                            }
                            //dd($totCurso, $totEstudante,$final,$incioAdd);
                            $dtAditivo  = Carbon::createFromFormat('d/m/Y', $formulario['dtAditivo']);
                            //Verifica se o TCE está vencido, se tiver então o aditivo obrigatoriamente precisa ser de data
                            /*if($getTce->dtFim->toDateString() < $dtAditivo->toDateString()){
                                return redirect()
                                    ->back()
                                    ->with('error', 'SEU TCE ESTÁ VENCIDO('.$getTce->dtFim->format('d/m/Y').'). SEU ADITIVO OBRIGATORIAMENTE PRECISA SER DE DATAS. Linha 444 Aditivo.')
                                    ->withInput();
                            }
                            */
                            //if($getTce->dtFim > $incioAdd){
                            //    return redirect()
                            //        ->back()
                            //        ->with('error', 'O FIM DO TCE('.$getTce->dtFim->format('d/m/Y').') É DEPOIS DO FIM DO CURSO('.$incioAdd->format('d/m/Y').') Linha 450 Aditivo.')
                            //        ->withInput();
                            //}else{
                                //cadastro de aditivo
                                $qtdAditivos = Aditivo::where('tce_id', $formulario['tce_id'])->count();
                                $qtdAditivos++;
                                $formulario['nnAditivo']= $qtdAditivos;

                                $new = $this->aditivo->create($formulario);
                                if(isset($formulario['atividades'])){
                                    $new->atividades()->attach(array_values($formulario['atividades']));
                                }
                            //}
                        //Nesse caso o aditivo é de datas então os calculos são feitos pela data do aditivo vindas do formulario
                        }else{
                            //Pegando o total de tempo do curso
                            $totCurso = $qtdCurso*6;
                            if($formulario['nnSemestreAno'] != ''){
                                $totEstudante = $formulario['nnSemestreAno']*6;
                                //Atualizando o semestre do Estudante
                                //$idEstudanteTce = $getTce->estudante->id;
                                //$dbEstudante = $this->estudante->find($idEstudanteTce);
                                //$dbEstudante->update(['nnSemestreAno' => $formulario['nnSemestreAno']]);
                            }else{
                                $totEstudante = $qtdEstudante*6;
                            }
                            // FIM.  Verificando se há nsemestre no aditivo//

                            $dif = $totCurso - $totEstudante;
                            $final = $dtInicio->addMonth($dif);

                            //Caso o estudante esteja no ultimo Semestre
                            if($totCurso == $totEstudante){
                                if($dtInicio->month <= 6){//pega do dtinicio pois o cara tá no ultimo semestre
                                    $dtInicio   = Carbon::createFromFormat('d/m/Y', $formulario['dtInicio']);
                                    //Primeiro Semestre do Ano
                                    $incioAdd = Carbon::create($dtInicio->year, 6, 30, 0);
                                } else {
                                    $dtInicio   = Carbon::createFromFormat('d/m/Y', $formulario['dtInicio']);
                                    //Segundo Semestre do Ano
                                    $incioAdd = Carbon::create($dtInicio->year, 12, 31, 0);
                                }
                            } else {
                                //Se o mês for menor ou igual 6 entra no 1 semestre do ano
                                if($final->month <= 6){//pega do dtfinal pois o cara tem mais tempo para estagio.
                                    //Primeiro Semestre do Ano
                                    $incioAdd = Carbon::create($final->year, 6, 30, 0);
                                } else {
                                    //Segundo Semestre do Ano
                                    $incioAdd = Carbon::create($final->year, 12, 31, 0);
                                }
                            }
                            //$incioAdd = $final->addMonth(6);
                            //Verifica de DtFim é maior que o calculo da previsão de fim de curso.
                            if($dtFim->toDateString() > $incioAdd){
                                return redirect()
                                    ->back()
                                    ->with('error', 'O FIM DO ADITIVO('.$dtFim->format('d/m/Y').') É DEPOIS DO FIM DO CURSO('.$incioAdd->format('d/m/Y').'). Linha 507 Aditivo.')
                                    ->withInput();
                            }else{
                                $dtFimAditivo      = Carbon::createFromFormat('d/m/Y', $formulario['dtFim']) ;
                                $dtInicioAditivo   = Carbon::createFromFormat('d/m/Y', $formulario['dtInicio']);
                                //Verifica se tem TCE com o mesmo concedente para poder calcular o tempo maximo de 24 meses por concedente.
                                if ($getEstudante->tce->where('concedente_id', $getConcedente->id)->all()) {
                                    $dtInicio   = Carbon::createFromFormat('d/m/Y', $formulario['dtInicio']);
                                    //dd($formulario, $getEstudante->tce->where('concedente_id', $formulario['concedente_id']));
                                    $dataTotTrabalhado = 0;
                                    //Verifica se tem TCE com o mesmo concedente para poder calcular o tempo maximo de 24 meses por concedente, retirando no caso o TCE Atual.
                                    if($getEstudante->tce->where('concedente_id', $getConcedente->id)->whereNotIn('id', $getTce->id)->count() > 0){
                                        foreach ($getEstudante->tce->where('concedente_id', $getConcedente->id)->whereNotIn('id', $getTce->id) as $tces) {
                                            //Pegando a diferença de tempo entre o cancelamento e o incio dos tces
                                            $dataTotTrabalhado += $tces->dtInicio->diffInMonths($tces->dtCancelamento);
                                        }
                                    //fim if se tem mais tce que o do aditivo atual que seja do mesmo concedente
                                    }
                                    $esseTce =  $getEstudante->tce->where('id', $getTce->id);
                                    foreach ($esseTce as $eTce) {
                                        //Pegando dtInicio e dtFim do TCE atual e soma com os outros Tces se houver
                                        $dataTotTrabalhado += $eTce->dtInicio->diffInMonths($eTce->dtFim);
                                    }
                                    //pega diferença em meses da dtInicio do formulario e dtFim
                                    $difInicioFim = $dtInicio->diffInMonths($dtFim);
                                    //Soma total trabalhado em todos tces encontrados com diferença do inicio/fim do aditivo sendo criado
                                    $totalFinal = $dataTotTrabalhado + $difInicioFim;
                                    //dd($formulario, $dtInicio, $dtFim, $dataTotTrabalhado,$difInicioFim, $totalFinal);
                                    //Verificando total vindo de cima com limite
                                    if ($totalFinal > 24) {
                                        return redirect()
                                            ->back()
                                            ->with('error', 'ERRO, O total de meses permitido nesta concedente é de 24 Meses | Totalizou ' . $totalFinal . ' Meses. Linha 539 Aditivo.')
                                            ->withInput();
                                    } else {
                                        //Pegando Aditivos de datas desse TCE
                                        $getAditivos = $getTce->aditivos->where('dtInicio','<>',null);
                                        if($getAditivos->count() > 0){
                                            $vlFinal = 0;
                                            foreach ($getAditivos as $adt => $valor){
                                                $vlFinal += $valor['dtInicio']->diffInMonths($valor['dtFim']);
                                            }
                                            //pegar diferença do ultimo aditivo
                                            $difIncioFimOutrosAditivos = $vlFinal;
                                            //Somando a difirença data aditivo + total do tce
                                            $totalAdTce = $difIncioFimOutrosAditivos + $totalFinal;
                                            //verificando diferença em mes do inicio aditivo e do fim do aditivo
                                            //$difDataAtivo = $dtInicioAditivo->diffInMonths($dtFimAditivo);
                                            //somando todos
                                            //$totalFinalAditivo = $totalAdTce +$difDataAtivo;
                                            $totalFinalAditivo = $difIncioFimOutrosAditivos + $totalFinal;
                                            //Verificando se é maior que 24 meses
                                            //dd('Meses TCE: '.$totalFinal.' Meses ADITIVO: '.$difIncioFimOutrosAditivos.'Total:'.$totalFinalAditivo);
                                            //dd('Meses ADITIVO: '.$difIncioFimOutrosAditivos);
                                            if ($totalFinalAditivo > 24) {
                                                //dd('ERRO, A Data Limite é maior Aditivo | Total é de - ' . $totalFinalAditivo . ' Meses');
                                                return redirect()
                                                    ->back()
                                                    ->with('error', 'ERRO, O total de meses permitido nesta concedente é de 24 Meses | Totalizou ' . $totalFinalAditivo . ' Meses. Linha 565 Aditivo.')
                                                    ->withInput();
                                            }else{
                                                //cadastrando  aditivo
                                                $qtdAditivos = Aditivo::where('tce_id', $formulario['tce_id'])->count();
                                                $qtdAditivos++;
                                                $formulario['nnAditivo']= $qtdAditivos;

                                                $new = $this->aditivo->create($formulario);
                                                if(isset($formulario['atividades'])){
                                                    $new->atividades()->attach(array_values($formulario['atividades']));
                                                }
                                            }
                                        //SEM ADITIVOS
                                        }else{
                                            //verificando diferença em mes do inicio aditivo e do fim do aditivo
                                            $difDataAtivo = $dtInicioAditivo->diffInMonths($dtFimAditivo);
                                            //Somando a difirença data aditivo + total do tce cancelado
                                            $totalFinalAditivo = $difDataAtivo + $totalFinal;
                                            //dd($difDataAtivo, $totalFinalAditivo, $totalFinal);
                                            if ($totalFinal > 24) {//Verificando dif fim com limite
                                                //dd('ERRO, A Data Limite é maior Aditivo | Total é de - ' . $totalFinalAditivo . ' Meses');
                                                return redirect()
                                                    ->back()
                                                    ->with('error', 'ERRO, O total de meses permitido neste concedente é de 24 Meses. | Totalizou ' . $totalFinalAditivo . ' Meses. Linha 589 Aditivo.')
                                                    ->withInput();
                                            }else{
                                                //cadastrando aditivo
                                                $qtdAditivos = Aditivo::where('tce_id', $formulario['tce_id'])->count();
                                                $qtdAditivos++;
                                                $formulario['nnAditivo']= $qtdAditivos;

                                                $new = $this->aditivo->create($formulario);
                                                if(isset($formulario['atividades'])){
                                                    $new->atividades()->attach(array_values($formulario['atividades']));
                                                }
                                            }

                                        }//fim do if e else Aditivo
                                    }
                                //não houve TCE da Concedente
                                } else {
                                //ACHO QUE NUNCA CAIRÁ NESSA CONDIÇÃO, POIS TRATEI OS ADITIVOS NOS CODIGOS ACIMA.
                                    return redirect()
                                        ->back()
                                        ->with('error', 'AÇÃO NÃO EXECUTADA. CONTATE O SETOR DE TECNOLOGIA. Linha 610.')
                                        ->withInput();

                                    $getAditivos = $getTce->aditivos->where('dtInicio','<>',NULL);//Pegando Aditivo
                                    //Verifica se esse tce tem aditivos de data.
                                    if($getAditivos->count() > 1){
                                        $vlFinal = 0;
                                        foreach ($getAditivos as $adt => $valor){
                                            $vlFinal += $valor['dtInicio']->diffInMonths($valor['dtFim']);
                                        }
                                        //Pegando Diferença da data Tce
                                        $getDifTce = $getTce->dtInicio->diffInMonths($getTce->dtFim);
                                        //Pegando Diferença da data Aditivo ja Cadastrado
                                        $getDifAditivo = $vlFinal;
                                        //Pegando a diferança form ativo atual
                                        $getDifAditivoNovo = $dtInicioAditivo->diffInMonths($dtFimAditivo);
                                        //Somando as datas DifTCE + difAditivo
                                        $totalData = $getDifTce + $getDifAditivo + $getDifAditivoNovo;
                                        //dd($totalData.'Estou aki 659');
                                        if ($totalData > 24) {//Verificando dif fim com limite
                                            //dd('ERRO, A Data Limite é maior Aditivo | Total é de - ' . $totalData . ' Meses');
                                            return redirect()
                                                ->back()
                                                ->with('error', 'ERRO, A Data Limite é maior | Total é de - ' . $totalData . ' Meses')
                                                ->withInput();

                                        }else{
                                            //cadastro de aditivo
                                            $qtdAditivos = Aditivo::where('tce_id', $formulario['tce_id'])->count();
                                            $qtdAditivos++;
                                            $formulario['nnAditivo']= $qtdAditivos;

                                            $new = $this->aditivo->create($formulario);
                                            if(isset($formulario['atividades'])){
                                                $new->atividades()->attach(array_values($formulario['atividades']));
                                            }
                                        }
                                    //Se não houver aditivo de data nesse tce
                                    }else{
                                        //Pegando Diferença da data Tce
                                        $getDifTce = $getTce->dtInicio->diffInMonths($getTce->dtFim);
                                        //Pegando Diferença da data Aditivo
                                        $getDifAditivo = $dtInicioAditivo->diffInMonths($dtFimAditivo);
                                        //Somando as datas DifTCE + difAditivo
                                        $totalData = $getDifTce + $getDifAditivo;
                                        //dd($totalData.'Estou aki 686');
                                        if ($totalData > 24) {//Verificando dif fim com limite
                                            //dd('ERRO, A Data Limite é maior Aditivo | Total é de - ' . $totalData . ' Meses');
                                            return redirect()
                                                ->back()
                                                ->with('error', 'ERRO, A Data Limite é maior | Total é de - ' . $totalData . ' Meses')
                                                ->withInput();

                                        }else{
                                            //cadastro de aditivo
                                            $qtdAditivos = Aditivo::where('tce_id', $formulario['tce_id'])->count();
                                            $qtdAditivos++;
                                            $formulario['nnAditivo']= $qtdAditivos;

                                            $new = $this->aditivo->create($formulario);
                                            if(isset($formulario['atividades'])){
                                                $new->atividades()->attach(array_values($formulario['atividades']));
                                            }
                                        }
                                    }
                                }
                                //FIM DO ELSE QUE ACHO QUE NUNCA CAIRÁ NELE.

                            }//fim verificação $totEstudante > $totCurso
                        }
                        BREAK;

                    default:
                        /*
                        $dtFimAditivo      = Carbon::createFromFormat('d/m/Y', $formulario['dtFim']) ;
                        $dtInicioAditivo   = Carbon::createFromFormat('d/m/Y', $formulario['dtInicio']);
                        //verificando se existe tce com o mesmo concedente
                        if ($getEstudante->tce->id != $getTce->id  && $getEstudante->tce->concedente_id == $getTce->concedente_id) {

                            //Pegando a diferença de tempo entre o cancelamento e o incio
                            $dataTotTrabalhado = $getEstudante->tce->dtInicio->diffInMonths($getEstudante->tce->dtCancelamento);
                            //Verificando se a dataFim é maior do a Limite e mair que 24
                            $difInicioFim = $getTce->dtInicio->diffInMonths($getTce->dtFim);
                            //Somando o total de meses que tem entre Inicio e Cancelamento + Total de meses do Formulario
                            $totalFinal = $dataTotTrabalhado + $difInicioFim;

                            if ($totalFinal >= 24) {//Verificando dif fim com limite
                                //dd('ERRO, A Data Limite é maior | Total é de - ' . $totalFinal . ' Meses');
                                return redirect()
                                    ->back()
                                    ->with('error', 'ERRO, A Data Limite é maior | Total é de - ' . $totalFinal . ' Meses')
                                    ->withInput();

                            } else {

                                $getAditivos = $getTce->aditivos->where('dtInicio','<>',null);//Pegando Aditivo
                                if($getAditivos->count() > 1){
                                    $vlFinal = 0;
                                    foreach ($getAditivos as $adt => $valor){
                                        $vlFinal += $valor['dtInicio']->diffInMonths($valor['dtFim']);
                                    }

                                    //pegar diferença do ultimo aditivo
                                    $difIncioFimOutrosAditivos = $vlFinal;
                                    //Somando a difirença data aditivo + total do tce cancelado
                                    $totalAdTce = $difIncioFimOutrosAditivos + $totalFinal;
                                    //verificando diferença em mes do inicio aditivo e do fim do aditivo
                                    $difDataAtivo = $dtInicioAditivo->diffInMonths($dtFimAditivo);
                                    //somando todos
                                    $totalFinalAditivo = $totalAdTce +$difDataAtivo;

                                    if ($totalFinalAditivo >= 24) {//Verificando dif fim com limite
                                        //dd('ERRO, A Data Limite é maior Aditivo | Total é de - ' . $totalFinalAditivo . ' Meses');
                                        return redirect()
                                            ->back()
                                            ->with('error', 'ERRO, A Data Limite é maior | Total é de - ' . $totalFinalAditivo . ' Meses')
                                            ->withInput();
                                    }else{
                                        //cadastro de aditivo
                                        $qtdAditivos = Aditivo::where('tce_id', $formulario['tce_id'])->count();
                                        $qtdAditivos++;
                                        $formulario['nnAditivo']= $qtdAditivos;

                                        $new = $this->aditivo->create($formulario);
                                        if(isset($formulario['atividades'])){
                                            $new->atividades()->attach(array_values($formulario['atividades']));
                                        }
                                    }

                                }else{ //VERIFICAÇÃO SEM ADITIVOS

                                    //verificando diferença em mes do inicio aditivo e do fim do aditivo
                                    $difDataAtivo = $dtInicioAditivo->diffInMonths($dtFimAditivo);
                                    //Somando a difirença data aditivo + total do tce cancelado
                                    $totalFinalAditivo = $difDataAtivo + $totalFinal;

                                    if ($totalFinalAditivo >= 24) {//Verificando dif fim com limite
                                        //dd('ERRO, A Data Limite é maior Aditivo | Total é de - ' . $totalFinalAditivo . ' Meses');
                                        return redirect()
                                            ->back()
                                            ->with('error', 'ERRO, A Data Limite é maior | Total é de - ' . $totalFinalAditivo . ' Meses')
                                            ->withInput();
                                    }else{
                                        //cadastro de aditivo
                                        $qtdAditivos = Aditivo::where('tce_id', $formulario['tce_id'])->count();
                                        $qtdAditivos++;
                                        $formulario['nnAditivo']= $qtdAditivos;

                                        $new = $this->aditivo->create($formulario);
                                        if(isset($formulario['atividades'])){
                                            $new->atividades()->attach(array_values($formulario['atividades']));
                                        }
                                    }

                                }//if Aditivo

                            }

                        } else { //não houve TCE oncedente

                            $getAditivos = $getTce->aditivos->where('dtInicio','<>',null);//Pegando Aditivo
                            if($getAditivos->count() > 1){
                                $vlFinal = 0;
                                foreach ($getAditivos as $adt => $valor){
                                    $vlFinal += $valor['dtInicio']->diffInMonths($valor['dtFim']);
                                }

                                //Pegando Diferença da data Tce
                                $getDifTce = $getTce->dtInicio->diffInMonths($getTce->dtFim);
                                //Pegando Diferença da data Aditivo ja Cadastrado
                                $getDifAditivo = $vlFinal;
                                //Pegando a diferança form ativo atual
                                $getDifAditivoNovo = $dtInicioAditivo->diffInMonths($dtFimAditivo);
                                //Somando as datas DifTCE + difAditivo
                                $totalData = $getDifTce + $getDifAditivo + $getDifAditivoNovo;

                                if ($totalData >= 24) {//Verificando dif fim com limite
                                    //dd('ERRO, A Data Limite é maior Aditivo | Total é de - ' . $totalData . ' Meses');
                                    return redirect()
                                        ->back()
                                        ->with('error', 'ERRO, A Data Limite é maior | Total é de - ' . $totalData . ' Meses')
                                        ->withInput();

                                }else{
                                    //cadastro de aditivo
                                    $qtdAditivos = Aditivo::where('tce_id', $formulario['tce_id'])->count();
                                    $qtdAditivos++;
                                    $formulario['nnAditivo']= $qtdAditivos;

                                    $new = $this->aditivo->create($formulario);
                                    if(isset($formulario['atividades'])){
                                        $new->atividades()->attach(array_values($formulario['atividades']));
                                    }
                                }


                            }else{

                                //Pegando Diferença da data Tce
                                $getDifTce = $getTce->dtInicio->diffInMonths($getTce->dtFim);
                                //Pegando Diferença da data Aditivo
                                $getDifAditivo = $dtInicioAditivo->diffInMonths($dtFimAditivo);
                                //Somando as datas DifTCE + difAditivo
                                $totalData = $getDifTce + $getDifAditivo;

                                if ($totalData >= 24) {//Verificando dif fim com limite
                                    //dd('ERRO, A Data Limite é maior Aditivo | Total é de - ' . $totalData . ' Meses');
                                    return redirect()
                                        ->back()
                                        ->with('error', 'ERRO, A Data Limite é maior | Total é de - ' . $totalData . ' Meses')
                                        ->withInput();

                                }else{
                                    //cadastro de aditivo
                                    $qtdAditivos = Aditivo::where('tce_id', $formulario['tce_id'])->count();
                                    $qtdAditivos++;
                                    $formulario['nnAditivo']= $qtdAditivos;

                                    $new = $this->aditivo->create($formulario);
                                    if(isset($formulario['atividades'])){
                                        $new->atividades()->attach(array_values($formulario['atividades']));
                                    }
                                }
                            }
                        }
                        */
                        return redirect()
                        ->back()
                        ->with('error', 'Opção Inativa. Geração de documentos apenas para cursos ANUAL e SEMESTRAL. Contate o administrador do Sistema. Linha: 821 Aditivo')
                        ->withInput();
                        BREAK;
                }
                //1 ANO / 2 SEMESTRE

            }elseif($fimContrato <= $dtFim ){// a data do contrato é finaliza antes
                //dd('O Fim do contrato é antes');
                return redirect()
                    ->back()
                    ->with('error', 'O FIM DO CONTRATO COM A CONCEDENTE ('.\Carbon\Carbon::parse($fimContrato)->format('d/m/Y').') É ANTES DO FIM DO ADITIVO ('.\Carbon\Carbon::parse($dtFim)->format('d/m/Y').'). Linha 831 Aditivo.')
                    ->withInput();
            }
            //-----//FIM. VERIFICANDO INSERT//---//

        } catch (Exception $e) {
            DB::rollback();
            echo $e->getMessage();
        }
        DB::commit();

        /* */
        if($new){
            //Atualizar semestre no cadastro do estudante caso tenha informado o semestre no formulario
            if($formulario['nnSemestreAno'] != ''){
                $idEstudanteTce = $getTce->estudante->id;
                $dbEstudante = $this->estudante->find($idEstudanteTce);
                $dbEstudante->update(['nnSemestreAno' => $formulario['nnSemestreAno']]);
            }
            //Estudante::where(['id' => $formulario['estudante_id']])->update(['tce_id' => $new->id]);
            return redirect()
                        //Redireciona para o cadastro da instituicao quando a mesma é cadastrada.
                        //->route('estudante.tce.show', ['estudante' => $formulario['estudante_id'],'tce' => $new->id]);
                        ->route('tce.aditivo.show', ['tce' => $formulario['tce_id'],'aditivo' => $new->id]);

                        //
                        //->with('success', 'Cadastro Realizado com Sucesso!');
        } else{
            return redirect()
                        ->back()
                        ->with('error', 'Falha ao Cadastrar!')
                        ->withInput();
        }


    }

    public function showConcedenteSecretaria($idConcedente, $id = null){
      $aditivos = new Collection;

      $tces = Tce::where('concedente_id', $idConcedente)->get();
      if($id != null){
        $tces = $tces->where('sec_conc_id', $id);
      }

      foreach($tces as $tce){
        if($tce->aditivos()->count() > 0){
          foreach($tce->aditivos as $aditivo){
            $aditivos->push($aditivo);
          }
        }
      }

      return view('concedente.aditivos.index', compact('aditivos'));
    }

    public function show($idTce, $idAditivo)
    {
        $titulo = "Aditivo";
        $aditivo = $this->aditivo->findOrFail($idAditivo);
        //Verificação de Estado do Usuario
        /*if(Auth::user()->unidade_id != null){//Verificando se recebe unidade
            if($aditivo->estado_id == Auth::user()->unidade->estado_id){//verificando se estado é igual ao estado da unidad
                $aditivo;
            }else{
                return redirect()->route('tce.index');
            }
        }
        */
        //$aditivos = Aditivo::where('tce_id', $idTce)->orderBy('id', 'desc')->get();
        //dd($aditivo);
        return view('tce.aditivo.visualizar', compact('titulo','aditivo'));
    }

    //FORMULARIO DE EDIÇÂO DE ADITIVO//
    public function edit($idTce, $idAditivo)
    {
        $aditivo = $this->aditivo->findOrFail($idAditivo);
        //dd($aditivo);
        $aditivosSemestre = Aditivo::where('tce_id', $idTce)->where('nnSemestreAno','<>', NULL)->get();
        return view('tce.aditivo.editar', compact('aditivo','aditivosSemestre'));
    }

    //LOGICA DE EDIÇÃO DE ADITIVO//
    public function update(Request $request, $idTce, $idAditivo)
    {
        DB::beginTransaction();
        try{
            $aditivo = $this->aditivo->findOrFail($idAditivo);
            $formulario = $request->all();

            //Pegando o Estudante do TCE
            $getEstudante = $this->estudante->findOrFail($formulario['estudante_id']);
            $getTce = $this->tce->findOrFail($formulario['tce_id']);
            $getConcedente = $getTce->concedente;

            //teste semestre
            $qtdCurso = $getTce->cursoDaInstituicao->qtdDuracao;
            $qtdEstudante = $getTce->estudante->nnSemestreAno;

            //-----//VERIFICANDO INSERT//---//
            if($getConcedente->dtFimContrato){
                $fimContrato = Carbon::createFromFormat('Y-m-d', $getConcedente->dtFimContrato);
            }
            if($formulario['dtFim'] != ''){
                $dtFim      = Carbon::createFromFormat('d/m/Y', $formulario['dtFim']) ;
            }
            if($formulario['dtInicio'] != ''){
                $dtInicio   = Carbon::createFromFormat('d/m/Y', $formulario['dtInicio']);
            }

            //verificar curso anteste//
            if($getConcedente->dtFimContrato == null OR $fimContrato > $getTce->dtFim) {//verificando a data do contrato se for null ou maior que data limite

                switch($getTce->cursoDaInstituicao->curso_duracao_id)
                {
                    CASE '1':

                        $totCurso = $qtdCurso*12;
                        $totEstudante = $qtdEstudante*12;
                        $dif = $totCurso - $totEstudante;
                        $final = $dtInicio->addMonth($dif);
                        $incioAdd = $final;

                        if($dtFim > $incioAdd){
                            return redirect()
                                ->back()
                                ->with('error', 'O FIM DO TCE É DEPOIS DO FIM DO CURSO. Linha 955.')
                                ->withInput();
                        }else{
                            $dtFimAditivo      = Carbon::createFromFormat('d/m/Y', $formulario['dtFim']) ;
                            $dtInicioAditivo   = Carbon::createFromFormat('d/m/Y', $formulario['dtInicio']);
                            //verificando se existe tce com o mesmo concedente
                            /*
                            if ($getEstudante->tce->id != $getTce->id  && $getEstudante->tce->concedente_id == $getTce->concedente_id) {

                                //Pegando a diferença de tempo entre o cancelamento e o incio
                                $dataTotTrabalhado = $getEstudante->tce->dtInicio->diffInMonths($getEstudante->tce->dtCancelamento);
                                //Verificando se a dataFim é maior do a Limite e mair que 24
                                $difInicioFim = $getTce->dtInicio->diffInMonths($getTce->dtFim);
                                //Somando o total de meses que tem entre Inicio e Cancelamento + Total de meses do Formulario
                                $totalFinal = $dataTotTrabalhado + $difInicioFim;

                                if ($totalFinal > 24) {//Verificando dif fim com limite
                                    //dd('ERRO, A Data Limite é maior | Total é de - ' . $totalFinal . ' Meses');
                                    return redirect()
                                        ->back()
                                        ->with('error', 'ERRO, A Data Limite é maior | Total é de - ' . $totalFinal . ' Meses')
                                        ->withInput();
                            */
                            if ($getEstudante->tce->where('concedente_id', $getConcedente->id)->all()) {
                                $dtInicio   = Carbon::createFromFormat('d/m/Y', $formulario['dtInicio']);
                                //dd($formulario, $getEstudante->tce->where('concedente_id', $formulario['concedente_id']));
                                //$totalFinal = 0;
                                $dataTotTrabalhado = 0;
                                //dd($getEstudante->tce->where('concedente_id', $getConcedente->id)->whereNotIn('id', $getTce->id ));
                                //foreach ($getEstudante->tce->where('concedente_id', $getConcedente->id) as $tces) {
                                //dd($getEstudante->tce->where('concedente_id', $getConcedente->id)->whereNotIn('id', $getTce->id)->count());

                                if($getEstudante->tce->where('concedente_id', $getConcedente->id)->whereNotIn('id', $getTce->id)->count() > 0){
                                    foreach ($getEstudante->tce->where('concedente_id', $getConcedente->id)->whereNotIn('id', $getTce->id) as $tces) {
                                        //Pegando a diferença de tempo entre o cancelamento e o incio dos tces
                                        $dataTotTrabalhado += $tces->dtInicio->diffInMonths($tces->dtCancelamento);
                                        //Verificando se a dataFim é maior do a Limite e mair que 24
                                        //$difInicioFim = $tces->dtInicio->diffInMonths($dtFim);
                                        //Somando o total de meses que tem entre Inicio e Cancelamento + Total de meses do Formulario
                                        //$totalFinal = $dataTotTrabalhado + $difInicioFim;
                                        //$totalFinal += $dataTotTrabalhado;
                                    }
                                //fim if se tem mais tce que o do aditivo atual que seja do mesmo concedente
                                }
                                $esseTce =  $getEstudante->tce->where('id', $getTce->id);
                                foreach ($esseTce as $eTce) {
                                    //Pegando dtInicio e dtFim do TCE atual e soma com os outros Tces se houver
                                    $dataTotTrabalhado += $eTce->dtInicio->diffInMonths($eTce->dtFim);
                                    //Verificando se a dataFim é maior do a Limite e mair que 24
                                    //$difInicioFim = $tces->dtInicio->diffInMonths($dtFim);
                                    //Somando o total de meses que tem entre Inicio e Cancelamento + Total de meses do Formulario
                                    //$totalFinal = $dataTotTrabalhado + $difInicioFim;
                                    //$totalFinal += $dataTotTrabalhado;
                                }
                                //dd($esseTce->get());
                                //$dataTotTrabalhado += $getEstudante->tce->where('id', $getTce->id)->dtInicio->diffInMonths()
                                //dd($dataTotTrabalhado);
                                //pega diferença em meses da dtInicio do formulario e dtFim
                                $difInicioFim = $dtInicio->diffInMonths($dtFim);
                                //Soma total trabalhado em todos tces encontrados com difInicioFim
                                $totalFinal = $dataTotTrabalhado + $difInicioFim;

                                //dd($formulario, $dtInicio, $dtFim, $dataTotTrabalhado,$difInicioFim, $totalFinal);

                                if ($totalFinal > 24) {//Verificando dif fim com limite
                                    //dd('ERRO, A Data Limite é maior | Total é de - ' . $totalFinal . ' Meses');
                                    return redirect()
                                        ->back()
                                        ->with('error', 'ERRO, O total de meses permitido nesta concedente é de 24 Meses | Total de ' . $totalFinal . ' Meses. Linha 1023.')
                                        ->withInput();
                                } else {

                                    $getAditivos = $getTce->aditivos->where('dtInicio','<>',null);//Pegando Aditivo
                                    if($getAditivos->count() > 1){
                                        $vlFinal = 0;
                                        foreach ($getAditivos as $adt => $valor){
                                            $vlFinal += $valor['dtInicio']->diffInMonths($valor['dtFim']);
                                        }

                                        //pegar diferença do ultimo aditivo
                                        $difIncioFimOutrosAditivos = $vlFinal;
                                        //Somando a difirença data aditivo + total do tce cancelado
                                        $totalAdTce = $difIncioFimOutrosAditivos + $totalFinal;
                                        //verificando diferença em mes do inicio aditivo e do fim do aditivo
                                        $difDataAtivo = $dtInicioAditivo->diffInMonths($dtFimAditivo);
                                        //somando todos
                                        $totalFinalAditivo = $totalAdTce +$difDataAtivo;

                                        if ($totalFinalAditivo > 24) {//Verificando dif fim com limite
                                            //dd('ERRO, A Data Limite é maior Aditivo | Total é de - ' . $totalFinalAditivo . ' Meses');
                                            return redirect()
                                                ->back()
                                                ->with('error', 'ERRO, A Data Limite é maior | Total é de - ' . $totalFinalAditivo . ' Meses. Linha 1047')
                                                ->withInput();
                                        }else{
                                            //atualizar aditivo
                                            $atualizar = $aditivo->update($formulario);
                                        }

                                    }else{ //VERIFICAÇÃO SEM ADITIVOS

                                        //verificando diferença em mes do inicio aditivo e do fim do aditivo
                                        $difDataAtivo = $dtInicioAditivo->diffInMonths($dtFimAditivo);
                                        //Somando a difirença data aditivo + total do tce cancelado
                                        $totalFinalAditivo = $difDataAtivo + $totalFinal;

                                        if ($totalFinalAditivo > 24) {//Verificando dif fim com limite
                                            //dd('ERRO, A Data Limite é maior Aditivo | Total é de - ' . $totalFinalAditivo . ' Meses');
                                            return redirect()
                                                ->back()
                                                ->with('error', 'ERRO, A Data Limite é maior | Total é de - ' . $totalFinalAditivo . ' Meses. Linha 1065')
                                                ->withInput();
                                        }else{
                                            //atualizar de aditivo
                                            $atualizar = $aditivo->update($formulario);
                                        }

                                    }//if Aditivo

                                }

                            } else { //não houve TCE concedente

                                $getAditivos = $getTce->aditivos->where('dtInicio','<>',null);//Pegando Aditivo
                                if($getAditivos->count() > 1){
                                    $vlFinal = 0;
                                    foreach ($getAditivos as $adt => $valor){
                                        $vlFinal += $valor['dtInicio']->diffInMonths($valor['dtFim']);
                                    }

                                    //Pegando Diferença da data Tce
                                    $getDifTce = $getTce->dtInicio->diffInMonths($getTce->dtFim);
                                    //Pegando Diferença da data Aditivo ja Cadastrado
                                    $getDifAditivo = $vlFinal;
                                    //Pegando a diferança form ativo atual
                                    $getDifAditivoNovo = $dtInicioAditivo->diffInMonths($dtFimAditivo);
                                    //Somando as datas DifTCE + difAditivo
                                    $totalData = $getDifTce + $getDifAditivo + $getDifAditivoNovo;

                                    if ($totalData > 24) {//Verificando dif fim com limite
                                        //dd('ERRO, A Data Limite é maior Aditivo | Total é de - ' . $totalData . ' Meses');
                                        return redirect()
                                            ->back()
                                            ->with('error', 'ERRO, A Data Limite é maior | Total é de - ' . $totalData . ' Meses. Linha 1098')
                                            ->withInput();

                                    }else{
                                        //atualizar de aditivo
                                        $atualizar = $aditivo->update($formulario);
                                    }


                                }else{

                                    //Pegando Diferença da data Tce
                                    $getDifTce = $getTce->dtInicio->diffInMonths($getTce->dtFim);
                                    //Pegando Diferença da data Aditivo
                                    $getDifAditivo = $dtInicioAditivo->diffInMonths($dtFimAditivo);
                                    //Somando as datas DifTCE + difAditivo
                                    $totalData = $getDifTce + $getDifAditivo;

                                    if ($totalData > 24) {//Verificando dif fim com limite
                                        //dd('ERRO, A Data Limite é maior Aditivo | Total é de - ' . $totalData . ' Meses');
                                        return redirect()
                                            ->back()
                                            ->with('error', 'ERRO, A Data Limite é maior | Total é de - ' . $totalData . ' Meses.Linha 1120')
                                            ->withInput();

                                    }else{
                                        //atualizar de aditivo
                                        $atualizar = $aditivo->update($formulario);
                                    }
                                }
                            }

                        }//fim verificação $totEstudante > $totCurso

                        BREAK;

                    CASE '2':
                        $totCurso = $qtdCurso*6;
                        $totEstudante = $qtdEstudante*6;
                        $dif = $totCurso - $totEstudante;
                        $final = $dtInicio->addMonth($dif);
                        $incioAdd = $final;

                        if($dtFim > $incioAdd){
                            return redirect()
                            ->back()
                            ->with('error', 'O FIM DO TCE É DEPOIS DO FIM DO CURSO. Linha 1144.')
                            ->withInput();
                        }else{
                            $dtFimAditivo      = Carbon::createFromFormat('d/m/Y', $formulario['dtFim']) ;
                            $dtInicioAditivo   = Carbon::createFromFormat('d/m/Y', $formulario['dtInicio']);
                            //verificando se existe tce com o mesmo concedente
                            /*
                            if ($getEstudante->tce->id != $getTce->id  && $getEstudante->tce->concedente_id == $getTce->concedente_id) {

                                //Pegando a diferença de tempo entre o cancelamento e o incio
                                $dataTotTrabalhado = $getEstudante->tce->dtInicio->diffInMonths($getEstudante->tce->dtCancelamento);
                                //Verificando se a dataFim é maior do a Limite e mair que 24
                                $difInicioFim = $getTce->dtInicio->diffInMonths($getTce->dtFim);
                                //Somando o total de meses que tem entre Inicio e Cancelamento + Total de meses do Formulario
                                $totalFinal = $dataTotTrabalhado + $difInicioFim;

                                if ($totalFinal >= 24) {//Verificando dif fim com limite
                                    //dd('ERRO, A Data Limite é maior | Total é de - ' . $totalFinal . ' Meses');
                                    return redirect()
                                        ->back()
                                        ->with('error', 'ERRO, A Data Limite é maior | Total é de - ' . $totalFinal . ' Meses')
                                        ->withInput();
                            */
                            if ($getEstudante->tce->where('concedente_id', $getConcedente->id)->all()) {
                                $dtInicio   = Carbon::createFromFormat('d/m/Y', $formulario['dtInicio']);
                                //dd($formulario, $getEstudante->tce->where('concedente_id', $formulario['concedente_id']));
                                //$totalFinal = 0;
                                $dataTotTrabalhado = 0;
                                //dd($getEstudante->tce->where('concedente_id', $getConcedente->id)->whereNotIn('id', $getTce->id ));
                                //foreach ($getEstudante->tce->where('concedente_id', $getConcedente->id) as $tces) {
                                //dd($getEstudante->tce->where('concedente_id', $getConcedente->id)->whereNotIn('id', $getTce->id)->count());

                                if($getEstudante->tce->where('concedente_id', $getConcedente->id)->whereNotIn('id', $getTce->id)->count() > 0){
                                    foreach ($getEstudante->tce->where('concedente_id', $getConcedente->id)->whereNotIn('id', $getTce->id) as $tces) {
                                        //Pegando a diferença de tempo entre o cancelamento e o incio dos tces
                                        $dataTotTrabalhado += $tces->dtInicio->diffInMonths($tces->dtCancelamento);
                                        //Verificando se a dataFim é maior do a Limite e mair que 24
                                        //$difInicioFim = $tces->dtInicio->diffInMonths($dtFim);
                                        //Somando o total de meses que tem entre Inicio e Cancelamento + Total de meses do Formulario
                                        //$totalFinal = $dataTotTrabalhado + $difInicioFim;
                                        //$totalFinal += $dataTotTrabalhado;
                                    }
                                //fim if se tem mais tce que o do aditivo atual que seja do mesmo concedente
                                }
                                $esseTce =  $getEstudante->tce->where('id', $getTce->id);
                                foreach ($esseTce as $eTce) {
                                    //Pegando dtInicio e dtFim do TCE atual e soma com os outros Tces se houver
                                    $dataTotTrabalhado += $eTce->dtInicio->diffInMonths($eTce->dtFim);
                                    //Verificando se a dataFim é maior do a Limite e mair que 24
                                    //$difInicioFim = $tces->dtInicio->diffInMonths($dtFim);
                                    //Somando o total de meses que tem entre Inicio e Cancelamento + Total de meses do Formulario
                                    //$totalFinal = $dataTotTrabalhado + $difInicioFim;
                                    //$totalFinal += $dataTotTrabalhado;
                                }
                                //dd($esseTce->get());
                                //$dataTotTrabalhado += $getEstudante->tce->where('id', $getTce->id)->dtInicio->diffInMonths()
                                //dd($dataTotTrabalhado);
                                //pega diferença em meses da dtInicio do formulario e dtFim
                                $difInicioFim = $dtInicio->diffInMonths($dtFim);
                                //Soma total trabalhado em todos tces encontrados com difInicioFim fo formulario
                                $totalFinal = $dataTotTrabalhado + $difInicioFim;

                                //dd($formulario, $dtInicio, $dtFim, $dataTotTrabalhado,$difInicioFim, $totalFinal);


                                if ($totalFinal > 24) {//Verificando dif fim com limite
                                    //dd('ERRO, A Data Limite é maior | Total é de - ' . $totalFinal . ' Meses');
                                    return redirect()
                                        ->back()
                                        ->with('error', 'ERRO, O total de meses permitido nesta concedente é de 24 Meses | Total de ' . $totalFinal . ' Meses. Linha 1213')
                                        ->withInput();
                                } else {
                                    //agora vamos contar os aditivos
                                    //$getAditivos = $getTce->aditivos->where('dtInicio','<>',null);//Pegando Aditivo
                                    $getAditivos = $getTce->aditivos->where('dtInicio','<>',NULL)
                                    ->whereNotIn('id', $idAditivo);
                                    //$idAditivo
                                    if($getAditivos->count() > 0){
                                        $vlFinal = 0;
                                        foreach ($getAditivos as $adt => $valor){
                                            $vlFinal += $valor['dtInicio']->diffInMonths($valor['dtFim']);
                                        }

                                        //pegar diferença do ultimo aditivo
                                        $difIncioFimOutrosAditivos = $vlFinal;
                                        //Somando a difirença data aditivo + total do tce vindo de cima
                                        $totalAdTce = $difIncioFimOutrosAditivos + $totalFinal;
                                        //verificando diferença em mes do inicio aditivo e do fim do aditivo
                                        $difDataAtivo = $dtInicioAditivo->diffInMonths($dtFimAditivo);
                                        //somando todos
                                        $totalFinalAditivo = $totalAdTce + $difDataAtivo;
                                        //dd('Soma dos Aditivos Achados: '.$difIncioFimOutrosAditivos.' | Soma Aditivo e TCE: '. $totalAdTce.' | Total TCE: '.$totalFinal.' | Diferença Aditivo Inicio e Fim:'.$difDataAtivo.'| Soma Total:'. $totalFinalAditivo);
                                        //if ($totalFinalAditivo > 24) {//Verificando dif fim com limite
                                        if ($totalAdTce > 24) {//Verificando total aditivos + tce
                                            //dd('ERRO, A Data Limite é maior Aditivo | Total é de - ' . $totalFinalAditivo . ' Meses');
                                            return redirect()
                                                ->back()
                                                ->with('error', 'ERRO, A Data Limite é maior | Total é de - ' . $totalFinalAditivo . ' Meses. Linha 1241')
                                                ->withInput();

                                        }else{
                                            ///atualizar de aditivo
                                            $atualizar = $aditivo->update($formulario);
                                        }

                                    }else{ //VERIFICAÇÃO SEM ADITIVOS

                                        //verificando diferença em mes do inicio aditivo e do fim do aditivo
                                        $difDataAtivo = $dtInicioAditivo->diffInMonths($dtFimAditivo);
                                        //Somando a difirença data aditivo + total do tce cancelado
                                        $totalFinalAditivo = $difDataAtivo + $totalFinal;

                                        if ($totalFinalAditivo > 24) {//Verificando dif fim com limite
                                            //dd('ERRO, A Data Limite é maior Aditivo | Total é de - ' . $totalFinalAditivo . ' Meses');
                                            return redirect()
                                                ->back()
                                                ->with('error', 'ERRO, A Data Limite é maior | Total é de - ' . $totalFinalAditivo . ' Meses. Linha 1260')
                                                ->withInput();

                                        }else{
                                            //atualizar de aditivo
                                            $atualizar = $aditivo->update($formulario);
                                        }

                                    }//if Aditivo

                                }

                            } else { //não houve TCE concedente

                                $getAditivos = $getTce->aditivos->where('dtInicio','<>',NULL);//Pegando Aditivo
                                if($getAditivos->count() > 1){
                                    $vlFinal = 0;
                                    foreach ($getAditivos as $adt => $valor){
                                        $vlFinal += $valor['dtInicio']->diffInMonths($valor['dtFim']);
                                    }

                                    //Pegando Diferença da data Tce
                                    $getDifTce = $getTce->dtInicio->diffInMonths($getTce->dtFim);
                                    //Pegando Diferença da data Aditivo ja Cadastrado
                                    $getDifAditivo = $vlFinal;
                                    //Pegando a diferança form ativo atual
                                    $getDifAditivoNovo = $dtInicioAditivo->diffInMonths($dtFimAditivo);
                                    //Somando as datas DifTCE + difAditivo
                                    $totalData = $getDifTce + $getDifAditivo + $getDifAditivoNovo;

                                    if ($totalData > 24) {//Verificando dif fim com limite
                                        //dd('ERRO, A Data Limite é maior Aditivo | Total é de - ' . $totalData . ' Meses');
                                        return redirect()
                                            ->back()
                                            ->with('error', 'ERRO, A Data Limite é maior | Total é de - ' . $totalData . ' Meses. Linha 1294')
                                            ->withInput();

                                    }else{
                                        //atualizar de aditivo
                                        $atualizar = $aditivo->update($formulario);
                                    }

                                }else{

                                    //Pegando Diferença da data Tce
                                    $getDifTce = $getTce->dtInicio->diffInMonths($getTce->dtFim);
                                    //Pegando Diferença da data Aditivo
                                    $getDifAditivo = $dtInicioAditivo->diffInMonths($dtFimAditivo);
                                    //Somando as datas DifTCE + difAditivo
                                    $totalData = $getDifTce + $getDifAditivo;

                                    if ($totalData > 24) {//Verificando dif fim com limite
                                        //dd('ERRO, A Data Limite é maior Aditivo | Total é de - ' . $totalData . ' Meses');
                                        return redirect()
                                            ->back()
                                            ->with('error', 'ERRO, A Data Limite é maior | Total é de - ' . $totalData . ' Meses. Linha 1315')
                                            ->withInput();

                                    }else{
                                        //atualizar de aditivo
                                        $atualizar = $aditivo->update($formulario);
                                    }
                                }
                            }

                        }//fim verificação $totEstudante > $totCurso

                        BREAK;

                    default:
                        /*
                        $dtFimAditivo      = Carbon::createFromFormat('d/m/Y', $formulario['dtFim']) ;
                        $dtInicioAditivo   = Carbon::createFromFormat('d/m/Y', $formulario['dtInicio']);
                        //verificando se existe tce com o mesmo concedente
                        if ($getEstudante->tce->id != $getTce->id  && $getEstudante->tce->concedente_id == $getTce->concedente_id) {

                            //Pegando a diferença de tempo entre o cancelamento e o incio
                            $dataTotTrabalhado = $getEstudante->tce->dtInicio->diffInMonths($getEstudante->tce->dtCancelamento);
                            //Verificando se a dataFim é maior do a Limite e mair que 24
                            $difInicioFim = $getTce->dtInicio->diffInMonths($getTce->dtFim);
                            //Somando o total de meses que tem entre Inicio e Cancelamento + Total de meses do Formulario
                            $totalFinal = $dataTotTrabalhado + $difInicioFim;

                            if ($totalFinal >= 24) {//Verificando dif fim com limite
                                //dd('ERRO, A Data Limite é maior | Total é de - ' . $totalFinal . ' Meses');
                                return redirect()
                                    ->back()
                                    ->with('error', 'ERRO, A Data Limite é maior | Total é de - ' . $totalFinal . ' Meses')
                                    ->withInput();

                            } else {

                                $getAditivos = $getTce->aditivos->where('dtInicio','<>',null);//Pegando Aditivo
                                if($getAditivos->count() > 1){
                                    $vlFinal = 0;
                                    foreach ($getAditivos as $adt => $valor){
                                        $vlFinal += $valor['dtInicio']->diffInMonths($valor['dtFim']);
                                    }

                                    //pegar diferença do ultimo aditivo
                                    $difIncioFimOutrosAditivos = $vlFinal;
                                    //Somando a difirença data aditivo + total do tce cancelado
                                    $totalAdTce = $difIncioFimOutrosAditivos + $totalFinal;
                                    //verificando diferença em mes do inicio aditivo e do fim do aditivo
                                    $difDataAtivo = $dtInicioAditivo->diffInMonths($dtFimAditivo);
                                    //somando todos
                                    $totalFinalAditivo = $totalAdTce +$difDataAtivo;

                                    if ($totalFinalAditivo >= 24) {//Verificando dif fim com limite
                                        //dd('ERRO, A Data Limite é maior Aditivo | Total é de - ' . $totalFinalAditivo . ' Meses');
                                        return redirect()
                                            ->back()
                                            ->with('error', 'ERRO, A Data Limite é maior | Total é de - ' . $totalFinalAditivo . ' Meses')
                                            ->withInput();

                                    }else{
                                        //atualizar de aditivo
                                        $atualizar = $aditivo->update($formulario);
                                    }

                                }else{ //VERIFICAÇÃO SEM ADITIVOS

                                    //verificando diferença em mes do inicio aditivo e do fim do aditivo
                                    $difDataAtivo = $dtInicioAditivo->diffInMonths($dtFimAditivo);
                                    //Somando a difirença data aditivo + total do tce cancelado
                                    $totalFinalAditivo = $difDataAtivo + $totalFinal;

                                    if ($totalFinalAditivo >= 24) {//Verificando dif fim com limite
                                       // dd('ERRO, A Data Limite é maior Aditivo | Total é de - ' . $totalFinalAditivo . ' Meses');
                                        return redirect()
                                            ->back()
                                            ->with('error', 'ERRO, A Data Limite é maior | Total é de - ' . $totalFinalAditivo . ' Meses')
                                            ->withInput();

                                    }else{
                                        //atualizar de aditivo
                                        $atualizar = $aditivo->update($formulario);
                                    }

                                }//if Aditivo

                            }

                        } else { //não houve TCE oncedente

                            $getAditivos = $getTce->aditivos->where('dtInicio','<>',null);//Pegando Aditivo
                            if($getAditivos->count() > 1){
                                $vlFinal = 0;
                                foreach ($getAditivos as $adt => $valor){
                                    $vlFinal += $valor['dtInicio']->diffInMonths($valor['dtFim']);
                                }

                                //Pegando Diferença da data Tce
                                $getDifTce = $getTce->dtInicio->diffInMonths($getTce->dtFim);
                                //Pegando Diferença da data Aditivo ja Cadastrado
                                $getDifAditivo = $vlFinal;
                                //Pegando a diferança form ativo atual
                                $getDifAditivoNovo = $dtInicioAditivo->diffInMonths($dtFimAditivo);
                                //Somando as datas DifTCE + difAditivo
                                $totalData = $getDifTce + $getDifAditivo + $getDifAditivoNovo;

                                if ($totalData >= 24) {//Verificando dif fim com limite
                                    //dd('ERRO, A Data Limite é maior Aditivo | Total é de - ' . $totalData . ' Meses');
                                    return redirect()
                                        ->back()
                                        ->with('error', 'ERRO, A Data Limite é maior | Total é de - ' . $totalData . ' Meses')
                                        ->withInput();

                                }else{
                                    //atualizar de aditivo
                                    $atualizar = $aditivo->update($formulario);
                                }


                            }else{

                                //Pegando Diferença da data Tce
                                $getDifTce = $getTce->dtInicio->diffInMonths($getTce->dtFim);
                                //Pegando Diferença da data Aditivo
                                $getDifAditivo = $dtInicioAditivo->diffInMonths($dtFimAditivo);
                                //Somando as datas DifTCE + difAditivo
                                $totalData = $getDifTce + $getDifAditivo;

                                if ($totalData >= 24) {//Verificando dif fim com limite
                                    //dd('ERRO, A Data Limite é maior Aditivo | Total é de - ' . $totalData . ' Meses');
                                    return redirect()
                                        ->back()
                                        ->with('error', 'ERRO, A Data Limite é maior | Total é de - ' . $totalData . ' Meses')
                                        ->withInput();

                                }else{
                                    //atualizar de aditivo
                                    $atualizar = $aditivo->update($formulario);
                                }
                            }
                        }
                        */
                        return redirect()
                        ->back()
                        ->with('error', 'Opção Inativa. Geração de documentos apenas para cursos ANUAL e SEMESTRAL. Contate o administrador do Sistema. Linha: 1459 Aditivo')
                        ->withInput();
                        BREAK;
                }
                //1 ANO / 2 SEMESTRE / 3 MENSAL //

            }elseif($fimContrato <= $dtFim ){// a data do contrato é finaliza antes

                //dd('O Fim do contrato é antes');
                return redirect()
                    ->back()
                    ->with('error', 'O Fim do contrato é antes')
                    ->withInput();
            }
            //-----//FIM. VERIFICANDO INSERT//---//


        } catch (Exception $e) {
            DB::rollback();
            echo $e->getMessage();
        }
        DB::commit();


        if($atualizar){
            if(isset($formulario['atividades']))
                //atualizar atividades selecionados para este tce
                $aditivo->atividades()->sync(array_values($formulario['atividades']));
            return redirect()
                       ->route('tce.aditivo.show', ['tce' => $idTce,'aditivo' => $idAditivo ])
                       ->with('success', 'Aditivo Atualizado com Sucesso!');
        } else
            return redirect()
                        ->back()
                        ->with('error', 'Falha ao Atualizar!');
    }

    public function showRelatorio($idTce,$idAditivo)
    {
        $aditivo = $this->aditivo->findOrFail($idAditivo);


        //$valor = extenso($tce->vlAuxilioMensal);
        //dd($aditivo->periodo);
            //dd($relatorio->nmRelatorio);
    //return view('tce.aditivo.relatorio.aditivo', compact('aditivo'));

        /* */
        $pdf = PDF::loadView('tce.aditivo.relatorio.aditivo', compact('aditivo'))
            ->setPaper('a4')
            ->setOptions(['dpi' => 100]);
        return $pdf->stream();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Aditivo  $aditivo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Aditivo $aditivo)
    {
        //
    }
}
