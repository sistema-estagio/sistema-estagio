<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use App\Models\Instituicao;
use App\Models\Curso;
use App\Models\CursoDuracao;
use App\Models\CursoInstituicao;
use App\Models\ConveniosIe;
use App\Models\Turno;
use App\Models\Estado;
use App\Models\Cidade;
use App\Models\Polo;
use App\Models\Tce;
use App\Models\Estudante;
use Auth;
use Validator;

use Illuminate\Support\Facades\DB;

class InstituicaoController extends Controller
{
    private $instituicao;

    public function __construct(Instituicao $instituicao)
    {
        $this->instituicao = $instituicao;
    }

    public function index()
    {
        $titulo = "Listagem de Instituições de Ensino";

        //Verificação de Estado do Usuario
        if(Auth::user()->unidade_id != null){
            $relEstado = Auth::user()->unidade->estado->pluck('id')->toArray();
            $instituicoes = $this->instituicao
                ->WhereIn('estado_id',$relEstado)
                ->orWhere('isEad', 'S')->get();
                //->paginate(config('sysGlobal.Paginacao'));
        }else{
            //$instituicoes = $this->instituicao->paginate(config('sysConfig.Paginacao'));
            $instituicoes = $this->instituicao->get();
        }

        return view('instituicao.index', compact('instituicoes','titulo'));
    }

    public function create(Request $request)
    {
        $estados = Estado::all();
        $polos = Polo::all();
        $titulo = "Cadastro de Instituição de Ensino";
        $cidades = [];

        return View('instituicao.cadastro', [
            'estados' => $estados,
            'polos' => $polos,
            'titulo' => $titulo,
            'cidades' => $request->session()->get('cidades') ?? []
        ]);
        //return view('instituicao.cadastro', compact('estados','titulo','polos'));
    }
    //
    public function store(Request $request)
    {
        $formulario = $request->all();
        //pega usuario logado e seta o id do usuario ao cadastro da instituição, para sabermos quem cadastrou a instituição
        $formulario['usuario_id'] = Auth::user()->id;
        
        $validar= Validator::make($request->all(), [
            'isEad' => 'required',
            'nmInstituicao' => 'required',
            'nmFantasia' => 'required',
            'cdCNPJ' => 'required|unique:instituicoes|cnpj',
            'cdCEP' => 'required',
            'dsEndereco' => 'required',
            'nnNumero' => 'required',
            'nmBairro' => 'required',
            'estado_id' => 'required',
            'cidade_id' => 'required',
            'dsEmail' => 'required',
        ],[
            'isEad.required' => ' O campo Tipo é Obrigatório.',
            'nmInstituicao.required' => ' O campo Razão Social é Obrigatório.',
            'nmFantasia.required' => ' O campo Nome Fantasia é obrigatório.',
            'cdCNPJ.required' => ' O campo CNPJ é obrigatório.',
            'cdCNPJ.unique' => ' Já existe uma instituição com esse numero de CNPJ.',
            'cdCNPJ.cnpj' => ' CNPJ Inválido',
            'cdCEP.required' => ' O campo CEP é obrigatório.',
            'dsEndereco.required' => ' O campo Endereço é obrigatório.',
            'nnNumero.required' => ' O campo Numero é obrigatório.',
            'nmBairro.required' => ' O campo Bairro é obrigatório.',
            'estado_id.required' => ' O campo Estado é obrigatório.',
            'cidade_id.required' => ' O campo Cidade é obrigatório.',
            'dsEmail.required' => ' O campo Email é obrigatório.',
        ]);

        if($validar->fails()) {
            $cidades = Cidade::where('estado_id', $formulario['estado_id'])->get();
            return 
            //dd($validar, $validar->any());
            redirect()
            ->back()
            //->route('estudante.create')
            ->with('cidades', $cidades)
            ->withErrors($validar)
            ->withInput();
        }
        
        $new = $this->instituicao->create($formulario);
        if($new)
            return redirect()
                    //Redireciona para o cadastro da instituicao quando a mesma é cadastrada.
                    ->route('instituicao.show', ['instituicao' => $new->id])
                    //
                    ->with('success', 'Cadastro da Instituição Realizado com Sucesso!');
        else
            $cidades = Cidade::where('estado_id', $formulario['estado_id'])->get();
            return redirect()
                    ->back()
                    ->with('error', 'Falha ao Cadastrar Instituição!')
                    ->with('cidades', $cidades)
                    ->withInput();
    }

    public function search(Request $request)
    {
       $dataForm = $request->except('_token');
       $instituicoes = $this->instituicao->search($request->key_search, config('sysConfig.Paginacao'));

       return view('instituicao.index', compact('instituicoes', 'dataForm'));
    }

    public function show($id)
    {
        $titulo = "Instituição de Ensino";
        $listaCursos = Curso::all()->sortBy('nmCurso');
        $cursoDuracoes = CursoDuracao::all()->sortBy('nmDuracao');
        $turnos = Turno::all();
        $convenio = ConveniosIe::where('instituicao_id',$id)->get();
        //tces ativos dessa instituição
        $tcesAtivos = Tce::where('instituicao_id', $id)->where('dtCancelamento', '=', NULL)->get();
        //estudantes cadastrados no sistema dessa instituição
        $estudantes = Estudante::where('instituicao_id', $id)->get();
        $instituicao = $this->instituicao->findOrFail($id);
        if(Auth::user()->unidade_id != null){//Verificando se recebe unidade
            $relEstado = Auth::user()->unidade->estado->pluck('id')->toArray();
            //if(in_array($instituicao->estado_id,$relEstado))                
            if((in_array($instituicao->estado_id,$relEstado)) OR ($instituicao->isEad == "S")){//verificando se estado é igual ao estado da unidad
                $instituicao;
            }else{
                return redirect()->back();
            }
        }

        return view('instituicao.visualizar', compact('titulo','instituicao','listaCursos','cursoDuracoes','turnos','convenio','tcesAtivos','estudantes'));
    }

    public function historico($id)
    {
        $titulo = "Instituição de Ensino";
        $instituicao = $this->instituicao->findOrFail($id);
        $logs = $instituicao->audits()->orderBy('created_at', 'desc')->paginate(config('sysConfig.Paginacao'));
        //dd($log);
        if(Auth::user()->unidade_id != null){//Verificando se recebe unidade
            $relEstado = Auth::user()->unidade->estado->pluck('id')->toArray();
            //if(in_array($instituicao->estado_id,$relEstado)){//verificando se estado é igual ao estado da unidad
            if((in_array($instituicao->estado_id,$relEstado)) OR ($instituicao->isEad == "S")){//verificando se estado é igual ao estado da unidad                    
                $instituicao;
            }else{
                return redirect()->back();
            }
        }
        return view('instituicao.historico', compact('titulo','instituicao','logs'));
    }

    public function edit($id)
    {
        $titulo = "Edição de Instituição de Ensino";
        $estados = Estado::all();
        $polos = Polo::all();
        $instituicao = $this->instituicao->findOrFail($id);

        if(Auth::user()->unidade_id != null){//Verificando se recebe unidade
            $relEstado = Auth::user()->unidade->estado->pluck('id')->toArray();
            //if(in_array($instituicao->estado_id,$relEstado)){//verificando se estado é igual ao estado da unidad
            if((in_array($instituicao->estado_id,$relEstado)) OR ($instituicao->isEad == "S")){//verificando se estado é igual ao estado da unidad                    
                $instituicao;
            }else{
                return redirect()->back();
            }
        }
        return view('instituicao.editar', compact('titulo','instituicao','polos','estados'));
    }

    public function update(Request $request, $id)
    {
        $instituicao = $this->instituicao->findOrFail($id);
        $formulario = $request->all();

        $atualizar = $instituicao->update($formulario);
        if($atualizar)
            return redirect()
                        ->route('instituicao.show', ['instituicao' => $id])
                        ->with('success', 'Cadastro Atualizado com Sucesso!');
        else
            return redirect()
                        ->back()
                        ->with('error', 'Falha ao Atualizar!');
    }

    public function getCursosInstituicao($idInstituicao)
    {
        $cursos = DB::table('instituicoes_cursos')
        ->join('cursos', 'instituicoes_cursos.curso_id', '=', 'cursos.id')
        ->orderBy('cursos.nmCurso', 'Asc')
        ->select('cursos.nmCurso', 'instituicoes_cursos.id')
        ->where('instituicao_id',$idInstituicao)
        ->get();
        return Response::json($cursos);
    }

    public function getInstituicaoEstado($idEstado)
    {
        $instituicoes = $this->instituicao
        ->where('estado_id',$idEstado)
        ->orWhere('isEad', 'S')
        ->orderBy('nmInstituicao','ASC')
        ->get(['id','nmInstituicao']); 
        //$instituicao = $instituicao->getQuery()->orderBy('nmCidade','ASC')->get(['id', 'nmCidade']);
        return Response::json($instituicoes);
    }
    
    //PEGAR POLO DA INSTITUICAO
    public function getPoloInstituicao($idInstituicao)
    {        
        $instituicao = $this->instituicao->find($idInstituicao);
        $polo = $instituicao->poloMatriz()->get();
        return Response::json($polo);
    }

}