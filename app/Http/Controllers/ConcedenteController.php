<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Concedente;
use App\Models\Supervisor;
use App\Models\Estado;
use App\Models\Cidade;
use App\Models\UserConcedente;
use App\Models\SecConcedente;
use App\Models\FolhaPagamento;
use App\Models\FolhaPagamentoItem;
use App\Models\Tce;
use Auth;
use Carbon\Carbon;
use Validator;
use PDF;
use Illuminate\Support\Collection;
use App\Models\MotivoCancelamento;
//use App\Http\Requests\ConcedenteStoreUpdateFormRequest;

class ConcedenteController extends Controller
{
    private $concedente;
    private $folhapagamento;
    private $estadoModel;

    public function __construct(Concedente $concedente, FolhaPagamento $folhapagamento)
    {
        $this->concedente = $concedente;
        $this->folhapagamento = $folhapagamento;
    }

    public function index()
    {
        $titulo = 'Listagem de Concedente';

        //Verificação de Estado do Usuario
        if (Auth::user()->unidade_id != null) {
            $relEstado = Auth::user()->unidade->estado->pluck('id')->toArray();
            $concedentes = $this->concedente
      ->WhereIn('estado_id', $relEstado)->orderBy('nmRazaoSocial')->get();
        //->paginate(config('sysGlobal.Paginacao'));
        } else {
            //$concedentes = $this->concedente->paginate(config('sysGlobal.Paginacao'));
            $concedentes = $this->concedente->orderBy('nmRazaoSocial')->get();
        }

        return view('concedente.index', compact('concedentes', 'titulo'));
    }

    public function create(Request $request)
    {
        $estados = Estado::all();
        $titulo = 'Cadastro de Concedentes';
        $cidades = [];

        return View('concedente.cadastro', [
      'estados' => $estados,
      'titulo' => $titulo,
      'cidades' => $request->session()->get('cidades') ?? [],
    ]);
    }

    public function store(Request $request)
    {
        $formulario = $request->all();
        $formulario['usuario_id'] = Auth::user()->id;
        $validar = Validator::make($request->all(), [
      'nmRazaoSocial' => 'required',
      'nmFantasia' => 'required',
      'cdCnpjCpf' => 'required|unique:concedentes|cpf_cnpj',
      'nmResponsavel' => 'required',
      'dsResponsavelCargo' => 'required',
      'cdCEP' => 'required',
      'dsEndereco' => 'required',
      'nnNumero' => 'required',
      'nmBairro' => 'required',
      'estado_id' => 'required',
      'cidade_id' => 'required',
      'dsFone' => 'required',
      'dsEmail' => 'required',
    ], [
      'nmRazaoSocial.required' => ' O campo Razão Social é Obrigatório.',
      'nmFantasia.required' => ' O campo Nome Fantasia é Obrigatório.',
      'cdCnpjCpf.required' => ' O campo CNPJ/CPF é obrigatório.',
      'cdCnpjCpf.unique' => ' Já existe concedente com esse numero de documento(CNPJ/CPF).',
      'cdCnpjCpf.cpf_cnpj' => ' CNPJ ou CPF Inválido',
      'nmResponsavel.required' => ' O campo Nome Responsável é obrigatório.',
      'dsResponsavelCargo.required' => ' O campo Cargo Responsável é obrigatório.',
      'cdCEP.required' => ' O campo CEP é obrigatório.',
      'dsEndereco.required' => ' O campo Endereço é obrigatório.',
      'nnNumero.required' => ' O campo Numero é obrigatório.',
      'nmBairro.required' => ' O campo Bairro é obrigatório.',
      'estado_id.required' => ' O campo Estado é obrigatório.',
      'cidade_id.required' => ' O campo Cidade é obrigatório.',
      'dsFone.required' => ' O campo Telefone é obrigatório.',
      'dsEmail.required' => ' O campo E-mail é obrigatório.',
    ]);

        if ($validar->fails()) {
            $cidades = Cidade::where('estado_id', $formulario['estado_id'])->get();

            return
      redirect()
      ->route('concedente.create')
      ->with('cidades', $cidades)
      ->withErrors($validar)
      ->withInput();
        }

        if ($this->concedente->create($formulario)) {
            return redirect()
    ->route('concedente.index')
    ->with('success', 'Cadastro Realizado com Sucesso!');
        } else {
            return redirect()
    ->back()
    ->with('error', 'Falha ao Cadastrar!');
        }
    }

    public function show($id)
    {
        $titulo = 'Concedente';
        $concedente = $this->concedente->findOrFail($id);
        //usuario do concedente
        $usersConcedente = UserConcedente::where('concedente_id', $id)->orderBy('id')->get();
        //usuario do concedente
        $secretariasConcedente = SecConcedente::where('concedente_id', $id)->orderBy('id')->get();
        $tcesAtivos = Tce::
    where('concedente_id', $id)
    ->where('dtCancelamento', '=', null)
    ->get();
        $tcesCancelados = Tce::
    where('concedente_id', $id)
    ->where('dtCancelamento', '<>', null)
    ->get();
        $supervisores = Supervisor::where('concedente_id', $id)->get()->count();
        $aditivos = 0;
        foreach ($tcesAtivos as $tce) {
            if ($tce->aditivos()->count() > 0) {
                $aditivos += $tce->aditivos()->count();
            }
        }
        //$UserConcedente = $this->concedente->findOrFail($id);
    //dd($tcesCancelados);
    //Verificação de Estado do Usuario
    if (Auth::user()->unidade_id != null) {//Verificando se recebe unidade
      $relEstado = Auth::user()->unidade->estado->pluck('id')->toArray();
        if (in_array($concedente->estado_id, $relEstado)) {//verificando se estado é igual ao estado da unidad
            $concedente;
        } else {
            return redirect()->route('concedente.index');
        }
    }
        //dd($concedente->cidade->estado);
        return view('concedente.visualizar', compact('concedente', 'titulo', 'usersConcedente', 'secretariasConcedente', 'tcesAtivos', 'tcesCancelados', 'supervisores', 'aditivos'));
    }

    public function showTcesAtivos($id)
    {
        $titulo = 'Tces Ativos Concedente';
        $tces = Tce::where('concedente_id', $id)->where('dtCancelamento', null)->whereNotNull('dt4Via')->orderBy('id', 'desc')->get();

        return view('tce.index', compact('tces', 'titulo'));
    }

    public function showTcesPendentes($id)
    {
        $titulo = 'Tces Pendentes Concedente';
        $tces = Tce::where('concedente_id', $id)->where('dtCancelamento', null)->whereNull('dt4Via')->orderBy('id', 'desc')->get();
        $concedente = Concedente::find($tces->first()->concedente_id);
        $secretaria = null;
        if ($tces->first()->sec_conc_id != null) {
            $secretaria = SecConcedente::find($tces->first()->sec_conc_id);
        }

        return view('tce.index', compact('tces', 'titulo', 'concedente', 'secretaria'));
    }

    public function showTcesCancelados($id)
    {
        $titulo = 'Tces Cancelados Concedente';
        $tcesCancelados = Tce::where('concedente_id', $id)->whereNotNull('dtCancelamento')->orderBy('dtCancelamento', 'desc')->paginate(config('sysGlobal.Paginacao'));
        $concedente = Concedente::find($tcesCancelados->first()->concedente_id);
        $secretaria = null;
        if ($tcesCancelados->first()->sec_conc_id != null) {
            $secretaria = SecConcedente::find($tcesCancelados->first()->sec_conc_id);
        }

        return view('tce.index', compact('tcesCancelados', 'titulo', 'concedente'));
    }

    public function showTcesVencidos($id)
    {
        $titulo = 'Tces Cancelados Concedente';
        $tces = Tce::getVencidos($id,null);
        $concedente = Concedente::find($id);
        $secretaria = null;
        $motivosCancelamento = MotivoCancelamento::all();
        return view('concedente.tce.index', compact('tces', 'titulo', 'concedente', 'secretaria', 'motivosCancelamento'));
    }

    public function edit($id)
    {
        $titulo = 'Edição de Concedente';
        $estados = Estado::all();
        $concedente = $this->concedente->findOrFail($id);

        //Verificação de Estado do Usuario
    if (Auth::user()->unidade_id != null) {//Verificando se recebe unidade
      $relEstado = Auth::user()->unidade->estado->pluck('id')->toArray();
        if (in_array($concedente->estado_id, $relEstado)) {//verificando se estado é igual ao estado da unidad
            $concedente;
        } else {
            return redirect()->route('concedente.index');
        }
    }

        return view('concedente.editar', compact('concedente', 'estados', 'titulo'));
    }

    public function update(Request $request, $concedente_id)
    {
        $concedente = $this->concedente->findOrFail($concedente_id);
        //dd($request->all());
        $formulario = $request->all();

        //$formulario['assUpaTCE'] = Auth::user()->id;
        //dd($formulario);
        $atualizar = $concedente->update($formulario);
        if ($atualizar) {
            return redirect()
    ->route('concedente.show', ['concedente' => $concedente_id])
    ->with('success', 'Cadastro Atualizado com Sucesso!');
        } else {
            return redirect()
    ->back()
    ->with('error', 'Falha ao Atualizar!');
        }
    }

    public function search(Request $request)
    {
        // dd($request->all());
        $dataForm = $request->except('_token');
        $concedentes = $this->concedente->search($request->key_search, config('sysGlobal.Paginacao'));

        return view('concedente.index', compact('concedentes', 'dataForm'));
    }

    public function financeiroIndex($concedente_id)
    {
        $titulo = 'Financeiro da Concedente';
        $concedente = $this->concedente->findOrFail($concedente_id);
        $folhas = FolhaPagamento::where('concedente_id', $concedente_id)->orderBy('id')->get();
        //dd($folhas);
        return view('concedente.financeiro.index', compact('concedente', 'titulo', 'folhas'));
    }

    public function financeiroCriarFolha(Request $request, $concedente)
    {
        $formulario = $request->all();
        $formulario['user_id'] = Auth::user()->id;
        $formulario['concedente_id'] = $concedente;
        $formulario['hash'] = base64_encode($formulario['referenciaMes'].$formulario['referenciaAno']);
        //dd($formulario, $concedente);
        $validar = Validator::make($request->all(), [
      'referenciaMes' => 'required',
      'referenciaAno' => 'required',
    ], [
      'referenciaMes.required' => ' O campo Mês Referência é Obrigatório.',
      'referenciaAno.required' => ' O campo Ano é Obrigatório.',
    ]);

        if ($validar->fails()) {
            return
      redirect()
      ->route('concedente.financeiro', ['concedente' => $concedente])
      ->withErrors($validar)
      ->withInput();
        }
        $hash = base64_encode($formulario['referenciaMes'].$formulario['referenciaAno']);
        $teste = FolhaPagamento::where('concedente_id', $formulario['concedente_id'])
    ->where('hash', $hash)
    ->get();
        //dd($teste);
        if ($teste->count() == 0) {
            $folha = $this->folhapagamento->create($formulario);
            if ($folha) {
                if ($request['alimentada'] === 'sim') {
                    $iFolha = FolhaPagamentoItem::where('folha_id', $folha->id)->pluck('tce_id')->toArray();
                    FolhaPagamentoItem::alimentaFolha($concedente, null, $folha, $iFolha, $request['diasBaseEstagiados'], $request['diasTrabalhados']);
                }

                return redirect()
        ->route('concedente.financeiro', ['concedente' => $concedente])
        ->with('success', 'Folha Criada com Sucesso!');
            } else {
                return redirect()
        ->back()
        ->with('error', 'Falha ao Cadastrar!');
            }
        } else {
            return redirect()
      ->route('concedente.financeiro', ['concedente' => $concedente])
      ->with('error', 'Falha ao Cadastrar! Já existe uma Folha de Pagamento para esta competência.');
        }
    }

    public function financeiroVerFolha($concedente, $id)
    {
        $titulo = 'Concedente';
        $folha = FolhaPagamento::findOrFail($id);

        $itensFolha = FolhaPagamentoItem::where('folha_id', $id)->get();
        // $itensFolha = $itensFolha->sortBy(function($folhapagamentoitem){
        //     //Definir qual coluna vai ser usada na ordenação, evidenciando o UTF8 pra nomes iniciados com letras acentuadas
        //     return preg_replace('/[`^~\'"]/', null, iconv('UTF-8', 'ASCII//TRANSLIT', $folhapagamentoitem->tce->estudante->nmEstudante));
        // });

        //pega lista com os ids dos tces que estão na folha até o momento, pra conseguir tira-los das proximas listagens
        $iFolha = FolhaPagamentoItem::where('folha_id', $id)->pluck('tce_id')->toArray();
        //Lista com os TCES ativos sem os que já estão na folha
        $tcesAtivos = Tce::where('dtCancelamento', null)->where('concedente_id', $concedente)->whereNotIn('id', $iFolha)->whereNotNull('dt4Via')->get();
        // $tcesAtivos = $tcesAtivos->sortBy(function($tce){
        //     //Definir qual coluna vai ser usada na ordenação, evidenciando o UTF8 pra nomes iniciados com letras acentuadas
        //     return iconv('UTF-8', 'ASCII//TRANSLIT', $tce->estudante->nmEstudante);
        // });

        //Lista com os Tces Cancelados sem os que já estão na folha
        $tcesCancelados = Tce::whereNotNull('dtCancelamento')
    ->where('concedente_id', $concedente)
    ->orderBy('dtCancelamento', 'desc')
    ->whereNotIn('id', $iFolha)
    ->get();
        $ativos = new Collection();
        $ativo = [];
        if ($tcesAtivos->count() > 0) {
            foreach ($tcesAtivos as $tce) {
                $ativo['id'] = $tce->id;
                $ativo['relatorio_id'] = $tce->tipo_tce;
                $ativo['nmEstudante'] = $tce->estudante->nmEstudante;
                $ativo['cdCPF'] = $tce->estudante->cdCPF;
                if (\Carbon\Carbon::parse($tce->dtInicio)->year === \Carbon\Carbon::now()->year && abs((\Carbon\Carbon::parse($tce->dtInicio)->month - \Carbon\Carbon::now()->month)) < 1) {
                    $ativo['diasTrabalhados'] = abs(\Carbon\Carbon::parse($tce->dtInicio)->day - 23);
                    $ativo['diasBaseEstagiados'] = abs(\Carbon\Carbon::parse($tce->dtInicio)->day - 31);
                } elseif (\Carbon\Carbon::parse($tce->dtFim)->year === \Carbon\Carbon::now()->year && abs((\Carbon\Carbon::parse($tce->dtFim)->month - \Carbon\Carbon::now()->month)) < 1) {
                    $ativo['diasTrabalhados'] = abs(\Carbon\Carbon::parse($tce->dtFim)->day - 23);
                    $ativo['diasBaseEstagiados'] = abs(\Carbon\Carbon::parse($tce->dtFim)->day - 31);
                } else {
                    $ativo['diasTrabalhados'] = 22;
                    $ativo['diasBaseEstagiados'] = 30;
                }

                $ativo['vlAuxilioTransporteReceber'] = $tce->vlAuxilioTransporte;
                $ativo['vlAuxilioTransporteDia'] = $tce->vlAuxilioTransporte / 30;

                $ativo['dtInicio'] = \Carbon\Carbon::parse($tce['dtInicio'])->format('d/m/Y');
                $ativo['dtFim'] = \Carbon\Carbon::parse($tce['dtFim'])->format('d/m/Y');

                if ($tce->aditivos->count() > 0 && $tce->aditivos()->latest()->first()->vlAuxilioMensal != null) {
                    $ativo['vlAuxilioMensal'] = number_format($tce->aditivos()->latest()->first()->vlAuxilioMensal, 2, ',', '.');
                } else {
                    $ativo['vlAuxilioMensal'] = number_format($tce->vlAuxilioMensal, 2, ',', '.');
                }

                $ativos->push($ativo);
            }
            $tcesAtivos = $ativos;
        }

        return view('concedente.financeiro.visualizar', compact('folha', 'titulo', 'itensFolha', 'tcesAtivos', 'tcesCancelados', 'concedente'));
    }

    //Folha em PDF
    public function financeiroVerFolhaPDF($concedente, $id, Request $request)
    {
        //modelo de relatorio
        $modelo = $request->query('modelo');
        //Querys Principal
        $folha = FolhaPagamento::findOrFail($id);
        //$itensFolha = FolhaPagamentoItem::all()->where('folha_id',$id);
        $itensFolha = FolhaPagamentoItem::where('folha_id', $id)->get();
        $itensFolha = $itensFolha->sortBy(function ($folhapagamentoitem) {
            //Definir qual coluna vai ser usada na ordenação
            return preg_replace('/[`^~\'"]/', null, iconv('UTF-8', 'ASCII//TRANSLIT', $folhapagamentoitem->tce->estudante->nmEstudante));
        });
        //Gets
        $dtHoje = Carbon::today()->format('d/m/Y');
        // $pdf = PDF::loadView('concedente.secretaria.financeiro.relatorio.recifeFolhaPDF', compact('folha','itensFolha','dtHoje','modelo'))
        // ->setPaper('a4','landscape')
        // ->setOptions(['dpi' => 100,'enable_php'=>true]);
        // return $pdf->stream();
        return view('concedente.secretaria.financeiro.relatorio.recifeFolhaPDF', compact('folha', 'itensFolha', 'dtHoje', 'modelo'));
    }

    //folha em pdf

    public function financeiroCriarItem($concedente, $id, $tce_id)
    {
        $titulo = 'Inclusão de TCE na Folha de Pagamento da Concedente';
        $folha = FolhaPagamento::findOrFail($id);
        $tce = Tce::findOrFail($tce_id);
        //$concedente = $this->concedente->findOrFail($id);
        return view('concedente.financeiro.cadFolha', compact('titulo', 'folha', 'tce'));
    }

    public function financeiroItemStore(Request $request)
    {
        $formulario = $request->all();
        $validar = Validator::make($request->all(), [
      'diasBaseEstagiados' => 'required',
      'diasTrabalhados' => 'required',
    ], [
      'diasBaseEstagiados.required' => ' O campo Dias base Estágio é Obrigatório.',
      'diasTrabalhados.required' => ' O campo Dias Trabahados é Obrigatório.',
    ]);

        if ($validar->fails()) {
            return
      redirect()
      ->back()
      //->route('concedente.financeiro',['concedente' => $concedente])
      ->withErrors($validar)
      ->withInput();
        }
        $formulario['user_id'] = Auth::user()->id;
        $formulario['hash'] = base64_encode($formulario['referenciaMes'].$formulario['referenciaAno']);

        if ($formulario['diasRecesso'] != null) {
            $formulario['vlRecesso'] = number_format($formulario['diasRecesso'] * str_replace(array('.', ','), array('', '.'), $formulario['vlAuxilioTransporteReceber']) / 30, 2, '.', '');
            $vlRecesso = $formulario['vlRecesso'];
        } else {
            $vlRecesso = 0;
        }
        if ($formulario['vlAuxilioTransporteDia'] != null) {
            $vlAuxilioTransporteReceber = $formulario['vlAuxilioTransporteReceber'];
        } else {
            $vlAuxilioTransporteReceber = 0;
        }

        $formulario['descontoVlAuxTransporte'] = 0;
        $formulario['descontoVlAuxMensal'] = 0;
        if ($formulario['faltas'] > 0) {
            $formulario['descontoVlAuxTransporte'] = number_format(($formulario['faltas'] + $formulario['justificadas']) * $formulario['vlAuxilioTransporteDia'], 2, '.', '');
            $formulario['descontoVlAuxMensal'] = $formulario['vlAuxilioMensal'] / $formulario['diasBaseEstagiados'] * $formulario['faltas'];
        }

        if ($formulario['justificadas'] > 0) {
            $formulario['descontoVlAuxTransporte'] = $formulario['descontoVlAuxTransporte'] + abs(number_format($formulario['justificadas'] * $formulario['vlAuxilioTransporteDia'], 2, '.', ''));
        }

        $formulario['vlAuxilioMensalReceber'] = abs($formulario['vlAuxilioMensal'] / 30 * $formulario['diasBaseEstagiados'] - $formulario['descontoVlAuxMensal']);

        $formulario['vlTotal'] = abs($vlRecesso - $vlAuxilioTransporteReceber + $formulario['vlAuxilioMensalReceber'] - ($formulario['descontoVlAuxMensal'] + $formulario['descontoVlAuxTransporte']));

        $hash = base64_encode($formulario['referenciaMes'].$formulario['referenciaAno']);
        $teste = FolhaPagamentoItem::where('concedente_id', $formulario['concedente_id'])
    ->where('hash', $hash)
    ->where('folha_id', $formulario['folha_id'])
    ->where('tce_id', $formulario['tce_id'])
    ->get();
        //dd($teste->count());
        if ($teste->count() == 0) {
            if (FolhaPagamentoItem::create($formulario)) {
                return redirect()
    ->route('concedente.financeiro.show', ['concedente' => $formulario['concedente_id'], 'folha' => $formulario['folha_id']])
    ->with('success', 'TCE Adicionado na folha com Sucesso!');
            } else {
                return redirect()
    ->back()
    ->with('error', 'Falha ao Cadastrar!');
            }
        } else {
            return redirect()
    //->back()
    ->route('concedente.financeiro.show', ['concedente' => $formulario['concedente_id'], 'folha' => $formulario['folha_id']])
    ->with('error', 'Falha ao Cadastrar! Já existe este lançamento para esta competência.');
        }
    }

    // teste editar item folha
    public function financeiroEditarItem($concedente, $folhaId, $itemFolhaId)
    {
        $titulo = 'Edição de Item na Folha de Pagamento da Concedente';
        //$folha = FolhaPagamento::findOrFail($id);
        $itemFolha = FolhaPagamentoItem::findOrFail($itemFolhaId);
        $tce = Tce::findOrFail($itemFolha->tce_id);
        //$concedente = $this->concedente->findOrFail($id);
        return view('concedente.financeiro.editFolha', compact('titulo', 'itemFolha', 'tce'));
    }

    public function financeiroUpdateItem(Request $request, $concedente, $idfolha, $id)
    {
        $itemFolha = FolhaPagamentoItem::findOrFail($id);
        $formulario = $request->all();
        if ($formulario['dtPagamento'] == null) {
            $formulario['dtPagamento'] = null;
        }
        $formulario['hash'] = base64_encode($formulario['referenciaMes'].$formulario['referenciaAno']);
        $formulario['vlAuxilioMensalReceber'] = $itemFolha->vlAuxilioMensalReceber / 30 * $formulario['diasBaseEstagiados'];

        if ($formulario['diasRecesso'] != null) {
            $formulario['vlRecesso'] = number_format($formulario['diasRecesso'] * str_replace(array('.', ','), array('', '.'), $formulario['vlAuxilioTransporte']) / 30, 2, '.', '');
            $vlRecesso = $formulario['vlRecesso'];
        } else {
            $vlRecesso = 0;
            $formulario['vlRecesso'] = null;
        }
        $vlAuxilioTransporteReceber = $itemFolha->vlAuxilioTransporteReceber;
        //round é um metódo usado para arredondar números
        $formulario['vlAuxilioTransporteDia'] = round($itemFolha->tce->vlAuxilioTransporte / 30, 2);

        $formulario['descontoVlAuxTransporte'] = 0;
        $formulario['descontoVlAuxMensal'] = 0;
        if ($formulario['faltas'] > 0) {
            $formulario['descontoVlAuxTransporte'] = $formulario['faltas'] * $formulario['vlAuxilioTransporteDia'];
            $formulario['descontoVlAuxMensal'] = $itemFolha->vlAuxilioMensalReceber / $formulario['diasBaseEstagiados'] * $formulario['faltas'];
        }

        if ($formulario['justificadas'] > 0) {
            $formulario['descontoVlAuxTransporte'] = $formulario['descontoVlAuxTransporte'] + abs(number_format($formulario['justificadas'] * $formulario['vlAuxilioTransporteDia'], 2, '.', ''));
        }

        $formulario['vlTotal'] = abs($vlRecesso + $vlAuxilioTransporteReceber + $formulario['vlAuxilioMensalReceber'] - ($formulario['descontoVlAuxMensal'] + $formulario['descontoVlAuxTransporte']));

        $hash = base64_encode($formulario['referenciaMes'].$formulario['referenciaAno']);
        $teste = FolhaPagamentoItem::whereNotIn('id', [$id])
    ->where('concedente_id', $formulario['concedente_id'])
    ->where('hash', $hash)
    ->where('folha_id', $formulario['folha_id'])
    ->where('tce_id', $formulario['tce_id'])
    ->get();
        //dd($teste->count());
        if ($teste->count() == 0) {
            $atualizar = $itemFolha->update($formulario);
            if ($atualizar) {
                return redirect()
      ->route('concedente.financeiro.show', ['concedente' => $formulario['concedente_id'], 'folha' => $formulario['folha_id']])
      ->with('success', 'Item Editado da folha com Sucesso!');
            } else {
                return redirect()
      ->back()
      ->with('error', 'Falha ao Cadastrar!');
            }
        } else {
            return redirect()
      //->back()
      ->route('concedente.financeiro.show', ['concedente' => $formulario['concedente_id'], 'folha' => $formulario['folha_id']])
      ->with('error', 'Falha ao Cadastrar! Já existe este lançamento para esta competência.');
        }
    }

    // teste deletar item folha
    public function financeiroDeletarItem($concedente, $folhaId, Request $request)
    {
        //$titulo = "Edição de Item na Folha de Pagamento da Concedente";
        //$folha = FolhaPagamento::findOrFail($id);
        $itemFolha = FolhaPagamentoItem::find($request['id']);
        if ($itemFolha) {
            $itemFolha->delete();
        }

        return redirect()
    ->back()
    ->with('success', 'Item Deletado da folha com Sucesso!');
        //$concedente = $this->concedente->findOrFail($id);
    //return view('concedente.financeiro.editFolha', compact('titulo','itemFolha','tce'))
    //->with('success', 'Item Deletado da folha com Sucesso!');
    }

    /*
    public function financeiroUpdateItem(Request $request, $concedente, $idfolha, $id)
    {
    $itemFolha = FolhaPagamentoItem::findOrFail($id);
    // $concedente = $this->concedente->findOrFail($concedente_id);
    dd($itemFolha);
    $formulario = $request->all();
    //dd($formulario);
    $atualizar = $concedente->update($formulario);
    if($atualizar)
    return redirect()
    ->route('concedente.show', ['concedente' => $concedente_id])
    ->with('success', 'Cadastro Atualizado com Sucesso!');
    else
    return redirect()
    ->back()
    ->with('error', 'Falha ao Atualizar!');
}
*/
    public function downloadRelatorioPDF(Request $request, $idConcedente)
    {
        $formulario = $request->all();
        $tcesCancelados;
        $dataFinal;
        $dataInicial;

        if ($formulario['dataInicial'] && $formulario['dataFinal']) {
            $dataInicial = explode('/', $formulario['dataInicial'])[2].'-'.explode('/', $formulario['dataInicial'])[1].'-'.explode('/', $formulario['dataInicial'])[0];
            $dataFinal = explode('/', $formulario['dataFinal'])[2].'-'.explode('/', $formulario['dataFinal'])[1].'-'.explode('/', $formulario['dataFinal'])[0];
            $tcesCancelados = Tce::where('sec_conc_id', $id)->whereNotNull('dtCancelamento')->where('cancelado_at', '<=', $dataFinal)->where('cancelado_at', '>=', $dataInicial)->orderBy('cancelado_at', 'desc')->get();
        } elseif ($formulario['dataInicial'] == null) {
            $dataFinal = explode('/', $formulario['dataFinal'])[2].'-'.explode('/', $formulario['dataFinal'])[1].'-'.explode('/', $formulario['dataFinal'])[0];
            $tcesCancelados = Tce::where('sec_conc_id', $id)->whereNotNull('dtCancelamento')->where('cancelado_at', '<=', $dataFinal)->orderBy('cancelado_at', 'desc')->get();
        } else {
            $dataInicial = explode('/', $formulario['dataInicial'])[2].'-'.explode('/', $formulario['dataInicial'])[1].'-'.explode('/', $formulario['dataInicial'])[0];
            $tcesCancelados = Tce::where('sec_conc_id', $id)->whereNotNull('dtCancelamento')->where('cancelado_at', '>=', $dataInicial)->orderBy('cancelado_at', 'desc')->get();
        }

        $pdf = PDF::loadView('tce.relatorios.tcesCancelados', compact('request', 'tcesCancelados'))
    ->setPaper('a4', 'landscape')
    ->setOptions(['dpi' => 100, 'enable_php' => true]);

        return $pdf->download();

        // return view('tce.relatorios.tcesCancelados', compact('request', 'tcesCancelados'));
    }
}
