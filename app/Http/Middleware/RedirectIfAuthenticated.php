<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        /* */
        if (Auth::guard($guard)->check()) {
            return redirect('/home');
        }
        return $next($request);
       

      /*switch ($guard) {
        case 'web':
          if (Auth::guard($guard)->check()) {
            return redirect('/sysEstagio');
          }
          break;
          case 'web_concedente':
          if (Auth::guard($guard)->check()) {
            return redirect()->route('concedente.dashboard');
          }
          break;
          case 'consultant':
          if (Auth::guard($guard)->check()) {
            return redirect()->route('consultant.dashboard');
          }
          break;
        default:
          if (Auth::guard($guard)->check()) {
              return redirect('/');
          }
          break;
      }
      return $next($request);
      */
      
    }
}
