<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Oportunidade;

class Oportunidade extends Model
{

    use SoftDeletes;

    //
    protected $table = 'oportunidades';

    protected $dates = ['created_at','dtEncerrada', 'deleted_at'];

    protected $fillable = [
        'tipo', 'code', 'titulo', 'foto', 'curso_id', 'cidade_id', 'cargaHorariaSemanal', 'horarioInicio', 'local', 'obs', 'horarioFim', 'bolsa', 'auxTransp', 'auxAlimentacao', 'vagas', 'concedente_id', 'user_id', 'secretaria_id', 'status', 'estado_id'
    ];

    public function requisitos(){
      return $this->hasMany(OportunidadeRequisito::class);
    }

    public function tarefas(){
      return $this->hasMany(TarefaOportunidade::class);
    }

    public function candidatos(){
      return $this->hasMany(OportunidadeEstudante::class);
    }

    public static function generateRandomStr() {
        $length = 8;
        $symbols = "0123456789";
        $charactersLength = strlen($symbols);
        $randomStr = null;

        for ($i = 0; $i < $length; $i++) {
            $randomStr .= $symbols[rand(0, $charactersLength - 1)];
            // $code = Oportunidade::where('code', $randomStr);
        }
        return $randomStr;
    }

    public static function hasCode() {
        $code = Oportunidade::generateRandomStr();
        $hasCode = Oportunidade::where('code', $code);

        if ($hasCode == null) {
          return $code;
        } else {
          return self::generateRandomStr();
        }
      }
}
