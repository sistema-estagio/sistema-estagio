<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Carbon\Carbon;



class EstudantePainel extends Model
{
    //nome da tabela no bd
    protected $table = 'estudantes';

    protected $dates = ['dtNascimento','updated_at', 'created_at'];
    protected $fillable = [
        'cdCPF','cdRG','dsOrgaoRG',
        'nmEstudante','dsEstadoCivil','dtNascimento','dsSexo','instituicao_id','curso_id',
        'nnSemestreAno','nivel_id','turno_id','estado_id',
        'cidade_id', 'dsEndereco','nnNumero','nmBairro','dsComplemento',
        'cdCEP', 'dsFone','dsOutroFone','dsEmail','nmPai','nmMae',
        'dsBanco','dsAgencia','dsConta','dsTipoConta','dsFavorecido',
        'user_id'
    ];
    public $timestamps = true;

    public function setDtNascimentoAttribute($value){
        //$this->attributes['dtNascimento'] = date('Y-m-d',strtotime($value));
        $this->attributes['dtNascimento'] = Carbon::createFromFormat('d/m/Y', $value)->toDateString();

    }
    // PARTE QUE CONVERTE CADA CAMPO PARA MAIUSCULO NO INSERT E UPDATE
    //
    public function setnmEstudanteAttribute($value){
    //Converte para Maiusculo
        $this->attributes['nmEstudante'] = mb_strtoupper($value, 'UTF-8');
    }
    public function setnmPaiAttribute($value){
        //Converte para Maiusculo
        $this->attributes['nmPai'] = mb_strtoupper($value, 'UTF-8');
    }
    public function setnmMaeAttribute($value){
        //Converte para Maiusculo
        $this->attributes['nmMae'] = mb_strtoupper($value, 'UTF-8');
    }
    public function setdsOrgaoRGAttribute($value){
        //Converte para Maiusculo
        $this->attributes['dsOrgaoRG'] = mb_strtoupper($value, 'UTF-8');
    }
    public function setdsEnderecoAttribute($value){
        //Converte para Maiusculo
        $this->attributes['dsEndereco'] = mb_strtoupper($value, 'UTF-8');
    }
    public function setdsComplementoAttribute($value){
        //Converte para Maiusculo
        $this->attributes['dsComplemento'] = mb_strtoupper($value, 'UTF-8');
    }
    public function setnmBairroAttribute($value){
        //Converte para Maiusculo
        $this->attributes['nmBairro'] = mb_strtoupper($value, 'UTF-8');
    }
    public function setdsBancoAttribute($value){
        //Converte para Maiusculo
        $this->attributes['dsBanco'] = mb_strtoupper($value, 'UTF-8');
    }
    public function setdsTipoContaAttribute($value){
        //Converte para Maiusculo
        $this->attributes['dsTipoConta'] = mb_strtoupper($value, 'UTF-8');
    }
    public function setdsfavorecidoAttribute($value){
        //Converte para Maiusculo
        $this->attributes['dsFavorecido'] = mb_strtoupper($value, 'UTF-8');
    }
    // FIM PARTE QUE CONVERTE CADA CAMPO PARA MAIUSCULO NO INSERT E UPDATE
    //
    public function cidade()
    {
        return $this->belongsTo(Cidade::class);
    }
    public function instituicao()
    {
        return $this->belongsTo(Instituicao::class);
    }

    public function CursoDaInstituicao()
    {
        //return $this->hasMany(CursoInstituicao::class);
        return $this->belongsTo(CursoInstituicao::class, 'curso_id');

        //return $this->belongsToMany(Curso::class, 'turnos_cursos', "curso_instituicao_id", "turno_id");

        //return $this->belongsToMany(Curso::class, 'curso_id', "cursos_instituicoes", "curso_id");
    }
    public function turno()
    {
        //return $this->belongsToMany(Curso::class, 'cursos_instituicoes', "instituicao_id", "curso_id")->withPivot('qtdDuracao');
        return $this->belongsTo(Turno::class);
    }

    public function nivel()
    {
        //return $this->belongsToMany(Curso::class, 'cursos_instituicoes', "instituicao_id", "curso_id")->withPivot('qtdDuracao');
        return $this->belongsTo(Nivel::class);
    }
    /*public function tce()
    {
        return $this->hasOne(Tce::class);
    }
    */
    public function tce()
    {
        return $this->hasMany(Tce::class);
    }
    //teste portabilidade
    public function portabilidade()
    {
        return $this->hasOne(PortabilidadeEstudante::class);
    }

    //Usuário que Cadastrou
    public function userCadastro()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function UserEstudante(){
        return $this->hasOne(UserEstudante::class,'id');
    }
}
