<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Atividade extends Model
{
    protected $fillable = [
        'atividade','nrNivel','curso_id','usuario_id',
    ];
    
    public function setatividadeAttribute($value)
    {
    //Converte para Maiusculo
        $this->attributes['atividade'] = mb_strtoupper($value, 'UTF-8');
    }

    public function curso()
    {

    return $this->belongsTo(Curso::class);
    }

    public function userCadastro()
    {
        return $this->belongsTo(User::class, 'usuario_id');
    }
}
