<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

//class Instituicao extends Model
class Instituicao extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    //protected $auditEnabled  = true;      //Desativa o registro de log nesta model.
    //nome da tabela no bd
    protected $table = 'instituicoes';

    protected $fillable = [
        'nmInstituicao', 'nmFantasia', 'cdCNPJ',
        'nmDiretor', 'nmDiretorCargo', 'nmSupervisor',
        'nmSupervisorCargo', 'nmReitor', 'nmReitorCargo',
        'dsEmail', 'dsEndereco', 'nnNumero',
        'nmBairro', 'cdCEP', 'dsFone',
        'dsOutro', 'usuario_id', 'polo_id', 'isEad',
        'estado_id', 'cidade_id',
    ];

    public function search($keySearch, $totalPage)
    {
        return $this
        ->where('nmInstituicao', 'LIKE', "%{$keySearch}%")
        ->orWhere('nmFantasia','LIKE', "%{$keySearch}%")
        ->orWhere('cdCNPJ','LIKE',"%{$keySearch}%")
        ->paginate(config('sysGlobal.Paginacao'));
    }
    // PARTE QUE CONVERTE CADA CAMPO PARA MAIUSCULO NO INSERT E UPDATE
    //
    public function setnmInstituicaoAttribute($value)
    //Converte para Maiusculo
    {
        $this->attributes['nmInstituicao'] = mb_strtoupper($value, 'UTF-8');
    }
    public function setnmFantasiaAttribute($value)
    //Converte para Maiusculo
    {
        $this->attributes['nmFantasia'] = mb_strtoupper($value, 'UTF-8');
    }
    public function setnmDiretorAttribute($value)
    //Converte para Maiusculo
    {
        $this->attributes['nmDiretor'] = mb_strtoupper($value, 'UTF-8');
    }
    public function setnmDiretorCargoAttribute($value)
    //Converte para Maiusculo
    {
        $this->attributes['nmDiretorCargo'] = mb_strtoupper($value, 'UTF-8');
    }
    public function setnmSupervisorAttribute($value)
    //Converte para Maiusculo
    {
        $this->attributes['nmSupervisor'] = mb_strtoupper($value, 'UTF-8');
    }
    public function setnmSupervisorCargoAttribute($value)
    //Converte para Maiusculo
    {
        $this->attributes['nmSupervisorCargo'] = mb_strtoupper($value, 'UTF-8');
    }
    public function setnmReitorAttribute($value)
    //Converte para Maiusculo
    {
        $this->attributes['nmReitor'] = mb_strtoupper($value, 'UTF-8');
    }
    public function setnmReitorCargoAttribute($value)
    //Converte para Maiusculo
    {
        $this->attributes['nmReitorCargo'] = mb_strtoupper($value, 'UTF-8');
    }
    public function setdsEnderecoAttribute($value)
    //Converte para Maiusculo
    {
        $this->attributes['dsEndereco'] = mb_strtoupper($value, 'UTF-8');
    }
    public function setnmBairroAttribute($value)
    //Converte para Maiusculo
    {
        $this->attributes['nmBairro'] = mb_strtoupper($value, 'UTF-8');
    }
    // FIM PARTE QUE CONVERTE CADA CAMPO PARA MAIUSCULO NO INSERT E UPDATE
    //

    public function CursoDaInstituicao()
    {
        return $this->hasMany(CursoInstituicao::class);
    }

    public function cidade()
    {
        return $this->belongsTo(Cidade::class);
    }
    public function convenio()
    {
        return $this->hasMany(ConveniosIe::class);
    }
    public function poloMatriz()
    {
        return $this->belongsTo(Polo::class, 'polo_id');
    }

}
