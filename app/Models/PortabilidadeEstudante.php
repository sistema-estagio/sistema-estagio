<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class PortabilidadeEstudante extends Model
{
    //
    //nome da tabela no bd	
    protected $table = 'portabilidade_estudantes';
    protected $dates = ['dtInicio','dtFim',];
    
    protected $fillable = [
        'estudante_id',
        'dtInicio','dtFim',
    ];
    public $timestamps = true;

    //Mascaras Datas
    public function setDtInicioAttribute($value)
    {
        $this->attributes['dtInicio'] = Carbon::createFromFormat('d/m/Y', $value);
    }

    public function setDtFimAttribute($value)
    {
        $this->attributes['dtFim'] = Carbon::createFromFormat('d/m/Y', $value);
    }

    public function estudante()
    {
        return $this->belongsTo(Estudante::class);
    }
}
