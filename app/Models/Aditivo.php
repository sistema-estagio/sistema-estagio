<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Aditivo extends Model
{
    //
    protected $dates = ['dtInicio','dtFim','dtAditivo','dtDeclaracao','dt4Via',];
    protected $fillable = [
    'tce_id','nnAditivo',
    'nnSemestreAno','nmSupervisor','dsSupervisorCargo','nmSupervisorIe','dsSupervisorIeCargo',
    'dtInicio','dtFim','periodo_id','dsPeriodo','hrInicio','hrFim',
    'vlAuxilioMensal','vlAuxilioAlimentacao','vlAuxilioTransporte','auxDescriminado',
    'aditivoOutro',
    'dsLotacao',
    'dtDeclaracao','dt4Via','dtAditivo',
    'user_id','estado_id', 'supervisor_id'
    ];

    public function setDtAditivoAttribute($value)
    {
        $this->attributes['dtAditivo'] = Carbon::createFromFormat('d/m/Y', $value);
    }
    public function setDtInicioAttribute($value)
    {
        if($value){
            $this->attributes['dtInicio'] = Carbon::createFromFormat('d/m/Y', $value);
        }
    }
    public function setDtFimAttribute($value)
    {
        if($value){
        $this->attributes['dtFim'] = Carbon::createFromFormat('d/m/Y', $value);
        }
    }
    public function setDtDeclaracaoAttribute($value)
    {
        if($value){
        $this->attributes['dtDeclaracao'] = Carbon::createFromFormat('d/m/Y', $value);
        }
    }
    public function setDt4ViaAttribute($value)
    {
        if($value){
        $this->attributes['dt4Via'] = Carbon::createFromFormat('d/m/Y', $value);
        }
    }
    //Fim Mascara Datas

    public function setaditivoOutroAttribute($value)
    {
        //Converte para Maiusculo
        $this->attributes['aditivoOutro'] = mb_strtoupper($value, 'UTF-8');
    }
    public function setnmSupervisorAttribute($value)
    {
    //Converte para Maiusculo
        $this->attributes['nmSupervisor'] = mb_strtoupper($value, 'UTF-8');
    }

    public function setdsSupervisorCargoAttribute($value)
    {
        //Converte para Maiusculo
            $this->attributes['dsSupervisorCargo'] = mb_strtoupper($value, 'UTF-8');
    }

    public function setnmSupervisorIeAttribute($value)
    {
    //Converte para Maiusculo
        $this->attributes['nmSupervisorIe'] = mb_strtoupper($value, 'UTF-8');
    }

    public function setdsSupervisorIeCargoAttribute($value)
    {
    //Converte para Maiusculo
        $this->attributes['dsSupervisorIeCargo'] = mb_strtoupper($value, 'UTF-8');
    }

    public function setVlAuxilioMensalAttribute($value)
    {
        if($value){
        $this->attributes['vlAuxilioMensal'] = str_replace(array(".",","), array("","."), $value);
        }
    }

    public function setVlAuxilioAlimentacaoAttribute($value)
    {
        if($value){
        $this->attributes['vlAuxilioAlimentacao'] = str_replace(array(".",","), array("","."), $value);
        }
    }

    public function setVlAuxilioTransporteAttribute($value)
    {
        if($value){
        $this->attributes['vlAuxilioTransporte'] = str_replace(array(".",","), array("","."), $value);
        }
    }

    public function setdsPeriodoAttribute($value)
    {
        //Converte para Maiusculo
        $this->attributes['dsPeriodo'] = mb_strtoupper($value, 'UTF-8');
    }

    public function atividades()
    {
        return $this->belongsToMany(Atividade::class, 'atividade_aditivos', "aditivo_id", "atividade_id")->withTimestamps();
    }
    public function estudante()
    {
        return $this->belongsTo(Estudante::class);
    }
    public function tce()
    {
        return $this->belongsTo(Tce::class);
    }
    public function periodo()
    {
        return $this->belongsTo(Periodo::class);
    }
    public function userCadastro()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function supervisor()
    {
        return $this->hasOne(Supervisor::class)->withDefault([
          'nome' => 'Nenhum supervisor'
        ]);
    }
}
