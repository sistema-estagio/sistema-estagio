<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Curso extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'cursos';

    protected $dates = ['created_at',];

    protected $fillable = [
        'nmCurso','nivel_id','usuario_id',
    ];

    public function setnmCursoAttribute($value)
    //Converte para Maiusculo
    {
        $this->attributes['nmCurso'] = mb_strtoupper($value, 'UTF-8');
    }

    public function CursoDaInstituicao()
    {
        //return $this->hasMany(CursoInstituicao::class);
        return $this->belongsTo(CursoInstituicao::class);
    }

    public function nivel()
    {
        return $this->belongsTo(Nivel::class);
    }
    public function atividades()
    {
        return $this->hasMany(Atividade::class);
    }

    public function search($keySearch, $totalPage)
    {
            return $this
            ->where('nmCurso', 'LIKE', "%{$keySearch}%")
            ->paginate(config('sysGlobal.Paginacao'));
    }

}
