<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cargo extends Model
{
    protected $fillable = [
        'id', 'nome', 'concedente_id',
    ];

    public function selecionado()
    {
        return $this->hasMany(Selecionado::class);
    }
}
