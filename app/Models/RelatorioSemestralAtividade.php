<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RelatorioSemestralAtividade extends Model
{
    protected $fillable = [
        'tce_id', 'created_at', 'updated_at'
    ];

    public function tce(){
      return $this->belongsTo(Tce::class);
    }
}
