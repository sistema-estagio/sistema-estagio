<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    //
    protected $fillable = ['nmEstado', 'cdUF'];
    public $timestamps = false;

    public function cidades()
    {
        return $this->hasMany(Cidade::class);
    }

    public function concedentes()
    {
        return $this->hasMany(Concedente::class);
    }

    public function instituicao(){
        return $this->hasMany(Instituicao::class);
    }
}
