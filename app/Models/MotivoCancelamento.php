<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MotivoCancelamento extends Model
{
    //

    //nome da tabela no bd
    protected $table = 'motivo_cancelamentos';

      protected $fillable = [
          'tipo', 'dsMotivoCancelamento'
      ];
}
