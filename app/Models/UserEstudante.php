<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;

use OwenIt\Auditing\Contracts\Auditable;

class UserEstudante extends Authenticatable
{

    use HasRoles, Notifiable;
    //protected $guard_name = 'web'; // or whatever guard you want to use
    protected $guard = 'web_estudante'; // or whatever guard you want to use

    protected $table = 'users_estudante';

    protected $fillable = ['name', 'email', 'cdCPF', 'dtNascimento', 'password', 'usuario_id', 'estudante_id'];

    protected $hidden = ['password', 'remember_token'];


    public function estudante(){
      return $this->belongsTo(Estudante::class, 'estudante_id');
    }

}
