<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cidade extends Model
{
    //
    protected $fillable = ['nmCidade', 'estado_id'];
    public $timestamps = false;

    public function estado()
    {
        return $this->belongsTo(Estado::class);
    }

    public function instituicao()
    {
        return $this->hasMany(Instituicao::class);
    }

    public function concedente()
    {
        return $this->hasMany(Concedente::class);
    }
}
