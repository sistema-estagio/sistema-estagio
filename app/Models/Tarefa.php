<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tarefa extends Model
{
    //
    protected $table = 'tarefas';
    protected $fillable = [
        'descricao', 'concedente_id', 'secretaria_id',
    ];

    public function oportunidade(){
      return $this->hasMany(TarefaOPortunidade::class);
    }
}
