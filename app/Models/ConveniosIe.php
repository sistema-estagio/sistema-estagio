<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ConveniosIe extends Model
{
    //nome da tabela no bd
    protected $table = 'convenios_ies';
    protected $dates = ['dtConvenioRetorno',];
    protected $fillable = [
        'instituicao_id','dtConvenioRetorno','usuario_id',
    ];

    public function setDtConvenioRetornoAttribute($value){
        if($value){
            $this->attributes['dtConvenioRetorno'] = Carbon::createFromFormat('d/m/Y', $value);
        }
    }

    public function instituicao()
    {
        //return $this->belongsTo(Curso::class, "curso_id", "id");
        return $this->hasMany(Instituicao::class);
    }
}
