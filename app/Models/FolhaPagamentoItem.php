<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Auth;
class FolhaPagamentoItem extends Model
{
    //nome da tabela no bd
    protected $table = 'folha_pagamento_item';
    protected $dates = ['dtPagamento',];
    protected $fillable = [
        'folha_id','tce_id','hash',
        'concedente_id','secConcedente_id','dtPagamento', 'diasRecesso', 'faltas', 'justificadas',
        'referenciaMes','referenciaAno','hash','mesesPropRecesso','diasBaseEstagiados','diasTrabalhados',
        'vlRecesso','vlAuxilioMensal','vlAuxilioMensalReceber','vlAuxilioAlimentacaoDia','vlAuxilioAlimentacaoReceber','vlAuxilioTransporteDia','vlAuxilioTransporteReceber','vlTotal',
        'user_id','tipo_tce', 'descontoVlAuxTransporte', 'descontoVlAuxMensal', 'descontoVlAuxAlimentacao', 'is_block'
    ];
    protected $hidden = ['is_block'];
    public $timestamps = true;

    /*public function setVlRecessoAttribute($value)
    {
        if($value){
        $this->attributes['vlRecesso'] = str_replace(array(".",","), array("","."), $value);
        }
    }
    */
    public function setDtPagamentoAttribute($value)
    {
        if($value){
        $this->attributes['dtPagamento'] = Carbon::createFromFormat('d/m/Y', $value);
        } else {
            $this->attributes['dtPagamento'] = null;
        }
    }
    public function setVlAuxilioMensalAttribute($value)
    {
        if($value){
        $this->attributes['vlAuxilioMensal'] = str_replace(array(".",","), array("","."), $value);
        }
    }
    public function setVlAuxilioAlimentacaoDiaAttribute($value)
    {
        if($value){
        $this->attributes['vlAuxilioAlimentacaoDia'] = str_replace(array(".",","), array("","."), $value);
        }
    }
    public function setVlAuxilioTransporteDiaAttribute($value)
    {
        if($value){
        $this->attributes['vlAuxilioTransporteDia'] = str_replace(array(".",","), array("","."), $value);
        }
    }
    public function ajuste(){
      return $this->belongsTo(AjusteItemFolha::class);
    }
    /*public function setVlTotalAttribute($value)
    {
        if($value){
        $this->attributes['vlTotal'] = str_replace(array(".",","), array("","."), $value);
        }
    }
    */
    public function folha()
    {
        return $this->belongsTo(FolhaPagamento::class);
    }
    public function tce()
    {
        return $this->belongsTo(Tce::class);
    }

    public function secConcedente(){
      return $this->belongsTo(SecConcedente::class);
    }

    public static function alimentaFolha($concedente, $secConcedente, $folha, $iFolha, $base, $trabalhados){
      if($secConcedente != null){
        $tcesAtivos = Tce::where('sec_conc_id',$secConcedente)
        ->where('dtCancelamento',NULL)
        ->where('dt4Via','<>',null)
        ->whereNotIn('id', $iFolha)
        ->get();
      }else{
        $tcesAtivos = Tce::where('concedente_id',$concedente)
        ->where('dtCancelamento',NULL)
        ->where('dt4Via','<>',null)
        ->whereNotIn('id', $iFolha)
        ->get();
      }
      foreach($tcesAtivos->chunk(50) as $chunk){
        foreach($chunk as $tce){
          if(($tce->dtInicio <= \Carbon\Carbon::now()->format('Y-m-d'))){
            if($tce->aditivos()->count() > 0){
              if($tce->aditivos()->latest()->first()->dtFim >= \Carbon\Carbon::now()->format('Y-m-d')){
                FolhaPagamentoItem::createItem($folha, $base, $trabalhados, $tce);
              }
            }else if($tce->dtFim >= \Carbon\Carbon::now()->format('Y-m-d')){
              FolhaPagamentoItem::createItem($folha, $base, $trabalhados, $tce);
            }
          }
        }
      }
    }

    public static function createItem($folha, $base, $trabalhados, $tce){
      try{
      $formulario['vlTotal'] = 0;
      $formulario['referenciaMes'] = $folha->referenciaMes;
      $formulario['referenciaAno'] = $folha->referenciaAno;
      $formulario['diasBaseEstagiados'] = $base;
      $formulario['diasTrabalhados'] = $trabalhados;
      $formulario['mesesPropRecesso'] = 0;
      $formulario['diasRecesso'] = 0;
      $formulario['vlAuxilioTransporteReceber'] = number_format($tce->vlAuxilioTransporte, 2, '.', '');
      $formulario['vlAuxilioMensalReceber'] = $tce->vlAuxilioMensal;
      $formulario['faltas'] = 0;
      $vlRecesso = 0;
      $formulario['justificadas'] = 0;
      $formulario['folha_id'] = $folha->id;
      $formulario['tce_id'] = $tce->id;
      $formulario['concedente_id'] = $tce->concedente_id;
      $formulario['secConcedente_id'] = $tce->sec_conc_id;
      if(\Carbon\Carbon::parse($tce->dtCancelamento)->year === \Carbon\Carbon::now()->year && abs((\Carbon\Carbon::parse($tce->dtCancelamento)->month - \Carbon\Carbon::now()->month)) <= 1){
        $formulario['diasTrabalhados'] = abs((\Carbon\Carbon::parse($tce->dtCancelamento)->day - 30));
      }else if(\Carbon\Carbon::parse($tce->dtFim)->year === \Carbon\Carbon::now()->year && abs((\Carbon\Carbon::parse($tce->dtFim)->month - \Carbon\Carbon::now()->month)) <= 1){
        $formulario['diasTrabalhados'] = abs((\Carbon\Carbon::parse($tce->dtFim)->day - 30));
      }
      $formulario['user_id'] = Auth::id();
      if($tce->sec_conc_id != null){
        $formulario['hash'] = base64_encode($formulario['referenciaMes'].$formulario['referenciaAno'].$tce->secConcedente_id);
      }else{
        $formulario['hash'] = base64_encode($formulario['referenciaMes'].$formulario['referenciaAno'].$tce->concedente_id);
      }
      $formulario['descontoVlAuxTransporte'] = 0;
      $formulario['descontoVlAuxMensal'] = 0;
      if($formulario['vlAuxilioMensalReceber'] > 0){
        $formulario['vlTotal'] = abs($vlRecesso + $formulario['vlAuxilioTransporteReceber'] + $formulario['vlAuxilioMensalReceber']);
      }

      FolhaPagamentoItem::create($formulario);
      }catch(\Exception $e){
        // return dd($e);
      }
    }

    public static function filterItens($itens, $filtros){
      if($filtros['modelo'] == 'bloqueio'){
        $itens = $itens->where('is_block',1);
      }else{
        if($filtros['itesnDesc'] === 'nao'){
          $itens = $itens->where('descontoVlAuxTransporte',0)->where('descontoVlAuxMensal',0);
        }

        if($filtros['valor'] != null && $filtros['valor'] != 0){
          $itens = $itens->where('vlTotal',$filtros['parametro'],$filtros['valor']);
        }
      }

      if($filtros['ordenacao'] == 'nomeCres'){
        $itens = $itens->sortBy(function($item){
          return preg_replace('/[`^~\'"]/', null, iconv('UTF-8', 'ASCII//TRANSLIT', $item->tce->estudante->nmEstudante));
        });
      }else if($filtros['ordenacao'] == 'nomeDec'){
        $itens = $itens->sortByDesc(function($item){
          return preg_replace('/[`^~\'"]/', null, iconv('UTF-8', 'ASCII//TRANSLIT', $item->tce->estudante->nmEstudante));
        });
      }else if($filtros['ordenacao'] == 'nTce'){
        $itens = $itens->sortBy(function($item){
          return $item->tce->id;
        });
      }else if($filtros['ordenacao'] == 'dtInicio'){
        $itens = $itens->sortBy(function($item){
          return $item->tce->dtInicio;
        });
      }else if($filtros['ordenacao'] == 'dtFim'){
        $itens = $itens->sortBy(function($item){
          return $item->tce->dtFim;
        });
      }

      return $itens;
    }

    public static function presenteNasFolhasAnteriores($id, $beforeMonth1, $beforeMonth2, $year1, $year2){
      $validate = true;
      $data = [
      1 => 'JANEIRO',
      2 => 'FEVEREIRO',
      3 => 'MARÇO',
      4 => 'ABRIL',
      5 => 'MAIO',
      6 => 'JUNHO',
      7 => 'JULHO',
      8 => 'AGOSTO',
      9 => 'SETEMBRO',
      10 => 'OUTUBRO',
      11 => 'NOVEMBRO',
      12 => 'DEZEMBRO'
      ];

       $folha1 = FolhaPagamento::where('referenciaMes',$beforeMonth1.'-'.$data[$beforeMonth1])->where('referenciaAno', $year1)->get();
       $folha1 = $folha1->where('concedente_id', Auth::user()->concedente_id);
       $folha2 = FolhaPagamento::where('referenciaMes',$beforeMonth2.'-'.$data[$beforeMonth2])->where('referenciaAno', $year2)->get();
       $folha2 = $folha2->where('concedente_id', Auth::user()->concedente_id);
       if(Auth::user()->sec_concedente_id != null){
         $folha1 = $folha1->where('secConcedente_id', Auth::user()->sec_concedente_id);
         $folha2 = $folha2->where('secConcedente_id', Auth::user()->sec_concedente_id);
       }

       $folha1 = $folha1->first();
       $folha2 = $folha2->first();

       if($folha1){
         if(FolhaPagamentoItem::where('tce_id', $id)->where('folha_id', $folha1->id)->get()->count() == 0){
           $validate = false;
         }
       }

       if($folha2){
         if(FolhaPagamentoItem::where('tce_id', $id)->where('folha_id', $folha2->id)->get()->count() == 0){
           $validate = false;
         }
       }

      return $validate;
    }

}
