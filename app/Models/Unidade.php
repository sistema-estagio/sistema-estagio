<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Unidade extends Model
{
    protected $table        = 'unidades';
    protected $fillable     = ['nmUnidade'];

    public function search($keySearch, $totalPage)
    {
        return $this
            ->where('nmUnidade', 'LIKE', "%{$keySearch}%")
            ->paginate(config('sysGlobal.Paginacao'));
    }

    public function estado(){
        return $this->belongsToMany(Estado::class,'unidades_estados');
    }
}
