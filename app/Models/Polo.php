<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Polo extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
 
    protected $fillable = [
        'nmPolo','cdCNPJ',
        'nmDiretor','nmDiretorCargo',
        'nmSupervisor','nmSupervisorCargo',
        'nmReitor','nmReitorCargo','dsEmail',
        'dsEndereco', 'nnNumero',
        'nmBairro', 'cdCEP', 'dsFone',
        'dsOutro', 'usuario_id',
        'estado_id', 'cidade_id',
    ];

    // INICIO PARTE QUE CONVERTE CADA CAMPO PARA MAIUSCULO NO INSERT E UPDATE
    public function setnmDiretorAttribute($value)
    //Converte para Maiusculo
    {
        $this->attributes['nmDiretor'] = mb_strtoupper($value, 'UTF-8');
    }
    public function setnmDiretorCargoAttribute($value)
    //Converte para Maiusculo
    {
        $this->attributes['nmDiretorCargo'] = mb_strtoupper($value, 'UTF-8');
    }

    public function setnmSupervisorAttribute($value)
    //Converte para Maiusculo
    {
        $this->attributes['nmSupervisor'] = mb_strtoupper($value, 'UTF-8');
    }
    public function setnmSupervisorCargoAttribute($value)
    //Converte para Maiusculo
    {
        $this->attributes['nmSupervisorCargo'] = mb_strtoupper($value, 'UTF-8');
    }

    public function setnmReitorAttribute($value)
    //Converte para Maiusculo
    {
        $this->attributes['nmReitor'] = mb_strtoupper($value, 'UTF-8');
    }
    public function setnmReitorCargoAttribute($value)
    //Converte para Maiusculo
    {
        $this->attributes['nmReitorCargo'] = mb_strtoupper($value, 'UTF-8');
    }

    public function setdsEmailAttribute($value)
    //Converte para Maiusculo
    {
        $this->attributes['dsEmail'] = mb_strtoupper($value, 'UTF-8');
    }
    public function setdsEmailCargoAttribute($value)
    //Converte para Maiusculo
    {
        $this->attributes['dsEmail'] = mb_strtoupper($value, 'UTF-8');
    }

    public function setnmPoloAttribute($value)
    //Converte para Maiusculo
    {
        $this->attributes['nmPolo'] = mb_strtoupper($value, 'UTF-8');
    }
    public function setdsEnderecoAttribute($value)
    //Converte para Maiusculo
    {
        $this->attributes['dsEndereco'] = mb_strtoupper($value, 'UTF-8');
    }
    public function setnmBairroAttribute($value)
    //Converte para Maiusculo
    {
        $this->attributes['nmBairro'] = mb_strtoupper($value, 'UTF-8');
    }
    // FIM PARTE QUE CONVERTE CADA CAMPO PARA MAIUSCULO NO INSERT E UPDATE
    //

    public function cidade()
    {
        return $this->belongsTo(Cidade::class);
    }

    public function Instituicoes()
    {
        return $this->hasMany(Instituicao::class);
    }
    
}
