<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TarefaOportunidade extends Model
{
    //
    //
    protected $table = 'tarefa_oportunidades';
    protected $fillable = [
        'tarefa_id', 'oportunidade_id'
    ];

    public function tarefa(){
      return $this->belongsTo(Tarefa::class);
    }

    public function oportunidade(){
      return $this->belongsTo(Oportunidade::class);
    }
}
