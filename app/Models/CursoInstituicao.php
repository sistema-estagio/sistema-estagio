<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CursoInstituicao extends Model
{
    //nome da tabela no bd
    protected $table = 'instituicoes_cursos';

    protected $fillable = [
        'instituicao_id',
        'curso_id',
        'curso_duracao_id',
        'qtdDuracao',
        'qtdEstagio',
    ];
    public $timestamps = false;

    public function instituicao()
    {
        //return $this->belongsTo(Curso::class, "curso_id", "id");
        return $this->belongsTo(Instituicao::class);
    }

    // public function curso2()
    // {
    //     return $this->belongsToMany(Curso::class, 'cursos_instituicoes', "instituicao_id", "curso_id");

    //             //return $this->hasMany(Curso::class);
    // }
    public function curso()
    {
        //return $this->belongsToMany(Curso::class, 'cursos_instituicoes', "instituicao_id", "curso_id")->withPivot('qtdDuracao');
        return $this->belongsTo(Curso::class)->withDefault([
            'nmCurso'=> 'Curso não cadastrado'
          ]);
    }

    public function cursoDuracao()
    {
        //return $this->belongsToMany(Curso::class, 'cursos_instituicoes', "instituicao_id", "curso_id")->withPivot('qtdDuracao');
        return $this->belongsTo(CursoDuracao::class);
    }

    //testando turno
    public function turnos()
    {
        return $this->belongsToMany(Turno::class, 'turnos_cursos', "curso_instituicao_id", "turno_id");
    }


}
