<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Requisito extends Model
{
    //
    protected $table = 'requisitos';

    protected $dates = ['created_at',];

    protected $fillable = [
        'tipo', 'titulo', 'descricao', 'concedente_id', 'secretaria_id'
    ];

    public function requisitos(){
      return $this->hasMany(OportunidadeRequisito::class);
    }
}
