<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Carbon\Carbon;
//use OwenIt\Auditing\Contracts\Auditable;

//class Tce extends Model implements Auditable
class Tce extends Model
{
    //use \OwenIt\Auditing\Auditable;

    protected $dates = ['dtInicio','dtFim','dtCancelamento','cancelado_at','dtDeclaracao','dt4Via','dt4Via_at',];
    protected $fillable = [
        'centroDeCusto',
        'migracao', 'estudante_id', 'instituicao_id', 'concedente_id', 'sec_conc_id',
        'curso_id', 'nnSemestreAno', 'nmSupervisor', 'dsSupervisorCargo','cdCPFSupervisor',
        'nmSupervisorIe', 'dsSupervisorIeCargo', 'dtInicio', 'dtFim', 'periodo_id',
        'dsPeriodo', 'hrInicio', 'hrFim', 'vlAuxilioMensal', 'vlAuxilioAlimentacao', 'vlAuxilioTransporte', 'auxDescriminado',
        'dsApolice', 'dsLotacao', 'dtDeclaracao', 'dt4Via', 'userDt4Via_id', 'dt4Via_at', 'relatorio_id', 'user_id',
        'motivoCancelamento_id', 'dsMotivoCancelamento', 'usuarioCancelamento_id', 'usuarioConcedenteCancelamento_id', 'dtCancelamento', 'cancelado_at', 'estado_id', 'supervisor_id'
    ];

    /**/
    public function search($keySearch, $totalPage)
    {
        return $this
        ->where('nmEstudante', 'LIKE', "%{$keySearch}%")
        ->orWhere('cdRG','LIKE', "%{$keySearch}%")
        ->orWhere('cdCPF','LIKE',"%{$keySearch}%")
        ->paginate(config('sysGlobal.Paginacao'));
    }

    public function supervisor(){
        return $this->belongsTo(Supervisor::class)->withDefault([
          'nome' => 'Nenhum supervisor cadastrado'
        ]);
    }

    public function setVlAuxilioMensalAttribute($value)
    {
        if($value){
            $this->attributes['vlAuxilioMensal'] = str_replace(array(".",","), array("","."), $value);
        } else {
            $this->attributes['vlAuxilioMensal'] = null;
        }
    }

    public function setVlAuxilioAlimentacaoAttribute($value)
    {
        if($value){
            $this->attributes['vlAuxilioAlimentacao'] = str_replace(array(".",","), array("","."), $value);
        } else {
            $this->attributes['vlAuxilioAlimentacao'] = null;
        }
    }

    public function setVlAuxilioTransporteAttribute($value)
    {
        if($value){
            $this->attributes['vlAuxilioTransporte'] = str_replace(array(".",","), array("","."), $value);
        } else {
            $this->attributes['vlAuxilioTransporte'] = null;
        }
    }
    //Mascaras Datas
    public function setDtInicioAttribute($value)
    {
        $this->attributes['dtInicio'] = Carbon::createFromFormat('d/m/Y', $value);
    }

    public function setDtFimAttribute($value)
    {
        $this->attributes['dtFim'] = Carbon::createFromFormat('d/m/Y', $value);
    }

    public function setDtDeclaracaoAttribute($value)
    {
        $this->attributes['dtDeclaracao'] = Carbon::createFromFormat('d/m/Y', $value);
    }

    public function setDt4ViaAttribute($value)
    {
        $this->attributes['dt4Via'] = Carbon::createFromFormat('d/m/Y', $value);
    }

    public function setDtCancelamentoAttribute($value)
    {
        $this->attributes['dtCancelamento'] = Carbon::createFromFormat('d/m/Y', $value);
    }
    //Fim Mascara Datas

    public function setnmSupervisorAttribute($value)
    {
    //Converte para Maiusculo
        $this->attributes['nmSupervisor'] = mb_strtoupper($value, 'UTF-8');
    }

    public function setdsSupervisorCargoAttribute($value)
    {
        //Converte para Maiusculo
            $this->attributes['dsSupervisorCargo'] = mb_strtoupper($value, 'UTF-8');
    }

    public function setnmSupervisorIeAttribute($value)
    {
    //Converte para Maiusculo
        $this->attributes['nmSupervisorIe'] = mb_strtoupper($value, 'UTF-8');
    }

    public function setdsSupervisorIeCargoAttribute($value)
    {
    //Converte para Maiusculo
        $this->attributes['dsSupervisorIeCargo'] = mb_strtoupper($value, 'UTF-8');
    }

    public function setdsMotivoCancelamentoAttribute($value)
    {
        //Converte para Maiusculo
        $this->attributes['dsMotivoCancelamento'] = mb_strtoupper($value, 'UTF-8');
    }

    public function setdsPeriodoAttribute($value)
    {
        //Converte para Maiusculo
        $this->attributes['dsPeriodo'] = mb_strtoupper($value, 'UTF-8');
    }

    public function setdsLotacaoAttribute($value)
    {
        //Converte para Maiusculo
        $this->attributes['dsLotacao'] = mb_strtoupper($value, 'UTF-8');
    }

    public function setauxDescriminadoAttribute($value)
    {
    //Converte para Maiusculo
        $this->attributes['auxDescriminado'] = mb_strtoupper($value, 'UTF-8');
    }
    public function atividades()
    {
        return $this->belongsToMany(Atividade::class, 'atividade_tces', "tce_id", "atividade_id")->withTimestamps();;
    }
    public function instituicao()
    {
        return $this->belongsTo(Instituicao::class);
    }
    public function estudante()
    {
        return $this->belongsTo(Estudante::class);
    }
    public function concedente()
    {
        return $this->belongsTo(Concedente::class);
    }
    public function periodo()
    {
        return $this->belongsTo(Periodo::class);
    }
    public function relatorio()
    {
        return $this->belongsTo(Relatorio::class);
    }
    //Teste
    public function aditivos()
    {
        return $this->hasMany(Aditivo::class);
    }
    public function CursoDaInstituicao()
    {
        //return $this->hasMany(CursoInstituicao::class);
        return $this->belongsTo(CursoInstituicao::class, 'curso_id');

        //return $this->belongsToMany(Curso::class, 'turnos_cursos', "curso_instituicao_id", "turno_id");

        //return $this->belongsToMany(Curso::class, 'curso_id', "cursos_instituicoes", "curso_id");
    }
    public function listaAtividades()
    {
        return $this->hasMany(AtividadeTce::class);
    }

    public function userCancelamento()
    {
        return $this->belongsTo(User::class, 'usuarioCancelamento_id');
    }

    public function userCadastro()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function userValidacao()
    {
        return $this->belongsTo(User::class, 'userDt4Via_id');
    }

    public function MotivoCancelamento()
    {
        return $this->belongsTo(MotivoCancelamento::class, 'motivoCancelamento_id');
    }
    //pegar secretaria da concedente
    public function secConcedente()
    {
        return $this->belongsTo(SecConcedente::class, 'sec_conc_id');
    }
    public function relatorioSemestrals(){
        return $this->hasMany(RelatorioSemestralAtividade::class);
    }

    public static function getVencidos($concedente, $secretaria = null){
        $tces = Tce::where('concedente_id', $concedente)->get();
        if($secretaria != null){
          $tces = $tces->where('sec_conc_id', $secretaria);
        }
        $dt = \Carbon\Carbon::now()->format('Y-m');
        $vencidos = new Collection;
        foreach($tces->where('dtCancelamento', null) as $tce){
          if($tce->aditivos()->count() > 0){
            if($tce->aditivos()->where('dtFim', '>', $tce->dtFim)->latest()->first() != null){
              if($tce->aditivos()->where('dtFim', '>', $tce->dtFim)->latest()->first()->dtFim < $dt){
                $vencidos->push($tce);
              }
            }
          }else{
            if($tce->dtFim < $dt){
              $vencidos->push($tce);
            }
          }
        }

        return $vencidos;
    }
}
