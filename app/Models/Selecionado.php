<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Selecionado extends Model
{
    protected $dates = ['dtNascimento', 'selecao_at'];
    protected $fillable = [
        'incricao', 'nome', 'nota', 'dtNascimento', 'selecao_at', 'user_id', 'classificacao', 'selecionado', 'concedente_id', 'email', 'telefone', 'cdCPF', 'semestreAno',
    ];

    public function cargo()
    {
        return $this->belongsTo(Cargo::class);
    }
}
