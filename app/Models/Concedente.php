<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Concedente extends Model
{
    protected $table = 'concedentes';
    protected $dates = ['dtFimContrato','dtInicioContrato',];
    protected $fillable = [
        'flTipo','nmRazaoSocial','nmFantasia',
        'cdCnpjCpf','dsOrgaoRegulador', 'concedente_pai',
        'nmResponsavel','dsResponsavelCargo',
        'nmAdministrador','dsAdministradorCargo',
        'dsEmail','dsEndereco','nnNumero','dsComplemento',
        'cdCEP','dsFone','dsOutroFone','dtInicioContrato','dtFimContrato','vlEstagio',
        'cidade_id','estado_id','nmBairro','logo','usuario_id','opCancelamento','assUpaTCE','assUpaADITIVO'
    ];

    public function search($keySearch, $totalPage)
    {
            return $this
            ->where('nmRazaoSocial', 'LIKE', "%{$keySearch}%")
            ->paginate(config('sysGlobal.Paginacao'));
    }
    //Mascaras Datas
    public function setDtInicioContratoAttribute($value)
    {
        if($value){
        $this->attributes['dtInicioContrato'] = Carbon::createFromFormat('d/m/Y', $value);
        }
    }

    public function setDtFimContratoAttribute($value)
    {
        if($value){
        $this->attributes['dtFimContrato'] = Carbon::createFromFormat('d/m/Y', $value);
        }
    }
    //Mascara Valores Real
    public function setVlEstagioAttribute($value)
    {
        if($value){
        $this->attributes['vlEstagio'] = str_replace(array(".",","), array("","."), $value);
        }
    }
    //CONVERSÃO PARA MAIUSCULAS
    public function setnmRazaoSocialAttribute($value){
        //Converte para Maiusculo
        $this->attributes['nmRazaoSocial'] = mb_strtoupper($value, 'UTF-8');
    }
    public function setnmFantasiaAttribute($value){
        //Converte para Maiusculo
        $this->attributes['nmFantasia'] = mb_strtoupper($value, 'UTF-8');
    }
    public function setdsOrgaoReguladorAttribute($value){
        //Converte para Maiusculo
        $this->attributes['dsOrgaoRegulador'] = mb_strtoupper($value, 'UTF-8');
    }
    public function setnmResponsavelAttribute($value){
        //Converte para Maiusculo
        $this->attributes['nmResponsavel'] = mb_strtoupper($value, 'UTF-8');
    }
    public function setdsResponsavelCargoAttribute($value){
        //Converte para Maiusculo
        $this->attributes['dsResponsavelCargo'] = mb_strtoupper($value, 'UTF-8');
    }
    public function setnmAdministradorAttribute($value){
        //Converte para Maiusculo
        $this->attributes['nmAdministrador'] = mb_strtoupper($value, 'UTF-8');
    }
    public function setdsAdministradorCargoAttribute($value){
        //Converte para Maiusculo
        $this->attributes['dsAdministradorCargo'] = mb_strtoupper($value, 'UTF-8');
    }
    public function setnmBairroAttribute($value){
        //Converte para Maiusculo
        $this->attributes['nmBairro'] = mb_strtoupper($value, 'UTF-8');
    }
    public function setdsEnderecoAttribute($value){
        //Converte para Maiusculo
        $this->attributes['dsEndereco'] = mb_strtoupper($value, 'UTF-8');
    }
    public function setdsComplementoAttribute($value){
        //Converte para Maiusculo
        $this->attributes['dsComplemento'] = mb_strtoupper($value, 'UTF-8');
    }
    //FIM CONVERSÃO PARA MAIUSCULAS
    public function cidade()
    {
        //return $this->belongsTo(Curso::class, "curso_id", "id");
        return $this->belongsTo(Cidade::class);
    }

    public function tce(){
        return $this->hasMany(Tce::class);
    }

    //pegar usuarios concedentes
    public function userConcedente()
    {
        return $this->hasMany(UserConcedente::class);
    }
    //pegar secretarias da concedente
    public function secConcedente()
    {
        return $this->hasMany(SecConcedente::class);
    }

    public function supervisor(){
        return $this->belongsTo(Supervisor::class);
    }

    //pega secretarias da concedente para editar
    public function secretaria()
    {
        //return $this->belongsTo(Curso::class, "curso_id", "id");
        return $this->belongsTo(SecConcedente::class);
    }

    //pegar folhas de pagamentos
    public function folhasPagamento()
    {
        return $this->hasMany(FolhaPagamento::class);
    }
}
