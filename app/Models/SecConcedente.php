<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SecConcedente extends Model
{
    protected $table = 'sec_concedentes';

    protected $fillable = [
        'nmSecretaria','cdCnpjCpf',
        'nmResponsavel','dsResponsavelCargo',
        'nmAdministrador','dsAdministradorCargo',
        'dsEmail','dsEndereco','nmBairro','nnNumero','dsComplemento',
        'cdCEP','dsFone','dsOutroFone','concedente_id',
        'usuario_id'
    ];

    public function userconcedente(){
        return $this->belongsTo(UserConcedente::class);
    }

    public function setnmSecretariaAttribute($value){
        //Converte para Maiusculo
        $this->attributes['nmSecretaria'] = mb_strtoupper($value, 'UTF-8');
    }
    public function setnmResponsavelAttribute($value){
        //Converte para Maiusculo
        $this->attributes['nmResponsavel'] = mb_strtoupper($value, 'UTF-8');
    }
    public function setdsResponsavelCargoAttribute($value){
        //Converte para Maiusculo
        $this->attributes['dsResponsavelCargo'] = mb_strtoupper($value, 'UTF-8');
    }
    public function setnmAdministradorAttribute($value){
        //Converte para Maiusculo
        $this->attributes['nmAdministrador'] = mb_strtoupper($value, 'UTF-8');
    }
    public function setdsAdministradorCargoAttribute($value){
        //Converte para Maiusculo
        $this->attributes['dsAdministradorCargo'] = mb_strtoupper($value, 'UTF-8');
    }
    public function setnmBairroAttribute($value){
        //Converte para Maiusculo
        $this->attributes['nmBairro'] = mb_strtoupper($value, 'UTF-8');
    }
    public function setdsEnderecoAttribute($value){
        //Converte para Maiusculo
        $this->attributes['dsEndereco'] = mb_strtoupper($value, 'UTF-8');
    }
    public function setdsComplementoAttribute($value){
        //Converte para Maiusculo
        $this->attributes['dsComplemento'] = mb_strtoupper($value, 'UTF-8');
    }

    public function concedente()
    {
        return $this->belongsTo(Concedente::class);
    }

    public function supervisor(){
        return $this->belongsTo(Supervisor::class);
    }

    public function tce(){
        return $this->hasMany(Tce::class);
    }

    //pegar folhas de pagamentos
    public function folhasPagamento()
    {
        return $this->hasMany(FolhaPagamento::class);
    }
}
