<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FolhaPagamento extends Model
{
    //nome da tabela no bd
    protected $table = 'folhas_pagamentos';
    protected $fillable = [
        'concedente_id','secConcedente_id',
        'referenciaMes','referenciaAno','hash','obs',
        'user_id',
    ];

    protected $hidden = ['fechado', 'fechado_at', 'fechado_user'];
    public $timestamps = true;

    public function concedente()
    {
        return $this->belongsTo(Concedente::class);
    }

    public function secConcedente()
    {
        return $this->belongsTo(SecConcedente::class,'secConcedente_id');
    }

    public function folhaItem()
    {
        return $this->belongsTo(FolhaPagamentoItem::class);
    }

    public function userCadastro()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public static function getReferenciaMesAnterior($referencia){
      $data = [
          1 => '1-JANEIRO',
          2 => '2-FEVEREIRO',
          3 => '3-MARÇO',
          4 => '4-ABRIL',
          5 => '5-MAIO',
          6 => '6-JUNHO',
          7 => '7-JULHO',
          8 => '8-AGOSTO',
          9 => '9-SETEMBRO',
          10 => '10-OUTUBRO',
          11 => '11-NOVEMBRO',
          12 => '12-DEZEMBRO'
      ];
      if($referencia == 1){
        $i = 12;
      }else{
        $i = (explode('-',$referencia)[0] - 1);
      }
      return $data[$i];
    }
}
