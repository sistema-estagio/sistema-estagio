<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class UserConcedente extends Authenticatable
{

    use HasRoles, Notifiable, SoftDeletes;

    protected $guard = 'web_concedente'; // or whatever guard you want to use

    protected $table = 'users_concedente';

    protected $fillable = ['tipo', 'name', 'email', 'password','usuario_id','concedente_id','sec_concedente_id', 'deleted_at'];

    protected $hidden = ['tipo', 'password', 'remember_token'];

    protected $dates = ['deleted_at', 'created_at', 'updated_at'];

    public function search($keySearch, $totalPage)
    {
        return $this
            ->where('name', 'LIKE', "%{$keySearch}%")
            ->paginate($totalPage);
    }

    public function concedente()
    {
        return $this->belongsTo(Concedente::class);
    }

    public function secConcedente()
    {
        return $this->hasOne(SecConcedente::class,'sec_concedente_id');
    }

    public function userCadastro()
    {
        return $this->belongsTo(User::class, 'usuario_id');
    }


    protected static function verifyUser($id,$pass){
      $user = UserConcedente::find($id);
      if($user){
        if($user->tipo != $pass){
          return redirect()->back();
        }
      }
    }

    protected static function verifyUserReturn($id,$pass){
      $user = UserConcedente::find($id);
      if($user){
        if($user->tipo === $pass){
          return true;
        }
      }
      return false;
    }
}
