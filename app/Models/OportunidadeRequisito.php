<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OportunidadeRequisito extends Model
{
    //
    protected $table = 'oportunidade_requisitos';

    protected $dates = ['created_at'];

    protected $fillable = [
        'isObrigatorio', 'tipo', 'semestre', 'requisito_id', 'oportunidade_id'
    ];

    public function requisito(){
      return $this->belongsTo(Requisito::class);
    }

    public function oportunidade(){
      return $this->belongsTo(Oportunidade::class);
    }
}
