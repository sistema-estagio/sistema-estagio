<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

class Supervisor extends Model
{
    protected $fillable = [
      'nome', 'cpf', 'email', 'telefone', 'cargo', 'concedente_id', 'secConcedente_id', 'created_at',
  ];

    public function setnomeAttribute($value)
    {
        //Converte para Maiusculo
        $this->attributes['nome'] = mb_strtoupper($value, 'UTF-8');
    }

    public function setcargoAttribute($value)
    {
        //Converte para Maiusculo
        $this->attributes['cargo'] = mb_strtoupper($value, 'UTF-8');
    }

    public function concedente()
    {
        return $this->hasOne(Concedente::class, 'concedente_id');
    }

    public function secConcedente()
    {
        return $this->hasOne(SecConcedente::class, 'secConcedente_id');
    }

    public function tces()
    {
        return $this->hasMany(Tce::class);
    }

    public function aditivos()
    {
        return $this->hasMany(Aditivo::class);
    }

    public static function validate($supervisor)
    {
        if ($supervisor) {
            $aditivos = Aditivo::where('supervisor_id', $supervisor->id)->get();
            $tces = Tce::where('supervisor_id', $supervisor->id)->get();
            $tces = $tces->where('dtCancelamento', null);
            $total = 0;
            foreach ($tces as $tce) {
                if ($tce->aditivos()->count() > 0) {
                    if (($tce->aditivos()->count() > 1) && ($tce->aditivos()->first()->supervisor_id != $supervisor->id)) {
                        ++$total;
                    }
                } else {
                    ++$total;
                }
            }

            if ($aditivos->count() > 0) {
                foreach ($aditivos as $aditivo) {
                    $aditivosTce = $aditivos->where('tce_id', $aditivo->tce_id)->where('supervisor_id', '!=', null)->where('supervisor_id', '!=', $supervisor->id);
                    if ($aditivosTce->count() > 1) {
                        if ($aditivosTce->sortBy('created_at')->first()->created_at < $aditivo->created_at) {
                            ++$total;
                        }
                    } else {
                        ++$total;
                    }
                }
            }
        }
        if ($total < 10) {
            return true;
        } else {
            return false;
        }
    }

    public static function contagem($supervisor)
    {
        if ($supervisor) {
            $aditivos = Aditivo::where('supervisor_id', $supervisor->id)->get();
            $tces = Tce::where('supervisor_id', $supervisor->id)->get();
            $tces = $tces->where('dtCancelamento', null);
            $total = 0;
            foreach ($tces as $tce) {
                if ($tce->aditivos()->count() > 0) {
                    if (($tce->aditivos()->count() > 1) && ($tce->aditivos()->first()->supervisor_id == $supervisor->id)) {
                        ++$total;
                    }
                } else {
                    ++$total;
                }
            }

            foreach ($aditivos as $aditivo) {
                $aditivosTce = $aditivos->where('tce_id', $aditivo->tce_id)->where('supervisor_id', '!=', null)->where('supervisor_id', '!=', $supervisor->id);
                if ($aditivosTce->count() > 1) {
                    if ($aditivosTce->sortByDesc('created_at')->first()->created_at < $aditivo->created_at) {
                        ++$total;
                    }
                } else {
                    ++$total;
                }
            }
        }

        return $total;
    }

    public static function getTcesAditivos($supervisor)
    {
        if ($supervisor) {
            $tcesSupervisor = new Collection();
            $aditivos = Aditivo::where('supervisor_id', $supervisor->id)->get();
            $tces = Tce::where('supervisor_id', $supervisor->id)->get();
            $total = 0;
            foreach ($tces as $tce) {
                if ($tce->aditivos()->count() > 0) {
                    if (($tce->aditivos()->count() > 1) && ($tce->aditivos()->first()->supervisor_id == $supervisor->id)) {
                        $tcesSupervisor->push($tce);
                    }
                } else {
                    $tcesSupervisor->push($tce);
                }
            }

            if ($aditivos->count() > 0) {
                foreach ($aditivos as $aditivo) {
                    $aditivosTce = $aditivos->where('tce_id', $aditivo->tce_id)->where('supervisor_id', '!=', null)->where('supervisor_id', '!=', $supervisor->id);
                    if ($aditivosTce->count() > 0) {
                        if ($aditivosTce->sortBy('created_at')->first()->created_at < $aditivo->created_at) {
                            $tcesSupervisor->push($aditivo->tce);
                        }
                    } else {
                        $tcesSupervisor->push($aditivo->tce);
                    }
                }
            }
        }

        return $tcesSupervisor;
    }
}
