<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OportunidadeEstudante extends Model
{
    //
    protected $table = 'oportunidade_estudantes';

    protected $dates = ['created_at'];

    protected $fillable = [
        'selecionado', 'estudante_id', 'oportunidade_id'
    ];

    public function oportunidade(){
      return $this->belongsTo(Oportunidade::class);
    }

    public function estudante(){
      return $this->belongsTo(Estudante::class);
    }
}
