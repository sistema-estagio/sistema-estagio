<?php
namespace App\SRoco;

use Carbon;


class Meses
{
  public static function getLatestMonth(){
    if(\Carbon\Carbon::now()->month == 1){
     $beforeMonth1 = 12;
     $beforeMonth2 = 11;
     $year1 = \Carbon\Carbon::now()->year - 1;
     $year2 = \Carbon\Carbon::now()->year - 1;
    }else if(\Carbon\Carbon::now()->month == 2){
     $beforeMonth1 = 1;
     $beforeMonth2 = 12;
     $year1 = \Carbon\Carbon::now()->year;
     $year2 = \Carbon\Carbon::now()->year - 1;
    }else{
     $beforeMonth1 = \Carbon\Carbon::now()->month - 1;
     $beforeMonth2 = \Carbon\Carbon::now()->month - 2;
     $year1 = \Carbon\Carbon::now()->year;
     $year2 = \Carbon\Carbon::now()->year;
    }
    return ['beforeMonth1' => $beforeMonth1, 'beforeMonth2' => $beforeMonth2, 'year1' => $year1, 'year2' => $year2];
  }
}
