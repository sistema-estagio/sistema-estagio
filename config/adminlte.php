<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Title
    |--------------------------------------------------------------------------
    |
    | The default title of your admin panel, this goes into the title tag
    | of your page. You can override it per page with the title section.
    | You can optionally also specify a title prefix and/or postfix.
    |
    */

    'title' => 'Sistema de Estagiários',

    'title_prefix' => '',

    'title_postfix' => '',

    /*
    |--------------------------------------------------------------------------
    | Logo
    |--------------------------------------------------------------------------
    |
    | This logo is displayed at the upper left corner of your admin panel.
    | You can use basic HTML here if you want. The logo has also a mini
    | variant, used for the mini side bar. Make it 3 letters or so
    |
    */

    'logo' => '<b>Sistema</b>Estágio',

    'logo_mini' => '<b>U</b>PA',

    /*
    |--------------------------------------------------------------------------
    | Skin Color
    |--------------------------------------------------------------------------
    |
    | Choose a skin color for your admin panel. The available skin colors:
    | blue, black, purple, yellow, red, and green. Each skin also has a
    | ligth variant: blue-light, purple-light, purple-light, etc.
    |
    */

    'skin' => 'green',

    /*
    |--------------------------------------------------------------------------
    | Layout
    |--------------------------------------------------------------------------
    |
    | Choose a layout for your admin panel. The available layout options:
    | null, 'boxed', 'fixed', 'top-nav'. null is the default, top-nav
    | removes the sidebar and places your menu in the top navbar
    |
    */

    'layout' => null,

    /*
    |--------------------------------------------------------------------------
    | Collapse Sidebar
    |--------------------------------------------------------------------------
    |
    | Here we choose and option to be able to start with a collapsed side
    | bar. To adjust your sidebar layout simply set this  either true
    | this is compatible with layouts except top-nav layout option
    |
    */

    'collapse_sidebar' => false,

    /*
    |--------------------------------------------------------------------------
    | URLs
    |--------------------------------------------------------------------------
    |
    | Register here your dashboard, logout, login and register URLs. The
    | logout URL automatically sends a POST request in Laravel 5.3 or higher.
    | You can set the request to a GET or POST with logout_method.
    | Set register_url to null if you don't want a register link.
    |
    */

    'dashboard_url' => '/home',

    'logout_url' => 'logout',

    'logout_method' => null,

    'login_url' => 'login',

    'register_url' => 'register',

    /*
    |--------------------------------------------------------------------------
    | Menu Items
    |--------------------------------------------------------------------------
    |
    | Specify your menu items to display in the left sidebar. Each menu item
    | should have a text and and a URL. You can also specify an icon from
    | Font Awesome. A string instead of an array represents a header in sidebar
    | layout. The 'can' is a filter on Laravel's built in Gate functionality.
    |
    */

    'menu' => [
        'NEVEGAÇÃO',
        [
            'text'    => 'Instituições de Ensino',
            'icon'    => 'university',
            'submenu' => 
            [
                [
                'text' => 'Listar',
                'url'  => 'home/instituicao',
                'icon' => 'list',
                ],
                [
                'text' => 'Adicionar',
                'url'  => 'home/instituicao/create',
                'icon' => 'plus',
                ],
                [
                'text' => 'Cursos',
                'url'  => 'home/curso',
                'icon' => 'file-text',
                ],

                //Submenu Polos Principal de Instituição de Ensino
                [
                    'text'    => 'Polos Matriz',
                    'icon'    => 'mortar-board',
                    'submenu' => 
                    [
                        [
                            'text' => 'Listar',
                            'url'  => 'home/polo',
                            'icon'    => 'list',
                        ],
                        [
                            'text' => 'Adicionar',
                            'url'  => 'home/polo/create',
                            'icon' => 'plus',
                        ],                 
                    ],
                ],
                //Fim Submenu Polos Principal de Instituição de Ensino
                //Submenu Relatorios de Instituições de Ensino
                [
                    'text'    => 'Relatorios',
                    'icon'    => 'bar-chart',
                    'submenu' => 
                    [
                        [
                            'text' => 'Instituições de Ensino',
                            'url'  => 'home/instituicao/relatorio/instituicao',
                            'icon'    => 'circle-o',
                        ],
                        [
                            'text' => 'Cursos por Instituições',
                            'url'  => 'home/instituicao/relatorio/curso',
                            'icon' => 'circle-o',
                        ],
                        [
                            'text' => 'Instituições por Curso',
                            'url'  => 'home/instituicao/relatorio/cursoinstituicao',
                            'icon' => 'circle-o',
                        ],               
                        [
                            'text' => 'Instituições por Polo',
                            //'url'  => 'home/instituicao/relatorio/poloinstituicao',
                            'url'  => '#',
                            'icon' => 'circle-o',
                        ],                 
                    ],
                ],
                //Fim Submenu Relatorios de Instituições de Ensino
            ],
        ],
        '',
        [
            'text'    => 'Concedentes',
            'icon'    => 'industry',
            'submenu' => 
            [
                [
                'text' => 'Listar',
                'url'  => 'home/concedente',
                'icon'    => 'list',
                ],
                [
                'text' => 'Adicionar',
                'url'  => 'home/concedente/create',
                'icon'    => 'plus',
                ],
                //Submenu Concedentes
                [
                    'text'    => 'Relatorios',
                    'icon'    => 'bar-chart',
                    'submenu' => 
                    [
                        [
                            'text' => 'Concedentes',
                            'url'  => 'home/concedente/relatorio/concedente',
                            'icon'    => 'circle-o',
                        ],
                    ],
                ],
                //Fim Submenu Concedentes
               
            ],
        ],
        '',
        [
            'text'    => 'Estudantes',
            'icon'    => 'users',
            'submenu' => 
            [
                //inicio Submenu Listagem Estudantes
                [
                    'text'    => 'Listar',
                    'icon'    => 'list',
                    'submenu' => 
                    [
                        [
                            'text' => 'Todos',
                            'url'  => 'home/estudante?tce=todos',
                            'icon'    => 'circle-o',
                        ],
                        [
                            'text' => 'Sem TCE',
                            'url'  => 'home/estudante?tce=sem',
                            'icon' => 'circle-o',
                        ],
                        [
                            'text' => 'Com TCE',
                            'url'  => 'home/estudante?tce=com',
                            'icon' => 'circle-o',
                        ],
                    ],
                ],
                //Fim Submenu Listagem Estudantes
                // [
                // 'text' => 'Listar',
                // 'url'  => 'home/estudante',
                // 'icon'    => 'list',
                // ],
                [
                'text' => 'Adicionar',
                'url'  => 'home/estudante/create',
                'icon'    => 'plus',
                ],
                //Submenu Estudantes
            [
                'text'    => 'Relatorios',
                'icon'    => 'bar-chart',
                'submenu' => 
                [
                    [
                        'text' => 'Estudantes',
                        'url'  => 'home/estudante/relatorio/estudante',
                        'icon'    => 'circle-o',
                    ],
                    [
                        'text' => 'Estudante Por Instituicao',
                        'url'  => 'home/estudante/relatorio/estudanteinstituicao',
                        'icon' => 'circle-o',
                    ],
                    [
                        'text' => 'Estudante Por Concedente',
                        'url'  => 'home/estudante/relatorio/estudanteconcedente',
                        'icon' => 'circle-o',
                    ],
                ],
            ],
            //Fim Submenu Estudantes
               
            ],  
        ],
        
        '',
        [
            'text'    => 'TCE',
            'icon'    => 'file-text-o',
            'submenu' => 
            [
                [
                'text' => 'Validar TCE',
                'url'  => 'home/tce/validar',
                'icon'    => 'check-square-o',
                ],
                [
                'text' => 'Listar Ativos',
                'url'  => 'home/tce',
                'icon'    => 'list',
                ],
                [
                    'text' => 'Listar Cancelados',
                    'url'  => 'home/tce/cancelados',
                    'icon'    => 'list',
                    ],
                //[
                ///'text' => 'Adicionar',
                //'url'  => 'estudante/create',
                //'icon'    => 'plus',
                //],
               //Submenu Estudantes
            [
                'text'    => 'Relatorios',
                'icon'    => 'bar-chart',
                'submenu' => 
                [
                    [
                        'text' => 'Tces',
                        'url'  => 'home/tce/relatorios/tce',
                        'icon'    => 'circle-o',
                    ],
                    [
                        'text' => 'Tces Desvalidados',
                        'url'  => 'home/tce/relatorios/tcedesvalidado',
                        'icon'    => 'circle-o',
                    ],
                    [
                        'text' => 'Tces Validados',
                        'url'  => 'home/tce/relatorios/concedentetcevalidado',
                        'icon' => 'circle-o',
                    ],
                    [
                        'text' => 'Tces por Concedente',
                        'url'  => 'home/tce/relatorios/concedentetce',
                        'icon' => 'circle-o',
                    ],
                    [
                        'text' => 'Tces Cancelados',
                        'url'  => 'home/tce/relatorios/concedentetcecancelado',
                        'icon' => 'circle-o',
                    ],
                ],
            ],
            //Fim Submenu Estudantes
            ],
        ],

        //Menu Original Abaixo
        'SISTEMA',
        [
            'text'    => 'Usuários',
            'icon'    => 'user',
            'submenu' =>
                [
                    [
                        'text' => 'Listar',
                        'url'  => 'home/usuario',
                        'icon'    => 'file-text-o',
                    ],
                    [
                        'text' => 'Adicionar',
                        'url'  => 'home/usuario/create',
                        'icon'    => 'plus',
                    ],

                ],
        ],
        '',
        [
            'text'    => 'Unidades',
            'icon'    => 'building-o',
            'submenu' =>
                [
                    [
                        'text' => 'Listar',
                        'url'  => 'home/unidade',
                        'icon'    => 'file-text-o',
                    ],
                    [
                        'text' => 'Adicionar',
                        'url'  => 'home/unidade/create',
                        'icon'    => 'plus',
                    ],

                ],
        ],
        /*
        [
            'text'    => 'Relatórios',
            'icon'    => 'bar-chart',
            'url'  => 'home/relatorio',
            'submenu' =>
                [
                    /*[
                        'text' => 'Instituições',
                        'url'  => 'home/relatorio/instituicao',
                        'icon' => 'university',
                    ],*/
                    /*[
                        'text' => 'Concedentes',
                        'url'  => 'home/relatorio/concedente',
                        'icon' => 'industry',
                    ],*/
                    /*[
                        'text' => 'Concedentes TCE',
                        'url'  => 'home/relatorio/concedentetce',
                        'icon' => 'industry',
                    ],*/
                    /*
                    [
                        'text' => 'Concedentes TCE Cancelado',
                        'url'  => 'home/relatorio/concedentetcecancelado',
                        'icon' => 'industry',
                    ],*/
                    /*[
                        'text' => 'Estudante',
                        'url'  => 'home/relatorio/estudante',
                        'icon' => 'users',
                    ],*/
                    /*[
                        'text' => 'Estudante Por Instituicao',
                        'url'  => 'home/relatorio/estudanteinstituicao',
                        'icon' => 'users',
                    ],*/
                    /*[
                        'text' => 'Cursos',
                        'url'  => 'home/relatorio/curso',
                        'icon' => 'file-text',
                    ],*/
                    /*[
                        'text' => 'Cursos Por Instituicao',
                        'url'  => 'home/relatorio/cursoinstituicao',
                        'icon' => 'file-text',
                    ],*/
                    /*[
                        'text' => 'TCE',
                        'url'  => 'home/relatorio/tce',
                        'icon' => 'graduation-cap',
                    ],
                ],
        ],
        */
    ],

    /*
    |--------------------------------------------------------------------------
    | Menu Filters
    |--------------------------------------------------------------------------
    |
    | Choose what filters you want to include for rendering the menu.
    | You can add your own filters to this array after you've created them.
    | You can comment out the GateFilter if you don't want to use Laravel's
    | built in Gate functionality
    |
    */

    'filters' => [
        JeroenNoten\LaravelAdminLte\Menu\Filters\HrefFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ActiveFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\SubmenuFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ClassesFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\GateFilter::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Plugins Initialization
    |--------------------------------------------------------------------------
    |
    | Choose which JavaScript plugins should be included. At this moment,
    | only DataTables is supported as a plugin. Set the value to true
    | to include the JavaScript file from a CDN via a script tag.
    |
    */

    'plugins' => [
        'datatables' => true,
        'select2'    => true,
       // 'inputmask'  => true,
    ],
];
