@extends('adminlte::page')

@if(!empty($titulo))
@section('title_postfix', ' - '.$titulo)
@endif
@section('css')
<link rel="stylesheet" href="{{ asset('vendor/adminlte/plugins/iCheck/square/blue.css')}}">
<link href="{{ asset('assets/vendor/toast/toastr.css')}}" rel="stylesheet">
<style media="screen">

.lds-roller {
  display: inline-block;
  position: relative;
  width: 64px;
  height: 64px;
}
.lds-roller div {
  animation: lds-roller 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
  transform-origin: 32px 32px;
}
.lds-roller div:after {
  content: " ";
  display: block;
  position: absolute;
  width: 4px;
  height: 4px;
  border-radius: 50%;
  background: #0e502d;
  margin: -3px 0 0 -3px;
}
.lds-roller div:nth-child(1) {
  animation-delay: -0.036s;
}
.lds-roller div:nth-child(1):after {
  top: 50px;
  left: 50px;
}
.lds-roller div:nth-child(2) {
  animation-delay: -0.072s;
}
.lds-roller div:nth-child(2):after {
  top: 54px;
  left: 45px;
}
.lds-roller div:nth-child(3) {
  animation-delay: -0.108s;
}
.lds-roller div:nth-child(3):after {
  top: 57px;
  left: 39px;
}
.lds-roller div:nth-child(4) {
  animation-delay: -0.144s;
}
.lds-roller div:nth-child(4):after {
  top: 58px;
  left: 32px;
}
.lds-roller div:nth-child(5) {
  animation-delay: -0.18s;
}
.lds-roller div:nth-child(5):after {
  top: 57px;
  left: 25px;
}
.lds-roller div:nth-child(6) {
  animation-delay: -0.216s;
}
.lds-roller div:nth-child(6):after {
  top: 54px;
  left: 19px;
}
.lds-roller div:nth-child(7) {
  animation-delay: -0.252s;
}
.lds-roller div:nth-child(7):after {
  top: 50px;
  left: 14px;
}
.lds-roller div:nth-child(8) {
  animation-delay: -0.288s;
}
.lds-roller div:nth-child(8):after {
  top: 45px;
  left: 10px;
}
@keyframes lds-roller {
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
}

.justify-content-center{
  justify-content: center!important;
}

</style>
@endsection
@section('content_header')
@if($folha->secConcedente_id != null)
<h1>Financeiro Secretaria</h1>
<ol class="breadcrumb">
  <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
  <li><a href="{{route('concedente.index')}}"><i class="fa fa-industry"></i> Concedentes</a></li>
  <li><a href="{{route('concedente.show', ['concedente' => $folha->concedente->id])}}"><i class="fa fa-file"></i> {{$folha->concedente->nmRazaoSocial}}</a></li>
  <li><a href="{{route('concedente.secretaria.show', ['idConcedente' => $folha->concedente->id, 'id' => $folha->secConcedente_id])}}"><i class="fa fa-file"></i> {{$folha->secConcedente->nmSecretaria}}</a></li>
  <li><a href="{{route('concedente.secretaria.financeiro', ['idConcedente' => $folha->concedente->id, 'id' => $folha->secConcedente_id])}}"><i class="fa fa-calculator"></i> Folha de Pagamento</a></li>
  <li class="active"><i class="fa fa-calculator"></i> {{$folha->referenciaMes}}/{{$folha->referenciaAno}}</li>
</ol>
@else
<h1>Financeiro Concedente</h1>
<ol class="breadcrumb">
  <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
  <li><a href="{{route('concedente.index')}}"><i class="fa fa-industry"></i> Concedentes</a></li>
  <li><a href="{{route('concedente.show', ['concedente' => $folha->concedente->id])}}"><i class="fa fa-file"></i> {{$folha->concedente->nmRazaoSocial}}</a></li>
  <li><a href="{{route('concedente.financeiro', ['concedente' => $folha->concedente->id])}}"><i class="fa fa-calculator"></i> Folha de Pagamento</a></li>
  <li class="active"><i class="fa fa-calculator"></i> {{$folha->referenciaMes}}/{{$folha->referenciaAno}}</li>
</ol>
@endif
@stop

@section('content')


@if(session('success'))
<div class="alert alert-success alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
  <h4>
    <i class="icon fa fa-check"></i> Alerta!</h4>
    {{session('success')}}
  </div>
  @endif

  @if(session('error'))
  <div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4>
      <i class="icon fa fa-ban"></i> Alerta!</h4>
      {{session('error')}}
    </div>
    @endif
    <div class="row">
      <a href="javascript:void(0);" onclick="showRow('Pagamento')" data-toggle="tooltip" data-placement="bottom" title="Mostrar todas as tabelas">
        <div class="col-md-3">
          <div class="info-box bg-green">
            <span class="info-box-icon"><i class="fa fa-list-ol"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Tce's na Folha</span>
              <span class="info-box-number" id="itens-count" data-count="{{$itensFolha->count()}}">{{$itensFolha->count()}}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
        </div>
      </a>
      <a href="javascript:void(0);" onclick="showRow('Ativos')" data-toggle="tooltip" data-placement="bottom" title="Mostrar todas as tabelas">
        <div class="col-md-3">
          <div class="info-box bg-blue">
            <span class="info-box-icon"><i class="fa fa-check-square-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Tce's Ativos</span>
              <span class="info-box-number" id="ativos-count" data-count="{{$tcesAtivos->count()}}">{{$tcesAtivos->count()}}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
        </div>
      </a>
      <a href="javascript:void(0);" onclick="showRow('Cancelados')" data-toggle="tooltip" data-placement="bottom" title="Mostrar todas as tabelas">
        <div class="col-md-3">
          <div class="info-box bg-red">
            <span class="info-box-icon"><i class="fa fa-times"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Tce's Cancelados</span>
              <span class="info-box-number" id="cancelados-count" data-count="{{$tcesCancelados->count()}}">{{$tcesCancelados->count()}}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
        </div>
      </a>
      <a href="javascript:void(0);" onclick="showRow('all')" data-toggle="tooltip" data-placement="bottom" title="Mostrar todas as tabelas">
        <div class="col-md-3">
          <div class="info-box bg-aqua">
            <span class="info-box-icon"><i class="fa fa-area-chart"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total</span>
              <span class="info-box-number" id="total-count" data-count="{{$itensFolha->count()+$tcesAtivos->count()+$tcesCancelados->count()}}">{{$itensFolha->count()+$tcesAtivos->count()+$tcesCancelados->count()}}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
        </div>
      </a>
    </div>
    <div class="row justify-content-center" style="margin-bottom:20px;">
      <div class="col-md-8">
        <button type="button" name="log-ajax" class="btn btn-danger btn-lg btn-flat" data-toggle="modal" data-target="#log" style="display:none;"><i class="fa fa-exclamation-circle"></i> Tce's Não Adicionados</button>
      </div>
      <div class="col-md-4">
        <form action="{{route('financeiro.relatorio')}}" method="post" style="display:none;" id="form-post-relatorio">
          {!! csrf_field() !!}
          <input type="hidden" name="id" value="">
          <input type="hidden" name="modelo" value="">
          <input type="submit" name="submit" value="">
        </form>
        @if($folha->fechado == 'N')
        <a href="{{ route('financeiro.folha.fechar', ['id'=>$folha->hash]) }}" role="button" class="btn bg-green btn-flat btn-lg pull-right" onclick="return confirm('Deseja fechar a folha?')" style="margin-left:40px;">Fechar Folha</a>
        @endif
        <button type="button" role="button" class="btn bg-gray btn-flat btn-lg pull-right" data-toggle="modal"  data-target="#modelModelo"><i class="fa fa-file-pdf-o"></i> Relatório</button>

        <!-- Modal de Relatório -->
        <div class="modal" tabindex="-1" role="dialog" id="modelModelo">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Relatório Folha</h5>
              </div>
              <form action="{{route('financeiro.relatorio')}}" method="post" id="form-relatorio-post">
                {!! csrf_field() !!}
                <input type="hidden" name="id" value="{{$folha->id}}">
                <input type="hidden" name="parametro" value=">=">
                <div class="modal-body">
                  <div class="form-group row">
                    <div class="col-lg-4">
                      <label for="selOrdenacao">Modelo</label><br>
                      <select class="form-control" id="selOrdenacao" name="modelo" required>
                        <option value="" selected>Selecione...</option>
                        <option value="upa">Pagamento 1</option>
                        <option value="recife">Pagamento 2</option>
                        <option value="simplificado">Pagamento Simplificado</option>
                        <option value="recibo">Recibo</option>
                        <option value="fatura">Fatura</option>
                        <option value="bloqueio">Bloqueios</option>
                      </select>
                    </div>
                    <div class="col-lg-4">
                      <label for="selOrdenacao">Ordenação</label>
                      <select class="form-control" id="selOrdenacao" name="ordenacao">
                        <option value="nomeCres" selected>Nome Crescente</option>
                        <option value="nomeDec">Nome decrescente</option>
                        <option value="nTce">Nº TCE</option>
                        <option value="dtInicio">Data Início</option>
                        <option value="dtFim">Data Fim</option>
                      </select>
                    </div>
                    <div class="col-lg-4">
                      <label for="vlSeparacao">Valor de filtragem</label>
                      <div class="input-group">
                        <span class="input-group-btn">
                          <button type="button" role="button" name="btn-muda-parametro" class="btn btn-info" value="=">=</button>
                        </span>
                        <input type="tel" class="form-control" id="vlSeparacao" min="0" value="0" name="valor" required>
                      </div>
                      <small>Digite 0 (zero) caso queira todos os itens</small>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-lg-4">
                      <label>Incluir itens com desconto?</label><br>
                      <input type="hidden" name="itesnDesc" value="sim">
                      <button type="button" class="btn btn-primary" style="margin-right: 10px;" name="btn-include" onclick="includeItens(this)">Sim</button>
                      <button type="button" class="btn btn-default" name="btn-not-include" onclick="includeItens(this)">Não</button>
                    </div>
                    <div class="col-lg-5">
                      <label for="">Comparar com a folha anterior?</label>
                      <input type="hidden" name="comparacao" value="nao">
                      <button type="button" class="btn btn-default" style="margin-right: 10px;" name="btn-comparar-sim" onclick="compararFolha(this)">Sim</button>
                      <button type="button" class="btn btn-primary" name="btn-comparar-nao" onclick="compararFolha(this)">Não</button>
                    </div>
                    <div class="col-lg-3">
                      <label for="selVisao">Gerar como:</label>
                      <select class="form-control" id="selOrdenacao" name="result">
                        <option value="view">Visão</option>
                        <option value="pdf">PDF</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                  <button type="submit" class="btn btn-primary">Confirmar</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--  Fim Botões abaixo da folha gerada  -->
    <div class="row tables-tces" id="rowPagamento" style="margin-bottom:20px;">
      <div class="col-md-12">
        <div class="box box-success">
          <div class="box-header with-border">
            <div class="col-md-4">
              <h3 class="box-title">Folha de Pagamento {{$folha->referenciaMes}}/{{$folha->referenciaAno}}</h3>
            </div>
            <div class="col-md-4">
              <div class="input-group">
                <input type="search" name="searchFolha" value="" placeholder="Buscar por nome, cpf, n° tce..." class="form-control">
                <div class="input-group-addon">
                  <i class="fa fa-search"></i>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="btn-group pull-right">
                <button type="button" class="btn btn-primary">Ações</button>
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                  <span class="caret"></span>
                  <span class="sr-only">Toggle Dropdown</span>
                </button>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="javascript:void(0);" onclick="downItensFolha()">Apagar todos os itens</a></li>
                </ul>
              </div>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table id="itemsFolha" class="table table-condensed table-striped" style="font-size: 12px;">
              <thead>
                <tr>
                  <th style="width: 5%;">TCE</th>
                  <th style="width: 20%;">Estudante</th>
                  <th style="width: 10%;">Início/Fim</th>
                  <th style="width: 8%; text-align:center;">Recesso/Valor</th>
                  <th style="width: 4%; text-align:center;">Dias B.</th>
                  <th style="width: 4%; text-align:center;">Faltas</th>
                  <th style="width: 4%; text-align:center;">Just.</th>
                  <th style="width: 4%;">Valor Aux.</th>
                  <th style="width: 4%;">Valor Transp.</th>
                  <th style="width: 8%;">Desc. Bolsa</th>
                  <th style="width: 8%;">Desc. Transp.</th>
                  <th style="width: 8%;">Valor Total</th>
                  <th style="width: 5%;">Registrado em</th>
                  <th style="width: 10%; text-align:center;">Ações</th>
                </tr>
              </thead>
              <tbody id="listItensFolha">
                @forelse($itensFolha->chunk(50)->sortByDesc('created_at') as $chunk)
                @foreach($chunk as $item)
                @if($item->is_block === 1)
                <tr id="item-tr{{$item->id}}" class="items-folha danger" data-nome="{{$item->tce->estudante->nmEstudante}}" data-tce="{{$item->tce->id}}" data-item="{{$item->id}}" data-cpf="{{ str_replace(array('.','-'),'',$item->tce->estudante->cdCPF)}}">
                  @else
                  <tr id="item-tr{{$item->id}}" class="items-folha" data-nome="{{$item->tce->estudante->nmEstudante}}" data-tce="{{$item->tce->id}}" data-item="{{$item->id}}" data-cpf="{{ str_replace(array('.','-'),'',$item->tce->estudante->cdCPF)}}">
                    @endif
                    <td class="folha" id="item-first-td{{$item->id}}">
                      {{$item->tce->id}}{{$item->tce->aditivos()->count() > 0 ? '/'.$item->tce->aditivos()->count() : ''}}
                      @if($item->tce->dtCancelamento != NULL)<i class="fa fa-remove text-red" title="Este TCE foi cancelado!"></i>@endif
                      @if($item->tce->centroDeCusto != null)<i class="fa fa-credit-card text-info" id="item-centroCusto{{$item->id}}" title="{{$item->tce->centroDeCusto}}" data-valor="{{$item->tce->centroDeCusto}}"></i> @endif
                    </td>
                    <td>{{$item->tce->estudante->nmEstudante}}<br><small><span class="direct-chat-timestamp pull-left"><b>CPF:</b> {{$item->tce->estudante->cdCPF}}</span></small></td>
                    <td>{{$item->tce->dtInicio->format('d/m/Y')}} até {{$item->tce->aditivos()->count() > 0 ? $item->tce->aditivos()->latest()->first()->dtFim->format('d/m/Y') : $item->tce->dtFim->format('d/m/Y')}}</td>
                    <td id="item-recesso{{$item->id}}" style="text-align:center" data-valor="{{$item->diasRecesso != null ? $item->diasRecesso : 0 }}">
                      {{$item->diasRecesso ? $item->diasRecesso.' dias' : "-"}}
                    </td>
                    <td id="item-dtPagamento{{$item->id}}" class="text-center" data-valor="{{$item->dtPagamento}}" style="display:none;"></td>
                    <td id="item-Block{{$item->id}}" class="text-center" data-valor="{{$item->is_block}}" style="display:none;"></td>
                    <td id="item-base{{$item->id}}" class="text-center" data-valor="{{$item->diasBaseEstagiados}}">{{$item->diasBaseEstagiados}}</td>
                    <td id="item-faltas{{$item->id}}" class="text-center" data-valor="{{$item->faltas}}">{{$item->faltas}}</td>
                    <td id="item-justificadas{{$item->id}}" class="text-center" data-valor="{{$item->justificadas}}">{{$item->justificadas}}</td>
                    <td id="item-vlMensal{{$item->id}}" data-valor="{{$item->vlAuxilioMensalReceber}}" data-mensal="{{$item->tce->vlAuxilioMensal}}">R$ {{number_format($item->vlAuxilioMensalReceber, 2, ',', '.')}}</td>
                    <td id="item-vlTransp{{$item->id}}" data-valor="{{$item->vlAuxilioTransporteReceber}}" data-transp="{{$item->tce->vlAuxilioTransporte}}">R$ {{number_format($item->vlAuxilioTransporteReceber, 2, ',', '.')}}</td>
                    <td id="item-descontoMensal{{$item->id}}" data-valor="{{$item->descontoVlAuxMensal}}">R$ {{$item->descontoVlAuxMensal}}</td>
                    <td id="item-descontoTransp{{$item->id}}" data-valor="{{$item->descontoVlAuxTransporte}}">R$ {{$item->descontoVlAuxTransporte}}</td>
                    <td id="item-vltotal{{$item->id}}" data-valor="{{$item->vlTotal}}">R$ {{number_format($item->vlTotal, 2, ',', '.')}}</td>
                    <td>{{$item->created_at->format('d/m/Y')}}</td>
                    <td class="text-center">
                      <button type="button" role="button" name="btn-edit-item_{{$item->id}}" class="btn btn-warning btn-xs" onclick="editItem('{{$item->id}}', '{{str_replace("'", '', $item->tce->estudante->nmEstudante)}}')" title="Editar"><i class="fa fa-edit"></i></button>
                      <form id="form-delete-item" action="{{ route('financeiro.delete.item', ['id'=>$folha->hash])}}" class="form-horizontal" method="post" style="display: inline-block">
                        {!! csrf_field() !!}
                        <input type="hidden" name="_method" value="DELETE">
                        <input type="hidden" name="id" value="{{$item->id}}">
                        <button type="submit" role="button" class="btn btn-danger btn-xs" onclick="return confirm('Deseja Realmente Excluir o TCE {{$item->tce->id}} dessa folha?');" title="Deletar"><i class="fa fa-trash-o"></i></button>
                      </form>
                    </td>
                  </tr>
                  @endforeach
                  @empty
                  <tr>
                    <td colspan="13" align="center">
                      <span class="badge bg-red">Sem registros no banco de dados!</span>
                    </td>
                  </tr>
                  @endforelse
                  <tr id="nothingItemsFolha" style="display:none;">
                    <td colspan="13" align="center">
                      <h4><label class="label label-info">Nenhum tce foi encontrado</label></h4>
                    </td>
                  </tr>
                </tbody>
                <tbody id="loadFolha" style="display:none;">
                  <tr>
                    <td colspan="13" class="text-center"><div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>
                  </tr>
                </tbody>
                <tfoot>
                  <tr>
                    <td colspan="5"><p class="h5 pull-left"><b>Total:</b> {{$itensFolha->count()}}</p></td>
                    <td colspan="8"><p class="h5" style="float:right;text-align:right;"><b>Valor Total da Folha:</b> R$ {{number_format($itensFolha->sum('vlTotal'), 2, ',', '.')}}</p></td>
                  </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>

        </div>
      </div>
      <!--  Botões abaixo da folha gerada  -->
      <!--Lista de Tces Ativos-->
      <div class="row tables-tces" style="display:none;margin-bottom:20px" id="rowAtivos">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <div class="row">
                <div class="col-md-4">
                  <h3 class="box-title">Tces Ativos que vão entrar na folha</h3>
                </div>
                <div class="col-md-4">
                  <div class="input-group">
                    <input type="search" name="searchAtivos" value="" placeholder="Buscar por nome, cpf, n° tce..." class="form-control">
                    <div class="input-group-addon">
                      <i class="fa fa-search"></i>
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div id="btnActiveSelect">
                    <button type="button" role="button" class="btn btn-primary pull-right" onclick="showSelect()">Add em Lote</button>
                  </div>
                  <div id="btnAddLote" style="display:none;">
                    <div class="pull-right">
                      <button type="button" name="button" class="btn btn-danger" onclick="hideSelect()" style="margin-right:5px;"><i class="fa fa-times"></i></button><button type="button" role="button" class="btn btn-success pull-right" name="btn-addLote">Finalizar</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-condensed table-striped" id="datatable">
                <thead>
                  <tr>
                    <th id="thCheckBoxSelectAll" class="text-center" style="display:none;width:5%;">
                      <label class="">
                        <input type="checkbox" name="allcheck" class="flat-red">
                      </label>
                    </th>
                    <th style="width: 2%">#</th>
                    <th style="width: 3%">TCE</th>
                    <th style="width: 25%;">Estudante</th>
                    <th style="width: 8%;">CPF</th>
                    <th style="width: 10%;">Início</th>
                    <th style="width: 10%;">Fim</th>
                    <th style="width: 5%;" class="text-center">Dias(base)</th>
                    <th style="width: 5%;" class="text-center">Justificadas</th>
                    <th style="width: 5%;" class="text-center">Falta</th>
                    <th style="width: 5%;" class="text-center">Recesso</th>
                    <th style="width: 10%;" class="text-center">Ações</th>
                  </tr>
                </thead>
                <tbody>
                  @forelse($tcesAtivos->chunk(50) as $chunk)
                  @foreach($chunk as $tceAtivo)
                  <tr id="row{{$tceAtivo['id']}}" class="tce-ativos @if($tceAtivo['presenteEmOutrasFolhas'] == false) info @endif" data-nome="{{$tceAtivo['nmEstudante']}}" data-tce="{{$tceAtivo['id']}}" data-cpf="{{ str_replace(array('.','-'),'',$tceAtivo['cdCPF']) }}">
                    <td class="text-center checkbox-ativo" style="display:none;">
                      <div class="form-group">
                        <label class="">
                          <input type="checkbox" id="checkboxAtivo{{$tceAtivo['id']}}" class="flat-red checkbox-tces" data-tce="{{$tceAtivo['id']}}">
                        </label>
                      </div>
                    </td>
                    <td>{{$loop->iteration}}</td>
                    <td class="ativos" id="{{$tceAtivo['id']}}">{{$tceAtivo['id']}} @if($tceAtivo['presenteEmOutrasFolhas'] == false) <i class="fa fa-info-circle" style="color:#17A2B8;" data-toggle="tooltip" data-placement="top" title="Item sem presença em nenhuma folha até o momento"></i> @else <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" title="Item presente em folhas anteriores"></i> @endif</td>
                    <td>{{$tceAtivo['nmEstudante']}}</td>
                    <td>{{$tceAtivo['cdCPF']}}</td>
                    <form class="form-inline add-item-folha" action="{{route('financeiro.item.store', ['id' => $folha->hash])}}" method="POST">
                      <input type="hidden" name="folha_id" value="{{$folha->id}}">
                      <input type="hidden" name="referenciaMes" value="{{$folha->referenciaMes}}" id="referenciaMes{{$tceAtivo['id']}}">
                      <input type="hidden" name="referenciaAno" value="{{$folha->referenciaAno}}" id="referenciaAno{{$tceAtivo['id']}}">
                      <input type="hidden" name="tipo_tce" value="{{$tceAtivo['relatorio_id']}}">
                      <input type="hidden" name="tce_id" value="{{$tceAtivo['id']}}">
                      <input type="hidden" name="vlAuxilioMensal" value="{{str_replace(',','.',$tceAtivo['vlAuxilioMensal'])}}" id="vlAuxilioMensal{{$tceAtivo['id']}}">
                      <input type="hidden" name="vlAuxilioTransporteReceber" value="{{$tceAtivo['vlAuxilioTransporteReceber'] != null ? number_format($tceAtivo['vlAuxilioTransporteReceber'], 2, '.', '') : 0 }}" id="vlAuxilioTransporte{{$tceAtivo['id']}}">
                      <input type="hidden" name="vlAuxilioTransporteDia" value="{{$tceAtivo['vlAuxilioTransporteDia'] != null ? number_format($tceAtivo['vlAuxilioTransporteDia'], 2, '.', '') : 0 }}" id="vlAuxilioTransporteDia{{$tceAtivo['id']}}">
                      {!! csrf_field() !!}
                      <td>{{$tceAtivo['dtInicio']}}</td>
                      <td>{{$tceAtivo['dtFim']}}</td>
                      <td>
                        <div class="form-group">
                          @if(DateTime::createFromFormat('Y', $tceAtivo['dtInicio']) === \Carbon\Carbon::now()->year && abs((DateTime::createFromFormat('m', $tceAtivo['dtInicio']) - \Carbon\Carbon::now()->month)) <= 1)
                          <input class="form-control input-sm" id="diasBaseEstagiados{{$tceAtivo['id']}}" name="diasBaseEstagiados" placeholder="Dias Trabalhados" type="text" value="{{abs((DateTime::createFromFormat('d', $tceAtivo['dtInicio']) - 30))}}">
                          @elseif(DateTime::createFromFormat('Y', $tceAtivo['dtFim']) === \Carbon\Carbon::now()->year && abs((DateTime::createFromFormat('m', $tceAtivo['dtFim']) - \Carbon\Carbon::now()->month)) <= 1)
                          <input class="form-control input-sm" id="diasBaseEstagiados{{$tceAtivo['id']}}" name="diasBaseEstagiados" placeholder="Dias Trabalhados" type="text" value="{{$tceAtivo['diasBaseEstagiados']}}{{abs((DateTime::createFromFormat('d', $tceAtivo['dtFim']) - 30))}}">
                          @else
                          <input class="form-control input-sm" id="diasBaseEstagiados{{$tceAtivo['id']}}" name="diasBaseEstagiados" placeholder="Dias Trabalhados" type="tel" value="30">
                          @endif
                        </div>
                      </td>
                      <td>
                        <div class="form-group">
                          <input type="tel" class="form-control input-sm" name="justificadas" id="justificadas{{$tceAtivo['id']}}" placeholder="0" value="0">
                        </div>
                      </td>
                      <td>
                        <div class="form-group">
                          <input type="tel" class="form-control input-sm" name="faltas" id="faltas{{$tceAtivo['id']}}" placeholder="0" value="0">
                        </div>
                      </td>
                      <td>
                        <div class="form-group">
                          <input class="form-control input-sm" id="diasRecesso{{$tceAtivo['id']}}" name="diasRecesso" placeholder="Dias Recesso" type="tel" value="0">
                        </div>
                      </td>
                      <td class="text-center">
                        <button type="submit" class="btn btn-success btn-sm" onclick="this.style.display = 'none'; document.getElementById('btn-add-refresh_{{$loop->iteration}}').style.display = 'block';"><i class="fa fa-plus"></i> Add</button>
                        <div class="btn" style="display:none;" id="btn-add-refresh_{{$loop->iteration}}"><i class="fa fa-refresh"></i> Adicionando</div>
                      </td>
                    </form>
                  </tr>
                  @endforeach
                  @empty
                  <tr>
                    <td colspan="11" align="center">
                      <h4><label class="label label-danger">Não há TCES disponivel!</label></h4>
                    </td>
                  </tr>
                  @endforelse
                  <tr id="nothingAtivos" style="display:none;">
                    <td colspan="11" align="center">
                      <h4><label class="label label-info">Nenhum tce foi encontrado</label></h4>
                    </td>
                  </tr>
                </tbody>
                <tbody id="loadAtivos" style="display:none;">
                  <tr>
                    <td colspan="11" class="text-center"><div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>
                  </tr>
                </tbody>
                <tfoot>
                  <tr>
                    <td colspan="8"><p class="h5 pull-left"><b>Total:</b> {{$tcesAtivos->count()}}</p></td>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
        <!--FIM Lista de Tces Ativos-->
        <!--Lista de Tces Cancelads-->
        <div class="row tables-tces" style="display:none;margin-bottom:20px;" id="rowCancelados">
          <div class="col-md-12">
            <div class="box box-danger">
              <div class="box-header with-border">
                <div class="row">
                  <div class="col-md-4">
                    <h3 class="box-title">Tces Cancelados que podem entrar na folha</h3>
                  </div>
                  <div class="col-md-4">
                    <div class="input-group">
                      <input type="search" name="searchCancelados" value="" placeholder="Buscar por nome, cpf, n° tce..." class="form-control">
                      <div class="input-group-addon">
                        <i class="fa fa-search"></i>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                  </div>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <table class="table table-condensed table-striped">
                  <thead>
                    <tr>
                      <th style="width: 5%;">TCE</th>
                      <th style="width:45%;">Estudante</th>
                      <th style="width:25%;">Data Cancelamento</th>
                      <th style="width: 5%;">Dias(base)</th>
                      <th style="width: 5%;">Justificadas</th>
                      <th style="width: 5%;">Faltas</th>
                      <th style="width: 5%;">Recesso</th>
                      <th style="width:35%;" class="text-center">Ações</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse($tcesCancelados->chunk(50) as $chunk)
                    @foreach($chunk as $tceCancelado)
                    <tr id="row{{$tceCancelado->id}}" class="tce-cancelados" data-nome="{{$tceCancelado->estudante->nmEstudante}}" data-tce="{{$tceCancelado->id}}" data-cpf="{{str_replace(array('.','-'), "", $tceCancelado->estudante->cdCPF)}}">
                      <td class="inativos" id="{{$tceCancelado->id}}">{{$tceCancelado->id}}</td>
                      <td>{{$tceCancelado->estudante->nmEstudante}} <br><small><span class="direct-chat-timestamp pull-left"><b>CPF:</b> {{$tceCancelado->estudante->cdCPF}}</span></small></td>
                      <td>{{$tceCancelado->dtCancelamento->format('d/m/Y')}}</td>
                      <form  class="form-inline" action="{{route('financeiro.item.store', ['concedente' => $folha->concedente_id, 'folha' => $folha->id])}}" method="POST">
                        <input type="hidden" name="folha_id" value="{{$folha->id}}">
                        <input type="hidden" name="referenciaMes" value="{{$folha->referenciaMes}}">
                        <input type="hidden" name="referenciaAno" value="{{$folha->referenciaAno}}">
                        <input type="hidden" name="tipo_tce" value="{{$tceCancelado->relatorio_id}}">
                        <input type="hidden" name="tce_id" value="{{$tceCancelado->id}}">
                        @if($tceCancelado->aditivos->count() > 0)
                        @if($tceCancelado->aditivos()->latest()->first()->vlAuxilioMensal != Null)
                        <input type="hidden"  name="vlAuxilioMensal" value="{{number_format($tceCancelado->aditivos()->latest()->first()->vlAuxilioMensal, 2, '.', '')}}">
                        @else
                        <input type="hidden"  name="vlAuxilioMensal" value="{{number_format($tceCancelado->vlAuxilioMensal, 2, '.', '')}}">
                        @endif
                        @else
                        <input type="hidden"  name="vlAuxilioMensal" value="{{number_format($tceCancelado->vlAuxilioMensal, 2, '.', '')}}">
                        @endif
                        <input type="hidden" name="vlAuxilioTransporteReceber" value="{{number_format($tceCancelado->vlAuxilioTransporteReceber, 2, '.', '')}}">
                        <input type="hidden" name="vlAuxilioTransporteDia" value="{{number_format($tceCancelado->concedente->vlTransporteDia, 2, '.', '')}}">
                        {!! csrf_field() !!}
                        <td>
                          <div class="form-group">
                            @if($tceCancelado->dtCancelamento->format('Y') === \Carbon\Carbon::now()->year && abs(($tceCancelado->dtCancelamento->format('m') - \Carbon\Carbon::now()->month)) <= 1)
                            <input class="form-control input-sm" id="diasBaseEstagiados" name="diasBaseEstagiados" placeholder="Dias Trabalhados" type="text" value="{{abs(($tceCancelado->dtCancelamento->format('d') - 30))}}">
                            @else
                            <input class="form-control input-sm" id="diasBaseEstagiados" name="diasBaseEstagiados" placeholder="Dias Trabalhados" type="text" value="0">
                            @endif
                          </div>
                        </td>
                        <td>
                          <div class="form-group">
                            <input type="tel" class="form-control input-sm" name="justificadas" id="justificadas{{$tceCancelado->id}}" placeholder="0" value="0">
                          </div>
                        </td>
                        <td>
                          <div class="form-group">
                            <input type="tel" class="form-control input-sm" name="faltas" id="faltas" placeholder="0" value="0">
                          </div>
                        </td>
                        <td>
                          <div class="form-group">
                            <input class="form-control input-sm" id="diasRecesso" name="diasRecesso" placeholder="Dias Recesso" type="text" value="0">
                          </div>
                        </td>
                        <td style="width:35%;" class="text-center">
                          <button type="submit" class="btn btn-success btn-sm" onclick="this.style.display = 'none'; document.getElementById('btn-add-refresh_{{$loop->iteration}}').style.display = 'block';"><i class="fa fa-plus"></i> Add</button>
                          <div class="btn" style="display:none;" id="btn-add-refresh_{{$loop->iteration}}"><i class="fa fa-refresh"></i> Adicionando</div>
                        </td>
                      </form>
                    </tr>
                    @endforeach
                    @empty
                    <tr>
                      <td colspan="11" align="center">
                        <h4><label class="label label-danger">Não há TCES CANCELADOS disponivel!</label></h4>
                      </td>
                    </tr>
                    @endforelse
                  </tbody>
                  <tfoot>
                    <tr>
                      <td colspan="11"><p class="h5 pull-left"><b>Total:</b> {{$tcesCancelados->count()}}</p></td>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- Modal -->
      <div class="modal fade" id="editItemModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">Editar Item</h4>
            </div>
            <form action="{{ url('home/folha/editar/item') }}" name="form-edit-item" method="post">
              <div class="modal-body">
                <input type="hidden" id="editItemId" name="id" value="">
                <div class="row form-group">
                  <div class="col-md-6">
                    <h4 class="text-center info">Tce: <br><small id="tceHtml"></small></h4>
                  </div>
                  <div class="col-md-6">
                    <div class="checkbox pull-right">
                      <label>
                        <input type="checkbox" name="block" id="editBlock">
                        Pagamento bloqueado
                      </label>
                    </div>
                  </div>
                </div>
                <div class="row form-group">
                  <div class="col-md-3">
                    <label class="control-label" for="">Dias Base</label>
                    <input type="tel" id="editDiasBaseEstagiados" name="diasBaseEstagiados" value="30" min="0" class="form-control" onchange="checkBase(this.value)">
                  </div>
                  <div class="col-md-3">
                    <label class="control-label" for="">Dias Recesso</label>
                    <input type="tel" id="editDiasRecesso" name="diasRecesso" value="0" min="0" class="form-control" onchange="checkAusencias(this.value, document.getElementById('editFaltas').value, document.getElementById('editJustificadas').value)">
                  </div>
                  <div class="col-md-3">
                    <label class="control-label" for="">Faltas</label>
                    <input type="tel" id="editFaltas" name="faltas" value="0" class="form-control" min="0" onchange="checkAusencias(document.getElementById('editDiasRecesso').value, this.value, document.getElementById('editJustificadas').value)">
                  </div>
                  <div class="col-md-3">
                    <label class="control-label" for="">Justificadas</label>
                    <input type="tel" id="editJustificadas" name="justificadas" value="0" min="0" class="form-control" onchange="checkAusencias(document.getElementById('editDiasRecesso').value, document.getElementById('editFaltas').value, this.value)">
                  </div>
                </div>
                <div class="row form-group">
                  <div class="col-md-3">
                    <label class="control-label" for="">Valor Bolsa</label>
                    <div class="input-group">
                      <span class="input-group-addon">
                        <strong>R$</strong>
                      </span>
                      <input type="tel" id="editVlMensal" name="vlAuxilioMensal" value="" min="0" class="form-control money">
                    </div>
                    <input type="hidden" id="editVlBolsa">
                  </div>
                  <div class="col-md-3">
                    <label class="control-label" for="">Valor Transporte</label>
                    <div class="input-group">
                      <span class="input-group-addon">
                        <strong>R$</strong>
                      </span>
                      <input type="tel" id="editVlTransporte" name="vlAuxilioTransporte" value="" min="0" class="form-control money">
                    </div>
                    <input type="hidden" id="editVlBolsaTransp">
                  </div>
                  <div class="col-md-3">
                    <label class="control-label" for="">Desc. Bolsa</label>
                    <div class="input-group">
                      <span class="input-group-addon">
                        <strong>R$</strong>
                      </span>
                      <input type="tel" id="editDescVlMensal" name="descontoAuxilioMensal" value="" min="0" class="form-control money">
                    </div>
                  </div>
                  <div class="col-md-3">
                    <label class="control-label" for="">Desc. Transporte</label>
                    <div class="input-group">
                      <span class="input-group-addon">
                        <strong>R$</strong>
                      </span>
                      <input type="tel" id="editDescVlTransporte" name="descontoAuxilioTransporte" value="" min="0" class="form-control money">
                    </div>
                  </div>
                </div>
                <div class="row form-group">
                  <div class="col-md-4">
                    <label class="control-label" for="">Data Pagamento</label>
                    <input type="tel" id="editDtPagamento" name="dtPagamento" value="" class="form-control dtPagamento" placeholder="dd-mm-aaaa">
                  </div>
                  <div class="col-md-4">
                    <label class="control-label" for="">Centro de Custo</label>
                    <input type="tel" id="editCentroDeCusto" name="centroDeCusto" value="" class="form-control">
                  </div>
                  <div class="col-md-4">
                    <label for="">Valor total</label>
                    <div class="input-group">
                      <div class="input-group-btn">
                        <button type="button" name="button" class="btn btn-primary" onclick="updateTotalItem()" data-toggle="tooltip" data-placement="top" title="Clique para calcular o valor total"><i class="fa fa-calculator"></i></button>
                      </div>
                      <input type="tel" id="editVlTotal" name="vlTotal" class="form-control money" value="">
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                <button type="submit" class="btn btn-primary">Salvar</button>
              </div>
            </form>
          </div>
        </div>
      </div>


      <div class="modal" tabindex="-1" role="dialog" id="log">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Log</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-12">
                  <div class="alert alert-danger">
                    <h4 class="text-center">Erros</h4>
                    <div class="table-responsive">
                      <table class="table">
                        <thead>
                          <tr>
                            <th style="width:10%;">TCE</th>
                            <th style="width:90%;">Erro</th>
                          </tr>
                        </thead>
                        <tbody id="listErrors">

                        </tbody>
                        <tfoot>
                          <tr>
                            <th>Total</th>
                            <th id="totalErrors" class="text-right">0</th>
                          </tr>
                        </tfoot>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="alert alert-warning">
                    <h4 class="text-center">Inválidos</h4>
                    <div class="table-responsive">
                      <table class="table">
                        <thead>
                          <tr>
                            <th style="width:10%;">TCE</th>
                            <th style="width:90%;">Erro</th>
                          </tr>
                        </thead>
                        <tbody id="listInvalid">

                        </tbody>
                        <tfoot>
                          <tr>
                            <th>Total</th>
                            <th id="totalInvalid" class="text-right">0</th>
                          </tr>
                        </tfoot>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>
          </div>
        </div>
      </div>
      <!--FIM Lista de Tces Ativos-->

      @section('post-script')
      <script type="text/javascript" src="{{ asset('vendor/adminlte/plugins/iCheck/icheck.min.js') }}"></script>
      <script type="text/javascript" src="{{ asset('assets/vendor/toast/toastr.js')}}"></script>
      <script type="text/javascript">

      $('.money').mask("###0.00", {reverse: true});
      $('.dtPagamento').mask("00/00/0000");
      toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-bottom-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "5000",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
      }

      function showSelect(){
        if($(".checkbox-ativo").length > 0 ){
          $('button[name=log-ajax]').hide();
          $("#thCheckBoxSelectAll").show();
          $(".checkbox-ativo").show();
          $("#btnActiveSelect").hide();
          $("#btnAddLote").show();
        }else{
          toastr.info('Nenhum tce disponível para seleção');
        }
      }

      $("input[name=allcheck]").click(function(){
        if(!$(this).is(':checked')){
          $(this).prop('checked',false);
          $(".checkbox-tces").prop('checked',false);
        }else{
          $(this).prop('checked',true);
          $(".checkbox-tces").prop('checked',true);
        }
      });

      // $(".add-item-folha").submit(function(){
      //   $(this 'button').attr('disabled','disabled');
      // });

      $("button[name=btn-addLote]").click(function(){
        $(this).attr('disabled','disabled');
        var itens = $(".checkbox-tces:checked");
        var itensAdd = [];
        var item = {};
        $(".tce-ativos").hide();
        if(itens.length > 0){
          for(var i = 0; i < itens.length; i++){
            if(itens[i].checked == true){
              item = {
                'id' : itens[i].dataset.tce,
                'referenciaMes' : $("#referenciaMes"+itens[i].dataset.tce).val(),
                'referenciaAno' : $("#referenciaAno"+itens[i].dataset.tce).val(),
                'vlAuxilioMensal' : $("#vlAuxilioMensal"+itens[i].dataset.tce).val(),
                'vlAuxilioMensalReceber' : $("#vlAuxilioMensal"+itens[i].dataset.tce).val(),
                'vlAuxilioTransporteReceber' : $("#vlAuxilioTransporte"+itens[i].dataset.tce).val(),
                'vlAuxilioTransporteDia' : $("#vlAuxilioTransporteDia"+itens[i].dataset.tce).val(),
                'diasRecesso' : $("#diasRecesso"+itens[i].dataset.tce).val(),
                'diasBaseEstagiados' : $("#diasBaseEstagiados"+itens[i].dataset.tce).val(),
                'faltas' : $("#faltas"+itens[i].dataset.tce).val(),
                'justificadas' : $("#justificadas"+itens[i].dataset.tce).val(),
                'folha_id':'{{$folha->id}}'
              };
              itensAdd.push(item);
            }
          }
          showLoadAtivos();
          $.post('{{url("home/concedente/financeiro/folha/".$folha->hash."/lote/store")}}', { "_token": "{{ csrf_token() }}", 'items': itensAdd, 'folha': '{{$folha->id}}' }, function(response){
            if(response != null){
              if(response.result == true){
                if(response.success.length > 0){
                  for(var x = 0; x < response.success.length;x++){
                    $('#row'+response.success[x]).remove();
                  }
                  toastr.success('Lote adicionado com sucesso');
                  if($(".tce-ativos").length == 0){
                    $("#nothingAtivos").show();
                  }
                  // $("#ativos-count").data("count", $(this).data('count') - response.success.length);
                  // $("#ativos-count").html($(this).data('count'));
                  // $("#itens-count").data("count", $(this).data('count') + response.success.length);
                  // $("#itens-count").html($(this).data('count'));
                }
                $('#listErrors').empty();
                if(response.errors.length > 0){
                  console.log(response.errors);
                  for(var i = 0; i < response.errors.length; i++){
                    $('#listErrors').append('<tr><td>'+response.errors[i].tce_id+'</td><td><p>'+response.errors[i].error+'<br>'+response.errors[i].e+'</p></td></tr>');
                  }
                  $("#totalErrors").html(response.errors.length);
                  $('button[name=log-ajax]').show();
                }else{
                  $('#listErrors').appendTo('<tr><td colspan="2" class="text-center">Nenhum tce sofreu erro</td></tr>');
                }
                $('#listInvalid').empty();
                if(response.invalid.length > 0){
                  for(var i = 0; i < response.invalid.length; i++){
                    $('#listInvalid').append('<tr><td>'+response.invalid[i].tce_id+'</td><td>'+response.invalid[i].errorInfo[2]+'</td></tr>');
                  }
                  $("#totalInvalid").html(response.invalid.length);
                  $('button[name=log-ajax]').show();
                }else{
                  $('#listInvalid').append('<tr><td colspan="2" class="text-center">Nenhum tce inválido</td></tr>');
                }
                $("#listItensFolha").appendTo('<tr><td colspan="11" class="text-center"><button type="button" role="button" class="btn btn-warning" id="btn-reload" onclick="location.reload()"><i class="fa fa-refresh"></i> Atualizar</button></td></tr>');
              }else{
                toastr.warning('Não foi possível adicionar o lote feito');
              }
            }else{
              toastr.warning('Não foi possível adicionar o lote feito');
            }
          });
          hideSelect();
        }else{
          toastr.info('Selecione pelo menos 1 tce para adicionar em lote');
        }
        hideLoadAtivos();
        $(".tce-ativos").show();
        $(this).removeAttr('disabled');
      });

      function showLoadAtivos(){
        $("#loadAtivos").show();
      }

      function hideLoadAtivos(){
        $("#loadAtivos").hide();
      }

      function showRow(rows){
        if(rows != "all"){
          $(".tables-tces").hide();
          $('#row'+rows).show();
        }else{
          $('.tables-tces').show();
        }
      }

      function hideSelect(){
        $("#thCheckBoxSelectAll").hide();
        $(".checkbox-ativo").hide();
        $(".checkbox-tces").prop('checked',false);
        if($("input[name=allcheck]").prop('checked') == true){
          $("input[name=allcheck]").prop('checked',false);
        }
        $("#btnAddLote").hide();
        $("#btnActiveSelect").show();
      }

      $('input[name=searchAtivos]').keyup(function(){
        var tces = $(".tce-ativos");
        var nothing = $('#nothingAtivos');
        var load = $("#loadAtivos");
        var nome = $(this).val();
        search(nome,tces,nothing,load);
      });

      $('input[name=searchFolha]').keyup(function(){
        var tces = $(".items-folha");
        var nothing = $('#nothingItemsFolha');
        var load = $("#loadFolha");
        var nome = $(this).val();
        search(nome,tces,nothing,load);
      });

      $('input[name=searchCancelados]').keyup(function(){
        var tces = $(".tce-cancelados");
        var nothing = $('#nothingCancelados');
        var load = $("#loadCancelados");
        var nome = $(this).val();
        search(nome,tces,nothing,load);
      });

      function search(nome,tces,nothing,load){
        nothing.hide();
        var count = 0;
        tces.show();
        if (nome != "") {
          for (var i = 0; i < tces.length; i++) {
            if (tces[i].dataset.nome.toLowerCase().search(nome.toLowerCase()) && tces[i].dataset.tce.toLowerCase().search(nome.toLowerCase()) && tces[i].dataset.cpf.search(nome)) {
              tces[i].style.display = "none";
            }else{
              count++;
            }
          }
          if(count == 0){
            nothing.show();
          }
        }
      }

      function downItensFolha(){
        var items = $(".items-folha");
        if(items.length > 0){
          var r = confirm('Deseja remover todos os itens da folha?');
          if(r == true){
            $("#listItensFolha").hide();
            $("#loadFolha").show();
            var send = [];
            var item = {};
            for(var i = 0; i < items.length; i++){
              item = {
                'id' : items[i].dataset.item
              };
              send.push(item);
            }
            $.post("{{ route('financeiro.lote.items.remove', ['id'=> $folha->hash])}}", { "_token": "{{ csrf_token() }}", 'items': send, 'folha':'{{$folha->id}}' }, function(response){
              if(response != null){
                if(response.result == true){
                  toastr.success('Remoção realizada com sucesso');
                  location.reload();
                }else{
                  toastr.warning('Não foi realizar a operação');
                }
              }else{
                toastr.warning('Não foi realizar a operação');
              }
            });
            $("#loadFolha").hide();
            $("#listItensFolha").show();
          }
        }else{
          toastr.info('A folha não possuí nenhum item que possa ser excluído');
        }
      }

      function editItem(id, nmEstudante){
        $("#editItemId").val(id);
        $("#tceHtml").html(nmEstudante);
        $("#editDiasRecesso").val($("#item-recesso"+id).data("valor"));
        $("#editDiasBaseEstagiados").val($("#item-base"+id).data("valor"));
        $("#editBlock").val($("#item-Block"+id).data("valor"));
        $("#editBlock").prop('checked', false);
        if($("#item-Block"+id).data("valor") == 1){
          $("#editBlock").prop('checked', true);
        }
        $("#editJustificadas").val($("#item-justificadas"+id).data("valor"));
        $("#editFaltas").val($("#item-faltas"+id).data("valor"));
        $("#editDtPagamento").val($("#item-dtPagamento"+id).data("valor"));
        $("#editVlMensal").val($("#item-vlMensal"+id).data("valor"));
        console.log($("#item-vlMensal"+id).data("mensal"));
        console.log($("#item-vlTransp"+id).data("transp"));
        $("#editVlBolsa").val($("#item-vlMensal"+id).data("mensal"));
        $("#editVlTransporte").val($("#item-vlTransp"+id).data("valor"));
        $("#editVlBolsaTransp").val($("#item-vlTransp"+id).data("transp"));
        $("#editDescVlMensal").val($("#item-descontoMensal"+id).data("valor"));
        $("#editCentroDeCusto").val($("#item-centroCusto"+id).data("valor"));
        $("#editDescVlTransporte").val($("#item-descontoTransp"+id).data("valor"));
        $("#editVlTotal").val($("#item-vltotal"+id).data("valor"));
        $("#editItemModal").modal('show');
      }

      function checkBase(base){
        var transp = $("#editVlBolsaTransp").val();
        var valor = $("#editVlBolsa").val();
        $("#editVlMensal").val((parseFloat(valor/30) * base).toFixed(2));
      }

      function checkAusencias(diasRecesso, faltas, justificadas){
        var bolsa = parseFloat($("#editVlBolsa").val());
        var transp = parseFloat($("#editVlBolsaTransp").val());
        var descBolsa = faltas * parseFloat((bolsa/30).toFixed(2));
        if(diasRecesso == null){
          diasRecesso = 0;
        }
        if(faltas == null){
          faltas = 0;
        }
        if(justificadas == null){
          justificadas = 0;
        }
        var descTrans = (parseInt(faltas) + parseInt(justificadas) + parseInt(diasRecesso)) * parseFloat(transp/30).toFixed(2);
        $("#editDescVlTransporte").val(descTrans.toFixed(2));
        $("#editDescVlMensal").val(descBolsa.toFixed(2));
      }

      function getItemForm(){
        var block = 0;
        if($("form[name=form-edit-item]").find('input[name=block]').prop('checked') === true){
          block = 1;
        }
        return {
          'id': $("form[name=form-edit-item]").find('input[name=id]').val(),
          'nmEstudante': $("#tceHtml").html(),
          'diasRecesso': $("form[name=form-edit-item]").find('input[name=diasRecesso]').val(),
          'diasBaseEstagiados': $("form[name=form-edit-item]").find('input[name=diasBaseEstagiados]').val(),
          'is_block': block,
          'faltas': $("form[name=form-edit-item]").find('input[name=faltas]').val(),
          'justificadas': $("form[name=form-edit-item]").find('input[name=justificadas]').val(),
          'vlAuxilioTransporte': parseFloat($("form[name=form-edit-item]").find('input[name=vlAuxilioTransporte]').val()).toFixed(2),
          'vlAuxilioMensal': parseFloat($("form[name=form-edit-item]").find('input[name=vlAuxilioMensal]').val()).toFixed(2),
          'descontoVlAuxMensal': parseFloat($("form[name=form-edit-item]").find('input[name=descontoAuxilioMensal]').val()).toFixed(2),
          'descontoVlAuxTransporte': parseFloat($("form[name=form-edit-item]").find('input[name=descontoAuxilioTransporte]').val()).toFixed(2),
          'vlTotal': parseFloat($("form[name=form-edit-item]").find('input[name=vlTotal]').val()).toFixed(2),
          'dtPagamento': $("form[name=form-edit-item]").find('input[name=dtPagamento]').val(),
          'centroDeCusto': $("form[name=form-edit-item]").find('input[name=centroDeCusto]').val()
        };
      }

      function updateItemTable(obj){
        var item = obj.item;
        if(item.is_block == 1){
          $("#item-tr"+item.id).addClass("danger");
        }else{
          $("#item-tr"+item.id).removeClass("danger");
        }
        if(item.centroDeCusto != null){
          if($('#item-centroCusto'+item.id).length === 0){
            $("#item-first-td"+item.id).append('<i class="fa fa-credit-card text-info" id="item-centroCusto'+item.id+'" title="'+item.centroDeCusto+'" data-valor="'+item.centroDeCusto+'"></i>');
          }else{
            $('#item-centroCusto'+item.id).data('valor', item.centroDeCusto);
            $('#item-centroCusto'+item.id).prop('title', item.centroDeCusto);
          }
        }else{
          $('#item-centroCusto'+item.id).remove();
        }
        $("#item-recesso"+item.id).html(item.diasRecesso+" dias");
        $("#item-base"+item.id).html(item.diasBaseEstagiados);
        $("#item-faltas"+item.id).html(item.faltas);
        $("#item-block"+item.id).html(item.is_block);
        $("#item-justificadas"+item.id).html(item.justificadas);
        $("#item-vlMensal"+item.id).html("R$ "+item.vlAuxilioMensalReceber);
        $("#item-vlTransp"+item.id).html("R$ "+item.vlAuxilioTransporteReceber);
        $("#item-descontoTransp"+item.id).html("R$ "+item.descontoVlAuxTransporte);
        $("#item-descontoMensal"+item.id).html("R$ "+item.descontoVlAuxMensal);
        $("#item-vltotal"+item.id).html("R$ "+item.vlTotal);
        //
        $("#item-recesso"+item.id).data("valor",item.diasRecesso);
        $("#item-base"+item.id).data("valor",item.diasBaseEstagiados);
        $("#item-faltas"+item.id).data("valor",item.faltas);
        $("#item-justificadas"+item.id).data("valor",item.justificadas);
        $("#item-vlMensal"+item.id).data("valor",item.vlAuxilioMensalReceber);
        $("#item-vlTransp"+item.id).data("valor",item.vlAuxilioTransporteReceber);
        $("#item-descontoTransp"+item.id).data("valor",item.descontoVlAuxTransporte);
        $("#item-descontoMensal"+item.id).data("valor",item.descontoVlAuxMensal);
        $("#item-vltotal"+item.id).data("valor",item.vlTotal);
      }

      function updateTotalItem(){
        var vlAuxilioTransporte = parseFloat($("form[name=form-edit-item]").find('input[name=vlAuxilioTransporte]').val());
        var vlAuxilioMensal = parseFloat($("form[name=form-edit-item]").find('input[name=vlAuxilioMensal]').val());
        var descontoVlAuxMensal = parseFloat($("form[name=form-edit-item]").find('input[name=descontoAuxilioMensal]').val());
        var descontoVlAuxTransporte = parseFloat($("form[name=form-edit-item]").find('input[name=descontoAuxilioTransporte]').val());
        $("form[name=form-edit-item]").find('input[name=vlTotal]').val(parseFloat(parseFloat(vlAuxilioMensal + vlAuxilioTransporte) - parseFloat(descontoVlAuxTransporte + descontoVlAuxMensal)).toFixed(2));
      }

      $("form[name=form-edit-item]").submit(function(){
        updateTotalItem();
        var item = getItemForm();
        var r = true;
        if(item.vlTotal === 0 || item.vlTotal === null || isNaN(item.vlTotal)){
          r = confirm('O valor total está 0, deseja confirmar?');
          if(r = true){
            item.vlTotal = 0;
          }
        }
        if(r = true){
          $.post($("form[name=form-edit-item]").prop('action'), { 'item': item, '_token': '{{ csrf_token() }}' }, function(response){
            if(response != null){
              if(response.msg.status == "success"){
                updateItemTable({'item': response.item});
                toastr.success('Item atualizado com sucesso');
                $(this).trigger("reset");
                $("#editItemModal").modal('hide');
              }else{
                toastr.error(response.msg.msg);
              }
            }else{
              toastr.error('Ocorreu uma falha ao tentar atualizar o item, tente novamente mais tarde');
            }
          });
        }
        return false;
      });

      function includeItens(btn){
        btn.classList.remove('btn-default');
        btn.classList.add('btn-primary');
        if(btn.getAttribute('name') == "btn-include"){
          $('input[name=itesnDesc]').find('input[name=itesnDesc]').val('sim');
          $('button[name=btn-not-include]').removeClass('btn-primary').addClass('btn-default');
        }else{
          $('#form-relatorio-post').find('input[name=itesnDesc]').val('nao');
          $('button[name=btn-include]').removeClass('btn-primary').addClass('btn-default');
        }
      }

      function getPagamento(id, modelo){
        $("#form-post-relatorio").find("input[name=id]").val(id);
        $("#form-post-relatorio").find("input[name=modelo]").val(modelo);
        $("#form-post-relatorio").find("input[name=submit]").click();
      }

      $("button[name=btn-muda-parametro]").click(function(){
        if($(this).val() == '>='){
          $('input[name=parametro]').val('=');
          $(this).val('=');
          $(this).html('=');
        }else if($(this).val() == '='){
          $('input[name=parametro]').val('<');
          $(this).val('<');
          $(this).html('<');
        }else if($(this).val() == '<'){
          $('input[name=parametro]').val('>');
          $(this).val('>=');
          $(this).html('>=');
        }
      });

      function compararFolha(btn){
        btn.classList.remove('btn-default');
        btn.classList.add('btn-primary');
        if(btn.getAttribute('name') == "btn-comparar-sim"){
          $('#form-relatorio-post').find('input[name=comparacao]').val('sim');
          $('button[name=btn-comparar-nao]').removeClass('btn-primary').addClass('btn-default');
        }else{
          $('#form-relatorio-post').find('input[name=comparacao]').val('nao');
          $('button[name=btn-comparar-sim]').removeClass('btn-primary').addClass('btn-default');
        }
      }

      </script>
      @endsection
      @stop
