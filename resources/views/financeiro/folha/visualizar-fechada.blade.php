@extends('adminlte::page')

@if(!empty($titulo))
@section('title_postfix', ' - '.$titulo)
@endif
@section('css')
<link rel="stylesheet" href="{{ asset('vendor/adminlte/plugins/iCheck/square/blue.css')}}">
<link href="{{ asset('assets/vendor/toast/toastr.css')}}" rel="stylesheet">
<style media="screen">

.lds-roller {
  display: inline-block;
  position: relative;
  width: 64px;
  height: 64px;
}
.lds-roller div {
  animation: lds-roller 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
  transform-origin: 32px 32px;
}
.lds-roller div:after {
  content: " ";
  display: block;
  position: absolute;
  width: 4px;
  height: 4px;
  border-radius: 50%;
  background: #0e502d;
  margin: -3px 0 0 -3px;
}
.lds-roller div:nth-child(1) {
  animation-delay: -0.036s;
}
.lds-roller div:nth-child(1):after {
  top: 50px;
  left: 50px;
}
.lds-roller div:nth-child(2) {
  animation-delay: -0.072s;
}
.lds-roller div:nth-child(2):after {
  top: 54px;
  left: 45px;
}
.lds-roller div:nth-child(3) {
  animation-delay: -0.108s;
}
.lds-roller div:nth-child(3):after {
  top: 57px;
  left: 39px;
}
.lds-roller div:nth-child(4) {
  animation-delay: -0.144s;
}
.lds-roller div:nth-child(4):after {
  top: 58px;
  left: 32px;
}
.lds-roller div:nth-child(5) {
  animation-delay: -0.18s;
}
.lds-roller div:nth-child(5):after {
  top: 57px;
  left: 25px;
}
.lds-roller div:nth-child(6) {
  animation-delay: -0.216s;
}
.lds-roller div:nth-child(6):after {
  top: 54px;
  left: 19px;
}
.lds-roller div:nth-child(7) {
  animation-delay: -0.252s;
}
.lds-roller div:nth-child(7):after {
  top: 50px;
  left: 14px;
}
.lds-roller div:nth-child(8) {
  animation-delay: -0.288s;
}
.lds-roller div:nth-child(8):after {
  top: 45px;
  left: 10px;
}
@keyframes lds-roller {
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
}

.justify-content-center{
  justify-content: center!important;
}

</style>
@endsection
@section('content_header')
@if($folha->secConcedente_id != null)
<h1>Financeiro Secretaria</h1>
<ol class="breadcrumb">
  <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
  <li><a href="{{route('concedente.index')}}"><i class="fa fa-industry"></i> Concedentes</a></li>
  <li><a href="{{route('concedente.show', ['concedente' => $folha->concedente->id])}}"><i class="fa fa-file"></i> {{$folha->concedente->nmRazaoSocial}}</a></li>
  <li><a href="{{route('concedente.secretaria.show', ['idConcedente' => $folha->concedente->id, 'id' => $folha->secConcedente_id])}}"><i class="fa fa-file"></i> {{$folha->secConcedente->nmSecretaria}}</a></li>
  <li><a href="{{route('concedente.secretaria.financeiro', ['idConcedente' => $folha->concedente->id, 'id' => $folha->secConcedente_id])}}"><i class="fa fa-calculator"></i> Folha de Pagamento</a></li>
  <li class="active"><i class="fa fa-calculator"></i> {{$folha->referenciaMes}}/{{$folha->referenciaAno}}</li>
</ol>
@else
<h1>Financeiro Concedente</h1>
<ol class="breadcrumb">
  <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
  <li><a href="{{route('concedente.index')}}"><i class="fa fa-industry"></i> Concedentes</a></li>
  <li><a href="{{route('concedente.show', ['concedente' => $folha->concedente->id])}}"><i class="fa fa-file"></i> {{$folha->concedente->nmRazaoSocial}}</a></li>
  <li><a href="{{route('concedente.financeiro', ['concedente' => $folha->concedente->id])}}"><i class="fa fa-calculator"></i> Folha de Pagamento</a></li>
  <li class="active"><i class="fa fa-calculator"></i> {{$folha->referenciaMes}}/{{$folha->referenciaAno}}</li>
</ol>
@endif
@stop

@section('content')


@if(session('success'))
<div class="alert alert-success alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
  <h4>
    <i class="icon fa fa-check"></i> Alerta!</h4>
    {{session('success')}}
  </div>
  @endif

  @if(session('error'))
  <div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4>
      <i class="icon fa fa-ban"></i> Alerta!</h4>
      {{session('error')}}
    </div>
    @endif
    <div class="row justify-content-center" style="margin-bottom:20px;">
      <div class="col-md-3">
        <div class="info-box bg-green">
          <span class="info-box-icon"><i class="fa fa-list-ol"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">Tce's na Folha</span>
            <span class="info-box-number" id="itens-count" data-count="{{$itensFolha->count()}}">{{$itensFolha->count()}}</span>
          </div>
          <!-- /.info-box-content -->
        </div>
      </div>
      <div class="col-md-9">

        <form action="{{route('financeiro.relatorio')}}" method="post" style="display:none;" id="form-post-relatorio">
          {!! csrf_field() !!}
          <input type="hidden" name="id" value="">
          <input type="hidden" name="modelo" value="">
          <input type="submit" name="submit" value="">
        </form>
        <div class="input-group-btn">
          <button type="button" role="button" class="btn bg-gray btn-flat btn-lg pull-right" data-toggle="modal"  data-target="#modelModelo"><i class="fa fa-file-pdf-o"></i> Relatório</button>
        </div>

        <!-- Modal de Relatório -->
        <div class="modal" tabindex="-1" role="dialog" id="modelModelo">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Relatório Folha</h5>
              </div>
              <form action="{{route('financeiro.relatorio')}}" method="post" id="form-relatorio-post">
                {!! csrf_field() !!}
                <input type="hidden" name="id" value="{{$folha->id}}">
                <div class="modal-body">
                  <div class="form-group row">
                    <div class="col-lg-4">
                      <label for="selOrdenacao">Modelo</label><br>
                      <select class="form-control" id="selOrdenacao" name="modelo" required>
                        <option value="" selected>Selecione...</option>
                        <option value="upa">Pagamento 1</option>
                        <option value="recife">Pagamento 2</option>
                        <option value="simplificado">Pagamento Simplificado</option>
                        <option value="recibo">Recibo</option>
                        <option value="fatura">Fatura</option>
                      </select>
                    </div>
                    <div class="col-lg-4">
                      <label for="selOrdenacao">Ordenação</label>
                      <select class="form-control" id="selOrdenacao" name="ordenacao">
                        <option value="nomeCres" selected>Nome Crescente</option>
                        <option value="nomeDec">Nome decrescente</option>
                        <option value="nTce">Nº TCE</option>
                        <option value="dtInicio">Data Início</option>
                        <option value="dtFim">Data Fim</option>
                      </select>
                    </div>
                    <div class="col-lg-4">
                      <label for="vlSeparacao">Valor de filtragem</label>
                      <input type="tel" class="form-control" id="vlSeparacao" min="0" value="0" name="valor" required>
                      <small>Digite 0 (zero) caso queira todos os itens</small>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-lg-4">
                      <label>Incluir itens com desconto?</label><br>
                      <input type="hidden" name="itesnDesc" value="sim">
                      <button type="button" class="btn btn-primary" style="margin-right: 10px;" name="btn-include" onclick="includeItens(this)">Sim</button>
                      <button type="button" class="btn btn-default" name="btn-not-include" onclick="includeItens(this)">Não</button>
                    </div>
                    <div class="col-lg-5">
                      <label for="">Comparar com a folha anterior?</label>
                      <input type="hidden" name="comparacao" value="nao">
                      <button type="button" class="btn btn-default" style="margin-right: 10px;" name="btn-comparar-sim" onclick="compararFolha(this)">Sim</button>
                      <button type="button" class="btn btn-primary" name="btn-comparar-nao" onclick="compararFolha(this)">Não</button>
                    </div>
                    <div class="col-lg-3">
                      <label for="selVisao">Gerar como:</label>
                      <select class="form-control" id="selOrdenacao" name="result">
                        <option value="view">Visão</option>
                        <option value="pdf">PDF</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                  <button type="submit" class="btn btn-primary">Confirmar</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--  Fim Botões abaixo da folha gerada  -->
    <div class="row tables-tces" id="rowPagamento" style="margin-bottom:20px;">
      <div class="col-md-12">
        <div class="box box-success">
          <div class="box-header with-border">
            <div class="col-md-4">
              <h3 class="box-title">Folha de Pagamento {{$folha->referenciaMes}}/{{$folha->referenciaAno}}</h3>
            </div>
            <div class="col-md-4">
              <div class="input-group">
                <input type="search" name="searchFolha" value="" placeholder="Buscar por nome, cpf, n° tce..." class="form-control">
                <div class="input-group-addon">
                  <i class="fa fa-search"></i>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <label for="" class="btn bg-green pull-right" disabled><i class="fa fa-check"></i> Folha Fechada</label>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table id="itemsFolha" class="table table-condensed table-striped" style="font-size: 12px;">
              <thead>
                <tr>
                  <th style="width: 5%;">TCE</th>
                  <th style="width: 20%;">Estudante</th>
                  <th style="width: 8%; text-align:center;">Recesso/Valor</th>
                  <th style="width: 4%; text-align:center;">Dias B.</th>
                  <th style="width: 4%; text-align:center;">Faltas</th>
                  <th style="width: 4%; text-align:center;">Just.</th>
                  <th style="width: 4%;">Valor Aux.</th>
                  <th style="width: 4%;">Valor Transp.</th>
                  <th style="width: 8%;">Desc. Bolsa</th>
                  <th style="width: 8%;">Desc. Transp.</th>
                  <th style="width: 8%;">Valor Total</th>
                </tr>
              </thead>
              <tbody id="listItensFolha">
                @forelse($itensFolha->chunk(50) as $chunk)
                @foreach($chunk as $item)
                @if($item->is_block === 1)
                <tr class="items-folha danger" data-nome="{{$item->tce->estudante->nmEstudante}}" data-tce="{{$item->tce->id}}" data-item="{{$item->id}}" data-cpf="{{ str_replace(array('.','-'),'',$item->tce->estudante->cdCPF)}}">
                  @else
                  <tr class="items-folha" data-nome="{{$item->tce->estudante->nmEstudante}}" data-tce="{{$item->tce->id}}" data-item="{{$item->id}}" data-cpf="{{ str_replace(array('.','-'),'',$item->tce->estudante->cdCPF)}}">
                    @endif
                    <td class="folha" id="{{$item->tce->id}}">{{$item->tce->id}}
                      @if($item->tce->dtCancelamento != NULL)<i class="fa fa-remove text-red" title="Este TCE foi cancelado!"></i>@else @endif
                    </td>
                    <td>{{$item->tce->estudante->nmEstudante}}<br><small><span class="direct-chat-timestamp pull-left"><b>CPF:</b> {{$item->tce->estudante->cdCPF}}</span></small></td>
                    <td id="item-recesso{{$item->id}}" style="text-align:center" data-valor="{{$item->diasRecesso}}">
                      {{$item->diasRecesso ? $item->diasRecesso.' dias - R$'.$item->vlRecesso : "-"}}
                    </td>
                    <td id="item-dtPagamento{{$item->id}}" class="text-center" data-valor="{{$item->dtPagamento}}" style="display:none;"></td>
                    <td id="item-base{{$item->id}}" class="text-center" data-valor="{{$item->diasBaseEstagiados}}">{{$item->diasBaseEstagiados}}</td>
                    <td id="item-faltas{{$item->id}}" class="text-center" data-valor="{{$item->faltas}}">{{$item->faltas}}</td>
                    <td id="item-justificadas{{$item->id}}" class="text-center" data-valor="{{$item->justificadas}}">{{$item->justificadas}}</td>
                    <td id="item-vlMensal{{$item->id}}" data-valor="{{$item->vlAuxilioMensalReceber}}">R$ {{number_format($item->vlAuxilioMensalReceber, 2, ',', '.')}}</td>
                    <td id="item-vlTransp{{$item->id}}" data-valor="{{$item->vlAuxilioTransporteReceber}}">R$ {{number_format($item->vlAuxilioTransporteReceber, 2, ',', '.')}}</td>
                    <td id="item-descontoMensal{{$item->id}}" data-valor="{{$item->descontoVlAuxMensal}}">R$ {{$item->descontoVlAuxMensal}}</td>
                    <td id="item-descontoTransp{{$item->id}}" data-valor="{{$item->descontoVlAuxTransporte}}">R$ {{$item->descontoVlAuxTransporte}}</td>
                    <td id="item-vltotal{{$item->id}}" data-valor="{{$item->vlTotal}}">R$ {{number_format($item->vlTotal, 2, ',', '.')}}</td>
                  </tr>
                  @endforeach
                  @empty
                  <tr>
                    <td colspan="13" align="center">
                      <span class="badge bg-red">Sem registros no banco de dados!</span>
                    </td>
                  </tr>
                  @endforelse
                  <tr id="nothingItemsFolha" style="display:none;">
                    <td colspan="13" align="center">
                      <h4><label class="label label-info">Nenhum tce foi encontrado</label></h4>
                    </td>
                  </tr>
                </tbody>
                <tbody id="loadFolha" style="display:none;">
                  <tr>
                    <td colspan="13" class="text-center"><div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>
                  </tr>
                </tbody>
                <tfoot>
                  <tr>
                    <td colspan="5"><p class="h5 pull-left"><b>Total:</b> {{$itensFolha->count()}}</p></td>
                    <td colspan="8"><p class="h5" style="float:right;text-align:right;"><b>Valor Total da Folha:</b> R$ {{number_format($itensFolha->sum('vlTotal'), 2, ',', '.')}}</p></td>
                  </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>

        </div>
      </div>
      <!--  Botões abaixo da folha gerada  -->
      @section('post-script')
      <script type="text/javascript" src="{{ asset('vendor/adminlte/plugins/iCheck/icheck.min.js') }}"></script>
      <script type="text/javascript" src="{{ asset('assets/vendor/toast/toastr.js')}}"></script>
      <script type="text/javascript">

      $('input[name=searchFolha]').keyup(function(){
        var tces = $(".items-folha");
        var nothing = $('#nothingItemsFolha');
        var load = $("#loadFolha");
        var nome = $(this).val();
        search(nome,tces,nothing,load);
      });

      function search(nome,tces,nothing,load){
        nothing.hide();
        var count = 0;
        tces.show();
        if (nome != "") {
          for (var i = 0; i < tces.length; i++) {
            if (tces[i].dataset.nome.toLowerCase().search(nome.toLowerCase()) && tces[i].dataset.tce.toLowerCase().search(nome.toLowerCase()) && tces[i].dataset.cpf.search(nome)) {
              tces[i].style.display = "none";
            }else{
              count++;
            }
          }
          if(count == 0){
            nothing.show();
          }
        }
      }

      function includeItens(btn){
        btn.classList.remove('btn-default');
        btn.classList.add('btn-primary');
        if(btn.getAttribute('name') == "btn-include"){
          $('input[name=itesnDesc]').find('input[name=itesnDesc]').val('sim');
          $('button[name=btn-not-include]').removeClass('btn-primary').addClass('btn-default');
        }else{
          $('#form-relatorio-post').find('input[name=itesnDesc]').val('nao');
          $('button[name=btn-include]').removeClass('btn-primary').addClass('btn-default');
        }
      }

      function getPagamento(id, modelo){
        $("#form-post-relatorio").find("input[name=id]").val(id);
        $("#form-post-relatorio").find("input[name=modelo]").val(modelo);
        $("#form-post-relatorio").find("input[name=submit]").click();
      }

      function compararFolha(btn){
        btn.classList.remove('btn-default');
        btn.classList.add('btn-primary');
        if(btn.getAttribute('name') == "btn-comparar-sim"){
          $('#form-relatorio-post').find('input[name=comparacao]').val('sim');
          $('button[name=btn-comparar-nao]').removeClass('btn-primary').addClass('btn-default');
        }else{
          $('#form-relatorio-post').find('input[name=comparacao]').val('nao');
          $('button[name=btn-comparar-sim]').removeClass('btn-primary').addClass('btn-default');
        }
      }
      </script>
      @endsection
      @stop
