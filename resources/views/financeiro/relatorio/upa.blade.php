<!DOCTYPE html>
<html>
<head>
    <title>Folha Pagamento</title>
    <!-- Bootstrap 4-ONLINE -->
    <style>
            html { margin: 20px}

            body {
                font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial;
            }

            footer.fixar-rodape {
                bottom: 0;
                left: 0;
                height: 40px;
                position: fixed;
                width: 100%;
                margin-left: 0;
            }

            .novaPagina {
                page-break-before: always;
                page-break-after: always
            }

            div.body-content {
                /** Essa margem vai evitar que o conteudo fique por baixo do rodapé **/
                margin-bottom: 40px;
            }

            .col-12 {
                width: 100% !important;
            }

            .col-6 {
                width: 50% !important;
            }

            .col-6,
            .col-12 {
                float: left !important;
            }

            .table {
                border-collapse: separate;
                border-spacing: 0;
                min-width: 350px;
                margin-bottom: 10px;
            }
            .table th tr {
                font-size: 5pt;
            }
            .table tr th,
            .table tr td {
                border-right: 1px solid #bbb;
                border-bottom: 1px solid #bbb;
                padding: 5px;
            }

            .table th {
                border-top: solid 1px #bbb;
            }

            .table tr th:first-child,
            .table tr th:last-child {
                border-top: solid 1px #bbb;
            }

            .table tr th:first-child,
            .table tr td:first-child {
                border-left: 1px solid #bbb;
            }

            .table tr th:first-child,
            .table tr td:first-child {
                border-left: 1px solid #bbb;
            }

            .table tr th {
                background: #eee;
                text-align: left;
            }

            .table.Info tr th,
            .table.Info tr:first-child td {
                border-top: 1px solid #bbb;
            }
            /* top-left border-radius */

            .table tr:first-child th:first-child,
            .table.Info tr:first-child td:first-child {
                border-top-left-radius: 6px;
            }
            /* top-right border-radius */

            .table tr:first-child th:last-child,
            .table.Info tr:first-child td:last-child {
                border-top-right-radius: 6px;
            }
            /* bottom-left border-radius */

            .table tr:last-child td:first-child {
                border-bottom-left-radius: 6px;
            }
            /* bottom-right border-radius */

            .table tr:last-child td:last-child {
                border-bottom-right-radius: 6px;
            }
        </style>
    </head>

    <body>
        <div id="body">
            <div id="content">

                <div class="page" style="font-size: 6pt">
                    <table style="width: 100%;" class="header">
                        <tr>
                                <td style="text-align: left; font-size: 8">
                                        <span>
                                                <strong>Folha de Pagamento</strong><br>
                                                <strong>Razão Social:</strong> {{mb_strtoupper($folha->concedente->nmRazaoSocial)}}<br>
                                                <strong>CNPJ:</strong> {{mb_strtoupper($folha->concedente->cdCnpjCpf)}}<br>
                                                <strong>Endereço:</strong> {{$folha->concedente->dsEndereco}}, {{$folha->concedente->nnNumero}} - {{$folha->concedente->nmBairro}}<br>
                                                <strong>CEP:</strong> {{$folha->concedente->cdCEP}} - {{$folha->concedente->cidade->nmCidade}}/{{$folha->concedente->cidade->estado->cdUF}}<br>
                                                <strong>COMPETENCIA: @php
                                                        $refMes = $folha->referenciaMes;
                                                        $mes = $partes= explode("-", $refMes);
                                                        echo $mes[1];
                                                        @endphp/{{$folha->referenciaAno}}
                                                </strong>
                                        </span>
                                            {{--  @if(!empty($secretaria))
                                            <h3 style="text-align: right">
                                            - {{mb_strtoupper($secretaria->nmSecretaria)}}
                                            </h3>
                                            @endif  --}}
                                </td>
                            <td style="text-align: right;">
                                <?php $image_path = '/img/relatorios/logo_default.png'; ?>
                                <img src="{{ public_path() . $image_path }}" alt="Upa - Estagio" width="170px" height="80px"/>
                            </td>

                        </tr>
                    </table>
                    <hr style=" margin-bottom: 0px; border-bottom: 1px solid #DCDCDC ;">
                    <br>
@if($modelo == "pagamento")
<!-- Modelo pagamento com contas dos estágiarios -->
<table class="table table-bordered" style="width: 100%;">
        <tr>
            <th width="1">#</th>
            <th width="1">TCE</th>
            <th>ESTUDANTE</th>
            <th width="50">CPF</th>
            <th width="50">DT.NASCIMENTO</th>
            <th width="20">BANCO</th>
            <th style="text-align: right;">AGENCIA</th>
            <th width="40"style="text-align: right;">CONTA</th>
            <th style="text-align: right;">TOTAL A RECEBER</th>
        </tr>
        @php
        $totalTCE = 0;
        $valorTotal = 0;
        $valorTotalTransporte = 0;
        $valorTotalBolsas = 0;
        $valorTotalRecessos = 0;
        @endphp
        {{-- @foreach($Concedente->tce->where('dtCancelamento','==',NULL)->sortBy($Ordenacao) as $tce) --}}
        @foreach($itensFolha as $item)
        @php $totalTCE++; @endphp
            <tr>
                <td>{{$totalTCE}}</td>
                <td>
                    @if($item->tce->migracao)
                        {{$item->tce->migracao}}
                    @else
                        {{$item->tce->id}}
                    @endif
                </td>
                <td>{{$item->tce->estudante->nmEstudante}}</td>
                <td>{{$item->tce->estudante->cdCPF}}</td>
                <td>{{$item->tce->estudante->dtNascimento->format('d/m/Y')}}</td>
                <td>
                    @php
                    $banco = $item->tce->estudante->dsBanco;
                    $cod_banco = $partes= explode("-", $banco);
                    echo $cod_banco[0];
                    @endphp
                    {{--  {{$item->tce->estudante->dsBanco}}  --}}
                </td>
                {{--  <td style="font-size: 10px;">{{$item->tce->estudante->dsAgencia}} / {{$item->tce->estudante->dsTipoConta}}</td>  --}}
                <td style="font-size: 10px;text-align: right;">{{$item->tce->estudante->dsAgencia}}</td>
                <td style="font-size: 10px;text-align: right;">{{$item->tce->estudante->dsConta}}</td>
                <td style="text-align: right;">{{number_format($item->vlTotal, 2, ',', '.')}}</td>
                @php
                $valorTotal+=$item->vlTotal;
                $valorTotalTransporte+= $item->vlAuxilioTransporteReceber;
                $valorTotalBolsas+= $item->vlAuxilioMensalReceber;
                $valorTotalRecessos+= $item->vlRecesso;
                @endphp

            </tr>

        @endforeach
    </table>
    <table style="width: 100%;" class="header">
            <tr>
                <td style="text-align: left">
                    <span>
                            <strong>Valor Total das Bolsas Auxilios:</strong> R$ {{number_format($valorTotalBolsas, 2, ',', '.')}}<br>
                            <strong>Valor Total dos Recessos Remunerados:</strong> R$ {{number_format($valorTotalRecessos, 2, ',', '.')}}<br>
                            <strong>Valor Total de Auxilio Transporte:</strong> R$ {{number_format($valorTotalTransporte, 2, ',', '.')}}<br>
                            {{--  <strong>Valor Total de Bolsas+Transporte:</strong> R$ {{number_format($valorTotal, 2, ',', '.')}}<br>  --}}
                            <strong>Total de Estágiarios:</strong> {{$totalTCE}}<br>
                            <strong>Contribuição Institucional(CI):</strong> R$ {{number_format($folha->concedente->vlEstagio, 2, ',', '.')}}<br>
                            <strong>Total de CI: </strong>R$ @php echo number_format($totalTCE*$folha->concedente->vlEstagio, 2, ',', '.')@endphp<br>
                            <strong>Valor Total: </strong>R$ @php echo number_format($totalTCE*$folha->concedente->vlEstagio+$valorTotal, 2, ',', '.')@endphp

                    </span>
                        {{--  @if(!empty($secretaria))
                        <h3 style="text-align: right">
                        - {{mb_strtoupper($secretaria->nmSecretaria)}}
                        </h3>
                        @endif  --}}

                </td>
            </tr>
        </table>
<!-- Modelo pagamento com contas dos estágiarios -->
@else
<!-- grupamento po Estado -->
<table class="table table-bordered" style="width: 100%;">
        <tr>
            <th width="1">#</th>
            <th width="1">TCE</th>
            <th>ESTUDANTE</th>
            <th width="20">BANCO</th>
            <th width="20" style="text-align: right;">AGENCIA</th>
            <th width="40" style="text-align: right;">CONTA</th>
            <th width="20">BOLSA AUXILIO MÊS</th>
            <th width="20">PROP.RECESSO / VALOR</th>
            <th width="20">DIAS BASE ESTAGIADO</th>
            <th width="20">BOLSA A RECEBER</th>
            <th width="20">DIAS TRABALHADOS</th>
            {{--  <th width="20">VALOR AUXILIO TRANSP. DIA</th>  --}}
            <th width="20">AUXILIO TRANSP. RECEBER</th>
            <th width="20">AUXILIO ALIM.</th>

            <th width="40" style="text-align: right;">TOTAL A RECEBER</th>
        </tr>
        @php
        $totalTCE = 0;
        $valorTotal = 0;
        $valorTotalTransporte = 0;
        $valorTotalBolsas = 0;
        $valorTotalRecessos = 0;
        @endphp
        {{-- @foreach($Concedente->tce->where('dtCancelamento','==',NULL)->sortBy($Ordenacao) as $tce) --}}
        @foreach($itensFolha as $item)
        @php $totalTCE++; @endphp
            <tr>
                <td>{{$totalTCE}}</td>
                <td>
                    @if($item->tce->migracao)
                        {{$item->tce->migracao}}
                    @else
                    {{$item->tce->id}}
                    @endif
                </td>

                <td>{{$item->tce->estudante->nmEstudante}}</td>
                <td style="text-align: right;">
                        @php
                        $banco = $item->tce->estudante->dsBanco;
                        $cod_banco = $partes= explode("-", $banco);
                        echo $cod_banco[0];
                        @endphp
                        {{--  {{$item->tce->estudante->dsBanco}}  --}}
                </td>
                {{--  <td style="font-size: 10px;">{{$item->tce->estudante->dsAgencia}} / {{$item->tce->estudante->dsTipoConta}}</td>  --}}
                <td style="font-size: 10px;text-align: right;">{{$item->tce->estudante->dsAgencia}}</td>
                <td style="font-size: 10px;text-align: right;">{{$item->tce->estudante->dsConta}}</td>
                <td >{{number_format($item->vlAuxilioMensal, 2, ',', '.')}}</td>
                @if(($item->mesesPropRecesso == NULL) AND ($item->vlRecesso == NULL))
                    <td style="text-align: right;">-</td>
                @else
                    <td style="text-align: right;">{{$item->mesesPropRecesso}} / {{number_format($item->vlRecesso, 2, ',', '.')}}</td>
                @endif
                <td style="text-align: right;">{{$item->diasBaseEstagiados}}</td>
                <td style="text-align: right;">{{number_format($item->vlAuxilioMensalReceber, 2, ',', '.')}}</td>
                <td style="text-align: right;">{{$item->diasTrabalhados}}</td>
                {{--  <td style="text-align: right;">{{number_format($item->vlAuxilioTransporteDia, 2, ',', '.')}}</td>  --}}
                <td style="text-align: right;">{{number_format($item->vlAuxilioTransporteReceber, 2, ',', '.')}}</td>
                <td style="text-align: right;">{{number_format($item->vlAuxilioAlimentacaoReceber, 2, ',', '.')}}</td>

                <td style="text-align: right;">{{number_format($item->vlTotal, 2, ',', '.')}}</td>
                @php
                $valorTotal+=$item->vlTotal;
                $valorTotalTransporte+= $item->vlAuxilioTransporteReceber;
                $valorTotalBolsas+= $item->vlAuxilioMensalReceber;
                $valorTotalRecessos+= $item->vlRecesso;
                @endphp

            </tr>

        @endforeach
    </table>
    <table style="width: 100%;" class="header">
            <tr>
                {{-- <td>
                    <div class="span12">
                        <p  style="margin: 10px; text-align: center;">___________________, _____ de ______________ de _______.                 Assinatura e Carimbo do Responsável: __________________________________________</p>
                    </div>
                </td> --}}
                <td style="text-align: left; font-size: 8">
                    {{--<span>
                              <strong>Valor Total das Bolsas Auxilios:</strong> R$ {{number_format($valorTotalBolsas, 2, ',', '.')}}<br>
                            <strong>Valor Total dos Recessos Remunerados:</strong> R$ {{number_format($valorTotalRecessos, 2, ',', '.')}}<br>
                            <strong>Valor Total de Auxilio Transporte:</strong> R$ {{number_format($valorTotalTransporte, 2, ',', '.')}}<br>
                            <!-- <strong>Valor Total de Bolsas+Transporte:</strong> R$ {{number_format($valorTotal, 2, ',', '.')}}<br>  -->
                            <strong>Total de Estágiarios:</strong> {{$totalTCE}}<br>
                            <strong>Contribuição Institucional(CI):</strong> R$ {{number_format($folha->concedente->vlEstagio, 2, ',', '.')}}<br>
                            <strong>Total de CI: </strong> R$ @php echo number_format($totalTCE*$folha->concedente->vlEstagio, 2, ',', '.')@endphp<br>
                            <strong>Valor Total: </strong> R$ @php echo number_format($totalTCE*$folha->concedente->vlEstagio+$valorTotal, 2, ',', '.')@endphp

                    </span>--}}
                        {{--  @if(!empty($secretaria))
                        <h3 style="text-align: right">
                        - {{mb_strtoupper($secretaria->nmSecretaria)}}
                        </h3>
                        @endif  --}}
                        <table width="300px" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                  <td><strong>Valor Total das Bolsas Auxilios:</strong></td>
                                  <td style="text-align: right;">R$ {{number_format($valorTotalBolsas, 2, ',', '.')}}</td>
                                </tr>
                                <tr>
                                  <td><strong>Valor Total dos Recessos Remunerados:</strong></td>
                                  <td style="text-align: right;">R$ {{number_format($valorTotalRecessos, 2, ',', '.')}}</td>
                                </tr>
                                <tr>
                                  <td><strong>Valor Total de Auxilio Transporte:</strong></td>
                                  <td style="text-align: right;">R$ {{number_format($valorTotalTransporte, 2, ',', '.')}}</td>
                                </tr>
                                <tr>
                                  <td><strong>Total de Estágiarios:</strong></td>
                                  <td style="text-align: right;">{{$totalTCE}}</td>
                                </tr>
                                <tr>
                                  <td><strong>Contribuição Institucional(CI):</strong></td>
                                  <td style="text-align: right;">R$ {{number_format($folha->concedente->vlEstagio, 2, ',', '.')}}</td>
                                </tr>
                                <tr>
                                  <td><strong>Total de CI: </strong></td>
                                  <td style="text-align: right;">R$ @php echo number_format($totalTCE*$folha->concedente->vlEstagio, 2, ',', '.')@endphp</td>
                                </tr>
                                <tr>
                                  <td><strong>Valor Total: </strong></td>
                                  <td style="text-align: right;">R$ @php echo number_format($totalTCE*$folha->concedente->vlEstagio+$valorTotal, 2, ',', '.')@endphp</td>
                                </tr>
                              </table>
                </td>
            </tr>
        </table>
<!-- FIM. Agrupamento po Estado -->
@endif
<div id="footer">
    @include('relatorios.inc_rodape')
    <script type="text/php">
        if (isset($pdf)) {
            $x = 785;
            $y = 575;
            $text = "Página {PAGE_NUM} de {PAGE_COUNT}";
            $font = null;
            $size = 7;
            $color = array(0,0,0);
            $word_space = 0.0;  //  default
            $char_space = 0.0;  //  default
            $angle = 0.0;   //  default
            $pdf->page_text($x, $y, $text, $font, $size, $color, $word_space, $char_space, $angle);
        }
    </script>
</div>
</div>
</div>
</body>
</html>
