<!DOCTYPE html>
<html>
<head>
  <title>Folha Pagamento - Recife</title>
  <!-- Bootstrap 4-ONLINE -->
  <style>
  html { margin: 20px}

  body {
    font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial;
  }

  footer.fixar-rodape {
    bottom: 0;
    left: 0;
    height: 40px;
    position: fixed;
    width: 100%;
    margin-left: 0;
  }

  .novaPagina {
    page-break-before: always;
    page-break-after: always
  }

  div.body-content {
    /** Essa margem vai evitar que o conteudo fique por baixo do rodapé **/
    margin-bottom: 40px;
  }

  .col-12 {
    width: 100% !important;
  }

  .col-6 {
    width: 50% !important;
  }

  .col-6,
  .col-12 {
    float: left !important;
  }

  .table {
    border-collapse: separate;
    border-spacing: 0;
    min-width: 350px;
    margin-bottom: 10px;
  }
  .table th tr {
    font-size: 5pt;
  }
  .table tr th,
  .table tr td {
    border-right: 1px solid #bbb;
    border-bottom: 1px solid #bbb;
    padding: 5px;
  }

  .table th {
    border-top: solid 1px #bbb;
  }

  .table tr th {
    background: #eee;
    text-align: left;
  }
  </style>
</head>

<body>
  <div id="body">
    <div id="content">

      <div class="page" style="font-size: 6pt">
        <table style="width: 100%;" class="header">
          <tr>
            <td style="text-align: left; font-size: 8">
              <span>
                <strong>Folha de Pagamento</strong><br>
                @if($folha->secConcedente_id != null)
                <strong>Razão Social:</strong> {{mb_strtoupper($folha->secConcedente->nmSecretaria)}}<br>
                <strong>CNPJ:</strong> {{mb_strtoupper($folha->secConcedente->cdCnpjCpf)}}<br>
                <strong>Endereço:</strong> {{$folha->secConcedente->dsEndereco}}, {{$folha->secConcedente->nnNumero}} - {{$folha->secConcedente->nmBairro}}<br>
                @else
                <strong>Razão Social:</strong> {{mb_strtoupper($folha->concedente->nmRazaoSocial)}}<br>
                <strong>CNPJ:</strong> {{mb_strtoupper($folha->concedente->cdCnpjCpf)}}<br>
                <strong>Endereço:</strong> {{$folha->concedente->dsEndereco}}, {{$folha->concedente->nnNumero}} - {{$folha->concedente->nmBairro}}<br>
                @endif

                <strong>CEP:</strong> {{$folha->concedente->cdCEP}} - {{$folha->concedente->cidade->nmCidade}}/{{$folha->concedente->cidade->estado->cdUF}}<br>
                <strong>COMPETENCIA: @php
                  $refMes = $folha->referenciaMes;
                  $mes = $partes= explode("-", $refMes);
                  echo $mes[1];
                  @endphp/{{$folha->referenciaAno}}
                </strong>
              </span>
            </td>
            <td style="float:right;">
              <p style="float:right;">
                <b>Código</b>: {{\Carbon\Carbon::now()->format('dm')}}{{Auth::id()}}{{$folha->concedente_id}}{{$folha->secConcedente_id != null ? $folha->secConcedente_id : "00"}}{{$itensFolha->count()}}{{\Carbon\Carbon::now()->year}}
                <br><b>Vencimento</b>: 20/{{\Carbon\Carbon::now()->month}}/{{\Carbon\Carbon::now()->year}}
                <br><b>Gerado em</b>: {{\Carbon\Carbon::now()->format('d/m/Y h:m:s')}}
              </p>
            </td>
            <td style="text-align: right;">
              <img src="{{ asset('img/relatorios/logo_default.png') }}" alt="Upa - Estagio" width="170px" height="80px"/>
            </td>
          </tr>
        </table>
        <hr style=" margin-bottom: 0px; border-bottom: 1px solid #DCDCDC ;">
        <br>
        <!-- Modelo pagamento com contas dos estágiarios -->
        <table class="table table-bordered" style="max-width: auto; width:100%; min-width:1024px;">
          <tbody>
            <col>
            <colgroup span="3"></colgroup>
            <colgroup span="2"></colgroup>
            <colgroup span="2"></colgroup>
            <colgroup span="2"></colgroup>
            <colgroup span="3"></colgroup>
            <colgroup span="5"></colgroup>
            <tr>
              <td colspan="4" style="width:30%;"></td>
              <th colspan="2" style="width:10%; text-align:center;" scope="colgroup">Contrato</th>
              <th colspan="2" style="width:10%; text-align:center;" scope="colgroup">Faltas</th>
              <th colspan="2" style="width:10%; text-align:center;" scope="colgroup">Vantagens</th>
              <th colspan="2" style="width:10%; text-align:center;" scope="colgroup">Descontos</th>
              <th colspan="3" style="width:10%; text-align:center;" scope="colgroup">Totais</th>
              <th colspan="5" style="width:30%;" scope="colgroup">Dados Bancários</th>
            </tr>
            <tr>
              <th scope="col">Ord.</th>
              <th scope="col">TCE</th>
              <th scope="col">CPF</th>
              <th scope="col">Nome</th>
              <th scope="col">Início</th>
              <th scope="col">Término</th>
              <th scope="col">Não Just.</th>
              <th scope="col">Just.</th>
              <th scope="col">Bolsa</th>
              <th scope="col">Aux Tra</th>
              <th scope="col">Bolsa</th>
              <th scope="col">Aux Tra</th>
              <th scope="col">Dias</th>
              <th scope="col">Líquido</th>
              <th scope="col">Taxa</th>
              <th scope="col">Bco</th>
              <th scope="col">Ag.</th>
              <th scope="col">C/C</th>
              <th scope="col">Obs.:</th>
            </tr>
            @php
            $totalTCE = 0;
            $valorTotal = 0;
            $valorTotalTransporte = 0;
            $valorTotalBolsas = 0;
            $valorTotalRecessos = 0;
            $vlLiquido = 0;
            $vlBolsa = 0;
            $extras = 0;
            @endphp
            @forelse($itensFolha->chunk(50) as $chunk)
            @foreach($chunk as $item)
            @php
            $totalTCE++;
            $vlBolsa = $vlBolsa + ($item->vlAuxilioMensalReceber + $item->descontoVlAuxMensal);
            $vlLiquido = ($vlLiquido + $item->vlAuxilioMensalReceber + $item->vlAuxilioTransporteReceber) - ($item->descontoVlAuxTransporte - $item->descontoVlAuxMensal);
            @endphp
            <tr>
              <td style="width:1%;">{{$totalTCE}}</td>
              <td style="width:1%;">{{$item->tce->aditivos()->count() > 0 ? $item->tce->id.'/'.$item->tce->aditivos()->count() : $item->tce->id}}</td>
              <td style="width:3%;">{{$item->tce->estudante->cdCPF}}</td>
              <td style="width:10%;">{{$item->tce->estudante->nmEstudante}}</td>
              <td style="width:4%;">{{$item->tce->dtInicio->format('d/m/Y')}}</td>
              @if($item->tce->dtCancelamento == null)
              <td style="width:4%;">{{$item->tce->aditivos()->count() > 0 ? \Carbon\Carbon::parse($item->tce->aditivos()->latest()->first()->dtFim)->format('d/m/Y') : \Carbon\Carbon::parse($item->tce->dtFim)->format('d/m/Y')}}</td>
              @else
              <td style="width:4%;">{{$item->tce->dtCancelamento->format('d/m/Y')}}</td>
              @endif
              <td style="width:2.5%;">{{$item->faltas > 0 ? $item->faltas : 0 }}</td>
              <td style="width:2.5%;">{{$item->justificadas > 0 ? $item->justificadas : 0}}</td>
              <td style="width:3%;">R$ {{$item->vlAuxilioMensalReceber != 0 ? number_format($item->vlAuxilioMensalReceber, 2, ',', '.') : '0,00'}}</td>
              <td style="width:3%;">R$ {{$item->vlAuxilioTransporteReceber != 0 ? number_format($item->vlAuxilioTransporteReceber, 2, ',', '.') : '0,00'}}</td>
              <td style="width:3%;">R$ {{$item->descontoVlAuxMensal != 0 ? number_format($item->descontoVlAuxMensal, 2, ',', '.') : '0,00'}}</td>
              <td style="width:3%;">R$ {{$item->descontoVlAuxTransporte != 0 ? number_format($item->descontoVlAuxTransporte, 2, ',', '.') : '0,00'}}</td>
              <td style="width:2%;">{{$item->diasBaseEstagiados}}</td>
              <td style="width:3.5%;">
                R$ {{ number_format(abs($item->vlTotal), 2, '.','') }}
              </td>
              <td style="width:3%;">
                @if($item->tce->concedente->vlEstagio != null)
                @if($item->diasBaseEstagiados > 30 AND $item->diasBaseEstagiados <= 60)
                {{ $item->tce->concedente->vlEstagio * 2 }}
                @php
                $extras = $extras + ($item->tce->concedente->vlEstagio * 2);
                @endphp
                @elseif($item->diasBaseEstagiados >= 61)
                {{ $item->tce->concedente->vlEstagio * 3 }}
                @php
                $extras = $extras + ($item->tce->concedente->vlEstagio * 3);
                @endphp
                @else
                {{ $item->tce->concedente->vlEstagio }}
                @endif
                @else
                0,00
                @endif
              </td>
              <td style="width:0.8%;">
                @php
                $banco = $item->tce->estudante->dsBanco;
                $cod_banco = $partes = explode("-", $banco);
                echo $cod_banco[0];
                @endphp
              </td>
              <td style="width:3%;">{{$item->tce->estudante->dsAgencia}}</td>
              <td style="width:4%;">{{$item->tce->estudante->dsConta}}</td>
              <td style="width:10%;">
                {{$item->diasRecesso > 0 ? 'Recesso de '.$item->diasRecesso.' dias;' : ""}}
                {{$item->faltas > 0 ? $item->faltas.' faltas registradas;' : ""}}
                {{$item->justificadas > 0 ? $item->justificadas.' faltas just. registradas;' : ""}}
                @if($item->diasBaseEstagiados > 30)
                Pagamento retroativo;
                @endif
                @if($item->tce->centroDeCusto != null)
                CC:{{$item->tce->centroDeCusto}};
                @endif
              </td>
              @php
              $valorTotal+=$item->vlTotal;
              $valorTotalTransporte+= $item->vlAuxilioTransporteReceber;
              $valorTotalBolsas+= $item->vlAuxilioMensalReceber;
              $valorTotalRecessos+= $item->vlRecesso;
              @endphp
            </tr>
            @endforeach
            @empty
            <tr>
              <td colspan="18" style="text-align:center;">Nenhum tce anexado a folha</td>
            </tr>
            @endforelse
            <tr>
              <td colspan="8" style="text-align:right;"><b>Total</b></td>
              <td colspan="1">R$ {{number_format($itensFolha->sum('vlAuxilioMensalReceber'), 2, ',', '.')}}</td>
              <td colspan="1">R$ {{number_format($itensFolha->sum('vlAuxilioTransporteReceber'), 2, ',', '.')}}</td>
              <td colspan="1">R$ {{number_format($itensFolha->sum('descontoVlAuxMensal'), 2, ',', '.')}}</td>
              <td colspan="1">R$ {{number_format($itensFolha->sum('descontoVlAuxTransporte'), 2, ',', '.')}}</td>
              <td colspan="1"></td>
              <td colspan="1">R$ {{number_format($itensFolha->sum('vlTotal'), 2, ',', '.')}}</td>
              <td colspan="5"></td>
            </tr>
          </tbody>
        </table>

        <table class="table" style="max-width:30%; float:right;">
          <tbody>
            <tr>
              <th colspan="2">Resultado Geral</th>
            </tr>
            <tr>
              <td style="width:50%;"><strong>Valor Total das Bolsas Auxilios:</strong></td>
              <td style="text-align: right;">R$ {{number_format($itensFolha->sum('vlAuxilioMensalReceber'), 2, ',', '.')}}</td>
            </tr>
            <tr>
              <td style="width:50%;"><strong>Total Desconto das Bolsas Auxilios:</strong></td>
              <td style="text-align: right;color:red;">- R$ {{number_format($itensFolha->sum('descontoVlAuxMensal'), 2, ',', '.')}}</td>
            </tr>
            <tr>
              <td style="width:50%;"><strong>Valor Total dos Recessos Remunerados:</strong></td>
              <td style="text-align: right;">R$ {{number_format($valorTotalRecessos, 2, ',', '.')}}</td>
            </tr>
            <tr>
              <td style="width:50%;"><strong>Valor Total de Auxilio Transporte:</strong></td>
              <td style="text-align: right;">R$ {{number_format($valorTotalTransporte, 2, ',', '.')}}</td>
            </tr>
            <tr>
              <td style="width:50%;"><strong>Total Desconto de Auxilio Transporte:</strong></td>
              <td style="text-align: right;color:red;">- R$ {{number_format($itensFolha->sum('descontoVlAuxTransporte'), 2, ',', '.')}}</td>
            </tr>
            <tr>
              <td style="width:50%;"><strong>Total de Estágiarios:</strong></td>
              <td style="text-align: right;">{{$totalTCE}}</td>
            </tr>
            <tr>
              <td style="width:50%;"><strong>Contribuição Institucional(CI):</strong></td>
              <td style="text-align: right;">R$ {{number_format($folha->concedente->vlEstagio, 2, ',', '.')}}</td>
            </tr>
            <tr>
              <td style="width:50%;"><strong>Quantidade Pagamentos Retroativos: </strong></td>
              <td style="text-align: right;">{{ $itensFolha->where('diasBaseEstagiados','>',30)->count()}}</td>
            </tr>
            <tr>
              <td style="width:50%;"><strong>Total de CI: </strong></td>
              <td style="text-align: right;">R$ {{number_format(($itensFolha->where('diasBaseEstagiados','<=',30)->count()*$folha->concedente->vlEstagio) + $extras, 2, ',', '.')}}</td>
            </tr>
            <tr>
              <td style="width:50%;"><strong>Valor Total: </strong></td>
              <td style="text-align: right;">R$ @php echo number_format(($itensFolha->where('diasBaseEstagiados','<=',30)->count()*$folha->concedente->vlEstagio) + $extras + $valorTotal, 2, ',', '.')@endphp</td>
            </tr>
          </tbody>
        </table>

        <div style="float:left;">
          <img src="{{ asset('img/assPalacio.jpg')}}" width="231" height="66">
          <br>
          <p style="text-align:center;"><b>Francisco Palacio Leite</b><br><small>Diretor Presidente - Universidade Patativa do Assaré</small></p>
        </div>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <div id="footer" style="bottom:0;left:0;width:100%;">
          @include('relatorios.inc_rodape_html')
          <script type="text/php">
          if (isset($pdf)) {
            $x = 785;
            $y = 575;
            $text = "Página {PAGE_NUM} de {PAGE_COUNT}";
            $font = null;
            $size = 7;
            $color = array(0,0,0);
            $word_space = 0.0;  //  default
            $char_space = 0.0;  //  default
            $angle = 0.0;   //  default
            $pdf->page_text($x, $y, $text, $font, $size, $color, $word_space, $char_space, $angle);
          }
          </script>
        </div>
      </div>
    </div>
  </body>
  </html>
