<!DOCTYPE html>
<html>
<head>
  <title>Folha Pagamento</title>
  <!-- Bootstrap 4-ONLINE -->
  <style>
  html { margin: 20px}

  body {
    font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial;
  }

  #modelo{
    margin-top: 10%;
  }

  .center {
    margin: auto;
    width: 70%;
    padding: 10px;
  }

  .font-18 {
    font-size: 18px;
  }

  footer.fixar-rodape {
    bottom: 0;
    left: 0;
    height: 40px;
    position: fixed;
    width: 100%;
    margin-left: 0;
  }

  .novaPagina {
    page-break-before: always;
    page-break-after: always
  }

  div.body-content {
    /** Essa margem vai evitar que o conteudo fique por baixo do rodapé **/
    margin-bottom: 40px;
  }

  .col-12 {
    width: 100% !important;
  }

  .col-6 {
    width: 50% !important;
  }

  .col-6,
  .col-12 {
    float: left !important;
  }

  .table {
    border-collapse: separate;
    border-spacing: 0;
    min-width: 350px;
    margin-bottom: 10px;
  }
  .table th tr {
    font-size: 5pt;
  }
  .table tr th,
  .table tr td {
    border-right: 1px solid #bbb;
    border-bottom: 1px solid #bbb;
    padding: 5px;
  }

  .table th {
    border-top: solid 1px #bbb;
  }

  .table tr th:first-child,
  .table tr th:last-child {
    border-top: solid 1px #bbb;
  }

  .table tr th:first-child,
  .table tr td:first-child {
    border-left: 1px solid #bbb;
  }

  .table tr th:first-child,
  .table tr td:first-child {
    border-left: 1px solid #bbb;
  }

  .table tr th {
    background: #eee;
    text-align: left;
  }

  .table.Info tr th,
  .table.Info tr:first-child td {
    border-top: 1px solid #bbb;
  }
  /* top-left border-radius */

  .table tr:first-child th:first-child,
  .table.Info tr:first-child td:first-child {
    border-top-left-radius: 6px;
  }
  /* top-right border-radius */

  .table tr:first-child th:last-child,
  .table.Info tr:first-child td:last-child {
    border-top-right-radius: 6px;
  }
  /* bottom-left border-radius */

  .table tr:last-child td:first-child {
    border-bottom-left-radius: 6px;
  }
  /* bottom-right border-radius */

  .table tr:last-child td:last-child {
    border-bottom-right-radius: 6px;
  }
  </style>
</head>

<body>
  <div id="body">
    <div id="section_header">
    </div>

    <div id="content">

      <div class="page" style="font-size: 6pt">
        <div id="modelo">
          <center><img src="{{ asset('/img/relatorios/logo_default.png') }}" alt="Upa - Estagio" width="170px" height="80px"/></center>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          @php
          $extras = ($itensFolha->where('diasBaseEstagiados','>', 30)->where('diasBaseEstagiados','<=', 60)->count() * $folha->concedente->vlEstagio) * 2;
          $extras = $extras + (($itensFolha->where('diasBaseEstagiados','>', 60)->count() * $folha->concedente->vlEstagio) * 3);
          @endphp
          <div class="center font-18">
            <p style="text-align:left;">
              Recibo n° <b>00{{$folha->id}}\{{\Carbon\Carbon::now()->format('Y')}}</b>
            </p>
          </div>
          <!-- grupamento po Estado -->
          <div class="center font-18">
            <p style="text-align:justify;">
              Recebi da <b>{{$concedente->nmRazaoSocial}}{{$secretaria != null ? " - ".$secretaria->nmSecretaria : "" }}</b>, inscrita no CNPJ Nº {{$secretaria != null ? " - ".$secretaria->cdCnpjCpf : $concedente->cdCnpjCpf }}, à quantia supra de R$ {{number_format($vlTotal + ($itensFolha->where('diasBaseEstagiados', '>', 30)->count() * $concedente->vlEstagio) + $itensFolha->count() * $concedente->vlEstagio, 2, ',', '.')}} ({{$vlExtenso}}), referente ao pagamento das Bolsas Auxílio e Contribuições Institucional dos estagiários de competência do mês de <b>{{explode('-', $dtFolha)[1]}}</b>.
            </p>
          </div>

          <table style="width: 100%;" class="header">
            <tr>
              <td style="text-align: center">
                <span class="font-18" style="align:left;">
                  <strong>VALOR TOTAL BOLSA AUXILIO:</strong> R$ {{number_format($vlTMensal, 2, ',', '.')}}<br>
                  <strong>TOTAL DESCONTO BOLSA AUXILIO:</strong><span style="color:red;">- R$ {{number_format($itensFolha->sum('descontoVlAuxMensal'), 2, ',', '.')}}</span><br>
                  <strong>VALOR TOTAL AUX. TRANSPORTE:</strong> R$ {{number_format($vlTTrans, 2, ',', '.')}}<br>
                  <strong>TOTAL DESCONTO AUX. TRANSPORTE:</strong><span style="color:red;">- R$ {{number_format($itensFolha->sum('descontoVlAuxTransporte'), 2, ',', '.')}}</span><br>
                  <strong>VALOR CONTRIBUIÇÃO INSTITUCIONAL:</strong> R$ {{number_format(($itensFolha->where('diasBaseEstagiados','<=', 30)->count()*$folha->concedente->vlEstagio) + $extras, 2, ',', '.')}}<br>
                  <strong>TOTAL GERAL:</strong>R$ {{ number_format(($itensFolha->where('diasBaseEstagiados','<=', 30)->count()*$folha->concedente->vlEstagio) + $extras + $vlTotal, 2, ',', '.') }}
                </span>
              </td>
            </tr>
          </table>
          <br>
          <br>
          <div style="text-align:center;float:right;margin-right:130px;font-size:14px;">
            {{$unidade}} - {{$estado}}, {{\Carbon\Carbon::now()->format('d M Y')}}
          </div>
          <br>
          <br>
          <div style="text-align:center;">
            <p class="font-18">Universidade Patativa do Assaré</p>
          </div>
          <br>
          <br>
          <div class="center" style="font-size:16px;text-align:center; margin-top:5%;">
            Coordenadora de Estágio - UPA {{$unidade}}
          </div>
          <br>
          <p class="center" style="font-size:14px;margin-top:5%;text-align:center;">DADOS BANCÁRIOS: BANCO DO BRASIL – Ag. 1598-9 / Conta Corrente 36.245-X</p>
          <div class="center" style="font-size:12px; text-align:center; margin-top:10%;">
            R. Monsenhor Esmeraldo, 36 - Bairro Franciscanos, Juazeiro do Norte - CE <br>Gerado em: {{\Carbon\Carbon::now()->format("d/m/Y h:m")}}
          </div>
        </div>

        <!-- FIM. Agrupamento po Estado -->
      </div>
    </div>
  </body>
  </html>
