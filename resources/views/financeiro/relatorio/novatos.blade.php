<!DOCTYPE html>
<html>
<head>
  <title>Folha Pagamento - Recife</title>
  <!-- Bootstrap 4-ONLINE -->
  <style>
  html { margin: 20px}

  body {
    font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial;
  }
  .text-green{
    color:#00a65a;
  }

  footer.fixar-rodape {
    bottom: 0;
    left: 0;
    height: 40px;
    position: fixed;
    width: 100%;
    margin-left: 0;
  }

  .novaPagina {
    page-break-before: always;
    page-break-after: always
  }

  div.body-content {
    /** Essa margem vai evitar que o conteudo fique por baixo do rodapé **/
    margin-bottom: 40px;
  }

  .col-12 {
    width: 100% !important;
  }

  .col-6 {
    width: 50% !important;
  }

  .col-6,
  .col-12 {
    float: left !important;
  }

  .table {
    border-collapse: separate;
    border-spacing: 0;
    min-width: 350px;
    margin-bottom: 10px;
  }
  .table th tr {
    font-size: 5pt;
  }
  .table tr th,
  .table tr td {
    border-right: 1px solid #bbb;
    border-bottom: 1px solid #bbb;
    padding: 5px;
  }

  .table th {
    border-top: solid 1px #bbb;
  }

  .table tr th {
    background: #eee;
    text-align: left;
  }
  </style>
</head>

<body>
  <div id="body">
    <div id="content">

      <div class="page" style="font-size: 6pt">
        <table style="width: 100%;" class="header">
          <tr>
            <td style="text-align: left; font-size: 8">
              <span>
                <strong>Relatório Novatos</strong><br>
                @if($folha->secConcedente_id != null)
                <strong>Razão Social:</strong> {{mb_strtoupper($folha->secConcedente->nmSecretaria)}}<br>
                @else
                <strong>Razão Social:</strong> {{mb_strtoupper($folha->concedente->nmRazaoSocial)}}<br>
                @endif
                <strong>CNPJ:</strong> {{mb_strtoupper($folha->concedente->cdCnpjCpf)}}<br>
                <strong>Endereço:</strong> {{$folha->concedente->dsEndereco}}, {{$folha->concedente->nnNumero}} - {{$folha->concedente->nmBairro}}<br>
                <strong>CEP:</strong> {{$folha->concedente->cdCEP}} - {{$folha->concedente->cidade->nmCidade}}/{{$folha->concedente->cidade->estado->cdUF}}<br>
                <strong>COMPETENCIA: @php
                  $refMes = $folha->referenciaMes;
                  $mes = $partes= explode("-", $refMes);
                  echo $mes[1];
                  @endphp/{{$folha->referenciaAno}}
                </strong>
              </span>
            </td>
            <td style="float:right;">
              <p style="float:right;">
                <b>Código</b>: {{\Carbon\Carbon::now()->format('dm')}}{{Auth::id()}}{{$folha->concedente_id}}{{$folha->secConcedente_id != null ? $folha->secConcedente_id : "00"}}{{$itensFolha->count()}}{{\Carbon\Carbon::now()->year}}
                <br><b>Vencimento</b>: 20/{{explode('-',$folha->referenciaMes)[0]}}/{{\Carbon\Carbon::now()->year}}
                <br><b>Gerado em</b>: {{\Carbon\Carbon::now()->format('d/m/Y h:m:s')}}
              </p>
            </td>
            <td style="text-align: right;">
              <img src="{{ asset('img/relatorios/logo_default.png') }}" alt="Upa - Estagio" width="170px" height="80px"/>
            </td>
          </tr>
        </table>
        <hr style=" margin-bottom: 0px; border-bottom: 1px solid #DCDCDC ;">
        <br>
        <!-- Modelo pagamento com contas dos estágiarios -->
        <table class="table table-bordered" style="max-width: auto; width:100%; min-width:1024px;">
          <thead>
            <tr>
              <th scope="col">Ord.</th>
              <th scope="col">CPF</th>
              <th scope="col">Nome</th>
              <th scope="col">Bco</th>
              <th scope="col">Ag.</th>
              <th scope="col">C/C</th>
              <th scope="col" style="width:10%;">Líquido</th>
              <th scope="col">Obs</th>
            </tr>
          </thead>
          <tbody>
            @php
            $totalTCE = 0;
            @endphp
            @forelse($itensFolha->where('exist', true) as $item)
            @php
            $totalTCE++;
            @endphp
            <tr style="font-size:14px;">
              <td>
                {{$totalTCE}}
              </td>
              <td>{{$item->tce->estudante->cdCPF}}</td>
              <td>{{$item->tce->estudante->nmEstudante}}</td>
              <td>
                @php
                $banco = $item->tce->estudante->dsBanco;
                $cod_banco = $partes = explode("-", $banco);
                echo $cod_banco[0];
                @endphp
              </td>
              <td>{{$item->tce->estudante->dsAgencia}}</td>
              <td>{{$item->tce->estudante->dsConta}}</td>
              <td>
                R$ {{ number_format(abs($item->vlTotal), 2, '.','') }}
              </td>
              <td style="width:10%;">

              </td>
            </tr>
            @empty
            <tr>
              <td colspan="18" style="text-align:center;">Nenhum novo tce anexado a folha</td>
            </tr>
            @endforelse
          </tbody>
        </table>
        <p style="font-size:14px;"><strong style="text-align:left;">Quantidade:{{$itensFolha->where('exist',true)->count()}}</strong><strong style="float:right;">Total: R$ {{number_format($itensFolha->where('exist', true)->sum('vlTotal'), 2,',','.')}}<strong></p>
      </div>
    </div>
  </body>
  </html>
