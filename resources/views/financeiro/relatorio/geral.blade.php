<!DOCTYPE html>
<html>
<head>
  <title>Relatório Geral</title>
  <!-- Bootstrap 4-ONLINE -->
  <link href="{{ asset('assets/css/argon.min.css')}}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
</head>
<body>
  <nav class="navbar navbar-horizontal navbar-expand-lg navbar-dark bg-default">
    <div class="container-fluid">
      <div class="row" style="width:100%;">
        <div class="col-md-6">
          <h4 class="navbar-brand float-left" href="#">{{$concedente->nmRazaoSocial}}</h4>
        </div>
        <div class="col-md-6">
          <b class="text-white float-right" href="#">{{$folhas->first()->referenciaMes}}/{{$folhas->first()->referenciaAno}}</b>
        </div>
      </div>
    </div>
  </nav>
  <br>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="row">
          <div class="col-md-4">
            @if($folhas->where('fechado','S')->count() == $folhas->count())
            <h5 class="card-title text-uppercase mb-0">Todas as folhas  estão fechadas</h5>
            <span class="h6 font-weight-bold mb-0"><i class="fa fa-check"></i></span>
            @else
            <h4 class="card-title text-uppercase text-muted mb-0">Faltam</h4>
            <span class="h2 font-weight-bold mb-0" style="color:red;">{{abs($folhas->where('fechado','S')->count() - $folhas->count())}} <small> serem fechadas</small></span>
            @endif
          </div>
          <div class="col-md-4">
            <h6 class="card-title text-uppercase text-muted mb-0">Gerado em:</h6>
            <span class="h4 font-weight-bold mb-0">{{\Carbon\Carbon::now()->format('d/m/Y h:m:s')}}</span>
          </div>
          <div class="col-md-4 float-right">
            <span class="btn bg-success text-white float-right" style="margin-left:25px;"><i class="far fa-envelope align-middle"></i> Folha Fechada</span><span class="btn bg-primary text-white float-right"><i class="far fa-envelope-open align-middle"></i> Folha Aberta</span>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="progress-wrapper">
              <div class="progress-info">
                <div class="progress-percentage">
                  <span>{{$folhas->where('fechado','S')->count()}}</span>
                </div>
                <div class="progress-percentage text-center">
                  <span>Folhas Fechadas</span>
                </div>
                <div class="progress-percentage">
                  <span>{{$folhas->count()}}</span>
                </div>
              </div>
              <div class="progress">
                <div class="progress-bar bg-success" role="progressbar" aria-valuenow="{{($folhas->where('fechado','S')->count() / $folhas->count()) * 100}}" aria-valuemin="0" aria-valuemax="100" style="width: {{($folhas->where('fechado','S')->count() / $folhas->count()) * 100 }}%;"></div>
              </div>
            </div>
          </div>
        </div>
        <br>
        @foreach($folhas as $folha)
        <div class="row mb-4">
          <div class="col-md-12">
            <div class="card card-stats mb-4 mb-lg-0">
              <div class="card-body">
                <div class="row">
                  @if($folha->secConcedente_id != null)
                  <div class="col-4">
                    <h6 class="card-title text-uppercase text-muted mb-0">Secretaria</h6>
                    <span class="h4 font-weight-bold mb-0">{{$folha->SecConcedente->nmSecretaria}}</span>
                  </div>
                  @else
                  <div class="col-4">

                  </div>
                  @endif
                  <div class="col-2">
                    <h5 class="card-title text-uppercase text-muted mb-0 text-center">Itens</h5>
                    <center><span class="h2 font-weight-bold mb-0">{{\App\Models\FolhaPagamentoItem::where('folha_id',$folha->id)->get()->count()}}</span></center>
                  </div>
                  {{--
                    @if($folha->secConcedente_id != null)
                  @if($folha->fechado == 'N')
                  <div class="col-1">
                    <h5 class="card-title text-uppercase text-muted mb-0 text-center">Ativos</h5>
                    <center><span class="h2 font-weight-bold mb-0">{{\App\Models\Tce::where('sec_conc_id',$folha->secConcedente_id)->where('dtCancelamento', null)->get()->count()}}</span></center>
                  </div>
                  <div class="col-2">
                    <h5 class="card-title text-uppercase text-muted mb-0 text-center">Cancelados</h5>
                    <center><span class="h2 font-weight-bold mb-0">{{\App\Models\Tce::where('sec_conc_id',$folha->secConcedente_id)->where('dtCancelamento', '!=', null)->get()->count()}}</span></center>
                  </div>
                  @else
                  <div class="col-1">
                    <h5 class="card-title text-uppercase text-muted mb-0 text-center">Ativos</h5>
                    <center><span class="h2 font-weight-bold mb-0">{{\App\Models\Tce::where('sec_conc_id',$folha->secConcedente_id)->where('dtCancelamento', null)->where('created_at','<=',$folha->fechado_at)->get()->count()}}</span></center>
                  </div>
                  <div class="col-2">
                    <h5 class="card-title text-uppercase text-muted mb-0 text-center">Cancelados</h5>
                    <center><span class="h2 font-weight-bold mb-0">{{\App\Models\Tce::where('sec_conc_id',$folha->secConcedente_id)->where('dtCancelamento', '!=', null)->where('created_at','<=',$folha->fechado_at)->get()->count()}}</span></center>
                  </div>
                  @endif
                  @else
                  @if($folha->fechado == 'N')
                  <div class="col-1">
                    <h5 class="card-title text-uppercase text-muted mb-0 text-center">Ativos</h5>
                    <center><span class="h2 font-weight-bold mb-0">{{\App\Models\Tce::where('concedente_id',$folha->concedente_id)->where('dtCancelamento', null)->get()->count()}}</span></center>
                  </div>
                  <div class="col-2">
                    <h5 class="card-title text-uppercase text-muted mb-0 text-center">Cancelados</h5>
                    <center><span class="h2 font-weight-bold mb-0">{{\App\Models\Tce::where('concedente_id',$folha->concedente_id)->where('dtCancelamento', '!=', null)->get()->count()}}</span></center>
                  </div>
                  @else
                  <div class="col-1">
                    <h5 class="card-title text-uppercase text-muted mb-0 text-center">Ativos</h5>
                    <center><span class="h2 font-weight-bold mb-0">{{\App\Models\Tce::where('concedente_id',$folha->concedente_id)->where('dtCancelamento', null)->where('created_at','<=',$folha->fechado_at)->get()->count()}}</span></center>
                  </div>
                  <div class="col-2">
                    <h5 class="card-title text-uppercase text-muted mb-0 text-center">Cancelados</h5>
                    <center><span class="h2 font-weight-bold mb-0">{{\App\Models\Tce::where('concedente_id',$folha->concedente_id)->where('dtCancelamento', '!=', null)->where('created_at','<=',$folha->fechado_at)->get()->count()}}</span></center>
                  </div>
                  @endif
                  @endif
                  --}}
                  <div class="col-2">
                    <h5 class="card-title text-uppercase text-muted mb-0 text-center">Bloqueados</h5>
                    <center><span class="h2 font-weight-bold mb-0">{{\App\Models\FolhaPagamentoItem::where('folha_id',$folha->id)->where('is_block',1)->get()->count()}}</span></center>
                  </div>
                  <div class="col-4">
                    @if($folha->fechado == 'N')
                    <div class="icon icon-shape bg-primary text-white rounded-circle shadow float-right">
                      <i class="far fa-envelope-open"></i>
                    </div>
                    @else
                    <div class="icon icon-shape bg-success text-white rounded-circle shadow float-right">
                      <i class="far fa-envelope"></i>
                    </div>
                    @endif
                  </div>
                </div>
                <br>
                <div class="row mb-4">
                  <div class="col-md-12">
                    <div class="table-responsive">
                      <table class="table table-striped">
                        <thead>
                          <tr>
                            <th class="text-center">Valor Bolsa</th>
                            <th class="text-center">Valor Transporte</th>
                            <th class="text-center">Total Desc. Bolsa</th>
                            <th class="text-center">Total Desc. Transporte</th>
                            <th class="text-center">Total Taxa</th>
                            <th class="text-center">Valor Folha</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td class="text-center">R$ {{ number_format(\App\Models\FolhaPagamentoItem::where('folha_id',$folha->id)->sum('vlAuxilioMensalReceber'), 2, ',', '.')}}</td>
                            <td class="text-center">R$ {{ number_format(\App\Models\FolhaPagamentoItem::where('folha_id',$folha->id)->sum('vlAuxilioTransporteReceber'), 2, ',', '.')}}</td>
                            <td class="text-center">R$ {{ number_format(\App\Models\FolhaPagamentoItem::where('folha_id',$folha->id)->sum('descontoVlAuxMensal'), 2, ',', '.')}}</td>
                            <td class="text-center">R$ {{ number_format(\App\Models\FolhaPagamentoItem::where('folha_id',$folha->id)->sum('descontoVlAuxTransporte'), 2, ',', '.')}}</td>
                            <td class="text-center">R$ {{ number_format((\App\Models\FolhaPagamentoItem::where('folha_id',$folha->id)->count() * $folha->concedente->vlEstagio) + (\App\Models\FolhaPagamentoItem::where('folha_id',$folha->id)->where('diasBaseEstagiados','>',30)->count() * $folha->concedente->vlEstagio), 2, ',' , '.')}}</td>
                            <td class="text-center">R$ {{ number_format((\App\Models\FolhaPagamentoItem::where('folha_id',$folha->id)->sum('vlAuxilioMensalReceber') + \App\Models\FolhaPagamentoItem::where('folha_id',$folha->id)->sum('vlAuxilioTransporteReceber')) + ((\App\Models\FolhaPagamentoItem::where('folha_id',$folha->id)->count() * $folha->concedente->vlEstagio) + (\App\Models\FolhaPagamentoItem::where('folha_id',$folha->id)->where('diasBaseEstagiados','>',30)->count() * $folha->concedente->vlEstagio)) - (\App\Models\FolhaPagamentoItem::where('folha_id',$folha->id)->sum('descontoVlAuxTransporte') + \App\Models\FolhaPagamentoItem::where('folha_id',$folha->id)->sum('descontoVlAuxMensal')), 2, ',' , '.')}}</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4">
                    <div class="mt-3 mb-0 text-muted text-sm">
                      <span class="text-nowrap">Data Criação: </span>
                      <span class="text-primary mr-2"><i class='fa fa-calendar'></i> {{\Carbon\Carbon::parse($folha->created_at)->format('d/m/Y h:m')}}</span>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="mt-3 mb-0 text-muted text-sm">
                      <span class="text-nowrap">Data Fechamento: </span>
                      <span class="text-success mr-2">@if($folha->fechado == 'S') <i class='fa fa-calendar'></i> {{\Carbon\Carbon::parse($folha->fechado_at)->format('d/m/Y h:m')}}  @else ----- @endif </span>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="mt-3 mb-0 text-muted text-sm">
                      <span class="text-nowrap">Fechada por: </span>
                      <span class="text-info mr-2">@if($folha->fechado == 'S')<i class="fa fa-user"></i> {{\App\Models\User::find($folha->fechado_user)->name}} @else ----- @endif</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <hr class="col-3">
        @endforeach
        <br>
        <div class="row">
          <div class="col-md-12">
            <center><b>sysEstagio &copy; {{\Carbon\Carbon::now()->year}}</b></center>
          </div>
        </div>
        <br>
      </div>
    </div>
  </div>
</body>
</html>
