<!DOCTYPE html>
<html>
<head>
  <title>Fatura de Folha de Pagamento</title>
  <!-- Bootstrap 4-ONLINE -->
  <style>
  html { margin: 20px}

  body {
    font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial;
  }

  #modelo{
    margin-top: 10%;
  }

  .center {
    margin: auto;
    width: 70%;
    padding: 10px;
  }

  .font-18 {
    font-size: 18px;
  }

  footer.fixar-rodape {
    bottom: 0;
    left: 0;
    height: 40px;
    position: fixed;
    width: 100%;
    margin-left: 0;
  }

  .novaPagina {
    page-break-before: always;
    page-break-after: always
  }

  div.body-content {
    /** Essa margem vai evitar que o conteudo fique por baixo do rodapé **/
    margin-bottom: 40px;
  }

  .col-12 {
    width: 100% !important;
  }

  .col-6 {
    width: 50% !important;
  }

  .col-6,
  .col-12 {
    float: left !important;
  }

  .table {
    border-collapse: separate;
    border-spacing: 0;
    min-width: 350px;
    margin-bottom: 10px;
  }
  .table th tr {
    font-size: 5pt;
  }
  .table tr th,
  .table tr td {
    border-right: 1px solid #bbb;
    border-bottom: 1px solid #bbb;
    padding: 5px;
  }

  .table th {
    border-top: solid 1px #bbb;
  }

  .table tr th:first-child,
  .table tr th:last-child {
    border-top: solid 1px #bbb;
  }

  .table tr th:first-child,
  .table tr td:first-child {
    border-left: 1px solid #bbb;
  }

  .table tr th:first-child,
  .table tr td:first-child {
    border-left: 1px solid #bbb;
  }

  .table tr th {
    background: #eee;
    text-align: left;
  }

  .table.Info tr th,
  .table.Info tr:first-child td {
    border-top: 1px solid #bbb;
  }
  /* top-left border-radius */

  .table tr:first-child th:first-child,
  .table.Info tr:first-child td:first-child {
    border-top-left-radius: 6px;
  }
  /* top-right border-radius */

  .table tr:first-child th:last-child,
  .table.Info tr:first-child td:last-child {
    border-top-right-radius: 6px;
  }
  /* bottom-left border-radius */

  .table tr:last-child td:first-child {
    border-bottom-left-radius: 6px;
  }
  /* bottom-right border-radius */

  .table tr:last-child td:last-child {
    border-bottom-right-radius: 6px;
  }
  </style>
</head>

<body>
  <div id="body">
    <div id="section_header">
    </div>

    <div id="content">

      <div class="page" style="font-size: 6pt">
        <div id="modelo">
          <center><img src="{{ asset('img/relatorios/logo_default.png') }}" alt="Upa - Estagio" width="170px" height="80px"/></center>
          <br>
          <div class="center font-18">
            <p style="text-align:left;">
              Fatura n° <b>10{{$folha->id}}\{{\Carbon\Carbon::now()->format('Y')}}</b>
            </p>
          </div>
          @php
          $extras = ($itensFolha->where('diasBaseEstagiados','>', 30)->where('diasBaseEstagiados','<=', 60)->count() * $folha->concedente->vlEstagio) * 2;
          $extras = $extras + (($itensFolha->where('diasBaseEstagiados','>', 60)->count() * $folha->concedente->vlEstagio) * 3);
          @endphp
          <!-- grupamento po Estado -->
          <div class="center font-18">
            <p style="text-align:justify;font-size:14px;">
              <b>Razão Social:</b>  {{$concedente->nmRazaoSocial}}<br>
              @if($secretaria != null)
              <b>Secretaria:</b>  {{$secretaria->nmSecretaria}}<br>
              @endif
              <b>Endereço:</b>  {{$secretaria != null ? $secretaria->dsEndereco : $concedente->dsEndereco}}<br>
              <b>CEP:</b> {{$secretaria != null ? $secretaria->cdCEP." - ".$secretaria->concedente->cidade->nmCidade."/".$secretaria->concedente->cidade->estado->cdUF : $concedente->cdCEP." - ".$concedente->cidade->nmCidade."/".$concedente->cidade->estado->cdUF}}<br>
              <b>Fone:</b>  {{$secretaria != null ? $secretaria->dsFone : $concedente->dsFone}}<br>
              <b>CNPJ:</b>  {{$secretaria != null ? $secretaria->cdCnpjCpf : $concedente->cdCnpjCpf}}<br>
              <b>Termo de Contrato N°:</b>  <br>
              <b>Competência:</b>  <br>
            </p>
            <p style="text-align:justify;">
              Solicitamos de Vossa Senhoria, a gentileza da liberação da parcela de RECURSOS FINANCEIROS para pagamento dos serviços abaixo relacionados que totalizam R$ {{number_format($vlTotal + ($itensFolha->count() * $folha->concedente->vlEstagio) + ($itensFolha->where('diasBaseEstagiados','>',30)->count() * $folha->concedente->vlEstagio), 2, ',', '.')}} ({{$vlExtenso}}) para Instituição Universidade Patativa do Assaré de competência do mês de <b>{{explode('-', $dtFolha)[1]}}</b>.
            </p>
          </div>
          <br>
          <br>
          <br>
          <br>
          <center>
          <table style="width: 30%;font-size:15px;" class="header table table-striped">
            <tbody>
              <tr>
                <td style="text-align: center">
                    <strong>VALOR TOTAL BOLSA AUXILIO:</strong>
                </td>
                <td>R$ {{number_format($vlTMensal, 2, ',', '.')}}</td>
              </tr>
              <tr>
                <td style="text-align: center">
                  <strong>VALOR TOTAL AUX. TRANSPORTE:</strong>
                </td>
                <td>
                  R$ {{number_format($vlTTrans, 2, ',', '.')}}
                </td>
              </tr>
              <tr>
                <td style="text-align: center">
                  <strong>VALOR TOTAL LÍQUIDO (Com desc.):</strong>
                </td>
                <td>
                  R$ {{number_format(($vlTTrans+$vlTMensal)-($descTrans + $descAux), 2, ',', '.')}}
                </td>
              </tr>
              <tr>
                <td style="text-align: center">
                  <strong>QUANTIDADE DE ESTAGIÁRIOS</strong>
                </td>
                <td>
                  {{$itensFolha->count()}}
                </td>
              </tr>
              <tr>
                <td style="text-align: center">
                    <strong>VALOR CONTRIBUIÇÃO INSTITUCIONAL:</strong>
                </td>
                <td>
                  R$ {{number_format(($itensFolha->where('diasBaseEstagiados','<=', 30)->count()*$folha->concedente->vlEstagio) + $extras, 2, ',', '.')}}
                </td>
              </tr>
              <tr>
                <td style="text-align: center">
                  <strong>TOTAL GERAL:</strong>
                </td>
                <td>
                  R$ {{ number_format(($itensFolha->where('diasBaseEstagiados','<=', 30)->count()*$folha->concedente->vlEstagio) + $extras + $vlTotal, 2, ',', '.') }}
                </td>
              </tr>
            </tbody>
          </table>
        </center>
          <br>
          <br>
          <div style="text-align:center;float:right;margin-right:120px;font-size:14px;">
            {{$unidade}} - {{$estado}}, {{\Carbon\Carbon::now()->format('d M Y')}}
          </div>
          <br>
          <br>
          <div style="text-align:center;font-size:16px;">
            <p style="font-size:;">Universidade Patativa do Assaré</p>
          </div>
          <br>
          <br>
          <div class="center" style="text-align:center;font-size:14px;margin-top:6%;">
            Coordenadora de Estágio - UPA {{$unidade}}
          </div>
          <br>
          <p class="center" style="text-align:center;">DADOS BANCÁRIOS: BANCO DO BRASIL – Ag. 1598-9 / Conta Corrente 36.245-X</p>
          <div class="center" style="font-size:12px; text-align:center; margin-top:4%;">
            R. Monsenhor Esmeraldo, 36 - Bairro Franciscanos, Juazeiro do Norte - CE <br>Gerado em: {{\Carbon\Carbon::now()->format("d/m/Y h:m")}}
          </div>
        </div>

        <!-- FIM. Agrupamento po Estado -->
      </div>
    </div>
  </body>
  </html>
