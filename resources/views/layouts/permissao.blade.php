
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Sistema de Estágio - UPA</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="http://localhost:8000/vendor/adminlte/vendor/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="http://localhost:8000/vendor/adminlte/vendor/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="http://localhost:8000/vendor/adminlte/vendor/Ionicons/css/ionicons.min.css">

    <!-- Theme style -->
    <link rel="stylesheet" href="http://localhost:8000/vendor/adminlte/dist/css/AdminLTE.min.css">

    <link rel="stylesheet"
          href="http://localhost:8000/vendor/adminlte/dist/css/skins/skin-red.min.css ">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-red sidebar-mini ">

<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">
        <!-- Logo -->
        <a href="/sysEstagio/" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>U</b>PA</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>Sistema</b>Estágio</span>
        </a>
    </header>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header with-border">
                            <h2 class="box-title">Ops, Desculpe</h2>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <h3>Desculpe, Você não tem permissão para essa área!</h3>
                            <h4>Faça seu login acessando o botão abaixo</h4>
                            <br>
                            <a href="{{route('login')}}">
                                <button class="btn btn-primary btn-lg">Faça seu Login</button>
                            </a>

                        </div>

                    </div>
                    <!-- /.box -->
                </div>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            Setor de TI(UPA)
            <b>Version</b> 1.0.0
        </div>
        <strong>Copyright © 2017 <a href="http://universidadepatativa.com.br">Universidade Patativa</a>.</strong> All rights
        reserved.
    </footer>
</div>
<!-- ./wrapper -->
</body>
</html>