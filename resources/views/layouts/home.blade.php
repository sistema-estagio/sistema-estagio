@extends('adminlte::master')
@section('title_postfix', ' - Escolha seu Login')
@section('adminlte_css')
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/plugins/iCheck/square/blue.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/css/auth.css') }}">
    @yield('css')
@stop

@section('body_class', 'login-page')

@section('body')

<div class="login-box">
        <div class="login-box-body">
                <div class="row">
                        <div class="col-xs-12">
                            <a href="{{route('painel.concedente.login')}}" class="btn btn-block btn-warning btn-lg">Sou concedente, Clique aqui</a>            
                        </div>
                        <!-- /.col -->
                        
                </div>
                <br>
                <div class="row">
                        <div class="col-xs-12">
                            <a href="{{route('login')}}" class="btn btn-block btn-danger btn-lg">Sou Upa, Clique aqui</a>            
                        </div>
                        <!-- /.col -->
                        
                </div>
                <br>
        </div>

        <!-- /.login-box-body -->
    </div><!-- /.login-box -->

@stop