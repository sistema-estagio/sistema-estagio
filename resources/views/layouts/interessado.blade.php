<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="UPA Estágio">
  <meta name="author" content="Creative Tim">
  <title>UPA - Estágio</title>
  <!-- Favicon -->
  <link href="{{ asset('assets/img/brand/favicon.png')}}" rel="icon" type="image/png">
  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <!-- Icons -->
  <link href="{{ asset('assets/vendor/nucleo/css/nucleo.css')}}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/@fortawesome/fontawesome-free/css/all.min.css')}}" rel="stylesheet">
  <!-- Argon CSS -->
  <link type="text/css" href="{{ asset('assets/css/argon.css?v=1.0.0')}}" rel="stylesheet">
</head>

<body>
  <!-- Main content -->
  <div class="main-content">
    <!-- Top navbar -->
    <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
      <div class="container-fluid">
        <!-- Brand -->
        <a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="{{ url('/home') }}">UPA Estágio</a>
        <!-- Form -->
        <ul class="navbar-nav align-items-center d-none d-md-flex">
          <li class="nav-item"><a class="nav-link"><i class="ni ni-bullet-list-67"></i> Vagas</a></li>
          <li class="nav-item"><a class="nav-link"><i class="ni ni-chart-pie-35"></i> Meus Processos</a></li>
          <li class="nav-item"><a class="nav-link"><i class="ni ni-favourite-28"></i> Vagas Favoritas</a></li>
        </ul>
        <!-- User -->
        <ul class="navbar-nav align-items-center d-none d-md-flex">
          <li class="nav-item dropdown">
            <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <div class="media align-items-center">
                <span class="avatar avatar-sm rounded-circle">
                  @if(Auth::user()->profile->photo != null)
                  <img src="{{ asset(Auth::user()->profile->photo) }}" class="rounded-circle">
                  @else
                  <img src="{{ asset('assets/img/theme/user.jpg') }}" class="rounded-circle">
                  @endif
                </span>
                <div class="media-body ml-2 d-none d-lg-block">
                  <span class="mb-0 text-sm  font-weight-bold">{{Auth::user()->profile->name}}</span>
                </div>
              </div>
            </a>
            <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
              <div class=" dropdown-header noti-title">
                <h6 class="text-overflow m-0"></h6>
              </div>
              <a href="{{ url('Estudante/Perfil') }}" class="dropdown-item">
                <i class="ni ni-single-02"></i>
                <span>Meu Perfil</span>
              </a>
              <a href="./examples/profile.html" class="dropdown-item">
                <i class="ni ni-chat-round"></i>
                <span>Falar com a UPA</span>
              </a>
              <div class="dropdown-divider"></div>
              <a href="{{ route('logout') }}" class="dropdown-item">
                <i class="ni ni-user-run"></i>
                <span>Sair</span>
              </a>
            </div>
          </li>
        </ul>
      </div>
    </nav>
    <!-- Header -->
    @yield('content')
      <!-- Footer -->
      <footer class="footer" style="bottom:0;width:100%;left:0;">
        <div class="row align-items-center">
          <div class="col-md-12">
            <div class="copyright text-center text-md-left text-muted">
              <center>&copy; 2018 <a href="https://www.universidadepatativa.com.br" class="font-weight-bold ml-1" target="_blank">Universidade Patativa</a><center>
            </div>
          </div>
        </div>
      </footer>
    </div>
  </div>
  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="{{ asset('assets/vendor/jquery/dist/jquery.min.js')}}"></script>
  <script src="{{ asset('assets/vendor/jquery/dist/jquery.mask.min.js')}}"></script>
  <script src="{{ asset('assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
  <!-- Optional JS -->
  <script src="{{ asset('assets/vendor/chart.js/dist/Chart.min.js')}}"></script>
  <script src="{{ asset('assets/vendor/chart.js/dist/Chart.extension.js')}}"></script>
  <!-- Argon JS -->
  <script src="{{ asset('assets/js/argon.js?v=1.0.0')}}"></script>
  @yield('js')
</body>

</html>
