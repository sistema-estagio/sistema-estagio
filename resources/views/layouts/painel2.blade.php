<!DOCTYPE html>
<html>

<head>

  <title>sysEstagio - Área Concedente</title>
  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <!-- Icons -->
  <link href="{{ asset('assets/vendor/nucleo/css/nucleo.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
  <!-- Argon CSS -->
  <link type="text/css" href="{{ asset('assets/css/argon.css') }}" rel="stylesheet">
  <link type="text/css" href="{{ asset('assets/vendor/toast/toastr.css') }}" rel="stylesheet">
  <link type="text/css" href="{{ asset('assets/vendor/datatable/datatable.css') }}" rel="stylesheet">
  @yield('css')
</head>

<body>
  <!-- Sidenav -->
  <nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main">
    <div class="container-fluid">
      <!-- Toggler -->
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"><i class="fa fa-bars"></i></span>
      </button>
      <!-- Brand -->
      <a href="{{ url('painel/concedente') }}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <!-- <span class="logo-mini"><b>U</b>PA</span> -->
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Sistema</b>Estágio</span>
      </a>
      <!-- User -->
      <ul class="nav align-items-center d-md-none">
        <li class="nav-item dropdown">
          <a class="nav-link nav-link-icon" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="ni ni-bell-55"></i>
          </a>
          <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right" aria-labelledby="navbar-default_dropdown_1">
            <a class="dropdown-item" href="#">Action</a>
            <a class="dropdown-item" href="#">Another action</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">Something else here</a>
          </div>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <div class="media align-items-center">
              <span class="avatar avatar-sm rounded-circle">

              </span>
            </div>
          </a>
          <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
            <div class=" dropdown-header noti-title">
              <h6 class="text-overflow m-0">Welcome!</h6>
            </div>
            <a href="../examples/profile.html" class="dropdown-item">
              <i class="ni ni-single-02"></i>
              <span>My profile</span>
            </a>
            <a href="../examples/profile.html" class="dropdown-item">
              <i class="ni ni-settings-gear-65"></i>
              <span>Settings</span>
            </a>
            <a href="../examples/profile.html" class="dropdown-item">
              <i class="ni ni-calendar-grid-58"></i>
              <span>Activity</span>
            </a>
            <a href="../examples/profile.html" class="dropdown-item">
              <i class="ni ni-support-16"></i>
              <span>Support</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="#!" class="dropdown-item">
              <i class="ni ni-user-run"></i>
              <span>Logout</span>
            </a>
          </div>
        </li>
      </ul>
      <!-- Collapse -->
      <div class="collapse navbar-collapse" id="sidenav-collapse-main">
        <!-- Collapse header -->
        <div class="navbar-collapse-header d-md-none">
          <div class="row">
            <div class="col-6 collapse-brand">
              <a href="{{ route('painel.concedente.dashboard') }}">

              </a>
            </div>
            <div class="col-6 collapse-close">
              <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle sidenav">
                <span></span>
                <span></span>
              </button>
            </div>
          </div>
        </div>
        <!-- Navigation -->
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="{{ route('painel.concedente.dashboard') }}">
              <i class="fa fa-tv text-primary"></i> Painel de Controle
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('painel.concedente.tces') }}">
              <i class="fa fa-list text-blue"></i> TCEs
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('painel.concedente.oportunidade.index') }}">
              <i class="fas fa-briefcase text-orange"></i> Vagas
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('painel.concedente.mediador.index') }}">
              <i class="fas fa-user-graduate text-lightblue"></i> Mediadores
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('painel.concedente.supervisor.index') }}">
              <i class="fas fa-user-graduate text-lightblue"></i> Supervisores
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('painel.concedente.folhas.index') }}">
              <i class="fas fa-check-double text-green"></i> Registro de Faltas
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('painel.concedente.financeiro') }}">
              <i class="fa fa-money-bill text-orange"></i> Folhas de Pagamento
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('painel.concedente.relatorio') }}">
              <i class="fa fa-file text-yellow"></i> Relatórios
            </a>
          </li>
          <li class="nav-item"><div class="dropdown-divider"></div></li>
          <li class="nav-item">
            <a class="nav-link" href="Javascript:void(0);" data-toggle="modal" data-target="#modalSuporte">
              <i class="fas fa-headset text-black"></i> Abrir Chamado
            </a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <div class="main-content">
    <!-- Top navbar -->
    <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
      <div class="container-fluid justify-content-end">
        <!-- User -->
        <ul class="navbar-nav align-items-center d-none d-md-flex">
          <li class="nav-item dropdown">
            <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <div class="media align-items-center">
                <span class="avatar avatar-sm rounded-circle">
                  <i class="fa fa-user"></i>
                </span>
                <div class="media-body ml-2 d-none d-lg-block">
                  <span class="mb-0 text-sm  font-weight-bold">{{Auth::user()->name}}</span>
                </div>
              </div>
            </a>
            <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
              <div class="dropdown-header noti-title">
                <h6 class="text-overflow m-0">Bem Vindo!</h6>
              </div>
              <div class="dropdown-divider"></div>
              <a href="JavaScript:void(0);" class="dropdown-item" data-toggle="modal" data-target="#modalChangePassword"><i class="fa fa-key"></i> Alterar Senha</a>
              <a href="{{ route('painel.concedente.logout') }}" class="dropdown-item" role="button" onclick="event.preventDefault();
              document.getElementById('logout-form').submit();"><i class="fas fa-sign-out-alt"></i> Sair</a>
              <form id="logout-form" action="{{ route('painel.concedente.logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
              </form>
            </div>
          </li>
        </ul>
      </div>
    </nav>
    <!-- Header -->
    @yield('content')
    <!-- Footer -->
    <footer class="footer">
      <div class="container-fluid">
        <div class="row align-items-center justify-content-xl-between">
          <div class="col-md-12">
            <div class="copyright text-center text-muted">
              &copy; 2018 <a href="http://www.universidadepatativa.com.br" class="font-weight-bold ml-1" target="_blank">sysEstagio</a>
            </div>
          </div>
        </div>
      </div>
    </footer>
  </div>
  <div class="modal" tabindex="-1" role="dialog" id="modalChangePassword">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Alterar Senha</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form id="form-post">
          <div class="modal-body">
            <div class="form-group row">
              <div class="col-md-12">
                <label for="">Senha Atual</label>
                <input type="text" name="password" class="form-control" value="" required>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-12">
                <label for="">Nova Senha</label>
                <input type="text" name="new_password" class="form-control" value="" required>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            <button type="button" class="btn btn-primary" onclick="changePassword()">Alterar</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <div class="modal" tabindex="-1" role="dialog" id="modalSuporte">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Abrir Chamado</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="JavaScript:void(0);" method="post" name="post-ticket">
          <div class="modal-body">
            <div class="form-group row">
              <div class="col-md-12">
                <label for="">Assunto</label>
                <select name="title" class="form-control" required>
                  <option value="">Selecione um assunto</option>
                  <option value="Erro em uma página">Erro em uma página</option>
                  <option value="Não consigo acessar uma página">Não consigo acessar uma página</option>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-12">
                <label for="">Descrição</label>
                <textarea name="body" class="form-control" value="" placeholder="Caso deseje, descreva mais o problema" rows="3"></textarea>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            <button type="submit" class="btn btn-primary">Enviar</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="{{ asset('assets/vendor/datatable/jquery-3.3.1.js') }}"></script>
  <script src="{{ asset('assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
  <!-- Argon JS -->
  <script src="{{ asset('assets/js/argon.js') }}"></script>
  <script src="{{ asset('assets/vendor/toast/toastr.js') }}"></script>
  <script src="{{ asset('startmin-master/js/dataTables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/datatable/datatable.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/mask/jquery.mask.min.js') }}"></script>
  <script type="text/javascript">
  toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-bottom-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  }

  $('form[name=post-ticket]').submit(function(){
    var requester = {
      'name': '{{Auth::user()->name}}',
      'email': '{{Auth::user()->email}}'
    };
    var title = $(this).find('select[name=title]').val();
    var body = $(this).find('textarea[name=body]').val();
    //
    $.ajax({
      url: 'http://localhost:8100/api/tickets',
      type: 'post',
      data: {
        'requester': requester, 'title': title, 'body': body, 'team_id': 1, 'token': '234532134'
      },
      headers: {
        token: 83
      },
      dataType: 'json',
      success: function (response) {
        if(response != null){
          console.log(response);
        }
      }
    });
    return false;
  });

  function changePassword(){
    var password = $('input[name=password]').val();
    var new_password = $('input[name=new_password]').val();
    var validate = true;

    if(password == null){
      toastr.info('A senha atual é necessário');
      validate = false;
    }

    if(new_password == null){
      toastr.info('A nova senha é necessário');
      validate = false;
    }else if(new_password.length < 8){
      toastr.info('A nova senha precisa de no mínimo 8 caracteres');
      validate = false;
    }

    if(validate == true){
      $.post("{{ route('painel.concedente.alterarSenha.submit') }}", { 'password': password, 'newpassword': new_password, '_token': '{{ csrf_token() }} '}, function(response){
        if(response != null){
          if(response.status == 'success'){
            $(this).find('input[name=password]').val('');
            $(this).find('input[name=new_password]').val('');
            toastr.success(response.msg);
            $('#modalChangePassword').modal('hide');
          }else if(response.status == 'error'){
            toastr.error(response.msg);
          }
        }else{

        }
      });
    }
  }
  </script>
  @yield('js')
</body>

</html>
