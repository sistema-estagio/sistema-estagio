@extends('adminlte::page')

@if(!empty($titulo))
  @section('title_postfix', ' - '.$titulo)
@endif

@section('content_header')
    <h1>Cursos</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Cursos</li>
    </ol>
@stop

@section('content')
    @if(session('success'))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> Alerta!</h4>
            {{session('success')}}
    </div>
    @endif

    @if(session('error'))
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Alerta!</h4>
            {{session('error')}}
    </div>
    @endif
    @if($errors->any())
        <div class="alert alert-warning alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Alerta!</h4>
            <ul class="alert-warning">
                <h3>:( Whoops, Houve algum erro! </h3>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="box box-default collapsed-box">
        <div class="box-header with-border">
          <h3 class="box-title" data-widget="collapse">Cadastrar Novo Curso</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body" style="display: none;">
          <form action="{{route('curso.store')}}" method="POST">
          {!! csrf_field() !!}
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Curso</label>
                  <input name="nmCurso" id="nmCurso" class="form-control" placeholder="Nome do Curso" type="text">
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="col-md-6">
                <div class="form-group">
                  <label>Nivel</label>
                  <select class="form-control" name="nivel_id" id="nivel_id">
                      @foreach($niveis as $nivel)
                        <option value="{{$nivel['id']}}">{{$nivel['nmNivel']}}</option>
                      @endforeach
                  </select> 
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->           
            </div>
            <!-- /.row -->
            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Cadastrar</button>
            </div>
            <!-- /.row -->
        </form>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box box-default collapsed-box -->

      <div class="row">
      <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Listagem de Cursos</h3>
              {{-- <div class="box-tools">
                <form action="{{route('curso.search')}}" method="POST">
                {!! csrf_field() !!}
                <div class="input-group input-group-sm" style="width: 200px;">
                  <input name="key_search" class="form-control pull-right" placeholder="Pesquisar" type="text">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
                </form>
              </div> --}}
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="cursos" class="table table-bordered">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Curso</th>
                  <th>Nivel</th>
                  <th>Atividades</th>
                  <!--<th style="width: 40px">Cadastro</th>-->
                  <th>Cadastro</th>
                </tr>
              </thead>
                <tbody>
                @forelse($cursos as $curso)
                <tr>
                  <td>{{$curso->id}}</td>
                  <td>{{$curso->nmCurso}}</td>
                  <td>{{$curso->Nivel->nmNivel}}</td>
                  <td><a href="{{route('curso.atividade.index', $curso->id)}}" class="btn-xs btn-info ad-click-event" title="Listar/Cadastrar Atividade"><i class="fa fa-pencil"></i> Atividades</a>
                    @php
                    $atividades = $curso->atividades->count()
                    @endphp
                    <span class="label label-primary">{{$atividades}}</span>  
                  </td>

                  <td>{{$curso->created_at->format("d/m/Y")}}</td>
                </tr>
                  @empty
                <tr>
                  <td colspan="3" align="center">
                    <span class="badge bg-red">NADA NO BANCO</span>
                  </td>
                </tr>
                @endforelse
              </tbody></table>
            </div>
            {{-- <div class="box-footer clearfix">
            @if(isset($dataForm))
            {!! $cursos->appends($dataForm)->links() !!}
            @else 
            {!! $cursos->links() !!}
            @endif
            </div> --}}
          </div>
          <!-- /.box -->
      </div>
    </div>

    @section('post-script')
    <script>
        $(function () {
          $('#cursos').DataTable({
          "pageLength": 25,
          "order": [[ 1, "asc" ]],
          "language": {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_ resultados por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar",
            "oPaginate": {
              "sNext": "Próximo",
              "sPrevious": "Anterior",
              "sFirst": "Primeiro",
              "sLast": "Último"
            },
            "oAria": {
              "sSortAscending": ": Ordenar colunas de forma ascendente",
              "sSortDescending": ": Ordenar colunas de forma descendente"
            }
          }
          })
          
        })
    </script>
    @endsection
@stop