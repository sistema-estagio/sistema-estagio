@extends('adminlte::page')

@if(!empty($titulo))
  @section('title_postfix', ' - '.$titulo)
@endif

@section('content_header')
    <h1>Atividades do Curso</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('curso.index')}}"><i class="fa fa-file-text"></i>Cursos</a></li>
        <li class="active"><i class="fa fa-pencil"></i>Atividades do Curso</li>
    </ol>
@stop

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box box-default">
			<div class="box-body">
				<table class="table table-condensed table-striped">
					<tbody>
						<tr>
							<td colspan="4">
								<label>Curso:</label> {{$ocurso->nmCurso}}</td>
							<td colspan="2">
								<label>Nível:</label> {{$ocurso->nivel->nmNivel}}</td>
						</tr>
					</tbody>
				</table>
			</div>
			<!-- /.box-body -->
		</div>
	</div>
</div>

    <div class="box box-default collapsed-box">
        <div class="box-header with-border">
          <h3 class="box-title" data-widget="collapse">Cadastrar Nova Atividade</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body" style="display: none;">
        <form action="{{route('curso.atividade.store', ['curso' => $ocurso->id])}}" method="POST">
          {!! csrf_field() !!}
            <div class="row">
              <div class="col-md-2">
                <div class="form-group">
                  <label>*Nivel da Atividade</label>
                  <select class="form-control" name="nrNivel" id="nrNivel">
                        <option value="1">Nível I</option>
                        <option value="2">Nível II</option>
                  </select> 
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="col-md-10">
                <div class="form-group">
                  <label>Atividade</label>
                  <textarea type="text" class="form-control" name="atividade" placeholder="Descrição da Atividade..." maxlength="255"></textarea>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->           
            </div>
            <!-- /.row -->
            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Cadastrar</button>
            </div>
            <!-- /.row -->
        </form>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box box-default collapsed-box -->
    <!--Session Error/Sucess -->
    @if(session('success'))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> Alerta!</h4>
            {{session('success')}}
    </div>
    @endif

    @if(session('error'))
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Alerta!</h4>
            {{session('error')}}
    </div>
    @endif
    <!--/Session Error/Sucess -->

      <div class="row">
      <div class="col-md-12">
        
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Listagem de Atividades</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            @if(count($atividades) > 0)
              <table class="table table-bordered">
                <tbody><tr>
                  <th style="width: 20px">Nível</th>
                  <th>Atividade</th>
                  <!--<th style="width: 40px">Cadastro</th>-->
                  <th style="width: 150px">Cadastro</th>
                </tr>
                @foreach($atividades as $atividade)
                <tr>
                  <td>{{$atividade->nrNivel}}</td>
                  <td>{{$atividade->atividade}}</td>
                  <td>@if($atividade->created_at) {{$atividade->created_at->format("d/m/Y H:i:s")}} @endif</td>
                </tr>
                @endforeach
              </tbody></table>
              @else 
                <span class="badge bg-red">Não existe registros no Banco de Dados.</span>
              @endif

            </div>
            <div class="box-footer clearfix">
            {{--  @if(isset($dataForm))
            {!! $cursos->appends($dataForm)->links() !!}
            @else 
            {!! $cursos->links() !!}
            @endif  --}}

            </div>
          </div>
          <!-- /.box -->
      </div>
    </div>
@stop