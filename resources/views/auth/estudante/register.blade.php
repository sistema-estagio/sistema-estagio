<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="UPA Estágio">
  <meta name="author" content="Universidade Patativa">
  <title>Painel de Estágiario - Universidade Patativa do Assaré</title>
  <!-- Favicon -->
  <link href="{{ asset('assets/img/brand/favicon.png')}}" rel="icon" type="image/png">
  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <!-- Icons -->
  <link href="{{ asset('assets/vendor/nucleo/css/nucleo.css')}}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/@fortawesome/fontawesome-free/css/all.min.css')}}" rel="stylesheet">
  <!-- Argon CSS -->
  <link type="text/css" href="{{ asset('assets/css/argon.css')}}" rel="stylesheet">
  <link type="text/css" href="{{ asset('assets/vendor/toast/toastr.css')}}" rel="stylesheet">
</head>

<body class="bg-gradient-primary">
  <div class="main-content">
    <!-- Header -->
    <div class="header bg-gradient-primary py-7 py-lg-8"></div>
    <!-- Page content -->
    <div class="container mt--8 pb-5">
      <div class="row justify-content-center">
        <div class="col-lg-12 col-md-12">
          <div class="card bg-secondary shadow border-0">
            <div class="card-body px-lg-1 py-lg-5">
              <div class="text-center text-muted mb-4" id="headerForm">
                <h3>Cadastre-se<br><small>Para criar seu cadastro na plataforma, precisamos validar alguns dados.</small></h3>
              </div>
              <br>
              <div class="text-center text-muted mb-4" id="message-email" style="display:none;">
                <div class="alert alert-warning">
                  <p>Já existe um estudante usando esse e-mail, deseja recuperar o acesso?</p>
                  <a href="" role="button" class="btn btn-default">Sim</a> <button type="button" role="button" class="btn btn-link" onclick="hideMessageEmail()">Não</button>
                </div>
              </div>
              <br>
              <form id="cadastro" role="form" method="POST" action="{{ route('painel.estudante.cadastrar.submit') }}">
                {{ csrf_field() }}
                <div class="form-group row justify-content-center mb-1" id="fieldsetCPF">
                  <div class="col-md-5 {{ $errors->has('cdCPF') ? ' has-error' : '' }}">
                    <label for="cdCPF" class="control-label">Digite seu CPF</label>
                    <div class="input-group input-group-alternative">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-badge"></i></span>
                      </div>
                      <input class="form-control cpf-mask" placeholder="000.000.000-00" type="tel" name="cdCPF" onchange="verifyCPF(this.value)" id="cpf" value="{{old('cdCPF')}}" required>
                    </div>
                    @if ($errors->has('cdCPF'))
                    <span class="help-block">
                      <strong>{{ $errors->first('cdCPF') }}</strong>
                    </span>
                    @endif
                  </div>

                  <div class="col-md-7 {{ $errors->has('nmEstudante') ? ' has-error' : '' }}">
                    <label for="nmEstudante" class="control-label">Digite seu Nome</label>
                    <div class="input-group input-group-alternative">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-single-02"></i></span>
                      </div>
                      <input class="form-control" placeholder="Seu nome" type="text " name="nmEstudante" id="nmEstudante" value="{{old('nmEstudante')}}">
                    </div>
                    @if ($errors->has('nmEstudante'))
                    <span class="help-block">
                      <div class="alert alert-warning" role="alert">
                        <strong>Erro: </strong>{{ $errors->first('nmEstudante') }}
                      </div>
                    </span>
                    @endif
                  </div>
                </div>

                <div class="row justify-content-center mb-3" id="fieldsetCPF">
                  <div class="col-md-2 {{ $errors->has('dtNascimento') ? ' has-error' : '' }}">
                    <label for="rg">Data de Nascimento</label>
                    <div class="input-group input-group-alternative">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-calendar-grid-58"></i></span>
                      </div>
                      <input class="form-control dt" type="tel" name="dtNascimento" value="{{old('dtNascimento')}}" placeholder="dd/mm/aaaa">
                    </div>
                    @if ($errors->has('dtNascimento'))
                    <span class="help-block">
                      <div class="alert alert-warning" role="alert">
                        <strong>Erro: </strong>{{ $errors->first('dtNascimento') }}
                      </div>
                    </span>
                    @endif
                  </div>
                  <div class="col-md-3 {{ $errors->has('cdRG') ? ' has-error' : '' }}">
                    <label for="cdRG">Rg</label>
                    <div class="input-group input-group-alternative">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-badge"></i></span>
                      </div>
                      <input class="form-control" type="tel" name="cdRG" value="{{old('cdRG')}}" placeholder="RG">
                    </div>
                    @if ($errors->has('cdRG'))
                    <span class="help-block">
                      <div class="alert alert-warning" role="alert">
                        <strong>Erro: </strong>{{ $errors->first('cdRG') }}
                      </div>
                    </span>
                    @endif
                  </div>
                  <div class="col-md-2 {{ $errors->has('dsOrgaoRG') ? ' has-error' : '' }}">
                    <label for="dsOrgaoRG">Orgão Expedidor</label>
                    <div class="input-group input-group-alternative">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-badge"></i></span>
                      </div>
                      <input class="form-control" type="text" name="dsOrgaoRG" value="{{old('dsOrgaoRG')}}" placeholder="SSP/CE">
                    </div>
                    @if ($errors->has('dsOrgaoRG'))
                    <span class="help-block">
                      <div class="alert alert-warning" role="alert">
                        <strong>Erro: </strong>{{ $errors->first('dsOrgaoRG') }}
                      </div>
                    </span>
                    @endif
                  </div>
                  <div class="col-md-2 {{ $errors->has('dsEstadoCivil') ? ' has-error' : '' }}">
                    <label for="cdRG">Estado Civil</label>
                    <div class="input-group input-group-alternative">
                      <select class="form-control" id="dsEstadoCivil" name="dsEstadoCivil">
                        <option value="" selected="">Selecione...</option>
                        <option value="SOLTEIRO(A)" @if (old('dsEstadoCivil') == "SOLTEIRO(A)") {{ 'selected' }} @endif>SOLTEIRO(A)</option>
                        <option value="CASADO(A)" @if (old('dsEstadoCivil') == "CASADO(A)") {{ 'selected' }} @endif>CASADO(A)</option>
                        <option value="VIUVO(A)" @if (old('dsEstadoCivil') == "VIUVO(A)") {{ 'selected' }} @endif>VIUVO(A)</option>
                        <option value="DIVORCIADO(A)" @if (old('dsEstadoCivil') == "DIVORCIADO(A)") {{ 'selected' }} @endif>DIVORCIADO(A)</option>
                        <option value="OUTRO" @if (old('dsEstadoCivil') == "OUTRO") {{ 'selected' }} @endif>OUTRO</option>
                      </select>
                    </div>
                    @if ($errors->has('dsEstadoCivil'))
                    <span class="help-block">
                      <div class="alert alert-warning" role="alert">
                        <strong>Erro: </strong>{{ $errors->first('dsEstadoCivil') }}
                      </div>
                    </span>
                    @endif
                  </div>

                  <div class="col-md-2 {{ $errors->has('dsSexo') ? ' has-error' : '' }}">
                    <label for="cdRG">Genero</label>
                    <div class="input-group input-group-alternative">
                      <select class="form-control" id="dsSexo" name="dsSexo">
                        <option value="" selected="">Selecione...</option>
                        <option value="MASCULINO" @if (old('dsSexo') == "MASCULINO") {{ 'selected' }} @endif>MASCULINO</option>
                        <option value="FEMININO" @if (old('dsSexo') == "FEMININO") {{ 'selected' }} @endif>FEMININO</option>
                      </select>
                    </div>
                    @if ($errors->has('dsSexo'))
                    <span class="help-block">
                      <div class="alert alert-warning" role="alert">
                        <strong>Erro: </strong>{{ $errors->first('dsSexo') }}
                      </div>
                    </span>
                    @endif
                  </div>
                </div>

                <div class="row justify-content-center mb-3">
                  <div class="col-md-6 {{ $errors->has('nmPai') ? ' has-error' : '' }}">
                    <label for="nmPai">Nome do Pai</label>
                    <div class="input-group input-group-alternative">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-calendar-grid-58"></i></span>
                      </div>
                      <input class="form-control" type="text" name="nmPai" value="{{old('nmPai')}}" placeholder="Nome do Pai">
                    </div>
                    @if ($errors->has('nmPai'))
                    <span class="help-block">
                      <div class="alert alert-warning" role="alert">
                        <strong>Erro: </strong>{{ $errors->first('nmPai') }}
                      </div>
                    </span>
                    @endif
                  </div>
                  <div class="col-md-5 {{ $errors->has('nmMae') ? ' has-error' : '' }}">
                    <label for="nmMae">Nome da Mãe</label>
                    <div class="input-group input-group-alternative">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-calendar-grid-58"></i></span>
                      </div>
                      <input class="form-control" type="text" name="nmMae" value="{{old('nmMae')}}" placeholder="Nome da Mãe">
                    </div>
                    @if ($errors->has('nmMae'))
                    <span class="help-block">
                      <div class="alert alert-warning" role="alert">
                        <strong>Erro: </strong>{{ $errors->first('nmMae') }}
                      </div>
                    </span>
                    @endif
                  </div>
                </div>

                <div class="text-center text-muted" id="headerForm">
                  <h3>Endereço e Contatos</h3>
                  <hr style="margin-top: 0 !important;">
                </div>
                <div class="row justify-content-center mb-3">
                  <div class="col-md-2 {{ $errors->has('cdCEP') ? ' has-error' : '' }}">
                    <label for="cdCEP">Cep</label>
                    <div class="input-group input-group-alternative">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-badge"></i></span>
                      </div>
                      <input  maxlength="9" class="form-control" type="tel" name="cdCEP" id="cdCEP" value="{{old('cdCEP')}}" placeholder="00000-000">
                    </div>
                    @if ($errors->has('cdCEP'))
                    <span class="help-block">
                      <div class="alert alert-warning" role="alert">
                        <strong>Erro: </strong>{{ $errors->first('cdCEP') }}
                      </div>
                    </span>
                    @endif
                  </div>
                  <div class="col-md-5 {{ $errors->has('dsEndereco') ? ' has-error' : '' }}">
                    <label for="dsEndereco">Endereço</label>
                    <div class="input-group input-group-alternative">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-badge"></i></span>
                      </div>
                      <input class="form-control" type="text" name="dsEndereco" id="dsEndereco" value="{{old('dsEndereco')}}" placeholder="Endereço">
                    </div>
                    @if ($errors->has('dsEndereco'))
                    <span class="help-block">
                      <div class="alert alert-warning" role="alert">
                        <strong>Erro: </strong>{{ $errors->first('dsEndereco') }}
                      </div>
                    </span>
                    @endif
                  </div>
                  <div class="col-md-1 {{ $errors->has('nnNumero') ? ' has-error' : '' }}">
                    <label for="nnNumero">Nº</label>
                    <div class="input-group input-group-alternative">
                      <input class="form-control" type="text" name="nnNumero" id="nnNumero" value="{{old('nnNumero')}}" placeholder="000">
                    </div>
                    @if ($errors->has('nnNumero'))
                    <span class="help-block">
                      <div class="alert alert-warning" role="alert">
                        <strong>E </strong>
                      </div>
                    </span>
                    @endif
                  </div>
                  <div class="col-md-3 {{ $errors->has('dsComplemento') ? ' has-error' : '' }}">
                    <label for="dsComplemento">Complemento</label>
                    <div class="input-group input-group-alternative">
                      <input class="form-control" type="text" name="dsComplemento" value="{{old('dsComplemento')}}" placeholder="Complemento">
                    </div>
                    @if ($errors->has('dsComplemento'))
                    <span class="help-block">
                      <div class="alert alert-warning" role="alert">
                        <strong>Erro: </strong>{{ $errors->first('dsComplemento') }}
                      </div>
                    </span>
                    @endif
                  </div>
                </div>

                <div class="row justify-content-center mb-3">
                  <div class="col-md-3 {{ $errors->has('nmBairro') ? ' has-error' : '' }}">
                    <label for="nmBairro">Bairro</label>
                    <div class="input-group input-group-alternative">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-badge"></i></span>
                      </div>
                      <input class="form-control" type="text" name="nmBairro" id="nmBairro" value="{{old('nmBairro')}}" placeholder="Bairro">
                    </div>
                    @if ($errors->has('nmBairro'))
                    <span class="help-block">
                      <div class="alert alert-warning" role="alert">
                        <strong>Erro: </strong>{{ $errors->first('nmBairro') }}
                      </div>
                    </span>
                    @endif
                  </div>
                  <div class="col-md-3 {{ $errors->has('estado_id') ? ' has-error' : '' }}">
                    <label for="estado_id">Estado</label>
                    <div class="input-group input-group-alternative">
                      <select class="form-control" id="estado_id" name="estado_id" required>
                        <option value="" selected="">Selecione...</option>
                        @foreach($estados as $estado)
                        <option value="{{ $estado['id'] }}" @if(old('estado_id') == $estado['id']) {{ 'selected' }} @endif>{{ $estado['nmEstado'] }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-md-5 {{ $errors->has('cidade_id') ? ' has-error' : '' }}">
                    <label for="cidade_id">Cidade</label>
                    <div class="input-group input-group-alternative">
                      <select class="form-control" id="cidade_id" name="cidade_id">
                        @if(old('estado_id'))
                        @foreach($cidades as $cidade)
                        <option value="{{ $cidade['id'] }}" @if(old('cidade_id') == $cidade['id']) {{ 'selected' }} @endif>{{ $cidade['nmCidade'] }}</option>
                        @endforeach
                        @else
                        <option value="" selected>Selecione Estado</option>
                        @endif
                      </select>
                    </div>
                    @if ($errors->has('cidade_id'))
                    <span class="help-block">
                      <div class="alert alert-warning" role="alert">
                        <strong>Erro: </strong>{{ $errors->first('cidade_id') }}
                      </div>
                    </span>
                    @endif
                  </div>
                </div>

                <div class="row justify-content-center mb-3">
                  <div class="col-md-3 {{ $errors->has('dsFone') ? ' has-error' : '' }}">
                    <label for="dsFone">Telefone</label>
                    <div class="input-group input-group-alternative">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-mobile-button"></i></span>
                      </div>
                      <input class="form-control" type="text" name="dsFone" id="dsFone" value="{{old('dsFone')}}" placeholder="(00)0000-0000">
                    </div>
                    @if ($errors->has('dsFone'))
                    <span class="help-block">
                      <div class="alert alert-warning" role="alert">
                        <strong>Erro: </strong>{{ $errors->first('dsFone') }}
                      </div>
                    </span>
                    @endif
                  </div>
                  <div class="col-md-3 {{ $errors->has('dsOutroFone') ? ' has-error' : '' }}">
                    <label for="dsOutroFone">Outro</label>
                    <div class="input-group input-group-alternative">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-mobile-button"></i></span>
                      </div>
                      <input class="form-control" type="text" name="dsOutroFone" id="dsOutroFone" value="{{old('dsOutroFone')}}" placeholder="(00)0000-0000">
                    </div>
                  </div>
                </div>
                <div class="text-center text-muted" id="headerForm">
                  <h3>Dados de Ensino</h3>
                  <hr style="margin-top: 0 !important;">
                </div>
                <div class="row justify-content-center mb-3">
                  <div class="col-md-8 {{ $errors->has('instituicao_id') ? ' has-error' : '' }}">
                    <label for="instituicao_id">Instituição de Ensino</label>
                    <div class="input-group input-group-alternative">
                      <select class="form-control" id="instituicao_id" name="instituicao_id" required>
                        @if(old('instituicao_id'))
                        @foreach($instituicoes as $instituicao)
                        <option value="{{ $instituicao['id'] }}" @if(old('instituicao_id') == $instituicao['id']) {{ 'selected' }} @endif>{{$instituicao['nmInstituicao']}}</option>
                        @endforeach
                        @else
                        <option value="" selected>Selecione uma Instituição</option>
                        @endif
                      </select>
                      <input type="hidden" name="nmInstituicao" class="form-control" value="" placeholder="Digite o nome da Instituição">
                    </div>
                    <small>Caso sua intstituição não esteja na lista acima <a href="javascript:void(0);" onclick="insertNmInstituicao(this)">clique aqui</a></small>
                    @if ($errors->has('instituicao_id'))
                    <span class="help-block">
                      <div class="alert alert-warning" role="alert">
                        <strong>Erro: </strong>{{ $errors->first('instituicao_id') }}
                      </div>
                    </span>
                    @endif
                  </div>
                  <div class="col-md-3 {{ $errors->has('nnSemestreAno') ? ' has-error' : '' }}">
                    <label for="turno_id">Semestre/Ano</label>
                    <div class="input-group input-group-alternative">
                      <input class="form-control" id="nnSemestreAno" name="nnSemestreAno" type="number" max="15" min="1" value="{{old('nnSemestreAno')}}">
                    </div>
                    @if ($errors->has('nnSemestreAno'))
                    <span class="help-block">
                      <div class="alert alert-warning" role="alert">
                        <strong>Erro: </strong>{{ $errors->first('nnSemestreAno') }}
                      </div>
                      <strong></strong>
                    </span>
                    @endif
                  </div>
                </div>

                <div class="row justify-content-center mb-3">
                  <div class="col-md-5 {{ $errors->has('curso_id') ? ' has-error' : '' }}">
                    <label for="curso_id">Curso</label>
                    <div class="input-group input-group-alternative">
                      <select class="form-control" id="curso_id" name="curso_id">
                        @if(old('curso_id'))
                        <option value="">Selecione...</option>
                        <option value="">----------</option>
                        @foreach($cursos->cursoDaInstituicao as $curso)
                        <option value="{{ $curso->id }}" @if(old('curso_id') == $curso->id) {{ 'selected' }} @endif>{{ $curso->curso->nmCurso }}</option>
                        @endforeach
                        @else
                        <option value="" selected>Selecione o curso</option>
                        @endif
                      </select>
                      <select class="form-control" id="curso_id" name="curso_id" style="display:none;">
                        <option value="">Selecione</option>
                        @foreach($cursos as $curso)
                        <option value="{{ $curso->id }}" @if(old('curso_id') == $curso->id) {{ 'selected' }} @endif>{{ $curso->curso->nmCurso }}</option>
                        @endforeach
                        <option value="Outro">Outro</option>
                      </select>
                    </div>
                    @if ($errors->has('curso_id'))
                    <span class="help-block">
                      <div class="alert alert-warning" role="alert">
                        <strong>Erro: </strong>{{ $errors->first('curso_id') }}
                      </div>
                    </span>
                    @endif
                  </div>

                  <div class="col-md-4 {{ $errors->has('nivel_id') ? ' has-error' : '' }}">
                    <label for="nivel_id">Nivel</label>
                    <div class="input-group input-group-alternative">
                      <select class="form-control" id="nivel_id" name="nivel_id">
                        @if(old('nivel_id'))
                        <option value="">Selecione...</option>
                        <option value="">----------</option>
                        @foreach($niveis as $nivel)
                        <option value="{{ $nivel->id }}" @if(old('nivel_id') == $nivel->id) {{ 'selected' }} @endif>{{ $nivel->nmNivel }}</option>
                        @endforeach
                        @else
                        <option value="" selected>Selecione um Curso</option>
                        @endif
                      </select>
                    </div>
                    @if ($errors->has('nivel_id'))
                    <span class="help-block">
                      <div class="alert alert-warning" role="alert">
                        <strong>Erro: </strong>{{ $errors->first('nivel_id') }}
                      </div>
                    </span>
                    @endif
                  </div>
                  <div class="col-md-2 {{ $errors->has('turno_id') ? ' has-error' : '' }}">
                    <label for="turno_id">Turno</label>
                    <div class="input-group input-group-alternative">
                      <select class="form-control" id="turno_id" name="turno_id">
                        @if(old('turno_id'))
                        <option value="">Selecione...</option>
                        <option value="">----------</option>
                        @foreach($turnos as $turno)
                        <option value="{{ $turno->id }}" @if(old('turno_id') == $turno->id) {{ 'selected' }} @endif>{{ $turno->nmTurno }}</option>
                        @endforeach
                        @else
                        <option value="" selected>Selecione</option>
                        @endif
                      </select>
                    </div>
                    @if ($errors->has('turno_id'))
                    <span class="help-block">
                      <div class="alert alert-warning" role="alert">
                        <strong>Erro: </strong>{{ $errors->first('turno_id') }}
                      </div>
                    </span>
                    @endif
                  </div>
                </div>

                <div class="text-center text-muted" id="headerForm">
                  <h3>Dados de Acesso</h3>
                  <hr style="margin-top: 0 !important;">
                </div>

                <div class="row justify-content-center mb-3">
                  <div class="col-md-6 {{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email">E-mail</label>
                    <div class="input-group input-group-alternative">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                      </div>
                      <input class="form-control" placeholder="" type="email" name="email" value="{{old('email')}}" >
                    </div>
                    @if ($errors->has('email'))
                    <span class="help-block">
                      <div class="alert alert-warning" role="alert">
                        <strong>Erro: </strong> {{ $errors->first('email') }}
                      </div>
                    </span>
                    @endif
                  </div>
                </div>
                <div class="row form-group justify-content-center mb-3">
                  <div class="col-md-6 {{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="passord">Senha</label>
                    <div class="input-group input-group-alternative">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-key-25"></i></span>
                      </div>
                      <input class="form-control" placeholder="" type="password" name="password" min="8" minlength="" required>
                    </div>
                    @if ($errors->has('password'))
                    <span class="help-block">
                      <div class="alert alert-warning" role="alert">
                        <strong>Erro: </strong>{{ $errors->first('password') }}
                      </div>
                    </span>
                    @endif
                  </div>
                </div>
                <div class="row form-group justify-content-center mb-3">
                  <div class="col-md-6">
                    <div class="custom-control custom-control-alternative custom-checkbox">
                      <input class="custom-control-input" id="authorize" type="checkbox" name="user_term" required>
                      <label class="custom-control-label" for="authorize">
                        <span class="text-muted">Quero me cadastrar na plataforma de estágio da Universidade Patativa </span>
                        {{-- <span class="text-muted">Concordo com o <a href="javascript:void(0);">TERMO DE USO DA PLATAFORMA</a></span> --}}
                      </label>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <center>
                      <button type="submit" class="btn btn-success my-4" id="btn-registrar">Cadastrar-se</button>
                    </center>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Footer -->
  <footer class="footer" style="background-color: #0e502d">
    <div class="container">
      <div class="row align-items-center justify-content-xl-between">
        <div class="col-md-12">
          <div class="copyright text-center text-xl-left text-muted">
            <center>&copy; 2018 <a href="http://www.universidadepatativa.com.br" target="_blank" style="color:white;">UPA</a></center>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="{{ asset('assets/vendor/jquery/dist/jquery.min.js')}}"></script>
  <script src="{{ asset('assets/vendor/jquery/dist/jquery.mask.min.js')}}"></script>
  <script src="{{ asset('assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
  <!-- Argon JS -->
  <script src="{{ asset('assets/js/argon.js?v=1.0.0')}}"></script>
  <script src="{{ asset('assets/vendor/toast/toastr.js')}}"></script>
  <!--select 2 -->
  <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
  <!--<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.css">
  <link rel="stylesheet" href="{{ asset('assets/vendor/select2.css')}}">-->
  <script>
  //cidades do estado
  $('select[name=estado_id]').change(function (){
    var idEstado = $(this).val();
    $.get('../../get-cidades/' + idEstado, function (cidades){
      $('select[name=cidade_id]').empty();
      $('#cidade_id').append('<option value=""selected="selected">Selecione a Cidade</option>');
      $('#cidade_id').append('<option value="">----------</option>');
      $.each(cidades, function (key, value){
        $('select[name=cidade_id]').append('<option value=' + value.id + '>' + value.nmCidade + '</option>');
      });
    });
  });
  //Instituicao por estado
  $('select[name=estado_id]').change(function (){
    var idEstado = $(this).val();
    $.get('../../get-instituicao-estado/' + idEstado, function (cidades){
      $('select[name=instituicao_id]').empty();
      $('#instituicao_id').append('<option value=""selected="selected">Selecione a Instituição</option>');
      $('#instituicao_id').append('<option value="">----------</option>');
      $.each(cidades, function (key, value){
        $('select[name=instituicao_id]').append('<option value=' + value.id + '>' + value.nmInstituicao + '</option>');
      });
    });
  });
  //Cursos da Instituição
  $('select[name=instituicao_id]').change(function (){
    var idInstituicao = $(this).val();

    $.get('../../get-cursos-instituicao/' + idInstituicao, function (cursos){
      $('select[name=curso_id]').empty();
      $('#curso_id').append('<option value=""selected="selected">Selecione o Curso</option>');
      $('#curso_id').append('<option value="">----------</option>');
      $.each(cursos, function (key, value){
        $('select[name=curso_id]').append('<option value=' + value.id + '>' + value.nmCurso + '</option>');
      });
    });
  });

  //Niveis do Curso da Instituição
  $('select[name=curso_id]').change(function (){
    var idCurso = $(this).val();

    $.get('../../get-nivel-curso-instituicao/' + idCurso, function (niveis){

      $('select[name=nivel_id]').empty();
      $('#nivel_id').append('<option value=""selected="selected">Selecione o Nivel</option>');
      $('#nivel_id').append('<option value="">----------</option>');
      $.each(niveis, function (key, value){
        $('select[name=nivel_id]').append('<option value=' + value.id + '>' + value.nmNivel + '</option>');
      });
    });
  });

  //Turnos do Curso na Instituição
  $('select[name=curso_id]').change(function (){
    var idCurso = $(this).val();

    $.get('../../get-turno-curso-instituicao/' + idCurso, function (turnos){
      $('select[name=turno_id]').empty();
      $('#turno_id').append('<option value=""selected="selected">Selecione o Turno</option>');
      $('#turno_id').append('<option value="">----------</option>');
      $.each(turnos, function (key, value){
        $('select[name=turno_id]').append('<option value=' + value.id + '>' + value.nmTurno + '</option>');
      });
    });
  });
  $(document).ready(function(){
    //$('#cidade_id').select2();
    //$('#instituicao_id').select2();
    //maskara nova de telefones
    var SPMaskBehavior = function (val) {
      return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    },
    spOptions = {
      onKeyPress: function(val, e, field, options) {
        field.mask(SPMaskBehavior.apply({}, arguments), options);
      }
    };

    $('#dsFone').mask(SPMaskBehavior, spOptions);
    $('#dsOutroFone').mask(SPMaskBehavior, spOptions);
  });
  var logs = {
    'invalid' : 'O CPF informado é inválido!',
    'is_existent' : 'Já existe uma conta de acesso com esse CPF!',
    'not_found' : 'Nenhum estudante registrado com esse CPF'
  };

  @if(session('status'))
  @if(session('status') != null)
  toastr['{{session("status")}}']('{{session("msg")}}');
  @endif
  @endif

  toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-center",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "5000",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  }


  function verifyCPF(cpf){
    if(cpf != null && cpf != "" && cpf.length === 14){
      $.get('../../verify/cpf/'+cpf ,function(response){
        if(response != null){
          if(response.status === true){
            $("#headerForm").html("<center>Olá, <b>"+response.nmEstudante+"</b><br><small>Você já tem um cadastro em nossa base de dados. Faça login caso já tenha acesso, ou faça seu primeiro acesso!</small> <br><a href='{{route('painel.estudante.login')}}' class='badge badge-success'>Clique Aqui!</a></center>");
            $("input[name=nome]").val(response.nmEstudante);
            // DESABILITA FORM CASO EXISTA
            $(document).ready(function() {
              $("#cadastro :input").prop("disabled", true);
            });
          }else if(response.status === false){
            toastr.warning(logs['is_existent']);
          }else if(response.status === null){
            toastr.info(logs['not_found']);
          }
        }
      });
    }else{
      toastr.error(logs['invalid']);
    }
  }

  function insertNmInstituicao(tag){
    $("select[name=instituicao_id]").hide();
    $("select[name=curso_id]").hide();
    $("input[name=nmCurso]").prop('type','text');
    $("input[name=nmInstituicao]").prop('type','text');
    tag.setAttribute('onclick','selectInstituicao(this)');
  }

  function selectInstituicao(tag){
    $("input[name=nmCurso]").prop('type','hidden');
    $("input[name=nmCurso]").val('');
    $("input[name=nmInstituicao]").prop('type','hidden');
    $("input[name=nmInstituicao]").val('');
    $("select[name=instituicao_id]").show();
    $("select[name=curso_id]").show();
    tag.setAttribute('onclick','insertNmInstituicao(this)');
  }

$(document).ready(function() {
  function limpa_formulário_cep() {
    // Limpa valores do formulário de cep.
    $("#dsEndereco").val("");
    $("#nmBairro").val("");
  }

  //Quando o campo cep perde o foco.
  $("#cdCEP").blur(function() {

    //Nova variável "cep" somente com dígitos.
    var cep = $(this).val().replace(/\D/g, '');

    //Verifica se campo cep possui valor informado.
    if (cep != "") {

      //Expressão regular para validar o CEP.
      var validacep = /^[0-9]{8}$/;

      //Valida o formato do CEP.
      if(validacep.test(cep)) {

        //Preenche os campos com "..." enquanto consulta webservice.
        $("#dsEndereco").val("...");
        $("#nmBairro").val("...");

        //Consulta o webservice viacep.com.br/
        $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

          if (!("erro" in dados)) {
            //Atualiza os campos com os valores da consulta.
            $("#dsEndereco").val(dados.logradouro);
            $("#nmBairro").val(dados.bairro);
            $("#nnNumero").focus();
          } //end if.
          else {
            //CEP pesquisado não foi encontrado.
            limpa_formulário_cep();
            alert("CEP não encontrado.");
          }
        });
      } //end if.
      else {
        //cep é inválido.
        limpa_formulário_cep();
        alert("Formato de CEP inválido.");
      }
    } //end if.
    else {
      //cep sem valor, limpa formulário.
      limpa_formulário_cep();
    }
  });
});
</script>
</body>
</html>
