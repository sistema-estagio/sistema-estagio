<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="UPA Estágio">
  <meta name="author" content="Universidade Patativa">
  <title>Login - Painel de Estágiario - Universidade Patativa do Assaré</title>
  <!-- Favicon -->
  <link href="{{ asset('assets/img/brand/favicon.png')}}" rel="icon" type="image/png">
  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <!-- Icons -->
  <link href="{{ asset('assets/vendor/nucleo/css/nucleo.css')}}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/@fortawesome/fontawesome-free/css/all.min.css')}}" rel="stylesheet">
  <!-- Argon CSS -->
  <link type="text/css" href="{{ asset('assets/css/argon.css')}}" rel="stylesheet">
  <!-- Toast CSS -->
  <link type="text/css" href="{{ asset('assets/vendor/toast/toastr.css')}}" rel="stylesheet">
</head>
<body class="bg-gradient-primary">
  <div class="main-content">
    <!-- Header -->
    <div class="header  py-7 py-lg-8">
      <div class="container">
        <div class="header-body text-center mb-7">
          <div class="row justify-content-center">
            <div class="col-lg-5 col-md-6">
              <h1 class="text-white">
                <b>Sistema</b>Estágio
              </h1>
              <p class="text-lead text-light">Plataforma de gestão de estágio</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container mt--8 pb-5">
      <div class="row justify-content-center">
        <div class="col-lg-5 col-md-5">
          <div class="card bg-secondary shadow border-0">
            <div class="card-header">
              <div class="row mt-3">
                <div class="col-md-12">
                  <center><a href="{{ route('painel.estudante.primeiro-acesso') }}" role="button" data-toggle="tooltip" data-placement="top" title="Clique para criar a sua conta">Primeiro Acesso</a></center>
                </div>
              </div>
            </div>
            <div class="card-body px-lg-5 py-lg-5">
              <div class="text-center text-muted mb-4">
                @if(session('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                  <span class="alert-inner--icon"><i class="ni ni-like-2"></i></span>
                  <span class="alert-inner--text"><strong>Sucesso!</strong><br>{{session('success')}}</span>
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                <h4>Entrar</h4>
              </div>
              <form role="form" method="POST" action="{{ route('painel.estudante.login.submit') }}">
                {{ csrf_field() }}
                <div class="form-group mb-3 {{ $errors->has('email') ? ' has-error' : '' }}">
                  <div class="input-group input-group-alternative">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="ni ni-single-02"></i></span>
                    </div>
                    <input class="form-control" placeholder="E-mail" type="text" name="email" value="{{ old('email') }}" required>
                  </div>
                  @if ($errors->has('email'))
                  <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                  </span>
                  @endif
                </div>
                <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                  <div class="input-group input-group-alternative">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                    </div>
                    <input class="form-control" placeholder="Senha" type="password" name="password" value="{{ old('password') }}" minlength="8" required>
                  </div>
                  @if ($errors->has('password'))
                  <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                  </span>
                  @endif
                </div>
                <div class="custom-control custom-control-alternative custom-checkbox">
                  <input class="custom-control-input" id=" customCheckLogin" type="checkbox" {{ old('remember') ? 'checked' : '' }}>
                  <label class="custom-control-label" for=" customCheckLogin">
                    <span class="text-muted">Lembrar-me</span>
                  </label>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <a href="#" role="button" class="float-left align-middle" style="margin-top:35px;font-size:14px;">Esqueceu sua senha?</a>
                  </div>
                  <div class="col-md-6">
                    <button type="submit" role="button" class="btn btn-success btn-lg float-right my-4">Entrar</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Footer -->
  <footer class="footer" style="background-color: #0e502d">
    <div class="container">
      <div class="row align-items-center justify-content-xl-between">
        <div class="col-md-12">
          <div class="copyright text-center text-xl-left text-muted">
            <center>&copy; 2018 <a href="http://www.universidadepatativa.com.br" target="_blank" style="color:white;">UPA</a></center>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="{{ asset('assets/vendor/jquery/dist/jquery.min.js')}}"></script>
  <script src="{{ asset('assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
  <!-- Argon JS -->
  <script src="{{ asset('assets/js/argon.js?v=1.0.0')}}"></script>
  <!-- Toastr JS -->
  <script src="{{ asset('assets/vendor/toast/toastr.js')}}"></script>
  <script type="text/javascript">
  toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-center",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "5000",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  }

  @if(session('status'))
  @if(session('status') != null)
  toastr['{{session("status")}}']('{{session("msg")}}');
  @endif
  @endif
  </script>
</body>
</html>
