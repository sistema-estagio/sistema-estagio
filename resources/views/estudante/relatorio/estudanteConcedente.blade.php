@extends('adminlte::page')

@if(!empty($titulo))
    @section('title_postfix', ' - '.$titulo)
@endif

@section('content_header')
    <h1>Estudantes</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('estudante.index')}}"><i class="fa fa-users"></i> Estudantes</a></li>
        <li class="active"><i class="fa fa-bar-chart"></i> Relatório de Estudantes</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <form action="{{route('relatorio.estudante.concedente.csv')}}" method="GET" target="_blank">
        
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="concedente_id">*Concedente</label>
                                    <select class="form-control" name="concedente_id" id="concedente_id">
                                        @foreach($Concedentes as $concedente)
                                            <option value="{{$concedente['id']}}">{{$concedente['nmRazaoSocial']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label>*Selecione o que quer ver:</label>
                                <div class="form-group">
                                    <label>
                                        <input type="checkbox"  name="dtNascimento" value="s">
                                        Data Nascimento
                                    </label>
                                    <label>
                                        <input type="checkbox" name="cdCPF" value="s">
                                        CPF
                                    </label>
                                    <label>
                                        <input type="checkbox" name="endereco" value="s">
                                        Endereço Completo
                                    </label>
                                    <label>
                                        <input type="checkbox" name="ensino" value="s">
                                        Dados de Ensino
                                    </label>
                                    <label>
                                        <input type="checkbox" name="contato" value="s">
                                        Contatos
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer text-left">
                            <button type="submit" class="btn btn-success">Gerar Excel</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.box -->
        </div>
    </div>
@section('post-script')

    <script type="text/javascript">
        $(document).ready(function(){
            $('#concedente_id').select2();
        });

    </script>
@endsection
@stop