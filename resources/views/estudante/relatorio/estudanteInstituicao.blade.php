@extends('adminlte::page')

@if(!empty($titulo))
    @section('title_postfix', ' - '.$titulo)
@endif

@section('content_header')
    <h1>Estudantes</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('estudante.index')}}"><i class="fa fa-users"></i> Estudantes</a></li>
        <li class="active"><i class="fa fa-bar-chart"></i> Relatório de Estudantes</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <form action="{{route('relatorio.estudante.instituicao.pdf')}}" method="GET" target="_blank">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="flTipo">*Instituicao</label>
                                    <select class="form-control" name="instituicao_id" id="instituicao_id">
                                        <option value="" selected>Todas Instituicao...</option>
                                        @foreach($Instituicao as $intituicao)
                                        <option value="{{$intituicao->id}}">{{$intituicao->nmInstituicao}}</option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="flTipo">*TCE</label>
                                    <select class="form-control" name="tce" id="tce" required="required">
                                        <option value="" selected>Escolha...</option>
                                        <option value="no">Sem TCE</option>
                                        <option value="ativo">TCES ativos</option>
                                        {{-- <option value="cancelado">TCES Cancelados</option> --}}
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="flTipo">Ordenação:</label>
                                        <select class="form-control" name="ordenacao">
                                                <option value="id">ID</option>
                                                <option value="nmEstudante">Nome</option>
                                        </select>
                                    </div>
                                </div>
                        </div>
                        <div class="box-footer text-left">
                            <button type="submit" class="btn btn-primary">Buscar</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.box -->
        </div>
    </div>
@section('post-script')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#instituicao_id').select2();
        });
    </script>
@endsection
@stop