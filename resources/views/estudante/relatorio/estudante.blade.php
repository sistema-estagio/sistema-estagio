@extends('adminlte::page')

@if(!empty($titulo))
    @section('title_postfix', ' - '.$titulo)
@endif

@section('content_header')
    <h1>Estudantes</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('estudante.index')}}"><i class="fa fa-users"></i> Estudantes</a></li>
        <li class="active"><i class="fa fa-bar-chart"></i> Relatório de Estudantes</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <form action="{{route('relatorio.estudante.pdf')}}" method="GET" target="_blank">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="estado_id">*Estado</label>
                                    <select class="form-control" name="estado_id" id="estado_id">
                                        <option value="" selected>Todos Estados...</option>
                                        @foreach($Estados as $estado)
                                            <option value="{{$estado['id']}}">{{$estado['nmEstado']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="cidade_id">*Cidade</label>
                                    <select class="form-control" id="cidade_id" name="cidade_id">
                                        <option value="" selected>Selecione Estado</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="curso">*Curso</label>
                                    <select class="form-control" name="curso" id="curso">
                                        <option value="">Todos</option>
                                        @foreach($Cursos as $curso)
                                            <option value="{{$curso->id}}">{{$curso->nmCurso}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="semestre">*Semestre/Ano</label>
                                    <select class="form-control" name="semestre" id="semestre">
                                        <option value="" selected>Escolha o tempo...</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                        <option value="13">13</option>
                                        <option value="14">14</option>
                                        <option value="15">15</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="idade">*Idade</label>
                                    <input type="text" class="form-control" name="idade" id="idade" />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="sexo">*Sexo</label>
                                    <select class="form-control" id="sexo" name="sexo">
                                        <option value="" selected>Selecione o Sexo</option>
                                        <option value="MASCULINO">Masculino</option>
                                        <option value="FEMININO">Feminino</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="ordenacao">Ordernar Por</label>
                                    <select class="form-control" name="ordenacao">
                                        <option value="id">ID</option>
                                        <option value="nmEstudante">Nome</option>
                                        <option value="curso_id">Curso</option>
                                        <option value="nnSemestreAno">Semestre</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer text-left">
                            <button type="submit" class="btn btn-primary">Buscar</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.box -->
        </div>
    </div>
@section('post-script')

    <script type="text/javascript">
        $(document).ready(function(){
            $('#curso').select2();
            $('#cidade_id').select2();
            $('#semestre').select2();
        });

        $('select[name=estado_id]').change(function () {
            var idEstado = $(this).val();

            $.get('../../get-cidades/' + idEstado, function (cidades) {

                $('select[name=cidade_id]').empty();
                $('select[name=cidade_id]').append('<option value="">Todos</option>');
                $.each(cidades, function (key, value) {
                    $('select[name=cidade_id]').append('<option value=' + value.id + '>' + value.nmCidade + '</option>');
                });
            });
        });

    </script>
@endsection
@stop