<!DOCTYPE html>
<html>
<head>
    <title>Relatório Estudantes</title>
    <style>
            body {
                font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif;
            }
    
            footer.fixar-rodape {
                bottom: 0;
                left: 0;
                height: 40px;
                position: fixed;
                width: 100%;
            }
    
            .novaPagina {
                page-break-before: always;
                page-break-after: always
            }
    
            div.body-content {
                /** Essa margem vai evitar que o conteudo fique por baixo do rodapé **/
                margin-bottom: 40px;
            }
    
            .col-12 {
                width: 100% !important;
            }
    
            .col-6 {
                width: 50% !important;
            }
    
            .col-6,
            .col-12 {
                float: left !important;
            }
    
            .table {
                border-collapse: separate;
                border-spacing: 0;
                min-width: 350px;
                margin-bottom: 10px;
            }
    
            .table tr th,
            .table tr td {
                border-right: 1px solid #bbb;
                border-bottom: 1px solid #bbb;
                padding: 5px;
            }
    
            .table th {
                border-top: solid 1px #bbb;
            }
    
            .table tr th:first-child,
            .table tr th:last-child {
                border-top: solid 1px #bbb;
            }
    
            .table tr th:first-child,
            .table tr td:first-child {
                border-left: 1px solid #bbb;
            }
    
            .table tr th:first-child,
            .table tr td:first-child {
                border-left: 1px solid #bbb;
            }
    
            .table tr th {
                background: #eee;
                text-align: left;
            }
    
            .table.Info tr th,
            .table.Info tr:first-child td {
                border-top: 1px solid #bbb;
            }
            /* top-left border-radius */
    
            .table tr:first-child th:first-child,
            .table.Info tr:first-child td:first-child {
                border-top-left-radius: 6px;
            }
            /* top-right border-radius */
    
            .table tr:first-child th:last-child,
            .table.Info tr:first-child td:last-child {
                border-top-right-radius: 6px;
            }
            /* bottom-left border-radius */
    
            .table tr:last-child td:first-child {
                border-bottom-left-radius: 6px;
            }
            /* bottom-right border-radius */
    
            .table tr:last-child td:last-child {
                border-bottom-right-radius: 6px;
            }
        </style>
    </head>
    
    <body>
        <div id="body">
    
            <div id="content">
    
                <div class="page" style="font-size: 7pt">
                    <table style="width: 100%;" class="header">
                        <tr>
                            <td>
                                   
                                <?php $image_path = '/img/relatorios/logo_default.png'; ?>
                               
                                <img src=" {{ asset($image_path) }}" alt="Upa - Estagio" />
                                {{--  <img src="{{ public_path() . $image_path }}" alt="Upa - Estagio" />  --}}
                            </td>
                            <td>
                                <h1 style="text-align: right">
                                    <strong>RELATÓRIO DE ESTUDANTES</strong>
                                </h1>
                            </td>
                        </tr>
                    </table>
                    <hr style=" margin-bottom: 0px; border-bottom: 1px solid #DCDCDC ;">
                    <br>

<!-- grupamento po Estado -->
<table class="table table-bordered" style="width: 100%;">
        <tr class="topo">
            <th width="1">#</th>
            <th>NOME</th>
            <th width="2">TCE</th>
            <th>INTITUIÇÃO</th>
            <th>CURSO</th>
            <th width="2">SEMESTRE/ANO</th>
        </tr>
        @php $total = 0; @endphp
        @foreach($Estudantes as $estudante)
            <tr>
                <td>{{$estudante->id}}</td>
                <td>{{$estudante->nmEstudante}}</td>
                <td style="text-align:center;">
                    @if($estudante->tce_id == null)
                        -
                    @else
                        {{$estudante->tce_id}}
                    @endif
                </td>
                <td>{{$estudante->instituicao->nmInstituicao}}</td>
                <td>{{$estudante->CursoDaInstituicao->curso->nmCurso}}</td>
                <td>{{$estudante->nnSemestreAno}}</td>
            </tr>
            @php $total++; @endphp
        @endforeach
    </table>
    <div style="margin-top:-05px;">Total de Resultados: {{$total}}</div>
<!-- FIM. Agrupamento po Estado -->
@include('relatorios.inc_rodape_html')
<div id="editor">hhhhhhhhhhhh</div>
<button type="button" id="pdfDownloader">Download</button>
</div>
</div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>

<script> 
        
        $(document).ready(function(){
            
                //pdf 	
                $("#pdfDownloader").click(function(){
            
                    html2canvas(document.getElementById("content"), {
                        onrendered: function(canvas) {
                                            
                            var imgData = canvas.toDataURL('image/jpeg');
                            
                            //console.log('Image URL: ' + imgData);
            
                            var doc = new jsPDF('p','mm','a4');
                            
                            doc.setFontSize(10);
                                                                            
                            doc.text(10, 15, 'Filter section will be printed where.')
                            
                            doc.addImage(imgData, 'jpeg', 10, 20);
                            
                            doc.save('sample.pdf');
                        }
                    });
            
                });
            });
        
</script>
</body>
</html>