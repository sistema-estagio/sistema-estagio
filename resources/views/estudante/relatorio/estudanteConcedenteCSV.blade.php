<!DOCTYPE html>
<html>
<head>
    <title>Relatório</title>
</head>
    
    <body>
           
    <table>
        <thead>
            <tr colspan="50">
                <th style="background-color: #666; color: #fff;">RELAÇÃO DE ESTÁGIARIOS POR CONCEDENTE ({{$Concedente->nmRazaoSocial}})</th>
            </tr>
        </thead>
        <thead>
        <tr>
            <th>#</th>
            <th>TCE</th>
            <th>NOME</th>
            @if($cdCPF == "s")
            <th>CPF</th>
            @endif
            @if($dtNascimento == "s")
                <th>NASCIMENTO</th>
            @endif
            @if($endereco == "s")
                <th>ENDEREÇO</th>
                <th>CEP</th>
                <th>BAIRRO</th>
                <th>CIDADE/UF</th>
            @endif
            @if($ensino == "s")
            <th>INSTITUIÇÃO DE ENSINO</th>
            <th>CURSO</th>
            <th>SEMESTRE/ANO</th>
            @endif
            @if($contato == "s")
            <th>TELEFONE</th>
            <th>OUTRO</th>
            <th>EMAIL</th>
            @endif
            {{-- <th>DT.CADASTRO</th> --}}
        </tr>
        </thead>
        <tbody>
        @php $total = 1; @endphp
        @foreach($Tces as $tce)
            <tr>
                <td>{{$total}}</td>
                <td>
                    @if($tce->migracao != NULL) 
                        {{$tce->migracao}}
                    @else 
                        {{$tce->id}}
                    @endif
                </td>
                <td>{{$tce->estudante->nmEstudante}}</td>
                {{--  se tiver escolhido cpfo exibe  --}}
                @if($cdCPF == "s")
                    <td>{{$tce->estudante->cdCPF}}</td>
                @endif
                {{--  se tiver escolhido dt nascimento exibe  --}}
                @if($dtNascimento == "s")
                    <td>{{$tce->estudante->dtNascimento->format('d/m/Y')}}</td>
                @endif
                {{--  se tiver escolhido endereco exibe  --}}
                @if($endereco == "s")
                    <td>{{$tce->estudante->dsEndereco}}, {{$tce->estudante->nnNumero}}</td>
                    <td>{{$tce->estudante->cdCEP}}</td>
                    <td>{{$tce->estudante->nmBairro}}</td>
                    <td>{{$tce->estudante->cidade->nmCidade}}/{{$tce->estudante->cidade->estado->cdUF}}</td>
                @endif
                {{--  se tiver escolhido ensino exibe  --}}
                @if($ensino == "s")
                    <td>{{$tce->estudante->instituicao->nmInstituicao}}</td>
                    <td>{{$tce->estudante->cursoDaInstituicao->curso->nmCurso}}</td>
                    <td>{{$tce->estudante->nnSemestreAno}}</td>
                @endif
                {{--  se tiver escolhido contato exibe  --}}
                @if($contato == "s")
                    <td>@if($tce->estudante->dsFone) {{$estudante->dsFone}} @else - @endif</td>
                    <td>@if($tce->estudante->dsOutroFone) {{$estudante->dsOutroFone}} @else - @endif</td>
                    <td>@if($tce->estudante->dsEmail) {{$estudante->dsEmail}} @else - @endif</td>
                @endif
                {{-- <td>{{$tce->estudante->created_at->format('d/m/Y')}}</td> --}}
            </tr>
        @php $total++; @endphp
        @endforeach
        </tbody>
    </table>
</body>
</html>