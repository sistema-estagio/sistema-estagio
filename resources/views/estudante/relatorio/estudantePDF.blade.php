<!DOCTYPE html>
<html>
<head>
    <title>Relatório Estudantes</title>
    <style>
            body {
                font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif;
            }
    
            footer.fixar-rodape {
                bottom: 0;
                left: 0;
                height: 40px;
                position: fixed;
                width: 100%;
            }
    
            .novaPagina {
                page-break-before: always;
                page-break-after: always
            }
    
            div.body-content {
                /** Essa margem vai evitar que o conteudo fique por baixo do rodapé **/
                margin-bottom: 40px;
            }
    
            .col-12 {
                width: 100% !important;
            }
    
            .col-6 {
                width: 50% !important;
            }
    
            .col-6,
            .col-12 {
                float: left !important;
            }
    
            .table {
                border-collapse: separate;
                border-spacing: 0;
                min-width: 350px;
                margin-bottom: 10px;
            }
    
        </style>
    </head>
    
    <body>
        <div id="body">
            <div id="section_header">
            </div>
    
            <div id="content">
    
                <div class="page" style="font-size: 7pt">
                    <table style="width: 100%;" class="header">
                        <tr>
                            <td>
                                <?php $image_path = '/img/relatorios/logo_default.png'; ?>
                                <img src="{{ public_path() . $image_path }}" alt="Upa - Estagio" />
                            </td>
                            <td>
                                <h1 style="text-align: right">
                                    <strong>RELATÓRIO DE ESTUDANTES</strong>
                                </h1>
                            </td>
                        </tr>
                    </table>
                    <hr style=" margin-bottom: 0px; border-bottom: 1px solid #DCDCDC ;">
                    <br>

<!-- grupamento po Estado -->
<table class="table table-bordered" style="width: 100%;">
        <tr class="topo">
            <th width="1">#</th>
            <th>NOME</th>
            <th width="2">TCE</th>
            <th>INTITUIÇÃO</th>
            <th>CURSO</th>
            <th width="2">SEMESTRE/ANO</th>
        </tr>
        @foreach($Estudantes as $estudante)
            <tr>
                <td>{{$estudante->id}}</td>
                <td>{{$estudante->nmEstudante}}</td>
                <td style="text-align:center;">
                    @if($estudante->tce_id == null)
                        -
                    @else
                        {{$estudante->tce_id}}
                    @endif
                </td>
                <td>{{$estudante->instituicao->nmInstituicao}}</td>
                <td>{{$estudante->CursoDaInstituicao->curso->nmCurso}}</td>
                <td>{{$estudante->nnSemestreAno}}</td>
            </tr>
        @endforeach
    </table>
<!-- FIM. Agrupamento po Estado -->
@include('relatorios.inc_rodape')

</div>
</div>
</div>
</body>
</html>