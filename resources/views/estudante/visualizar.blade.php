@extends('adminlte::page') 
@if(!empty($titulo))
	@section('title_postfix', ' - '.$titulo)
@endif
@section('content_header')

<h1>Estudantes</h1>
<ol class="breadcrumb">
	<li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="{{route('estudante.index')}}"><i class="fa fa-users"></i> Estudantes</a></li>
	<li class="active">
		<i class="fa fa-file"></i> Estudante: {{$estudante->id}}
	</li>
	<li>
		<a href="{{route('estudante.historico', ['estudante' => $estudante->id])}}">
			<i class="fa fa-comments"></i> Historico</a>
	</li>
</ol>
@stop 
@section('content') 
@if(session('success'))
<div class="alert alert-success alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h4>
		<i class="icon fa fa-check"></i> Alerta!</h4>
	{{session('success')}}
</div>
@endif 
@if(session('error'))
<div class="alert alert-danger alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h4>
		<i class="icon fa fa-ban"></i> Alerta!</h4>
	{{session('error')}}
</div>
@endif
<div class="row">
	<div class="col-md-12">
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">{{$estudante->nmEstudante}}</h3>
				<div class="box-tools pull-right">
					@if ($estudante->portabilidade()->count() != "0")
						<span class="label label-danger">PORTABILIDADE</span>
						Ini: {{$estudante->portabilidade->dtInicio->format('d/m/Y')}} | Fim: {{$estudante->portabilidade->dtFim->format('d/m/Y')}}
					@endif
					<b>Codigo do Estudante:</b> {{$estudante->id}}
				</div>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<table class="table table-condensed table-striped">
					<tbody>
						<tr>
							<td colspan="1"><label>CPF:</label> {{$estudante->cdCPF}}</td>
							<td colspan="1"><label>RG:</label> {{$estudante->cdRG}} {{$estudante->dsOrgaoRG}}</td>
							<td colspan="1"><label>Estado Civil:</label> {{$estudante->dsEstadoCivil}}</td>
							<td colspan="1"><label>Genero:</label> {{$estudante->dsSexo}}</td>
							<td colspan="1"><label>Nascimento:</label> {{ date('d/m/Y',strtotime($estudante->dtNascimento)) }}</td>
						</tr>
						<tr>
							<td colspan="2"><label>Pai:</label><br>{{$estudante->nmPai}}</td>
							<td colspan="3"><label>Mãe:</label><br>{{$estudante->nmMae}}</td>
						</tr>
						
						<tr>
							<td colspan="1">
								<label>Instituicao de Ensino: </label><br>
								{{$estudante->instituicao->nmInstituicao}}
							</td>
							<td colspan="1">
								<label>Curso: </label><br>
								{{$estudante->cursoDaInstituicao->curso->nmCurso}}
							</td>
							<td colspan="1">
								<label>Nível: </label><br>
								{{$estudante->cursoDaInstituicao->curso->nivel->nmNivel}}
							</td>
							<td colspan="1">
								<label>Semestre/Ano: </label><br>
								{{$estudante->nnSemestreAno}} {{$estudante->cursoDaInstituicao->cursoDuracao->nmTipoDuracao}}
							</td>
							<td colspan="1">
								<label>Turno: </label><br>
								{{$estudante->turno->nmTurno}}
							</td>
						</tr>
					
						<tr>
							<td colspan="6">
								<label>Endereço: </label>
								{{$estudante->dsEndereco}},
								<strong>N°</strong> {{$estudante->nnNumero}} 
							</td>
						</tr>
						<tr>
							<td colspan="2"><label>Bairro:</label><br>{{$estudante->nmBairro}}</td>
							<td colspan="1"><label>Cidade:</label><br>{{$estudante->cidade->nmCidade}}</td>
							<td colspan="1"><label>Estado:</label><br>{{$estudante->cidade->estado->nmEstado}}</td>
							<td colspan="1"><label>Cep:</label><br>{{$estudante->cdCEP}}</td>
						</tr>
						<tr>
							<td colspan="1"><label>Telefone:</label> {{$estudante->dsFone}}</td>
							<td colspan="2"><label>Outro:</label> {{$estudante->dsOutroFone}}</td>
							<td colspan="2"><label>E-mail:</label> {{$estudante->dsEmail}}</td>
						</tr>
						
						<tr>
							<td colspan="6">
								<label>Dados Bancários: </label><br>
								@if($estudante->dsBanco != NULL)
									<label>Banco:</label> {{$estudante->dsBanco}} <label> | Agencia:</label> {{$estudante->dsAgencia}} <label> | Conta:</label> {{$estudante->dsConta}} <label> | Favorecido:</label> {{$estudante->dsFavorecido}}
								@else
									Não Informado!
								@endif
							</td>
						</tr>
						
						<tr>
							<td colspan="6" align="right">
								<label>Cadastrado em: </label>
								{{$estudante->created_at->format("d/m/Y H:i:s")}}
								@if ($estudante->created_at->format("d/m/Y H:i:s") != $estudante->updated_at->format("d/m/Y H:i:s"))
								<br><label>Atualizado em:</label> {{$estudante->updated_at->format("d/m/Y H:i:s")}}
								@endif
								@if($estudante->user_id !== NULL)
									<br><label>Cadastrado por:</label> {{$estudante->userCadastro->name}}
								@endif
							</td>

						</tr>
					</tbody>
				</table>

			</div>
			<!-- /.box-body -->

			<div class="box-footer text-center">
				@if($estudante->tce_id == NULL)
				<a href="{{route('estudante.tce.create', ['estudante' => $estudante->id])}}" class="btn btn-primary ad-click-event">Criar TCE</a>
				@else
				 <!-- ->route('estudante.tce.show', ['estudante' => $formulario['estudante_id'],'tce' => $new->id]);-->
					<a href="{{route('tce.show', ['tce' => $estudante->tce_id])}}" class="btn btn-primary ad-click-event">Ver TCE</a>
				@endif
				<a href="{{route('estudante.edit', $estudante->id)}}" class="btn btn-warning ad-click-event">Editar</a>
				{{--  <a href="{{route('estudante.copiar', $estudante->id)}}" class="btn btn-warning ad-click-event">Teste</a>  --}}
			</div>
		</div>

	</div>
</div>
@if ($tces->count())
<h1>TCE Antigos</h1>
<div class="box box-default">
	<div class="box-body">
		<table class="table table-bordered">
			<tbody>
				<tr>
					<th style="width: 40px">TCE</th>
					<th>Concedente</th>
					<th>Cancelamento</th>
					<th>Motivo</th>
					{{--<th>TCE</th>
					<th>Ações</th>  --}}
				</tr>
				@foreach($tces as $tce)
							<tr>
								<td>{{$tce->id}}</td>
								<td>
									<a href="{{route('tce.show', ['tce' => $tce->id])}}">{{$tce->concedente->nmRazaoSocial}}</a>
								</td>
								<td>{{$tce->dtCancelamento->format('d/m/Y')}}</td>
								<td>{{$tce->motivoCancelamento->dsMotivoCancelamento}}</td>
								<!-- ->route('estudante.tce.show', ['estudante' => $formulario['estudante_id'],'tce' => $new->id]); -->
							</tr>
				@endforeach

			</tbody>
		</table>
	</div>
	<!-- /.box-body -->
</div>
@endif
@stop