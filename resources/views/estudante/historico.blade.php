@extends('adminlte::page') 

@if(!empty($titulo))
	@section('title_postfix', ' - '.$titulo)
@endif

@section('content_header')

<h1>Historico de Estudante</h1>

<ol class="breadcrumb">
	<li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="{{route('estudante.index')}}"><i class="fa fa-users"></i> Estudantes</a></li>
	<li><a href="{{route('estudante.show', ['estudante' => $estudante->id])}}"><i class="fa fa-file"></i> {{$estudante->id}}</a></li>	
	<li class="active"><i class="fa fa-comments"></i> Historico</li>
</ol>
@stop 
@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title"><b>{{$estudante->nmEstudante}}</b></h3>
				<div class="box-tools pull-right">
					<b>Codigo:</b> {{$estudante->id}}
				</div>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
					<table class="table table-bordered">
						<tbody>
							<tr>
								<th>DATA</th>
								<th>USUARIO</th>
								<th>ANTES</th>
								<th>DEPOIS</th>
							</tr>
							  	@forelse ($logs as $log)
							<tr>
								<td>{{ $log->created_at->format('d/m/Y H:i') }}</td>
								<td>{{ $log->user->name }}</td>
								{{--  <td>{{ $log->old_values }}</td>  --}}
								<td style="font-size: 10px;">
								@foreach($log->old_values as $old => $value)
									<li>{{$old}}: {{$value}}</li>
								@endforeach  
								</td>
								<td style="font-size: 10px;">
								@foreach($log->new_values as $new => $value)
									<li>{{$new}}: 
										@if(is_array($value))
										@php
										print date( 'Y-m-d' , strtotime($value['date']))
										@endphp
										@else
										{{($value)}}
										@endif
									</li>
								@endforeach
								</td>
								{{--  <td>{{ $log->new_values }}</td>  --}}
							</tr>
								@empty
							<tr>
								<td colspan="4" align="center">
								  <span class="badge bg-red">Sem LOG Registrados para este registro!</span>
								</td>
							</tr>
								@endforelse
						</tbody>
					</table>	
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
					@if(isset($dataForm)) {!! $logs->appends($dataForm)->links() !!} 
						@else {!! $logs->links() !!} 
					@endif
			</div>
		</div>

	</div>
</div>

@stop