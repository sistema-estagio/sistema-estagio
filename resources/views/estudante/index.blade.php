@extends('adminlte::page') 
@if(!empty($titulo))
	@section('title_postfix', ' - '.$titulo)
@endif
@section('content_header')
<h1>Estudantes</h1>
<ol class="breadcrumb">
	<li>
		<a href="{{route('home')}}">
			<i class="fa fa-dashboard"></i> Home</a>
	</li>
	<li class="active">
		<i class="fa fa-users"></i> Estudantes</li>
</ol>
@stop 
@section('content') 
@if(session('success'))
<div class="alert alert-success alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h4>
		<i class="icon fa fa-check"></i> Alerta!</h4>
	{{session('success')}}
</div>
@endif 
@if(session('error'))
<div class="alert alert-danger alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h4>
		<i class="icon fa fa-ban"></i> Alerta!</h4>
	{{session('error')}}
</div>
@endif
<a href="{{route('estudante.create')}}" class="btn btn-success ad-click-event">Novo Estudante</a>
</br>
</br>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Listagem de Estudantes</h3>
				{{--  --}}<div class="box-tools">
					<form action="{{route('estudante.search')}}" method="POST">
						{!! csrf_field() !!}
						<div class="input-group input-group-sm" style="width: 200px;">
							<input name="key_search" class="form-control pull-right" placeholder="Pesquisar" type="text">

							<div class="input-group-btn">
								<button type="submit" class="btn btn-default">
									<i class="fa fa-search"></i>
								</button>
							</div>
						</div>
					</form>
				</div>  

			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<table id="estudantes" class="table table-bordered table-hover">
				{{--  <table class="table table-bordered">  --}}
						<thead>
						<tr>
							<th style="width: 40px">#</th>
							<th>Nome</th>
							<th>Cidade/UF</th>
							<th class="no-sort">Cadastro</th>
							<th class="no-sort">TCE</th>
							<!--<th>Ações</th>-->
						</tr>
						</thead>
						{{-- @forelse($estudantes as $estudante)
						<tr>
							<td>{{$estudante->id}}</td>
							<td>
								<a href="{{route('estudante.show', ['estudante' => $estudante->id])}}">{{$estudante->nmEstudante}}</a>
								<br><span class="direct-chat-timestamp pull-left"><b>CPF:</b> {{$estudante->cdCPF}} / <b>RG:</b> {{$estudante->cdRG}}</span>
							</td>
							<td>{{$estudante->cidade->nmCidade}}/{{$estudante->cidade->estado->cdUF}}</td>
							<td>{{$estudante->created_at->format("d/m/Y")}}</td>
							<!-- ->route('estudante.tce.show', ['estudante' => $formulario['estudante_id'],'tce' => $new->id]); -->
							<td>
							@if($estudante->tce_id == NULL)
								<a href="{{route('estudante.tce.create', ['estudante' => $estudante->id])}}" class="btn btn-xs btn-primary ad-click-event">Criar TCE</a>
							@else
							<!-- ->route('estudante.tce.show', ['estudante' => $formulario['estudante_id'],'tce' => $new->id]); -->
								<!--<a href="{{route('estudante.tce.show', ['estudante' => $estudante->id, 'tce' => $estudante->tce_id])}}" class="btn btn-xs btn-primary ad-click-event">Ver TCE</a>-->
								<a href="{{route('tce.show', ['tce' => $estudante->tce_id])}}" class="btn btn-xs btn-success ad-click-event">Ver TCE</a>
							@endif
							</td>
							<!--<td>Ações</td>-->
						</tr>
						@empty
						<tr>
							<td colspan="5" align="center">
								<span class="badge bg-red">Sem registros no banco de dados!</span>
							</td>
						</tr>
						@endforelse --}}
				</table>
			</div>
			<div class="box-footer clearfix">

			</div>
			<!-- /.box-body -->
			{{-- <div class="box-footer clearfix">
				@if(isset($dataForm)) {!! $estudantes->appends($dataForm)->links() !!} @else {!! $estudantes->links() !!} @endif
			</div> --}}
		</div>
		<!-- /.box -->
	</div>
	@section('post-script')
    <script>
        /*$(function () {
          $('#estudantes').DataTable({
		  "columnDefs": [
		  	{ "orderable": false, "targets": 'no-sort' }
		  ],
          "pageLength": 25,
          "language": {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_ resultados por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar",
            "oPaginate": {
              "sNext": "Próximo",
              "sPrevious": "Anterior",
              "sFirst": "Primeiro",
              "sLast": "Último"
            },
            "oAria": {
              "sSortAscending": ": Ordenar colunas de forma ascendente",
              "sSortDescending": ": Ordenar colunas de forma descendente"
            }
          }
          })
          
		})*/
		$(document)
		.ready(function() {
			$.ajax({
				url: 'estudante/indexJson',
				method: 'get',
				dataType: 'json',
				success: function(data) {
					var exampleTable = $('#estudantes')
						.DataTable({
							data: data,
							'aaSorting': [[1, 'asc']],
							
							'columns': [
								{
									'data': 'id',
									'render': function(data, type, full, meta) {
										return '<button class="btn btn-primary btn-xs" id="btnOne"><i class="fa fa-edit"></i></button>';
									}
								},
								{ 'data': 'nmEstudante' },
								{ 'data': 'cidadeUF' },
								{
									//'data': 'email',
									'render': function(data, type, full, meta) {
										return '<a href="mailto:' + full.cdCPF + '?">E-Mail</a>';
									}
								},
								
								//{ 'data': 'phone' },
								{
									'render': function(data, type, full, meta) {
										return '<a href="http://' + full.cdRG + '"target=_blank">Website</a>';
									}
								},
								
							]

						});
				}
			});
		});
    </script>
    @endsection
	@stop