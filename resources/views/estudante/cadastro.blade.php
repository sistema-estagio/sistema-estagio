@extends('adminlte::page')
@if(!empty($titulo))
    @section('title_postfix', ' - '.$titulo)
@endif
@section('content_header')

    <h1>Estudantes</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('estudante.index')}}"><i class="fa fa-users"></i>Estudantes</a></li>
        <li class="active"><i class="fa fa-file"></i> Cadastro de Estudante</li>
    </ol>
@stop

@section('content')
    @if(session('success'))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> Alerta!</h4>
            {{session('success')}}
    </div>
    @endif

    @if(session('error'))
    <div class="alert alert-warning alert alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Alerta!</h4>
            {{session('error')}}
    </div>
    @endif

    @if($errors->any())
    <div class="alert alert-warning alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Alerta!</h4>
        <ul class="alert-warning">
            <h3>:( Whoops, Houve algum erro! </h3>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <div class="row">
        <div class="col-md-12">
            <!-- form start -->
            <form action="{{route('estudante.store')}}" method="POST">
            {!! csrf_field() !!}
            {{-- PORTABILIDADE --}}
            <div class="box box-warning collapsed-box">
                <div class="box-header with-border">
                    <h3 class="box-title text-yellow" data-widget="collapse">TCE PORTABILIDADE</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-plus"></i>
                        </button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body" style="display: none;">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group has-error">
                                <label class="control-label" for="migracao">
                                    <i class="fa fa-bell-o"></i> Data Inicio:
                                </label>
                                <input class="form-control" data-date-format="dd/mm/yyyy" id="dtInicio" name="dtInicio" type="text"  value="{{old('dtInicio')}}">

                            </div>
                        </div>
                        <div class="col-md-2">
                                <div class="form-group has-error">
                                    <label class="control-label" for="migracao">
                                        <i class="fa fa-bell-o"></i> Data Fim:
                                    </label>
                                    <input class="form-control" data-date-format="dd/mm/yyyy" id="dtFim" name="dtFim" type="text"  value="{{old('dtFim')}}">

                                </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer text-center">
                        <span class="help-block">*Caso o TCE deste Estudante seja de PORTABILIDADE informe as datas acima!</span>
                    </div>
            </div>
            <!-- /.box box-default collapsed-box -->

            {{-- PORTABILIDADE --}}

            <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Formulário de Cadastro</h3>
            </div>
            <!-- /.box-header -->

                <div class="box-body">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nmEstudante">*Nome</label>
                                <input class="form-control" id="nmEstudante" name="nmEstudante" placeholder="Nome do Estudante" type="text" value="{{old('nmEstudante')}}">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="cdCPF">*CPF</label>
                                <input class="form-control" id="cdCPF" name="cdCPF"placeholder="CPF" type="text" value="{{old('cdCPF')}}" onchange="verifyCPF(this.value)">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="cdRG">*RG</label>
                                <input class="form-control" id="cdRG" name="cdRG" placeholder="RG" type="text" value="{{old('cdRG')}}">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="dsOrgaoRG">*Orgão Expedição</label>
                                <input class="form-control" id="dsOrgaoRG" name="dsOrgaoRG" placeholder="SSP/CE" type="text" value="{{old('dsOrgaoRG')}}">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nmPai">Pai</label>
                                <input class="form-control" id="nmPai" name="nmPai"placeholder="Nome do Pai" type="text" value="{{old('nmPai')}}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nmMae">Mãe</label>
                                <input class="form-control" id="nmMae" name="nmMae"placeholder="Nome da Mãe" type="text" value="{{old('nmMae')}}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="dsEstadoCivil">*Estado Civil</label>
                                <select class="form-control" id="dsEstadoCivil" name="dsEstadoCivil">
                                    <option value="" selected>Selecione...</option>
                                    <option value="SOLTEIRO(A)" @if (old('dsEstadoCivil') == "SOLTEIRO(A)") {{ 'selected' }} @endif>SOLTEIRO(A)</option>
                                    <option value="CASADO(A)" @if (old('dsEstadoCivil') == "CASADO(A)") {{ 'selected' }} @endif>CASADO(A)</option>
                                    <option value="VIUVO(A)" @if (old('dsEstadoCivil') == "VIUVO(A)") {{ 'selected' }} @endif>VIUVO(A)</option>
                                    <option value="DIVORCIADO(A)" @if (old('dsEstadoCivil') == "DIVORCIADO(A)") {{ 'selected' }} @endif>DIVORCIADO(A)</option>
                                    <option value="OUTRO" @if (old('dsEstadoCivil') == "OUTRO") {{ 'selected' }} @endif>OUTRO</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="dsSexo">*Genero</label>
                                <select class="form-control" id="dsSexo" name="dsSexo">
                                    <option value="" selected>Selecione...</option>
                                    <option value="MASCULINO" @if (old('dsSexo') == "MASCULINO") {{ 'selected' }} @endif>MASCULINO</option>
                                    <option value="FEMININO" @if (old('dsSexo') == "FEMININO") {{ 'selected' }} @endif>FEMININO</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="dtNascimento">*Nascimento</label>
                                 <input data-date-format="dd/mm/yyyy" class="form-control" id="dtNascimento" name="dtNascimento" placeholder="Data Nascimento" type="text" value="{{old('dtNascimento')}}" required>
                            </div>
                        </div>
                    </div>

                    <legend>Dados de Ensino</legend>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="instituicao_id">*Instituição de Ensino</label>
                                <select class="form-control" name="instituicao_id" id="instituicao_id" required>
                                    <option value="" selected>Selecione...</option>
                                    @foreach($instituicoes as $instituicao)
                                    <option value="{{ $instituicao['id'] }}" @if(old('instituicao_id') == $instituicao['id']) {{ 'selected' }} @endif>{{$instituicao['id']}} - {{$instituicao['nmInstituicao']}}</option>

                                    <!--<option value="{{ $instituicao['id'] }}">{{$instituicao['id']}} - {{$instituicao['nmInstituicao']}}</option>-->
                                    @endforeach
                                </select>
                            </div>
                        </div>


                    </div>
                    <div class="row">
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="curso_id">*Curso</label>
                                        <select class="form-control" id="curso_id" name="curso_id" required>
                                            @if(old('curso_id'))
                                                <option value="">Selecione...</option>
                                                <option value="">----------</option>
                                                @foreach($cursos->cursoDaInstituicao as $curso)
                                                    <option value="{{ $curso->id }}" @if(old('curso_id') == $curso->id) {{ 'selected' }} @endif>{{ $curso->curso->nmCurso }}</option>
                                                @endforeach
                                            @else
                                                <option value="" selected>Selecione uma Instituição</option>
                                            @endif

                                        </select>
                                    </div>
                            </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="nivel_id">*Nivel</label>
                                <select class="form-control" id="nivel_id" name="nivel_id">
                                    @if(old('nivel_id'))
                                        <option value="">Selecione...</option>
                                        <option value="">----------</option>
                                        @foreach($niveis as $nivel)
                                            <option value="{{ $nivel->id }}" @if(old('nivel_id') == $nivel->id) {{ 'selected' }} @endif>{{ $nivel->nmNivel }}</option>
                                        @endforeach
                                    @else
                                        <option value="" selected>Selecione um Curso</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="turno_id">*Turno</label>
                                <select class="form-control" id="turno_id" name="turno_id">
                                    @if(old('turno_id'))
                                        <option value="">Selecione...</option>
                                        <option value="">----------</option>
                                        @foreach($turnos as $turno)
                                            <option value="{{ $turno->id }}" @if(old('turno_id') == $turno->id) {{ 'selected' }} @endif>{{ $turno->nmTurno }}</option>
                                        @endforeach
                                    @else
                                        <option value="" selected>Selecione um Curso</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="nnSemestreAno">*Semestre/Ano</label>
                                <input class="form-control" id="nnSemestreAno" name="nnSemestreAno" type="number"max="15" min="1" value="{{old('nnSemestreAno')}}" required>
                            </div>
                        </div>

                    </div>

                    <legend>Endereço e Contatos</legend>
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="cdCEP">*CEP</label>
                                <input class="form-control" id="cdCEP" name="cdCEP" placeholder="CEP" type="text" value="{{old('cdCEP')}}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="dsEndereco">*Endereço</label>
                                <input class="form-control" id="dsEndereco" name="dsEndereco" placeholder="Endereço" type="text" value="{{old('dsEndereco')}}">
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group">
                                <label for="nnNumero">*Numero</label>
                                <input class="form-control" id="nnNumero" name="nnNumero" placeholder="000" type="text" value="{{old('nnNumero')}}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="nmBairro">*Bairro</label>
                                <input class="form-control" id="nmBairro" name="nmBairro" placeholder="Bairro" type="text" value="{{old('nmBairro')}}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="dsComplemento">Complemento</label>
                                <input class="form-control" id="dsComplemento" name="dsComplemento" placeholder="Complemento" type="text" value="{{old('dsComplemento')}}">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="cidade_id">*Estado</label>
                                <select class="form-control" name="estado_id" id="estado_id">
                                    <option value="" selected>Selecione...</option>
                                    @foreach($estados as $estado)
                                    <option value="{{ $estado['id'] }}" @if(old('estado_id') == $estado['id']) {{ 'selected' }} @endif>{{ $estado['nmEstado'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="cidade_id">*Cidade</label>
                                <select class="form-control" id="cidade_id" name="cidade_id">
                                @if(old('estado_id'))
                                    @foreach($cidades as $cidade)
                                        <option value="{{ $cidade['id'] }}" @if(old('cidade_id') == $cidade['id']) {{ 'selected' }} @endif>{{ $cidade['nmCidade'] }}</option>
                                    @endforeach
                                @else
                                    <option value="" selected>Selecione Estado</option>
                                @endif
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="dsFone">Telefone</label>
                                <input class="form-control" id="dsFone" name="dsFone" placeholder="(88)3535-12345" type="text" value="{{old('dsFone')}}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="dsOutroFone">Outro</label>
                                <input class="form-control" id="dsOutroFone" name="dsOutroFone" placeholder="(88)98801-1234" type="text" value="{{old('dsOutro')}}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="dsEmail">E-mail</label>
                                <input class="form-control" id="dsEmail" name="dsEmail" placeholder="email@email.com.br" type="text" value="{{old('dsEmail')}}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <!--<div class="col-md-4">
                            <div class="form-group">
                                <label for="unidade_id">Unidade Upa</label>
                                <input class="form-control" id="unidade_id" name="unidade_id" placeholder="Unidade" type="text">
                            </div>
                        </div>-->
                    </div>

                    <legend>Dados Bancários</legend>
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="dsBanco">Banco</label>
                                <select class="form-control" id="dsBanco" name="dsBanco">
                                    <option value="" selected>Selecione o Banco</option>
                                    <option value="1 - Banco do Brasil">1 - Banco do Brasil</option>
                                    <option value="4 - Banco do Nordeste">4 - Banco do Nordeste</option>
                                    <option value="077-9 - Banco Inter">077-9 - Banco Inter</option>
                                    <option value="104 - Caixa">104 - Caixa</option>
                                    <option value="237 - Bradesco">237 - Bradesco</option>
                                    <option value="341 - Itaú">341 - Itaú</option>

                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="dsAgencia">Agencia</label>
                                <input class="form-control" id="dsAgencia" name="dsAgencia" placeholder="000-0" type="text" value="{{old('dsAgencia')}}">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="dsTipoConta">Tipo</label>
                                <select class="form-control" id="dsTipoConta" name="dsTipoConta">
                                    <option value="" selected>Selecione</option>
                                    <option value="CONTA POUPANÇA">Conta Poupança</option>
                                    <option value="CONTA CORRENTE">Conta Corrente</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="dsConta">Conta</label>
                                <input class="form-control" id="dsConta" name="dsConta" placeholder="00000-0" type="text" value="{{old('dsConta')}}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="dsFavorecido">Favorecido</label>
                                <input class="form-control" id="dsFavorecido" name="dsFavorecido" placeholder="Nome Completo" type="text" value="{{old('dsFavorecido')}}">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->

                    <div class="box-footer text-center">
                        <button type="submit" class="btn btn-success">Cadastrar</button>
                    </div>
                </form>
          </div>

        </div>
    </div>
@section('post-script')
<script>
    $(document).ready(function(){
        $('#cidade_id').select2();
        $('#instituicao_id').select2();
        //maskara nova de telefones
        var SPMaskBehavior = function (val) {
            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
          },
          spOptions = {
            onKeyPress: function(val, e, field, options) {
                field.mask(SPMaskBehavior.apply({}, arguments), options);
              }
          };

          $('#dsFone').mask(SPMaskBehavior, spOptions);
          $('#dsOutroFone').mask(SPMaskBehavior, spOptions);
    });

    jQuery(function($){
           $("#cdCEP").mask("99999-999");
           $("#cdCPF").mask("999.999.999-99");
           $("#dtNascimento").mask("99/99/9999");
           $("#dtInicio").mask("99/99/9999");
           $("#dtFim").mask("99/99/9999");
    });

    //Cursos da Instituição
    $('select[name=instituicao_id]').change(function (){
            var idInstituicao = $(this).val();

            $.get('../get-cursos-instituicao/' + idInstituicao, function (cursos){
                $('select[name=curso_id]').empty();
                $('#curso_id').append('<option value=""selected="selected">Selecione o Curso</option>');
                $('#curso_id').append('<option value="">----------</option>');
                $.each(cursos, function (key, value){
                    $('select[name=curso_id]').append('<option value=' + value.id + '>' + value.nmCurso + '</option>');
                });
            });
    });

    //Niveis do Curso da Instituição
    $('select[name=curso_id]').change(function (){
            var idCurso = $(this).val();

            $.get('../get-nivel-curso-instituicao/' + idCurso, function (niveis){

                $('select[name=nivel_id]').empty();
                $('#nivel_id').append('<option value=""selected="selected">Selecione o Nivel</option>');
                $('#nivel_id').append('<option value="">----------</option>');
                $.each(niveis, function (key, value){
                    $('select[name=nivel_id]').append('<option value=' + value.id + '>' + value.nmNivel + '</option>');
                });
            });
    });

    //Turnos do Curso na Instituição
    $('select[name=curso_id]').change(function (){
            var idCurso = $(this).val();

            $.get('../get-turno-curso-instituicao/' + idCurso, function (turnos){
                $('select[name=turno_id]').empty();
                $('#turno_id').append('<option value=""selected="selected">Selecione o Turno</option>');
                $('#turno_id').append('<option value="">----------</option>');
                $.each(turnos, function (key, value){
                    $('select[name=turno_id]').append('<option value=' + value.id + '>' + value.nmTurno + '</option>');
                });
            });
    });
    //cidades do estado
    $('select[name=estado_id]').change(function (){
            var idEstado = $(this).val();

            $.get('../get-cidades/' + idEstado, function (cidades){

                $('select[name=cidade_id]').empty();
                $('#cidade_id').append('<option value=""selected="selected">Selecione a Cidade</option>');
                $('#cidade_id').append('<option value="">----------</option>');
                $.each(cidades, function (key, value){
                    $('select[name=cidade_id]').append('<option value=' + value.id + '>' + value.nmCidade + '</option>');
                });
            });
    });

</script>
<!-- Adicionando Javascript -->
<script type="text/javascript" >

            $(document).ready(function() {

                function limpa_formulário_cep() {
                    // Limpa valores do formulário de cep.
                    $("#dsEndereco").val("");
                    $("#nmBairro").val("");
                }

                //Quando o campo cep perde o foco.
                $("#cdCEP").blur(function() {

                    //Nova variável "cep" somente com dígitos.
                    var cep = $(this).val().replace(/\D/g, '');

                    //Verifica se campo cep possui valor informado.
                    if (cep != "") {

                        //Expressão regular para validar o CEP.
                        var validacep = /^[0-9]{8}$/;

                        //Valida o formato do CEP.
                        if(validacep.test(cep)) {

                            //Preenche os campos com "..." enquanto consulta webservice.
                            $("#dsEndereco").val("...");
                            $("#nmBairro").val("...");

                            //Consulta o webservice viacep.com.br/
                            $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                                if (!("erro" in dados)) {
                                    //Atualiza os campos com os valores da consulta.
                                    $("#dsEndereco").val(dados.logradouro);
                                    $("#nmBairro").val(dados.bairro);
                                    
                                } //end if.
                                else {
                                    //CEP pesquisado não foi encontrado.
                                    limpa_formulário_cep();
                                    alert("CEP não encontrado.");
                                }
                            });
                        } //end if.
                        else {
                            //cep é inválido.
                            limpa_formulário_cep();
                            alert("Formato de CEP inválido.");
                        }
                    } //end if.
                    else {
                        //cep sem valor, limpa formulário.
                        limpa_formulário_cep();
                    }
                });
            });

      function verifyCPF(cpf){
        if(cpf != null){
          $.get('../estudante/verify/'+cpf, function(response) {
            if(response != null){
              if(response.status != "success"){
                alert('Já existe um estudante cadastrado com esse CPF');
              }
            }
          })
        }
      }

</script>

@endsection
@stop
