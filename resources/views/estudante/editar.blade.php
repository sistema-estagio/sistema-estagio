@extends('adminlte::page')
@if(!empty($titulo))
    @section('title_postfix', ' - '.$titulo)
@endif
@section('content_header')

    <h1>Edição de Estudantes</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('estudante.index')}}"><i class="fa fa-users"></i>Estudantes</a></li>
        <li><a href="{{route('estudante.show', ['estudante' => $estudante->id])}}"><i class="fa fa-user"></i> {{$estudante->nmEstudante}}</a></li>
        <li class="active"><i class="fa fa-pencil"></i> Edição</li>
    </ol>
@stop

@section('content')
    @if(session('success'))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> Alerta!</h4>
            {{session('success')}}
    </div>
    @endif

    @if(session('error'))
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Alerta!</h4>
            {{session('error')}}
    </div>
    @endif      
    <div class="row">
        <div class="col-md-12">
            <!-- form start -->
            <form action="{{route('estudante.update', ['estudante' => $estudante->id])}}" method="POST">
            {!! csrf_field() !!}
            {!! method_field('PUT') !!}
            {{-- PORTABILIDADE --}}
            <div class="box box-warning collapsed-box">
                    <div class="box-header with-border">
                        <h3 class="box-title text-yellow" data-widget="collapse">TCE PORTABILIDADE</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-plus"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body" style="display: none;">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group has-error">
                                    <label class="control-label" for="migracao">
                                        <i class="fa fa-bell-o"></i> Data Inicio:
                                    </label>
                                    @if($portabilidade)
                                        <input class="form-control" data-date-format="dd/mm/yyyy" id="dtInicio" name="dtInicio" type="text" value="{{ date('d/m/Y',strtotime($portabilidade->dtInicio))}}">
                                    @else
                                        <input class="form-control" data-date-format="dd/mm/yyyy" id="dtInicio" name="dtInicio" type="text" value="">                                    
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-2">
                                    <div class="form-group has-error">
                                        <label class="control-label" for="migracao">
                                            <i class="fa fa-bell-o"></i> Data Fim:
                                        </label>
                                        @if($portabilidade)
                                        <input class="form-control" data-date-format="dd/mm/yyyy" id="dtFim" name="dtFim" type="text" value="{{ date('d/m/Y',strtotime($portabilidade->dtFim))}}">
                                        @else
                                        <input class="form-control" data-date-format="dd/mm/yyyy" id="dtFim" name="dtFim" type="text" value="">                                                                            
                                        @endif
                                    </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-center">
                            <span class="help-block">*Caso o TCE deste Estudante seja de PORTABILIDADE informe as datas acima!</span>
                        </div>
                </div>
                <!-- /.box box-default collapsed-box -->
    
                {{-- PORTABILIDADE --}}
            <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">{{$estudante->nmEstudante}}</h3>
            </div>
            <!-- /.box-header -->
            
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nmEstudante">*Nome</label>
                                <input class="form-control" id="nmEstudante" name="nmEstudante" type="text" value="{{$estudante->nmEstudante}}">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="cdCPF">*CPF</label>
                                <input class="form-control" id="cdCPF" name="cdCPF" type="text" value="{{$estudante->cdCPF}}">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="cdRG">*RG</label>
                                <input class="form-control" id="cdRG" name="cdRG" type="text" value="{{$estudante->cdRG}}">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="dsOrgaoRG">*Orgão Expedição</label>
                                <input class="form-control" id="dsOrgaoRG" name="dsOrgaoRG" type="text" value="{{$estudante->dsOrgaoRG}}">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nmPai">Pai</label>
                                <input class="form-control" id="nmPai" name="nmPai" type="text" value="{{$estudante->nmPai}}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nmMae">Mãe</label>
                                <input class="form-control" id="nmMae" name="nmMae" type="text" value="{{$estudante->nmMae}}">
                            </div>
                        </div>
                    </div>
   
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="dsEstadoCivil">Estado Civil</label>
                                <select class="form-control" id="dsEstadoCivil" name="dsEstadoCivil">
                                    <option value="{{$estudante->dsEstadoCivil}}" selected>{{$estudante->dsEstadoCivil}}</option>
                                    <option value="">----------</option>
                                    <option value="SOLTEIRO(A)">SOLTEIRO(A)</option>
                                    <option value="CASADO(A)">CASADO(A)</option>
                                    <option value="VIUVO(A)">VIUVO(A)</option>
                                    <option value="DIVORCIADO(A)">DIVORCIADO(A)</option>
                                    <option value="OUTRO">OUTRO</option>
                                </select> 
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="dsSexo">Genero</label>
                                <select class="form-control" id="dsSexo" name="dsSexo">
                                    <option value="{{$estudante->dsSexo}}" selected>{{$estudante->dsSexo}}</option>
                                    <option value="">----------</option>
                                    <option value="MASCULINO">MASCULINO</option>
                                    <option value="FEMININO">FEMININO</option>
                                </select> 
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="dtNascimento">Nascimento</label>                                           
                                 <input class="form-control" id="dtNascimento" name="dtNascimento" type="text" value="{{ date('d/m/Y',strtotime($estudante->dtNascimento))}}">
                            </div>
                        </div>
                    </div>

                    <legend>Dados de Ensino</legend>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="instituicao_id">*Instituição de Ensino</label>
                                <select class="form-control" name="instituicao_id" id="instituicao_id">
                                    <option value="{{$estudante->instituicao->id}}" selected>{{$estudante->instituicao->id}} - {{$estudante->instituicao->nmInstituicao}}</option>
                                    <option value="">----------</option>
                                    @foreach($instituicoes as $instituicao)
                                    <option value="{{$instituicao['id']}}">{{$instituicao['id']}} - {{$instituicao['nmInstituicao']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="curso_id">*Curso</label>
                                        <select class="form-control" id="curso_id" name="curso_id">
                                        <option value="{{$estudante->cursoDaInstituicao->id}}" selected>{{$estudante->cursoDaInstituicao->curso->nmCurso}}</option>
                                        <option value="">----------</option>
                                        </select>
                                    </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="nivel_id">*Nivel</label>
                                <select class="form-control" id="nivel_id" name="nivel_id">
                                <option value="{{$estudante->cursoDaInstituicao->curso->nivel->id}}" selected>{{$estudante->cursoDaInstituicao->curso->nivel->nmNivel}}</option>
                                <option value="">----------</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="turno_id">*Turno</label>
                                <select class="form-control" id="turno_id" name="turno_id">
                                <option value="{{$estudante->turno->id}}" selected>{{$estudante->turno->nmTurno}}</option>
                                <option value="">----------</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="nnSemestreAno">*Semestre/Ano</label>
                                <input class="form-control" id="nnSemestreAno" name="nnSemestreAno" type="number"max="15" min="1" value="{{$estudante->nnSemestreAno}}">
                            </div>
                        </div>
                
                    </div>

                    <legend>Endereço e Contatos</legend>
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="cdCEP">*CEP</label>
                                <input class="form-control" id="cdCEP" name="cdCEP" type="text" value="{{$estudante->cdCEP}}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="dsEndereco">*Endereço</label>
                                <input class="form-control" id="dsEndereco" name="dsEndereco" type="text" value="{{$estudante->dsEndereco}}">
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group">
                                <label for="nnNumero">*Numero</label>
                                <input class="form-control" id="nnNumero" name="nnNumero" type="text" value="{{$estudante->nnNumero}}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="nmBairro">*Bairro</label>
                                <input class="form-control" id="nmBairro" name="nmBairro" type="text" value="{{$estudante->nmBairro}}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="dsComplemento">Complemento</label>
                                <input class="form-control" id="dsComplemento" name="dsComplemento" type="text" value="{{$estudante->dsComplemento}}">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="cidade_id">*Estado</label>
                                <select class="form-control" name="estado_id" id="estado_id">
                                <option value="{{$estudante->cidade->estado_id}}" selected>{{$estudante->cidade->estado->nmEstado}}</option>
                                    <option value="">----------</option>
                                    @foreach($estados as $estado)
                                    <option value="{{$estado['id']}}">{{$estado['nmEstado']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="cidade_id">*Cidade</label>
                                <select class="form-control" id="cidade_id" name="cidade_id">
                                <option value="{{$estudante->cidade->id}}" selected>{{$estudante->cidade->nmCidade}}</option>
                                <option value="">----------</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="dsFone">*Telefone</label>
                                <input class="form-control" id="dsFone" name="dsFone" type="text" value="{{$estudante->dsFone}}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="dsOutroFone">Outro</label>
                                <input class="form-control" id="dsOutroFone" name="dsOutroFone" type="text" value="{{$estudante->dsOutroFone}}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="dsEmail">*E-mail</label>
                                <input class="form-control" id="dsEmail" name="dsEmail" type="text" value="{{$estudante->dsEmail}}">
                            </div>
                        </div>
                        <!--<div class="col-md-4">
                            <div class="form-group">
                                <label for="unidade_id">Unidade Upa</label>
                                <input class="form-control" id="unidade_id" name="unidade_id" placeholder="Unidade" type="text">
                            </div>
                        </div>-->
                    </div>

                    <legend>Dados Bancários</legend>
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="dsBanco">Banco</label>
                                <select class="form-control" id="dsBanco" name="dsBanco">
                                    <option value="{{$estudante->dsBanco}}" selected>{{$estudante->dsBanco}}</option>
                                    <option value="">----------</option>
                                    <option value="1 - BANCO DO BRASIL">1 - Banco do Brasil</option>
                                    <option value="4 - BANCO DO NORDESTE">4 - Banco do Nordeste</option>
                                    <option value="077-9 - BANCO INTER">077-9 - Banco Inter</option>
                                    <option value="104 - CAIXA">104 - Caixa</option>
                                    <option value="237 - BRADESCO">237 - Bradesco</option>
                                    <option value="341 - ITAÚ">341 - Itaú</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="dsAgencia">Agencia</label>
                                <input class="form-control" id="dsAgencia" name="dsAgencia" type="text" value="{{$estudante->dsAgencia}}">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="dsTipoConta">Tipo</label>
                                <select class="form-control" id="dsTipoConta" name="dsTipoConta">
                                    <option value="{{$estudante->dsTipoConta}}" selected>{{$estudante->dsTipoConta}}</option>
                                    <option value="">----------</option>
                                    <option value="CONTA POUPANÇA">Conta Poupança</option>
                                    <option value="CONTA CORRENTE">Conta Corrente</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="dsConta">Conta</label>
                                <input class="form-control" id="dsConta" name="dsConta" type="text" value="{{$estudante->dsConta}}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="dsFavorecido">Favorecido</label>
                                <input class="form-control" id="dsFavorecido" name="dsFavorecido" type="text" value="{{$estudante->dsFavorecido}}">
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.box-body -->

                    <div class="box-footer text-center">
                        <button type="submit" class="btn btn-success">Atualizar</button>
                    </div>
                </form>
          </div>    
  
        </div>
    </div>
@section('post-script')
<script>
    $(document).ready(function(){
        $('#cidade_id').select2();
        $('#instituicao_id').select2();
        //maskara nova de telefones
        var SPMaskBehavior = function (val) {
            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
          },
          spOptions = {
            onKeyPress: function(val, e, field, options) {
                field.mask(SPMaskBehavior.apply({}, arguments), options);
              }
          };
        
          $('#dsFone').mask(SPMaskBehavior, spOptions);
          $('#dsOutroFone').mask(SPMaskBehavior, spOptions);
    });

    jQuery(function($){
           $("#cdCEP").mask("99999-999");
           $("#cdCPF").mask("999.999.999-99");
           $("#dtNascimento").mask("99/99/9999");
           $("#dtInicio").mask("99/99/9999");
           $("#dtFim").mask("99/99/9999");
    });

    //Cursos da Instituição
    $('select[name=instituicao_id]').change(function (){
            var idInstituicao = $(this).val();
          
            $.get('../../get-cursos-instituicao/' + idInstituicao, function (cursos){
                $('select[name=curso_id]').empty();
                $('#curso_id').append('<option value="0"selected="selected">Selecione o Curso</option>');
                $('#curso_id').append('<option value="0">----------</option>');
                $.each(cursos, function (key, value){
                    $('select[name=curso_id]').append('<option value=' + value.id + '>' + value.nmCurso + '</option>');
                });
            });
    });
    
    //Niveis do Curso da Instituição
    $('select[name=curso_id]').change(function (){
            var idCurso = $(this).val();
          
            $.get('../../get-nivel-curso-instituicao/' + idCurso, function (niveis){
               
                $('select[name=nivel_id]').empty();
                $('#nivel_id').append('<option value="0"selected="selected">Selecione o Nivel</option>');
                $('#nivel_id').append('<option value="0">----------</option>');
                $.each(niveis, function (key, value){
                    $('select[name=nivel_id]').append('<option value=' + value.id + '>' + value.nmNivel + '</option>');
                });
            });
    });

    //Turnos do Curso na Instituição
    $('select[name=curso_id]').change(function (){
            var idCurso = $(this).val();
          
            $.get('../../get-turno-curso-instituicao/' + idCurso, function (turnos){
                $('select[name=turno_id]').empty();
                $('#turno_id').append('<option value="0"selected="selected">Selecione o Turno</option>');
                $('#turno_id').append('<option value="0">----------</option>');
                $.each(turnos, function (key, value){
                    $('select[name=turno_id]').append('<option value=' + value.id + '>' + value.nmTurno + '</option>');
                });
            });
    });
    //cidades do estado
    $('select[name=estado_id]').change(function (){
            var idEstado = $(this).val();
          
            $.get('../../get-cidades/' + idEstado, function (cidades){
               
                $('select[name=cidade_id]').empty();
                $('#cidade_id').append('<option value="0"selected="selected">Selecione a Cidade</option>');
                $('#cidade_id').append('<option value="0">----------</option>');
                $.each(cidades, function (key, value){
                    $('select[name=cidade_id]').append('<option value=' + value.id + '>' + value.nmCidade + '</option>');
                });
            });
    });

</script>
<!-- Adicionando Javascript -->
<script type="text/javascript" >
    
            $(document).ready(function() {
    
                function limpa_formulário_cep() {
                    // Limpa valores do formulário de cep.
                    $("#dsEndereco").val("");
                    $("#nmBairro").val("");
                }
                
                //Quando o campo cep perde o foco.
                $("#cdCEP").blur(function() {
    
                    //Nova variável "cep" somente com dígitos.
                    var cep = $(this).val().replace(/\D/g, '');
    
                    //Verifica se campo cep possui valor informado.
                    if (cep != "") {
    
                        //Expressão regular para validar o CEP.
                        var validacep = /^[0-9]{8}$/;
    
                        //Valida o formato do CEP.
                        if(validacep.test(cep)) {
    
                            //Preenche os campos com "..." enquanto consulta webservice.
                            $("#dsEndereco").val("...");
                            $("#nmBairro").val("...");
    
                            //Consulta o webservice viacep.com.br/
                            $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {
    
                                if (!("erro" in dados)) {
                                    //Atualiza os campos com os valores da consulta.
                                    $("#dsEndereco").val(dados.logradouro);
                                    $("#nmBairro").val(dados.bairro);
                                } //end if.
                                else {
                                    //CEP pesquisado não foi encontrado.
                                    limpa_formulário_cep();
                                    alert("CEP não encontrado.");
                                }
                            });
                        } //end if.
                        else {
                            //cep é inválido.
                            limpa_formulário_cep();
                            alert("Formato de CEP inválido.");
                        }
                    } //end if.
                    else {
                        //cep sem valor, limpa formulário.
                        limpa_formulário_cep();
                    }
                });
            });
        </script>
@endsection
@stop