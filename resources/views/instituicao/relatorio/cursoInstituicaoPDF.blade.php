<!DOCTYPE html>
<html>

<head>
	<title>Reatorio Instituições de Ensino por Cursos</title>
	<style>
			html { margin: 10px}
			body {
				font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif;
			}
	
			footer.fixar-rodape {
				bottom: 0;
				left: 0;
				height: 40px;
				position: fixed;
				width: 100%;
			}
	
			.novaPagina {
				page-break-before: always;
				page-break-after: always
			}
	
			div.body-content {
				/** Essa margem vai evitar que o conteudo fique por baixo do rodapé **/
				margin-bottom: 40px;
			}
	
			.col-12 {
				width: 100% !important;
			}
	
			.col-6 {
				width: 50% !important;
			}
	
			.col-6,
			.col-12 {
				float: left !important;
			}
	
			.table {
				border-collapse: separate;
				border-spacing: 0;
				min-width: 350px;
				margin-bottom: 10px;
			}
	
			.table tr th,
			.table tr td {
				border-right: 1px solid #bbb;
				border-bottom: 1px solid #bbb;
				padding: 5px;
			}
	
			.table th {
				border-top: solid 1px #bbb;
			}
	
			.table tr th:first-child,
			.table tr th:last-child {
				border-top: solid 1px #bbb;
			}
	
			.table tr th:first-child,
			.table tr td:first-child {
				border-left: 1px solid #bbb;
			}
	
			.table tr th:first-child,
			.table tr td:first-child {
				border-left: 1px solid #bbb;
			}
	
			.table tr th {
				background: #eee;
				text-align: left;
			}
	
			.table.Info tr th,
			.table.Info tr:first-child td {
				border-top: 1px solid #bbb;
			}
			/* top-left border-radius */
	
			.table tr:first-child th:first-child,
			.table.Info tr:first-child td:first-child {
				border-top-left-radius: 6px;
			}
			/* top-right border-radius */
	
			.table tr:first-child th:last-child,
			.table.Info tr:first-child td:last-child {
				border-top-right-radius: 6px;
			}
			/* bottom-left border-radius */
	
			.table tr:last-child td:first-child {
				border-bottom-left-radius: 6px;
			}
			/* bottom-right border-radius */
	
			.table tr:last-child td:last-child {
				border-bottom-right-radius: 6px;
			}
		</style>
</head>

<body>
	<div id="body">
		<div id="section_header">
		</div>

		<div id="content">

			<div class="page" style="font-size: 7pt">
				<table style="width: 100%;" class="header">
					<tr>
						
						<td>
							<?php $image_path = '/img/relatorios/logo_default.png'; ?>
							<img src="{{ public_path() . $image_path }}" alt="Upa - Estagio" />
						</td>
						<td>
							<h1 style="text-align: right">
								<strong>RELATÓRIO DE INSTITUIÇÕES DE ENSINO
									<br>POR CURSOS</strong>
							</h1>
						</td>
					</tr>
				</table>
				<hr style=" margin-bottom: 0px; border-bottom: 1px solid #DCDCDC ;">

				<!-- grupamento po Estado -->
				@foreach($Cursos as $Curso)
				<div class="estado-table">
					<h2>{{$Curso->nmCurso}}</h2>
					<table class="table table-bordered" style="width: 100%;">
						<tr>
							<th>INSTITUIÇÃO</th>
							<th>CIDADE/UF</th>
							<th>DURAÇÃO</th>
							<th>TURNOS</th>
						</tr>
                        @foreach($Instituicao as $instituicao)
							@foreach($instituicao->CursoDaInstituicao->sortBy('nmCurso')->where('curso_id',$Curso->id) as $cr)
							<tr>
								<td>{{$cr->instituicao->nmInstituicao}}</td>
								<td>{{$cr->instituicao->cidade->nmCidade}}/{{$cr->instituicao->cidade->estado->cdUF}}</td>
								<td>{{$cr->qtdDuracao}}</td>
								<td>
									@forelse ($cr->turnos as $turno)
									[{{ $turno->nmTurno }}]
									@empty
									<p>-</p>
									@endforelse
								</td>
							</tr>
							@endforeach 
						@endforeach

					</table>
				</div>
				@endforeach
				<!-- FIM. Agrupamento po Estado -->

				@include('relatorios.inc_rodape')
			</div>

		</div>
	</div>
</body>

</html>