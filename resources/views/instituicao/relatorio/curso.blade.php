@extends('adminlte::page')

@if(!empty($titulo))
    @section('title_postfix', ' - '.$titulo)
@endif

@section('content_header')
    <h1>Instituições de Ensino</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('instituicao.index')}}"><i class="fa fa-university"></i> Instituições de Ensino</a></li>
        <li class="active"><i class="fa fa-bar-chart"></i> Relatório de Cursos por Instituição de Ensino</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <form action="{{route('relatorio.curso.pdf')}}" method="GET" target="_blank">
                    <div class="box-body">
                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="flTipo">*Estado</label>
                                    <select class="form-control" name="estado_id" id="estado_id">
                                        <option value="" selected>Todos Estados...</option>
                                        @foreach($Estados as $estado)
                                            <option value="{{$estado['id']}}">{{$estado['nmEstado']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="cidade_id">*Cidade</label>
                                    <select class="form-control" id="cidade_id" name="cidade_id">
                                        <option value="" selected>Selecione Estado</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer text-left">
                            <button type="submit" class="btn btn-primary">Buscar</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.box -->
        </div>
    </div>
@section('post-script')

    <script type="text/javascript">

        $('select[name=estado_id]').change(function () {
            var idEstado = $(this).val();
            $.get('../../get-cidades/' + idEstado, function (cidades) {
            //$.get('/sysEstagio/get-cidades/' + idEstado, function (cidades) {

                $('select[name=cidade_id]').empty();
                $('select[name=cidade_id]').append('<option value="">Todos</option>');
                $.each(cidades, function (key, value) {
                    $('select[name=cidade_id]').append('<option value=' + value.id + '>' + value.nmCidade + '</option>');
                });
            });
        });



    </script>
@endsection
@stop