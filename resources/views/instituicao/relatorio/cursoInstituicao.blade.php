@extends('adminlte::page')

@if(!empty($titulo))
    @section('title_postfix', ' - '.$titulo)
@endif

@section('content_header')
    <h1>Instituições de Ensino</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('instituicao.index')}}"><i class="fa fa-university"></i> Instituições de Ensino</a></li>
        <li class="active"><i class="fa fa-bar-chart"></i> Relatório de Cursos por Instituiçãos de Ensino </li>
    </ol>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <form action="{{route('relatorio.curso.instituicao.pdf')}}" method="GET" target="_blank">
                    <div class="box-body">
                        <div class="row">

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="flTipo">*Cursos</label>
                                    <select class="form-control" name="curso_id" id="curso_id">
                                        <!--<option value="" selected>Todos Cursos...</option>
                                        <option value="">- - - - - - -</option>-->
                                        @foreach($Curso as $curso)
                                            <option value="{{$curso->id}}">{{$curso->nmCurso}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="box-footer text-left">
                            <button type="submit" class="btn btn-primary">Buscar</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.box -->
        </div>
    </div>

@section('post-script')
<script>
$(document).ready(function(){
    $('#curso_id').select2();
});
</script>
@endsection
@stop