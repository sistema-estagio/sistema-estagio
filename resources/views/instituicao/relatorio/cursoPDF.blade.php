<!DOCTYPE html>
<html>
<head>
	<title>Cursos - Instituição de Ensino</title>
	<style>
		html { margin: 10px}
		body {
			font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif;
		}

		footer.fixar-rodape {
			bottom: 0;
			left: 0;
			height: 40px;
			position: fixed;
			width: 100%;
		}

		.novaPagina {
			page-break-before: always;
			page-break-after: always
		}

		div.body-content {
			/** Essa margem vai evitar que o conteudo fique por baixo do rodapé **/
			margin-bottom: 40px;
		}

		.col-12 {
			width: 100% !important;
		}

		.col-6 {
			width: 50% !important;
		}

		.col-6,
		.col-12 {
			float: left !important;
		}

		.table {
			border-collapse: separate;
			border-spacing: 0;
			min-width: 350px;
			margin-bottom: 10px;
		}

		.table tr th,
		.table tr td {
			border-right: 1px solid #bbb;
			border-bottom: 1px solid #bbb;
			padding: 5px;
		}

		.table th {
			border-top: solid 1px #bbb;
		}

		.table tr th:first-child,
		.table tr th:last-child {
			border-top: solid 1px #bbb;
		}

		.table tr th:first-child,
		.table tr td:first-child {
			border-left: 1px solid #bbb;
		}

		.table tr th:first-child,
		.table tr td:first-child {
			border-left: 1px solid #bbb;
		}

		.table tr th {
			background: #eee;
			text-align: left;
		}

		.table.Info tr th,
		.table.Info tr:first-child td {
			border-top: 1px solid #bbb;
		}
		/* top-left border-radius */

		.table tr:first-child th:first-child,
		.table.Info tr:first-child td:first-child {
			border-top-left-radius: 6px;
		}
		/* top-right border-radius */

		.table tr:first-child th:last-child,
		.table.Info tr:first-child td:last-child {
			border-top-right-radius: 6px;
		}
		/* bottom-left border-radius */

		.table tr:last-child td:first-child {
			border-bottom-left-radius: 6px;
		}
		/* bottom-right border-radius */

		.table tr:last-child td:last-child {
			border-bottom-right-radius: 6px;
		}
	</style>
</head>

<body>

	<div id="body">
		<div id="content">

			<div class="page" style="font-size: 7pt">
				<table style="width: 100%;" class="header">
					<tr>
						<td>
							<?php $image_path = '/img/relatorios/logo_default.png'; ?>
							<img src="{{ public_path() . $image_path }}" alt="Upa - Estagio" />
						</td>
						<td>
							<h1 style="text-align: right">
								<strong>INSTITUIÇÕES DE ENSINO<br>RELATÓRIO DE CURSOS</strong>
							</h1>
						</td>
					</tr>
				</table>
				<hr style=" margin-bottom: 0px; border-bottom: 1px solid #DCDCDC ;">

				<!-- grupamento po Estado -->
				@foreach($Estados as $estado) @if($estado->instituicao->count() > 0)
				<div class="estado-table">
					<h2><strong>{{$estado->nmEstado}}</strong></h2>

					@if($cidadeGet > 0 )

						@foreach($estado->cidades->where('id',$cidadeGet) as $cidadeGET)

							<div class="cidades">
								<h4> - {{$cidadeGET->nmCidade}}</h4>
								<table class="table table-bordered table-condensed" style="width: 100%;">

									@foreach($cidadeGET->instituicao->sortBy('nmInstituicao') as $instituicao)
										<tr class="topo">
											<th class="text-center" colspan="2">({{$instituicao->id}}){{$instituicao->nmInstituicao}}</th>
										</tr>
										@forelse($instituicao->CursoDaInstituicao as $cursoIntituicao)
											<tr>
												<td>{{$cursoIntituicao->curso->nmCurso}}</td>
												<td style="width: 20%">
													{{$cursoIntituicao->qtdDuracao}}
													@if($cursoIntituicao->curso_duracao_id == 1)
														@if($cursoIntituicao->qtdDuracao == 1)
															Ano
														@else
															Anos
														@endif
													@elseif($cursoIntituicao->curso_duracao_id == 2)
														@if($cursoIntituicao->qtdDuracao == 1)
															Semestre
														@else
															Semestres
														@endif
													@else
														Meses
													@endif
												</td>
											</tr>
										@endforeach
									@empty
											<tr>
												<td colspan="3"> Sem Resultado</td>
											</tr>
									@endforelse
								</table>
							</br>
							</div>
						@endforeach

					@else

						@foreach($estado->instituicao->sortBy('cidade_id')->groupBy('cidade_id') as $Instituicao)
							<div class="cidades">
								@foreach($Instituicao->slice(0,1) as $cidade)
									<h4><strong>{{$cidade->cidade->nmCidade}}</strong></h4>
								@endforeach
								<table class="table table-bordered table-condensed" style="width: 100%;">
									@foreach($Instituicao->sortBy('nmInstituicao') as $instituicao)
										<tr class="topo">
											<th class="text-center" colspan="2">[ {{$instituicao->id}} ] {{$instituicao->nmInstituicao}}</th>
										</tr>
										@foreach($instituicao->CursoDaInstituicao as $cursoIntituicao)
											<tr>
												<td>{{$cursoIntituicao->curso->nmCurso}}</td>
												<td style="width: 20%">
													{{$cursoIntituicao->qtdDuracao}}
													@if($cursoIntituicao->curso_duracao_id == 1)
														@if($cursoIntituicao->qtdDuracao == 1)
															Ano
														@else
															Anos
														@endif
													@elseif($cursoIntituicao->curso_duracao_id == 2)
														@if($cursoIntituicao->qtdDuracao == 1)
															Semestre
														@else
															Semestres
														@endif
													@else
														Meses
													@endif
												</td>
											</tr>
										@endforeach
										
									@endforeach
								</table>
							</br>
							</div>
						@endforeach

					@endif

				</div>
				@endif @endforeach
				<!-- FIM. Agrupamento po Estado -->
				@include('relatorios.inc_rodape')
			</div>

		</div>
	</div>
</body>

</html>