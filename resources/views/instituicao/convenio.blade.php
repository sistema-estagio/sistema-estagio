<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Reatorio Convenio</title>

	<!-- Bootstrap 4-ONLINE -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb"
	 crossorigin="anonymous">
	<style>
		body {
			font-size: 13px;
		}
        
        #rodape {
        height: 50px; /*ajusta a altura do rodapé*/
        margin-top: -50px;/*estes números devem ser opostos*/
        }
		[class*="texto"] {
			font-size: 10px;
		}

		@media print {
			#bnt_print {
				display: none;
			}
		}
	</style>
</head>

<body>
	<div class="container">
		<div class="row">
			<div class="col-3">
				<img src="{{URL::asset('img/relatorios/logo_default.png')}}" alt="Logomarca Universidade Patativa" />
			</div>

			<div class="col-9">
				<h5 style="margin-top: 50px; float: right">Convênio nº{{$instituicao->id}}/{{$id_convenio}}</h5>
			</div>
		</div>

		<h3>
			<strong>INSTITUIÇÃO DE ENSINO</strong>
		</h3>

		<div class="row">
			<div class="col-12">
				<strong>Razão Social:</strong> {{$instituicao->nmInstituicao}}
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<strong>Nome Fantasia:</strong> {{$instituicao->nmFantasia}}
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<strong>CNPJ:</strong> {{$instituicao->cdCNPJ}}
			</div>
		</div>
		<div class="row">
			<div class="col-6">
				<strong>Endereço:</strong> {{$instituicao->dsEndereco}}, N° {{$instituicao->nnNumero}}
			</div>
		</div>
		<div class="row">
			<div class="col-3">
				<strong>Bairro:</strong> {{$instituicao->nmBairro}}
			</div>
			<div class="col-3">
				<strong>Cidade:</strong> {{$instituicao->cidade->nmCidade}}
			</div>
			<div class="col-3">
				<strong>Estado:</strong> {{$instituicao->cidade->estado->nmEstado}}
			</div>
			<div class="col-3">
				<strong>Cep:</strong> {{$instituicao->cdCEP}}
			</div>
		</div>
		<div class="row">
			<div class="col-4">
				<strong>Telefone:</strong> {{$instituicao->dsFone}}
			</div>
			<div class="col-4">
				<strong>Outro:</strong>
				@if($instituicao->dsOutro) {{$instituicao->dsOutro}} @else Não Informado @endif
			</div>
			<div class="col-4">
				<strong>E-mail:</strong>
				@if($instituicao->dsEmail) {{$instituicao->dsEmail}} @else Não Informado @endif
			</div>
		</div>

		<h4>
			<strong>AGENTE DE INTEGRAÇÃO</strong>
		</h4>
		<div class="row">
			<div class="col-6">
				<strong>Razão Social:</strong> UNIVERSIDADE PATATIVA DO ASSARÉ</div>

			<div class="col-6">
				<strong>Cnpj:</strong> 05.342.580/0001-19</div>
		</div>
		<div class="row">
			<div class="col-6">
				<strong>Endereco:</strong> RUA MONSENHOR ESMERALDO, N°:36</div>
		</div>
		<div class="row">
			<div class="col-3">
				<strong>Bairro:</strong> FRANCISCANOS</div>
			<div class="col-3">
				<strong>Cidade:</strong> JUAZEIRO DO NORTE</div>
			<div class="col-3">
				<strong>Estado:</strong> CEARÁ</div>
			<div class="col-3">
				<strong>Cep:</strong> 63020-020</div>
		</div>
		<div class="row">
			<div class="col-4">
				<strong>Telefone:</strong> (88) 3512-2450</div>
			<div class="col-4">
				<strong>Outro:</strong> Não Informado</div>
			<div class="col-4">
				<strong>E-mail: </strong>estagio@universidadepatativa.com.br</div>
		</div>
		<div class="row">
			<div class="col-6">
				<strong>Representante:</strong> FRANCISCO PALACIO LEITE</div>
			<div class="col-6">
				<strong>Cargo:</strong> DIRETOR PRESIDENTE</div>
		</div>
		<div class="row">
			<div class="col-6">
				<strong>Resp. Administrativo:</strong> FRANCISCO PALACIO LEITE</div>
			<div class="col-6">
				<strong>Cargo:</strong> DIRETOR PRESIDENTE</div>
		</div>
		{{-- </div> --}}
	
		<div class="row justify-content-center">
			<div class="col-10">
				<p>Resolvem celebrar entre si este Termo de Compromisso de Estágio, que se regerá pelas seguintes cláulas e condições:</p>
			</div>
		</div>
    <div class="texto">
		<div class="row" id="cl1">
			<div class="col-12">
				<p class="termo">
					<strong>CLÁUSULA 1ª - </strong>Este convênio estabelece cooperação entre as partes, visando o desenvolvimento de atividades
					conjuntas, que propicie a “promoção da integração ao mercado de trabalho”, a “formação para o trabalho” de acordo com
					a Constituição Federal vigente (Art. 203. item III e 214. item IV), através da operacionalização de programa de Estágio
					de Estudantes.
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<p class="termo">
					<strong>CLÁUSULA 2ª - </strong>O Estágio para estudantes de cursos do ensino superior e de cursos de educação profissional,
					de ensino médio, da educação especial e dos anos finais do ensino fundamental, na modalidade profissional da educação
					de jovens e adultos, obrigatório ou não, caracteriza-se como uma possibilidade do estudante ser colocado em situações
					reais de vida e de trabalho, junto às pessoas de direito público e privado, em atividades de aprendizagem social, profissional
					e cultural compatíveis com os respectivos Cursos de Formação, de acordo com o disposto na Lei nº 11.788, de 25 de setembro
					de 2008.</p>
			</div>
		</div>
		<div class="row">
			<div class="col-12 ">
				<strong>CLÁUSULA 3ª - Das obrigações da Instituição de Ensino</strong>
				<ol class="margin">
					<li>Fornecer ao Agente de Integração, quando solicitado, relação de alunos por Curso que mantém informações e requisitos
						mínimos para a realização dos Estágios de Estudantes, bem como informações sobre adequação do estagio a proposta pedagógica
						do curso descrito no Plano de Estágio Integrado a Proposta Pedagógica do Curso que contenham as condições e requisitos
						suficientes à exigência legal de adequação à etapa e modalidade da formação escolar dos estudantes sempre que necessário;</li>
					<li>Propiciar condições que facilitem e agilizem a inclusão de seus estudantes no cadastro do Agente de Integração de candidatos
						de estágios;</li>
					<li>Divulgar junto a seus estudantes, quando for o caso, as oportunidades de estágio captadas pelo Agente de Integração;</li>
					<li>Assinar como interveniente, os Termos de Compromissos de Estágios que vieram a ser celebrados entre seus estudantes
						e as unidades concedentes de estágios conveniadas com o Agente de Integração;</li>
					<li>Confirmar a situação escolar dos estagiários, alunos da Instituição de Ensino, quando solicitado;</li>
					<li>Supervisionar e avaliar o desenvolvimento do estágio de seus estudantes, através de relatórios e/ou contato por intermédio
						do Agente de Integração;</li>
					<li>Validar semestralmente, o Plano de Atividade de Estagio compatibilizando as atividades com a etapa da formação escolar
						do estagiário bem como atualizar informações cadastrais, áreas profissionais e de atuação de seus cursos sempre que
						necessário através dos Termos Aditivos;</li>
					<li>Atualizar o Plano de Estagio Integrado a Proposta Pedagógica do Curso, sempre que necessário;</li>
					<li>Informar os casos de rescisões, por iniciativa da Instituição de Ensino, dos Termos de Compromissos de Estagio de seus
						alunos;</li>
					<li>Avaliar as instalações da parte concedente do estágio e sua adequação à formação cultural e profissional do educando;</li>
					<li>Indicar professor orientador, da área a ser desenvolvida no estágio, como responsável pelo acompanhamento e avaliação
						das atividades do estagiário; </li>
					<li>Exigir do educando a apresentação periódica, em prazo não superior a 6 (seis) meses, de relatório das atividades; </li>
					<li>Comunicar o estudante, no início do período letivo, as datas de realização de avaliações escolares ou acadêmicas. </li>
				</ol>
			</div>
		</div>

		<div class="row" id="cl4">
			<div class="col-12">
				<strong>CLÁUSULA 4ª - Das obrigações da UNIVERSIDADE PATATIVA DO ASSARÉ - UPA </strong>
				<ol class="margin">
					<li>Desenvolver esforços para captar oportunidades de estágios, obtendo das unidades concedentes a identificação e características
						dos programas e das oportunidades a serem concedidas;</li>
					<li>Promover o ajuste das condições de estágios definidas pela Instituição de Ensino com as disponibilidades da concedente,
						indicando as principais atividades a serem desenvolvidas pelos estagiários, observando sua compatibilidade com o contexto
						básico da profissão ao qual o curso se refere;</li>
					<li>Cadastrar os estudantes da Instituição de Ensino, candidatos a estágios;</li>
					<li>Encaminhar às unidades concedentes os estudantes cadastrados e interessados nas oportunidades de estágio;</li>
					<li>Preparar toda a documentação legal referente ao estágio, incluindo:</li>
					<li>Termo de Compromisso de Estágio – TCE, entre a Unidade Concedente e o estudante, com interveniência e assinatura da
						Instituição de Ensino;</li>
					<li>Efetivação do seguro contra acidentes pessoais em favor do estagiário;</li>
					<li>Acompanhar a realização dos estágios junto às unidades concedentes, subsidiando a Instituição de Ensino com informações
						pertinentes;</li>
					<li>Colocar a disposição da Instituição de ensino relatórios informativos contendo;</li>
					<li>Total de estudantes em estágios;</li>
					<li>Total de estudantes cadastrados no Agente de Integração, candidatos;</li>
					<li>Total de estagiários por curso, indicando as concedentes e a vigência dos Termos de Compromisso de Estágio;</li>
					<li>Informações sobre rescisões de Termo de Compromisso de Estágio solicitada pela Concedente, bem como o Termo de realização
						de Estágio, com indicação resumida das atividades desenvolvidas dos períodos e da avaliação de desempenho pela concedente
						por ocasião do desligamento do estagiário;</li>
					<li>Disponibilizar à Instituição de Ensino com informações sobre instalações da parte Concedente;</li>
					<li>Fornecer à Instituição de Ensino as informações necessárias para o cumprimento pelo estagiário da apresentação semestral
						do relatório de estagio, contendo descrição das atividades do estagio;</li>
				</ol>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<p class="termo">
					<strong>CLÁUSULA 5ª - </strong>As ações da Universidade Patativa do Assaré - UPA na cláusula 4ª, não implicarão em quaisquer
					ônus para a Instituição de Ensino, nem para seus alunos que vieram a beneficiar-se de tais ações.</p>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<p class="termo">
					<strong>CLÁUSULA 6ª - </strong>O presente convênio terá duração indeterminada, podendo ser alterado através de termos aditivos,
					bem como rescindido de comum acordo entre as partes, mediante comunicação escrita, com antecedência mínima de 30 (trinta)
					dias. Após a denúncia do convenio serão suspensos os serviços entre as partes, incluso o cadastro e encaminhamento dos
					estudantes.</p>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<p class="termo">
					<strong>CLÁUSULA 7ª - </strong>As partes, de comum acordo, elegem o foro de Juazeiro do Norte - CE, para dirimir qualquer questão
					fundada no presente Convênio.</p>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<p>E, por estarem de inteiro e comum acordo com as condições e com o texto deste Convênio, as partes o assinam em 2 (duas)
					vias de igual teor e forma.</p>
			</div>
		</div>
		<div class="row">
			<div class="col-6 offset-md-9" style="margin-top: 15px;">
				<p>Juazeiro do Norte,_____de____________de__________.</p>
			</div>
		</div>


		<div class="row" style="margin-top: 70px;">
			<div class="col-6">
				<div class="row">
					<div class="col-6" style="font-size: 11px">
						<hr style=" margin-bottom: 0px; border-bottom: 1px solid #333 ;" />
						<p class="p">Instituição de Ensino</p>
						<p class="p">(Assinatura e Carimbo)</p>
					</div>
					<!--                <div class="span4" style="font-size: 11px; margin-top: 25px;">
                                <hr style=" margin-bottom: 0px; border-bottom: 1px solid #333 ;" />
                                <p class="p">Instituição de Ensino</p>
                                <p class="p">(Assinatura e Carimbo)</p>
                            </div>-->
				</div>
			</div>
			<div class="col-6">
				<div class="row">
					<div class="col-6" style="font-size: 11px">
						<hr style=" margin-bottom: 0px; border-bottom: 1px solid #333 ;" />
						<p class="p">Universidade Patativa do Assaré - UPA</p>
						<p class="p">(Assinatura e Carimbo)</p>
					</div>
				</div>
			</div>
		</div>

		<div class="row" id="rodape">
			<div class="col-10" style="text-align: center; margin-top: 50px; ">
				<p class="p">Rua São Jorge 530, Centro - Juazeiro do Norte - CE - CEP 63010-470</p>
				<p class="p">Telefone: (88) 3512-2450 - www.universidadepatativa.com.br</p>
			</div>
		</div>


		
	</div>
</body>

</html>