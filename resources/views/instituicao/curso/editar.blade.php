@extends('adminlte::page')

@if(!empty($titulo))
    @section('title_postfix', ' - '.$titulo)
@endif

@section('content_header')
    <h1>Edição de Curso da Instituição de Ensino</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('instituicao.index')}}"><i class="fa fa-university"></i> Instituições</a></li>
        <li><a href="{{route('instituicao.show', ['instituicao' => $instituicao->id])}}"><i class="fa fa-file"></i> {{$instituicao->nmInstituicao}}</a></li>
        <li><i class="fa fa-file-text"></i> {{$CursoInstituicao->curso->nmCurso}}</li>
    </ol>
@stop

@section('content')
    @if(session('success'))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> Alerta!</h4>
            {{session('success')}}
    </div>
    @endif

    @if(session('error'))
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Alerta!</h4>
            {{session('error')}}
    </div>
    @endif      
    <div class="row">
        <div class="col-md-12">
            <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title"><strong>Curso:</strong> {{$CursoInstituicao->curso->nmCurso}} <strong>Instituição:</strong> {{$instituicao->nmInstituicao}}</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
                <form action="{{route('instituicao.curso.update', ['instituicao' => $instituicao->id, 'curso' => $CursoInstituicao->id ])}}" method="POST">
                {!! csrf_field() !!}
                {!! method_field('PUT') !!}
                    <div class="box-body">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Duração: </label> {{$CursoInstituicao->CursoDuracao->nmDuracao}}
                                <select class="form-control" name="curso_duracao_id" id="curso_duracao_id">
                                    <option value="{{$CursoInstituicao->CursoDuracao->id}}" selected>{{$CursoInstituicao->CursoDuracao->nmDuracao}}</option>
                                    <option value="">------------------</option>
                                    @foreach($cursoDuracoes as $duracao)
                                    <option value="{{$duracao['id']}}">{{$duracao['nmDuracao']}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <!-- /.form-group -->
                        </div>
                        <!-- /.col -->
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Qtd Duração:</label> {{$CursoInstituicao->qtdDuracao}}
                                <input class="form-control" id="qtdDuracao" name="qtdDuracao" placeholder="Duração" type="number" min="0" value="{{$CursoInstituicao->qtdDuracao}}">
                            </div>
                            <!-- /.form-group -->
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Semestre Minimo p/ Estágio:</label> {{$CursoInstituicao->qtdEstagio}}
                                <input class="form-control" id="qtdEstagio" name="qtdEstagio" type="number" min="0" value="{{$CursoInstituicao->qtdEstagio}}">
                            </div>
                            <!-- /.form-group -->
                        </div>
                        <div class="row text-center">	
                            <div class="col-md-12">
                                <label>Turnos deste Curso: </label>
                                <div class="form-group">
                                    @foreach($turnos as $turno)
                                        <label class="checkbox-inline">
                                            <input type="checkbox" 
                                                   name="turnos[]" 
                                                   value="{{$turno['id']}}"
                                                   @if ($CursoInstituicao->turnos->contains($turno['id']))
                                                   checked
                                                   @endif
                                                   > {{$turno['nmTurno']}}
                                        </label>
                                    @endforeach
                                </div>
                            </div> 
                            <!-- /.col -->
                        </div>
                        
                        
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer text-center">
                        <button type="submit" class="btn btn-success">Atualizar</button>
                    </div>
                </form>
          </div>    
  
        </div>
    </div>
   
@stop