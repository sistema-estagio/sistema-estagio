@extends('adminlte::page') 

@if(!empty($titulo))
	@section('title_postfix', ' - '.$titulo)
@endif

@section('content_header')

<h1>Historico de Instituição de Ensino</h1>

<ol class="breadcrumb">
	<li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="{{route('instituicao.index')}}"><i class="fa fa-university"></i> Instituições</a></li>
	<li><a href="{{route('polo.index')}}"><i class="fa fa-mortar-board"></i> Polos</a></li>
	<li><a href="{{route('polo.show', ['polo' => $polo->id])}}"><i class="fa fa-file"></i> {{$polo->id}} - {{$polo->nmPolo}}</a></li>	
	<li class="active"><i class="fa fa-comments"></i> Historico</li>
</ol>
@stop 
@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title"><b>{{$polo->nmPolo}}</b></h3>
				<div class="box-tools pull-right">
					<b>Codigo:</b> {{$polo->id}}
				</div>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
					<table class="table table-bordered">
						<tbody>
							<tr>
								<th>DATA</th>
								<th>USUARIO</th>
								<th>ANTES</th>
								<th>DEPOIS</th>
							</tr>
							  	@forelse ($logs as $log)
							<tr>
								<td>{{ $log->created_at->format('d/m/Y H:i') }}</td>
								<td>{{ $log->user->name }}</td>
								{{--  <td>{{ $log->old_values }}</td>  --}}
								<td style="font-size: 10px;">
								@foreach($log->old_values as $old => $value)
									<li>{{$old}}: {{$value}}</li>
								@endforeach
								</td>
								<td style="font-size: 10px;">
								@foreach($log->new_values as $new => $value)
									<li>{{$new}}: {{$value}}</li>
								@endforeach
								</td>
								{{--  <td>{{ $log->new_values }}</td>  --}}
							</tr>
								@empty
							<tr>
								<td colspan="4" align="center">
								  <span class="badge bg-red">Sem LOGO Registrados para esta Instituição!</span>
								</td>
							</tr>
								@endforelse
						</tbody>
					</table>	
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
					@if(isset($dataForm)) {!! $logs->appends($dataForm)->links() !!} 
						@else {!! $logs->links() !!} 
					@endif
			</div>
		</div>

	</div>
</div>

@stop