@extends('adminlte::page') 

@if(!empty($titulo))
	@section('title_postfix', ' - '.$titulo)
@endif

@section('content_header')
<h1>Polos Matriz das Instituições de Ensino</h1>
<ol class="breadcrumb">
	<li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="{{route('instituicao.index')}}"><i class="fa fa-university"></i> Instituições</a></li>
	<li class="active"><i class="fa fa-mortar-board"></i> Polos</li>
</ol>
@stop @section('content') @if(session('success'))
<div class="alert alert-success alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h4>
		<i class="icon fa fa-check"></i> Alerta!</h4>
	{{session('success')}}
</div>
@endif @if(session('error'))
<div class="alert alert-danger alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h4>
		<i class="icon fa fa-ban"></i> Alerta!</h4>
	{{session('error')}}
</div>
@endif
<a href="{{route('polo.create')}}" class="btn btn-success ad-click-event">Novo Polo</a>
</br>
</br>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Listagem de Polos</h3>
				<div class="box-tools">
					<form action="{{route('polo.search')}}" method="POST">
						{!! csrf_field() !!}
						<div class="input-group input-group-sm" style="width: 200px;">
							<input name="key_search" class="form-control pull-right" placeholder="Pesquisar" type="text">

							<div class="input-group-btn">
								<button type="submit" class="btn btn-default">
									<i class="fa fa-search"></i>
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<table class="table table-bordered">
					<tbody>
						<tr>
							<th style="width: 40px">#</th>
							<th>Polo</th>
							<!--<th style="width: 40px">Cadastro</th>-->
							<th>CNPJ</th>
						</tr>
						@forelse($polos as $polo)
						<tr>
							<td>{{$polo->id}}</td>
							<td>
								<a href="{{route('polo.show', ['polo' => $polo->id])}}">{{$polo->nmPolo}}</a>
							</td>
							<td>{{$polo->cdCNPJ}}</td>
						</tr>
						@empty
						<tr>
							<td colspan="3" align="center">
								<span class="badge bg-red">Sem registros no banco de dados!</span>
							</td>
						</tr>

						@endforelse
					</tbody>
				</table>
			</div>
			<div class="box-footer clearfix">

			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				@if(isset($dataForm)) {!! $polos->appends($dataForm)->links() !!} 
					@else {!! $polos->links() !!} 
				@endif
			</div>
		</div>
		<!-- /.box -->
	</div>
	@stop