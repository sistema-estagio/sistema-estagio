@extends('adminlte::page') 

@if(!empty($titulo))
	@section('title_postfix', ' - '.$titulo)
@endif

@section('content_header')

<h1>Polos Matriz de Instituição de Ensino</h1>
<ol class="breadcrumb">
	<li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="{{route('polo.index')}}"><i class="fa fa-university"></i> Instituições</a></li>
	<li class="active"><i class="fa fa-file"></i> {{$polo->id}} - {{$polo->nmPolo}}</li>
	<li><a href="{{route('polo.historico', ['polo' => $polo->id])}}"><i class="fa fa-comments"></i> Historico</a></li>
</ol>
@stop 
@section('content')

@if(session('success'))
<div class="alert alert-success alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h4><i class="icon fa fa-check"></i> Alerta!</h4>
		{{session('success')}}
</div>
@endif

@if(session('error'))
<div class="alert alert-danger alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h4><i class="icon fa fa-ban"></i> Alerta!</h4>
		{{session('error')}}
</div>
@endif
<div class="row">
	<div class="col-md-12">
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">{{$polo->nmPolo}}</h3>
				<div class="box-tools pull-right">
					<b>Codigo:</b> {{$polo->id}}
				</div>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<table class="table table-condensed table-striped">
					<tbody>
						<tr>
							<td colspan="4">
								<label>Nome:</label> {{$polo->nmPolo}}</td>
							<td colspan="2">
								<label>CNPJ:</label> {{$polo->cdCNPJ}}</td>
						</tr>
						
						@if($polo->nmReitor != NULL)
						<tr>
							<td colspan="3">
								<label>Reitor:</label> {{$polo->nmReitor}}</td>
							<td colspan="2">
								<label>Cargo:</label> {{$polo->nmReitorCargo}}</td>
						</tr>
						@endif
						
						@if($polo->nmDiretor != NULL)
						<tr>
							<td colspan="3">
								<label>Diretor:</label> {{$polo->nmDiretor}}</td>
							<td colspan="2">
								<label>Cargo:</label> {{$polo->nmDiretorCargo}}</td>
						</tr>
						@endif
						@if($polo->nmSupervisor != NULL)
						<tr>
							<td colspan="3">
								<label>Supervisor:</label> {{$polo->nmSupervisor}}</td>
							<td colspan="2">
								<label>Cargo:</label> {{$polo->nmSupervisorCargo}}</td>
						</tr>
						@endif
						<tr>
							<td colspan="6">
								<label>Endereço: </label>
								{{$polo->dsEndereco}},
								<strong>N°</strong> {{$polo->nnNumero}} </td>

						</tr>
						<tr>
							<td colspan="2">
								<label>Bairro:</label> {{$polo->nmBairro}}</td>
							<td colspan="1">
								<label>Cidade:</label> {{$polo->cidade->nmCidade}}</td>
							<td colspan="1">
								<label>Estado:</label> {{$polo->cidade->estado->nmEstado}}</td>
							<td colspan="1">
								<label>Cep:</label> {{$polo->cdCEP}}</td>
						</tr>
						<tr>
							<td colspan="1">
								<label>Telefone:</label> {{$polo->dsFone}}</td>
							<td colspan="2">
								<label>Outro:</label> {{$polo->dsOutro}}</td>
							<td colspan="2">
								<label>E-mail:</label> {{$polo->dsEmail}}</td>
						</tr>
					</tbody>
				</table>

			</div>
			<!-- /.box-body -->

			<div class="box-footer text-right">
				<a href="{{route('polo.edit', $polo->id)}}" class="btn btn-warning ad-click-event">Editar</a>
			</div>
		</div>

	</div>
</div>

@stop