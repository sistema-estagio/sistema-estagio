@extends('adminlte::page')

@if(!empty($titulo))
    @section('title_postfix', ' - '.$titulo)
@endif

@section('content_header')
    <h1>Polos Matriz de Instituição de Ensino</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('instituicao.index')}}"><i class="fa fa-university"></i> Instituições</a></li>
        <li><a href="{{route('polo.index')}}"><i class="fa fa-mortar-board"></i> Polos</a></li>
        <li><a href="{{route('polo.show', ['polo' => $polo->id])}}"><i class="fa fa-mortar-board"></i> {{$polo->nmPolo}}</a></li>
        <li class="active"><i class="fa fa-pencil"></i> Edição</li>
    </ol>
@stop

@section('content')
    @if(session('success'))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> Alerta!</h4>
            {{session('success')}}
    </div>
    @endif

    @if(session('error'))
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Alerta!</h4>
            {{session('error')}}
    </div>
    @endif      
    <div class="row">
        <div class="col-md-12">
            <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">{{$polo->nmPolo}}</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
                <form action="{{route('polo.update', ['polo' => $polo->id])}}" method="POST">
                {!! csrf_field() !!}
                {!! method_field('PUT') !!}
                    <div class="box-body">
                    <div class="row">
            
                        <div class="col-md-8">
                            <div class="form-group">
                                <label for="nmPolo">*Nome Polo</label>
                                <input class="form-control" id="nmPolo" name="nmPolo" type="text" value="{{$polo->nmPolo}}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="cdCNPJ">*CNPJ</label>
                                <input class="form-control" id="cdCNPJ" name="cdCNPJ" type="text" value="{{$polo->cdCNPJ}}">
                            </div>
                        </div>
                    </div>
                    
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="nmReitor">Reitor</label>
                                    <input class="form-control" id="nmReitor" name="nmReitor" type="text" value="{{$polo->nmReitor}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="nmReitorCargo">Cargo</label>
                                    <input class="form-control" id="nmReitorCargo" name="nmReitorCargo" type="text" value="{{$polo->nmReitorCargo}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="nmDiretor">Diretor</label>
                                        <input class="form-control" id="nmDiretor" name="nmDiretor" type="text" value="{{$polo->nmDiretor}}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="nmDiretorCargo">Cargo</label>
                                        <input class="form-control" id="nmDiretorCargo" name="nmDiretorCargo" type="text" value="{{$polo->nmDiretorCargo}}">
                                    </div>
                                </div>
                        </div>
                        <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="nmSupervisor">Supervisor</label>
                                        <input class="form-control" id="nmSupervisor" name="nmSupervisor" type="text" value="{{$polo->nmSupervisor}}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="nmSupervisorCargo">Cargo</label>
                                        <input class="form-control" id="nmSupervisorCargo" name="nmSupervisorCargo" type="text" value="{{$polo->nmSupervisorCargo}}">
                                    </div>
                                </div>
                            </div>
                    <legend>Endereço e Contatos</legend>
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="cdCEP">*CEP</label>
                                <input class="form-control" id="cdCEP" name="cdCEP" type="text" value="{{$polo->cdCEP}}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="dsEndereco">*Endereço</label>
                                <input class="form-control" id="dsEndereco" name="dsEndereco" type="text" value="{{$polo->dsEndereco}}">
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group">
                                <label for="nnNumero">*Numero</label>
                                <input class="form-control" id="nnNumero" name="nnNumero" type="text" value="{{$polo->nnNumero}}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="nmBairro">*Bairro</label>
                                <input class="form-control" id="nmBairro" name="nmBairro" type="text" value="{{$polo->nmBairro}}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="estado_id">*Estado</label>
                                <select class="form-control" name="estado_id" id="estado_id">
                                    <option value="{{$polo->cidade->estado_id}}" selected>{{$polo->cidade->estado->nmEstado}}</option>
                                    <option value="">----------</option>
                                    @foreach($estados as $estado)  
                                    <option value="{{$estado['id']}}">{{$estado['nmEstado']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="cidade_id">*Cidade</label>
                                <select class="form-control" id="cidade_id" name="cidade_id">
                                <option value="{{$polo->cidade->id}}" selected>{{$polo->cidade->nmCidade}}</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="dsFone">*Telefone</label>
                                <input class="form-control" id="dsFone" name="dsFone" type="text" value="{{$polo->dsFone}}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="dsOutro">Outro</label>
                                <input class="form-control" id="dsOutro" name="dsOutro" type="text" value="{{$polo->dsOutro}}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <!--<div class="col-md-4">
                            <div class="form-group">
                                <label for="unidade_id">Unidade Upa</label>
                                <input class="form-control" id="unidade_id" name="unidade_id" type="text" value="{{$polo->unidade_id}}">
                            </div>
                        </div> -->
                    </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer text-center">
                        <button type="submit" class="btn btn-success">Atualizar</button>
                    </div>
                </form>
          </div>    
  
        </div>
    </div>
@section('post-script')
<script type="text/javascript">
	$(document).ready(function() {
        $('#cidade_id').select2();
    });
</script>
<script type="text/javascript">
	
        $('select[name=estado_id]').change(function () {
            var idEstado = $(this).val();
          
            $.get('../get-cidades/' + idEstado, function (cidades) {
               
                $('select[name=cidade_id]').empty();
                $.each(cidades, function (key, value) {
                    $('select[name=cidade_id]').append('<option value=' + value.id + '>' + value.nmCidade + '</option>');
                });
            });
        });

jQuery(function($){
           $("#cdCEP").mask("99999-999");
           $("#cdCNPJ").mask("99.999.999/9999-99");
    });
    jQuery("#dsFone, #dsOutroFone")
        .mask("(99) 9999-9999?9")
        .focusout(function (event) {  
            var target, phone, element;  
            target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
            phone = target.value.replace(/\D/g, '');
            element = $(target);  
            element.unmask();  
            if(phone.length > 10) {  
                element.mask("(99) 99999-999?9");  
            } else {  
                element.mask("(99) 9999-9999?9");  
            }  
        });
</script>
<!-- Adicionando Javascript -->
<script type="text/javascript" >
    
            $(document).ready(function() {
    
                function limpa_formulário_cep() {
                    // Limpa valores do formulário de cep.
                    $("#dsEndereco").val("");
                    $("#nmBairro").val("");
                }
                
                //Quando o campo cep perde o foco.
                $("#cdCEP").blur(function() {
    
                    //Nova variável "cep" somente com dígitos.
                    var cep = $(this).val().replace(/\D/g, '');
    
                    //Verifica se campo cep possui valor informado.
                    if (cep != "") {
    
                        //Expressão regular para validar o CEP.
                        var validacep = /^[0-9]{8}$/;
    
                        //Valida o formato do CEP.
                        if(validacep.test(cep)) {
    
                            //Preenche os campos com "..." enquanto consulta webservice.
                            $("#dsEndereco").val("...");
                            $("#nmBairro").val("...");
    
                            //Consulta o webservice viacep.com.br/
                            $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {
    
                                if (!("erro" in dados)) {
                                    //Atualiza os campos com os valores da consulta.
                                    $("#dsEndereco").val(dados.logradouro);
                                    $("#nmBairro").val(dados.bairro);
                                } //end if.
                                else {
                                    //CEP pesquisado não foi encontrado.
                                    limpa_formulário_cep();
                                    alert("CEP não encontrado.");
                                }
                            });
                        } //end if.
                        else {
                            //cep é inválido.
                            limpa_formulário_cep();
                            alert("Formato de CEP inválido.");
                        }
                    } //end if.
                    else {
                        //cep sem valor, limpa formulário.
                        limpa_formulário_cep();
                    }
                });
            });
        </script>
@endsection
@stop