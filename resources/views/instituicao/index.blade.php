@extends('adminlte::page') 

@if(!empty($titulo))
	@section('title_postfix', ' - '.$titulo)
@endif

@section('content_header')
<h1>Instituições de Ensino</h1>
<ol class="breadcrumb">
	<li>
		<a href="{{route('home')}}">
			<i class="fa fa-dashboard"></i> Home</a>
	</li>
	<li class="active">
		<i class="fa fa-university"></i> Instituições</li>
</ol>
@stop @section('content') @if(session('success'))
<div class="alert alert-success alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h4>
		<i class="icon fa fa-check"></i> Alerta!</h4>
	{{session('success')}}
</div>
@endif @if(session('error'))
<div class="alert alert-danger alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h4>
		<i class="icon fa fa-ban"></i> Alerta!</h4>
	{{session('error')}}
</div>
@endif
<a href="{{route('instituicao.create')}}" class="btn btn-success ad-click-event">Nova Instituição</a>
</br>
</br>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Listagem de Instituições de Ensino</h3>
				{{-- Mostrar Barra de Pesquisa quando não usa o datatable  
					<div class="box-tools">
					<form action="{{route('instituicao.search')}}" method="POST">
						{!! csrf_field() !!}
						<div class="input-group input-group-sm" style="width: 200px;">
							<input name="key_search" class="form-control pull-right" placeholder="Pesquisar" type="text">

							<div class="input-group-btn">
								<button type="submit" class="btn btn-default">
									<i class="fa fa-search"></i>
								</button>
							</div>
						</div>
					</form>
				</div>  --}}
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<!--<table id="instituicoes" class="table table-bordered">-->
				<table id="instituicoes" class="table table-bordered table-hover">		
						<thead>
						<tr>
							<th style="width: 20px">#</th>
							<th>Instituição</th>
							<!--<th style="width: 40px">Cadastro</th>
							<th>Nome Fantasia</th>-->
							<th>Cidade/UF</th>
							<th>CNPJ</th>
						</tr>
						</thead>
						<tbody>
						@forelse($instituicoes as $instituicao)
						<tr>
							<td>{{$instituicao->id}}</td>
							<td>
								<a href="{{route('instituicao.show', ['instituicao' => $instituicao->id])}}">{{$instituicao->nmInstituicao}}</a>
								<br><span class="direct-chat-timestamp pull-left">{{$instituicao->nmFantasia}}</span>
							</td>
							{{-- <td></td> --}}
							<td>{{$instituicao->cidade->nmCidade}}/{{$instituicao->cidade->estado->cdUF}}</td>
							<td>{{$instituicao->cdCNPJ}}</td>
						</tr>
						
						@empty
						<tr>
							<td colspan="4" align="center">
								<span class="badge bg-red">Sem registros no banco de dados!</span>
							</td>
						</tr>
						@endforelse
					</tbody>
				</table>
			</div>
			<div class="box-footer clearfix">

			</div>
			<!-- /.box-body -->
			{{--  <div class="box-footer clearfix">
				@if(isset($dataForm)) {!! $instituicoes->appends($dataForm)->links() !!} 
					@else {!! $instituicoes->links() !!} 
				@endif
			</div>  --}}
		</div>
		<!-- /.box -->
	</div>
	@section('post-script')
	<script>
			$(function () {
			  $('#instituicoes').DataTable({
				"order": [[ 1, "asc" ]],
				"pageLength": 25,
				"language": {
					"sEmptyTable": "Nenhum registro encontrado",
					"sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
					"sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
					"sInfoFiltered": "(Filtrados de _MAX_ registros)",
					"sInfoPostFix": "",
					"sInfoThousands": ".",
					"sLengthMenu": "_MENU_ resultados por página",
					"sLoadingRecords": "Carregando...",
					"sProcessing": "Processando...",
					"sZeroRecords": "Nenhum registro encontrado",
					"sSearch": "Pesquisar",
					"oPaginate": {
						"sNext": "Próximo",
						"sPrevious": "Anterior",
						"sFirst": "Primeiro",
						"sLast": "Último"
					},
					"oAria": {
						"sSortAscending": ": Ordenar colunas de forma ascendente",
						"sSortDescending": ": Ordenar colunas de forma descendente"
					}
				}
			  })
			  
			})
	</script>
	@endsection
@stop