@extends('adminlte::page')

@if(!empty($titulo))
    @section('title_postfix', ' - '.$titulo)
@endif


@section('content_header')
    <h1>Instituições de Ensino</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('instituicao.index')}}"><i class="fa fa-university"></i>Instituições</a></li>
        <li class="active"><i class="fa fa-file"></i> Cadastro de Instituição</li>
    </ol>
@stop

@section('content')
    @if(session('success'))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> Alerta!</h4>
            {{session('success')}}
    </div>
    @endif

    @if(session('error'))
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Alerta!</h4>
            {{session('error')}}
    </div>
    @endif  
    
    @if($errors->any())
    <div class="alert alert-warning alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Alerta!</h4>
        <ul class="alert-warning">
            <h3>:( Whoops, Houve algum erro! </h3>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <div class="row">
        <div class="col-md-12">
            <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Formulário de Cadastro</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
                <form action="{{route('instituicao.store')}}" method="POST">
                {!! csrf_field() !!}
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="isEad">*Tipo</label>
                                <select class="form-control" name="isEad" id="isEad">
                                        <option value="N" @if (old('isEad') == "N") {{ 'selected' }} @endif>Normal</option>
                                        <option value="S" @if (old('isEad') == "S") {{ 'selected' }} @endif>EAD</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="nmInstituicao">*Razão Social da Instituição</label>
                                <input class="form-control" id="nmInstituicao" name="nmInstituicao" placeholder="Nome da Instituição" type="text" value="{{old('nmInstituicao')}}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="nmFantasia">*Nome Fantasia</label>
                                <input class="form-control" id="nmFantasia" name="nmFantasia" placeholder="Nome Fantasia da Instituição" type="text" value="{{old('nmFantasia')}}">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="cdCNPJ">*CNPJ</label>
                                <input class="form-control" id="cdCNPJ" name="cdCNPJ" placeholder="CNPJ" type="text" value="{{old('cdCNPJ')}}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="nmDiretor">Diretor</label>
                                    <input class="form-control" id="nmDiretor" name="nmDiretor"placeholder="Diretor" type="text" value="{{old('nmDiretor')}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="nmDiretorCargo">Cargo</label>
                                    <input class="form-control" id="nmDiretorCargo" name="nmDiretorCargo"placeholder="Cargo do Diretor" type="text" value="{{old('nmDiretorCargo')}}">
                                </div>
                            </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nmReitor">Reitor</label>
                                <input class="form-control" id="nmReitor" name="nmReitor" placeholder="Reitor" type="text" value="{{old('nmReitor')}}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nmReitorCargo">Cargo</label>
                                <input class="form-control" id="nmReitorCargo" name="nmReitorCargo" placeholder="Cargo do Reitor" type="text" value="{{old('nmReitorCargo')}}">
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="nmSupervisor">Supervisor</label>
                                    <input class="form-control" id="nmSupervisor" name="nmSupervisor"placeholder="Supervisor" type="text" value="{{old('nmSupervisor')}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="nmSupervisorCargo">Cargo</label>
                                    <input class="form-control" id="nmSupervisorCargo" name="nmSupervisorCargo" placeholder="Cargo do Supervisor" type="text" value="{{old('nmSupervisorCargo')}}">
                                </div>
                            </div>
                    </div>
                    <legend>Endereço e Contatos</legend>
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="cdCEP">*CEP</label>
                                <input class="form-control" id="cdCEP" name="cdCEP" placeholder="CEP" type="text" value="{{old('cdCEP')}}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="dsEndereco">*Endereço</label>
                                <input class="form-control" id="dsEndereco" name="dsEndereco" placeholder="Endereço" type="text" value="{{old('dsEndereco')}}">
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group">
                                <label for="nnNumero">*Numero</label>
                                <input class="form-control" id="nnNumero" name="nnNumero" placeholder="000" type="text" value="{{old('nnNumero')}}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="nmBairro">*Bairro</label>
                                <input class="form-control" id="nmBairro" name="nmBairro" placeholder="Bairro" type="text" value="{{old('nmBairro')}}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="cidade_id">*Estado</label>
                                <select class="form-control" name="estado_id" id="estado_id">
                                    <option value="" selected>Selecione...</option>
                                    @foreach($estados as $estado)
                                    <option value="{{ $estado['id'] }}" @if(old('estado_id') == $estado['id']) {{ 'selected' }} @endif>{{ $estado['nmEstado'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="cidade_id">*Cidade</label>
                                <select class="form-control" id="cidade_id" name="cidade_id">
                                    @if(old('estado_id'))
                                        @foreach($cidades as $cidade)
                                            <option value="{{ $cidade['id'] }}" @if(old('cidade_id') == $cidade['id']) {{ 'selected' }} @endif>{{ $cidade['nmCidade'] }}</option>
                                        @endforeach
                                    @else
                                        <option value="" selected>Selecione Estado</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="dsFone">*Telefone</label>
                                <input class="form-control" id="dsFone" name="dsFone" placeholder="(88)3535-12345" type="text" value="{{old('dsFone')}}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="dsOutro">Outro</label>
                                <input class="form-control" id="dsOutro" name="dsOutro" placeholder="(88)98801-1234" type="text" value="{{old('dsOutroFone')}}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="dsEmail">*E-mail</label>
                                <input class="form-control" id="dsEmail" name="dsEmail" placeholder="email@email.com.br" type="text" value="{{old('dsEmail')}}">
                            </div>
                        </div>
                        <!--<div class="col-md-4">
                            <div class="form-group">
                                <label for="unidade_id">Unidade Upa</label>
                                <input class="form-control" id="unidade_id" name="unidade_id" placeholder="Unidade" type="text">
                            </div>
                        </div> -->
                    </div>

                    <legend>Polo Matriz</legend>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <select class="form-control" name="polo_id" id="polo_id">
                                        <option value="" selected>Sem Polo</option>
                                        @foreach($polos as $polo)
                                            <option value="{{ $polo['id'] }}" @if(old('polo_id') == $polo['id']) {{ 'selected' }} @endif>{{ $polo['nmPolo'] }}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    
                    <div class="box-footer text-center">
                        <button type="submit" class="btn btn-success">Cadastrar</button>
                    </div>
                </form>
          </div>    
          <!-- /.box-body -->
        </div>
    </div>
@section('post-script')
<script>
    $(document).ready(function(){
        $('#cidade_id').select2();
        $('#polo_id').select2();
        //maskara nova de telefones
        var SPMaskBehavior = function (val) {
            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
          },
          spOptions = {
            onKeyPress: function(val, e, field, options) {
                field.mask(SPMaskBehavior.apply({}, arguments), options);
              }
          };
        
          $('#dsFone').mask(SPMaskBehavior, spOptions);
          $('#dsOutro').mask(SPMaskBehavior, spOptions);
    });

    jQuery(function($){
           $("#cdCEP").mask("99999-999");
           $("#cdCNPJ").mask("99.999.999/9999-99");
    });

     $('select[name=estado_id]').change(function () {
            var idEstado = $(this).val();
          
            $.get('../get-cidades/' + idEstado, function (cidades) {
               
                $('select[name=cidade_id]').empty();
                $('#cidade_id').append('<option value=""selected="selected">Selecione a Cidade</option>');
                $.each(cidades, function (key, value) {
                    $('select[name=cidade_id]').append('<option value=' + value.id + '>' + value.nmCidade + '</option>');
                });
            });
        });

</script>
<!-- Adicionando Javascript -->
<script type="text/javascript" >
    
            $(document).ready(function() {
    
                function limpa_formulário_cep() {
                    // Limpa valores do formulário de cep.
                    $("#dsEndereco").val("");
                    $("#nmBairro").val("");
                }
                
                //Quando o campo cep perde o foco.
                $("#cdCEP").blur(function() {
    
                    //Nova variável "cep" somente com dígitos.
                    var cep = $(this).val().replace(/\D/g, '');
    
                    //Verifica se campo cep possui valor informado.
                    if (cep != "") {
    
                        //Expressão regular para validar o CEP.
                        var validacep = /^[0-9]{8}$/;
    
                        //Valida o formato do CEP.
                        if(validacep.test(cep)) {
    
                            //Preenche os campos com "..." enquanto consulta webservice.
                            $("#dsEndereco").val("...");
                            $("#nmBairro").val("...");
    
                            //Consulta o webservice viacep.com.br/
                            $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {
    
                                if (!("erro" in dados)) {
                                    //Atualiza os campos com os valores da consulta.
                                    $("#dsEndereco").val(dados.logradouro);
                                    $("#nmBairro").val(dados.bairro);
                                } //end if.
                                else {
                                    //CEP pesquisado não foi encontrado.
                                    limpa_formulário_cep();
                                    alert("CEP não encontrado.");
                                }
                            });
                        } //end if.
                        else {
                            //cep é inválido.
                            limpa_formulário_cep();
                            alert("Formato de CEP inválido.");
                        }
                    } //end if.
                    else {
                        //cep sem valor, limpa formulário.
                        limpa_formulário_cep();
                    }
                });
            });
        </script>
@endsection
@stop