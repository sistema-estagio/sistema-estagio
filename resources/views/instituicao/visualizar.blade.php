@extends('adminlte::page') 

@if(!empty($titulo))
	@section('title_postfix', ' - '.$titulo)
@endif

@section('content_header')

<h1>Instituição de Ensino</h1>
<ol class="breadcrumb">
	<li>
		<a href="{{route('home')}}">
			<i class="fa fa-dashboard"></i> Home</a>
	</li>
	<li>
		<a href="{{route('instituicao.index')}}">
			<i class="fa fa-university"></i> Instituições</a>
	</li>
	<li class="active">
		<i class="fa fa-file"></i> {{$instituicao->id}} - {{$instituicao->nmInstituicao}}</li>
	<li>
		<a href="{{route('instituicao.historico', ['instituicao' => $instituicao->id])}}">
			<i class="fa fa-comments"></i> Historico</a>
	</li>
</ol>
@stop 
@section('content')

@if(session('Conveniosuccess'))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> Alerta!</h4>
            {{session('Conveniosuccess')}}
    </div>
@endif

@if(session('Convenioerror'))
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Alerta!</h4>
            {{session('Convenioerror')}}
    </div>
@endif

@if(session('success'))
<div class="alert alert-success alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h4><i class="icon fa fa-check"></i> Alerta!</h4>
		{{session('success')}}
</div>
@endif

@if(session('error'))
<div class="alert alert-danger alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h4><i class="icon fa fa-ban"></i> Alerta!</h4>
		{{session('error')}}
</div>
@endif

<div class="row">
		<div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                  <span class="info-box-icon bg-yellow"><i class="fa fa-users"></i></span>
      
                  <div class="info-box-content">
                    <span class="info-box-text">ESTUDANTES</span>
                    <span class="info-box-number">{{$estudantes->count()}}</span>
                  </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
        </div>
		<!-- /.col --> 
		
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-file-text-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">TCES ATIVOS</span>
              <span class="info-box-number">{{$tcesAtivos->count()}}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->         
</div>

<div class="row">
	
	<div class="col-md-12">
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title"><b>{{$instituicao->nmInstituicao}}</b></h3>
				<div class="box-tools pull-right">
					<b>Codigo:</b> {{$instituicao->id}}
				</div>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<table class="table table-condensed table-striped">
					<tbody>
						<tr>
							<td colspan="1"><label>Tipo:</label> @if($instituicao->isEad == "S") EAD @else NORMAL @endif</td>
							<td colspan="3"><label>Nome Fantasia:</label> {{$instituicao->nmFantasia}}</td>
							<td colspan="2"><label>CNPJ:</label> {{$instituicao->cdCNPJ}}</td>
						</tr>
						@if($instituicao->nmDiretor != NULL)
						<tr>
							<td colspan="3"><label>Diretor:</label> {{$instituicao->nmDiretor}}</td>
							<td colspan="2"><label>Cargo:</label> {{$instituicao->nmDiretorCargo}}</td>
						</tr>
						@endif 
						@if($instituicao->nmReitor != NULL)
						<tr>
							<td colspan="3"><label>Reitor:</label> {{$instituicao->nmReitor}}</td>
							<td colspan="2"><label>Cargo:</label> {{$instituicao->nmReitorCargo}}</td>
						</tr>
						@endif 
						@if($instituicao->nmSupervisor != NULL)
						<tr>
							<td colspan="3"><label>Supervisor:</label> {{$instituicao->nmSupervisor}}</td>
							<td colspan="2"><label>Cargo:</label> {{$instituicao->nmSupervisorCargo}}</td>
						</tr>
						@endif 
						<tr>
							<td colspan="6">
								<label>Endereço: </label>
								{{$instituicao->dsEndereco}},
								<strong>N°</strong> {{$instituicao->nnNumero}}
							</td>
						</tr>
						<tr>
							<td colspan="2"><label>Bairro:</label> {{$instituicao->nmBairro}}</td>
							<td colspan="1"><label>Cidade:</label> {{$instituicao->cidade->nmCidade}}</td>
							<td colspan="1"><label>Estado:</label> {{$instituicao->cidade->estado->nmEstado}}</td>
							<td colspan="1"><label>Cep:</label> {{$instituicao->cdCEP}}</td>
						</tr>
						<tr>
							<td colspan="1"><label>Telefone:</label> {{$instituicao->dsFone}}</td>
							<td colspan="2"><label>Outro:</label> {{$instituicao->dsOutro}}</td>
							<td colspan="2"><label>E-mail:</label> {{$instituicao->dsEmail}}</td>
						</tr>
					</tbody>
				</table>
				
				<!-- se houver polo matriz -->
				@if($instituicao->poloMatriz != NULL)
				<div class="box-header with-border">
					<h3 class="box-title"><b>Polo Matriz</b></h3>
				</div>
				<table class="table table-condensed table-striped">
					<tbody>
						<tr>
							<td colspan="4">
								<label>Nome Polo:</label> {{$instituicao->poloMatriz->nmPolo}}</td>
							<td colspan="2">
								<label>CNPJ:</label> {{$instituicao->poloMatriz->cdCNPJ}}</td>
						</tr>
						@if($instituicao->poloMatriz->nmReitor != NULL)
						<tr>
							<td colspan="3">
								<label>Reitor:</label> {{$instituicao->poloMatriz->nmReitor}}</td>
							<td colspan="3">
								<label>Cargo:</label> {{$instituicao->poloMatriz->nmReitorCargo}}</td>
						</tr>
						@endif
						@if($instituicao->poloMatriz->nmDiretor != NULL)
						<tr>
							<td colspan="3">
								<label>Diretor:</label> {{$instituicao->poloMatriz->nmDiretor}}</td>
							<td colspan="3">
								<label>Cargo:</label> {{$instituicao->poloMatriz->nmDiretorCargo}}</td>
						</tr>
						@endif
						@if($instituicao->poloMatriz->nmSupervisor != NULL)
						<tr>
							<td colspan="3">
								<label>Supervisor:</label> {{$instituicao->poloMatriz->nmSupervisor}}</td>
							<td colspan="3">
								<label>Cargo:</label> {{$instituicao->poloMatriz->nmSupervisorCargo}}</td>
						</tr>
						@endif
						<tr>
							<td colspan="3">
								<label>Endereço: </label>
								{{$instituicao->poloMatriz->dsEndereco}},
								<strong>N°</strong> {{$instituicao->poloMatriz->nnNumero}} </td>
							<td colspan="2">
								<label>Bairro:</label> {{$instituicao->poloMatriz->nmBairro}}</td>
							<td colspan="1">
								<label>Cep:</label> {{$instituicao->poloMatriz->cdCEP}}</td>
						</tr>
						<tr>
							<td colspan="2">
								<label>Cidade:</label> {{$instituicao->poloMatriz->cidade->nmCidade}}</td>
							<td colspan="1">
								<label>Estado:</label> {{$instituicao->poloMatriz->cidade->estado->nmEstado}}</td>
							<td colspan="1">
								<label>Telefone:</label> {{$instituicao->poloMatriz->dsFone}}</td>
							<td colspan="2">
								<label>Outro:</label> {{$instituicao->poloMatriz->dsOutro}}</td>
						</tr>
						<tr>
							
						</tr>
					</tbody>
				</table>
				@endif
				<!-- fim de houver polo matriz -->
				@if($convenio->count())
				@foreach ($convenio as $conv)
				
				<div class="col-xs-4">
						<p class="lead"><strong>Convênio</strong></p>
						<a href="{{route('instituicao.convenio.show', [$instituicao->id, $conv->id])}}" class="btn-xs btn-info ad-click-event" target="_blank">Visualizar Convênio</a>
						<div class="table-responsive">
						  	<table class="table">
								<tbody>
								<tr>
									<th style="width:50%">Data Convênio:</th>
									<td>xx/xx/xxxx</td>
								</tr>
								<tr>
									<th>Data Devolução Convênio:</th>
									<td>{{$conv->dtConvenioRetorno}}</td>
								</tr>
								</tbody>
							</table>
						</div>
				</div>
				
				@endforeach
				@else
				<br>
				<button type="button" class="btn-xs btn-warning ad-click-event" data-toggle="modal" data-target="#modal-convenio">Gerar Convênio</button>
					{{--  <a href="{{route('instituicao.convenio', $instituicao->id)}}" class="btn-xs btn-warning ad-click-event">Gerar Convênio</a>  --}}
					<div class="modal fade" id="modal-convenio" style="display: none;">
							<div class="modal-dialog">
								<div class="modal-content">
									<form action="{{route('instituicao.convenio')}}" method="POST">
										{!! csrf_field() !!}
										<input id="instituicao_id" name="instituicao_id" type="hidden" value="{{$instituicao->id}}">								
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">×</span></button>
											<h4 class="modal-title">Convênio com Instituição de Ensino</h4>
										</div>
										<div class="modal-body">
											<div class="form-group">
												<label  class="col-sm-4"for="dtConvenio">Data Convênio</label>
													<div class="col-sm-3">
															<input type="text" class="form-control" id="dtConvenio" name="dtConvenio" placeholder="dd/mm/aaaa"/>
													</div>
													<br>
											</div>
											<div class="form-group">
													<label  class="col-sm-4 "for="dtConvenioRetorno">Data Devolução</label>
														<div class="col-sm-3">
																<input type="text" class="form-control" id="dtConvenioRetorno" name="dtConvenioRetorno" placeholder="dd/mm/aaaa"/>
														</div>
														<br>
												</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Fechar</button>
											<button type="submit" class="btn btn-primary">Gerar Convênio</button>
										</div>
									</form>
								</div>
								<!-- /.modal-content -->
							</div>
							<!-- /.modal-dialog -->
					</div>
				@endif
			</div>
			<!-- /.box-body -->

			<div class="box-footer text-right">
				<a href="{{route('instituicao.edit', $instituicao->id)}}" class="btn btn-warning ad-click-event">Editar</a>
			</div>
		</div>

	</div>
</div>

</br>
<h3><strong>Cursos</strong>({{$instituicao->cursoDaInstituicao->count()}})</h3>
@if(session('successCurso'))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> Alerta!</h4>
            {{session('successCurso')}}
    </div>
@endif

@if(session('errorCurso'))
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Alerta!</h4>
            {{session('errorCurso')}}
    </div>
@endif
<div class="box box-default collapsed-box">
	<div class="box-header with-border">
		<h3 class="box-title" data-widget="collapse">Cadastrar Curso</h3>

		<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse">
				<i class="fa fa-plus"></i>
			</button>
		</div>
	</div>
	<!-- /.box-header -->
	<div class="box-body" style="display: none;">
		<form action="{{route('instituicao.curso.store', ['instituicao' => $instituicao->id])}}" method="POST">
			{!! csrf_field() !!}

			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label>Curso: </label>
						@if ($listaCursos->count())
						<select class="form-control" name="curso_id" id="curso_id">
							@foreach($listaCursos as $cursoLista)
								<option value="{{$cursoLista['id']}}">{{$cursoLista['nmCurso']}} - {{$cursoLista->nivel['nmNivel']}}</option>
							@endforeach
						</select>
						@else NENHUM REGISTRO NO BANCO DE DADOS :( @endif
					</div>
					<!-- /.form-group -->
				</div>
			</div>
			<div class="row col-md-offset-3">
				<!-- /.col -->
				<div class="col-md-2">
					<div class="form-group">
						<label>Duração: </label>
						<select class="form-control" name="curso_duracao_id" id="curso_duracao_id">
							@foreach($cursoDuracoes as $duracao)
							<option value="{{$duracao['id']}}">{{$duracao['nmDuracao']}}</option>
							@endforeach
						</select>
					</div>
					<!-- /.form-group -->
				</div>
				<!-- /.col -->
				<div class="col-md-2">
					<div class="form-group">
						<label>Qtd Duração: </label>
						<input class="form-control" id="qtdDuracao" name="qtdDuracao" placeholder="Duração" type="number" min="1" max="15" value="1">
					</div>
					<!-- /.form-group -->
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>Semestre Minimo p/ Estágio: </label>
						<input class="form-control" id="qtdEstagio" name="qtdEstagio" type="number" min="0" value="1">
					</div>
					<!-- /.form-group -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->

				{{--   --}}
			<div class="row text-center">	
				<div class="col-md-12">
					<label>Turnos deste Curso: </label>
					<div class="form-group">
						@foreach($turnos as $turno)		
							<label class="checkbox-inline">
								<input type="checkbox" name="turnos[]" value="{{$turno['id']}}"> {{$turno['nmTurno']}}
							</label>
						@endforeach
					</div>
				</div> 
				<!-- /.col -->
			</div>
			<div class="box-footer text-center">
				<button type="submit" class="btn btn-success">Cadastrar</button>
			</div>
			<!-- /.row -->
		</form>
	</div>
	<!-- /.box-body -->
</div>
<!-- /.box box-default collapsed-box -->
@if($instituicao->cursoDaInstituicao->count() > 0)
<div class="row">
	<div class="col-xs-12 table-responsive">
		<table class="table table-striped">
			<thead>
				<tr>
					<th style="width: 20px">#</th>
					<th style="width: 200px">Curso(Nível)</th>
					<th>Duração</th>
					<th>Estágio Apartir</th>
					<th style="width: 450px">Turnos</th>
					<th>Ações</th>
				</tr>
			</thead>
			<tbody class="cursosInstituicao">
				@foreach($instituicao->cursoDaInstituicao as $cursoInstituicao)
				
				<tr class="cursos" data-nome="{{$cursoInstituicao->curso->nmCurso}}">
					<td>{{$cursoInstituicao->id}}</td>
					<td>{{$cursoInstituicao->curso->nmCurso}} <br>({{$cursoInstituicao->curso->nivel->nmNivel}})</td>
					<td>{{$cursoInstituicao->qtdDuracao}} ({{$cursoInstituicao->cursoDuracao->nmDuracao}})</td>
					<td>{{$cursoInstituicao->qtdEstagio}}</td>
					<td>
						@forelse ($cursoInstituicao->turnos as $turno)
							[{{ $turno->nmTurno }}] 
						@empty
							<p>-</p>
						@endforelse
					</td>
					<td> <a href="{{route('instituicao.curso.edit', ['idIntituicao' => $instituicao->id, 'id' =>$cursoInstituicao->id])}}" class="btn-xs btn-warning ad-click-event" title="Editar"><i class="fa fa-pencil"></i> Editar</a></td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	<!-- /.col -->
</div>
@else
@endif

@section('post-script')
<script>
    jQuery(function($){
		$("#dtConvenioRetorno").mask("99/99/9999");
		var alphabeticallyOrderedDivs = $('.cursos').sort(function (a, b) {                   
			return String.prototype.localeCompare.call($(a).data('nome').toLowerCase(), $(b).data('nome').toLowerCase());                     
		});

		var container = $(".cursosInstituicao");              
			container.append(alphabeticallyOrderedDivs);                    
	});
	 	
</script>
@endsection
@stop