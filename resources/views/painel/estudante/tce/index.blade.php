@extends('layouts.estudante')
@section('css')
<style media="screen">
.lds-ring {
  display: inline-block;
  position: relative;
  width: 64px;
  height: 64px;
}
.lds-ring div {
  box-sizing: border-box;
  display: block;
  position: absolute;
  width: 51px;
  height: 51px;
  margin: 6px;
  border: 6px solid #0e502d;
  border-radius: 50%;
  animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
  border-color: #0e502d transparent transparent transparent;
}
.lds-ring div:nth-child(1) {
  animation-delay: -0.45s;
}
.lds-ring div:nth-child(2) {
  animation-delay: -0.3s;
}
.lds-ring div:nth-child(3) {
  animation-delay: -0.15s;
}
@keyframes lds-ring {
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
}
</style>
@endsection
@section('content')
<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
  {{-- <div class="container-fluid">
    <div class="header-body">
      <!-- Card stats -->
      <div class="row">
        @if($folhas->count() != 0)
        <div class="col-xl-4 col-lg-6">
          <a href="#" data-toggle="tooltip" data-placement="top" title="Ver minhas folhas de pagamentos" onclick="mostrarPagamento()">
            <div class="card card-default mb-4 mb-xl-0">
              <div class="card-body">
                <div class="row">
                  <div class="col">
                    <h5 class="card-title text-uppercase text-muted mb-0">Financeiro</h5>
                    <span class="h2 font-weight-bold mb-0">{{$folhas->count()}}</span>
                  </div>
                  <div class="col-auto">
                    <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                      <i class="ni ni-money-coins"></i>
                    </div>
                  </div>
                </div>
                <p class="mt-3 mb-0 text-muted text-sm">
                  <span class="text-nowrap">Quantidade de pagamentos já realizados</span>
                </p>
              </div>
            </div>
          </a>
        </div>
        @endif
        <div class="col-xl-4 col-lg-6">
          <a href="{{ route('painel.estudante.home')}}" data-toggle="tooltip" data-placement="right" title="Ver aditivos do TCE">
            <div class="card card-default mb-4 mb-xl-0">
              <div class="card-body">
                <div class="row">
                  <div class="col">
                    <h5 class="card-title text-uppercase text-muted mb-0">Aditivos</h5>
                    <span class="h2 font-weight-bold mb-0">{{$aditivos->count()}}</span>
                  </div>
                  <div class="col-auto">
                    <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                      <i class="ni ni-bullet-list-67"></i>
                    </div>
                  </div>
                </div>
                <p class="mt-3 mb-0 text-muted text-sm">
                  <span class="text-nowrap">Quantidade de aditivos ligados ao seu estágio</span>
                </p>
              </div>
            </div>
          </a>
        </div>
        @if($tce->relatorioSemestrals()->count() > 0)
        @if((\Carbon\Carbon::now()->format('m') - $tce->relatorioSemestrals->last()->created_at->format('m')) == 5 || (\Carbon\Carbon::now()->format('m') - $tce->relatorioSemestrals->last()->created_at->format('m')) == 1)
        <div class="col-xl-4 col-lg-6">
          <div class="alert-warning alert mb-4 mb-xl-0">
            <div class="">
              <div class="row">
                <div class="col">
                  <h3 class="card-title text-white mb-0"><i class="fa fa-exclamation-circle"></i> Atenção<button type="button" role="button" class="btn btn-default float-right" title="Imprimir questionário" data-toggle="modal" data-target="#check"><i class="fa fa-file-pdf"></i> Imprimir</button></h3>
                  <br>
                  <small class="h5 text-white font-weight-bold mb-0">O relatório de atividades semestral está disponível,<br>preencha-o e entre uma via a sua instituição de ensino</small>
                </div>
              </div>
              <p class="mt-3 mb-0 text-muted text-sm">
                <span class="text-nowrap"></span>
              </p>
            </div>
          </div>
        </div>
        @endif
        @elseif((\Carbon\Carbon::now()->format('m') - $tce->dtInicio->format('m')) == 5 || (\Carbon\Carbon::now()->format('m') - $tce->dtInicio->format('m')) == 1)
        <div class="col-xl-4 col-lg-6">
          <div class="alert-warning alert mb-4 mb-xl-0">
            <div class="">
              <div class="row">
                <div class="col">
                  <h3 class="card-title text-white mb-0"><i class="fa fa-exclamation-circle"></i> Atenção<a href="#" class="btn btn-default float-right" data-toggle="modal" data-target="#check"><i class="fa fa-file-pdf"></i> Imprimir</a></h3>
                  <br>
                  <small class="h5 text-white font-weight-bold mb-0">O relatório de atividades semestral está disponível,<br>preencha-o e entre uma via a sua instituição de ensino</small>
                </div>
              </div>
              <p class="mt-3 mb-0 text-muted text-sm">
                <span class="text-nowrap"></span>
              </p>
            </div>
          </div>
        </div>
        @endif
      </div>
    </div>
  </div> --}}
</div>
<!-- Page content -->
<div class="container-fluid mt--7">
  <div class="row justify-content-center mb-3" id="load" style="display:none;">
    <div class="col-md-6">
      <center><div class="lds-ring"><div></div><div></div><div></div><div></div></div></center>
    </div>
  </div>
  <div class="row justify-content-center mb-3" id="estagio">
    <div class="col-xl-8">
      <div class="card shadow">
        <div class="card-header bg-transparent">
          @if($tce->migracao != null)
          <h4 class="text-left">Dados do Estágio <b class="float-right">TCE: {{$tce->migracao}}</b></h4>
          @else
          <h4 class="text-left">Dados do Estágio <b class="float-right">TCE: {{$tce->id}}</b></h4>
          @endif
        </div>
        <div class="card-body">
          <table>
            <tbody>
              <tr>
                <th style="width:33%;">Curso</th>
                <th style="width:33%;">Semestre/Ano</th>
                <th style="width:33%;">Turno</th>
              </tr>
              <tr>
                <td>{{$tce->cursoDaInstituicao->curso->nmCurso}}</td>
                <td>{{$tce->nnSemestreAno}} {{$tce->cursoDaInstituicao->cursoDuracao->nmTipoDuracao}}</td>
                <td>{{$estudante->turno->nmTurno}}</td>
              </tr>
              <tr><td colspan="4"><br></td></tr>
              <tr>
                <th style="width:33%;">Período Semanal</th>
                <th style="width:33%;">Descrição do Período</th>
              </tr>
              <tr>
                <td>{{$tce->periodo->dsPeriodo}}</td>
                <td>{{$tce->dsPeriodo}}</td>
              </tr>
              <tr><td colspan="4"><br></td></tr>
              <tr>
                <th style="width:33%;">Vigência Inicio</th>
                <th style="width:33%;">Vigência Fim</th>
                <th style="width:33%;">Hora Inical e Final</th>
              </tr>
              <tr>
                <td>{{$tce->dtInicio->format('d/m/Y')}}</td>
                <td>{{$tce->dtFim->format('d/m/Y')}}</td>
                <td>{{$tce->hrInicio}} - {{$tce->hrFim}}</td>
              </tr>
              <tr><td colspan="4"><br></td></tr>
              <tr>
                <th style="width:33%;">Auxílio Transporte</th>
                <th style="width:33%;">Auxílio Alimentação</th>
                <th style="width:33%;">Valor da Bolsa</th>
              </tr>
              <tr>
                @if($tce->vlAuxilioTransporte != null)
                <td>R$ {{$tce->vlAuxilioTransporte}}</td>
                @else
                <td>----</td>
                @endif
                @if($tce->vlAuxilioAlimentacao != null)
                <td>R$ {{$tce->vlAuxilioAlimentacao}}</td>
                @else
                <td>----</td>
                @endif
                @if($tce->vlAuxilioMensal != null)
                <td>R$ {{$tce->vlAuxilioMensal}}</td>
                @else
                <td>----</td>
                @endif
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="col-xl-4">
      <div class="card shadow">
        <div class="card-header bg-transparent">
          <h4 class="text-left">Atividades</h4>
        </div>
        <div class="card-body">
          <ul>
            @foreach($tce->atividades as $atividade)
              <li>{{$atividade->atividade}}</li>
            @endforeach
          </ul>
        </div>
      </div>
    </div>
  </div>
  @if($folhas->count() == 0)
  @else
  <div class="row justify-content-center mb-3">
    <div class="col-xl-12">
      <div class="card shadow">
        <div class="card-header border-0">
         <div class="row">
         <h3 class="col-lg-4 mb-0">Pagamentos</h3>
        <h3 class="col-lg-8 mb-0" style="text-align:right"><strong class="badge badge-info" >Estes dados podem sofrer alterações pela concedente conforme frequência e não gera direito de liquidação.</strong></h3>
         </div>
         <small>Demonstrativo dos créditos gerados por competência.</small>
        </div>
        <div class="table-responsive">
          <table class="table align-items-center table-flush">
            <thead class="thead-light">
              <tr>
                <th>Aux. Transporte</th>
                <th>Aux. Transporte por Dia</th>
                <th>Aux. Alimentação</th>
                <th>Aux. Alimentação por Dia</th>
                <th>Aux. Mensal</th>
                <th>Aux. Mensal por Dia</th>
                <th>Aux. Recesso</th>
                <th>Dias Trabalhados</th>
                <th>Data de Pagamento</th>
              </tr>
            </thead>
            <tbody>
              @forelse($folhas as $folha)
              @if($folha->folha->fechado == 'S')
              <tr>
                <td class="text-center">{{$folha->vlAuxilioTransporteReceber != null ? 'R$ '.$folha->vlAuxilioTransporteReceber : "----"}}</td>
                <td class="text-center">{{$folha->vlAuxilioTransporteDia != null ? 'R$ '.$folha->vlAuxilioTransporteDia : "----"}}</td>
                <td class="text-center">{{$folha->vlAuxilioAlimentacaoReceber != null ? 'R$ '.$folha->vlAuxilioAlimentacaoReceber : "----"}}</td>
                <td class="text-center">{{$folha->vlAuxilioAlimentacaoDia != null ? 'R$ '.$folha->vlAuxilioAlimentacaoDia : "----"}}</td>
                <td class="text-center">{{$folha->vlAuxilioMensalReceber != null ? 'R$ '.$folha->vlAuxilioMensalReceber : "----"}}</td>
                <td class="text-center">{{$folha->vlAuxilioMensal != null ? 'R$ '.$folha->vlAuxilioMensal : "----"}}</td>
                <td class="text-center">{{$folha->vlRecesso != null ? 'R$ '.$folha->vlRecesso : "----"}}</td>
                <td class="text-center">{{$folha->diasTrabalhados != null ? $folha->diasTrabalhados : "----"}}</td>
                <td class="text-center">{{$folha->dtPagamento != null ? \Carbon\Carbon::parse($folha->dtPagamento)->format('d/m/Y') : "----"}}</td>
              </tr>
              @endif
              @empty
              <tr>
                <td colspan="9"><p class="text-center"><strong>Nenhum pagamento realizado até o mesmo</strong></p></td>
              </tr>
              @endforelse
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  @endif

  <div class="row justify-content-center mb-3">
    <div class="col-xl-12">
      <div class="card shadow">
        <div class="card-header border-0">
          <h3 class="mb-0">Aditivos</h3>
        </div>
        <div class="table-responsive">
          <table class="table align-items-center table-flush">
            <thead class="thead-light">
              <tr>
                <th style="width:10%;">N°</th>
                <th style="width:40%;">Supervisor</th>
                <th style="width:10%;">Período</th>
                <th style="width:15%;">Data de Início e Fim</th>
                <th style="width:10%;">Data de Criação</th>
                <th style="width:15%; display:none;" class="text-center">Ações</th>
              </tr>
            </thead>
            <tbody>
              @forelse($aditivos as $aditivo)
              <tr>
                <td>{{$aditivo->nnAditivo}}</td>
                <td>{{$aditivo->nmSupervisor != null ? $aditivo->nmSupervisor : $tce->supervisor->nome}}</td>
                <td>{{$aditivo->dsPeriodo != null ? $aditivo->dsPeriodo : "----"}}</td>
                <td>{{\Carbon\Carbon::parse($aditivo->dtInicio)->format('d/m/Y')}} até {{ \Carbon\Carbon::parse($aditivo->dtFim)->format('d/m/Y')}}</td>
                <td>{{\Carbon\Carbon::parse($aditivo->create_at)->format('d/m/Y')}}</td>
                <td class="text-center" style="display:none;">
                  <button type="button" name="button" class="btn btn-default"><i class="fa fa-eye"></i></button>
                </td>
              </tr>
              @empty
              <tr>
                <td colspan="9"><p class="text-center"><strong>Sem Aditivo Neste Tce</strong></p></td>
              </tr>
              @endforelse
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <div class="modal" tabindex="-1" role="dialog" id="check">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Confirmação</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <h3 class="text-left">Atenção!</h3>
          <p class="h4">Ao imprimir o relatório semestral de atividades, você deve preenche-lo e deixar uma cópia na instituição de ensino.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
          <a href="{{ url('painel/estudante/relatorio/baixar')}}" class="btn btn-primary">Imprimir</a>
        </div>
      </div>
    </div>
  </div>
  @endsection
