@extends('layouts.estudante')
@section('css')
<style media="screen">
.lds-ring {
  display: inline-block;
  position: relative;
  width: 64px;
  height: 64px;
}
.lds-ring div {
  box-sizing: border-box;
  display: block;
  position: absolute;
  width: 51px;
  height: 51px;
  margin: 6px;
  border: 6px solid #0e502d;
  border-radius: 50%;
  animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
  border-color: #0e502d transparent transparent transparent;
}
.lds-ring div:nth-child(1) {
  animation-delay: -0.45s;
}
.lds-ring div:nth-child(2) {
  animation-delay: -0.3s;
}
.lds-ring div:nth-child(3) {
  animation-delay: -0.15s;
}
@keyframes lds-ring {
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
}
</style>
@endsection
@section('content')
<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
  @if($tce)
  @if($tce->relatorioSemestrals()->count() > 0)
  @if((\Carbon\Carbon::now()->format('m') - $tce->relatorioSemestrals->last()->created_at->format('m')) == 5 || (\Carbon\Carbon::now()->format('m') - $tce->relatorioSemestrals->last()->created_at->format('m')) == 1)
  <div class="col-xl-4 col-lg-6">
    <div class="alert-warning alert mb-4 mb-xl-0">
      <div class="">
        <div class="row">
          <div class="col">
            <h3 class="card-title text-white mb-0"><i class="fa fa-exclamation-circle"></i> Atenção<button type="button" role="button" class="btn btn-default float-right" title="Imprimir questionário" data-toggle="modal" data-target="#check"><i class="fa fa-file-pdf"></i> Imprimir</button></h3>
            <br>
            <small class="h5 text-white font-weight-bold mb-0">O relatório de atividades semestral está disponível,<br>preencha-o e entre uma via a sua instituição de ensino</small>
          </div>
        </div>
        <p class="mt-3 mb-0 text-muted text-sm">
          <span class="text-nowrap"></span>
        </p>
      </div>
    </div>
  </div>
  @endif
  @elseif((\Carbon\Carbon::now()->format('m') - $tce->dtInicio->format('m')) == 5 || (\Carbon\Carbon::now()->format('m') - $tce->dtInicio->format('m')) == 1)
  <div class="col-xl-4 col-lg-6">
    <div class="alert-warning alert mb-4 mb-xl-0">
      <div class="">
        <div class="row">
          <div class="col">
            <h3 class="card-title text-white mb-0"><i class="fa fa-exclamation-circle"></i> Atenção<a href="#" class="btn btn-default float-right" data-toggle="modal" data-target="#check"><i class="fa fa-file-pdf"></i> Imprimir</a></h3>
            <br>
            <small class="h5 text-white font-weight-bold mb-0">O relatório de atividades semestral está disponível,<br>preencha-o e entre uma via a sua instituição de ensino</small>
          </div>
        </div>
        <p class="mt-3 mb-0 text-muted text-sm">
          <span class="text-nowrap"></span>
        </p>
      </div>
    </div>
  </div>
  @endif
  @endif
</div>
</div>
</div>
</div>
@if($tce)
<!-- Page content -->
<div class="container-fluid mt--7">
  <div class="row justify-content-center mb-3" id="load" style="display:none;">
    <div class="col-md-6">
      <center><div class="lds-ring"><div></div><div></div><div></div><div></div></div></center>
    </div>
  </div>
  <div class="row justify-content-center mb-3" id="estagio">
    <div class="col-xl-8">
      <div class="card shadow">
        <div class="card-header bg-transparent">
          <h4 class="text-left">Dados do Estagiario</h4>
        </div>
        <div class="card-body">
          <table>
            <tbody>
              <tr>
                <th style="width:50%;">Nome</th>
                <th style="width:33%;">Cpf</th>
                <th style="width:33%;">Nascimento</th>
              </tr>
              <tr>
                <td>{{$estudante->nmEstudante}}</td>
                <td>{{$estudante->cdCPF}}</td>
                <td>{{$estudante->dtNascimento->format('d/m/Y')}} </td>
              </tr>
              <tr>
                <th style="width:33%;">Instituição</th>
                <th style="width:33%;">Curso</th>
                <th style="width:17%;">Semestre/Ano</th>
                <th style="width:17%;">Turno</th>
              </tr>
              <tr>
                <td>{{$estudante->instituicao->nmInstituicao }}</td>
                <td>{{$estudante->cursoDaInstituicao->curso->nmCurso}}</td>
                <td style="text-align: center;">{{$estudante->nnSemestreAno != null ? $estudante->nnSemestreAno : '---'}} </td>
                <td>{{$estudante->turno->nmTurno}}</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="col-xl-4">
      <div class="card shadow">
        <div class="card-header bg-transparent">
          <h4 class="text-left">Tces</h4>
        </div>
        <div class="card-body">
          <table>
            <tbody>
              @forelse ($tces as $tce)
              <tr>
                <td style="width:100%;">
                  @if($tce->migracao != NULL)
                  {{$tce->migracao}}
                  @else
                  {{$tce->id}}
                  @endif
                  @if($tce->dtCancelamento != NULL)
                  <span class="badge badge-pill badge-danger text-uppercase">Cancelado</span>
                  @else
                  <span class="badge badge-pill badge-success text-uppercase">Ativo</span>
                  @endif
                </td>
                <td style="width:33%;"><a href="{{route('painel.estudante.tce', ['tce' => base64_encode($tce->id)])}}" class="btn btn-sm btn-primary">ver</a></td>
              </tr>
              @empty
              <tr>
                <p>Sem Tce</p>
              </tr>
              @endforelse

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endif
<div class="container-fluid mt--7">
  <div class="row">
    <div class="col-md-12">
      <div class="card card-default">
        <div class="card-body">
          <h4 class="text-left">Vagas de Estágio</h4>
        </div>
      </div>
    </div>
  </div>
</div>
<br>
<br>
<div class="container-fluid">
  <div class="row">
    @forelse($oportunidades as $oportunidade)
    <!-- col -->
    <div class="col-md-4">
      <div class="card card-default">
        <div class="card-header">
          <div class="row">
            <div class="col-md-8">
              <h5 class="text-left">{{$oportunidade->titulo}}</h5>
            </div>
            <div class="col-md-4">
              <small class="text-right text-gray">cod:{{$oportunidade->code}}</small>
            </div>
          </div>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col">
              <h4 class="card-title text-uppercase text-muted mb-0">Vaga destinada à <strong style="color:;">{{$oportunidade->cidade->nmCidade}} - {{$oportunidade->cidade->estado->cdUF}}</strong></h4>
              <span class="h6 font-weight-bold mb-0"><b>Possui Bolsa:</b> {{$oportunidade->bolsa > 0 ? 'Sim' : 'Não'}}</span>
              <br>
              <span class="h6 font-weight-bold mb-0"><b>Possui Aux. Transporte:</b> {{$oportunidade->auxTransp > 0 ? 'Sim' : 'Não'}}</span>
            </div>
            <div class="col-auto">
              <div class="icon icon-shape bg-success text-white rounded-circle shadow">
                <a href="{{ route('painel.estudante.vaga', ['code'=>$oportunidade->code])}}" class="btn btn-link" style="text-decoration:none;" data-toggle="tooltip" data-placement="right" title="Veja mais sobre a vaga clicando"><i class="fas fa-eye text-white"></i></a>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer">
          <div class="row">
            <div class="col-md-12">
              <small>{{\App\Models\Concedente::find($oportunidade->concedente_id)->nmRazaoSocial}} - {{$oportunidade->created_at->format('d/m/Y')}}</small>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /col -->
    @empty
    <div class="col-md-6">
      <div class="alert alert-info">
        <p class="text-left">Nenhuma vagas disponível na sua área no momento</p>
      </div>
    </div>
    @endforelse
  </div>
  <!-- /row -->
</div>


<div class="modal" tabindex="-1" role="dialog" id="check">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Confirmação</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h3 class="text-left">Atenção!</h3>
        <p class="h4">Ao imprimir o relatório semestral de atividades, você deve preenche-lo e deixar uma cópia na instituição de ensino.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <a href="{{ url('painel/estudante/relatorio/baixar')}}" class="btn btn-primary">Imprimir</a>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script type="text/javascript">

</script>
@endsection
