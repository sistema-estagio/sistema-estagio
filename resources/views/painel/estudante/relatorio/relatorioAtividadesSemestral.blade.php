<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<title>Relatório de Atividades Semestral</title>
	<style>
		html { margin: 30px}
		body {
			font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif;
		}
		footer.fixar-rodape{
			bottom: 0;
			left: 0;
			height: 40px;
			position: fixed;
			width: 100%;
		  }
		.novaPagina {
			page-break-before: always;
			page-break-after: always
		}
		div.body-content{
			/** Essa margem vai evitar que o conteudo fique por baixo do rodapé **/
			margin-bottom: 40px;
		}

		.col-12 {
			width: 100% !important;
		}

		.col-6 {
			width: 50% !important;
		}

		.col-6,
		.col-12 {
			float: left !important;
		}

		.table {
			border-collapse: separate;
			border-spacing: 0;
			min-width: 350px;
			margin-bottom: 10px;
		}

		.table tr th,
		.table tr td {
			border-right: 1px solid #bbb;
			border-bottom: 1px solid #bbb;
			padding: 5px;
		}

		.table tr th:first-child,
		.table tr th:last-child {
			border-top: solid 1px #bbb;
		}

		.table tr th:first-child,
		.table tr td:first-child {
			border-left: 1px solid #bbb;
		}

		.table tr th:first-child,
		.table tr td:first-child {
			border-left: 1px solid #bbb;
		}

		.table tr th {
			background: #eee;
			text-align: left;
		}

		.table.Info tr th,
		.table.Info tr:first-child td {
			border-top: 1px solid #bbb;
		}
		/* top-left border-radius */

		.table tr:first-child th:first-child,
		.table.Info tr:first-child td:first-child {
			border-top-left-radius: 6px;
		}
		/* top-right border-radius */

		.table tr:first-child th:last-child,
		.table.Info tr:first-child td:last-child {
			border-top-right-radius: 6px;
		}

		/* bottom-left border-radius */
		.table tr:last-child td:first-child {
			border-bottom-left-radius: 6px;
		}

		/* bottom-right border-radius */
		.table tr:last-child td:last-child {
			border-bottom-right-radius: 6px;
		}
	</style>
</head>
<body>
	<div id="body">

		<div id="content">

			<div class="page" style="font-size: 7pt">
				<table style="width: 100%;" class="header">
					<tr>
						<td>
							<?php $image_path = '/img/relatorios/logo_default.png'; ?>
							<img src="{{ public_path() . $image_path }}" alt="Upa - Estagio" />
						</td>
						<td>
							<h1 style="text-align: right">
								<strong>
                                    RELATÓRIO DE ATIVIDADES - ESTAGIÁRIO
                                    <br>{{$tce->estudante->nivel->nmNivel}}
								</strong>
							</h1>
						</td>
					</tr>
				</table>
                <br>
				<!--ESTAGIARIO-->
				<table style="width: 100%;" class="table table-bordered table-condensed">
                        <thead>
                            <tr>
                                <th colspan="3" style="text-align: center">
                                    <strong>INFORMAÇÕES</strong>
                                </th>
                            </tr>
                        </thead>
                    <tr>
						<td colspan="2">
							<strong>ESTÁGIARIO: </strong>{{$tce->estudante->nmEstudante}}
						</td>
						<td>
                            <strong>TCE:</strong>
                            @if($tce->migracao != NULL)
                                {{$tce->migracao}}
                            @else
                                {{$tce->id}}
                            @endif
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<strong>CPF: </strong>{{$tce->estudante->cdCPF}}
						</td>
						<td>
                            <strong>COMPETÊNCIA (MÊS/ANO):</strong>
                            @php
                            $hoje = \Carbon\Carbon::now();
                            echo $hoje->month.'/'.$hoje->year;
                            @endphp
						</td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <strong>CONCEDENTE: </strong> {{$tce->concedente->nmRazaoSocial}}
                            ({{$tce->concedente->nmFantasia}})
                        </td>
                    </tr>
					<tr>
						<td colspan="2">
							<strong>ENDEREÇO: </strong> {{$tce->estudante->dsEndereco}},
							<strong> N°</strong> {{$tce->estudante->nnNumero}}
						</td>
						<td>
							<strong>BAIRRO: </strong>{{$tce->estudante->nmBairro}}
						</td>
					</tr>
					<tr>
						<td colspan="1">
							<strong>CIDADE: </strong>{{$tce->estudante->cidade->nmCidade}} - {{$tce->estudante->cidade->estado->nmEstado}}
						</td>
						<td>
							<strong>E-MAIL: </strong>{{$tce->estudante->dsEmail}}
						</td>
						<td>
							<strong>TELEFONE: </strong>{{$tce->estudante->dsFone}}
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<strong>CURSO: </strong> {{$tce->cursoDaInstituicao->curso->nmCurso}}
						</td>
						<td>
							<strong>{{$tce->cursoDaInstituicao->cursoDuracao->nmTipoDuracao}}: </strong>
							{{$tce->nnSemestreAno}} {{$tce->cursoDaInstituicao->cursoDuracao->nmTipoDuracao}}
						 </td>
					</tr>
				</table>

				<div class="texto" style="font-size: 8pt">
						<div class="row" id="cl1">
							<div class="col-sm-12">
                                <p style="text-align: justify" class="termo">Caro Estagiário,</p>
								<p style="text-align: justify" class="termo">
                                    A Lei 11.788/2008 dispõe que a Instituição de Ensino deve exigir do educando o Relatório de Atividades de Estágio em prazo não superior a 6 meses.
                                </p>
                                <p style="text-align: justify" class="termo">
                                        A UPA, a fim de subsidiar as Instituições de Ensino na supervisão e avaliação do estágio de seus alunos, elaborou este documento, cujo preenchimento é obrigatório, conforme consta no Termo de Compromisso de estágio, assinado no início do estágio.
                                </p>
                                <ol class="margin justi"  >
                                    <li style="margin-top:10px; ">
                                        As atividades que você desenvolve estão de acordo com as descritas no Termo de Compromisso de Estagio?
                                        <br> (    ) Sim             (    ) Não
                                    </li>
                                    <li style="margin-top:10px; ">
                                        Você tem encontrado facilidade para solicitar orientações durante o estagio, e, quando as solicita, esclarecimentos são suficientes?
                                        <br> (    ) Sim       (    ) Eventualmente      (    ) Não
                                    </li>
                                    <li style="margin-top:10px; ">
                                        O estágio tem contribuído para melhorar a compreensão de seus direitos e deveres na sociedade?
                                        <br> (    ) Sim (    ) Em parte (    ) Não
                                    </li>
                                    <li style="margin-top:10px; ">
                                        O estágio está proporcionando a busca de novos conhecimentos?
                                        <br> (    ) Sim (    ) Em parte (    ) Não
                                    </li>
                                    <li style="margin-top:10px; ">
                                        O ambiente de estágio é adequado para o desenvolvimento de suas atividades?
                                        <br> (    ) Sim  (    ) Não
                                    </li>
                                    <li style="margin-top:10px; ">
                                        Você avalia que o estágio tem auxiliado na escolha de uma futura carreira profissional?
                                        <br>( ) Sim ( ) Não
                                    </li>
                                    <li style="margin-top:10px; ">
                                        O estágio tem permitido que você adquira uma melhor preparação para o mercado de trabalho?
                                        <br>( ) Sim ( ) Não
                                    </li>
                                    <li style="margin-top:10px; ">
                                        O estágio está sendo um fator determinante na motivação para a continuidade dos estudos?
                                        <br>( ) Sim ( ) Não
                                    </li>
                                    <li style="margin-top:10px; ">
                                        Caso haja possibilidade, você teria interesse em ser contratado pela empresa?
                                        <br>( ) Sim ( ) Não
                                    </li>
                                    </ol>
                                <p style="text-align: justify" class="termo">

                                </p>
							</div>
						</div>


						<br><br><br><br>
						<div class="row body-content">
                                <div class="span6">
                                        <p style="margin: 10px; float: left;">Assinatura: ____________________________________</p>
                                    </div>
							<div class="span6">
								<p style="margin: 10px; float: right;">___________________, _____ de ______________ de _______.</p>
                            </div>
						</div>
                        <br><br><br><br>
                        <div class="texto" style="font-size: 8pt">
                                <div class="row" id="cl1">
                                    <div class="col-sm-12">
                                        <p style="text-align: center" class="termo">Preencher em caso de ALTERAÇÃO DE DADOS</p>
                                        <p style="text-align: center" class="termo">Envio relatório por e-mail – <a href="mailto:estagio@universidadepatativa.com.br">estagio@universidadepatativa.com.br</a></p>
																				<p style="text-align: center" class="termo">Gerado em: {{\Carbon\Carbon::parse($check->create_at)->format('d/m/Y')}} às {{\Carbon\Carbon::parse($check->create_at)->format('h:m')}}</p>
                                    </div>
                                </div>
                        </div>









						@include('tce.relatorio.inc_rodape')
					</div>

			</div>

		</div>
	</div>



</body>

</html>
