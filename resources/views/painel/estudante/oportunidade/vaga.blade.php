@extends('layouts.painel2')
@section('css')
<link rel="stylesheet" href="{{asset('assets/vendor/switch/style.css')}}">
@stop
@section('content')
<!-- Header -->
<div class="header bg-gradient-primary pb-4 pt-5 pt-md-8">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row">
        <div class="col-md-12">
          <h2 class="text-left text-white">
            Oportunidade
            <br>
            <small>Confira esta oportunidade</small>
          </h2>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid">
  <!-- Card stats -->
  <br>
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <div class="row">
            <div class="col-md-12 text-left">
              {{$oportunidade->titulo}}
            </div>
          </div>
        </div>
        
          <div class="card-body">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group row">
                  <div class="col-md-12 col-sm-12">
                    <h4>Vaga</h4>
                    <p><strong>Descrição: </strong><span>{{$oportunidade->obs}}</span></p>
                    @if($oportunidade->bolsa)
                      <p><strong>Valor da Bolsa: </strong><span>{{$oportunidade->bolsa}}</span></p>
                    @endif
                    <p><strong>Carga horária: </strong><span>{{$oportunidade->cargaHorariaSemanal}} horas</span></p>
                    <p><strong>Horário do estágio: </strong><span>{{$oportunidade->horarioInicio}}</span> às <span>{{$oportunidade->horarioFim}}</span></p>
                    <h4>Local de Estágio:</h4>
                    <p><strong>Local: </strong><span>{{$oportunidade->local}}</span></p>
                    <p><strong>Cidade: </strong><span>{{$cidade->nmCidade}}</span></p>
                    <p><strong>Estado: </strong><span>{{$estado->nmEstado}}</span></p>
                    <h4>Requisitos</h4>
                    @if($requisitos)
                      @foreach($requisitos as $requisito)
                        <p>{{$requisito->titulo}}</p>
                      @endforeach
                    @endif
                    <h4>Tarefas</h4>
                    @if($tarefas)
                      @foreach($tarefas as $tarefa)
                        <p>{{$tarefa->descricao}}</p>
                      @endforeach
                    @endif
                    <h4>Contato: </h4>
                    <p><strong>Local de Estágio: </strong><span>{{$oportunidade->local}}</span></p>
                    <button class="btn btn-success">Candidatar-se</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
@stop
@section('js')

@endsection
