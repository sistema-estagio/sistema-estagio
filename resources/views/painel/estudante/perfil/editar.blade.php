@extends('layouts.estudante')
@section('css')
<link type="text/css" href="{{ asset('assets/vendor/toast/toastr.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="header pb-8 pt-5 pt-lg-8 d-flex align-items-center" style="min-height: 600px; background-image: url(../assets/img/cities/recife.jpg); background-size: cover; background-position: bottom center;">
  <!-- Mask -->
  <span class="mask bg-gradient-default opacity-8"></span>
  <!-- Header container -->
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <center>
          <h1 class="text-white">Olá {{ $estudante->nmEstudante }}</h1>
          <p class="text-white mt-0 mb-5">Mantenha sempre seus dados atualizados</p>
        </center>
        @if($errors->any())
        <div class="alert alert-warning alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Alerta!</h4>
            <span class="help-block">
            <ul class="alert-warning">
                <strong>:( Whoops, Houve algum erro! </strong>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
            </span>
        </div>
        @endif
      </div>
    </div>
  </div>
</div>
<!-- Page content -->
<div class="container-fluid mt--7">
  <div class="row">
    <div class="col-xl-12">
      <div class="card bg-secondary shadow">
        <div class="card-header bg-white border-0">
          <div class="row align-items-center">
            <div class="col-md-12">
              <h4>Dados Pessoais e Perfil de Acesso</h4>
            </div>
          </div>
        </div>
        <div class="card-body">
        @if($estudante->tce_id == NULL)
          <form action="{{ route('painel.estudante.perfil.submit') }}" method="post">
            {{ csrf_field() }}
            {{ method_field('PUT') }}
            <h6 class="heading-small text-muted mb-4">Dados Pessoais</h6>
            <div class="row">
                <div class="col-lg-2">
                    <div class="form-group">
                        <label class="form-control-label" for="input-username">CPF:</label>
                        <br> <span style="margin-top: 10px;">{{$estudante->cdCPF}}</span>
                    </div>
                </div>
              <div class="col-lg-10">
                <div class="form-group">
                  <label class="form-control-label" for="nmEstudante">Nome:</label>
                  <input type="text" id="nmEstudante" class="form-control form-control-alternative" placeholder="Nome" value="{{ $estudante->nmEstudante }}" name="nmEstudante">
                </div>
              </div>
              
            </div>
            <div class="row">
                <div class="col-lg-2">
                    <div class="form-group">
                        <label class="form-control-label" for="dtNascimento">Dt. Nascimento:</label>
                        <input type="tel" id="dtNascimento" class="form-control form-control-alternative" value="{{ $estudante->dtNascimento->format('d/m/Y') }}" name="dtNascimento">
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="form-group">
                        <label class="form-control-label" for="cdRG">RG:</label>
                        <input type="tel" id="cdRG" class="form-control form-control-alternative" value="{{ $estudante->cdRG }}" name="cdRG">
                    </div>
                </div>
                <div class="col-lg-1">
                    <div class="form-group">
                        <label class="form-control-label" for="dsOrgaoRG">Expedidor:</label>
                        <input type="tel" id="dsOrgaoRG" class="form-control form-control-alternative" value="{{ $estudante->dsOrgaoRG }}" name="dsOrgaoRG">
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="form-group">
                        <label class="form-control-label" for="dsEstadoCivil">Estado Civil:</label>
                        <select class="form-control" id="dsEstadoCivil" name="dsEstadoCivil">
                            <option value="SOLTEIRO(A)" @if (($estudante->dsEstadoCivil) == "SOLTEIRO(A)") {{ 'selected' }} @endif>SOLTEIRO(A)</option>
                            <option value="CASADO(A)" @if (($estudante->dsEstadoCivil) == "CASADO(A)") {{ 'selected' }} @endif>CASADO(A)</option>
                            <option value="VIUVO(A)" @if (($estudante->dsEstadoCivil) == "VIUVO(A)") {{ 'selected' }} @endif>VIUVO(A)</option>
                            <option value="DIVORCIADO(A)" @if (($estudante->dsEstadoCivil) == "DIVORCIADO(A)") {{ 'selected' }} @endif>DIVORCIADO(A)</option>
                            <option value="OUTRO" @if (($estudante->dsEstadoCivil) == "OUTRO") {{ 'selected' }} @endif>OUTRO</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="form-group">
                        <label class="form-control-label" for="dsSexo">Genero:</label>
                        <select class="form-control" id="dsSexo" name="dsSexo">
                            <option value="MASCULINO" @if (($estudante->dsSexo) == "MASCULINO") {{ 'selected' }} @endif>MASCULINO</option>
                            <option value="FEMININO" @if (($estudante->dsSexo) == "FEMININO") {{ 'selected' }} @endif>FEMININO</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label class="form-control-label" for="nmPai">Nome do Pai:</label>
                        <input type="tel" id="nmPai" class="form-control form-control-alternative" value="{{ $estudante->nmPai }}" name="nmPai">
                    </div>
                </div>
                <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-control-label" for="nmMae">Nome da Mãe:</label>
                            <input type="tel" id="nmMae" class="form-control form-control-alternative" value="{{ $estudante->nmMae }}" name="nmMae">
                        </div>
                </div>
            </div>
            <hr>
            <h6 class="heading-small text-muted mb-4">Endereço e Contatos</h6>
            <div class="row">
                <div class="col-lg-2">
                    <div class="form-group">
                        <label class="form-control-label" for="cdCEP">CEP:</label>
                        <input type="tel" id="cdCEP" class="form-control form-control-alternative" value="{{ $estudante->cdCEP }}" name="cdCEP">
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="form-group">
                        <label class="form-control-label" for="dsEndereco">Endereço:</label>
                        <input type="tel" id="dsEndereco" class="form-control form-control-alternative" value="{{ $estudante->dsEndereco }}" name="dsEndereco">
                    </div>
                </div>
                <div class="col-lg-1">
                    <div class="form-group">
                        <label class="form-control-label" for="nnNumero">N:</label>
                        <input type="tel" id="nnNumero" class="form-control form-control-alternative" value="{{ $estudante->nnNumero }}" name="nnNumero">
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="form-group">
                        <label class="form-control-label" for="dsComplemento">Complemento:</label>
                        <input type="tel" id="dsComplemento" class="form-control form-control-alternative" value="{{ $estudante->dsComplemento }}" name="dsComplemento">
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="form-group">
                        <label class="form-control-label" for="nmBairro">Bairro:</label>
                        <input type="tel" id="nmBairro" class="form-control form-control-alternative" value="{{ $estudante->nmBairro }}" name="nmBairro">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-4">
                    <div class="form-group">
                        <label class="form-control-label" for="estado_id">Estado:</label>
                        <select class="form-control" id="estado_id" name="estado_id" required>
                            <option value="" selected="">Selecione...</option>
                            @foreach($estados as $estado)
                                <option value="{{ $estado['id'] }}" @if(($estudante->estado_id) == $estado['id']) {{ 'selected' }} @endif>{{ $estado['nmEstado'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label class="form-control-label" for="cidade_id">Cidade:</label>
                        <select class="form-control" id="cidade_id" name="cidade_id">
                            <option value="{{$estudante->cidade->id}}" selected>{{$estudante->cidade->nmCidade}}</option>
                            <option value="">----------</option>
                            @foreach($cidades as $cidade)
                                <option value="{{ $cidade->id }}">{{ $cidade->nmCidade }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="form-group">
                        <label class="form-control-label" for="dsFone">Telefone:</label>
                        <input type="tel" id="dsFone" class="form-control form-control-alternative" value="{{ $estudante->dsFone }}" name="dsFone">
                    </div>
                </div>
                <div class="col-lg-2">
                        <div class="form-group">
                            <label class="form-control-label" for="dsOutroFone">Outro Fone:</label>
                            <input type="tel" id="dsOutroFone" class="form-control form-control-alternative" value="{{ $estudante->dsOutroFone }}" name="dsOutroFone">
                        </div>
                </div>
            </div>

            <div class="row">
                    <div class="col-lg-2">
                        <div class="form-group">
                            <label class="form-control-label" for="dsEmail">E-mail:</label>
                            <input type="tel" id="dsEmail" class="form-control form-control-alternative" value="{{ $estudante->dsEmail }}" name="dsEmail">
                        </div>
                    </div>
            </div>
            <hr>
            <h6 class="heading-small text-muted mb-4">Dados de Ensino</h6>
            <div class="row">
                <div class="col-lg-8">
                    <div class="form-group">
                        <label class="form-control-label" for="instituicao_id">Instituição de Ensino:</label>
                        <select class="form-control" name="instituicao_id" id="instituicao_id">
                            <option value="{{$estudante->instituicao->id}}" selected>{{$estudante->instituicao->nmInstituicao}}</option>
                            <option value="">----------</option>
                            @foreach($instituicoes as $instituicao)
                                <option value="{{$instituicao['id']}}">{{$instituicao['nmInstituicao']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label class="form-control-label" for="curso_id">Curso:</label>
                        <select class="form-control" id="curso_id" name="curso_id">
                            <option value="{{$estudante->cursoDaInstituicao->id}}" selected>{{$estudante->cursoDaInstituicao->curso->nmCurso}}</option>
                            <option value="">----------</option>
                            @foreach($cursos as $curso)
                                <option value="{{$curso['id']}}">{{$curso->curso->nmCurso}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <div class="form-group">
                        <label class="form-control-label" for="nivel_id">Nivel:</label>
                        <select class="form-control" id="nivel_id" name="nivel_id">
                            <option value="{{$estudante->cursoDaInstituicao->curso->nivel->id}}" selected>{{$estudante->cursoDaInstituicao->curso->nivel->nmNivel}}</option>
                            <option value="">----------</option>
                            @foreach($niveis as $nivel)
                                <option value="{{ $nivel->id }}" @if(old('nivel_id') == $nivel->id) {{ 'selected' }} @endif>{{ $nivel->nmNivel }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label class="form-control-label" for="turno_id">Turno:</label>
                        <select class="form-control" id="turno_id" name="turno_id">
                            <option value="{{$estudante->turno->id}}" selected>{{$estudante->turno->nmTurno}}</option>
                            <option value="">----------</option>
                            @foreach($turnos as $turno)
                                <option value="{{ $turno->id }}" @if(old('turno_id') == $turno->id) {{ 'selected' }} @endif>{{ $turno->nmTurno }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label class="form-control-label" for="nnSemestreAno">Semestre/Ano:</label>
                        <input type="tel" id="nnSemestreAno" class="form-control form-control-alternative" value="{{ $estudante->nnSemestreAno }}" name="nnSemestreAno">
                    </div>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary float-right"><i class="fa fa-save"></i> Salvar</button>
                </div>
            </div>
          </form>
          @else
            <div class="row">
                <div class="col-lg-12">
                    <strong>Dados Pessoais</strong>
                    <br>
                    <div class="alert alert-warning" role="alert">
                        <strong>Atenção:</strong> Você não pode alterar seus dados enquanto tem Termo de Compromisso Ativo. Entre em contato!
                    </div>
                </div>
            </div>
          @endif
          <form action="{{ route('painel.estudante.perfil.acesso.submit') }}" method="post">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
            <hr>
            <div class="row">
              <div class="col-lg-12">
                <strong>Perfil de Acesso</strong>
                <br>
                <small>Caso deseje mudar a senha, será necessário preencher o campo da senha atual e após isso colocar a nova senha.</small>
              </div>
            </div>
            <br>
            <div class="row">
              <div class="col-lg-6">
                <div class="form-group">
                  <label class="form-control-label" for="input-username">Senha Atual</label>
                  <input type="password" id="input-username" class="form-control form-control-alternative" placeholder="Senha" value="" name="old_password">
                </div>
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                  <label class="form-control-label" for="input-email">Nova Senha</label>
                  <input type="password" id="input-email" class="form-control form-control-alternative" placeholder="Nova Senha" value="" name="new_password">
                </div>
              </div>
            </div>
            <div class="row form-group">
              <div class="col-md-12">
                <button type="submit" class="btn btn-primary float-right"><i class="fa fa-save"></i> Salvar</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script src="{{ asset('assets/vendor/toast/toastr.js')}}"></script>

<script type="text/javascript">
    //cidades do estado
    $('select[name=estado_id]').change(function (){
        var idEstado = $(this).val();
        $.get('../../../get-cidades/' + idEstado, function (cidades){
            $('select[name=cidade_id]').empty();
            $('#cidade_id').append('<option value=""selected="selected">Selecione a Cidade</option>');
            $('#cidade_id').append('<option value="">----------</option>');
            $.each(cidades, function (key, value){
                $('select[name=cidade_id]').append('<option value=' + value.id + '>' + value.nmCidade + '</option>');
            });
        });
      });
      //Instituicao por estado
      $('select[name=estado_id]').change(function (){
        var idEstado = $(this).val();
        $.get('../../../get-instituicao-estado/' + idEstado, function (cidades){
            $('select[name=instituicao_id]').empty();
            $('#instituicao_id').append('<option value=""selected="selected">Selecione a Instituição</option>');
            $('#instituicao_id').append('<option value="">----------</option>');
            $.each(cidades, function (key, value){
                $('select[name=instituicao_id]').append('<option value=' + value.id + '>' + value.nmInstituicao + '</option>');
            });
        });
      });
      //Cursos da Instituição
      $('select[name=instituicao_id]').change(function (){
        var idInstituicao = $(this).val();
  
        $.get('../../../get-cursos-instituicao/' + idInstituicao, function (cursos){
            $('select[name=curso_id]').empty();
            $('#curso_id').append('<option value=""selected="selected">Selecione o Curso</option>');
            $('#curso_id').append('<option value="">----------</option>');
            $.each(cursos, function (key, value){
                $('select[name=curso_id]').append('<option value=' + value.id + '>' + value.nmCurso + '</option>');
            });
        });
      });
  
      //Niveis do Curso da Instituição
      $('select[name=curso_id]').change(function (){
            var idCurso = $(this).val();
  
            $.get('../../../get-nivel-curso-instituicao/' + idCurso, function (niveis){
  
                $('select[name=nivel_id]').empty();
                $('#nivel_id').append('<option value=""selected="selected">Selecione o Nivel</option>');
                $('#nivel_id').append('<option value="">----------</option>');
                $.each(niveis, function (key, value){
                    $('select[name=nivel_id]').append('<option value=' + value.id + '>' + value.nmNivel + '</option>');
                });
            });
      });
  
      //Turnos do Curso na Instituição
      $('select[name=curso_id]').change(function (){
            var idCurso = $(this).val();
  
            $.get('../../../get-turno-curso-instituicao/' + idCurso, function (turnos){
                $('select[name=turno_id]').empty();
                $('#turno_id').append('<option value=""selected="selected">Selecione o Turno</option>');
                $('#turno_id').append('<option value="">----------</option>');
                $.each(turnos, function (key, value){
                    $('select[name=turno_id]').append('<option value=' + value.id + '>' + value.nmTurno + '</option>');
                });
            });
      });
    $(document).ready(function(){
        //$('#cidade_id').select2();
        //$('#instituicao_id').select2();
        //maskara nova de telefones
        var SPMaskBehavior = function (val) {
            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
          },
          spOptions = {
            onKeyPress: function(val, e, field, options) {
                field.mask(SPMaskBehavior.apply({}, arguments), options);
              }
          };

          $('#dsFone').mask(SPMaskBehavior, spOptions);
          $('#dsOutroFone').mask(SPMaskBehavior, spOptions);
    });
$("#dtNascimento").mask('00/00/0000');
$("#cdCEP").mask("99999-999");

toastr.options = {
  "closeButton": false,
  "debug": false,
  "newestOnTop": false,
  "progressBar": false,
  "positionClass": "toast-bottom-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "5000",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}

@if(session('status'))
  @if(session('status') != null)
    toastr['{{session("status")}}']('{{session("msg")}}');
  @endif
@endif

</script>
<!-- Adicionando Javascript -->
<script type="text/javascript" >
    
                $(document).ready(function() {
                    function limpa_formulário_cep() {
                        // Limpa valores do formulário de cep.
                        $("#dsEndereco").val("");
                        $("#nmBairro").val("");
                    }
    
                    //Quando o campo cep perde o foco.
                    $("#cdCEP").blur(function() {
    
                        //Nova variável "cep" somente com dígitos.
                        var cep = $(this).val().replace(/\D/g, '');
    
                        //Verifica se campo cep possui valor informado.
                        if (cep != "") {
    
                            //Expressão regular para validar o CEP.
                            var validacep = /^[0-9]{8}$/;
    
                            //Valida o formato do CEP.
                            if(validacep.test(cep)) {
    
                                //Preenche os campos com "..." enquanto consulta webservice.
                                $("#dsEndereco").val("...");
                                $("#nmBairro").val("...");
    
                                //Consulta o webservice viacep.com.br/
                                $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {
    
                                    if (!("erro" in dados)) {
                                        //Atualiza os campos com os valores da consulta.
                                        $("#dsEndereco").val(dados.logradouro);
                                        $("#nmBairro").val(dados.bairro);
                                        $("#nnNumero").focus();
                                    } //end if.
                                    else {
                                        //CEP pesquisado não foi encontrado.
                                        limpa_formulário_cep();
                                        alert("CEP não encontrado.");
                                    }
                                });
                            } //end if.
                            else {
                                //cep é inválido.
                                limpa_formulário_cep();
                                alert("Formato de CEP inválido.");
                            }
                        } //end if.
                        else {
                            //cep sem valor, limpa formulário.
                            limpa_formulário_cep();
                        }
                    });
                });
    </script>
@endsection
