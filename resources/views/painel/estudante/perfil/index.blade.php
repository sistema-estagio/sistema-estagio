@extends('layouts.estudante')
@section('css')
<link type="text/css" href="{{ asset('assets/vendor/toast/toastr.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="header pb-8 pt-5 pt-lg-8 d-flex align-items-center" style="min-height: 600px; background-image: url(../assets/img/cities/recife.jpg); background-size: cover; background-position: bottom center;">
  <!-- Mask -->
  <span class="mask bg-gradient-default opacity-8"></span>
  <!-- Header container -->
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <center>
          <h1 class="text-white">Olá {{ $estudante->nmEstudante }}</h1>
          <p class="text-white mt-0 mb-5">Mantenha sempre seus dados atualizados</p>
        </center>
      </div>
    </div>
  </div>
</div>
<!-- Page content -->
<div class="container-fluid mt--7">
  <div class="row">
    <div class="col-xl-12">
      <div class="card bg-secondary shadow">
        <div class="card-header bg-white border-0">
          <div class="row align-items-center">
            <div class="col-md-12">
              <h4>Dados Pessoais</h4>
            </div>
          </div>
        </div>
        <div class="card-body">
            <table class="table table-condensed table-striped">
                <tbody>
                  <tr>
                    <td colspan="1"><label><strong>CPF:</strong></label> {{$estudante->cdCPF}}</td>
                    <td colspan="1"><label><strong>RG:</strong></label> {{$estudante->cdRG}} {{$estudante->dsOrgaoRG}}</td>
                    <td colspan="1"><label><strong>Estado Civil:</strong></label> {{$estudante->dsEstadoCivil}}</td>
                    <td colspan="1"><label><strong>Genero:</strong></label> {{$estudante->dsSexo}}</td>
                    <td colspan="1"><label><strong>Nascimento:</strong></label> {{ date('d/m/Y',strtotime($estudante->dtNascimento)) }}</td>
                  </tr>
                  <tr>
                    <td colspan="2"><label><strong>Pai:</strong></label><br>@if($estudante->nmPai){{$estudante->nmPai}}@else Não Informado @endif</td>
                    <td colspan="3"><label><strong>Mãe:</strong></label><br>@if($estudante->nmMae){{$estudante->nmMae}}@else Não informado @endif</td>
                  </tr>
                  
                  <tr>
                    <td colspan="1">
                      <label><strong>Instituicao de Ensino:</strong></label><br>
                      {{$estudante->instituicao->nmInstituicao}}
                    </td>
                    <td colspan="1">
                      <label><strong>Curso:</strong></label><br>
                      {{$estudante->cursoDaInstituicao->curso->nmCurso}}
                    </td>
                    <td colspan="1">
                      <label><strong>Nível:</strong></label><br>
                      {{$estudante->cursoDaInstituicao->curso->nivel->nmNivel}}
                    </td>
                    <td colspan="1">
                      <label><strong>Semestre/Ano:</strong></label><br>
                      {{$estudante->nnSemestreAno}} {{$estudante->cursoDaInstituicao->cursoDuracao->nmTipoDuracao}}
                    </td>
                    <td colspan="1">
                      <label><strong>Turno:</strong></label><br>
                      {{$estudante->turno->nmTurno}}
                    </td>
                  </tr>
                
                  <tr>
                    <td colspan="6">
                      <label><strong>Endereço:</strong></label>
                      {{$estudante->dsEndereco}},
                      <strong>N°</strong> {{$estudante->nnNumero}} 
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2"><label><strong>Bairro:</strong></label><br>{{$estudante->nmBairro}}</td>
                    <td colspan="1"><label><strong>Cidade:</strong></label><br>{{$estudante->cidade->nmCidade}}</td>
                    <td colspan="1"><label><strong>Estado:</strong></label><br>{{$estudante->cidade->estado->nmEstado}}</td>
                    <td colspan="1"><label><strong>Cep:</strong></label><br>{{$estudante->cdCEP}}</td>
                  </tr>
                  <tr>
                    <td colspan="1"><label><strong>Telefone:</strong></label> {{$estudante->dsFone}}</td>
                    <td colspan="2"><label><strong>Outro:</strong></label> {{$estudante->dsOutroFone}}</td>
                    <td colspan="2"><label><strong>E-mail:</strong></label> {{$estudante->dsEmail}}</td>
                  </tr>
                  {{-- <tr>
                    <td colspan="6">
                      <label>Dados Bancários: </label><br>
                      @if($estudante->dsBanco != NULL)
                        <label>Banco:</label> {{$estudante->dsBanco}} <label> | Agencia:</label> {{$estudante->dsAgencia}} <label> | Conta:</label> {{$estudante->dsConta}} <label> | Favorecido:</label> {{$estudante->dsFavorecido}}
                      @else
                        Não Informado!
                      @endif
                    </td> --}}
                  </tr>
                  
                  <tr>
                    <td colspan="6" align="right">
                      <label><strong>Cadastrado em: </strong></label>
                      {{$estudante->created_at->format("d/m/Y H:i:s")}}
                      @if ($estudante->created_at->format("d/m/Y H:i:s") != $estudante->updated_at->format("d/m/Y H:i:s"))
                      <br><label><strong>Atualizado em:</strong></label> {{$estudante->updated_at->format("d/m/Y H:i:s")}}
                      @endif
                      {{-- @if($estudante->user_id !== NULL)
                        <br><label>Cadastrado por:</label> {{$estudante->userCadastro->name}}
                      @endif --}}
                    </td>
                  </tr>
                </tbody>
              </table>
              <hr>
            
            <div class="row form-group">
              <div class="col-md-12">
                <center><a href="{{route('painel.estudante.perfil.editar')}}" class="btn btn-warning" role="button" aria-pressed="true"><i class="fa fa-edit"></i> Editar</a></center>
              </div>
            </div>

            
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script src="{{ asset('assets/vendor/toast/toastr.js')}}"></script>

<script type="text/javascript">
$(".cellphone").mask('(00) 9 0000-0000');
$(".code-postal").mask('00000-000');
$(".document-rg").mask('00000000000');
$(".date-expedition").mask('00/00/0000');

toastr.options = {
  "closeButton": false,
  "debug": false,
  "newestOnTop": false,
  "progressBar": false,
  "positionClass": "toast-bottom-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "5000",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}

@if(session('status'))
  @if(session('status') != null)
    toastr['{{session("status")}}']('{{session("msg")}}');
  @endif
@endif

</script>
@endsection
