<!DOCTYPE html>
<html>

<head>

  <title>sysEstagio - Área Mediador</title>
  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <!-- Icons -->
  <link href="{{ asset('assets/vendor/nucleo/css/nucleo.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
  <!-- Argon CSS -->
  <link type="text/css" href="{{ asset('assets/css/argon.css') }}" rel="stylesheet">
  <link type="text/css" href="{{ asset('assets/vendor/toast/toastr.css') }}" rel="stylesheet">
  <link type="text/css" href="{{ asset('assets/vendor/datatable/datatable.css') }}" rel="stylesheet">
  <style>
  btn {
    border: none;
  }

  #saveButton {
    display: none;
  }

</style>
</head>
<body>
  <div class="main-content">
    <!-- Header -->
    <div class="header bg-gradient-primary pb-4 pt-5 pt-md-8">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row">
            <div class="col-md-6">
              <h3 class="text-left text-white">Resgistro de faltas</h3>
              <h5 class="text-left text-white">{{Auth::user()->name}}</h5>
            </div>
            <div class="col-md-3">
            <a href="JavaScript:void(0);" class="btn btn-default" data-toggle="modal" data-target="#modalChangePassword" style="left:94.2%;"><i class="fa fa-key"></i> Alterar Senha</a>
              <a href="{{ route('painel.concedente.logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="btn btn-default text-right float-right" style="left:95%;"><i class="fas fa-sign-out-alt"></i> Sair</a>
              <form id="logout-form" action="{{ route('painel.concedente.logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="modal" tabindex="-1" role="dialog" id="modalConfirmLockFolha">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Confirmação de Fechamento de Folha</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{ route('painel.concedente.folha.fechar', ['hash'=>$folha->hash]) }}" method="post">
      {{ csrf_field() }}
      <div class="modal-body">
        <div class="input-group row">
          <div class="col-md-12">
            <label for="" style="text-align:justify;">
              <input type="checkbox" name="confirm" value="" required> Concordo que ao confirmar declaro que os dados abaixo estão conformes ao fatos reais e por assim fecha a folha para que seja gerado a documentação referente ao pagamento da mesma.
            </label>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button type="submit" class="btn btn-primary">Confirmar</button>
      </div>
      </form>
    </div>
  </div>
</div>
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-md-12">
          @if($folha)
          <div class="card shadow mt-4">
            <div class="card-header">
              <div class="row">
                <div class="col-md-5">
                  <small class="text-muted">Registro de Faltas</small>
                  <br>
                  <h3 class="mb-0">{{ $folha->referenciaMes.'/'.$folha->referenciaAno}} | {{$folha->secConcedente_id != null ? $folha->secConcedente->nmSecretaria : $folha->concedente->nmRazaoSocial}}</h3>
                </div>
                <div class="col-md-3">
                  @if($folha->fechado == "S")
                  <div class="alert alert-success float-left" style="width:60%;">
                    <i class="fa fa-check"></i> Folha já foi fechada
                  </div>
                  @else
                  <button type="button" name="button" class="btn btn-danger" data-toggle="modal" data-target="#modalConfirmLockFolha"><i class="fas fa-lock"></i> Fechar Folha</button>
                  @endif
                </div>
                <div class="col-md-4">
                  <div class="input-group">
                    <input type="search" name="search" value="" class="form-control" placeholder="Buscar por nome,cpf,tce..." onkeyup="busca(this.value)">
                    <span class="input-group-append">
                      <span class="input-group-text"><i class="fa fa-search"></i></span>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                @if($folha->fechado === 'N' && ($folha->created_at->format('Y-m-d') <= \Carbon\Carbon::now()->format('Y-m-d')))
                <table class="table align-items-center table-flush datatable">
                  <thead class="thead-light">
                    <tr>
                      <th class="text-center" style="width: 5%"scope="col">#</th>
                      <th style="width: 30%" scope="col">Nome <br>
                        <small>CPF</small></th>
                        <th class="text-center"  style="width: 10%" scope="col">Início-Fim</th>
                        <th class="text-center" style="width: 10%" scope="col">Dias Trabalhados</th>
                        <th class="text-center" style="width: 10%" scope="col">Faltas</th>
                        <th class="text-center" style="width: 10%" scope="col">Justificadas</th>
                        <th class="text-center" style="width: 15%" scope="col">Ações</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($items as $item)
                      <tr class="items" data-nome="{{ $item->tce->estudante->nmEstudante }}" data-tce="{{ $item->tce->id }}" data-cpf="{{ $item->tce->estudante->cdCPF }}">
                        <th scope="row">{{ $loop->iteration }}</th>
                        <td>{{ $item->tce->estudante->nmEstudante }}<br><span class="text-muted">CPF:</span>{{$item->tce->estudante->cdCPF}}</td>
                        <td class="text-center">
                          {{ $item->tce->dtInicio->format('d/m/Y') }} até {{ $item->tce->dtFim->format('d/m/Y') }}
                          <input type="hidden" name="id" id="input-id_{{ $loop->iteration }}" value="{{$item->id}}">
                        </td>
                        <td class="text-center">
                          <span id="span-base_{{ $loop->iteration }}" class="span_{{ $loop->iteration }}">{{ $item->diasBaseEstagiados != null ? $item->diasBaseEstagiados : 30}}</span>
                          <input type="hidden" class="form-control input_edit_{{ $loop->iteration }}" id="input-base_{{ $loop->iteration }}" value="{{ $item->diasBaseEstagiados != null ? $item->diasBaseEstagiados : 30}}">
                        </td>
                        <td class="text-center">
                          <span id="span-faltas_{{ $loop->iteration }}" class="span_{{ $loop->iteration }}">{{ $item->faltas != null ? $item->faltas : 0 }}</span>
                          <input type="hidden" class="form-control input_edit_{{ $loop->iteration }}" id="input-faltas_{{ $loop->iteration }}" value="{{ $item->faltas != null ? $item->faltas : 0 }}">
                        </td>
                        <td class="text-center">
                          <span id="span-justificadas_{{ $loop->iteration }}" class="span_{{ $loop->iteration }}">{{ $item->justificadas != null ? $item->justificadas : 0 }}</span>
                          <input type="hidden" class="form-control input_edit_{{ $loop->iteration }}" id="input-justificadas_{{ $loop->iteration }}" value="{{ $item->justificadas != null ? $item->justificadas : 0 }}">
                        </td>
                        <td class="text-center">
                          <button class="btn btn-primary" id="btn_edit_{{ $loop->iteration }}" onClick="showInputs({{ $loop->iteration }});"><i class="fa fa-edit"></i> Editar</button>
                          <button class="btn btn-success" id="btn_save_{{ $loop->iteration }}" style="display:none" onClick="submitDados({{ $loop->iteration }})"><i class="fa fa-save"></i> Salvar</button>
                          <button class="btn btn-danger" id="btn_cancel_{{ $loop->iteration }}" style="display:none" onClick="cancelDados({{ $loop->iteration }})"><i class="fa fa-times"></i> Cancelar</button>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                  @else
                  <table class="table align-items-center table-flush datatable">
                    <thead class="thead-light">
                      <tr>
                        <th class="text-center" style="width: 5%" scope="col">#</th>
                        <th style="width: 30%" scope="col">Nome <br>
                          <small>CPF</small></th>
                          <th class="text-center"  style="width: 10%" scope="col">Início-Fim</th>
                          <th class="text-center" style="width: 10%" scope="col">Dias Trabalhados</th>
                          <th class="text-center" style="width: 10%" scope="col">Faltas</th>
                          <th class="text-center" style="width: 10%" scope="col">Justificadas</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($items as $item)
                        <tr class="items" data-nome="{{ $item->tce->estudante->nmEstudante }}" data-tce="{{ $item->tce->id }}" data-cpf="{{ $item->tce->estudante->cdCPF }}">
                          <th scope="row">{{ $loop->iteration }}</th>
                          <td>{{ $item->tce->estudante->nmEstudante }}<br><span class="text-muted">CPF:</span>{{$item->tce->estudante->cdCPF}}</td>
                          <td class="text-center">
                            {{ $item->tce->dtInicio->format('d/m/Y') }} até {{ $item->tce->dtFim->format('d/m/Y') }}
                            <input type="hidden" name="id" id="input-id_{{ $loop->iteration }}" value="{{$item->id}}">
                          </td>
                          <td class="text-center">
                            <span id="span-base_{{ $loop->iteration }}" class="span_{{ $loop->iteration }}">{{ $item->diasBaseEstagiados != null ? $item->diasBaseEstagiados : 30}}</span>
                          </td>
                          <td class="text-center">
                            <span id="span-faltas_{{ $loop->iteration }}" class="span_{{ $loop->iteration }}">{{ $item->faltas != null ? $item->faltas : 0 }}</span>
                          </td>
                          <td class="text-center">
                            <span id="span-justificadas_{{ $loop->iteration }}" class="span_{{ $loop->iteration }}">{{ $item->justificadas != null ? $item->justificadas : 0 }}</span>
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                    @endif
                  </div>
                </div>
              </div>
              @else
              <br>
              <div class="center alert bg-orange" style="width:50%;">
                <h4 class="text-center">Não existe nenhuma folha disponível para registro no momento!</h4>
              </div>
              @endif
            </div>
          </div>
        </div>
        <!-- Footer -->
        @if($folha)
        <footer class="footer">
        @else
        <footer class="footer" style="margin-top:500px;">
        @endif
          <div class="container-fluid">
            <div class="row align-items-center justify-content-center">
              <div class="col-md-6 col-md-offset-3">
                <div class="copyright text-center text-muted">
                  &copy; 2018 <a href="http://www.universidadepatativa.com.br" class="font-weight-bold ml-1" target="_blank">sysEstagio</a>
                </div>
              </div>
            </div>
          </div>
        </footer>
      </div>
      <div class="modal" tabindex="-1" role="dialog" id="modalChangePassword">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Alterar Senha</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form id="form-post">
            <div class="modal-body">
              <div class="form-group row">
                <div class="col-md-12">
                  <label for="">Senha Atual</label>
                  <input type="text" name="password" class="form-control" value="" required>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-12">
                  <label for="">Nova Senha</label>
                  <input type="text" name="new_password" class="form-control" value="" required>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
              <button type="button" class="btn btn-primary" onclick="changePassword()">Alterar</button>
            </div>
          </form>
        </div>
      </div>
    </div>
      <!-- Argon Scripts -->
      <!-- Core -->
      <script src="{{ asset('assets/vendor/datatable/jquery-3.3.1.js') }}"></script>
      <script src="{{ asset('assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
      <!-- Argon JS -->
      <script src="{{ asset('assets/js/argon.js') }}"></script>
      <script src="{{ asset('assets/vendor/toast/toastr.js') }}"></script>
      <script src="{{ asset('startmin-master/js/dataTables/jquery.dataTables.min.js') }}"></script>
      <script src="{{ asset('assets/vendor/datatable/datatable.min.js') }}"></script>
      <script src="{{ asset('assets/vendor/mask/jquery.mask.min.js') }}"></script>
      <script type="text/javascript">
      toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-bottom-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
      }
      </script>
      <script>
      localStorage.clear();
      $(".datatable").DataTable({
        "searching": false,
        "lengthChange": false,
        "columnDefs": [
          { "orderable": false, "targets": 'no-sort' }
        ],
        "pageLength": 25,
        "language": {
          "sEmptyTable": "Nenhum registro encontrado",
          "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
          "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
          "sInfoFiltered": "(Filtrados de _MAX_ registros)",
          "sInfoPostFix": "",
          "sInfoThousands": ".",
          "sLengthMenu": "_MENU_ resultados por página",
          "sLoadingRecords": "Carregando...",
          "sProcessing": "Processando...",
          "sZeroRecords": "Nenhum registro encontrado",
          "sSearch": "Pesquisar",
          "oPaginate": {
            "sNext": "Próximo",
            "sPrevious": "Anterior",
            "sFirst": "Primeiro",
            "sLast": "Último"
          },
          "oAria": {
            "sSortAscending": ": Ordenar colunas de forma ascendente",
            "sSortDescending": ": Ordenar colunas de forma descendente"
          }
        }
      });
      @isset($folha)
      @if($folha->fechado == "N")
      function busca(nome){
        var tces = $(".items");
        var nothing = $('#nenhum');
        var load = $("#loadCancelados");
        filtra(nome,tces,nothing,load);
      }

      function filtra(nome,tces,nothing,load){
        nothing.hide();
        var count = 0;
        tces.show();
        if (nome != "") {
          for (var i = 0; i < tces.length; i++) {
            if (tces[i].dataset.nome.toLowerCase().search(nome.toLowerCase()) && tces[i].dataset.tce.toLowerCase().search(nome.toLowerCase()) && tces[i].dataset.cpf.search(nome)) {
              tces[i].style.display = "none";
            }else{
              count++;
            }
          }
          if(count == 0){
            nothing.show();
          }
        }
      }

      function showInputs(id) {

        //var editButton = document.getElementById('editButton');
        $('#btn_edit_'+id).hide();
        $('#btn_save_'+id).show();
        $('#btn_cancel_'+id).show();
        $('.input_edit_'+id).attr('type', 'text');
        $('.span_'+id).hide();
        localStorage.setItem('obj_base_'+id, $('#input-base_'+id).val());
        localStorage.setItem('obj_faltas_'+id, $('#input-faltas_'+id).val());
        localStorage.setItem('obj_justificadas_'+id, $('#input-justificadas_'+id).val());
      };

      function submitDados(id) {
        var itensAdd = [];
        var item = {};

        item = {
          'id' : $('#input-id_'+id).val(),
          'dias': $('#input-base_'+id).val(),
          'faltas': $('#input-faltas_'+id).val(),
          'justificadas': $('#input-justificadas_'+id).val(),
        };

        $.post('{{route("painel.concedente.folha.item.update", ["hash"=>$folha->hash])}}', { 'item': item, '_token':'{{csrf_token()}}' }, function(response){
          if(response != null){
            if(response.status == "success"){
              $('#span-base_'+id).html(item.dias);
              $('#span-faltas_'+id).html(item.faltas);
              $('#span-justificadas_'+id).html(item.justificadas);
              //
              $('#btn_edit_'+id).show();
              $('#btn_save_'+id).hide();
              $('#btn_cancel_'+id).hide();
              $('.input_edit_'+id).attr('type', 'hidden');
              $('.span_'+id).show();
              toastr.success('O item '+id+' foi editado com sucesso');
            }else{
              toastr.success('Não foi posssível editar o item '+ id);
            }
          }
        });
      }

      function cancelDados(id){
        $('#btn_edit_'+id).show();
        $('#btn_save_'+id).hide();
        $('#btn_cancel_'+id).hide();
        //
        $('#input-base_'+id).val(localStorage.getItem('obj_base_'+id));
        $('#input-faltas_'+id).val(localStorage.getItem('obj_faltas_'+id));
        $('#input-justificadas_'+id).val(localStorage.getItem('obj_justificadas_'+id));
        //
        $('.input_edit_'+id).attr('type', 'hidden');
        $('.span_'+id).show();
      }
      @endif
      @endisset

      function changePassword(){
        var password = $('input[name=password]').val();
        var new_password = $('input[name=new_password]').val();
        var validate = true;

        if(password == null){
          toastr.info('A senha atual é necessário');
          validate = false;
        }

        if(new_password == null){
          toastr.info('A nova senha é necessário');
          validate = false;
        }else if(new_password.length < 8){
          toastr.info('A nova senha precisa de no mínimo 8 caracteres');
          validate = false;
        }

        if(validate == true){
          $.post("{{ route('painel.concedente.alterarSenha.submit') }}", { 'password': password, 'newpassword': new_password, '_token': '{{ csrf_token() }} '}, function(response){
            if(response != null){
              if(response.status == 'success'){
                $(this).find('input[name=password]').val('');
                $(this).find('input[name=new_password]').val('');
                toastr.success(response.msg);
                $('#modalChangePassword').modal('hide');
              }else if(response.status == 'error'){
                toastr.error(response.msg);
              }
            }else{

            }
          });
        }
      }
      </script>
    </body>

    </html>
