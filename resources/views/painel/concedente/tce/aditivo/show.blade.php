@extends('layouts.painel2')
@section('title_postfix', ' - '.$titulo)
@section('conteudo')
<div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                {{-- <h1 class="page-header">Tce {{$tce->id}} - {{$tce->estudante->nmEstudante}}</h1> --}}
                <h1 class="page-header">Tce
                    @if($tce->migracao != NULL)
                        {{$tce->migracao}}
                    @else
                        {{$tce->id}}
                    @endif
                    - {{$tce->estudante->nmEstudante}}
                </h1>

            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <a href="{{route('painel.concedente.estudante', base64_encode($tce->id))}}" class="btn btn-warning ad-click-event">Dados do Estágiário</a>
                <a href="{{route('painel.concedente.instituicao', base64_encode($tce->id))}}" class="btn btn-primary ad-click-event">Dados da Instituição de Ensino</a>
                {{-- {{route('concedente.financeiro.relatorio', ['concedente' => $folha->concedente->id,'folha' => $folha->id])}} --}}
                <a href="{{route('painel.concedente.folha', base64_encode($tce->id))}}" class="btn btn-success ad-click-event">Folha de Pagamento</a>
            </div>
        </div>
        <br>

        <div class="row">
            <div class="col-lg-12">
                <!--
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Ultimos Tce's
                    </div>
                    <div class="panel-body"></div>
                </div>
                -->
                @if($tce->dtCancelamento != NULL)
                <div class="alert alert-danger">
                    <strong> TCE CANCELADO </strong>
                    <br> Esse TCE foi cancelado em <strong>{{$tce->dtCancelamento->format('d/m/Y')}}</strong>.
                </div>
                @endif
                <table class="table table-bordered">
                        <tbody>
                        <tr>
                            @if($tce->sec_conc_id <> NULL)
                            <tr>
                                <td colspan="5"><label>Secretaria:</label><br>{{$tce->secConcedente->nmSecretaria}}</td>
                            </tr>
                            @endif
                            <td colspan="2"><label>Instituição de Ensino:</label> {{$tce->estudante->instituicao->nmInstituicao}}</td>
                            <td colspan="2"><label>Curso:</label> {{$tce->cursoDaInstituicao->curso->nmCurso}}</td>
                            <td><label>Semestre/Ano TCE:</label> {{$tce->nnSemestreAno}} {{$tce->cursoDaInstituicao->cursoDuracao->nmTipoDuracao}}</td>

                        </tr>
                        <!--Inicio se houver informado nomes e cargos supervisores no TCE.-->
                    @if(($tce->nmSupervisor === NULL OR $tce->nmSupervisor === "") AND ($tce->nmSupervisorIe === NULL OR $tce->nmSupervisorIe === ""))
                    @else
                    <tr>
                        <td><label>Supervisor Concedente:</label><br>
                        @if ($tce->nmSupervisor === NULL OR $tce->nmSupervisor === "")
                        -
                        @else
                            {{$tce->nmSupervisor}}
                        @endif
                        </td>
                        <td><label>Cargo Supervisor Concedente:</label><br>
                        @if ($tce->dsSupervisorCargo === NULL OR $tce->dsSupervisorCargo === "")
                        -
                        @else
                            {{$tce->dsSupervisorCargo}}
                        @endif
                        </td>
                        <td colspan="2"><label>Supervisor Ie:</label><br>
                        @if ($tce->nmSupervisorIe === NULL OR $tce->nmSupervisorIe === "")
                        -
                        @else
                            {{$tce->nmSupervisorIe}}
                        @endif
                        </td>
                        <td colspan="2"><label>Cargo Supervisor Ie:</label><br>
                        @if ($tce->dsSupervisorIeCargo === NULL OR $tce->dsSupervisorIeCargo === "")
                        -
                        @else
                            {{$tce->dsSupervisorIeCargo}}
                        @endif
                        </td>
                    </tr>
                    @endif
                    <!--Fim se não houver informado nomes e cargos supervisores no TCE.-->
                        <tr>
                            <td><label>Auxilio Mensal:</label><br>
                                @if ($tce->vlAuxilioMensal === NULL)
                                -
                                @else
                                    R$ {{number_format($tce->vlAuxilioMensal, 2, ',', '.')}}
                                @endif
                            </td>
                            <td><label>Auxilio Alimentação:</label><br>
                                @if ($tce->vlAuxilioAlimentacao === NULL)
                                -
                                @else
                                    R$ {{number_format($tce->vlAuxilioAlimentacao, 2, ',', '.')}}
                                @endif
                            </td>
                            <td colspan="2"><label>Auxilio Transporte:</label><br>
                                @if ($tce->vlAuxilioTransporte === NULL)
                                -
                                @else
                                    R$ {{number_format($tce->vlAuxilioTransporte, 2, ',', '.')}}
                                @endif
                            </td>
                            <td colspan="1"><label>Total:</label><br>
                                @if ($tce->vlAuxilioMensal === NULL)
                                -
                                @else
                                    R$ {{number_format($tce->vlAuxilioMensal+$tce->vlAuxilioAlimentacao+$tce->vlAuxilioTransporte, 2, ',', '.')}}
                                @endif
                            </td>

                        </tr>
                        <tr>
                            <td colspan="1"><label>Vigência Inicio:</label><br>{{$tce->dtInicio->format("d/m/Y")}}</td>
                            <td colspan="1"><label>Vigência Fim:</label><br>{{$tce->dtFim->format("d/m/Y")}}</td>
                            <td colspan="1"><label>Hora Inical de Estágio:</label><br>
                                @if ($tce->hrInicio === NULL)
                                -
                                @else
                                    {{$tce->hrInicio}}
                                @endif
                            </td>
                            <td colspan="2"><label>Hora Final de Estágio:</label><br>
                                @if ($tce->hrFim === NULL)
                                -
                                @else
                                    {{$tce->hrFim}}
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5"><label>Periodo Semanal:</label><br>{{$tce->periodo->dsPeriodo}} {{$tce->dsPeriodo}}</td>
                            {{-- <td ><label>Número Apolice:</label><br>{{$tce->dsApolice}}</td>  --}}
                        </tr>
                        {{-- <tr>
                            <td colspan="5"><label>Descrição do Periodo:</label><br></td>

                        </tr> --}}
                        <tr>
                           <td colspan="5"><label>Atividades:</label><br>
                            <ol type="1">
                                @foreach($tce->atividades as $atividade)
                                    <li>{{$atividade->atividade}}</li>
                                @endforeach
                            </ol>
                           </td>
                        </tr>
                        </tbody>
                    </table>
            </div>
        </div>
        <!--opção de cancelamento de TCE para concedente que tem opCancelamento = SIM-->
        @if(($tce->concedente->opCancelamento != "NAO") AND ($tce->dtCancelamento == NULL))
        <div class="row">
            <div class="col-lg-12 text-center">
                <button type="button" class="btn btn-danger ad-click-event" data-toggle="modal" data-target=".cancelamento-tce" onclick="return confirm('Deseja realmente cancelar este TCE?');">Cancelar TCE</button>
            </div>
        </div>
        @endif

        <div class="row">
            <div class="col-lg-12">
                <!--ADITIVOS-->
                @if ($aditivos->count())
                <legend>Aditivos</legend>
                <table class="table table-striped">
                        <tbody>
                        <tr>
                        <th style="width: 22px;">Ação</th>
                        <th style="width: 22px;">Aditivo</th>
                        <th style="width: 90px;">Data Inicio</th>
                        <th style="width: 90px;">Data Fim</th>
                        <th>Outro</th>
                        </tr>
                        @foreach($aditivos as $aditivo)

                        <tr>
                            <td><a href="{{route('painel.concedente.tce', $aditivo->id)}}" class="btn btn-success btn-xs ad-click-event">Ver Aditivo</a></td>
                            <td>{{$aditivo->nnAditivo}} </td>
                            <td>
                                @if ($aditivo->dtInicio === NULL)
                                -
                                @else
                                    {{$aditivo->dtInicio->format('d/m/Y')}}
                                @endif
                            </td>
                            <td>
                                @if ($aditivo->dtFim === NULL)
                                -
                                @else
                                    {{$aditivo->dtFim->format('d/m/Y')}}
                                @endif
                            </td>
                            <td>
                                @if (($aditivo->aditivoOutro === NULL) OR ($aditivo->aditivoOutro === ""))
                                -
                                @else
                                    {{$aditivo->aditivoOutro}}
                                @endif
                            </td>
                        </tr>
                        @endforeach

                        </tbody>
                </table>
                @endif
                <!-- ADITIVO FIM -->
            </div>
        </div>

        {{-- <div class="box-footer text-center">
            @if($tce->dtCancelamento == NULL)
                <a href="{{route('concedente.tce.relatorio.show', [base64_encode($tce->id), $tce->relatorio_id])}}" target="_blank" class="btn btn-success ad-click-event">Ver TCE</a>
            @else
                <a href="{{route('concedente.tce.recisao.show', base64_encode($tce->id))}}" target="_blank" class="btn btn-primary ad-click-event">Ver Recisão de TCE</a>
            @endif
        </div> --}}

        <br>
        <br>
    </div>
    <!-- /#page-wrapper -->
    <!-- Large modal -->


<div class="modal fade cancelamento-tce" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
            <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">
                            Cancelamento do Tce
                            @if($tce->migracao != NULL)
                                {{$tce->migracao}}
                            @else
                                {{$tce->id}}
                            @endif
                            - {{$tce->estudante->nmEstudante}}
                    </h4>
                  </div>
                  <form action="{{route('painel.concedente.tce.cancelar', ['tce' => $tce->id])}}" method="POST">
                        {!! csrf_field() !!}
                        {!! method_field('PUT') !!}
                  <div class="modal-body">
                                    <div class="box-body">

                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="dtCancelamento">*Data de Encerramento</label>
                                                <input class="form-control" id="dtCancelamento" name="dtCancelamento"  type="text" value="{{$hoje}}">
                                            </div>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="form-group">
                                                <label for="motivoCancelamento_id">*Motivo do Cancelamento</label>
                                                <select class="form-control" id="motivoCancelamento_id" name="motivoCancelamento_id">
                                                    @foreach($motivosCancelamento as $motivo)
                                                        <option value="{{$motivo['id']}}">{{$motivo['dsMotivoCancelamento']}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="dtIniio">Descrição do Motivo</label>
                                                <input class="form-control" id="dsMotivoCancelamento" name="dsMotivoCancelamento" placeholder="Caso queira detalhar melhor..." type="text">
                                                <span class="help-block">Digite aqui, os motivos que levaram ao cancelamento deste TCE</span>
                                            </div>
                                        </div>
                                    </div>

                                    </div>
                                    <!-- /.box-body -->



                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
                    <button type="submit" class="btn btn-danger" onclick="return confirm('Você está cancelando o este TCE, Confirma?');">Cancelar TCE</button>
                  </div>
                </form>
    </div>
  </div>
</div>
    @stop
