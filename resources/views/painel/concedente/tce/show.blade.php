@extends('layouts.painel2')
@section('title_postfix', ' - '.$titulo)
@section('content')
<!-- Header -->
<div class="header bg-gradient-primary pb-4 pt-5 pt-md-8">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row">
        <div class="col-lg-12">
          <a href="{{route('painel.concedente.estudante', base64_encode($tce->id))}}" class="btn btn-default ad-click-event">Dados do Estágiário</a>
          <a href="{{route('painel.concedente.instituicao', base64_encode($tce->id))}}" class="btn btn-default ad-click-event">Dados da Instituição de Ensino</a>
          {{-- {{route('concedente.financeiro.relatorio', ['concedente' => $folha->concedente->id,'folha' =>
          $folha->id])--}}
          <a href="{{route('painel.concedente.folha', base64_encode($tce->id))}}" class="btn btn-default ad-click-event">Folha de Pagamento</a>
          <a href="{{route('painel.concedente.relatorioAtividades', base64_encode($tce->id))}}" class="btn btn-default ad-click-event"
            title="Relatório de Atividades Semestral" target="_blank">Relatório de Atividades</a>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="container-fluid mt-4">
    <div class="row justify-content-center">
      <div class="col-md-12">
        <div class="card shadow">
          <div class="card-header border-0 @if($tce->dt4Via == null) bg-blue text-white @elseif($tce->dtCancelamento == null) bg-success text-white @endif" data-toggle="tooltip" data-placement="top" title="@if($tce->dt4Via == null) O TCE possui pendência de entrega de documentação  @elseif($tce->dtCancelamento == null) O TCE está ativo sem pendências  @else O TCE está cancelado desde de {{$tce->dtCancelamento}} @endif">
            <div class="row">
              <div class="col-lg-12">
                <h4 class="page-header text-center"><strong>
                  {{$tce->estudante->nmEstudante}} -
                  @if($tce->migracao != NULL)
                  TCE n° {{$tce->migracao}}
                  @else
                  TCE n° {{$tce->id}}
                  @endif
                </strong>
              </h4>
            </div>
          </div>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">
              @if($tce->dtCancelamento != NULL)
              <div class="alert alert-danger">
                <strong> TCE CANCELADO </strong>
                <br> Esse TCE foi cancelado em <strong>{{$tce->dtCancelamento->format('d/m/Y')}}</strong>.
                <br><a href="{{route('painel.concedente.tce.recisao.show', base64_encode($tce->id))}}" class="btn btn-danger btn-xs ad-click-event"
                  target="_blank">Recisão de Cancelamento</a>
                </div>
                @endif
                <div class="table-responsive">
                  <table class="table align-items-center table-flush col-12">
                    <tbody>
                      @if($tce->sec_conc_id <> NULL)
                      <tr>
                        <td colspan="5"><label><b>Secretaria:</b></label><br>{{$tce->secConcedente->nmSecretaria}}</td>
                      </tr>
                      @endif
                      <tr>
                        <td colspan="2" style="width:25%"><label><b>Instituição de Ensino:</b></label><br>{{$tce->estudante->instituicao->nmInstituicao}}</td>
                        <td colspan="2"><label><b>Curso:</b></label> <br>{{$tce->cursoDaInstituicao->curso->nmCurso}}</td>
                        <td><label><b>Semestre/Ano TCE:</b></label> <br>{{$tce->nnSemestreAno}}
                          {{$tce->cursoDaInstituicao->cursoDuracao->nmTipoDuracao}}</td>
                        </tr>
                        <!--Inicio se houver informado nomes e cargos supervisores no TCE.-->
                        @if(($tce->nmSupervisor === NULL OR $tce->nmSupervisor === "") AND ($tce->nmSupervisorIe === NULL OR
                        $tce->nmSupervisorIe === ""))
                        @else
                        <tr>
                          <td style="15%"><label><b>Supervisor Concedente:</b></label><br>
                            @if ($tce->nmSupervisor === NULL OR $tce->nmSupervisor === "")
                            -
                            @else
                            {{$tce->nmSupervisor}}
                            @endif
                          </td>
                          <td><label><b>Cargo Supervisor Concedente:</b></label><br>
                            @if ($tce->dsSupervisorCargo === NULL OR $tce->dsSupervisorCargo === "")
                            -
                            @else
                            {{$tce->dsSupervisorCargo}}
                            @endif
                          </td>
                          <td colspan="2"><label><b>Supervisor Ie:</b></label><br>
                            @if ($tce->nmSupervisorIe === NULL OR $tce->nmSupervisorIe === "")
                            -
                            @else
                            {{$tce->nmSupervisorIe}}
                            @endif
                          </td>
                          <td colspan="2"><label><b>Cargo Supervisor Ie:</b></label><br>
                            @if ($tce->dsSupervisorIeCargo === NULL OR $tce->dsSupervisorIeCargo === "")
                            -
                            @else
                            {{$tce->dsSupervisorIeCargo}}
                            @endif
                          </td>
                        </tr>
                        @endif
                        <tr>
                          <td><label><b>Auxilio Mensal:</b></label><br>
                            @if ($tce->vlAuxilioMensal === NULL)
                            -
                            @else
                            R$ {{number_format($tce->vlAuxilioMensal, 2, ',', '.')}}
                            @endif
                          </td>
                          <td><label><b>Auxilio Alimentação:</b></label><br>
                            @if ($tce->vlAuxilioAlimentacao === NULL)
                            -
                            @else
                            R$ {{number_format($tce->vlAuxilioAlimentacao, 2, ',', '.')}}
                            @endif
                          </td>
                          <td colspan="2"><label>Auxilio Transporte:</label><br>
                            @if ($tce->vlAuxilioTransporte === NULL)
                            -
                            @else
                            R$ {{number_format($tce->vlAuxilioTransporte, 2, ',', '.')}}
                            @endif
                          </td>
                          <td colspan="1"><label><b>Total:</b></label><br>
                            @if ($tce->vlAuxilioMensal === NULL)
                            -
                            @else
                            R$ {{number_format($tce->vlAuxilioMensal+$tce->vlAuxilioAlimentacao+$tce->vlAuxilioTransporte, 2,
                              ',',
                              '.')}}
                              @endif
                            </td>
                          </tr>
                          <tr>
                            <td colspan="1"><label><b>Vigência Inicio:</b></label><br>{{$tce->dtInicio->format("d/m/Y")}}</td>
                            <td colspan="1"><label><b>Vigência Fim:</b></label><br>{{$tce->dtFim->format("d/m/Y")}}</td>
                            <td colspan="1"><label><b>Hora Inical de Estágio:</b></label><br>
                              @if ($tce->hrInicio === NULL)
                              -
                              @else
                              {{$tce->hrInicio}}
                              @endif
                            </td>
                            <td colspan="2"><label><b>Hora Final de Estágio:</b></label><br>
                              @if ($tce->hrFim === NULL)
                              -
                              @else
                              {{$tce->hrFim}}
                              @endif
                            </td>
                          </tr>
                          <tr>
                            <td colspan="@if($tce->dsApolice != null) 4 @else 5 @endif"><label><b>Periodo Semanal:</b></label><br>{{$tce->periodo->dsPeriodo}} {{$tce->dsPeriodo}}</td>
                            @if($tce->dsApolice != null)
                            <td colspan="1"><label><b>Número Apolice:</b></label><br>{{$tce->dsApolice}}</td>
                            @endif
                          </tr>
                          <tr>
                            <td colspan="5"><label><b>Atividades:</b></label><br>
                              <ol type="1">
                                @foreach($tce->atividades as $atividade)
                                <li>{{$atividade->atividade}}</li>
                                @endforeach
                              </ol>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="col-lg-12">
                  <!--opção de cancelamento de TCE para concedente que tem opCancelamento = SIM-->
                  @if(($tce->concedente->opCancelamento == "SIM") && ($tce->dtCancelamento == NULL))
                  <div class="row">
                    <div class="col-lg-12 text-center pb-3">
                      <button type="button" class="btn btn-primary ad-click-event" data-toggle="modal" data-target=".desligar-tce">Desligar TCE</button>
                      <button type="button" class="btn btn-danger ad-click-event" data-toggle="modal" data-target=".cancelamento-tce">Cancelar TCE</button>
                      <a href="{{route('painel.concedente.modelos.relatorio.show', ['idTce' => base64_encode($tce->id), 'idRelatorio' => $tce->relatorio_id])}}" type="button" class="btn btn-info ad-click-event" target="_blank">Visualizar</a>
                    </div>
                  </div>
                  @endif
                </div>
                <div class="row justify-content-center">
                  <div class="col-md-12">
                    <!--ADITIVOS-->
                    @if ($aditivos->count())
                    <legend>Aditivos</legend>
                    <div class="table-responsive">
                      <table class="table align-items-center table-flush col-12">
                        <tbody>
                          <tr>
                            {{-- <th style="width: 22px;">Ação</th> --}}
                            <th style="width: 22px;">Aditivo</th>
                            <th style="width: 90px;">Data Inicio</th>
                            <th style="width: 90px;">Data Fim</th>
                            <th>Outro</th>
                          </tr>
                          @foreach($aditivos as $aditivo)
                          <tr>
                            {{-- <td><a href="{{route('concedente.tce.aditivo',['tce' => base64_encode($tce->id), 'aditivo' => $aditivo->id] )}}"
                              class="btn btn-success btn-xs ad-click-event">Ver Aditivo</a></td> --}}
                              <td>{{$aditivo->nnAditivo}} </td>
                              <td>
                                @if ($aditivo->dtInicio === NULL)
                                -
                                @else
                                {{$aditivo->dtInicio->format('d/m/Y')}}
                                @endif
                              </td>
                              <td>
                                @if ($aditivo->dtFim === NULL)
                                -
                                @else
                                {{$aditivo->dtFim->format('d/m/Y')}}
                                @endif
                              </td>
                              <td>
                                @if (($aditivo->aditivoOutro === NULL) OR ($aditivo->aditivoOutro === ""))
                                -
                                @else
                                {{$aditivo->aditivoOutro}}
                                @endif
                              </td>
                            </tr>
                            @endforeach
                          </tbody>
                        </table>
                      </div>
                      @endif
                      <!-- ADITIVO FIM -->
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- modal -->
            <div class="modal fade cancelamento-tce" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">
                      Cancelamento do Tce
                      @if($tce->migracao != NULL)
                      {{$tce->migracao}}
                      @else
                      {{$tce->id}}
                      @endif
                      - {{$tce->estudante->nmEstudante}}
                    </h4>
                  </div>
                  <form action="{{route('painel.concedente.tce.cancelar', ['tce' => $tce->id])}}" method="POST">
                    {!! csrf_field() !!}
                    {!! method_field('PUT') !!}
                    <div class="modal-body">
                      <div class="box-body">
                        <div class="row">
                          <div class="col-md-5">
                            <div class="form-group">
                              <label for="dtCancelamento">*Data de Encerramento</label>
                              <input class="form-control" id="dtCancelamento" name="dtCancelamento" type="text" value="{{$hoje}}">
                            </div>
                          </div>
                          <div class="col-md-7">
                            <div class="form-group">
                              <label for="motivoCancelamento_id">*Motivo do Cancelamento</label>
                              <select class="form-control" id="motivoCancelamento_id" name="motivoCancelamento_id">
                                @foreach($motivosCancelamento->where('tipo', 'c') as $motivo)
                                <option value="{{$motivo['id']}}">{{$motivo['dsMotivoCancelamento']}}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group">
                              <label for="dtIniio">Descrição do Motivo</label>
                              <input class="form-control" id="dsMotivoCancelamento" name="dsMotivoCancelamento" placeholder="Caso queira detalhar melhor..." type="text">
                              <span class="help-block">Digite aqui, os motivos que levaram ao cancelamento deste TCE</span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
                      <button type="submit" class="btn btn-danger" onclick="return confirm('Você está cancelando o este TCE, Confirma?');">Cancelar TCE</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            <!-- /modal -->
            <div class="modal fade desligar-tce" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">
                      Desligar do Tce
                      @if($tce->migracao != NULL)
                      {{$tce->migracao}}
                      @else
                      {{$tce->id}}
                      @endif
                      - {{$tce->estudante->nmEstudante}}
                    </h4>
                  </div>
                  <form action="{{route('painel.concedente.tce.cancelar', ['tce' => $tce->id])}}" method="POST">
                    {!! csrf_field() !!}
                    {!! method_field('PUT') !!}
                    <div class="modal-body">
                      <div class="box-body">
                        <div class="row">
                          <div class="col-md-5">
                            <div class="form-group">
                              <label for="dtCancelamento">*Data de Encerramento</label>
                              <input class="form-control" id="dtCancelamento" name="dtCancelamento" type="text" value="{{$hoje}}">
                            </div>
                          </div>
                          <div class="col-md-7">
                            <div class="form-group">
                              <label for="motivoCancelamento_id">*Motivo do Desligamento</label>
                              <select class="form-control" id="motivoCancelamento_id" name="motivoCancelamento_id">
                                @foreach($motivosCancelamento->where('tipo', 'd') as $motivo)
                                <option value="{{$motivo['id']}}">{{$motivo['dsMotivoCancelamento']}}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group">
                              <label for="dtIniio">Descrição do Motivo</label>
                              <input class="form-control" id="dsMotivoCancelamento" name="dsMotivoCancelamento" placeholder="Caso queira detalhar melhor..." type="text">
                              <span class="help-block">Digite aqui, os motivos que levaram ao desligamento deste TCE</span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
                      <button type="submit" class="btn btn-primary" onclick="return confirm('Você está cancelando o este TCE, Confirma?');">Desligar TCE</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            <!-- /Modal -->
          </div>
          <!-- </div> -->

          @stop
