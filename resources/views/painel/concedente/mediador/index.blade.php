@extends('layouts.painel2')
@section('content')

<!-- Header -->
<div class="header bg-gradient-primary pb-4 pt-5 pt-md-8">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row">
        <div class="col-md-6">
          <h2 class="text-left text-white">
            Mediadores
            <br>
            <small>Faça o controle dos mediadores responsáveis pelos registros de faltas</small>
          </h2>
        </div>
        <div class="col-md-6">
          <button type="button" name="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#modal-add-mediador"><i class="fa fa-plus"></i> Add Mediador</button>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid mt-4">
  <!-- Card stats -->
  <div class="row">
    <div class="col-md-12">
      <div class="card card-default">
        <div class="card-header">
          <div class="row">
            <div class="col-md-8">
              <h4 class="text-left">Mediadores</h4>
            </div>
            <div class="col-md-4">
              <div class="input-group">
                <input type="search" name="search" value="" class="form-control" placeholder="Buscar por nome,cpf,tce..." onkeyup="busca(this.value)">
                <span class="input-group-append">
                  <span class="input-group-text"><i class="fa fa-search"></i></span>
                </span>
              </div>
            </div>
          </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-striped datatable" id="dtMediador">
              <thead>
                <tr>
                  <th style="width:5%;">#</th>
                  <th style="width:25%;">Nome</th>
                  <th style="width:15%;">E-mail</th>
                  <th style="width:10%;">Criado em</th>
                  <th class="text-center" style="width:15%;">Ações</th>
                </tr>
              </thead>
              <tbody id="body-table-mentores">
                @foreach($mediadors as $mediador)
                <tr class="objects" id="mediador_{{$loop->iteration}}" data-id="{{$mediador->id}}" data-nome="{{$mediador->name}}" data-email="{{$mediador->email}}">
                  <td>{{$loop->iteration}}</td>
                  <td>{{$mediador->name}}</td>
                  <td>{{$mediador->email}}</td>
                  <td>{{$mediador->created_at->format('d/m/Y')}}</td>
                  <td class="text-center">
                    <button type="button" name="btn-delete" class="btn btn-danger" onclick="deleteMediador('{{$loop->iteration}}', '{{$mediador->name}}')"><i class="fa fa-trash"></i> Excluir</button>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- modal Add Mediador -->
  <div class="modal" tabindex="-1" role="dialog" id="modal-add-mediador">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Cadastrar Mediador</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{ route('painel.concedente.mediador.store')}}" method="post" name="form-add-mediador">
          <div class="modal-body">
            {!! csrf_field() !!}
            <div class="form-group row">
              <div class="col-md-12">
                <label class="control-label" for="">Nome</label>
                <input type="text" name="name" class="form-control" value="" required>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-12">
                <label class="control-label" for="">E-mail</label>
                <input type="email" name="email" class="form-control" value="" required>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-12">
                <label class="control-label" for="">Senha</label>
                <div class="input-group">
                  <input type="text" name="password" class="form-control" value="" required>
                  <span class="input-group-append">
                    <button type="button" role="button" name="btn-formAdd-generate-password" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Clique para gerar uma senha aleatória"><i class="fa fa-cog"></i></button>
                  </span>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            <button type="submit" class="btn btn-success">Cadastrar</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!-- /modal Add Mediador -->
  <!-- modal Edit Mediador -->
  <div class="modal" tabindex="-1" role="dialog" id="modal-edit-mediador">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Excluir Mediador</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{ route('painel.concedente.mediador.update')}}" method="post" name="form-edit-mediador">
          <div class="modal-body">
            {!! csrf_field() !!}
            <h4 class="text-center">Deseja realmente excluir o mediador?</h4>
            <h6 class="text-center" id="mediador-exclusao"></h6>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            <button type="submit" class="btn btn-success">Salvar</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!-- /modal Edit Mediador -->
</div>
@stop
@section('js')
<script type="text/javascript">
$(".datatable").DataTable({
  "searching": false,
  "lengthChange": false,
  "columnDefs": [
    { "orderable": false, "targets": 'no-sort' }
  ],
  "pageLength": 25,
  "language": {
    "sEmptyTable": "Nenhum registro encontrado",
    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
    "sInfoPostFix": "",
    "sInfoThousands": ".",
    "sLengthMenu": "_MENU_ resultados por página",
    "sLoadingRecords": "Carregando...",
    "sProcessing": "Processando...",
    "sZeroRecords": "Nenhum registro encontrado",
    "sSearch": "Pesquisar",
    "oPaginate": {
      "sNext": "Próximo",
      "sPrevious": "Anterior",
      "sFirst": "Primeiro",
      "sLast": "Último"
    },
    "oAria": {
      "sSortAscending": ": Ordenar colunas de forma ascendente",
      "sSortDescending": ": Ordenar colunas de forma descendente"
    }
  }
});

var table = $('#dtMediador').DataTable();

$('form[name=form-add-mediador]').submit(function(){
  if($(this).find('input[name=name]').val().length > 0 && $(this).find('input[name=email]').val().length > 0 && $(this).find('input[name=password]').val().length > 0){
    $.post($(this).prop('action'), { 'name': $(this).find('input[name=name]').val() ,'email': $(this).find('input[name=email]').val(), 'password':$(this).find('input[name=password]').val() ,'_token': '{{ csrf_token() }}' }, function(response){
      if(response != null){
        if(response.status == "success"){
          var nome = "'"+response.user.name+"'";
          table.row.add([($(".objects").length + 1), response.user.name, response.user.email, response.created_at, '<center><button type="button" name="btn-delete" class="btn btn-danger" onclick="deleteMediador('+($(".objects").length + 1)+','+nome+')"><i class="fa fa-trash"></i> Excluir</button></center>']).draw();
          //
          $('#modal-add-mediador').modal('hide');
          toastr.success(response.msg);
          //
          $(this).find('input[name=name]').val('');
          $(this).find('input[name=email]').val('');
          $(this).find('input[name=password]').val('');
          //
        }else if(response.status == "error"){
          toastr.error(response.msg);
        }
      }
    });
  }else{
    toastr.warning('Todos os campos são obrigatório');
  }

  return false;
});

$("button[name=btn-formAdd-generate-password]").click(function(){
  $('form[name=form-add-mediador]').find('input[name=password]').val(generatePassword());
});

function generatePassword(){
  var text = "";
  var possible = "abcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < 7; i++)
  text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}

function deleteMediador(id, nome){
  var r = confirm('Deseja realmente excluir o mediador de nome: '+ nome +' ?');
  console.log($('#mediador_'+id).data('id'));
  if(r === true){
    $.post('{{ route("painel.concedente.mediador.destroy") }}',{ 'id': $('#mediador_'+id).data('id'), '_token': '{{ csrf_token() }}' }, function(response){
      if(response != null){
        if(response.status == 'success'){
          toastr.success(response.msg);
          table.row('#mediador_'+id).remove().draw();
          console.log(true);
        }else if(response.status == 'error'){
          toastr.error(response.msg);
        }else{
          toastr.warning('Não foi possível realizar a operação');
        }
      }
    });
  }else{
    toastr.info('Exclusão cancelada');
  }
}

function busca(nome){
  var mediadores = $(".objects");
  filtra(nome,mediadores);
}

function filtra(nome,mediadores){
  mediadores.show();
  if (nome != "") {
    for (var i = 0; i < mediadores.length; i++) {
      if (mediadores[i].dataset.nome.toLowerCase().search(nome.toLowerCase()) && mediadores[i].dataset.email.toLowerCase().search(nome.toLowerCase())) {
        mediadores[i].style.display = "none";
      }
    }
  }else{
    mediadores.show();
  }
}
</script>
@endsection
