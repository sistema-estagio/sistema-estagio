@extends('layouts.painel2')
@section('title_postfix', ' - '.$titulo)
@section('content')
<!-- Header -->
<div class="header bg-gradient-primary pb-4 pt-5 pt-md-8">
  <div class="container-fluid">
    <div class="header-body">
      <!-- .row -->
      <div class="row">
        <div class="col-lg-3 col-md-6">
          <div class="card card-stats mb-4 mb-xl-0">
            <a href="{{ route('painel.concedente.tces.pendentes') }}">
              <div class="card-body">
                <div class="row">
                  <div class="col">
                    <h5 class="card-title text-uppercase text-muted mb-0">Pendentes</h5>
                    <span class="h2 font-weight-bold mb-0">{{$tcesPendentes->count()}}</span>
                    <small style="font-weight: 600;font-size:8pt" class="card-title text-uppercase text-muted mb-0">TCE's</small>
                  </div>
                  <div class="col-auto">
                    <div class="icon icon-shape bg-info text-white rounded-circle shadow">
                      <i class="fas fa-file"></i>
                    </div>
                  </div>
                </div>
              </div>
            </a>
          </div>
        </div>
        <div class="col-lg-3 col-md-6">
          <div class="card card-stats mb-4 mb-xl-0">
            <a href="{{ route('painel.concedente.tces.ativos') }}">
              <div class="card-body">
                <div class="row">
                  <div class="col">
                    <h5 class="card-title text-uppercase text-muted mb-0">Ativos</h5>
                    <span class="h2 font-weight-bold mb-0">{{$tcesAtivos->count()}}</span>
                    <small style="font-weight: 600;font-size:8pt" class="card-title text-uppercase text-muted mb-0">TCE's</small>
                  </div>
                  <div class="col-auto">
                    <div class="icon icon-shape bg-success text-white rounded-circle shadow">
                      <i class="fas fa-file"></i>
                    </div>
                  </div>
                </div>
              </div>
            </a>
          </div>
        </div>
        <div class="col-lg-3 col-md-6">
          <div class="card card-stats mb-4 mb-xl-0">
            <a href="{{ route('painel.concedente.tces.cancelados') }}">
              <div class="card-body">
                <div class="row">
                  <div class="col">
                    <h5 class="card-title text-uppercase text-muted mb-0">Cancelados</h5>
                    <span class="h2 font-weight-bold mb-0">{{$tcesCancelados->count()}}</span>
                    <small style="font-weight: 600;font-size:8pt" class="card-title text-uppercase text-muted mb-0">TCE's</small>
                  </div>
                  <div class="col-auto">
                    <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                      <i class="fas fa-file"></i>
                    </div>
                  </div>
                </div>
              </div>
            </a>
          </div>
        </div>
        <div class="col-lg-3 col-md-6">
          <div class="card card-stats mb-4 mb-xl-0">
            <a href="{{ route('painel.concedente.supervisor.index') }}">
              <div class="card-body">
                <div class="row">
                  <div class="col">
                    <h5 class="card-title text-uppercase text-muted mb-0">Supervisores</h5>
                    <span class="h2 font-weight-bold mb-0">{{$supervisors->count()}}</span>
                  </div>
                  <div class="col-auto">
                    <div class="icon icon-shape bg-primary text-white rounded-circle shadow">
                      <i class="fas fa-chalkboard-teacher"></i>
                    </div>
                  </div>
                </div>
              </div>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- /.row -->
<div class="container-fluid mt-4">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <div class="card shadow">
        <div class="card-header border-0">
          {{-- @if($concedente->logo == "S")
          <div class="col-lg-2">
            <img src="../../img/concedente/{{$concedente->id}}.jpg">
          </div>
          <div class="col-lg-10">
            <h1 class="page-header">{{$concedente->nmRazaoSocial}}</h1>
          </div>
        </div>
        @else
        <div class="col-lg-12">
          <h1 class="page-header">{{$concedente->nmRazaoSocial}}</h1>
        </div>
        @endif --}}
        <div class="col-lg-12">
          @if(isset($secConcedente))
          <h3 class="page-header"><strong>{{$secConcedente->nmSecretaria}}</strong><br>CNPJ: {{$secConcedente->cdCnpjCpf}}</h3>
          @else
          <h3 class="page-header"><strong>{{$concedente->nmRazaoSocial}}</strong><br>CNPJ: {{$concedente->cdCnpjCpf}}</h3>
          @endif
        </div>
        <div class="table-responsive">
          <table class="table align-items-center table-flush">
            <thead class="thead-light">
              <tr>
                <th style="width: 10%" scope="col">TCE</th>
                <th style="width: 60%" scope="col">ESTUDANTE</th>
                <th class="text-center" style="width: 15%" scope="col">VIGÊNCIA INICIAL</th>
                <th class="text-center" style="width: 15%" scope="col">VIGÊNCIA FINAL</th>
              </tr>
            </thead>
            <tbody>
              @forelse($tces as $tce)
              <tr>
                <td>
                  @if($tce->aditivos->count() > 0)
                  @if($tce->migracao != NULL)
                  {{$tce->migracao}}/{{$tce->aditivos->count()}}
                  @else
                  {{$tce->id}}/{{$tce->aditivos->count()}}
                  @endif
                  @else
                  @if($tce->migracao != NULL)
                  {{$tce->migracao}}
                  @else
                  {{$tce->id}}
                  @endif
                  @endif
                </td>
                <td>
                  <a href="{{route('painel.concedente.tce', ['idTce' => base64_encode($tce->id)])}}">{{$tce->estudante->nmEstudante}}</a>
                </td>
                <td class="text-center">{{$tce->dtInicio->format('d/m/Y')}}</td>
                <td class="text-center">
                  @if($tce->aditivos->count() > 0)
                  @if($tce->aditivos()->latest()->first()->dtInicio != Null)
                  {{$tce->aditivos()->latest()->first()->dtFim->format('d/m/Y')}}
                  @else
                  {{$tce->dtFim->format('d/m/Y')}}
                  @endif
                  @else
                  {{--  {{date('d/m/Y',strtotime($tce->dtFim))}}  --}}
                  {{$tce->dtFim->format('d/m/Y')}}
                  @endif
                </td>
                <!-- ->route('estudante.tce.show', ['estudante' => $formulario['estudante_id'],'tce' => $new->id]); -->
              </tr>
              @empty
              <tr>
                <td colspan="3" class="text-center">
                  <span class="badge bg-red">Não existe registros no banco!</span>
                </td>
              </tr>
              @endforelse
            </tbody>
          </table>

        </div>
        <!-- /.panel-body -->
      </div>
      <!-- /.panel -->
    </div>
  </div>

@stop
