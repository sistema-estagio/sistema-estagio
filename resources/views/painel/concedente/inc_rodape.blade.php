{{-- <div class="footer" style="width: 100%; position: absolute; bottom: 0;"> --}}
<div class="footer" style="width: 100%; bottom: 0;">
        <div style="width: 100%; text-align: center; margin-top: 10px; ">
            <hr style=" margin-bottom: 0px; border-bottom: 1px solid #DCDCDC ;">
            @isset($dtHoje)
            <div style="width: 50%; text-align: left;">
                Gerado em: {{$dtHoje}}
            </div>
            @endif
            <strong>www.universidadepatativa.com.br</strong>
            <br> Rua Monsenhor Esmeraldo 36, Franciscanos - Juazeiro do Norte - CE - CEP 63000-020
            <br> Telefone: (88) 3512-2450
        </div>
</div>
<script type="text/php">
        if (isset($pdf)) {
            $x = 540;
            $y = 830;
            $text = "Página {PAGE_NUM} de {PAGE_COUNT}";
            $font = null;
            $size = 7;
            $color = array(0,0,0);
            $word_space = 0.0;  //  default
            $char_space = 0.0;  //  default
            $angle = 0.0;   //  default
            $pdf->page_text($x, $y, $text, $font, $size, $color, $word_space, $char_space, $angle);
        }
</script>