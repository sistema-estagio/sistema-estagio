@extends('layouts.painel2')
@section('title_postfix', ' - '.$titulo)
@section('content')

<!-- Header -->
<div class="header bg-gradient-primary pb-4 pt-5 pt-md-8">
  <div class="container-fluid">
    <div class="header-body">
    </div>
  </div>
</div>

<!-- .row -->
<div class="container-fluid">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <div class="card shadow mt-4">
        <div class="card-header border-0">
          <strong>TCE'S CANCELADOS</strong>
        </div>
        <div class="table-responsive">
          <table class="table align-items-center table-flush">
            <thead class="thead-light">
              <tr>
                <th style="width: 10%" scope="col">TCE</th>
                <th style="width: 50%" scope="col">ESTUDANTE</th>
                <th class="text-center" style="width: 15%" scope="col">VIGÊNCIA INICIAL | FINAL</th>
                <th class="text-center" style="width: 15%" scope="col">DATA CANCELAMENTO</th>
                <th class="text-center" style="width: 10%" scope="col">AÇÕES</th>
              </tr>
            </thead>
            <tbody>
              @forelse($tces as $tce)
              <tr>
                @if($tce->aditivos->count() > 0)
                @if($tce->migracao != NULL)
                <td>{{$tce->migracao}}/{{$tce->aditivos->count()}}</td>
                @else
                <td>{{$tce->id}}/{{$tce->aditivos->count()}}</td>
                @endif
                @else
                @if($tce->migracao != NULL)
                <td>{{$tce->migracao}}</td>
                @else
                <td>{{$tce->id}}</td>
                @endif
                @endif
                {{-- <td>{{$tce->estudante->cdCPF}}</td> --}}
                {{--CONTANDO APENAS ADITIVOS DE DATAS
                  @if($tce->aditivos->count() > 0)
                  @if($tce->aditivos()->latest()->first()->dtInicio != Null)
                  <td>{{$tce->id}}/{{$tce->aditivos()->latest()->first()->nnAditivo}}</td>
                  @else
                  <td>{{$tce->id}}</td>
                  @endif
                  @else
                  <td>{{$tce->id}}</td>
                  @endif  --}}
                  <!--<td>{{$tce->id}}</td>-->
                  <td>
                    <a>{{$tce->estudante->nmEstudante}}</a>
                  </td>
                  <td class="text-center">{{$tce->dtInicio->format('d/m/Y')}} |
                    @if($tce->aditivos->count() > 0)
                    @if($tce->aditivos()->latest()->first()->dtInicio != Null)
                    {{$tce->aditivos()->latest()->first()->dtFim->format('d/m/Y')}}
                    @else
                    {{$tce->dtFim->format('d/m/Y')}}
                    @endif
                    @else
                    {{--  {{date('d/m/Y',strtotime($tce->dtFim))}}  --}}
                    {{$tce->dtFim->format('d/m/Y')}}
                    @endif
                  </td>
                  <td class="text-center">
                    {{$tce->dtCancelamento->format('d/m/Y')}}
                  </td>
                  <td class="text-center">
                    <a href="{{route('painel.concedente.tce', ['idTce' => base64_encode($tce->id)])}}" class="btn btn-primary btn-sm" target="_blank">Ver</a>
                    @if($tce->cancelado_at != null)
                      <a href="{{route('painel.concedente.tce.cancelado.declaracao.show', base64_encode($tce->id))}}" class="btn btn-default btn-sm ad-click-event fa fa-file-pdf" data-toggle="tooltip" data-placement="top"  title="Declaração de Estágio" target="_blank" ></a>
                    @endif
                  </td>
                  <!-- ->route('estudante.tce.show', ['estudante' => $formulario['estudante_id'],'tce' => $new->id]); -->
                </tr>
                @empty
                <tr>
                  <td colspan="3" align="center">
                    <span class="badge bg-red">Não existe registros no banco!</span>
                  </td>
                </tr>
                @endforelse
              </tbody>
            </table>

          </div>
          <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
      </div>
    </div>

    @section('post-script')
    <script src="http://code.jquery.com/jquery-2.2.4.min.js"></script>
    <script src="https://rawgit.com/joequery/Stupid-Table-Plugin/master/stupidtable.js?dev"></script>
    <script>
    //$(function(){
    //    $("table").stupidtable();
    //});

    $(document).ready( function () {
      //$('#table_id').DataTable();
      $('#table_id').DataTable({
        pageLength: 25,
        "order": [[ 1, "asc" ]],
        "language": {
          "sEmptyTable": "Nenhum registro encontrado",
          "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
          "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
          "sInfoFiltered": "(Filtrados de _MAX_ registros)",
          "sInfoPostFix": "",
          "sInfoThousands": ".",
          "sLengthMenu": "_MENU_ resultados por página",
          "sLoadingRecords": "Carregando...",
          "sProcessing": "Processando...",
          "sZeroRecords": "Nenhum registro encontrado",
          "sSearch": "Pesquisar",
          "oPaginate": {
            "sNext": "Próximo",
            "sPrevious": "Anterior",
            "sFirst": "Primeiro",
            "sLast": "Último"
          },
          "oAria": {
            "sSortAscending": ": Ordenar colunas de forma ascendente",
            "sSortDescending": ": Ordenar colunas de forma descendente"
          }
        }
      })

    } );

    </script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.js"></script>
    @endsection
    @stop
