<!DOCTYPE html>
<html>
<head>
    <title>Relatório Tces 1</title>
    <style>
            html { margin: 20px}

            body {
                font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial;
            }
    
            footer.fixar-rodape {
                bottom: 0;
                left: 0;
                height: 40px;
                position: fixed;
                width: 100%;
                margin-left: 0;
            }
    
            .novaPagina {
                page-break-before: always;
                page-break-after: always
            }
    
            div.body-content {
                /** Essa margem vai evitar que o conteudo fique por baixo do rodapé **/
                margin-bottom: 40px;
            }
    
            .col-12 {
                width: 100% !important;
            }
    
            .col-6 {
                width: 50% !important;
            }
    
            .col-6,
            .col-12 {
                float: left !important;
            }
    
            .table {
                border-collapse: separate;
                border-spacing: 0;
                min-width: 350px;
                margin-bottom: 10px;
            }
            .table th tr {
                font-size: 5pt;
            }
            .table tr th,
            .table tr td {
                border-right: 1px solid #bbb;
                border-bottom: 1px solid #bbb;
                padding: 5px;
            }
    
            .table th {
                border-top: solid 1px #bbb;
            }
    
            .table tr th:first-child,
            .table tr th:last-child {
                border-top: solid 1px #bbb;
            }
    
            .table tr th:first-child,
            .table tr td:first-child {
                border-left: 1px solid #bbb;
            }
    
            .table tr th:first-child,
            .table tr td:first-child {
                border-left: 1px solid #bbb;
            }
    
            .table tr th {
                background: #eee;
                text-align: left;
            }
    
            .table.Info tr th,
            .table.Info tr:first-child td {
                border-top: 1px solid #bbb;
            }
            /* top-left border-radius */
    
            .table tr:first-child th:first-child,
            .table.Info tr:first-child td:first-child {
                border-top-left-radius: 6px;
            }
            /* top-right border-radius */
    
            .table tr:first-child th:last-child,
            .table.Info tr:first-child td:last-child {
                border-top-right-radius: 6px;
            }
            /* bottom-left border-radius */
    
            .table tr:last-child td:first-child {
                border-bottom-left-radius: 6px;
            }
            /* bottom-right border-radius */
    
            .table tr:last-child td:last-child {
                border-bottom-right-radius: 6px;
            }
        </style>
    </head>
    <body>
        <div id="body">
            <div id="section_header">
            </div>
    
            <div id="content">
    
                <div class="page" style="font-size: 6pt">
                    <table style="width: 100%;" class="header">
                        <tr>
                            <td>
                                <?php $image_path = 'http://teste.universidadepatativa.com.br/sysEstagio/img/relatorios/logo_default.png'; ?>
                                {{-- <img src="{{ public_path() . $image_path }}" alt="Upa - Estagio" width="170px" height="80px"/> --}}
                                <img src="{{ $image_path }}" alt="Upa - Estagio" width="170px" height="80px"/>
                            </td>
                            <td>
                                <h1 style="text-align: right">
                                    <strong>TCEs Concedente<br>{{mb_strtoupper($Concedente->nmRazaoSocial)}}</strong>
                                </h1>
                            </td>
                        </tr>
                    </table>
                    <hr style=" margin-bottom: 0px; border-bottom: 1px solid #DCDCDC ;">
                    <br>

<!-- grupamento po Estado -->
<table class="table table-bordered" style="width: 100%;">
    <thead>
        <tr>
            <th>TCE</th>
            <th data-sort="string" data-sort-onload="yes">ESTUDANTE</th>
            <th>INSTITUICAO</th>
            <th>CURSO</th>
            <th>SEMESTRE/ANO ATUAL</th>
            <th>DURAÇÃO</th>
            <th>PERIODO</th>
            <th>AUX-MÊS</th>
            <th>AUX-TRANS</th>
            <th>AUX-ALIM</th>
            <th>INICIO</th>
            <th>FIM</th>
        </tr>
    </thead>
    @php $totalTCE = 0; @endphp
    @foreach($Concedente->tce->where('dtCancelamento','==',NULL) as $tce)
        <tr>
            @if($tce->aditivos->count() > 0)
                <td>
                    @if($tce->migracao != NULL)
                        {{$tce->migracao}}
                    @else
                        {{$tce->id}}
                    @endif
                    /{{$tce->aditivos->where('dtInicio','<>',NULL)->where('dtFim','<>',Null)->count()}}
                </td>
            @else
                <td>
                    @if($tce->migracao != NULL)
                        {{$tce->migracao}}
                    @else
                        {{$tce->id}}
                    @endif
                </td>
            @endif
            <td>{{$tce->estudante->nmEstudante}}</td>
            {{-- <td>{{$tce->estudante->cdCPF}}</td> --}}
            <td>{{$tce->instituicao->nmInstituicao}}</td>
            <td>{{$tce->CursoDaInstituicao->curso->nmCurso}}</td>
            <td>
                @if($tce->aditivos->count() > 0)
                    @if($tce->aditivos()->latest()->first()->nnSemestreAno != NULL)
                        {{$tce->aditivos()->latest()->first()->nnSemestreAno}}
                    @else
                        {{$tce->nnSemestreAno}}
                    @endif
                @else
                    {{$tce->nnSemestreAno}}
                @endif
            </td>
            <td>{{$tce->CursoDaInstituicao->qtdDuracao}}</td>
            <td>{{$tce->periodo->nmPeriodo}}</td>
            <td>
                @if($tce->aditivos->count() > 0)
                    @if($tce->aditivos()->latest()->first()->vlAuxilioMensal != Null)
                        {{$tce->aditivos()->latest()->first()->vlAuxilioMensal}}
                    @else
                        {{$tce->vlAuxilioMensal}}
                    @endif
                @else
                    {{$tce->vlAuxilioMensal}}
                @endif
            </td>
            <td>
                @if($tce->aditivos->count() > 0)
                    @if($tce->aditivos()->latest()->first()->vlAuxilioTransporte != Null)
                        {{$tce->aditivos()->latest()->first()->vlAuxilioTransporte}}
                    @else
                        {{$tce->vlAuxilioTransporte}}
                    @endif
                @else
                    {{$tce->vlAuxilioTransporte}}
                @endif
            </td>
            <td>
                    @if($tce->aditivos->count() > 0)
                        @if($tce->aditivos()->latest()->first()->vlAuxilioAlimentacao != Null)
                            {{$tce->aditivos()->latest()->first()->vlAuxilioAlimentacao}}
                        @else
                            {{$tce->vlAuxilioAlimentacao}}
                        @endif
                    @else
                        {{$tce->vlAuxilioAlimentacao}}
                    @endif
                </td>
            <td>{{date('d/m/Y',strtotime($tce->dtInicio))}}</td>
            <td>
                @if($tce->aditivos->count() > 0)
                    @if($tce->aditivos()->latest()->first()->dtInicio != Null)
                        {{date('d/m/Y',strtotime($tce->aditivos()->latest()->first()->dtFim))}}
                    @else
                        {{date('d/m/Y',strtotime($tce->dtFim))}}
                    @endif
                @else
                    {{date('d/m/Y',strtotime($tce->dtFim))}}
                @endif
            </td>
        </tr>
    @php $totalTCE++; @endphp
    @endforeach
</table>
<div style="margin-top:-10px;">Total de Resultados: {{$totalTCE}}</div>
<!-- FIM. Agrupamento po Estado -->
</div>
<div id="footer" style="font-size: 6pt">
        @include('painel.concedente.inc_rodape')
    </div>
</div>
<script src="http://code.jquery.com/jquery-2.2.4.min.js"></script>
<script src="https://rawgit.com/joequery/Stupid-Table-Plugin/master/stupidtable.js?dev"></script>
<script>
        $(function(){
            $("table").stupidtable();
        });
</script>
</body>
</html>