@extends('layouts.painel2')
@section('title_postfix', ' - '.$titulo)
@section('content')


<!-- Header -->
<div class="header bg-gradient-primary pb-4 pt-5 pt-md-8">
  <div class="container-fluid">
    <div class="header-body">
    </div>
  </div>
</div>
<!-- /.row -->
<div class="container-fluid mt-4">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <div class="card shadow">
        <div class="card-header border-0">
          <div class="row">
            <div class="col-md-4">
              <h4 class="text-left">
                TCE'S
                <br>
                <small>Abaixo está listado todos os TCE's</small>
              </h4>
            </div>
            <div class="col-md-4 justify-content-center">
              <form action="{{route('painel.concedente.tces')}}" method="POST">
                {!! csrf_field() !!}
                <div class="input-group text-center" style="width:80%;">
                  <input name="key_search" class="form-control" placeholder="Buscar por nome, cpf, nº tce..." type="text">
                  <button type="submit" class="btn btn-default btn-sm" style="border-top-left-radius: 0px; border-bottom-left-radius: 0px; box-shadow:none">
                    <i class="fa fa-search"></i>
                  </button>
                </div>
              </form>
            </div>
            <div class="col-md-4">
              <select class="custom-select float-right" name="" onChange="filter(document.getElementById('selFiltro'), this.value)" id="selTces" style="width:45%;">
                <option value="0">Todos</option>
                <option value="1">Ativos</option>
                <option value="2">Pendentes</option>
                <option value="3">Cancelados</option>
              </select>
            </div>
          </div>
        </div>
        <div class="card-body">
          <!-- /.panel-heading -->
          <div class="table-responsive">
            <table class="table align-items-center table-flush" id="id_table">
              <thead class="thead-light">
                <tr>
                  <th style="width: 3%" scope="col">Status</th>
                  <th style="width: 5%" scope="col">TCE</th>
                  <th style="width: 50%" scope="col">Estudante</th>
                  <th class="text-center" style="width: 15%" scope="col">Vigência Inicial</th>
                  <th class="text-center" style="width: 15%" scope="col">Vigência Final</th>
                  <th class="text-center" style="width: 15%" scope="col">Ações</th>
                </tr>
              </thead>
              <tbody>
                @forelse($tces->chunk(25) as $chunk)
                @foreach($chunk as $tce)
                <tr data-tipo="{{$tce->dt4Via == null ? 2 : $tce->dtCancelamento == null ? 1 : 3 }}"
                  data-nome="{{ \App\SRoco\RemoverAcentos::tirarAcentos($tce->estudante->nmEstudante) }}"
                  data-tce="{{ $tce->aditivos->count() > 0 ? ($tce->migracao != NULL ? $tce->migracao.'/'.$tce->aditivos->count() : $tce->id/$tce->aditivos->count()) : ($tce->migracao != NULL ? $tce->migracao : $tce->id)}}"
                  data-final="{{ $tce->dtFim->format('d/m/Y') }}"
                  data-inicial="{{ $tce->dtInicio->format('d/m/Y') }}"
                  data-cpf="{{ str_replace(array('.','-'),'',$tce->estudante->cdCPF)}}"
                  data-id="{{ $loop->iteration }}" class="tces">
                  <td class="text-center">
                    @if($tce->dt4Via == null)
                    <span class="btn bg-info"><i class="fa fa-file text-white"></i></span>
                    @elseif($tce->dtCancelamento == null)
                    <span class="btn bg-green"><i class="fa fa-check text-white"></i></span>
                    @else
                    <span class="btn bg-red"><i class="fa fa-times text-white"></i></span>
                    @endif
                  </td>
                  <td>
                    @if($tce->aditivos->count() > 0)
                    @if($tce->migracao != NULL)
                    {{$tce->migracao}}/{{$tce->aditivos->count()}}
                    @else
                    {{$tce->id}}/{{$tce->aditivos->count()}}
                    @endif
                    @else
                    @if($tce->migracao != NULL)
                    {{$tce->migracao}}
                    @else
                    {{$tce->id}}
                    @endif
                    @endif
                  </td>
                  {{-- <td>{{$tce->estudante->cdCPF}}</td> --}}
                  {{--CONTANDO APENAS ADITIVOS DE DATAS
                    @if($tce->aditivos->count() > 0)
                    @if($tce->aditivos()->latest()->first()->dtInicio != Null)
                    <td>{{$tce->id}}/{{$tce->aditivos()->latest()->first()->nnAditivo}}</td>
                    @else
                    <td>{{$tce->id}}</td>
                    @endif
                    @else
                    <td>{{$tce->id}}</td>
                    @endif  --}}
                    <td class="nomes">
                      {{$tce->estudante->nmEstudante}}
                    <br><small><span class="direct-chat-timestamp pull-left"><b>CPF:</b> {{$tce->estudante->cdCPF}} @if($tce->dsLotacao != null) | <b>Lotação:</b> {{$tce->dsLotacao}}@endif</span></small>
                    </td>
                    <td class="text-center">{{$tce->dtInicio->format('d/m/Y')}}</td>
                    <td class="text-center">
                      @if($tce->aditivos->count() > 0)
                      @if($tce->aditivos()->latest()->first()->dtInicio != Null)
                      {{$tce->aditivos()->latest()->first()->dtFim->format('d/m/Y')}}
                      @else
                      {{$tce->dtFim->format('d/m/Y')}}
                      @endif
                      @else
                      {{--  {{date('d/m/Y',strtotime($tce->dtFim))}}  --}}
                      {{$tce->dtFim->format('d/m/Y')}}
                      @endif
                    </td>
                    <td class="text-center">
                      <a href="{{route('painel.concedente.tce', ['idTce' => base64_encode($tce->id)])}}" class="btn btn-primary">Ver</a>
                      @if($tce->cancelado_at != null)
                        <a href="{{route('painel.concedente.tce.cancelado.declaracao.show', base64_encode($tce->id))}}" class="btn btn-default ad-click-event fa fa-file-pdf" data-toggle="tooltip" data-placement="top"  title="Declaração de Estágio" target="_blank" ></a>
                      @endif
                    </td>
                  </tr>
                  @endforeach
                  @empty
                  <tr id="nothingAtivos">
                    <td colspan="3" class="center">
                      <span class="badge bg-red">Não existe registros no banco!</span>
                    </td>
                  </tr>
                  @endforelse
                </tbody>
              </table>
            </div>
          </div>
          <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
      </div>
    </div>
  </div>
  @stop
  @section('js')
  <script>
  $("#id_table").DataTable({
    "searching": false,
    "lengthChange": false,
    "columnDefs": [
      { "orderable": false, "targets": 'no-sort' }
    ],
    "pageLength": 25,
    "language": {
      "sEmptyTable": "Nenhum registro encontrado",
      "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
      "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
      "sInfoFiltered": "(Filtrados de _MAX_ registros)",
      "sInfoPostFix": "",
      "sInfoThousands": ".",
      "sLengthMenu": "_MENU_ resultados por página",
      "sLoadingRecords": "Carregando...",
      "sProcessing": "Processando...",
      "sZeroRecords": "Nenhum registro encontrado",
      "sSearch": "Pesquisar",
      "oPaginate": {
        "sNext": "Próximo",
        "sPrevious": "Anterior",
        "sFirst": "Primeiro",
        "sLast": "Último"
      },
      "oAria": {
        "sSortAscending": ": Ordenar colunas de forma ascendente",
        "sSortDescending": ": Ordenar colunas de forma descendente"
      }
    }
  });

  function tirarBarras(str) {
    str = str.replace('/', '');
    str = str.replace('/', '');
    str = parseInt(str);
    return str;
  }

  function filter(filtro, status) {
    if(filtro) {
      let selFiltro = $('#selFiltro :selected').val();
      let tces = $('.tces');
      if (selFiltro != 0) {
        let row = []
        let order = []
        if (selFiltro === "ascNome") {
          for (let i = 0; i < tces.length; i++) {
            let tr = tces[i];
            row.push(tr)
            order = row.sort(function(a, b) {
              if(a.dataset.nome < b.dataset.nome) {
                return -1;
              }
              if(a.dataset.nome > b.dataset.nome) {
                return 1;
              }
              return 0;
            })
          }

          $('#id_table').html(order)

        } else if (selFiltro === "descNome") {
          for (let i = 0; i < tces.length; i++) {
            let tr = tces[i];
            row.push(tr)
            order = row.sort(function(a, b) {
              if(a.dataset.nome < b.dataset.nome) {
                return -1;
              }
              if(a.dataset.nome > b.dataset.nome) {
                return 1;
              }
              return 0;
            })
          }
          order.reverse()
          $('#id_table').html(order)
        } else if (selFiltro === "nTCE") {
          for (let i = 0; i < tces.length; i++) {
            let tr = tces[i];
            row.push(tr)
            order = row.sort(function(a, b) {
              if(a.dataset.tce < b.dataset.tce) {
                return -1;
              }
              if(a.dataset.tce > b.dataset.tce) {
                return 1;
              }
              return 0;
            })
          }

          $('#id_table').html(order)
        } else if (selFiltro === "dataInicio") {
          for (let i = 0; i < tces.length; i++) {
            let tr = tces[i];
            row.push(tr)
            order = row.sort(function (a, b) {
              // '01/03/2014'.split('/')
              // gives ["01", "03", "2014"]
              c = a.dataset.inicial.split('/');
              d = b.dataset.inicial.split('/');

              return c[2] - d[2] || c[1] - d[1] || c[0] - d[0];
            });
          }
          $('#id_table').html(order)
        } else if (selFiltro === "dataFim") {
          for (let i = 0; i < tces.length; i++) {
            let tr = tces[i];
            row.push(tr)
            order = row.sort(function(a, b) {
              // '01/03/2014'.split('/')
              // gives ["01", "03", "2014"]
              c = a.dataset.inicial.split('/');
              d = b.dataset.inicial.split('/');

              return c[2] - d[2] || c[1] - d[1] || c[0] - d[0];
            });
          }
          order.reverse()
          $('#id_table').html(order)
        }
      } else {
        tces.show();
      }
    }

    if(status) {
      var selTces = status;
      var tces = $('.tces').show();
      if (selTces != 0) {
        for(let i = 0; i < tces.length; i++) {
          if (selTces != tces[i].dataset.tipo) {
            tces[i].style.display = 'none';
          }
        }
      } else {
        tces.show();
      }

    }
  }
  $('input[name=key_search]').keyup(function(){
    var tces = $(".tces");
    var nothing = $('#nothingAtivos');
    var nome = $(this).val();
    search(nome,tces,nothing);
  });

  function search(nome,tces,nothing){
    nothing.hide();
    var count = 0;
    tces.show();
    if (nome != "") {
      for (var i = 0; i < tces.length; i++) {
        if (tces[i].dataset.nome.toLowerCase().search(nome.toLowerCase()) && tces[i].dataset.tce.toLowerCase().search(nome.toLowerCase()) && tces[i].dataset.cpf.search(nome)) {
          tces[i].style.display = "none";
        }else{
          count++;
        }
      }
      if(count == 0){
        nothing.show();
      }
    }
  }


</script>
@stop
