@extends('layouts.painel2')
@section('title_postfix', ' - '.$titulo)
@section('content')

<div class="header bg-gradient-primary pb-4 pt-5 pt-md-8">
  <div class="container-fluid">
    <div class="header-body">
      <div class="col-lg-12">
        <a href="{{URL::previous()}}" class="btn btn-default ad-click-event btn-xs">Voltar</a>
      </div>
    </div>
  </div>
</div>

<!-- /.row -->
<div class="container-fluid mt-4">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <div class="card shadow">
        <div class="card-header border-0">
          <div class="row">
            <div class="col-md-6">
              <strong class="float-left">FOLHAS DE PAGAMENTO - </strong>
            </div>
            <div class="">

            </div>
          </div>
        </div>
        <!-- /.panel-heading -->
        <div class="card-body">
          <div class="table-responsive">
            @if($folhasPagamento->where('secConcedente_id','!=',null)->count() > 0)
            <table class="table align-items-center table-flush datatable">
              <thead>
                <tr>
                  <th style="width:5%;">Status</th>
                  <th>SECRETARIA</th>
                  <th>REFERÊNCIA MÊS</th>
                  <th>REFERÊNCIA ANO</th>
                  <th>CRIADA EM</th>
                  <th style="width:15%;">FECHADA EM</th>
                  <th width="100" class="text-center">AÇÔES</th>
                </tr>
              </thead>
              <tbody>
                @forelse($folhasPagamento->sortByDesc('created_at') as $folha)
                <tr>
                  <td>
                    @if($folha->fechado == 'S')
                    <span class="btn btn-link bg-success"><i class="fas fa-lock text-white"></i></span>
                    @else
                    <span class="btn btn-link bg-primary"><i class="fas fa-lock-open text-white"></i></span>
                    @endif
                  </td>
                  <td>{{$folha->secConcedente->nmSecretaria != null ? $folha->secConcedente->nmSecretaria : $folha->concedente->nmRazaoSocial}}</td>
                  <td>{{$folha->referenciaMes}}</td>
                  <td>{{$folha->referenciaAno}}</td>
                  <td>{{$folha->created_at->format('d/m/Y')}}</td>
                  <td>{{$folha->fechado == 'S' ? \Carbon\Carbon::parse($folha->fechado_at)->format('d/m/Y') : '-----------'}}</td>
                  <td class="text-center">
                    <a href="{{route('painel.concedente.financeiro.show', $folha->id)}}" class="btn btn-success ad-click-event" target="_blank">Ver Folha</a>
                  </td>
                </tr>
                @empty
                <tr>
                  <td colspan="4" align="center">
                    <span class="badge bg-red">Não existe registros no banco!</span>
                  </td>
                </tr>
                @endforelse
              </tbody>
            </table>
            @else
            <table class="table align-items-center table-flush datatable">
              <thead>
                <tr>
                  <th style="width:5%;">Status</th>
                  <th>REFERÊNCIA MÊS</th>
                  <th>REFERÊNCIA ANO</th>
                  <th>CRIADA EM</th>
                  <th style="width:15%;">FECHADA EM</th>
                  <th width="100" class="text-center">AÇÔES</th>
                </tr>
              </thead>
              <tbody>
                @forelse($folhasPagamento->sortByDesc('created_at') as $folha)
                <tr>
                  <td>
                    @if($folha->fechado == 'S')
                    <span class="btn btn-link bg-success"><i class="fas fa-lock text-white"></i></span>
                    @else
                    <span class="btn btn-link bg-primary"><i class="fas fa-lock-open text-white"></i></span>
                    @endif
                  </td>
                  <td>{{$folha->referenciaMes}}</td>
                  <td>{{$folha->referenciaAno}}</td>
                  <td>{{$folha->created_at->format('d/m/Y')}}</td>
                  <td>{{$folha->fechado == 'S' ? \Carbon\Carbon::parse($folha->fechado_at)->format('d/m/Y') : '-----------'}}</td>
                  <td class="text-center">
                    <a href="{{route('painel.concedente.financeiro.show', $folha->id)}}" class="btn btn-success ad-click-event" target="_blank">Ver Folha</a>
                  </td>
                </tr>
                @empty
                <tr>
                  <td colspan="4" align="center">
                    <span class="badge bg-red">Não existe registros no banco!</span>
                  </td>
                </tr>
                @endforelse
              </tbody>
            </table>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /#page-wrapper -->
@stop
@section('js')
<script>

$(".datatable").DataTable({
  "searching": false,
  "lengthChange": false,
  "columnDefs": [
    { "orderable": false, "targets": 'no-sort' }
  ],
  "pageLength": 25,
  "language": {
    "sEmptyTable": "Nenhum registro encontrado",
    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
    "sInfoPostFix": "",
    "sInfoThousands": ".",
    "sLengthMenu": "_MENU_ resultados por página",
    "sLoadingRecords": "Carregando...",
    "sProcessing": "Processando...",
    "sZeroRecords": "Nenhum registro encontrado",
    "sSearch": "Pesquisar",
    "oPaginate": {
      "sNext": "Próximo",
      "sPrevious": "Anterior",
      "sFirst": "Primeiro",
      "sLast": "Último"
    },
    "oAria": {
      "sSortAscending": ": Ordenar colunas de forma ascendente",
      "sSortDescending": ": Ordenar colunas de forma descendente"
    }
  }
});

</script>
@stop
