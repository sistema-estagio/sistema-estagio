@extends('layouts.painel2')
@section('title_postfix', ' - '.$titulo)
@section('content')

<div class="header bg-gradient-primary pb-4 pt-5 pt-md-8">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row">
        <div class="col-lg-12">
          <a href="{{URL::previous()}}" class="btn btn-default ad-click-event btn-xs">Voltar</a>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid mt-4">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <div class="card shadow">
        <div class="card-header border-0">
          <div class="col-lg-12">
            <h3 class="page-header">{{$tce->estudante->nmEstudante}}</h3>
          </div>
        </div>
        <div class="table-responsive">
          <table class="table table-bordered">
            <tbody>
              <tr>
                <td colspan="3"><label>CPF:</label> {{$tce->estudante->cdCPF}}</td>
                <td colspan="1"><label>Nascimento:</label> {{$tce->estudante->dtNascimento->format("d/m/Y")}}</td>
                <td colspan="2"><label>Estado Civil:</label> {{$tce->estudante->dsEstadoCivil}}</td>
              </tr>
              <tr>
                <td colspan="3"><label>Instituicao de Ensino:</label><br>{{$tce->instituicao->nmInstituicao}}</td>
                <td colspan="1"><label>Curso:</label><br>{{$tce->cursoDaInstituicao->curso->nmCurso}}</td>
                <td colspan="1"><label>{{$tce->cursoDaInstituicao->cursoDuracao->nmTipoDuracao}} Atual:</label><br>{{$tce->estudante->nnSemestreAno}}
                  {{$tce->cursoDaInstituicao->cursoDuracao->nmTipoDuracao}}</td>
              </tr>
              <tr>
                <td colspan="2"><label>Endereço:</label><br>
                  {{$tce->estudante->dsEndereco}}, N° {{$tce->estudante->nnNumero}}
                </td>
                <td><label>CEP:</label><br>
                  {{$tce->estudante->cdCEP}}
                </td>
                <td><label>Bairro:</label><br>
                  {{$tce->estudante->nmBairro}}
                </td>
                <td><label>Cidade/UF:</label><br>
                  {{$tce->estudante->cidade->nmCidade}}/{{$tce->estudante->cidade->estado->cdUF}}
                </td>
              </tr>
              <tr>
                <td><label>Telefone:</label> {{$tce->estudante->dsFone}}</td>
                <td><label>Outro: </label>
                  @if($tce->estudante->dsOutroFone == NULL)
                    -
                  @else
                    {{$tce->estudante->dsOutroFone }}
                  @endif
                </td>
                <td colspan="2"><label>E-mail: </label>
                  @if($tce->estudante->dsEmail == NULL)
                    -
                  @else
                    {{$tce->estudante->dsEmail}}
                  @endif
                </td>
                <td colspan="1"><label>Possui acesso: </label>
                  @if(\App\Models\UserEstudante::where('estudante_id',$tce->estudante->id)->count() > 0)
                    <b style="color:blue;">Sim</b>
                  @else
                    <b style="color:red;">Não</b>
                  @endif
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    {{--
    <div class="box-footer text-center">
      @if($tce->dtCancelamento == NULL)
        <a href="{{route('concedente.tce.relatorio.show', [base64_encode($tce->id), $tce->relatorio_id])}}" target="_blank"
        class="btn btn-success ad-click-event">Ver TCE</a>
      @else
        <a href="{{route('concedente.tce.recisao.show', base64_encode($tce->id))}}" target="_blank" class="btn btn-primary ad-click-event">Ver
          Recisão de TCE</a>
      @endif
    </div> --}}
  </div><br>
  <div class="row justify-content-center">
    <div class="col-lg-12">
      <div class="card shadow">
        <div class="card-header border-0">
          <div class="col-lg-12">
            <h3>TCES Antigos</h3>
          </div>
        </div>
        @if ($tces->count())
        <div class="box box-default">
          <div class="box-body">
            <table class="table table-bordered">
              <tbody>
                <tr>
                  <th style="width: 40px">TCE</th>
                  <th>Concedente</th>
                  <th>Tempo Estágiado</th>
                  <th>Cancelamento</th>
                  <th>Motivo</th>
                  {{--
                    <th>TCE</th>
                    <th>Ações</th>
                  --}}
                </tr>
                @foreach($tces as $tce)
                  <tr>
                    <td>{{$tce->id}}</td>
                    <td>{{$tce->concedente->nmRazaoSocial}}</td>
                    {{--
                      <td>{{$tce->dtInicio}}{{$tce->dtFim}}</td>
                      <td>{{ Carbon\Carbon::parse($tce->dtInicio)->diff($tce->dtCancelamento) }} </td> --}}
                      <td>{{ Carbon\Carbon::parse($tce->dtInicio)->diff(($tce->dtCancelamento))->format('%y Anos, %m Meses e %d
                        dias.') }} </td>
                      <td>{{$tce->dtCancelamento->format('d/m/Y')}}</td>
                      <td>{{$tce->motivoCancelamento->dsMotivoCancelamento}}</td>
                    <!-- ->route('estudante.tce.show', ['estudante' => $formulario['estudante_id'],'tce' => $new->id]); -->
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
        @endif
      </div>
    </div>
  </div>
<!-- /#page-wrapper -->
@stop
