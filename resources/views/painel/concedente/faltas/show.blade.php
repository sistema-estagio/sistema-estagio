@extends('layouts.painel2')
@section('content')

<style>
btn {
  border: none;
}

#saveButton {
  display: none;
}

</style>

<!-- Header -->
<div class="header bg-gradient-primary pb-4 pt-5 pt-md-8">
  <div class="container-fluid">
    <div class="header-body">
    </div>
  </div>
</div>
<div class="modal" tabindex="-1" role="dialog" id="modalConfirmLockFolha">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Confirmação de Fechamento de Folha</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{ route('painel.concedente.folha.fechar', ['hash'=>$folha->hash]) }}" method="post">
      {{ csrf_field() }}
      <div class="modal-body">
        <div class="input-group row">
          <div class="col-md-12">
            <label for="" style="text-align:justify;">
              <input type="checkbox" name="confirm" value="" required> Concordo que ao confirmar declaro que os dados abaixo estão conformes ao fatos reais e por assim fecha a folha para que seja gerado a documentação referente ao pagamento da mesma.
            </label>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button type="submit" class="btn btn-primary">Confirmar</button>
      </div>
      </form>
    </div>
  </div>
</div>
<!-- Page content -->
<div class="container-fluid">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <div class="card shadow mt-4">
        <div class="card-header">
          <div class="row">
            <div class="col-md-5">
              <small class="text-muted">Registro de Faltas</small>
              <br>
              <h3 class="mb-0">{{ $folha->referenciaMes.'/'.$folha->referenciaAno}} | {{$folha->secConcedente_id != null ? $folha->secConcedente->nmSecretaria : $folha->concedente->nmRazaoSocial}}</h3>
            </div>
            <div class="col-md-3">
              @if($folha->fechado == "S")
              <div class="alert alert-success float-left" style="width:60%;">
                <i class="fa fa-check"></i> Folha já foi fechada
              </div>
              @else
              <button type="button" name="button" class="btn btn-danger" data-toggle="modal" data-target="#modalConfirmLockFolha"><i class="fas fa-lock"></i> Fechar Folha</button>
              @endif
            </div>
            <div class="col-md-4">
              <div class="input-group">
                <input type="search" name="search" value="" class="form-control" placeholder="Buscar por nome,cpf,tce..." onkeyup="busca(this.value)">
                <span class="input-group-append">
                  <span class="input-group-text"><i class="fa fa-search"></i></span>
                </span>
              </div>
            </div>
          </div>
        </div>
        <div class="card-body">
        <div class="table-responsive">
          @if($folha->fechado === 'N' && ($folha->created_at->format('Y-m-d') <= \Carbon\Carbon::now()->format('Y-m-d')))
          <table class="table align-items-center table-flush datatable">
            <thead class="thead-light">
              <tr>
                <th class="text-center" style="width: 5%"scope="col">#</th>
                <th style="width: 30%" scope="col">Nome <br>
                  <small>CPF</small></th>
                  <th class="text-center"  style="width: 10%" scope="col">Início-Fim</th>
                  <th class="text-center" style="width: 10%" scope="col">Dias Trabalhados</th>
                  <th class="text-center" style="width: 10%" scope="col">Dias Recessos</th>
                  <th class="text-center" style="width: 10%" scope="col">Faltas</th>
                  <th class="text-center" style="width: 10%" scope="col">Justificadas</th>
                  <th class="text-center" style="width: 15%" scope="col">Ações</th>
                </tr>
              </thead>
              <tbody>
                @foreach($items as $item)
                <tr class="items" data-nome="{{ $item->tce->estudante->nmEstudante }}" data-tce="{{ $item->tce->id }}" data-cpf="{{ $item->tce->estudante->cdCPF }}">
                  <th scope="row">{{ $loop->iteration }}</th>
                  <td>{{ $item->tce->estudante->nmEstudante }}<br><span class="text-muted">CPF:</span>{{$item->tce->estudante->cdCPF}}</td>
                  <td class="text-center">
                    {{ $item->tce->dtInicio->format('d/m/Y') }} até {{ $item->tce->dtFim->format('d/m/Y') }}
                    <input type="hidden" name="id" id="input-id_{{ $loop->iteration }}" value="{{$item->id}}">
                  </td>
                  <td class="text-center">
                    <span id="span-base_{{ $loop->iteration }}" class="span_{{ $loop->iteration }}">{{ $item->diasBaseEstagiados != null ? $item->diasBaseEstagiados : 30}}</span>
                    <input type="hidden" class="form-control input_edit_{{ $loop->iteration }}" id="input-base_{{ $loop->iteration }}" value="{{ $item->diasBaseEstagiados != null ? $item->diasBaseEstagiados : 30}}">
                  </td>
                  <td class="text-center">
                    <span id="span-recesso_{{ $loop->iteration }}" class="span_{{ $loop->iteration }}">{{ $item->diasRecesso != null ? $item->diasRecesso : 'Não registrado'}}</span>
                    <input type="hidden" class="form-control input_edit_{{ $loop->iteration }}" id="input-recesso_{{ $loop->iteration }}" value="{{ $item->diasRecesso != null ? $item->diasRecesso : 0}}">
                  </td>
                  <td class="text-center">
                    <span id="span-faltas_{{ $loop->iteration }}" class="span_{{ $loop->iteration }}">{{ $item->faltas != null ? $item->faltas : 'Não registrado' }}</span>
                    <input type="hidden" class="form-control input_edit_{{ $loop->iteration }}" id="input-faltas_{{ $loop->iteration }}" value="{{ $item->faltas != null ? $item->faltas : 0 }}">
                  </td>
                  <td class="text-center">
                    <span id="span-justificadas_{{ $loop->iteration }}" class="span_{{ $loop->iteration }}">{{ $item->justificadas != null ? $item->justificadas : 'Não registrado' }}</span>
                    <input type="hidden" class="form-control input_edit_{{ $loop->iteration }}" id="input-justificadas_{{ $loop->iteration }}" value="{{ $item->justificadas != null ? $item->justificadas : 0 }}">
                  </td>
                  <td class="text-center">
                    <button class="btn btn-primary" id="btn_edit_{{ $loop->iteration }}" onClick="showInputs({{ $loop->iteration }});"><i class="fa fa-edit"></i> Editar</button>
                    <button class="btn btn-success" id="btn_save_{{ $loop->iteration }}" style="display:none" onClick="submitDados({{ $loop->iteration }})"><i class="fa fa-save"></i> Salvar</button>
                    <button class="btn btn-danger" id="btn_cancel_{{ $loop->iteration }}" style="display:none" onClick="cancelDados({{ $loop->iteration }})"><i class="fa fa-times"></i> Cancelar</button>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
            @else
            <table class="table align-items-center table-flush datatable">
              <thead class="thead-light">
                <tr>
                  <th class="text-center" style="width: 5%" scope="col">#</th>
                  <th style="width: 30%" scope="col">Nome <br>
                    <small>CPF</small></th>
                    <th class="text-center"  style="width: 10%" scope="col">Início-Fim</th>
                    <th class="text-center" style="width: 10%" scope="col">Dias Trabalhados</th>
                    <th class="text-center" style="width: 10%" scope="col">Faltas</th>
                    <th class="text-center" style="width: 10%" scope="col">Justificadas</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($items as $item)
                  <tr class="items" data-nome="{{ $item->tce->estudante->nmEstudante }}" data-tce="{{ $item->tce->id }}" data-cpf="{{ $item->tce->estudante->cdCPF }}">
                    <th scope="row">{{ $loop->iteration }}</th>
                    <td>{{ $item->tce->estudante->nmEstudante }}<br><span class="text-muted">CPF:</span>{{$item->tce->estudante->cdCPF}}</td>
                    <td class="text-center">
                      {{ $item->tce->dtInicio->format('d/m/Y') }} até {{ $item->tce->dtFim->format('d/m/Y') }}
                      <input type="hidden" name="id" id="input-id_{{ $loop->iteration }}" value="{{$item->id}}">
                    </td>
                    <td class="text-center">
                      <span id="span-base_{{ $loop->iteration }}" class="span_{{ $loop->iteration }}">{{ $item->diasBaseEstagiados != null ? $item->diasBaseEstagiados : 30}}</span>
                    </td>
                    <td class="text-center">
                      <span id="span-faltas_{{ $loop->iteration }}" class="span_{{ $loop->iteration }}">{{ $item->faltas != null ? $item->faltas : 0 }}</span>
                    </td>
                    <td class="text-center">
                      <span id="span-justificadas_{{ $loop->iteration }}" class="span_{{ $loop->iteration }}">{{ $item->justificadas != null ? $item->justificadas : 0 }}</span>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            @endif
          </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@stop
@section('js')
  <script>
  localStorage.clear();
  $(".datatable").DataTable({
    "searching": false,
    "lengthChange": false,
    "columnDefs": [
      { "orderable": false, "targets": 'no-sort' }
    ],
    "pageLength": 25,
    "language": {
      "sEmptyTable": "Nenhum registro encontrado",
      "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
      "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
      "sInfoFiltered": "(Filtrados de _MAX_ registros)",
      "sInfoPostFix": "",
      "sInfoThousands": ".",
      "sLengthMenu": "_MENU_ resultados por página",
      "sLoadingRecords": "Carregando...",
      "sProcessing": "Processando...",
      "sZeroRecords": "Nenhum registro encontrado",
      "sSearch": "Pesquisar",
      "oPaginate": {
        "sNext": "Próximo",
        "sPrevious": "Anterior",
        "sFirst": "Primeiro",
        "sLast": "Último"
      },
      "oAria": {
        "sSortAscending": ": Ordenar colunas de forma ascendente",
        "sSortDescending": ": Ordenar colunas de forma descendente"
      }
    }
  });
  @if($folha->fechado == "N")
  function busca(nome){
    var tces = $(".items");
    var nothing = $('#nenhum');
    var load = $("#loadCancelados");
    filtra(nome,tces,nothing,load);
  }

  function filtra(nome,tces,nothing,load){
    nothing.hide();
    var count = 0;
    tces.show();
    if (nome != "") {
      for (var i = 0; i < tces.length; i++) {
        if (tces[i].dataset.nome.toLowerCase().search(nome.toLowerCase()) && tces[i].dataset.tce.toLowerCase().search(nome.toLowerCase()) && tces[i].dataset.cpf.search(nome)) {
          tces[i].style.display = "none";
        }else{
          count++;
        }
      }
      if(count == 0){
        nothing.show();
      }
    }
  }

  function showInputs(id) {

    //var editButton = document.getElementById('editButton');
    $('#btn_edit_'+id).hide();
    $('#btn_save_'+id).show();
    $('#btn_cancel_'+id).show();
    $('.input_edit_'+id).attr('type', 'text');
    $('.span_'+id).hide();
    localStorage.setItem('obj_base_'+id, $('#input-base_'+id).val());
    localStorage.setItem('obj_faltas_'+id, $('#input-faltas_'+id).val());
    localStorage.setItem('obj_justificadas_'+id, $('#input-justificadas_'+id).val());
  };

  function submitDados(id) {
    console.log(id)
    var itensAdd = [];
    var item = {};

    item = {
      'id' : $('#input-id_'+id).val(),
      'dias': $('#input-base_'+id).val(),
      'recesso': $('#input-recesso_'+id).val(),
      'faltas': $('#input-faltas_'+id).val(),
      'justificadas': $('#input-justificadas_'+id).val(),
    };

    $.post('{{route("painel.concedente.folha.item.update", ["hash"=>$folha->hash])}}', { 'item': item, '_token':'{{csrf_token()}}' }, function(response){
      if(response != null){
        if(response.status == "success"){
          $('#span-base_'+id).html(item.dias);
          $('#span-faltas_'+id).html(item.faltas);
          $('#span-recesso_'+id).html(item.recesso);
          $('#span-justificadas_'+id).html(item.justificadas);
          //
          $('#btn_edit_'+id).show();
          $('#btn_save_'+id).hide();
          $('#btn_cancel_'+id).hide();
          $('.input_edit_'+id).attr('type', 'hidden');
          $('.span_'+id).show();
          toastr.success('O item '+id+' foi editado com sucesso');
        }else{
          toastr.error('Não foi posssível editar o item '+ id);
        }
      }
    });
  }

  function cancelDados(id){
    $('#btn_edit_'+id).show();
    $('#btn_save_'+id).hide();
    $('#btn_cancel_'+id).hide();
    //
    $('#input-base_'+id).val(localStorage.getItem('obj_base_'+id));
    $('#input-faltas_'+id).val(localStorage.getItem('obj_faltas_'+id));
    $('#input-justificadas_'+id).val(localStorage.getItem('obj_justificadas_'+id));
    //
    $('.input_edit_'+id).attr('type', 'hidden');
    $('.span_'+id).show();
  }
@endif
</script>
@endsection
