<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<title>Tce Novacap - TCE 
		@if($tce->migracao != NULL) 
			{{$tce->migracao}}
		@else 
			{{$tce->id}}
		@endif
	</title>
	<style>
		html { margin: 30px}
		body {
			font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif;
		}
		footer.fixar-rodape{
			bottom: 0;
			left: 0;
			height: 40px;
			position: fixed;
			width: 100%;
		  }
		.novaPagina {
			page-break-before: always;
			/*page-break-after: always*/
		}
		div.body-content{
			/** Essa margem vai evitar que o conteudo fique por baixo do rodapé **/
			margin-bottom: 40px;
		}

		.col-12 {
			width: 100% !important;
		}

		.col-6 {
			width: 50% !important;
		}

		.col-6,
		.col-12 {
			float: left !important;
		}

		.table {
			border-collapse: separate;
			border-spacing: 0;
			min-width: 350px;
			margin-bottom: 10px;
		}

		.table tr th,
		.table tr td {
			border-right: 1px solid #bbb;
			border-bottom: 1px solid #bbb;
			padding: 3px;
			
		}

		.table tr th:first-child,
		.table tr th:last-child {
			border-top: solid 1px #bbb;
		}

		.table tr th:first-child,
		.table tr td:first-child {
			border-left: 1px solid #bbb;
		}

		.table tr th:first-child,
		.table tr td:first-child {
			border-left: 1px solid #bbb;
		}

		.table tr th {
			background: #eee;
			text-align: left;
		}

		.table.Info tr th,
		.table.Info tr:first-child td {
			border-top: 1px solid #bbb;
		}
		/* top-left border-radius */

		.table tr:first-child th:first-child,
		.table.Info tr:first-child td:first-child {
			border-top-left-radius: 6px;
		}
		/* top-right border-radius */

		.table tr:first-child th:last-child,
		.table.Info tr:first-child td:last-child {
			border-top-right-radius: 6px;
		}
		/* bottom-left border-radius */

		.table tr:last-child td:first-child {
			border-bottom-left-radius: 6px;
		}
		/* bottom-right border-radius */

		.table tr:last-child td:last-child {
			border-bottom-right-radius: 6px;
		}

		.marcaDagua {
			position: fixed;
			width:100%;
			height:100%;
			font-size:6rem;
			font-weight:bold;
			text-align: center;
			display: table;
			color: rgba(187, 187, 187, 0.6);
		}

		.marcaDagua p {
			display: table-cell;
  			vertical-align: middle;
		}
	</style>
</head>

<body>

	<div class="marcaDagua">
		<p> NÃO OFICIAL </p>
	</div>

	<div style="text-align:center;color:red;font-weight:bolder">ESTE DOCUMENTO É MERAMENTE ILUSTRATIVO</div><br>

	<div id="body">

		<div id="section_header">
		</div>

		<div id="content">

			<div class="page" style="font-size: 7pt">
				<table style="width: 100%;" class="header">
					<tr>
						<td>
							<?php $image_path = '/img/relatorios/logo_default.png'; ?>
							<img src="{{ public_path() . $image_path }}" alt="Upa - Estagio" />
						</td>
						<td>
							<h2 style="text-align: left;text-decoration: underline;">
                                <strong>TERMO DE COMPROMISSO DE ESTÁGIO TCE 
									@if($tce->migracao != NULL) 
										{{$tce->migracao}}
									@else 
										{{$tce->id}}
									@endif
									<br>
                                    CONTRATO NOVACAP N° 083/2017<br>
                                    (PROCESSO COMPANHIA URBANIZADORA DA NOVA CAPITAL DO BRASIL N°112.004.160/2016)</strong>
							</h2>
						</td>
					</tr>
				</table>

				<div class="row">
					<div class="span12">
						<p style="margin: 10px;">As partes abaixo qualificadas celebram neste ato Termo de Compromisso de Estágio, conforme a Lei n°. 11.788/08.</p>
					</div>
				</div>

				@if($tce->instituicao->poloMatriz != NULL)
				<!-- SE INSTITUIÇÃO TIVER POLO MATRIZ DEFINIDA EXIBE ENDEREÇO E DADOS DO POLO MATRIZ -->
				<!--INSTITUIÇÃO DE ENSINO-->
				<table style="width: 100%;" class="table table-bordered table-condensed ">
						<tr>
							<th colspan="2" style="text-align: center">
								<strong>DADOS DA INSTITUIÇÃO DE ENSINO</strong><span style="float: right;font-size: 6pt"> (Cod.: {{$tce->instituicao_id}})</span>
							</th>
						</tr>
						<tr>
							<td colspan="2">
								<strong>{{$tce->instituicao->poloMatriz->nmPolo}}({{$tce->instituicao->nmInstituicao}})</strong>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<strong>CNPJ:</strong> {{$tce->instituicao->poloMatriz->cdCNPJ}}
							</td>
						</tr>
						<tr>
							<td>
								<strong>ENDEREÇO: </strong>{{$tce->instituicao->poloMatriz->dsEndereco}} ,
								<strong>N°</strong> {{$tce->instituicao->poloMatriz->nnNumero}}
							</td>
							<td>
								<strong>CIDADE: </strong>{{$tce->instituicao->poloMatriz->cidade->nmCidade}} - {{$tce->instituicao->poloMatriz->cidade->estado->nmEstado}}</td>
						</tr>
						<tr>
							@if($tce->instituicao->poloMatriz->nmDiretor <> NULL)
							<td>
								<strong>REPRESENTANTE: </strong>{{$tce->instituicao->poloMatriz->nmDiretor}}
							</td>
							@else
							<td>
								<strong>REPRESENTANTE: </strong>{{$tce->instituicao->nmDiretor}}
							</td>
							@endif
							<td>
								<strong>TELEFONE: </strong> {{$tce->instituicao->poloMatriz->dsFone}}
							</td>
						</tr>
						<!--SE TIVER SIDO PREENCHIDO NMREITOR NO POLO-->
						@if($tce->instituicao->poloMatriz->nmReitor <> NULL)
							<tr>
								<td colspan="2">
									<strong>REITOR(A) MATRIZ: </strong>{{$tce->instituicao->poloMatriz->nmReitor}}
								</td>
							</tr>
						<!--SE NÂO TIVER PREENCHIDO NMREITOR NO POLO-->
						@else
							<!--SE TIVER PREENCHIDO NMREITOR NA INSTITUICAO-->
							@if($tce->instituicao->nmReitor <> NULL)
							<tr>
								<td colspan="2">
									<strong>REITOR(A): </strong>{{$tce->instituicao->nmReitor}}
								</td>
							</tr>
							@endif
						@endif

							@if(($tce->nmSupervisorIe) AND ($tce->dsSupervisorIeCargo) != NULL)
							<tr>
								<td>
									<strong>SUPERVISOR(A): </strong>{{$tce->nmSupervisorIe}}
								</td>
								<td>
									<strong>CARGO/FORMAÇÃO: </strong>{{$tce->dsSupervisorIeCargo}}
								</td>
							</tr>
							@else 
								@if(($tce->instituicao->nmSupervisor) AND ($tce->instituicao->nmSupervisorCargo))
								<tr>
									<td>
										<strong>SUPERVISOR(A): </strong>{{$tce->instituicao->nmSupervisor}}
									</td>
									<td>
										<strong>CARGO/FORMAÇÃO: </strong>{{$tce->instituicao->nmSupervisorCargo}}
									</td>
								</tr>
								@endif 
							@endif
				</table>
				@else
				<!-- SE INSTITUIÇÃO NÃO TIVER POLO MATRIZ DEFINIDA EXIBE ENDEREÇO E DADOS DO POLO MATRIZ -->
				<!--INSTITUIÇÃO DE ENSINO-->
				<table style="width: 100%;" class="table table-bordered table-condensed ">
						<tr>
							<th colspan="2" style="text-align: center">
								<strong>DADOS DA INSTITUIÇÃO DE ENSINO</strong><span style="float: right;font-size: 6pt"> (Cod.: {{$tce->instituicao_id}})</span>
							</th>
						</tr>
						<tr>
							<td colspan="2">
								<strong>{{$tce->instituicao->nmInstituicao}}</strong>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<strong>CNPJ:</strong> {{$tce->instituicao->cdCNPJ}}
							</td>
						</tr>
						<tr>
							<td>
								<strong>ENDEREÇO: </strong>{{$tce->instituicao->dsEndereco}} ,
								<strong>N°</strong> {{$tce->instituicao->nnNumero}}
							</td>
							<td>
								<strong>CIDADE: </strong>{{$tce->instituicao->cidade->nmCidade}} - {{$tce->instituicao->cidade->estado->nmEstado}}</td>
						</tr>
						<tr>
							<td>
								<strong>REPRESENTANTE: </strong>{{$tce->instituicao->nmDiretor}}
							</td>
							<td>
								<strong>TELEFONE: </strong> {{$tce->instituicao->dsFone}}
							</td>
						</tr>
						@if($tce->instituicao->nmReitor <> NULL)
							<tr>
								<td colspan="2">
									<strong>REITOR(A): </strong>{{$tce->instituicao->nmReitor}}
								</td>
							</tr>
						@endif 
							@if(($tce->nmSupervisorIe) AND ($tce->dsSupervisorIeCargo) != NULL)
							<tr>
								<td>
									<strong>SUPERVISOR(A): </strong>{{$tce->nmSupervisorIe}}
								</td>
								<td>
									<strong>CARGO/FORMAÇÃO: </strong>{{$tce->dsSupervisorIeCargo}}
								</td>
							</tr>
							@else 
								@if(($tce->instituicao->nmSupervisor) AND ($tce->instituicao->nmSupervisorCargo))
								<tr>
									<td>
										<strong>SUPERVISOR(A): </strong>{{$tce->instituicao->nmSupervisor}}
									</td>
									<td>
										<strong>CARGO/FORMAÇÃO: </strong>{{$tce->instituicao->nmSupervisorCargo}}
									</td>
								</tr>
								@endif 
							@endif
				</table>
				@endif
				
				<!--CONCEDENTE-->
				<table style="width: 100%;" class="table table-bordered table-condensed">
					<tbody>
						<tr>
							<th colspan="3" style="text-align: center;">
								<strong>DADOS DA CONCEDENTE</strong><span style="float: right;font-size: 6pt"> (Cod.: {{$tce->concedente_id}})</span>
							</th>
						</tr>
						<tr>
							<td colspan="3">
								<strong>LOCAL DE REALIZAÇÃO DE ESTÁGIO:</strong> {{$tce->concedente->nmRazaoSocial}}
							</td>
						</tr>
						<!--SE O TCE FOR DE UMA SECRETARIA DA CONCEDENTE-->
						@if($tce->sec_conc_id <> NULL)
							<tr>
								<td colspan="3">
									<strong>SECRETARIA:</strong> {{$tce->secConcedente->nmSecretaria}} <span style="float: right;font-size: 5pt"> (Cod.: {{$tce->secConcedente->id}})</span>
								</td>
							</tr>
							<tr>
								<td colspan="3">
									<strong>CNPJ: </strong>{{$tce->secConcedente->cdCnpjCpf}}
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<strong>ENDEREÇO: </strong> {{$tce->secConcedente->dsEndereco}} , 
									<strong>N°</strong> {{$tce->secConcedente->nnNumero}}
								</td>
								<td>
									@if($tce->secConcedente->nmBairro)<strong>BAIRRO: </strong>{{$tce->secConcedente->nmBairro}}@endif
								</td>
							</tr>
							@if($tce->secConcedente->dsComplemento)
							<tr>
								<td colspan="3">
									<strong>COMPLEMENTO: </strong> {{$tce->secConcedente->dsComplemento}}
								</td>
							</tr>
							@endif
							<tr>
								<td colspan="2">
									<strong>CIDADE: </strong>{{$tce->concedente->cidade->nmCidade}} - {{$tce->concedente->cidade->estado->nmEstado}}
								</td>
								<td>
									<strong>CEP: </strong>{{$tce->secConcedente->cdCEP}}
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<strong>TELEFONE: </strong>{{$tce->secConcedente->dsFone}}
								</td>
								<td>
									<strong>OUTRO: </strong> @if  ( $tce->secConcedente->dsOutroFone != NULL) {{$tce->secConcedente->dsOutroFone}} @else - @endif
								</td>
							</tr>
							
							@if(($tce->nmSupervisor) AND ($tce->dsSupervisorCargo) != NULL)
							<tr>
								<td colspan="2">
									<em><strong>SUPERVISOR(A): </strong>{{$tce->nmSupervisor}}</em>
								</td>
								<td>
									<em><strong>CARGO/FORMAÇÃO: </strong>{{$tce->dsSupervisorCargo}}</em>
								</td>
							</tr>
							@else
								@if(($tce->secConcedente->nmResponsavel) AND ($tce->secConcedente->dsResponsavelCargo) != NULL)
								<tr>
									<td colspan="2">
										<strong>SUPERVISOR(A): </strong>{{$tce->secConcedente->nmResponsavel}}
									</td>
									<td>
										<strong>CARGO/FORMAÇÃO: </strong>{{$tce->secConcedente->dsResponsavelCargo}}
									</td>
								</tr>
								@endif
							@endif
						<!--SE NÂO FOR DE SECRETARIA EXIBE OS DADOS DA CONCEDENTE-->
						@else
							@if($tce->concedente->flTipo == "PF")
							<tr>
								<td colspan="2">
									<strong>CPF: </strong>{{$tce->concedente->cdCnpjCpf}}
								</td>
								<td>
									<strong>ORGÃO REGULADOR: </strong>{{$tce->concedente->dsOrgaoRegulador}}
								</td>
							</tr>
							@else
							<tr>
								<td colspan="3">
									<strong>CNPJ: </strong>{{$tce->concedente->cdCnpjCpf}}
								</td>
							</tr>
							@endif
							<tr>
								<td colspan="2">
									<strong>ENDEREÇO: </strong> {{$tce->concedente->dsEndereco}} , 
									<strong>N°</strong> {{$tce->concedente->nnNumero}}
								</td>
								<td>
									@if($tce->concedente->nmBairro)<strong>BAIRRO: </strong>{{$tce->concedente->nmBairro}}@endif
								</td>
							</tr>
							@if($tce->concedente->dsComplemento)
							<tr>
								<td colspan="3">
									<strong>COMPLEMENTO: </strong> {{$tce->concedente->dsComplemento}}
								</td>
							</tr>
							@endif
							<tr>
								<td colspan="2">
									<strong>CIDADE: </strong>{{$tce->concedente->cidade->nmCidade}} - {{$tce->concedente->cidade->estado->nmEstado}}
								</td>
								<td>
									<strong>CEP: </strong>{{$tce->concedente->cdCEP}}
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<strong>TELEFONE: </strong>{{$tce->concedente->dsFone}}
								</td>
								<td>
									<strong>OUTRO: </strong> @if  ( $tce->concedente->dsOutroFone != NULL) {{$tce->concedente->dsOutroFone}} @else - @endif
								</td>
							</tr>
							
							@if(($tce->nmSupervisor) AND ($tce->dsSupervisorCargo) != NULL)
							<tr>
								<td colspan="2">
									<strong>SUPERVISOR(A): </strong>{{$tce->nmSupervisor}}
								</td>
								<td>
									<strong>CARGO/FORMAÇÃO: </strong>{{$tce->dsSupervisorCargo}}
								</td>
							</tr> 
							@else
								@if(($tce->concedente->nmSupervisor) AND ($tce->concedente->dsSupervisorCargo) != NULL)
								<tr>
									<td colspan="2">
										<strong>SUPERVISOR(A): </strong>{{$tce->concedente->nmSupervisor}}
									</td>
									<td>
										<strong>CARGO/FORMAÇÃO: </strong>{{$tce->concedente->dsSupervisorCargo}}
									</td>
								</tr> 
								@endif
							@endif
						<!--ABAIXO FIM DO IF SE É SECRETARIA DE CONCEDENTE-->
						@endif
						@if($tce->dsLotacao)
						<tr>
							<td colspan="3">
								<strong>LOTAÇÃO:</strong> {{$tce->dsLotacao}}
							</td>
						</tr>
						@endif
					</tbody>
				</table>

				<!--ESTAGIARIO-->
				<table style="width: 100%;" class="table table-bordered table-condensed">
					<tr>
						<th colspan="3" style="text-align: center">
							<strong>DADOS DO ESTÁGIARIO </strong><span style="float: right;font-size: 6pt">(Cod.: {{$tce->estudante_id}})</span>
						</th>
					</tr>
					<tr>
						<td colspan="">
							<strong>NOME: </strong>{{$tce->estudante->nmEstudante}}
						</td>
						<td>
							<strong>DATA NASC: </strong>{{$tce->estudante->dtNascimento->format('d/m/Y')}}
						</td>
						<td>
							<strong>CPF: </strong>{{$tce->estudante->cdCPF}}
						</td>
					</tr>
					<tr>
						<td>
							<strong>RG: </strong>{{$tce->estudante->cdRG}}
						</td>
						<td>
							<strong>ORG.: </strong>{{$tce->estudante->dsOrgaoRG}}
						</td>
						<td>
							@if($tce->estudante->dsEmail)<strong>EMAIL: </strong>{{$tce->estudante->dsEmail}} @endif
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<strong>ENDEREÇO: </strong> {{$tce->estudante->dsEndereco}},
							<strong> N°</strong> {{$tce->estudante->nnNumero}}
						</td>
						<td>
							<strong>BAIRRO: </strong>{{$tce->estudante->nmBairro}}
						</td>
					</tr>
					@if($tce->estudante->dsComplemento)
					<tr>
						<td colspan="3">
							<strong>COMPLEMENTO: </strong> {{$tce->estudante->dsComplemento}}
						</td>
					</tr>
					@endif
					<tr>
						<td colspan="1">
							<strong>CIDADE: </strong>{{$tce->estudante->cidade->nmCidade}} - {{$tce->estudante->cidade->estado->nmEstado}}
						</td>
						<td>
							<strong>CEP: </strong>{{$tce->estudante->cdCEP}}
						</td>
						<td>
							@if($tce->estudante->dsFone)<strong>TELEFONE: </strong>{{$tce->estudante->dsFone}}@endif
						</td>
					</tr>
					<tr>
						<td>
							<strong>CURSO: </strong> {{$tce->cursoDaInstituicao->curso->nmCurso}}
						</td>
						<td>
							<strong>{{$tce->cursoDaInstituicao->cursoDuracao->nmTipoDuracao}}: </strong>
							{{$tce->nnSemestreAno}} {{$tce->cursoDaInstituicao->cursoDuracao->nmTipoDuracao}}
						 </td>
						<td>
							<strong>TURNO: </strong>{{$tce->estudante->turno->nmTurno}} 
						</td>
					</tr>
                </table>
                
                @include('tce.relatorio.inc_agente')

				<div class="texto">
						<div class="row" id="cl1">
							<div class="col-sm-12">
								<p style="text-align: justify" class="termo">
									<strong>CLÁUSULA 1ª - DO OBJETIVO:</strong>
									O presente Termo de Compromisso formaliza as condições para realização de estágio de caráter não obrigatório, conforme a legislação vigente, sem caracterização de vínculo empregatício, visando a realização de atividades compatíveis com a programação curricular e projeto pedagógico do curso, devendo permitir ao ESTAGIÁRIO, regularmente matriculado, a prática complementar do aprendizado.
								</p>
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-sm-12">
								<strong>CLÁUSULA 2ª - DA VIGÊNCIA:</strong>
								<p style="text-align: justify" class="termo">
                                    O presente Termo de Compromisso de Estágio vigerá do dia <strong>{{$tce->dtInicio->format("d/m/Y")}}</strong> até <strong>{{$tce->dtFim->format("d/m/Y")}}</strong>
                                </p>
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-sm-12">
                                <strong class="justi">CLÁUSULA 3ª - DA JORNADA DE ESTÁGIO:</strong>
                                <p style="text-align: justify" class="termo">
                                    A jornada de estágio será de {{substr($tce->periodo->nmPeriodo, -3, 2)}} ({{Extenso::valorPorExtenso(substr($tce->periodo->nmPeriodo, -3, 2), false, true)}}) horas semanais, 
                                    @php 
                                    if (!empty($tce->dsPeriodo)) {
                                        print $tce->dsPeriodo;
                                    }
                                    if (!empty($tce->hrInicio)) {
                                        print ' das ' . substr($tce->hrInicio, 0, 5) . ' às ' . substr($tce->hrFim, 0, 5);
                                    }
                                    @endphp
                                     na UNIDADE CONCEDENTE, salvo em situações excepcionais a critério da CONTRATANTE.
                                </p>
							</div>
						</div>
						<div class="row" id="cl4">
							<div class="col-sm-12">
								<strong class="justi">CLÁUSULA 4ª - DO VALOR DA BOLSA DE ESTÁGIO E OUTROS BENEFÍCIOS:</strong>
                                <p style="text-align: justify" class="termo">
                                    O ESTAGIÁRIO que realizar a jornada completa prevista na Cláusula 3ª, terá direito a receber, mensalmente, uma bolsa de estágio no valor de 
                                    <strong>R${{number_format($tce->vlAuxilioMensal, 2, ',', '.')}}</strong> ({{Extenso::Converter($tce->vlAuxilioMensal)}}), Auxílio-Alimentação no valor de <strong>R$235,93</strong> (Duzentos e Trinta e Cinco Reais e Noventa e Três Centavos) e Auxílio-Transporte no valor de <strong>R$8,00</strong> (Oito Reais), devido apenas quanto aos dias de efetiva frequência ao estágio, conforme art.12º da Lei 11.788/2008.
                                </p>
                                <ol class="margin justi">
                                
                                    <li>Além da bolsa de estágio estabelecida no “caput” desta cláusula, o ESTAGIÁRIO fará jus a um período de recesso remunerado, proporcional ao tempo de duração, até no máximo 30 (trinta) dias por ano, devido apenas quanto aos dias de efetiva frequência ao estágio, conforme art.13º da Lei 11.788/2008.</li>
                                    <li>O ESTAGIÁRIO, durante a vigência do presente Termo de Compromisso de Estágio, estará segurado contra acidentes pessoais, conforme <strong>Apólice Coletiva de nº. 9.850-3 de Porto Seguro Cia de Seguros Gerais</strong>, a cargo da Universidade Patativa do Assaré – UPA, conforme inciso IV do art. 9º da Lei 11.788/2008.</li>
                                </ol>
                            </div>
						</div>
						<br>
						<div class="row" id="cl5">
							<div class="col-sm-12 justi">
								<strong>CLÁUSULA 5ª - SÃO OBRIGAÇÕES DA INSTITUIÇÃO DE ENSINO:</strong>
								<ol class="margin justi">
									<li>Avaliar as instalações da CONCEDENTE, através de instrumento próprio;</li>
									<li>Notificar a CONCEDENTE quando ocorrer a transferência, trancamento de curso, abandono ou outro fato impeditivo da continuidade do estágio;</li>
									<li>Indicar professor-orientador da área a ser desenvolvida no estágio, para acompanhar e avaliar as atividades do estagiário;</li>
									<li>Comunicar à CONCEDENTE, no início do período letivo, as datas de realização de avaliações escolares ou acadêmicas;</li>
									<li>Aprovar o ESTÁGIO de que trata o presente instrumento, considerando as condições de sua adequação à proposta pedagógica do curso, à etapa e modalidade da formação escolar do ESTAGIÁRIO e o horário e calendário escolar;</li>
									<li>Aprovar o Plano de Atividades de Estágio que consubstancie as condições/requisitos suficientes à exigência legal de adequação à etapa e modalidade da formação escolar do ESTAGIÁRIO.</li>
								</ol>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12 justi">
								<strong>CLÁUSULA 6ª - SÃO OBRIGAÇÕES DO ESTAGIÁRIO:</strong>
                                <ol class="margin justi">
                                    <li>Cumprir com empenho e interesse toda a programação estabelecida para seu estágio;</li>
                                    <li>Observar, obedecer e cumprir as normas internas da CONCEDENTE;</li>
                                    <li>Apresentar documentos comprobatórios da regularidade da sua situação escolar, sempre que solicitado pela CONCEDENTE;</li>
                                    <li>Informar imediatamente à INSTITUIÇÃO DE ENSINO a rescisão antecipada do presente termo, para que possa adotar as providências administrativas cabíveis;</li>
                                    <li>Informar, de imediato, à CONCEDENTE, qualquer alteração na sua situação escolar, tais como: trancamento de matrícula, abandono, conclusão de curso ou transferência de Instituição de Ensino;</li>
                                    <li>Manter atualizados, junto à CONCEDENTE, seus dados pessoais e escolares;</li>
                                    <li>Entregar, obrigatoriamente, à INSTITUIÇÃO DE ENSINO, uma via do presente instrumento, devidamente assinado pelas partes;</li>
                                    <li>Informar previamente à CONCEDENTE os períodos de avaliação na Instituição de Ensino, para fins de redução da jornada de estágio;</li>
                                    <li>Preencher os relatórios de estágio, a fim de subsidiar as instituições de ensino com informações sobre estágio.</li>
                                </ol>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12 justi">
								<strong>CLÁUSULA 7ª - SÃO OBRIGAÇÕES DA CONCEDENTE:</strong>
                                <ol class="margin justi">
                                    <li>Ofertar instalações que tenham condições de proporcionar ao educando atividades de aprendizagem social, profissional e cultural;</li>
                                    <li>Zelar pelo cumprimento do presente Termo de Compromisso;</li>
                                    <li>Designar um supervisor que seja funcionário do seu quadro de pessoal, com formação ou experiência profissional na área de conhecimento desenvolvida no curso do ESTAGIÁRIO, para orientá-lo e acompanhá-lo no desenvolvimento das atividades do estágio;</li>
                                    <li>Solicitar ao ESTAGIÁRIO, a qualquer tempo, documentos comprobatórios da sua situação escolar, uma vez que, trancamento de matrícula, abandono, conclusão de curso ou transferência de instituição de ensino constituem motivo de imediata rescisão;</li>
                                    <li>Pagar a bolsa-auxílio, auxilio-alimentação e auxílio-transporte ao ESTAGIÁRIO através de processo de pagamento administrado pela UPA;</li>
                                    <li>Assegurar ao ESTAGIÁRIO recesso remunerado nos termos da lei 11.788/2008;</li>
                                    <li>Reduzir a jornada de estágio nos períodos de avaliação, previamente informados pelo ESTAGIÁRIO;</li>
                                    <li>Entregar, por ocasião do desligamento, Termo de Realização do Estágio;</li>
                                    <li>Manter os arquivos, e à disposição da fiscalização, os documentos firmados que comprovem a relação de estágio.</li>
                                </ol>
                            </div>
						</div>
                        <br>
                        <div class="row">
                            <div class="col-sm-12 justi">
                                <strong>CLÁUSULA 8ª - DAS OBRIGAÇÕES DO AGENTE DE INTEGRAÇÃO UNIVERSIDADE PATATIVA DO ASSARÉ - UPA:</strong>
                                <ol class="margin justi">
                                    <li>Firmar convênio com as Instituições de Ensino, contendo as condições exigidas pelas mesmas para a caracterização e definição dos estágios de seus alunos;</li>
                                    <li>Obter da UNIDADE CONCEDENTE a identificação das oportunidades de estágio a serem concedidas;</li>
                                    <li>Encaminhar à CONCEDENTE os alunos interessados em estagiar, conforme necessidades definidas conjuntamente;</li>
                                    <li>Promover o ajuste das condições de estágio, definidas pela INSTITUIÇÃO DE ENSINO, com as condições e disponibilidades da CONCEDENTE, através de Termos Aditivos;</li>
                                    <li>Preparar e providenciar para que a Concedente e o estudante assinem o respectivo Termo de Compromisso de Estágio, nos termos da Lei 11.788, de 25 de setembro de 2008;</li>
                                    <li>Preparar toda a documentação legal referente ao estágio, bem como efetivar o Seguro conforme citado na Cláusula 4ª item 2;</li>
                                    <li>Fornecer à CONCEDENTE, sempre que entender necessário, ou quando solicitado, instruções específicas acerca da prática e supervisão dos estágios nela realizados;</li>
                                    <li>Propiciar instrumento de controle semestral dos relatórios de atividades preenchido pelo supervisor de estágio da CONCEDENTE e informar a INSTITUÇÃO DE ENSINO;</li>
                                    <li>Controlar e acompanhar a elaboração do relatório final de estágio, de responsabilidade da concedente;</li>
                                    <li>Avaliar as instalações de estágio da CONCEDENTE, subsidiando as Instituições de Ensino conforme a Lei.</li>
                                </ol>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-12 justi">
                                <strong>CLÁUSULA 9ª - O PRESENTE TERMO DE COMPROMISSO DE ESTÁGIO E O PLANO DE ATIVIDADES DE ESTÁGIO SERÃO ALTERADOS OU PRORROGADOS ATRAVÉS DE TERMO ADITIVO, PODENDO O TERMO, NO ENTANTO, SER:</strong>
                                <ol class="margin justi">
                                    <li>Extinto automaticamente ao término do estágio;</li>
                                    <li>Rescindido por deliberação da CONCEDENTE ou do estagiário;</li>
                                    <li>Rescindido por conclusão, abandono ou trancamento de matrícula do curso realizado pelo estagiário.</li>
                                </ol>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-12 justi">
                                <strong>CLÁUSULA 10ª – A CONCEDENTE E O ESTAGIÁRIO, SIGNATÁRIOS DESTE INSTRUMENTO, DE COMUM ACORDO E PARA OS EFEITOS DA LEI Nº. 11.788/08, ELEGEM A UNIVERSIDADE PATATIVA DO ASSARÉ - UPA COMO SEU AGENTE DE INTEGRAÇÃO, A QUEM COMUNICARÃO A INTERRUPÇÃO OU EVENTUAIS MODIFICAÇÕES DO CONVENCIONADO NO PRESENTE INSTRUMENTO.</strong>
                                <p style="text-align: justify" class="termo">
                                    E, por estarem de inteiro e comum acordo com o Plano de Atividades em anexo e com as demais condições estabelecidas neste TERMO DE COMPROMISSO DE ESTÁGIO-TCE, as partes os assinam em 4 (quatro) vias de igual teor. 
                                </p>
                            </div>
                        </div>
                        <br>
    
						<div class="row body-content">
							<div class="span12">
								<p style="margin: 10px; float: right;">___________________, _____ de ______________ de _______.</p>
							</div>
						</div>
					
						<div class="col-12" style=" margin-top: 90px;">
								
							<div class="col-6">
								<div style="font-size: 10px;margin-top: 60px;border-top: 1px solid #333;">
									<p class="p">{{$tce->estudante->nmEstudante}}</p>
								</div>
								@php
								$nascimento = $tce->estudante->dtNascimento;
								$nascimento->addYears(18)->toDateString();   
								$hoje = \Carbon\Carbon::now()->toDateString();
								if($nascimento > $hoje){
									echo '<div style="font-size: 10px;margin-top: 60px;border-top: 1px solid #333;">
											<p class="p">REPRESENTANTE LEGAL DO ESTAGIÁRIO / CPF</p>
										</div>';
								}
								@endphp
								<div style="font-size: 10px;margin-top: 60px;border-top: 1px solid #333;">
									<p class="p">{{$tce->concedente->nmRazaoSocial}}</p>
								</div>
								<div style="font-size: 10px;margin-top: 60px;border-top: 1px solid #333;">
									<p class="p">{{$tce->instituicao->nmInstituicao}}</p>
								</div>					
							</div>
							<div style="clear: both"></div>
						</div>		
                    </div>
                    @include('tce.relatorio.inc_rodape')
                    
                    <div class="novaPagina"></div>

                    <table style="width: 100%;" class="header">
                        <tr>
                            <td>
                                <?php $image_path = '/img/relatorios/logo_default.png'; ?>
                                <img src="{{ public_path() . $image_path }}" alt="Upa - Estagio" />
                            </td>
                            <td>
                                <h2 style="text-align: left;">
                                    <strong>PLANO DE ATIVIDADES</strong>
                                </h2>
                            </td>
                        </tr>
                    </table>
                    <br>
                    <table style="width: 100%;" class="table table-bordered table-condensed ">
                            <tr>
                                <th colspan="3" style="text-align: center">
                                    <strong>DADOS DA CONCEDENTE</strong>
                                </th>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <strong>Empresa(Concedente):</strong><br>
                                    {{$tce->concedente->nmRazaoSocial}}
                                </td>
                            </tr>
                            <tr>
 
                                <td colspan="2">
                                    <strong>Nome do Supervisor (a) do Estágio na Concedente:</strong><br>
                                        {{$tce->nmSupervisor}}
                                </td>
                                <td>
                                    <strong>Telefone:</strong><br>
                                    @if($tce->Concedente->dsFone){{$tce->Concedente->dsFone}}@else <font color="#fff">c</font> @endif
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>E-mail:</strong><br>
                                    <font color="#fff">e</font>
                                </td>
                                <td>
                                    <strong>Formação Academica:</strong><br>
                                    <font color="#fff">e</font>
                                </td>
                                <td>
                                    <strong>Cargo:</strong><br>
                                    <font color="#fff">c</font>
                                </td>
                            </tr>
                    </table>

                    <table style="width: 100%;" class="table table-bordered table-condensed ">
                            <tr>
                                <th colspan="3" style="text-align: center">
                                    <strong>DADOS DO ESTAGIARIO</strong>
                                </th>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <strong>Nome do Estagiario:</strong><br>
                                    {{$tce->estudante->nmEstudante}}
                                </td>
                                <td>
                                    <strong>Telefone:</strong><br>
                                    @if($tce->estudante->dsFone){{$tce->estudante->dsFone}}@else <font color="#fff">c</font> @endif
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <strong>Curso:</strong><br>
                                        {{$tce->cursoDaInstituicao->curso->nmCurso}}
                                </td>
                                <td>
                                    <strong>Email:</strong><br>
                                    @if($tce->estudante->dsEmail){{$tce->estudante->dsEmail}} @else <font color="#fff">c</font>@endif
                                </td>
                            </tr>
                    </table>

                    <center>Vigência: <strong>{{$tce->dtInicio->format("d/m/Y")}}</strong> até <strong>{{$tce->dtFim->format("d/m/Y")}}</strong></center><br>

                    <table style="width: 100%;" class="table table-bordered table-condensed ">
                            <tr>
                                    <th colspan="3" style="text-align: center">
                                        <strong>ATIVIDADES A DESENVLVER NO ESTÁGIO:</strong>
                                    </th>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <ol class="margin">
                                            @foreach($tce->atividades as $atividade)	
                                                <li>{{$atividade->atividade}}</li>
                                            @endforeach
                                    </ol>
                                    <strong>Assinatura do Supervisor (a) do Estágio da Empresa:</strong>
                                </td>
                            </tr>
                    </table>

                    <table style="width: 100%;" class="table table-bordered table-condensed ">
                            <tr>
                                    <th colspan="3" style="text-align: center">
                                        <strong>PARECER DO(A) ORIENTADOR(A) PEDAGÓGICO(A):</strong>
                                    </th>
                            </tr>
                            <tr>
                                <td colspan="3">
                                        <hr style="font-size: 10px;margin-top: 20px; margin-bottom: 0px; border-bottom: 1px solid #333 ;" />
                                        <hr style="margin-top: 20px; margin-bottom: 0px; border-bottom: 1px solid #333 ;" />
                                        <hr style="margin-top: 20px; margin-bottom: 0px; border-bottom: 1px solid #333 ;" />
                                        <hr style="margin-top: 20px; margin-bottom: 0px; border-bottom: 1px solid #333 ;" />
                                        <hr style="margin-top: 20px; margin-bottom: 0px; border-bottom: 1px solid #333 ;" />
                                    <br>
                                    <strong>Assinatura do(a) Supervisor(a) do Estágio da Empresa:</strong>
                                </td>
                            </tr>
                    </table>
                    <div class="row body-content">
							<div class="span12">
								<p style="margin: 10px; float: right;">Brasilia-DF, _____ de ______________ de _______.</p>
							</div>
                    </div>
                    
                    <div class="col-12" style=" margin-top: 90px;">
                        <div class="col-6">
                            <div style="font-size: 10px;margin-top: 60px;border-top: 1px solid #333;">
                                <p class="p">{{$tce->concedente->nmRazaoSocial}}</p>
                            </div>
                            <div style="font-size: 10px;margin-top: 60px;border-top: 1px solid #333;">
                                <p class="p">{{$tce->estudante->nmEstudante}}</p>
                            </div>
                            @php
                            $nascimento = $tce->estudante->dtNascimento;
                            $nascimento->addYears(18)->toDateString();   
                            $hoje = \Carbon\Carbon::now()->toDateString();
                                if($nascimento > $hoje){
                                    echo '<div style="font-size: 10px;margin-top: 60px;border-top: 1px solid #333;">
                                            <p class="p">REPRESENTANTE LEGAL DO ESTAGIÁRIO / CPF</p>
                                        </div>';
                                }
                            @endphp
                            <div style="font-size: 10px;margin-top: 60px;border-top: 1px solid #333;">
                                <p class="p">{{$tce->instituicao->nmInstituicao}}</p>
                            </div>					
                        </div>
                        <div style="clear: both"></div>
                    </div>
                    @include('tce.relatorio.inc_rodape')		
            </div>
		</div>

		</div>
	</div>
</body>
</html>