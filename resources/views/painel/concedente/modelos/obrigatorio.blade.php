<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<title>Tce Obrigatório - TCE 
		@if($tce->migracao != NULL) 
			{{$tce->migracao}}
		@else 
			{{$tce->id}}
		@endif
	</title>
	
	<style>
		html { margin: 30px}
		body {
			font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif;
		}
		footer.fixar-rodape{
			bottom: 0;
			left: 0;
			height: 40px;
			position: fixed;
			width: 100%;
		  }
		.novaPagina {
			page-break-before: always;
			page-break-after: always
		}
		.row {
			margin-bottom: 20px;
		}
		div.body-content{
			/** Essa margem vai evitar que o conteudo fique por baixo do rodapé **/
			margin-bottom: 40px;
		}
		.col-12 {
			width: 100% !important;
		}
		.col-6 {
			width: 50% !important;
		}
		.col-6,
		.col-12 {
			float: left !important;
		}
		.table {
			border-collapse: separate;
			border-spacing: 0;
			min-width: 350px;
			margin-bottom: 10px;
		}
		.table tr th,
		.table tr td {
			border-right: 1px solid #bbb;
			border-bottom: 1px solid #bbb;
			padding: 3px;
			
		}
		.table tr th:first-child,
		.table tr th:last-child {
			border-top: solid 1px #bbb;
		}
		.table tr th:first-child,
		.table tr td:first-child {
			border-left: 1px solid #bbb;
		}
		.table tr th:first-child,
		.table tr td:first-child {
			border-left: 1px solid #bbb;
		}
		.table tr th {
			background: #eee;
			text-align: left;
		}
		.table.Info tr th,
		.table.Info tr:first-child td {
			border-top: 1px solid #bbb;
		}
		/* top-left border-radius */
		.table tr:first-child th:first-child,
		.table.Info tr:first-child td:first-child {
			border-top-left-radius: 6px;
		}
		/* top-right border-radius */
		.table tr:first-child th:last-child,
		.table.Info tr:first-child td:last-child {
			border-top-right-radius: 6px;
		}
		/* bottom-left border-radius */
		.table tr:last-child td:first-child {
			border-bottom-left-radius: 6px;
		}
		/* bottom-right border-radius */
		.table tr:last-child td:last-child {
			border-bottom-right-radius: 6px;
		}

		.marcaDagua {
			position: fixed;
			width:100%;
			height:100%;
			font-size:6rem;
			font-weight:bold;
			text-align: center;
			display: table;
			color: rgba(187, 187, 187, 0.6);
		}

		.marcaDagua p {
			display: table-cell;
  			vertical-align: middle;
		}
	</style>
</head>
<body>
	<div class="marcaDagua">
		<p> NÃO OFICIAL </p>
	</div>
	
	<div style="text-align:center;color:red;font-weight:bolder">ESTE DOCUMENTO É MERAMENTE ILUSTRATIVO</div><br>

	<div id="body">
		<div id="content">

			<div class="page" style="font-size: 7pt">
				<table style="width: 100%;" class="header">
					<tr>
						<td>
							<?php $image_path = '/img/relatorios/logo_default.png'; ?>
							<img src="{{ public_path() . $image_path }}" alt="Upa - Estagio" />
						</td>
						<td>
							<h1 style="text-align: right">
								<strong>TERMO DE COMPROMISSO DE ESTÁGIO<br>TCE 
									@if($tce->migracao != NULL) 
										{{$tce->migracao}}
									@else 
										{{$tce->id}}
									@endif
								</strong>
							</h1>
						</td>
					</tr>
				</table>

				<div class="row">
					<div class="span12">
						<p style="margin: 10px;">As partes abaixo qualificadas celebram neste ato Termo de Compromisso de Estágio, conforme a Lei n°. 11.788/08.</p>
					</div>
				</div>

				@if($tce->instituicao->poloMatriz != NULL)
				<!-- SE INSTITUIÇÃO TIVER POLO MATRIZ DEFINIDA EXIBE ENDEREÇO E DADOS DO POLO MATRIZ -->
				<!--INSTITUIÇÃO DE ENSINO-->
				<table style="width: 100%;" class="table table-bordered table-condensed ">
						<tr>
							<th colspan="2" style="text-align: center">
								<strong>DADOS DA INSTITUIÇÃO DE ENSINO</strong><span style="float: right;font-size: 6pt"> (Cod.: {{$tce->instituicao_id}})</span>
							</th>
						</tr>
						<tr>
							<td colspan="2">
								<strong>{{$tce->instituicao->poloMatriz->nmPolo}}({{$tce->instituicao->nmInstituicao}})</strong>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<strong>CNPJ:</strong> {{$tce->instituicao->poloMatriz->cdCNPJ}}
							</td>
						</tr>
						<tr>
							<td>
								<strong>ENDEREÇO: </strong>{{$tce->instituicao->poloMatriz->dsEndereco}} ,
								<strong>N°</strong> {{$tce->instituicao->poloMatriz->nnNumero}}
							</td>
							<td>
								<strong>CIDADE: </strong>{{$tce->instituicao->poloMatriz->cidade->nmCidade}} - {{$tce->instituicao->poloMatriz->cidade->estado->nmEstado}}</td>
						</tr>
						<tr>
							@if($tce->instituicao->poloMatriz->nmDiretor <> NULL)
							<td>
								<strong>REPRESENTANTE: </strong>{{$tce->instituicao->poloMatriz->nmDiretor}}
							</td>
							@else
							<td>
								<strong>REPRESENTANTE: </strong>{{$tce->instituicao->nmDiretor}}
							</td>
							@endif
							<td>
								<strong>TELEFONE: </strong> {{$tce->instituicao->poloMatriz->dsFone}}
							</td>
						</tr>
						<!--SE TIVER SIDO PREENCHIDO NMREITOR NO POLO-->
						@if($tce->instituicao->poloMatriz->nmReitor <> NULL)
							<tr>
								<td colspan="2">
									<strong>REITOR(A) MATRIZ: </strong>{{$tce->instituicao->poloMatriz->nmReitor}}
								</td>
							</tr>
						<!--SE NÂO TIVER PREENCHIDO NMREITOR NO POLO-->
						@else
							<!--SE TIVER PREENCHIDO NMREITOR NA INSTITUICAO-->
							@if($tce->instituicao->nmReitor <> NULL)
							<tr>
								<td colspan="2">
									<strong>REITOR(A): </strong>{{$tce->instituicao->nmReitor}}
								</td>
							</tr>
							@endif
						@endif

							@if(($tce->nmSupervisorIe) AND ($tce->dsSupervisorIeCargo) != NULL)
							<tr>
								<td>
									<strong>SUPERVISOR(A): </strong>{{$tce->nmSupervisorIe}}
								</td>
								<td>
									<strong>CARGO/FORMAÇÃO: </strong>{{$tce->dsSupervisorIeCargo}}
								</td>
							</tr>
							@else 
								@if(($tce->instituicao->nmSupervisor) AND ($tce->instituicao->nmSupervisorCargo))
								<tr>
									@if($tce->instituicao->id == 144)
									<td>
										<strong>RESP. ASSINATURA TCE: </strong>{{$tce->instituicao->nmSupervisor}}
									</td>
									@else
									<td>
										<strong>SUPERVISOR(A): </strong>{{$tce->instituicao->nmSupervisor}}
									</td>
									@endif
									<td>
										<strong>CARGO/FORMAÇÃO: </strong>{{$tce->instituicao->nmSupervisorCargo}}
									</td>
								</tr>
								@endif 
							@endif
				</table>
				@else
				<!-- SE INSTITUIÇÃO NÃO TIVER POLO MATRIZ DEFINIDA EXIBE ENDEREÇO E DADOS DO POLO MATRIZ -->
				<!--INSTITUIÇÃO DE ENSINO-->
				<table style="width: 100%;" class="table table-bordered table-condensed ">
						<tr>
							<th colspan="2" style="text-align: center">
								<strong>DADOS DA INSTITUIÇÃO DE ENSINO</strong><span style="float: right;font-size: 6pt"> (Cod.: {{$tce->instituicao_id}})</span>
							</th>
						</tr>
						<tr>
							<td colspan="2"><strong>{{$tce->instituicao->nmInstituicao}}</strong></td>
						</tr>
						<tr>
							<td colspan="2"><strong>CNPJ:</strong> {{$tce->instituicao->cdCNPJ}}</td>
						</tr>
						<tr>
							<td>
								<strong>ENDEREÇO: </strong>{{$tce->instituicao->dsEndereco}},<strong> N°</strong> {{$tce->instituicao->nnNumero}}
							</td>
							<td><strong>CIDADE: </strong>{{$tce->instituicao->cidade->nmCidade}} - {{$tce->instituicao->cidade->estado->nmEstado}}</td>
						</tr>
						<tr>
							<td><strong>REPRESENTANTE: </strong>{{$tce->instituicao->nmDiretor}}</td>
							<td><strong>TELEFONE: </strong> {{$tce->instituicao->dsFone}}</td>
						</tr>
						@if($tce->instituicao->nmReitor <> NULL)
							<tr>
								<td colspan="2">
									<strong>REITOR(A): </strong>{{$tce->instituicao->nmReitor}}
								</td>
							</tr>
						@endif 
						
						@if(($tce->nmSupervisorIe) AND ($tce->dsSupervisorIeCargo) != NULL)
							<tr>
								<td>
									<strong>SUPERVISOR(A): </strong>{{$tce->nmSupervisorIe}}
								</td>
								<td>
									<strong>CARGO/FORMAÇÃO: </strong>{{$tce->dsSupervisorIeCargo}}
								</td>
							</tr>
						@endif 
						@if(($tce->instituicao->nmSupervisor) AND ($tce->instituicao->nmSupervisorCargo))
							<tr>
								@if($tce->instituicao->id == 144)
									<td>
										<strong>RESP. ASSINATURA TCE: </strong>{{$tce->instituicao->nmSupervisor}}
									</td>
								@else
									<td>
										<strong>SUPERVISOR(A): </strong>{{$tce->instituicao->nmSupervisor}}
									</td>
								@endif
									<td>
										<strong>CARGO/FORMAÇÃO: </strong>{{$tce->instituicao->nmSupervisorCargo}}
									</td>
							</tr>
						@endif
				</table>
				@endif
				
				<!--CONCEDENTE-->
				<table style="width: 100%;" class="table table-bordered table-condensed">
					<tbody>
						<tr>
							<th colspan="3" style="text-align: center;">
								<strong>DADOS DA CONCEDENTE</strong><span style="float: right;font-size: 6pt"> (Cod.: {{$tce->concedente_id}})</span>
							</th>
						</tr>
						<tr>
							<td colspan="3">
								<strong>LOCAL DE REALIZAÇÃO DE ESTÁGIO:</strong> {{$tce->concedente->nmRazaoSocial}}
							</td>
						</tr>
						<!--SE O TCE FOR DE UMA SECRETARIA DA CONCEDENTE - até linha 373-->
						@if($tce->sec_conc_id <> NULL)
							<tr>
								<td colspan="3">
									<strong>SECRETARIA:</strong> {{$tce->secConcedente->nmSecretaria}} <span style="float: right;font-size: 5pt"> (Cod.: {{$tce->concedente_id}}/{{$tce->secConcedente->id}})</span>
								</td>
							</tr>
							<tr>
								<td colspan="3">
									<strong>CNPJ: </strong>{{$tce->secConcedente->cdCnpjCpf}}
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<strong>ENDEREÇO: </strong> {{$tce->secConcedente->dsEndereco}} , 
									<strong>N°</strong> {{$tce->secConcedente->nnNumero}}
								</td>
								<td>
									@if($tce->secConcedente->nmBairro)<strong>BAIRRO: </strong>{{$tce->secConcedente->nmBairro}}@endif
								</td>
							</tr>
							@if($tce->secConcedente->dsComplemento)
							<tr>
								<td colspan="3">
									<strong>COMPLEMENTO: </strong> {{$tce->secConcedente->dsComplemento}}
								</td>
							</tr>
							@endif
							<tr>
								<td colspan="2">
									<strong>CIDADE: </strong>{{$tce->concedente->cidade->nmCidade}} - {{$tce->concedente->cidade->estado->nmEstado}}
								</td>
								<td>
									<strong>CEP: </strong>{{$tce->secConcedente->cdCEP}}
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<strong>TELEFONE: </strong>{{$tce->secConcedente->dsFone}}
								</td>
								<td>
									<strong>OUTRO: </strong> @if  ( $tce->secConcedente->dsOutroFone != NULL) {{$tce->secConcedente->dsOutroFone}} @else - @endif
								</td>
							</tr>
							@if(($tce->concedente->nmResponsavel) AND ($tce->concedente->dsResponsavelCargo) != NULL)
							<tr>
								<td colspan="2">
									<strong>REPRESENTADO PELO SR(A): </strong>{{$tce->concedente->nmResponsavel}}
								</td>
								<td>
									<strong>CARGO: </strong>{{$tce->concedente->dsResponsavelCargo}}
								</td>
							</tr>
							@endif
							@if(($tce->secConcedente->nmResponsavel) AND ($tce->secConcedente->dsResponsavelCargo) != NULL)
							<tr>
								<td colspan="2">
									<strong>RESP. ADMINISTRATIVO(A): </strong>{{$tce->secConcedente->nmResponsavel}}
								</td>
								<td>
									<strong>CARGO: </strong>{{$tce->secConcedente->dsResponsavelCargo}}
								</td>
							</tr>
							@endif
							@if(($tce->nmSupervisor) AND ($tce->dsSupervisorCargo) != NULL)
							<tr>
								<td colspan="2">
									<em><strong>SUPERVISOR(A): </strong>{{$tce->nmSupervisor}}</em>
								</td>
								<td>
									<em><strong>CARGO/FORMAÇÃO: </strong>{{$tce->dsSupervisorCargo}}</em>
								</td>
							</tr>
							@endif

						<!--SE NÂO FOR DE SECRETARIA EXIBE OS DADOS DA CONCEDENTE-->
						@else
							@if($tce->concedente->flTipo == "PF")
							<tr>
								<td colspan="2">
									<strong>CPF: </strong>{{$tce->concedente->cdCnpjCpf}}
								</td>
								<td>
									<strong>ORGÃO REGULADOR: </strong>{{$tce->concedente->dsOrgaoRegulador}}
								</td>
							</tr>
							@else
							<tr>
								<td colspan="3">
									<strong>CNPJ: </strong>{{$tce->concedente->cdCnpjCpf}}
								</td>
							</tr>
							@endif
							<tr>
								<td colspan="2">
									<strong>ENDEREÇO: </strong> {{$tce->concedente->dsEndereco}} , 
									<strong>N°</strong> {{$tce->concedente->nnNumero}}
								</td>
								<td>
									@if($tce->concedente->nmBairro)<strong>BAIRRO: </strong>{{$tce->concedente->nmBairro}}@endif
								</td>
							</tr>
							@if($tce->concedente->dsComplemento)
							<tr>
								<td colspan="3">
									<strong>COMPLEMENTO: </strong> {{$tce->concedente->dsComplemento}}
								</td>
							</tr>
							@endif
							<tr>
								<td colspan="2">
									<strong>CIDADE: </strong>{{$tce->concedente->cidade->nmCidade}} - {{$tce->concedente->cidade->estado->nmEstado}}
								</td>
								<td>
									<strong>CEP: </strong>{{$tce->concedente->cdCEP}}
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<strong>TELEFONE: </strong>{{$tce->concedente->dsFone}}
								</td>
								<td>
									<strong>OUTRO: </strong> @if  ( $tce->concedente->dsOutroFone != NULL) {{$tce->concedente->dsOutroFone}} @else - @endif
								</td>
							</tr>
							<!--mostrar resp da concedente no tce-->
							@if(($tce->concedente->nmResponsavel) AND ($tce->concedente->dsResponsavelCargo) != NULL)
							<tr>
								<td colspan="2">
									<strong>REPRESENTADO PELO SR(A): </strong>{{$tce->concedente->nmResponsavel}}
								</td>
								<td>
									<strong>CARGO: </strong>{{$tce->concedente->dsResponsavelCargo}}
								</td>
							</tr> 
							@endif
							<!--mostrar resp da concedente no tce fim-->
							@if(($tce->nmSupervisor) AND ($tce->dsSupervisorCargo) != NULL)
							<tr>
								<td colspan="2">
									<strong>SUPERVISOR(A): </strong>{{$tce->nmSupervisor}}
								</td>
								<td>
									<strong>CARGO/FORMAÇÃO: </strong>{{$tce->dsSupervisorCargo}}
								</td>
							</tr> 
							@endif
						<!--ABAIXO FIM DO IF SE É SECRETARIA DE CONCEDENTE-->
						@endif
						@if($tce->dsLotacao)
						<tr>
							<td colspan="3">
								<strong>LOTAÇÃO:</strong> {{$tce->dsLotacao}}
							</td>
						</tr>
						@endif
					</tbody>
				</table>

				<!--ESTAGIARIO-->
				<table style="width: 100%;" class="table table-bordered table-condensed">
					<tr>
						<th colspan="3" style="text-align: center">
							<strong>DADOS DO ESTÁGIARIO </strong><span style="float: right;font-size: 6pt">(Cod.: {{$tce->estudante_id}})</span>
						</th>
					</tr>
					<tr>
						<td colspan="">
							<strong>NOME: </strong>{{$tce->estudante->nmEstudante}}
						</td>
						<td>
							<strong>DATA NASC: </strong>{{$tce->estudante->dtNascimento->format('d/m/Y')}}
						</td>
						<td>
							<strong>CPF: </strong>{{$tce->estudante->cdCPF}}
						</td>
					</tr>
					<tr>
						<td>
							<strong>RG: </strong>{{$tce->estudante->cdRG}}
						</td>
						<td>
							<strong>ORG.: </strong>{{$tce->estudante->dsOrgaoRG}}
						</td>
						<td>
							@if($tce->estudante->dsEmail)<strong>EMAIL: </strong>{{$tce->estudante->dsEmail}} @endif
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<strong>ENDEREÇO: </strong> {{$tce->estudante->dsEndereco}},
							<strong> N°</strong> {{$tce->estudante->nnNumero}}
						</td>
						<td>
							<strong>BAIRRO: </strong>{{$tce->estudante->nmBairro}}
						</td>
					</tr>
					@if($tce->estudante->dsComplemento)
					<tr>
						<td colspan="3">
							<strong>COMPLEMENTO: </strong> {{$tce->estudante->dsComplemento}}
						</td>
					</tr>
					@endif
					<tr>
						<td colspan="1">
							<strong>CIDADE: </strong>{{$tce->estudante->cidade->nmCidade}} - {{$tce->estudante->cidade->estado->nmEstado}}
						</td>
						<td>
							<strong>CEP: </strong>{{$tce->estudante->cdCEP}}
						</td>
						<td>
							@if($tce->estudante->dsFone)<strong>TELEFONE: </strong>{{$tce->estudante->dsFone}}@endif
						</td>
					</tr>
					<tr>
						<td>
							<strong>CURSO: </strong> {{$tce->cursoDaInstituicao->curso->nmCurso}}
						</td>
						<td>
							<strong>{{$tce->cursoDaInstituicao->cursoDuracao->nmTipoDuracao}}: </strong>
							{{$tce->nnSemestreAno}} {{$tce->cursoDaInstituicao->cursoDuracao->nmTipoDuracao}}
						 </td>
						<td>
							<strong>TURNO: </strong>{{$tce->estudante->turno->nmTurno}} 
						</td>
					</tr>
				</table>

				<div class="texto">
						<div class="row" id="cl1">
							<div class="col-sm-12">
								<p style="text-align: justify" class="termo">
									<strong>CLÁUSULA 1ª -</strong>
									Este instrumento tem por objetivo formalizar as condições para a realização de ESTÁGIO DE ESTUDANTE e particularizar a relação jurídica especial existente entre o ESTUDANTE, a CONCEDENTE e a INSTITUIÇÃO DE ENSINO caracterizando a não vinculação empregatícia, nos termos da Lei nº. 11.788, de 25 de setembro de 2008 e demais disposições vigentes. 
								</p>
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-sm-12">
									<strong>CLÁUSULA 2ª - FICAM COMPROMISSADAS ENTRE AS PARTES AS SEGUINTES CONDIÇÕES PARA REALIZAÇÃO DO ESTÁGIO:</strong>
									<!--<br>O presente Termo de Compromisso de Estágio vigerá do dia _____/_____/_________ até _____/_____/_________-->
									<ol class="margin justi">
										<li>Vigência de: <strong>{{$tce->dtInicio->format("d/m/Y")}}</strong> até <strong>{{$tce->dtFim->format("d/m/Y")}}</strong></li>
										
										@if(substr($tce->periodo->dsPeriodo, -6) == "SENDO:")
											@if($tce->hrInicio != NULL)
												<li>Horário: <strong>{{$tce->periodo->dsPeriodo}} {{$tce->dsPeriodo}} de {{substr($tce->hrInicio, 0, 5)}} as {{substr($tce->hrFim, 0, 5)}}</strong>.</li>
											@else
												<li>Horário: <strong>{{$tce->periodo->dsPeriodo}} {{$tce->dsPeriodo}}</strong>.</li>
											@endif
										@else
											<li>Horário: <strong>{{$tce->periodo->dsPeriodo}}</strong>.</li>
										@endif
										{{-- @if($tce->vlAuxilioMensal != NULL)
										<li>Bolsa-Auxílio mensal de: <strong>R${{number_format($tce->vlAuxilioMensal, 2, ',', '.')}}</strong> ({{Extenso::Converter($tce->vlAuxilioMensal)}})
											@if (isset($tce->vlAuxilioAlimentacao) && !empty($tce->vlAuxilioAlimentacao) && !is_null($tce->vlAuxilioAlimentacao))
												@if ($tce->vlAuxilioTransporte)
												,  Auxílio-Alimentação de <strong>R${{number_format($tce->vlAuxilioAlimentacao, 2, ',', '.')}}</strong> ({{Extenso::Converter($tce->vlAuxilioAlimentacao)}})
												@else
												e  Auxílio-Alimentação de <strong>R${{number_format($tce->vlAuxilioAlimentacao, 2, ',', '.')}}</strong> ({{Extenso::Converter($tce->vlAuxilioAlimentacao)}})
												@endif
											@endif
											@if (isset($tce->vlAuxilioTransporte) && !empty($tce->vlAuxilioTransporte) && !is_null($tce->vlAuxilioTransporte))
												@if ($tce->vlAuxilioAlimentacao)
												e Auxílio-Transporte de <strong>R${{number_format($tce->vlAuxilioTransporte, 2, ',', '.')}}</strong> ({{Extenso::Converter($tce->vlAuxilioTransporte)}})
												@elseif(isset($tce->vlAuxilioAlimentacao) && isset($tce->vlAuxilioTransporte))
												e Auxílio-Transporte de <strong>R${{number_format($tce->vlAuxilioTransporte, 2, ',', '.')}}</strong> ({{Extenso::Converter($tce->vlAuxilioTransporte)}})
												@else
												e Auxílio-Transporte de <strong>R${{number_format($tce->vlAuxilioTransporte, 2, ',', '.')}}</strong> ({{Extenso::Converter($tce->vlAuxilioTransporte)}})
												@endif
											@endif
											@php
												$total = $tce->vlAuxilioMensal+$tce->vlAuxilioAlimentacao+$tce->vlAuxilioTransporte;
											@endphp
											@if(isset($tce->vlAuxilioAlimentacao) OR isset($tce->vlAuxilioTransporte))
											totalizando <strong>R${{number_format($total, 2, ',', '.')}}</strong> ({{Extenso::Converter($total)}}).
											@endif
											@if(isset($tce->auxDescriminado))
												{{$tce->auxDescriminado}}.
											@endif
										</li>
                                        @endif --}}
                                        <li>Bolsa-Auxílio mensal de: <b>SEM REMUNERAÇÃO.</b></li>
										<li>O ESTAGIÁRIO durante a vigência do presente Termo de Compromisso de Estágio estará segurando contra acidentes pessoais conforme <strong>Apólice Coletiva de n°. 9.850-3 da Porto Seguro Cia de Seguros Gerais</strong> a cargo da Universidade Patativa do Assaré - UPA.</li>
									</ol>
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-sm-12">
								<strong class="justi">CLÁUSULA 3ª - CABE À INSTITUIÇÃO DE ENSINO</strong>
								<ol class="margin justi">
										<li>Aprovar o ESTÁGIO de que trata o presente instrumento, considerando as condições de sua adequação à proposta pedagógica do curso, à etapa e modalidade de formação escolar do ESTAGIÁRIO e ao horário e calendário escolar;</li>
										<li>Aprovar o Plano de Atividades de Estágio que consubstancie as condições / requisitos suficientes à exigência legal de adequação à etapa e modalidade da formação escolar do ESTAGIÁRIO;</li>
										<li>Avaliar as instalações da CONCEDENTE;</li>
										<li>Indicar professor orientador, da área a ser desenvolvida no ESTÁGIO, como responsável pelo acompanhamento e avaliação das atividades do ESTAGIÁRIO;</li>
										<li>Comunicar à parte concedente do estágio, no início do período letivo, as datas de realização de avaliações escolares ou acadêmicas, caso não adote outro sistema de avaliação.</li>
								</ol>
							</div>
						</div>
						<div class="row" id="cl4">
							<div class="col-sm-12">
								<strong class="justi">CLÁUSULA 4ª - CABE À CONCEDENTE</strong>
								<ol class="margin justi">
									<li>Zelar pelo cumprimento do presente Termo de Compromisso de Estágio;</li>
									<li>Proporcionar ao ESTAGIÁRIO instalações que tenham condições para o exercício das atividades práticas compatíveis com plano de atividades de estágio;</li>
									<li>Designar um supervisor que seja funcionário de seu quadro de pessoal, com formação ou experiência profissonal na área de conhecimento desenvolvida no curso do ESTAGIÁRIO, para orientá-lo e acompanhá-lo no desenvolvimento das atividades do estágio;</li>
									<li>Solicitar ao ESTAGIÁRIO, a qualquer tempo, documentos comprobátorios da regularidade da situação escolar, uma vez que trancar a matrícula, abandono, conclusão de curso ou transferência de Instituição de Ensino constituem motivos de imediata rescisão;</li>
									<li>Efetuar o pagamento da bolsa-auxílio e conceder auxílio transporte diretamente ao ESTAGIÁRIO;</li>
									<li>Conceder recesso remunerado a ser gozado preferenciamente durante as férias escolares, no termos da legislação vigente 11788/08;</li>
									<li>Reduzir a jornada de estágio nos períodos de avaliação, previamente informados pelo ESTAGIÁRIO;</li>
									<li>Elaborar os Rélatorios de Atividades semestrais para encaminhamento a Instituição de Ensino com vista obrigatória do ESTAGIÁRIO;</li>
									<li>Entregar, por ocasião do desligamento, Termo de Realização do Estágio com indicação resumida das atividades desenvolvidas, dos períodos e da avaliação de desempenho;</li>
									<li>Manter em arquivo e à disposição da fiscalização os documentos firmados que comprovem a relação de estágio;</li>
									<li>Informar a Universidade Patativa do Assaré - UPA a rescisão antecipada deste instrumento, para as devidas providencias administrativas que se fizerem necessárias;</li>
									<li>Permitir o início das atividades de estágio apenas após o recebimento deste instrumento assinado pelas 3 (três) partes signatárias.</li>
								</ol>
							</div>
						</div>
						<br>
						<div class="row" id="cl5">
							<div class="col-sm-12 justi">
								<strong>CLÁUSULA 5ª - CABE AO ESTAGIÁRIO</strong>
								<ol class="margin">
									<li>Cumprir, com todo empenho e interesse, toda programação estabelecida para seu ESTÁGIO;</li>
									<li>Observar, obedecer e cumprir as normas internas da CONCEDENTE, preservando o sigilo e a confidencialidade das informações que tiver acesso;</li>
									<li>Apresentar documentos comprobatórios da regularidade de sua situação escolar, sempre que solicitado pela CONCEDENTE;</li>
									<li>Manter rigorosamente atualizados seus dados cadastrais e escolares, junto à Concedente e a Instituição de Ensino;</li>
									<li>Informar de imediato, qualquer alteração na sua situação escolar, tais como: trancamento de matrícula, abandono, conclusão de curso ou transferência de Instituição de Ensino;</li>
									<li>Entregar, obrigatoriamente, à Instituição de Ensino, à Concedente uma via do presente instrumento, devidamente assinado pelas partes;</li>
									<li>Informar previamente à CONCEDENTE os períodos de avaliação na Instituição de Ensino, para fins de redução da jornada de estágio;</li>
									<li>Fornecer semestralmente os Rélatórios de Atividades previstos na lei 11.788, de 25 de setembro de 2008 e demais disposições vigentes.</li>
								</ol>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12 columns justi">
								<strong>CLÁUSULA 6ª -</strong> O presente instrumento e o Plano de Atividades de Estágio serão alterados ou prorrogados através dos TERMOS ADITIVOS.
								<p>
									<strong>PARÁGRAFO PRIMEIRO: </strong>O presente Termo de Compromisso de Estágio pode ser denunciado, a qualquer tempo, mediante a comunicação escrita, pela Instituição de Ensino, pela Concedente ou pelo Estagiário.<br>
									<strong>PARÁGRAFO SEGUNDO: </strong>O não cumprimento de quaisquer cláusulas do presente TERMO DE COMPROMISSO DE ESTÁGIO constitui motivos de imediata rescisão.
								</p>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12 justi">
								<strong>CLÁUSULA 7ª -</strong> A INSTITUIÇÃO DE ENSINO, a CONCEDENTE e o ESTAGIÁRIO, signatários deste instrumento, de comum acordo e para os efeitos da Lei n°. 11.788/08 elegem a Universidade Patativa do Assaré - UPA como seu AGENTE DE INTEGRAÇÃO aquem comunicarão a interrupção ou eventuais modificações do convencionamento no presente instrumento.
									<br>
									<br> 
									E, por estarem de inteiro e comum acordo com o Plano de Atividades em anexo e com as demais condições estabelecidas
									neste TERMO DE COMPROMISSO DE ESTÁGIO-TCE, as partes os assinam em 4 (quatro) vias de igual teor.
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-sm-12 justi">
								<strong>PLANO DE ATIVIDADES DE ESTÁGIO:</strong>
									<ol class="margin">
										@foreach($tce->atividades as $atividade)	
											<li>{{$atividade->atividade}}</li>
										@endforeach
									</ol>
							</div>
						</div>
						<div class="row body-content">
							<div class="span12">
								<p style="margin: 10px; float: right;">___________________, _____ de ______________ de _______.</p>
							</div>
						</div>
					
						<div class="col-12" style=" margin-top: 70px;">
								
							<div class="col-6">
								<div style="font-size: 10px;margin-top: 50px;border-top: 1px solid #333;">
									<p class="p">{{$tce->estudante->nmEstudante}}</p>
								</div>
								@php
								$nascimento = $tce->estudante->dtNascimento;
								$nascimento->addYears(18)->toDateString();   
								$hoje = \Carbon\Carbon::now()->toDateString();
								if($nascimento > $hoje){
									echo '<div style="font-size: 10px;margin-top: 50px;border-top: 1px solid #333;">
											<p class="p">REPRESENTANTE LEGAL DO ESTAGIÁRIO / CPF</p>
										</div>';
								}
								@endphp
								<!--//Se for secretaria exibe concedente - secretaria no rodape para assinaturas-->
								@if($tce->sec_conc_id <> NULL)
									<div style="font-size: 10px;margin-top: 50px;border-top: 1px solid #333;">
										<p class="p">{{$tce->concedente->nmRazaoSocial}}<br>- {{$tce->secConcedente->nmSecretaria}}</p>
									</div>
								@else
									<div style="font-size: 10px;margin-top: 50px;border-top: 1px solid #333;">
										<p class="p">{{$tce->concedente->nmRazaoSocial}}</p>
									</div>
								<!--//FIM Se for secretaria exibe concedente - secretaria no rodape para assinaturas-->
								@endif
								<div style="font-size: 10px;margin-top: 50px;border-top: 1px solid #333;">
									<p class="p">{{$tce->instituicao->nmInstituicao}}</p>
								</div>
								{{--  Se a concedente tiver escolhido a opção de exibir assinatura da agente de integração upa  --}}
								@if($tce->concedente->assUpaTCE <> NULL)
								<div style="font-size: 10px;margin-top: 50px;border-top: 1px solid #333;">
									<p class="p">UNIVERSIDADE PATATIVA DO ASSARÉ - UPA</p>
								</div>
								@endif
								
							</div>
							<div style="clear: both"></div>
						</div>
						{{-- <div class="row">
								<div class="col-6 invoice-col">
								  From
								  <address>
									<strong>Admin, Inc.</strong><br>
									795 Folsom Ave, Suite 600<br>
									San Francisco, CA 94107<br>
									Phone: (804) 123-5432<br>
									Email: info@almasaeedstudio.com
								  </address>
								</div>
								<!-- /.col -->
								<div class="col-4 invoice-col">
								  To
								  <address>
									<strong>John Doe</strong><br>
									795 Folsom Ave, Suite 600<br>
									San Francisco, CA 94107<br>
									Phone: (555) 539-1037<br>
									Email: john.doe@example.com
								  </address>
								</div>
								<!-- /.col -->
								<div class="col-4 invoice-col">
								  <b>Invoice #007612</b><br>
								  <br>
								  <b>Order ID:</b> 4F3S8J<br>
								  <b>Payment Due:</b> 2/22/2014<br>
								  <b>Account:</b> 968-34567
								</div>
								<!-- /.col -->
							  </div>	 --}}
						@include('tce.relatorio.inc_rodape')
					</div>
			</div>

		</div>
	</div>
</body>
</html>