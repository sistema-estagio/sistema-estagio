@extends('layouts.painel2')
@section('title_postfix', ' - '.$titulo)
@section('content')

<!-- Header -->
<div class="header bg-gradient-primary pb-4 pt-5 pt-md-8">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row">
        <div class="col-lg-12">
          <a href="{{URL::previous()}}" class="btn btn-default ad-click-event btn-xs">Voltar</a>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="container-fluid mt-4">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <div class="card card-default shadow">
        <div class="card-header border-0">
          <div class="row">
            <div class="col-md-6">
              <h4 class="text-left">Folhas de Pagamento do Tce</h4>
            </div>
            <div class="col-md-6 text-right">
              {{$tce->estudante->nmEstudante}} - {{$tce->estudante->cdCPF}} -
              @if($tce->migracao != NULL)
              TCE n° {{$tce->migracao}}
              @else
              TCE n° {{$tce->id}}
              @endif
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            @if($tce->dtCancelamento != NULL)
              <div class="alert alert-danger">
                <strong> TCE CANCELADO </strong>
                <br> Esse TCE foi cancelado em <strong>{{$tce->dtCancelamento->format('d/m/Y')}}</strong>.
              </div>
            @endif
          </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table align-items-center table-flush col-12">
              <tbody>
                <tr>
                  <th style="width:3%;" class="text-center">FOLHA FECHADA</th>
                  <th style="width:20%;">REFERÊNCIA</th>
                  <th style="width:10%;">SITUAÇÃO</th>
                  <th style="width:15%;" class="text-center">DIAS TRABALHADOS</th>
                  <th style="width:15%;" class="text-center">FALTAS NÃO JUSTIFICADAS</th>
                  <th style="width:15%;" class="text-center">FALTAS JUSTIFICADAS</th>
                  <th style="width:15%;" class="text-center">DIAS DE RECESSO</th>
                  <th style="width:15%;">VALOR TOTAL</th>
                </tr>
                @forelse($folhasPagamentoItem as $folha)
                <tr class="@if($folha->dtPagamento != null) table-success @endif">
                  <td class="text-center">{{$folha->folha->fechado_at != null ? 'Sim' : 'Não' }}</td>
                  <td>{{$folha->referenciaMes}}/{{$folha->referenciaAno}}</td>
                  <td>
                    @if($folha->dtPagamento == NULL)
                    A RECEBER
                    @else
                    PAGO
                    @endif
                  </td>
                  <td class="text-center">
                    {{$folha->diasTrabalhados != null ? $folha->diasTrabalhados : "Não registrado"}}
                  </td>
                  <td class="text-center">
                    {{$folha->faltas != null ? $folha->faltas : "Não registrado"}}
                  </td>
                  <td class="text-center">
                    {{$folha->justificadas != null ? $folha->justificadas : "Não registrado"}}
                  </td>
                  <td class="text-center">
                    {{$folha->diasRecesso != null ? $folha->diasRecesso : "Não registrado"}}
                  </td>
                  <td>
                    R$ {{$folha->vlTotal}}
                  </td>
                </tr>
                @empty
                <tr>
                  <td colspan="8" align="center">
                    <span class="badge badge-pill badge-warning">Não existe registros no banco!</span>
                  </td>
                </tr>
                @endforelse
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

  @stop
