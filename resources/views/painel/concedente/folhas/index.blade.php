@extends('layouts.painel2')
@section('content')
@php
$data = [
1 => 'JANEIRO',
2 => 'FEVEREIRO',
3 => 'MARÇO',
4 => 'ABRIL',
5 => 'MAIO',
6 => 'JUNHO',
7 => 'JULHO',
8 => 'AGOSTO',
9 => 'SETEMBRO',
10 => 'OUTUBRO',
11 => 'NOVEMBRO',
12 => 'DEZEMBRO'
];
$nextMonth = (\Carbon\Carbon::now()->month + 1) <= 12 ? (\Carbon\Carbon::now()->month + 1).('-').$data[(\Carbon\Carbon::now()->month + 1)].'/'.(\Carbon\Carbon::now()->year + 1) : '1-'.$data[1].'/'.(\Carbon\Carbon::now()->year + 1);
@endphp
<!-- Header -->
<div class="header bg-gradient-primary pb-4 pt-5 pt-md-8">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row">
        <div class="col-md-4">
          <h2 class="text-left text-white">
            Folhas de Registro de Faltas
            <br>
            <small>Gere e controle folhas de faltas</small>
          </h2>
        </div>
        <div class="col-md-4">

        </div>
        <div class="col-md-4">
          @if(($folhas->where('referenciaMes',\Carbon\Carbon::now()->month.'-'.$data[\Carbon\Carbon::now()->month])->where('referenciaAno', \Carbon\Carbon::now()->year)->count() == 0) && ($folhas->where('referenciaMes',explode('/',$nextMonth)[0])->where('referenciaAno', explode('/',$nextMonth)[1])->count() == 0))
          <button type="button" name="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#addFolha"><i class="fa fa-plus"></i> Add Registro</button>
          @endif
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid mt-4">
  <!-- Card stats -->
  <div class="row">
    @forelse($folhas->sortBy('created_at') as $folha)
    <div class="col-xl-3 col-lg-6 mt-4">
      <a href="{{route('painel.concedente.folha.show', ['hash'=>$folha->hash])}}" data-toggle="tooltip" data-placement="top" title="Clique para visualizar a folha">
        <div class="card card-stats mb-4 mb-xl-0">
          <div class="card-body">
            <div class="row">
              <div class="col">
                <h5 class="card-title text-uppercase text-muted mb-0">Referência</h5>
                <span class="h2 font-weight-bold mb-0">{{$folha->referenciaMes.'/'.$folha->referenciaAno}}</span>
              </div>
              <div class="col-auto">
                @if($folha->fechado == 'N')
                <div class="icon icon-shape bg-blue text-white rounded-circle shadow">
                  <i class="fas fa-file"></i>
                </div>
                @else
                <div class="icon icon-shape bg-success text-white rounded-circle shadow">
                  <i class="fas fa-check"></i>
                </div>
                @endif
              </div>
            </div>
            <p class="mt-3 mb-0 text-muted text-sm">
              <span class="text-nowrap">Quantidade de</span>
              <span class="text-primary mr-2"><b>{{ App\Models\FolhaPagamentoItem::where('folha_id',$folha->id)->get()->count() }}</b></span>
              <span class="text-nowrap">Estagiários</span>
            </p>
          </div>
        </div>
      </a>
    </div>
    @empty
    <div class="col-xl-3 col-lg-6 mt-4">
      <div class="card card-stats mb-4 mb-xl-0">
        <div class="card-body">
          <div class="row justify-content-center">
            <div class="col">
              <h5 class="card-title text-uppercase text-center mb-0">Nenhuma folha cadastrada</h5>
            </div>
          </div>
        </div>
      </div>
    </div>
    @endforelse
  </div>
  <div class="modal" tabindex="-1" role="dialog" id="addFolha">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Criar Folha</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{ route('painel.concedente.folhas.store')}}" method="post">
          <div class="modal-body">
            {!! csrf_field() !!}
            <div class="form-group row">
              <div class="col-md-12">
                <label for="">Mês</label>
                <select class="form-control" name="referencia" required>
                  <option value="">Selecione um mês</option>
                  @if($folhas->where('referenciaMes',\Carbon\Carbon::now()->month.'-'.$data[\Carbon\Carbon::now()->month])->where('referenciaAno', \Carbon\Carbon::now()->year)->count() == 0)
                  <option value="{{ \Carbon\Carbon::now()->month.'-'.$data[\Carbon\Carbon::now()->month].'/'.(\Carbon\Carbon::now()->year)}}" selected>{{ $data[\Carbon\Carbon::now()->month]}}</option>
                  @endif
                  @if($folhas->where('referenciaMes',explode('/',$nextMonth)[0])->where('referenciaAno', explode('/',$nextMonth)[1])->count() == 0)
                  <option value="{{$nextMonth}}">{{ explode('/',$nextMonth)[0] }}</option>
                  @endif
                </select>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            <button type="submit" class="btn btn-primary">Criar</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection
