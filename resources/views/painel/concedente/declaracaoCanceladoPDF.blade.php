<!DOCTYPE html>
<html>
<head>
    <title>Declaração de Estágio</title>
    <style>
            html { 
                margin-left: 2,5cm;
                margin-right: 2,5cm;
                margin-bottom: 20px;
            }

            body {
                font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial;
                font-size: 11pt;
            }
    
            footer.fixar-rodape {
                bottom: 0;
                left: 0;
                height: 40px;
                position: fixed;
                width: 100%;
                margin-left: 0;
            }
    
            .novaPagina {
                page-break-before: always;
                page-break-after: always
            }
    
            div.body-content {
                /** Essa margem vai evitar que o conteudo fique por baixo do rodapé **/
                margin-bottom: 40px;
            }
    
            .col-12 {
                width: 100% !important;
            }
    
            .col-6 {
                width: 50% !important;
            }
    
            .col-6,
            .col-12 {
                float: left !important;
            }
    
            .table {
                border-collapse: separate;
                border-spacing: 0;
                min-width: 350px;
                margin-bottom: 10px;
            }
            .table th tr {
                font-size: 5pt;
            }
            .table tr th,
            .table tr td {
                border-right: 1px solid #bbb;
                border-bottom: 1px solid #bbb;
                padding: 5px;
            }
    
            .table th {
                border-top: solid 1px #bbb;
            }
    
            .table tr th:first-child,
            .table tr th:last-child {
                border-top: solid 1px #bbb;
            }
    
            .table tr th:first-child,
            .table tr td:first-child {
                border-left: 1px solid #bbb;
            }
    
            .table tr th:first-child,
            .table tr td:first-child {
                border-left: 1px solid #bbb;
            }
    
            .table tr th {
                background: #eee;
                text-align: left;
            }
    
            .table.Info tr th,
            .table.Info tr:first-child td {
                border-top: 1px solid #bbb;
            }
            /* top-left border-radius */
    
            .table tr:first-child th:first-child,
            .table.Info tr:first-child td:first-child {
                border-top-left-radius: 6px;
            }
            /* top-right border-radius */
    
            .table tr:first-child th:last-child,
            .table.Info tr:first-child td:last-child {
                border-top-right-radius: 6px;
            }
            /* bottom-left border-radius */
    
            .table tr:last-child td:first-child {
                border-bottom-left-radius: 6px;
            }
            /* bottom-right border-radius */
    
            .table tr:last-child td:last-child {
                border-bottom-right-radius: 6px;
            }
        </style>
    </head>
    <body>
        <div id="body">
            <div id="content">
                <div class="page">
                    <div class="row" style="text-align: center">
                            <?php $image_path = '/img/relatorios/logo-declaracao.png'; ?>
							<img src="{{ public_path() . $image_path }}" alt="Upa - Estagio" />
                    </div>
                    <br><br><br><br>
                    <div class="row">
                            <h2 style="text-align: center">
                                <strong>DECLARAÇÃO</strong>
                            </h2>
                    </div>
                    
                    <div class="texto">
                            <div class="row">
                                <div class="col-sm-12">
                                    <p style="text-align: justify" class="termo">
                                        Declaramos para os devidos fins, a pedido da parte interessada, que {{$tce->estudante->nmEstudante}}, 
                                        aluno(a) do curso de {{$tce->CursoDaInstituicao->curso->nmCurso}}, da {{$tce->instituicao->nmInstituicao}} realizou estágio no(a) {{$tce->concedente->nmRazaoSocial}}, 
                                        no período de {{date('d/m/Y',strtotime($tce->dtInicio))}} à {{date('d/m/Y',strtotime($tce->dtCancelamento))}}
                                        {{-- @if($tce->aditivos->count() > 0)
                                            @if($tce->aditivos()->latest()->first()->dtInicio != Null)
                                                {{date('d/m/Y',strtotime($tce->aditivos()->latest()->first()->dtFim))}}
                                            @else
                                                {{date('d/m/Y',strtotime($tce->dtFim))}}
                                            @endif
                                        @else
                                            {{date('d/m/Y',strtotime($tce->dtFim))}}
                                        @endif --}}
                                    , cumprindo carga horária semanal de {{$tce->periodo->nmPeriodo}}. Sendo administrado pela UNIVERSIDADE PATATIVA DO ASSARÉ (Agente de Integração de Estagio).
                                            
                                    {{-- <br><br>Quanto à qualidade de trabalho e o conhecimento técnico, o estagiário foi considerado excelente.  --}}
                                   
                                    </p>
                                </div>
                            </div>
                            <br><br><br>
                            <table width="35%" cellspacing="0" cellpadding="0" border="0" align="center">
                                <tbody>
                                    <tr>
                                        <td style="border-bottom:dashed; border-bottom-width:1px;" align="center">
                                            <?php $image_path = '/img/assPalacio.jpg'; ?>
                                            <img src="{{ public_path() . $image_path }}" alt="Ass. Palacio" width="211" height="58"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" style="font-size:9pt;">
                                            Francisco  Palacio Leite
                                              <br>
                                          Diretor Presidente<br>
                                            Universidade Patativa do Assaré – UPA <br>
                                            CNPJ 05.342.580/0001-19<br>
                                            {{-- <span style="font-size:6pt;">
                                            hash: [{{base64_encode($tce->id)}}/
                                            @if($tce->migracao != NULL)
                                                m{{$tce->migracao}}
                                            @else
                                                id{{$tce->id}}
                                            @endif
                                            ]
                                            </span> --}}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            
                            <br><br><br>
                            <div class="row body-content">
                                <div class="span12">
                                    <p style="margin: 10px; float: right;">Juazeiro do Norte, {{$dtHoje}}</p>
                                </div>
                            </div>
                        
                            <div style="font-size: 6pt;">     
                                @include('tce.relatorio.inc_rodape')
                            </div>
                            
                            <footer class="fixar-rodape">
                                <span style="font-size:6pt;">
                                            hash: [{{base64_encode($tce->id)}}/
                                            @if($tce->migracao != NULL)
                                                m{{$tce->migracao}}
                                            @else
                                                id{{$tce->id}}
                                            @endif
                                            ]
                                </span>
                            </footer>
                        </div>
            </div>
        </div>
</body>
</html>