@extends('layouts.painel2')
@section('title_postfix', ' - '.$titulo)
@section('content')

<div class="header bg-gradient-primary pb-4 pt-5 pt-md-8">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row">
        <div class="col-lg-12">
            <a href="{{URL::previous()}}" class="btn btn-default ad-click-event btn-xs">Voltar</a>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid mt-4">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <div class="card shadow">
        <div class="card-header border-0">
          <div class="row">
            <div class="col-lg-12">
              <h4 class="text-left">{{$tce->instituicao->nmInstituicao}}</h4>
            </div>
          </div>
        </div>
        <div class="table-responsive">
          <table class="table align-items-center table-flush col-12">
            <tbody>
              <tr>
                <td colspan="3"><label><b>Nome Fantasia:</b></label> {{$tce->instituicao->nmFantasia}}</td>
                <td colspan="2"><label><b>CNPJ:</b></label> {{$tce->instituicao->cdCNPJ}}</td>
              </tr>
              <tr>
                <td colspan="3"><label><b>Diretor:</b></label> {{$tce->instituicao->nmDiretor}}</td>
                <td colspan="2"><label><b>Reitor:</b></label> {{$tce->instituicao->nmReitor}}</td>
              </tr>
              <tr>
                <td colspan="2"><label><b>Endereço:</b></label><br>
                  {{$tce->instituicao->dsEndereco}}, N° {{$tce->instituicao->nnNumero}}
                </td>
                <td><label><b>CEP:</b></label><br>
                    {{$tce->instituicao->cdCEP}}
                </td>
                <td><label><b>Bairro:</b></label><br>
                    {{$tce->instituicao->nmBairro}}
                </td>
                <td><label><b>Cidade/UF:</b></label><br>
                    {{$tce->instituicao->cidade->nmCidade}}/{{$tce->instituicao->cidade->estado->cdUF}}
                </td>
              </tr>
              <tr>
                <td><label><b>Telefone:</b></label> {{$tce->instituicao->dsFone}}</td>
                <td><label><b>Outro: </b></label>
                    @if($tce->instituicao->dsOutroFone == NULL)
                    -
                    @else
                    {{$tce->instituicao->dsOutroFone }}
                    @endif
                </td>
                <td colspan="3"><label><b>E-mail: </b></label>
                    @if($tce->instituicao->dsEmail == NULL)
                    -
                    @else
                    {{$tce->instituicao->dsEmail}}
                    @endif
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@stop
