@extends('layouts.painel2')
@section('content')

<!-- Header -->
<div class="header bg-gradient-primary pb-4 pt-5 pt-md-8">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row">
        <div class="col-md-6">
          <h2 class="text-left text-white">
            Supervisores
            <br>
            <small>Faça o controle dos supervisores de estágio</small>
          </h2>
        </div>
        <div class="col-md-6">
          <button type="button" name="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#modal-add-supervisor"><i class="fa fa-plus"></i> Add Supervisor</button>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid mt-4">
  <!-- Card stats -->
  <div class="row">
    <div class="col-md-12">
      <div class="card card-default">
        <div class="card-header">
          <div class="row">
            <div class="col-md-8">
              <h4 class="text-left">Supervisores</h4>
            </div>
            <div class="col-md-4">
              <div class="input-group">
                <input type="search" name="search" value="" class="form-control" placeholder="Buscar por nome,cpf,tce..." onkeyup="busca(this.value)">
                <span class="input-group-append">
                  <span class="input-group-text"><i class="fa fa-search"></i></span>
                </span>
              </div>
            </div>
          </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-striped datatable" id="dtSupervisor">
              <thead>
                <tr>
                  <th style="width:5%;">#</th>
                  <th style="width:25%;">Nome</th>
                  <th style="width:15%;">CPF</th>
                  <th style="width:15%;">E-mail</th>
                  <th style="width:10%;">Criado em</th>
                  <th class="text-center" style="width:15%;">Ações</th>
                </tr>
              </thead>
              <tbody id="body-table-mentores">
                @foreach($supervisors as $supervisor)
                <tr class="objects" id="supervisor_{{$loop->iteration}}" data-id="{{$supervisor->id}}" data-nome="{{$supervisor->name}}" data-email="{{$supervisor->email}}">
                  <td>{{$loop->iteration}}</td>
                  <td>{{$supervisor->nome}}</td>
                  <td>{{$supervisor->cpf}}</td>
                  <td>{{$supervisor->email != null ? $supervisor->email : "Não cadastrado"}}</td>
                  <td>{{$supervisor->created_at->format('d/m/Y')}}</td>
                  <td class="text-center">
                    <a href="{{ route('painel.concedente.supervisor.show', ['id'=>$supervisor->id]) }}" role="button" class="btn btn-primary"><i class="fa fa-eye"></i> Ver</a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- modal Add Supervisor -->
  <div class="modal" tabindex="-1" role="dialog" id="modal-add-supervisor">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Cadastrar Supervisor</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{ route('painel.concedente.supervisor.store')}}" method="post" name="form-add-supervisor">
          <div class="modal-body">
            {!! csrf_field() !!}
            <div class="form-group row">
              <div class="col-md-6">
                <label class="control-label" for="">Nome</label>
                <input type="text" name="nome" class="form-control" value="" required>
              </div>
              <div class="col-md-6">
                <label class="control-label" for="">E-mail</label>
                <input type="email" name="email" class="form-control" value="" required>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-6">
                <label class="control-label" for="">CPF</label>
                <input type="tel" name="cpf" value="" class="form-control" id="cpf" onchange="" required>
              </div>
              <div class="col-md-6">
                <label class="control-label" for="">Telefone</label>
                <input type="text" name="telefone" value="" class="form-control" id="telefone" required>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-12">
                <label class="control-label" for="">Formação</label>
                <input type="text" name="formacao" class="form-control" value="" required>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-12">
                <label class="control-label" for="">Cargo</label>
                <input type="text" name="cargo" class="form-control" value="" required>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            <button type="submit" class="btn btn-success">Cadastrar</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!-- /modal Add Supervisor -->
  <!-- modal Edit Supervisor -->
  <div class="modal" tabindex="-1" role="dialog" id="modal-edit-supervisor">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Excluir Supervisor</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{ route('painel.concedente.supervisor.update')}}" method="post" name="form-edit-supervisor">
          <div class="modal-body">
            {!! csrf_field() !!}
            <h4 class="text-center">Deseja realmente excluir o supervisor?</h4>
            <h6 class="text-center" id="supervisor-exclusao"></h6>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            <button type="submit" class="btn btn-success">Salvar</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!-- /modal Edit Supervisor -->
</div>
@stop
@section('js')
<script type="text/javascript">
$(".datatable").DataTable({
  "searching": false,
  "lengthChange": false,
  "columnDefs": [
    { "orderable": false, "targets": 'no-sort' }
  ],
  "pageLength": 25,
  "language": {
    "sEmptyTable": "Nenhum registro encontrado",
    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
    "sInfoPostFix": "",
    "sInfoThousands": ".",
    "sLengthMenu": "_MENU_ resultados por página",
    "sLoadingRecords": "Carregando...",
    "sProcessing": "Processando...",
    "sZeroRecords": "Nenhum registro encontrado",
    "sSearch": "Pesquisar",
    "oPaginate": {
      "sNext": "Próximo",
      "sPrevious": "Anterior",
      "sFirst": "Primeiro",
      "sLast": "Último"
    },
    "oAria": {
      "sSortAscending": ": Ordenar colunas de forma ascendente",
      "sSortDescending": ": Ordenar colunas de forma descendente"
    }
  }
});

$("#cpf").mask("000.000.000-00");
$("#telefone").mask("(99) 9 9999-9999");

function busca(nome){
  var supervisores = $(".objects");
  filtra(nome,supervisores);
}

function filtra(nome,supervisores){
  supervisores.show();
  if (nome != "") {
    for (var i = 0; i < supervisores.length; i++) {
      if (supervisores[i].dataset.nome.toLowerCase().search(nome.toLowerCase()) && supervisores[i].dataset.email.toLowerCase().search(nome.toLowerCase())) {
        supervisores[i].style.display = "none";
      }
    }
  }else{
    supervisores.show();
  }
}
</script>
@endsection
