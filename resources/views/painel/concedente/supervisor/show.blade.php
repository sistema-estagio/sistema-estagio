@extends('layouts.painel2')
@section('content')

<!-- Header -->
<div class="header bg-gradient-primary pb-4 pt-5 pt-md-8">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row">
        <div class="col-md-6">
          <h2 class="text-left text-white">
            Supervisor: <strong>{{$supervisor->nome}}</strong>
            <br>
            <small>Veja os tce's em que estão sendo supervisionados</small>
          </h2>
        </div>
        <div class="col-md-6">

        </div>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid mt-4">
  <!-- Card stats -->
  <div class="row">
    <div class="col-md-12">
      <div class="card card-default">
        <div class="card-header">
          <div class="row">
            <div class="col-md-4">
              <h4 class="text-left">TCE's</h4>
            </div>
            <div class="col-md-4">
              <div class="input-group">
                <select class="form-control" name="filtro">
                  <option value="0">Todos</option>
                  <option value="1">Ativos</option>
                  <option value="2">Pendentes</option>
                  <option value="3">Cancelados</option>
                </select>
                <span class="input-group-append">
                  <button type="button" role="button" class="btn btn-primary" name="btn-filter"><i class="fas fa-filter"></i> Filtrar</button>
                </span>
              </div>
            </div>
            <div class="col-md-4">
              <div class="input-group">
                <input type="search" name="search" value="" class="form-control" placeholder="Buscar por nome,cpf,tce..." onkeyup="busca(this.value)">
                <span class="input-group-append">
                  <span class="input-group-text"><i class="fa fa-search"></i></span>
                </span>
              </div>
            </div>
          </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-striped datatable" id="dtMediador">
              <thead>
                <tr>
                  <th style="width:5%;">#</th>
                  <th style="width:5%;">N°</th>
                  <th style="width:25%;">Estagiário</th>
                  <th style="width:15%;">CPF</th>
                  <th style="width:10%;">Data Início</th>
                  <th style="width:10%;">Data Fim</th>
                  <th class="text-center" style="width:15%; display:none;">Ações</th>
                </tr>
              </thead>
              <tbody id="body-table-mentores">
                @foreach(App\Models\Supervisor::getTcesAditivos($supervisor) as $tce)
                <tr class="objects" id="tce_{{$loop->iteration}}" data-id="{{$tce->migracao != null ? $tce->migracao : $tce->id}}" data-nome="{{$tce->estudante->nmEstudante}}" data-cpf="{{$tce->estudante->cdCPF}}" @if($tce->dtCancelamento != null) data-tipo="3" @elseif($tce->dt4Via != null) data-tipo="1"  @else data-tipo="2"  @endif>
                  <td>
                    @if($tce->dtCancelamento != null)
                    <span class="btn bg-danger" data-toggle="tooltip" data-placement="top" title="Cancelado"><i class="text-white fa fa-times"></i></span>
                    @elseif($tce->dt4Via != null)
                    <span class="btn bg-success" data-toggle="tooltip" data-placement="top" title="Ativo"><i class="text-white fas fa-check"></i></span>
                    @else
                    <span class="btn bg-primary" data-toggle="tooltip" data-placement="top" title="Pendente"><i class="text-white fas fa-book-open"></i></span>
                    @endif
                  </td>
                  <td>{{$tce->migracao != null ? $tce->migracao : $tce->id}}</td>
                  <td>{{$tce->estudante->nmEstudante}}</td>
                  <td>{{$tce->estudante->cdCPF}}</td>
                  <td>{{$tce->dtInicio->format('d/m/Y')}}</td>
                  <td>{{$tce->dtFim->format('d/m/Y')}}</td>
                  <td class="text-center" style="display:none;">
                    <a href="{{ route('painel.concedente.supervisor.show', ['id'=>$supervisor->id]) }}" role="button" class="btn btn-primary"><i class="fa fa-file"></i> Ver</a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop
@section('js')
<script>
$(".datatable").DataTable({
  "searching": false,
  "lengthChange": false,
  "columnDefs": [
    { "orderable": false, "targets": 'no-sort' }
  ],
  "pageLength": 25,
  "language": {
    "sEmptyTable": "Nenhum registro encontrado",
    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
    "sInfoPostFix": "",
    "sInfoThousands": ".",
    "sLengthMenu": "_MENU_ resultados por página",
    "sLoadingRecords": "Carregando...",
    "sProcessing": "Processando...",
    "sZeroRecords": "Nenhum registro encontrado",
    "sSearch": "Pesquisar",
    "oPaginate": {
      "sNext": "Próximo",
      "sPrevious": "Anterior",
      "sFirst": "Primeiro",
      "sLast": "Último"
    },
    "oAria": {
      "sSortAscending": ": Ordenar colunas de forma ascendente",
      "sSortDescending": ": Ordenar colunas de forma descendente"
    }
  }
});

function busca(nome){
  var tces = $(".objects");
  filtra(nome,mediadores);
}

function filtra(nome,tces){
  tces.show();
  if (nome != "") {
    for (var i = 0; i < mediadores.length; i++) {
      if (tces[i].dataset.nome.toLowerCase().search(nome.toLowerCase()) && tces[i].dataset.email.toLowerCase().search(nome.toLowerCase())) {
        tces[i].style.display = "none";
      }
    }
  }else{
    tces.show();
  }
}

$("button[name=btn-filter]").click(function(){
  var tipo = $('select[name=filtro]').val();
  var tces = $(".objects");
  tces.show();
  if (tipo != "0") {
    for (var i = 0; i < tces.length; i++) {
      if (tces[i].dataset.tipo.toLowerCase().search(tipo.toLowerCase())) {
        tces[i].style.display = "none";
      }
    }
  }else{
    tces.show();
  }
});
</script>
@endsection
