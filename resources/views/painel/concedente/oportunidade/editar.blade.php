@extends('layouts.painel2')
@section('css')
<link rel="stylesheet" href="{{asset('assets/vendor/switch/style.css')}}">
@stop
@section('content')
<!-- Header -->
<div class="header bg-gradient-primary pb-4 pt-5 pt-md-8">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row">
        <div class="col-md-12">
          <h2 class="text-left text-white">
            ABERTURA DE VAGAS DE ESTÁGIO
            <br>
            <small>Gere e controle oportunidades de estágio</small>
          </h2>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid">
  <!-- Card stats -->
  <br>
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <div class="row">
            <div class="col-md-12 text-left">
              Insira os dados da vaga
            </div>
          </div>
        </div>
        <form action="{{route('painel.concedente.oportunidade.store')}}" method="post" name="form-oportunidade-store">
          <div class="card-body">
            <div class="row">
              <div class="col-md-12">
                {!! csrf_field() !!}
                <div class="form-group row">
                  <div class="col-md-3 col-sm-12">
                    <label for="">Nome da vaga</label>
                    <input type="text" name="titulo" value="{{$oportunidade->titulo}}" class="form-control">
                  </div>
                  <div class="col-md-3 col-sm-3">
                    <label for="">Tipo do estágio</label>
                    <select class="form-control" name="tipo">
                      <option value="normal" @if($oportunidade->tipo == 'normal') selected @endif>Não Obrigatório</option>
                      <option value="obrigatorio" @if($oportunidade->tipo == 'obrigatorio') selected @endif>Obrigatório</option>
                    </select>
                  </div>
                  <div class="col-md-1 col-sm-3">
                    <label for="">N° Vagas</label>
                    <input type="tel" name="vagas" value="1" class="form-control">
                  </div>
                  <div class="col-md-2 col-sm-3">
                    <label for="">Carga Horária</label>
                    <input type="tel" name="cargaHorariaSemanal" value="0" class="form-control">
                  </div>
                  <div class="col-md-3 col-sm-3">
                    <label for="">Disponibilizar ao criar a vaga?</label>
                    <br>
                    <input type="hidden" name="status" value="">
                    <button type="button" name="btn-status-sim" class="btn btn-default" onclick="mudarStatus(this)">Sim</button>
                    <button type="button" name="btn-status-nao" class="btn btn-danger" onclick="mudarStatus(this)">Não</button>
                  </div>
                </div>
                <div class="form-group row" style="display:none;">
                  <div class="col-md-12">
                    <label for="">Observações</label>
                    <textarea name="obs" rows="8" cols="80" class="form-control"></textarea>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-3">
                    <label for="">Turno</label>
                    <div class="input-group">
                      <span class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-business-time"></i></span>
                      </span>
                      <select name="turno" value="" class="form-control">
                        <option value="1">Não informar</option>
                        <option value="1">Manhã</option>
                        <option value="2">Tarde</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-2" style="display:none;">
                    <label for="">Entrada</label>
                    <div class="input-group">
                      <span class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-business-time"></i></span>
                      </span>
                      <input type="tel" name="horarioInicio" value="" class="form-control hour">
                    </div>
                  </div>
                  <div class="col-md-2" style="display:none;">
                    <label for="">Saída</label>
                    <div class="input-group">
                      <span class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-business-time"></i></span>
                      </span>
                      <input type="tel" name="horarioFim" value="" class="form-control hour">
                    </div>
                  </div>
                  <div class="col-md-3">
                    <label for="">Valor Bolsa</label>
                    <div class="input-group">
                      <span class="input-group-prepend">
                        <span class="input-group-text">R$</span>
                      </span>
                      <input type="tel" name="bolsa" value="0" class="form-control money">
                      <span class="input-group-append" id="button-addon1">
                        <button type="button" role="button" name="btn-bolsa" class="btn btn-default" value="0" data-toggle="tooltip" data-placement="top" title="Ocultar valor"><i class="fa fa-eye"></i></span>
                        </span>
                      </div>
                      <input type="hidden" name="showBolsa" value="true">
                    </div>
                    <div class="col-md-3">
                      <label for="">Valor Aux. Transp.</label>
                      <div class="input-group">
                        <span class="input-group-prepend">
                          <span class="input-group-text">R$</span>
                        </span>
                        <input type="tel" name="auxTransp" value="0" class="form-control money">
                        <span class="input-group-append" id="button-addon2">
                          <button type="button" role="button" name="btn-transp" class="btn btn-default" value="0" data-toggle="tooltip" data-placement="top" title="Ocultar valor"><i class="fa fa-eye"></i></span>
                          </span>
                        </div>
                        <input type="hidden" name="showTransp" value="true">
                      </div>
                      <div class="col-md-3">
                        <label for="">Valor Aux. Alimentação</label>
                        <div class="input-group">
                          <span class="input-group-prepend">
                            <span class="input-group-text">R$</span>
                          </span>
                          <input type="tel" name="auxAlimentacao" value="0" class="form-control money">
                          <span class="input-group-append" id="button-addon3">
                            <button type="button" role="button" name="btn-alimentacao" class="btn btn-default" value="0" data-toggle="tooltip" data-placement="top" title="Ocultar valor"><i class="fa fa-eye"></i></span>
                            </span>
                          </div>
                          <input type="hidden" name="showAlimentacao" value="true">
                        </div>
                      </div>
                      <hr class="col-11">
                      <div class="accordion" id="accordion-educacional">
                        <div class="card">
                          <div class="col-12 card-header bg-green" id="headingOne">
                            <button class="btn btn-link btn-sm" tooltip="Clique para ver os requisitos" type="button" data-toggle="collapse" data-target="#educacional" aria-expanded="true" aria-controls="collapseOne" data-toggle="tooltip" data-placement="top" title="Expandir lista de requisitos">
                              <h3 class="mb-0 text-white">
                                FORMAÇÕES EDUCACIONAIS
                              </h3>
                            </button>
                          </div>

                          <div id="educacional" class="collapse" aria-labelledby="headingOne" data-parent="#educacional">
                            <div class="card-body">
                              <div class="col-md-12">
                                <div class="row mb-1">
                                  <div class="col-md-4">
                                    <small>Adicione abaixo as formações para a vaga</small>
                                  </div>
                                  <div class="col-md-4">
                                    <div class="input-group">
                                      <input type="search" name="searchEducacional" value="" class="form-control" placeholder="Procurar formação...">
                                      <span class="input-group-prepend">
                                        <span class="input-group-text">
                                          <i class="fa fa-search"></i>
                                        </span>
                                      </span>
                                    </div>
                                  </div>
                                  <div class="col-md-4">
                                    <button type="button" name="button" class="btn bg-green text-white float-right" data-toggle="modal" data-target="#modalAddEducacional"><i class="fa fa-plus"></i> Add Formação</button>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-12">
                                    <table class="table table-striped datatable">
                                      <thead>
                                        <tr>
                                          <th width="5%">Adicionar?</th>
                                          <th width="85%">Curso</th>
                                          <th width="10%">Semestre</th>
                                        </tr>
                                      </thead>
                                      <tbody id="listFormacao">
                                        @foreach($oportunidadeRequisito->where('tipo','educacional') as $requisito)
                                        <tr id="educacional_{{$loop->iteration}}" class="educacional" data-titulo="{{$requisito->requisito->titulo}}" data-descricao="{{$requisito->requisito->descricao}}" data-requisito="{{$requisito->requisito_id}}">
                                          <td>
                                            <label class="switch">
                                              <input type="checkbox" class="inputs-educacional" data-tid="{{$requisito->requisito_id}}" checked>
                                              <span class="slider round"></span>
                                            </label>
                                          </td>
                                          <td>{{$requisito->requisito->titulo}}</td>
                                          <td>
                                            <input type="tel" class="form-control form-control-sm" id="semestre_{{$loop->iteration}}" style="width:19%;" value="1">
                                          </td>
                                        </tr>
                                        @endforeach
                                        @foreach($requisitos->where('tipo','educacional') as $requisito)
                                        <tr id="educacional_{{$loop->iteration}}" class="educacional" data-titulo="{{$requisito->titulo}}" data-descricao="{{$requisito->descricao}}" data-requisito="{{$requisito->id}}">
                                          <td>
                                            <label class="switch">
                                              <input type="checkbox" class="inputs-educacional" data-tid="{{$requisito->id}}">
                                              <span class="slider round"></span>
                                            </label>
                                          </td>
                                          <td>{{$requisito->titulo}}</td>
                                          <td>
                                            <input type="tel" class="form-control form-control-sm" id="semestre_{{$loop->iteration}}" style="width:19%;" value="1">
                                          </td>
                                        </tr>
                                        @endforeach
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <br>
                      <div class="accordion" id="accordionExample-1">
                        <div class="card">
                          <div class="col-12 card-header bg-blue" id="headingOne">
                            <button class="btn btn-link btn-sm" tooltip="Clique para ver os requisitos" type="button" data-toggle="collapse" data-target="#collapseOne-1" aria-expanded="true" aria-controls="collapseOne" data-toggle="tooltip" data-placement="top" title="Expandir lista de requisitos">
                              <h3 class="mb-0 text-white">
                                REQUISITOS
                              </h3>
                            </button>
                          </div>

                          <div id="collapseOne-1" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample-1">
                            <div class="card-body">
                              <div class="col-md-12">
                                <div class="row mb-1">
                                  <div class="col-md-4">
                                    <small>Adicione abaixo os requisitos</small>
                                  </div>
                                  <div class="col-md-4">
                                    <div class="input-group">
                                      <input type="search" name="searchRequisito" value="" class="form-control" placeholder="Procurar requisito...">
                                      <span class="input-group-prepend">
                                        <span class="input-group-text">
                                          <i class="fa fa-search"></i>
                                        </span>
                                      </span>
                                    </div>
                                  </div>
                                  <div class="col-md-4">
                                    <button type="button" name="button" class="btn bg-blue float-right text-white" data-toggle="modal" data-target="#modalAddRequisito"><i class="fa fa-plus"></i> Add Requisito</button>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-12">
                                    <table class="table table-striped datatable">
                                      <thead>
                                        <tr>
                                          <th width="5%">Adicionar?</th>
                                          <th width="40%">Título</th>
                                          <th width="40%">Descrição</th>
                                          <th class="text-center" width="10%">Obrigatório</th>
                                        </tr>
                                      </thead>
                                      <tbody id="listRequisitos">
                                        @foreach($oportunidadeRequisito->where('tipo', 'outro') as $requisito)
                                        <tr id="requisito_{{$requisito->id}}" class="requisitos" data-titulo="{{$requisito->titulo}}" data-descricao="{{$requisito->descricao}}" data-requisito="{{$requisito->id}}">
                                          <td>
                                            <label class="switch">
                                              <input type="checkbox" class="inputs-requisito" data-tid="{{$requisito->id}}">
                                              <span class="slider round"></span>
                                            </label>
                                          </td>
                                          <td>{{$requisito->requisito->titulo}}</td>
                                          <td>{{$requisito->requisito->descricao != null ? $requisito->requisito->descricao : '------'}}</td>
                                          <td>
                                            <select class="form-control" id="is_Obrigatorio_{{$loop->iteration}}" value="">
                                              <option value="true">Sim</option>
                                              <option value="false">Não</option>
                                            </select>
                                          </td>
                                        </tr>
                                        @endforeach
                                        @foreach($requisitos->where('tipo', 'outro') as $requisito)
                                        <tr id="requisito_{{$loop->iteration}}" class="requisitos" data-titulo="{{$requisito->titulo}}" data-descricao="{{$requisito->descricao}}" data-requisito="{{$requisito->id}}">
                                          <td>
                                            <label class="switch">
                                              <input type="checkbox" class="inputs-requisito" data-tid="{{$requisito->id}}">
                                              <span class="slider round"></span>
                                            </label>
                                          </td>
                                          <td>{{$requisito->titulo}}</td>
                                          <td>{{$requisito->descricao != null ? $requisito->descricao : '------'}}</td>
                                          <td>
                                            <select class="form-control" id="is_Obrigatorio_{{$loop->iteration}}" value="">
                                              <option value="true">Sim</option>
                                              <option value="false">Não</option>
                                            </select>
                                          </td>
                                        </tr>
                                        @endforeach
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <br>
                      <div class="accordion" id="accordionExample-2">
                        <div class="card">
                          <div class="col-12 card-header bg-info" id="headingOne">
                            <button class="btn btn-link btn-sm" tooltip="Clique para ver os requisitos" type="button" data-toggle="collapse" data-target="#collapseOne-2" aria-expanded="true" aria-controls="collapseOne" data-toggle="tooltip" data-placement="top" title="Expandir lista de atividades">
                              <h3 class="mb-0 text-white">
                                ATIVIDADES
                              </h3>
                            </button>
                          </div>

                          <div id="collapseOne-2" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample-2">
                            <div class="card-body">
                              <div class="col-md-12">
                                <div class="row mb-1">
                                  <div class="col-md-4">
                                    <small>Adicione abaixo as atividades</small>
                                  </div>
                                  <div class="col-md-4">
                                    <div class="input-group">
                                      <input type="search" name="searchAtividade" value="" class="form-control" placeholder="Procurar atividade...">
                                      <span class="input-group-prepend">
                                        <span class="input-group-text">
                                          <i class="fa fa-search"></i>
                                        </span>
                                      </span>
                                    </div>
                                  </div>
                                  <div class="col-md-4">
                                    <button type="button" name="button" class="btn bg-info text-white float-right" data-toggle="modal" data-target="#modalAddAtividade"><i class="fa fa-plus"></i> Add Atividade</button>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-12">
                                    <table class="table table-striped datatable">
                                      <thead>
                                        <tr>
                                          <th width="5%">Adicionar?</th>
                                          <th width="95%">Título</th>
                                        </tr>
                                      </thead>
                                      <tbody id="listAtividades">
                                        @foreach($oportunidadeTarefa as $tarefa)
                                        <tr id="atividade_{{$loop->iteration}}" class="atividades" data-descricao="{{$tarefa->descricao}}" data-atividade="{{$tarefa->id}}">
                                          <td>
                                            <label class="switch">
                                              <input type="checkbox" class="inputs-atividade" data-tid="{{$tarefa->id}}" checked>
                                              <span class="slider round"></span>
                                            </label>
                                          </td>
                                          <td>{{$tarefa->descricao}}</td>
                                        </tr>
                                        @endforeach
                                        @foreach($tarefas as $tarefa)
                                        <tr id="atividade_{{$loop->iteration}}" class="atividades" data-descricao="{{$tarefa->descricao}}" data-atividade="{{$tarefa->id}}">
                                          <td>
                                            <label class="switch">
                                              <input type="checkbox" class="inputs-atividade" data-tid="{{$tarefa->id}}">
                                              <span class="slider round"></span>
                                            </label>
                                          </td>
                                          <td>{{$tarefa->descricao}}</td>
                                        </tr>
                                        @endforeach
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card-footer">
                  <div class="row">
                    <div class="col-md-12">
                      <button type="submit" name="button" class="btn btn-success float-right"><i class="fa fa-check"></i> Cadastrar</button>
                    </div>
                  </div>
                </div>
                <div>
                  <!-- <input type="hidden" name="atividades[]" id="atividades">
                  <input type="hidden" name="requisitos[]" id="requisitos">
                  <input type="hidden" name="obrigatorios[]" id="obrigatório"> -->
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>

      <div class="modal" tabindex="-1" id="modalAddEducacional" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Nova Formação Educacional</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form action="{{ route('painel.concedente.requisito.store') }}" method="post" name="add-post-formacao">
              <div class="modal-body">
                <div class="form-group row">
                  <div class="col-md-12">
                    <label for="">Curso</label>
                    <input type="text" name="titulo" value="" class="form-control" required>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                <button type="submit" class="btn btn-primary">Cadastrar</button>
              </div>
            </form>
          </div>
        </div>
      </div>

      <div class="modal" tabindex="-1" id="modalAddRequisito" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Novo Requisito</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form action="{{ route('painel.concedente.requisito.store') }}" method="post" name="add-post-requisito">
              <div class="modal-body">
                <div class="form-group row">
                  <div class="col-md-12">
                    <label for="">O requisito é obrigatório?</label>
                    <select class="form-control" name="is_Obrigatorio" value="true">
                      <option value="true">Sim</option>
                      <option value="false">Não</option>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label for="">Título</label>
                    <input type="text" name="titulo" value="" class="form-control" required>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label for="">Descrição</label>
                    <textarea name="descricao" value="" class="form-control" rows="4"></textarea>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                <button type="submit" class="btn btn-primary">Cadastrar</button>
              </div>
            </form>
          </div>
        </div>
      </div>

      <div class="modal" tabindex="-1" id="modalAddAtividade" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Novo Atividade</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form action="{{ route('painel.concedente.atividade.store') }}" method="post" name="add-post-atividade">
              <div class="modal-body">
                <div class="form-group row">
                  <div class="col-md-12">
                    <label for="">Descrição</label>
                    <textarea name="descricao" value="" class="form-control" rows="4" required></textarea>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                <button type="submit" class="btn btn-primary">Cadastrar</button>
              </div>
            </form>
          </div>
        </div>
      </div>
      @stop
      @section('js')
      <script type="text/javascript">

      $('.money').mask("###0.00", {reverse: true});
      $('.hour').mask("00:00");

      $('.datatable').DataTable({
        "bFilter" : false,
        "bLengthChange": false,
        "ordering": false,
        "info":     false,
        "language": {
          "sEmptyTable": "Nenhum registro encontrado",
          "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
          "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
          "sInfoFiltered": "(Filtrados de _MAX_ registros)",
          "sInfoPostFix": "",
          "sInfoThousands": ".",
          "sLengthMenu": "_MENU_ resultados por página",
          "sLoadingRecords": "Carregando...",
          "sProcessing": "Processando...",
          "sZeroRecords": "Nenhum registro encontrado",
          "sSearch": "Pesquisar",
          "oPaginate": {
            "sNext": "Próximo",
            "sPrevious": "Anterior",
            "sFirst": "Primeiro",
            "sLast": "Último"
          },
          "oAria": {
            "sSortAscending": ": Ordenar colunas de forma ascendente",
            "sSortDescending": ": Ordenar colunas de forma descendente"
          },
        }
      });
      //

      $("form[name=add-post-requisito]").submit(function(){
        if($(this).find('input[name=titulo]').val() != null){
          $.post($(this).prop('action'),{ 'tipo': 'outro', 'titulo': $(this).find('input[name=titulo]').val(), 'descricao': $(this).find('textarea[name=descricao]').val(), '_token' : '{{ csrf_token() }}' },function(response){
            if(response != null){
              if(response.status == 'success'){
                $("#listRequisitos").prepend('<tr id="'+($('.requisitos').length+1)+'" class="requisitos" data-titulo="'+response.requisito.titulo+'" data-descricao="'+response.requisito.descricao+'" data-requisito="'+response.requisito.id+'"><td><label class="switch"><input type="checkbox" class="inputs-requisito" data-tid="'+($('.requisitos').length+1)+'" checked><span class="slider round"></span></label></td><td>'+response.requisito.titulo+'</td><td>'+response.requisito.descricao+'</td><td><select class="form-control" id="is_Obrigatorio_'+($('.requisitos').length+1)+'" value="'+($("form[name=add-post-requisito]").find('select[name=is_Obrigatorio]').val())+'"><option value="true">Sim</option><option value="false">Não</option></select></td></tr>');
                toastr.success('Requisito cadastrado com sucesso');
                $('#modalAddRequisito').modal('hide');
              }else if(response.status == 'fail'){
                toastr.warning(response.msg);
              }else{
                toastr.error('Não foi possível cadastrar o requisito');
              }
            }else{
              toastr.error('Não foi possível cadastrar o requisito');
            }
          });
        }
        return false;
      });


      $("form[name=add-post-atividade]").submit(function(){
        if($(this).find('textarea[name=descricao]').val() != null){
          $.post($(this).prop('action'),{ 'descricao': $(this).find('textarea[name=descricao]').val(), '_token' : '{{ csrf_token() }}' },function(response){
            if(response != null){
              if(response.status == 'success'){
                $("#listAtividades").prepend('<tr id="'+($('.atividades').length+1)+'" class="atividades" data-descricao="'+response.tarefa.descricao+'" data-atividade="'+response.tarefa.id+'"><td><label class="switch"><input type="checkbox" class="inputs-atividade" data-tid="'+($('.atividades').length+1)+'" checked><span class="slider round"></span></label></td><td>'+response.tarefa.descricao+'</td></tr>');
                toastr.success('Atividade cadastrada com sucesso');
                $('#modalAddAtividade').modal('hide');
              }else if(response.status == 'fail'){
                toastr.warning(response.msg);
              }else{
                toastr.error('Não foi possível cadastrar o tarefa');
              }
            }else{
              toastr.error('Não foi possível cadastrar o tarefa');
            }
          });
        }
        return false;
      });

      $("form[name=add-post-formacao]").submit(function(){
        if($(this).find('input[name=titulo]').val() != null){
          $.post($(this).prop('action'),{ 'tipo': 'educacional', 'titulo': $(this).find('input[name=titulo]').val(), 'descricao': null, '_token' : '{{ csrf_token() }}' },function(response){
            if(response != null){
              if(response.status == 'success'){
                $("#listFormacao").prepend('<tr id="educacional_'+($('.educacional').length+1)+'" class="educacional" data-descricao="'+response.requisito.descricao+'" data-="'+response.requisito.id+'"><td><label class="switch"><input type="checkbox" class="inputs-atividade" data-tid="'+($('.educacional').length+1)+'" checked><span class="slider round"></span></label></td><td>'+response.requisito.titulo+'</td><td><input type="tel" class="form-control form-control-sm" id="semestre_'+($(".educacional").length + 1)+'" style="width:19%;" value="1"></td></tr>');
                toastr.success('Formação cadastrada com sucesso');
                $('#modalAddEducacional').modal('hide');
              }else if(response.status == 'fail'){
                toastr.warning(response.msg);
              }else{
                toastr.error('Não foi possível cadastrar o formação');
              }
            }else{
              toastr.error('Não foi possível cadastrar o formação');
            }
          });
        }
        return false;
      });


      $("input[name=searchRequisito]").keyup(function() {
        var nothing = $("#nothing");
        var titulo = $(this).val().toLowerCase();
        var requisitos = $(".requisitos");
        requisitos.show();
        if (titulo != "") {
          for (var i = 0; i < requisitos.length; i++) {
            if (requisitos[i].dataset.titulo.toLowerCase().search(titulo)) {
              requisitos[i].style.display = "none";
            }else{
              count++;
            }
          }
        }
      });

      $("input[name=searchAtividade]").keyup(function() {
        var nothing = $("#nothing");
        var descricao = $(this).val().toLowerCase();
        var atividades = $(".atividades");
        atividades.show();
        if (descricao != "") {
          for (var i = 0; i < atividades.length; i++) {
            if (atividades[i].dataset.descricao.toLowerCase().search(descricao)) {
              atividades[i].style.display = "none";
            }else{
              count++;
            }
          }
        }
      });

      function mudarStatus(btn){
        if(btn.getAttribute('name') == "btn-status-sim"){
          btn.classList.remove('btn-default');
          btn.classList.add('btn-success');
          $('input[name=status]').val('disponivel');
          $('button[name=btn-status-nao]').removeClass('btn-danger').addClass('btn-default');
        }else{
          btn.classList.remove('btn-default');
          btn.classList.add('btn-danger');
          $('input[name=status]').val('oculta');
          $('button[name=btn-status-sim]').removeClass('btn-success').addClass('btn-default');
        }
      }

      $("form[name=form-oportunidade-store]").submit(function(){
        var educacional = $(".inputs-educacional");
        var requisitos = $(".inputs-requisito");
        var atividades = $(".inputs-atividade");

        for (let i = 0; i < educacional.length; i++) {
          if (educacional[i].checked == true) {
            $(this).append('<input type="hidden" name="educacional[]" value="'+ educacional[i].dataset.tid +'">');
          }
        }

        for (let i = 0; i < requisitos.length; i++) {
          if (requisitos[i].checked == true) {
            $(this).append('<input type="hidden" name="requisitos[]" value="'+ requisitos[i].dataset.tid +'">');
          }
        }

        for (let i = 0; i < atividades.length; i++) {
          if (requisitos[i].checked == true) {
            $(this).append('<input type="hidden" name="atividades[]" value="'+ atividades[i].dataset.tid +'">');
          }
        }

        if(educacional.length == 0){
          var r = confirm("Tem certeza que deseja cadastrar a vaga sem formações?");
          if(r == false){
            return false;
          }
        }

        if(atividades.length == 0){
          var r = confirm("Tem certeza que deseja cadastrar a vaga sem atividades?");
          if(r == false){
            return false;
          }
        }

        if(requisitos.length == 0){
          var r = confirm("Tem certeza que deseja cadastrar a vaga sem requisitos?");
          if(r == false){
            return false;
          }
        }

      });


      $('button[name=btn-bolsa]').click(function(){
        console.log($('input[name=showBolsa]').val());
        if($(this).val() == 0){
          $(this).removeClass('btn-default').addClass('btn-danger');
          $(this).html('<i class="fa fa-eye-slash"></i>');
          $(this).attr('title', 'Mostrar valor');
          $('input[name=showBolsa]').val(false);
          $(this).val(1);
        }else{
          $(this).removeClass('btn-danger').addClass('btn-default');
          $(this).html('<i class="fa fa-eye"></i>');
          $(this).attr('title', 'Ocultar valor');
          $('input[name=showBolsa]').val(true);
          $(this).val(0);
        }
      });

      $('button[name=btn-transp]').click(function(){
        if($(this).val() == 0){
          $(this).removeClass('btn-default').addClass('btn-danger');
          $(this).html('<i class="fa fa-eye-slash"></i>');
          $(this).attr('title', 'Mostrar valor');
          $('input[name=showTransp]').val(false);
          $(this).val(1);
        }else{
          $(this).removeClass('btn-danger').addClass('btn-default');
          $(this).html('<i class="fa fa-eye"></i>');
          $(this).attr('title', 'Ocultar valor');
          $('input[name=showTransp]').val(true);
          $(this).val(0);
        }
      });

      $('button[name=btn-alimentacao]').click(function(){
        if($(this).val() == 0){
          $(this).removeClass('btn-default').addClass('btn-danger');
          $(this).html('<i class="fa fa-eye-slash"></i>');
          $(this).attr('title', 'Mostrar valor');
          $('input[name=showAlimentacao]').val(false);
          $(this).val(1);
        }else{
          $(this).removeClass('btn-danger').addClass('btn-default');
          $(this).html('<i class="fa fa-eye"></i>');
          $(this).attr('title', 'Ocultar valor');
          $('input[name=showAlimentacao]').val(true);
          $(this).val(0);
        }
      });

      </script>

      @endsection
