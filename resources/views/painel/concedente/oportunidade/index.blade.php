@extends('layouts.painel2')
@section('content')

<!-- Header -->
<div class="header bg-gradient-primary pb-4 pt-5 pt-md-8">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row">
        <div class="col-md-6">
          <h2 class="text-left text-white">
            Vagas
            <br>
            <small>Gere e controle oportunidades de estágio</small>
          </h2>
        </div>
        <div class="col-md-6">
          <a href="{{route('painel.concedente.oportunidade.criar')}}" class="btn btn-primary float-right"><i class="fa fa-plus"></i> Add Vaga</a>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid mt-4">
  <!-- Card stats -->
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <div class="row">
            <div class="col-md-4">
              Vagas
            </div>
            <div class="col-md-4">

            </div>
            <div class="col-md-4">

            </div>
          </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-striped" id="dt-vagas">
              <thead>
                <tr>
                  <th style="width:5%;">#</th>
                  <th style="width:5%;">Código</th>
                  <th style="width:20%;">Título</th>
                  <th style="width:10%;">Possui algum aux.?</th>
                  <th style="width:10%;">Carga Horária</th>
                  <th style="width:10%;">Vagas</th>
                  <th style="width:10%;">Criada em</th>
                  <th style="width:10%;">Criada por</th>
                  <th class="text-center" style="width:15%;">Ações</th>
                </tr>
              </thead>
              <tbody>
                @foreach($oportunidades as $oportunidade)
                <tr>
                  <td>{{$loop->iteration}}</td>
                  <td><a href="javascript:void(0);" role="button" onclick="copyText('{{$loop->iteration}}')" class="badge badge-secondary text-black" value="{{$oportunidade->code}}" data-toggle="tooltip" data-placement="left" title="Clique para copiar número da vaga" style="color:black;">{{$oportunidade->code}}</a></td>
                  <input type="hidden" id="code-{{$loop->iteration}}" value="{{$oportunidade->code}}">
                  <td>{{$oportunidade->titulo}}</td>
                  <td class="text-center">
                    @if($oportunidade->bolsa != null || $oportunidade->auxTransp != null || $oportunidade->auxAlimentacao != null)
                    Sim
                    @else
                    Não
                    @endif
                  </td>
                  <td>{{$oportunidade->cargaHorariaSemanal}}</td>
                  <td>{{$oportunidade->vagas}}</td>
                  <td>{{$oportunidade->created_at->format('d/m/Y')}}</td>
                  <td>{{\App\Models\UserConcedente::find($oportunidade->user_id)->name}}</td>
                  <td class="text-center">
                    <a href="{{route('painel.concedente.oportunidade.editar', ['code'=>$oportunidade->code])}}" name="button" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i> Editar</a>
                    <form id="form-delete-item" action="{{ route('painel.concedente.oportunidade.delete', ['code'=>$oportunidade->code]) }}" method="post" style="display: inline-block">
                      {!! csrf_field() !!}
                      <input type="hidden" name="_method" value="DELETE">
                      <input type="hidden" name="id" value="{{$oportunidade->id}}">
                      <button type="submit" role="button" class="btn btn-danger btn-sm" onclick="return confirm('Deseja realmente excluir esta oportunidade?');"><i class="fa fa-trash-o"></i>Excluir</button>
                    </form>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop
@section('js')
<script type="text/javascript">
$('#dt-vagas').DataTable({
  "bLengthChange": false,
  "ordering": false,
  "info":     false,
  "language": {
    "sEmptyTable": "Nenhum registro encontrado",
    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
    "sInfoPostFix": "",
    "sInfoThousands": ".",
    "sLengthMenu": "_MENU_ resultados por página",
    "sLoadingRecords": "Carregando...",
    "sProcessing": "Processando...",
    "sZeroRecords": "Nenhum registro encontrado",
    "sSearch": "Pesquisar",
    "oPaginate": {
      "sNext": "Próximo",
      "sPrevious": "Anterior",
      "sFirst": "Primeiro",
      "sLast": "Último"
    },
    "oAria": {
      "sSortAscending": ": Ordenar colunas de forma ascendente",
      "sSortDescending": ": Ordenar colunas de forma descendente"
    },
  }
});
@if(Session::get('success'))
  toastr.success('{{Session::get("success")}}');
@endif
@if(Session::get('error'))
  toastr.error('{{Session::get("error")}}');
@endif
@if(Session::get('fail'))
  toastr.warning('{{Session::get("fail")}}');
@endif
function copyText(code){
  /* Select the text field */
  /* Get the text field */
  var copyText = document.getElementById("code-"+code);

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("copy");
  toastr.info('Código copiado: '+copyText.value);
}
</script>
@endsection
