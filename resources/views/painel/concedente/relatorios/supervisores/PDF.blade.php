<!DOCTYPE html>
<html>
<head>
    <title>Relatório Tce Cancelados Por Concedente</title>
    <!-- Bootstrap 4-ONLINE -->
    <style>
        html { margin: 10px}
        
        body {
            font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial;
        }
        
        footer.fixar-rodape {
            bottom: 0;
            left: 0;
            height: 40px;
            position: fixed;
            width: 100%;
        }
        
        .novaPagina {
            page-break-before: always;
            page-break-after: always
        }
        
        div.body-content {
            /** Essa margem vai evitar que o conteudo fique por baixo do rodapé **/
            margin-bottom: 40px;
        }
        
        .col-12 {
            width: 100% !important;
        }
        
        .col-6 {
            width: 50% !important;
        }
        
        .col-6,
        .col-12 {
            float: left !important;
        }
        
        .table {
            border-collapse: separate;
            border-spacing: 0;
            min-width: 350px;
            margin-bottom: 10px;
        }
        
        .table tr th,
        .table tr td {
            border-right: 1px solid #bbb;
            border-bottom: 1px solid #bbb;
            padding: 5px;
        }
        
        .table th {
            border-top: solid 1px #bbb;
        }
        
        .table tr th:first-child,
        .table tr th:last-child {
            border-top: solid 1px #bbb;
        }
        
        .table tr th:first-child,
        .table tr td:first-child {
            border-left: 1px solid #bbb;
        }
        
        .table tr th:first-child,
        .table tr td:first-child {
            border-left: 1px solid #bbb;
        }
        
        .table tr th {
            background: #eee;
            text-align: left;
        }
        
        .table.Info tr th,
        .table.Info tr:first-child td {
            border-top: 1px solid #bbb;
        }
        /* top-left border-radius */
        
        .table tr:first-child th:first-child,
        .table.Info tr:first-child td:first-child {
            border-top-left-radius: 6px;
        }
        /* top-right border-radius */
        
        .table tr:first-child th:last-child,
        .table.Info tr:first-child td:last-child {
            border-top-right-radius: 6px;
        }
        /* bottom-left border-radius */
        
        .table tr:last-child td:first-child {
            border-bottom-left-radius: 6px;
        }
        /* bottom-right border-radius */
        
        .table tr:last-child td:last-child {
            border-bottom-right-radius: 6px;
        }
    </style>
</head>
<body>
    <div id="body">
        <div id="section_header">
    </div>
        
    <div id="content">
        
        <div class="page" style="font-size: 6pt">
            <table style="width: 100%;" class="header">
                <tr>
                    <td>
                        <?php $image_path = "{{asset('img/relatorios/logo_default.png')}}"; ?>
                        {{-- <img src="{{ public_path() . $image_path }}" alt="Upa - Estagio" width="170px" height="80px"/> --}}
                        <img src="{{ $image_path }}" alt="Upa - Estagio" width="170px" height="80px"/>
                        
                    </td>
                    <td>
                        <h1 style="text-align: right">
                            <strong>RELATÓRIO DE SUPERVISORES<br>{{mb_strtoupper($concedente->nmRazaoSocial)}}</strong>
                        </h1>
                    </td>
                </tr>
            </table>
            <hr style=" margin-bottom: 0px; border-bottom: 1px solid #DCDCDC ;">
            <br>
            
            @if($tipo == 'completo')
                @php $totalSupervisores = 0; @endphp
            
                @foreach ($supervisores as $supervisor)
                    <h4>{{$supervisor->nome}}</h4>
                    <table class="table table-bordered" style="width: 100%;">
                        <thead>
                            <tr>
                                <th>TCE</th>
                                <th>Estudante</th>
                                <th>CPF</th>
                                <th>Data Inicial</th>
                                <th>Data final</th>
                            </tr>
                        </thead>
                        <tbody>
                        @forelse (\App\Models\Supervisor::getTcesAditivos($supervisor) as $tce)    
                            <tr>
                                <td>{{$tce->aditivos()->count() > 0 ? $tce->id.'/'.$tce->aditivos()->count() : $tce->id}}</td>
                                <td>{{$tce->estudante->nmEstudante}}</td>
                                <td>{{$tce->estudante->cdCPF}}</td>
                                <td>{{\Carbon\Carbon::parse($tce->dtInicio)->format('d/m/Y')}}</td>
                                <td>{{\Carbon\Carbon::parse($tce->dtFim)->format('d/m/Y')}}</td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="5">Nenhum TCE a ser supervisionado</td>
                            </tr>    
                        @endforelse
                        </tbody>
                    </table>
                @php $totalSupervisores++; @endphp
                @endforeach
    
            @else
                <!-- grupamento po Estado -->
                <table class="table table-bordered " style="width: 100%;">
                    <thead>
                        <tr>
                            <th>NOME</th>
                            <th>CPF</th>
                            <th>CARGO</th>
                            <th>FORMAÇÃO</th>
                            <th>TELEFONE</th>
                            <th>TCES SUPERVISIONADOS</th>
                        </tr>
                    </thead>
                    <tbody>
                    @php $totalSupervisores = 0; @endphp
                    @foreach ($supervisores as $key => $supervisor)
                        <tr>
                            <td>{{$supervisor->nome}}</td>
                            <td>{{$supervisor->cpf}}</td>
                            <td>{{$supervisor->cargo}}</td>
                            <td>{{$supervisor->formacao}}</td>
                            <td>{{$supervisor->telefone}}</td>
                            <td>{{$supervTces[$key]->count()}}</td>
                        </tr>
                        @php $totalSupervisores++; @endphp
                        @endforeach
                    </tbody>     
                </table>
            @endif
            <div style="margin-top:-10px;">Total de Resultados: {{$totalSupervisores}}</div>
            @include('relatorios.inc_rodape_html')
        </div>
    </div>
</body>
</html>