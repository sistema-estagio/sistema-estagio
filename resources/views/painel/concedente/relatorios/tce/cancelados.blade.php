<!DOCTYPE html>
<html>
<head>
  <title>Relatório Tce Cancelados Por Concedente</title>
  <!-- Bootstrap 4-ONLINE -->
  <style>
  html { margin: 10px}

  body {
    font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial;
  }

  footer.fixar-rodape {
    bottom: 0;
    left: 0;
    height: 40px;
    position: fixed;
    width: 100%;
  }

  .novaPagina {
    page-break-before: always;
    page-break-after: always
  }

  div.body-content {
    /** Essa margem vai evitar que o conteudo fique por baixo do rodapé **/
    margin-bottom: 40px;
  }

  .col-12 {
    width: 100% !important;
  }

  .col-6 {
    width: 50% !important;
  }

  .col-6,
  .col-12 {
    float: left !important;
  }

  .table {
    border-collapse: separate;
    border-spacing: 0;
    min-width: 350px;
    margin-bottom: 10px;
  }

  .table tr th,
  .table tr td {
    border-right: 1px solid #bbb;
    border-bottom: 1px solid #bbb;
    padding: 5px;
  }

  .table th {
    border-top: solid 1px #bbb;
  }

  .table tr th:first-child,
  .table tr th:last-child {
    border-top: solid 1px #bbb;
  }

  .table tr th:first-child,
  .table tr td:first-child {
    border-left: 1px solid #bbb;
  }

  .table tr th:first-child,
  .table tr td:first-child {
    border-left: 1px solid #bbb;
  }

  .table tr th {
    background: #eee;
    text-align: left;
  }

  .table.Info tr th,
  .table.Info tr:first-child td {
    border-top: 1px solid #bbb;
  }
  /* top-left border-radius */

  .table tr:first-child th:first-child,
  .table.Info tr:first-child td:first-child {
    border-top-left-radius: 6px;
  }
  /* top-right border-radius */

  .table tr:first-child th:last-child,
  .table.Info tr:first-child td:last-child {
    border-top-right-radius: 6px;
  }
  /* bottom-left border-radius */

  .table tr:last-child td:first-child {
    border-bottom-left-radius: 6px;
  }
  /* bottom-right border-radius */

  .table tr:last-child td:last-child {
    border-bottom-right-radius: 6px;
  }
  </style>
</head>
<body>
  <div id="body">
    <div id="section_header">
    </div>

    <div id="content">

      <div class="page" style="font-size: 6pt">
        <table style="width: 100%;" class="header">
          <tr>
            <td>
              <?php $image_path = 'http://teste.universidadepatativa.com.br/sysEstagio/img/relatorios/logo_default.png'; ?>
              {{-- <img src="{{ public_path() . $image_path }}" alt="Upa - Estagio" width="170px" height="80px"/> --}}
              <img src="{{ $image_path }}" alt="Upa - Estagio" width="170px" height="80px"/>

            </td>
            <td>
              <h1 style="text-align: right">
                <strong>TCEs CANCELADOS<br>{{mb_strtoupper($concedente->nmRazaoSocial)}}</strong>({{$concedente->id}})
                @if($secretaria)
                <br><strong>{{mb_strtoupper($secretaria->nmSecretaria)}}</strong>
                @endif
              </h1>
            </td>
          </tr>
        </table>
        <hr style=" margin-bottom: 0px; border-bottom: 1px solid #DCDCDC ;">
        <br>
        @if($tipo == 'completo')
        <table class="table table-bordered " style="width: 100%;">
          <tr>
            <th>TCE</th>
            <th>ESTUDANTE</th>
            <th width="65">CPF</th>
            <th width="40">DIAS TRABALHADOS</th>
            <th width="40">DIAS RECESSO</th>
            <th width="40">FALTAS JUST.</th>
            <th width="40">FALTAS NÃO JUST.</th>
            <th width="40">PAGAMENTOS REALIZADOS</th>
            <th width="20">POSSUI ACESSO</th>
            <th>INICIO</th>
            <th>FIM</th>
            <th>CANCELAMENTO</th>
            <th>USUÁRIO</th>
            <th>MOTIVO</th>
          </tr>
          @php $totalTCE = 0; @endphp

          @forelse($tces as $tce)
          <tr>
            @if($tce->migracao != null)
            <td>{{$tce->migracao}}</td>
            @else
            <td>{{$tce->id}}</td>
            @endif
            <td>{{$tce->estudante->nmEstudante}}</td>
            <td>{{$tce->estudante->cdCPF}}</td>
            <td stlye="text-align:center;">{{\App\Models\FolhaPagamentoItem::where('tce_id',$tce->id)->sum('diasTrabalhados')}}</td>
            <td stlye="text-align:center;">{{\App\Models\FolhaPagamentoItem::where('tce_id',$tce->id)->sum('diasRecesso')}}</td>
            <td stlye="text-align:center;">{{\App\Models\FolhaPagamentoItem::where('tce_id',$tce->id)->sum('justificadas')}}</td>
            <td stlye="text-align:center;">{{\App\Models\FolhaPagamentoItem::where('tce_id',$tce->id)->sum('faltas')}}</td>
            <td stlye="text-align:center;">{{\App\Models\FolhaPagamentoItem::where('tce_id',$tce->id)->count()}}</td>
            <td stlye="text-align:center;">{{$tce->estudante->UserEstudante != null ? 'Sim' : 'Não' }}</td>
            <td>{{$tce->dtInicio->format('d/m/Y')}}</td>
            <td>
              @if($tce->aditivos->count() > 0)
              @if($tce->aditivos()->latest()->first()->dtInicio != Null)
              {{date('d/m/Y',strtotime($tce->aditivos()->latest()->first()->dtFim))}} ADT
              @else
              {{date('d/m/Y',strtotime($tce->dtFim))}} TCE
              @endif
              @else
              {{date('d/m/Y',strtotime($tce->dtFim))}} TCE
              @endif
            </td>
            <td>{{$tce->dtCancelamento->format('d/m/Y')}}</td>
            <td>
              @if(isset($tce->userCancelamento->name))
              {{$tce->userCancelamento->name}}
              @else Propia Concedente
              @endif
            </td>
            {{-- <td>{{$tce->motivoCancelamento_id}}</td>--}}
            <td>
              @if($tce->MotivoCancelamento != NULL)
              {{$tce->MotivoCancelamento->dsMotivoCancelamento}}
              @endif
            </td>
          </tr>
          @php $totalTCE++; @endphp
          @empty
          <tr>
            <td colspan="7" style="text-align:center">Nenhum TCE foi cancelado neste período</td>
          </tr>
          @endforelse
        </table>
        @else
        <!-- grupamento po Estado -->
        <table class="table table-bordered " style="width: 100%;">
          <tr>
            <th>TCE</th>
            <th>ESTUDANTE</th>
            <th>INICIO</th>
            <th>FIM</th>
            <th>CANCELAMENTO</th>
            <th>USUÁRIO</th>
            <th>MOTIVO</th>
          </tr>
          @php $totalTCE = 0; @endphp

          @forelse($tces as $tce)
          <tr>
            @if($tce->migracao != null)
            <td>{{$tce->aditivos->count() > 0 ? $tce->migracao.'/'.$tce->aditivos->count() : $tce->migracao}}</td>
            @else
            <td>{{$tce->aditivos->count() > 0 ? $tce->id.'/'.$tce->aditivos->count() : $tce->id}}</td>
            @endif
            <td>{{$tce->estudante->nmEstudante}}</td>
            <td>{{$tce->dtInicio->format('d/m/Y')}}</td>
            <td>
              @if($tce->aditivos->count() > 0)
              @if($tce->aditivos()->latest()->first()->dtInicio != Null)
              {{date('d/m/Y',strtotime($tce->aditivos()->latest()->first()->dtFim))}} ADT
              @else
              {{date('d/m/Y',strtotime($tce->dtFim))}} TCE
              @endif
              @else
              {{date('d/m/Y',strtotime($tce->dtFim))}} TCE
              @endif
            </td>
            <td>{{$tce->dtCancelamento->format('d/m/Y')}}</td>
            <td>
              @if(isset($tce->userCancelamento->name))
              {{$tce->userCancelamento->name}}
              @else Propia Concedente
              @endif
            </td>
            {{-- <td>{{$tce->motivoCancelamento_id}}</td>--}}
            <td>
              @if($tce->MotivoCancelamento != NULL)
              {{$tce->MotivoCancelamento->dsMotivoCancelamento}}
              @endif
            </td>
          </tr>
          @php $totalTCE++; @endphp
          @empty
          <tr>
            <td colspan="7" style="text-align:center">Nenhum TCE foi cancelado neste período</td>
          </tr>
          @endforelse
        </table>
        @endif
        <div style="margin-top:-10px;">Total de Resultados: {{$totalTCE}}</div>
        @include('relatorios.inc_rodape_html')
      </div>
    </div>
  </body>
  </html>
