@extends('layouts.painel2')
@section('title_postfix', ' - '.$titulo)
@section('content')

<!-- Header -->
<div class="header bg-gradient-primary pb-4 pt-5 pt-md-8">
  <div class="container-fluid">
    <div class="header-body">
    </div>
  </div>
</div>
<!-- /.row -->
<div class="container-fluid mt-4">
  <div class="row justify-content-center">
    <div class="col-md-4">
      <a href="Javascrip:void(0);" role="button">
        <div class="card card-stats mb-4 mb-xl-0">
          <div class="card-body">
            <div class="row">
              <div class="col">
                <h5 class="card-title text-uppercase text-muted mb-0">Relatório TCE's Pendentes</h5>
                <span class="h2 font-weight-bold mb-0">{{ $tces->where('dt4Via', null)->where('dtCancelamento', null)->count() }}</span>
              </div>
              <div class="col-auto">
                <div class="icon icon-shape bg-blue text-white rounded-circle shadow">
                  <i class="fas fa-file"></i>
                </div>
              </div>
            </div>
          </div>
          <div class="card-footer">
            <div class="row d-flex justify-content-center">
              <div class="col-md-6">
                <button class="btn btn-success" type="button" id="btnBaixarPendentes" data-toggle="modal" data-target="#modalPendentes">
                  <i class="fa fa-file-pdf"></i> BAIXAR
                </button>
              </div>
            </div>
          </div>
        </div>
      </a>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="modalPendentes" tabindex="-1" role="dialog" aria-labelledby="modalPendentesLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="modalPendentesLabel">TCE's Pendentes</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form action="javascript:void(0)" method="GET" id="formPendentes">
              <div class="form-group">
                <select id="tcesPendentes" class="form-control">
                  <option selected>Tipo do TCE</option>
                  <option value="simplificado">Simplificado</option>
                  <option value="completo">Completo</option>
                </select>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="btnPendentes" >BAIXAR</button>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="card card-stats mb-4 mb-xl-0">
        <div class="card-body">
          <div class="row">
            <div class="col">
              <h5 class="card-title text-uppercase text-muted mb-0">Relatório TCE's Ativos</h5>
              <span class="h2 font-weight-bold mb-0">{{ $tces->where('dt4Via', '!=', null)->where('dtCancelamento', null)->count() }}</span>
            </div>
            <div class="col-auto">
              <div class="icon icon-shape bg-green text-white rounded-circle shadow">
                <i class="fas fa-check"></i>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer">
          <div class="row d-flex justify-content-center">
            <div class="col-md-6">
              <button class="btn btn-success" type="button" id="btnBaixarAtivos" data-toggle="modal" data-target="#modalAtivos">
                <i class="fa fa-file-pdf"></i> BAIXAR
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="modalAtivos" tabindex="-1" role="dialog" aria-labelledby="modalAtivosLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="modalAtivosLabel">TCE's Ativos</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form action="javascript:void(0)" method="GET" id="formAtivos">
              <div class="form-group">
                <select id="tcesAtivos" class="form-control">
                  <option selected>Tipo do TCE</option>
                  <option value="simplificado">Simplificado</option>
                  <option value="completo">Completo</option>
                </select>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="btnAtivos" >BAIXAR</button>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="card card-stats mb-4 mb-xl-0">
        <div class="card-body">
          <div class="row">
            <div class="col">
              <h5 class="card-title text-uppercase text-muted mb-0">Relatório TCE's Cancelados</h5>
              <span class="h2 font-weight-bold mb-0">{{ $tces->where('dtCancelamento', '<>', null)->count() }}</span>
            </div>
            <div class="col-auto">
              <div class="icon icon-shape bg-red text-white rounded-circle shadow">
                <i class="fas fa-times"></i>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer">
          <div class="row d-flex justify-content-center">
            <div class="col-md-6">
              <button class="btn btn-success" type="button" id="btnBaixarCancelados" data-toggle="modal" data-target="#modalCancelados">
                <i class="fa fa-file-pdf"></i> BAIXAR
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="modalCancelados" tabindex="-1" role="dialog" aria-labelledby="modalCancelados" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="modalCanceladosLabel">TCE's Cancelados</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form action="javascript:void(0)" method="GET" id="formCancelados">
              <div class="form-group">
                <select id="tcesCancelados" class="form-control">
                  <option selected>Tipo do TCE</option>
                  <option value="simplificado">Simplificado</option>
                  <option value="completo">Completo</option>
                </select>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="btnCancelados" >BAIXAR</button>
          </div>
        </div>
      </div>
    </div>
    <!--/cols -->
  </div>
  <br>
  <!-- row -->
  <div class="row justify-content-start">
    <!-- col -->
    <div class="col-md-4">
      <a href="Javascrip:void(0);" role="button">
        <div class="card card-stats mb-4 mb-xl-0">
          <div class="card-body">
            <div class="row">
              <div class="col">
                <h5 class="card-title text-uppercase text-muted mb-0">Relatório Supervisor</h5>
                <span class="h2 font-weight-bold mb-0">{{ $supervisores->count() }}</span>
              </div>
              <div class="col-auto">
                <div class="icon icon-shape bg-blue text-white rounded-circle shadow">
                  <i class="fas fa-chart-pie"></i>
                </div>
              </div>
            </div>
          </div>
          <div class="card-footer">
            <div class="row d-flex justify-content-center">
              <div class="col-md-6">
                <button class="btn btn-success" type="button" id="btnRelatSupervisor" data-toggle="modal" data-target="#modalRelatSupervisor">
                  <i class="fa fa-file-pdf"></i> BAIXAR
                </button>
              </div>
            </div>
          </div>
        </div>
      </a>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="modalRelatSupervisor" tabindex="-1" role="dialog" aria-labelledby="modalRelatSupervisorLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="modalRelatSupervisorLabel">Relatório de Supervisores</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form action="javascript:void(0)" method="GET" id="formSupervisores">
              <div class="form-group">
                <select id="relSupervisores" class="form-control">
                  <option selected>Tipo do Relatório</option>
                  <option value="simplificado">Simplificado</option>
                  <option value="completo">Completo</option>
                </select>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="btnSupervisores" >BAIXAR</button>
          </div>
        </div>
      </div>
    </div>
    {{-- <!-- col -->
    <div class="col-md-4">
      <div class="card card-stats mb-4 mb-xl-0">
        <div class="card-body">
          <div class="row">
            <div class="col">
              <h5 class="card-title text-uppercase text-muted mb-0">Relatório Mediador</h5>
              <span class="h2 font-weight-bold mb-0">{{$mediadores->count()}}</span>
            </div>
            <div class="col-auto">
              <div class="icon icon-shape bg-green text-white rounded-circle shadow">
                <i class="fas fa-chart-pie"></i>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer">
          <div class="row d-flex justify-content-center">
            <div class="col-md-6">
              <button class="btn btn-success" type="button" id="btnRelatMediador" data-toggle="modal" data-target="#modalRelatMediador">
                <i class="fa fa-file-pdf"></i> BAIXAR
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="modalRelatMediador" tabindex="-1" role="dialog" aria-labelledby="modalRelatMediadorLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="modalRelatMediadorLabel">Relatório de Mediadores</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form action="javascript:void(0)" method="GET" id="formMediadores">
              <div class="form-group">
                <select id="relMediadores" class="form-control">
                  <option selected>Tipo do Relatório</option>
                  <option value="simplificado">Simplificado</option>
                  <option value="completo">Completo</option>
                </select>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="btnMediadores" >BAIXAR</button>
          </div>
        </div>
      </div>
    </div> --}}
    <!--col -->
    <div class="col-md-4">
      <div class="card card-stats mb-4 mb-xl-0">
        <div class="card-body">
          <div class="row">
            <div class="col">
              <h5 class="card-title text-uppercase text-muted mb-0">Relatório Estudantes</h5>
              <span class="h2 font-weight-bold mb-0">{{ $estudantes->count() }}</span>
            </div>
            <div class="col-auto">
              <div class="icon icon-shape bg-red text-white rounded-circle shadow">
                <i class="fas fa-chart-pie"></i>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer">
          <div class="row d-flex justify-content-center">
            <div class="col-md-6">
              <button class="btn btn-success" type="button" id="btnRelatEstudantes" data-toggle="modal" data-target="#modalRelatEstudantes">
                <i class="fa fa-file-pdf"></i> BAIXAR
              </button>
            </div>
          </div>
        </div>
      </div>
      <!--/cols -->
    </div>
    <!-- Modal -->
    <div class="modal fade" id="modalRelatEstudantes" tabindex="-1" role="dialog" aria-labelledby="modalRelatEstudanteLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="modalRelatEstudanteLabel">Relatório de Estudantes</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form action="javascript:void(0)" method="GET" id="formEstudantes">
              <div class="form-group">
                <select id="relEstudantes" class="form-control">
                  <option selected>Tipo do Relatório</option>
                  <option value="simplificado">Simplificado</option>
                  <option value="completo">Completo</option>
                </select>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="btnEstudantes" >BAIXAR</button>
          </div>
        </div>
      </div>
    </div>
    <!--/row -->
  </div>
  <!-- /#page-wrapper -->
  @stop
  @section('js')
  
  <script>
    $('#tcesPendentes').change(function(){
      switch ($('#tcesPendentes').val()) {
        case "simplificado":
        $('#formPendentes').attr('action', '{{route("painel.concedente.relatorio.pendentes", ["tipo" => "simplificado"])}}');
        $('#btnPendentes').click(function(){
          $('#formPendentes').submit()
        });
        break;
        case "completo":
        $('#formPendentes').attr('action', '{{route("painel.concedente.relatorio.pendentes", ["tipo" => "completo"])}}');
        $('#btnPendentes').click(function(){
          $('#formPendentes').submit()
        });
        break;
        
        default:
        break;
      }
    });
    
    $('#tcesAtivos').change(function(){
      switch ($('#tcesAtivos').val()) {
        case "simplificado":
        $('#formAtivos').attr('action', '{{route("painel.concedente.relatorio.ativos", ["tipo" => "simplificado"])}}');
        $('#btnAtivos').click(function(){
          $('#formAtivos').submit()
        });
        break;
        case "completo":
        $('#formAtivos').attr('action', '{{route("painel.concedente.relatorio.ativos", ["tipo" => "completo"])}}');
        $('#btnAtivos').click(function(){
          $('#formAtivos').submit()
        });
        break;
        
        default:
        break;
      }
    });
    
    $('#tcesCancelados').change(function(){
      switch ($('#tcesCancelados').val()) {
        case "simplificado":
        $('#formCancelados').attr('action', '{{route("painel.concedente.relatorio.cancelados", ["tipo" => "simplificado"])}}');
        $('#btnCancelados').click(function(){
          $('#formCancelados').submit()
        });
        break;
        case "completo":
        $('#formCancelados').attr('action', '{{route("painel.concedente.relatorio.cancelados", ["tipo" => "completo"])}}');
        $('#btnCancelados').click(function(){
          $('#formCancelados').submit()
        });
        break;
        
        default:
        break;
      }
    });
    
    $('#relSupervisores').change(function(){
      switch ($('#relSupervisores').val()) {
        case "simplificado":
        $('#formSupervisores').attr('action', '{{route("painel.concedente.relatorio.supervisores", ["tipo" => "simplificado"])}}');
        $('#btnSupervisores').click(function(){
          $('#formSupervisores').submit()
        });
        break;
        case "completo":
        $('#formSupervisores').attr('action', '{{route("painel.concedente.relatorio.supervisores", ["tipo" => "completo"])}}');
        $('#btnSupervisores').click(function(){
          $('#formSupervisores').submit()
        });
        break;
        
        default:
        break;
      }
    });
    
    $('#relMediadores').change(function(){
      switch ($('#relMediadores').val()) {
        case "simplificado":
        $('#formMediadores').attr('action', '{{route("painel.concedente.relatorio.mediadores", ["tipo" => "simplificado"])}}');
        $('#btnMediadores').click(function(){
          $('#formMediadores').submit()
        });
        break;
        case "completo":
        $('#formMediadores').attr('action', '{{route("painel.concedente.relatorio.mediadores", ["tipo" => "completo"])}}');
        $('#btnMediadores ').click(function(){
          $('#formMediadores').submit()
        });
        break;
        
        default:
        break;
      }
    });
    
    $('#relEstudantes').change(function(){
      switch ($('#relEstudantes').val()) {
        case "simplificado":
        $('#formEstudantes').attr('action', '{{route("painel.concedente.relatorio.estudantes", ["tipo" => "simplificado"])}}');
        $('#btnEstudantes').click(function(){
          $('#formEstudantes').submit()
        });
        break;
        case "completo":
        $('#formEstudantes').attr('action', '{{route("painel.concedente.relatorio.estudantes", ["tipo" => "completo"])}}');
        $('#btnEstudantes ').click(function(){
          $('#formEstudantes').submit()
        });
        break;
        
        default:
        break;
      }
    });
  </script>
  
  @endsection