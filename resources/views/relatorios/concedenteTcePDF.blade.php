<!DOCTYPE html>
<html>
<head>
    <title>Relatório Concedentes Tce</title>
    <!-- Bootstrap 4-ONLINE -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb"
          crossorigin="anonymous" media="all">
    <link rel="stylesheet" href="http://localhost/projetosLaravel/freelancer/estagio/public/css/relatoriosPDF.css" />
</head>
<body>

<div class="row" style="margin-bottom: 30px;">
    <div style="width: 30%">
        <?php $image_path = '/img/relatorios/logo_default.png'; ?>
        <img src="{{ public_path() . $image_path }}" alt="Upa - Estagio" style="padding-left: 30px"/>
    </div>
    <div style="width: 70%; float: right">
        <h3 style="text-align: center; margin-top: 40px;">
            <strong>TCEs: {{mb_strtoupper($Concedente->nmRazaoSocial)}}</strong>
        </h3>
    </div>
</div>

<!-- grupamento po Estado -->
<table class="table table-bordered">
    <tr class="topo">
        <td width="40">Nº TCE</td>
        <td>Estudante</td>
        <td width="70">Inicio</td>
        <td width="70">Fim</td>
    </tr>

    @foreach($Concedente->tce->sortBy($Ordenacao) as $tce)
        <tr>
            @if($tce->aditivos->count() > 0)
                <td>{{$tce->id}}/{{$tce->aditivos->where('dtInicio','<>',Null)->where('dtFim','<>',Null)->count()}}</td>
            @else
                <td>{{$tce->id}}</td>
            @endif
            <td>{{$tce->estudante->nmEstudante}}</td>
            <td>{{date('d/m/Y',strtotime($tce->dtInicio))}}</td>
            <td>
                @if($tce->aditivos->count() > 0)
                    @if($tce->aditivos()->latest()->first()->dtInicio != Null)
                        {{date('d/m/Y',strtotime($tce->aditivos()->latest()->first()->dtFim))}}
                    @else
                        {{date('d/m/Y',strtotime($tce->dtFim))}}
                    @endif
                @else
                    {{date('d/m/Y',strtotime($tce->dtFim))}}
                @endif
            </td>
        </tr>
    @endforeach
</table>
<!-- FIM. Agrupamento po Estado -->
@include('relatorio.inc_rodape')
</body>
</html>