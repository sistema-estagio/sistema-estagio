<!DOCTYPE html>
<html>
<head>
    <title>Relatório Estudantes</title>
    <!-- Bootstrap 4-ONLINE -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb"
          crossorigin="anonymous" media="all">
    <link rel="stylesheet" href="http://localhost/projetosLaravel/freelancer/estagio/public/css/relatoriosPDF.css" />
</head>
<body>

<div class="row" style="margin-bottom: 30px;">
    <div style="width: 30%">
        <?php $image_path = '/img/relatorios/logo_default.png'; ?>
        <img src="{{ public_path() . $image_path }}" alt="Upa - Estagio" style="padding-left: 30px"/>
    </div>
    <div style="width: 70%; float: right">
        <h3 style="text-align: center; margin-top: 40px;">
            <strong>RELATÓRIO DE ESTUDANTES</strong>
        </h3>
    </div>
</div>

<!-- grupamento po Estado -->
<h4><b>--</b></h4>
    <table class="table table-bordered">
        <tr class="topo">
            <td width="1">#</td>
            <td>Nome Estudante</td>
            <td width="2">TCE</td>
        </tr>
        @foreach($Estudantes as $estudante)
            <tr>
                <td>{{$estudante->id}}</td>
                <td>{{$estudante->nmEstudante}}</td>
                @if($estudante->tce)
                    <td>{{$estudante->tce->id}}</td>
                @else
                    <td>--</td>
                @endif
            </tr>
        @endforeach
    </table>
<!-- FIM. Agrupamento po Estado -->
@include('relatorio.inc_rodape')
</body>
</html>