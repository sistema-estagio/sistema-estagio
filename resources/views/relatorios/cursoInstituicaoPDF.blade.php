<!DOCTYPE html>
<html>
<head>
    <title>Reatorio Instituições de Ensino</title>
    <!-- Bootstrap 4-ONLINE -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb"
          crossorigin="anonymous" media="all">
    <link rel="stylesheet" href="http://localhost/projetosLaravel/freelancer/estagio/public/css/relatoriosPDF.css" />
</head>
<body>

<div class="row" style="margin-bottom: 30px;">
    <div style="width: 30%">
        <?php $image_path = '/img/relatorios/logo_default.png'; ?>
        <img src="{{ public_path() . $image_path }}" alt="Upa - Estagio" style="padding-left: 30px"/>
    </div>
    <div style="width: 70%; float: right">
        <h3 style="text-align: center; margin-top: 40px;">
            <strong>RELATÓRIO DE CURSOS</strong>
        </h3>
    </div>
</div>

<!-- grupamento po Estado -->
@foreach($Cursos as $Curso)
    <div class="estado-table">
        <h5>{{$Curso->nmCurso}}</h5>
        <table class="table table-bordered">
            <tr class="topo">
                <td>Instituição de Ensino</td>
                <td>Periodo</td>
                <td>Turnos</td>
            </tr>

            @foreach($Instituicao as $instituicao)
                @foreach($instituicao->CursoDaInstituicao->where('curso_id',$Curso->id) as  $cr)
                <tr>
                    <td>{{$cr->instituicao->nmInstituicao}}</td>
                    <td>{{$cr->qtdDuracao}}</td>
                    <td>
                        @forelse ($cr->turnos as $turno)
                        [{{ $turno->nmTurno }}]
                    @empty
                        <p>-</p>
                    @endforelse
                    </td>
                </tr>
                @endforeach
            @endforeach

        </table>
    </div>
@endforeach
<!-- FIM. Agrupamento po Estado -->

@include('relatorio.inc_rodape')

</body>
</html>