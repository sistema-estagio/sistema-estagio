<!DOCTYPE html>
<html>

<head>
	<title>Reatorio Instituições de Ensino</title>
	<!-- Bootstrap 4-ONLINE -->
	<style>
		html { margin: 10px}
		body {
			font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif;
		}

		footer.fixar-rodape {

			bottom: 0;
			left: 0;
			height: 40px;
			position: fixed;
			width: 100%;
		}

		.novaPagina {
			page-break-before: always;
			page-break-after: always
		}

		div.body-content {
			/** Essa margem vai evitar que o conteudo fique por baixo do rodapé **/
			margin-bottom: 40px;
		}

		.col-12 {
			width: 100% !important;
		}

		.col-6 {
			width: 50% !important;
		}

		.col-6,
		.col-12 {
			float: left !important;
		}

		.table {
			border-collapse: separate;
			border-spacing: 0;
			min-width: 350px;
			margin-bottom: 10px;
		}

		.table tr th,
		.table tr td {
			border-right: 1px solid #bbb;
			border-bottom: 1px solid #bbb;
			padding: 5px;
        }
        .table th {
            border-top: solid 1px #bbb;
        }

		.table tr th:first-child,
		.table tr th:last-child {
			border-top: solid 1px #bbb;
		}

		.table tr th:first-child,
		.table tr td:first-child {
			border-left: 1px solid #bbb;
		}

		.table tr th:first-child,
		.table tr td:first-child {
			border-left: 1px solid #bbb;
		}

		.table tr th {
			background: #eee;
			text-align: left;
		}

		.table.Info tr th,
		.table.Info tr:first-child td {
			border-top: 1px solid #bbb;
		}
		/* top-left border-radius */

		.table tr:first-child th:first-child,
		.table.Info tr:first-child td:first-child {
			border-top-left-radius: 6px;
		}
		/* top-right border-radius */

		.table tr:first-child th:last-child,
		.table.Info tr:first-child td:last-child {
			border-top-right-radius: 6px;
		}
		/* bottom-left border-radius */

		.table tr:last-child td:first-child {
			border-bottom-left-radius: 6px;
		}
		/* bottom-right border-radius */

		.table tr:last-child td:last-child {
			border-bottom-right-radius: 6px;
		}
	</style>
</head>

<body>
	<div id="body">
		<div id="section_header">
		</div>

		<div id="content">

			<div class="page" style="font-size: 7pt">
				<table style="width: 100%;" class="header">
					<tr>
						<td>
							<?php $image_path = '/img/relatorios/logo_default.png'; ?>
							<img src="{{ public_path() . $image_path }}" alt="Upa - Estagio" />
						</td>
						<td>
							<h1 style="text-align: right">
								<strong>RELATÓRIO DE INSTITUIÇÕES DE ENSINO</strong>
							</h1>
						</td>
					</tr>
				</table>
                <hr style=" margin-bottom: 0px; border-bottom: 1px solid #DCDCDC ;">
				<!-- grupamento po Estado -->
				@foreach($Estados as $estado) @if($estado->instituicao->count() > 0)
				<div class="estado-table">
					<h2><strong>{{$estado->nmEstado}}</strong></h2>
					@foreach($estado->instituicao->groupBy('cidade_id') as $Instituicao)
					<div class="cidades">
						@foreach($Instituicao->slice(0,1) as $cidade)
						<h4> - {{$cidade->cidade->nmCidade}}</h4>
						@endforeach
						<table class="table table-bordered table-condensed" style="width: 100%;">
							<tr style="text-align: center">
								<th width="2">Cód</th>
								<th>Razão Social</th>
								<th>Nome Fantasia</th>
								<th>CNPJ</th>
								<th>Cursos</th>
							</tr>
							@foreach($Instituicao as $instituicao)
							<tr>
								<td>{{$instituicao->id}}</td>
								<td>{{$instituicao->nmInstituicao}}</td>
								<td>{{$instituicao->nmFantasia}}</td>
								<td>{{$instituicao->cdCNPJ}}</td>
								<td>
									@foreach($instituicao->CursoDaInstituicao as $cursos) [{{$cursos->curso->nmCurso}}] @if($cursos->count > 1){ , } @endif  @endforeach
								</td>
							</tr>
							@endforeach
						</table>
					</div>
					@endforeach
				</div>
                @endif 
                @endforeach
				<!-- FIM. Agrupamento po Estado -->
                {{--@include('tce.relatorio.inc_rodape') --}}
                @include('relatorio.inc_rodape') 
			</div>

		</div>
	</div>
</body>

</html>