<!DOCTYPE html>
<html>
<head>
    <title>Cursos</title>
    <!-- Bootstrap 4-ONLINE -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb"
          crossorigin="anonymous" media="all">
    <link rel="stylesheet" href="http://localhost/projetosLaravel/freelancer/estagio/public/css/relatoriosPDF.css" />
</head>
<body>

<div class="row" style="margin-bottom: 30px;">
    <div style="width: 30%">
        <?php $image_path = '/img/relatorios/logo_default.png'; ?>
        <img src="{{ public_path() . $image_path }}" alt="Upa - Estagio" style="padding-left: 30px"/>
    </div>
    <div style="width: 70%; float: right">
        <h3 style="text-align: center; margin-top: 40px;">
            <strong>RELATÓRIO DE CURSOS</strong>
        </h3>
    </div>
</div>

<!-- grupamento po Estado -->
@foreach($Estados as $estado)
@if($estado->instituicao->count() > 0)
    <div class="estado-table">
        <h5>{{$estado->nmEstado}}</h5>
        @foreach($estado->instituicao->groupBy('cidade_id') as $Instituicao)
            <div class="cidades">
            @foreach($Instituicao->slice(0,1) as $cidade)
                <h5> - {{$cidade->cidade->nmCidade}}</h5>
            @endforeach
        <table class="table table-bordered">
            @foreach($Instituicao as $instituicao)
                <tr class="topo">
                    <td class="text-center" colspan="2">{{$instituicao->nmInstituicao}}</td>
                </tr>
                @foreach($instituicao->CursoDaInstituicao as $cursoIntituicao)
                    <tr>
                        <td>{{$cursoIntituicao->curso->nmCurso}}</td>
                        <td style="width: 20%">
                            {{$cursoIntituicao->qtdDuracao}} /
                            @if($cursoIntituicao->qtdDuracao == 1)
                                Ano
                            @elseif($cursoIntituicao->qtdDuracao == 2)
                                Semestres
                            @else
                                Meses
                            @endif
                        </td>
                    </tr>
                @endforeach
            @endforeach
        </table>
            </div>
        @endforeach
    </div>
@endif
@endforeach
<!-- FIM. Agrupamento po Estado -->
@include('relatorio.inc_rodape')
</body>
</html>