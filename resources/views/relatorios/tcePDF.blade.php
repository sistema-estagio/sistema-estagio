<!DOCTYPE html>
<html>
<head>
    <title>Relatório Tce</title>
    <!-- Bootstrap 4-ONLINE -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb"
          crossorigin="anonymous" media="all">
    <link rel="stylesheet" href="http://localhost/projetosLaravel/freelancer/estagio/public/css/relatoriosPDF.css" />
</head>
<body>

<div class="row" style="margin-bottom: 30px;">
    <div style="width: 30%">
        <?php $image_path = '/img/relatorios/logo_default.png'; ?>
        <img src="{{ public_path() . $image_path }}" alt="Upa - Estagio" style="padding-left: 30px"/>
    </div>
    <div style="width: 70%; float: right">
        <h3 style="text-align: center; margin-top: 40px;">
            <strong>RELATÓRIO DE TCE</strong>
        </h3>
    </div>
</div>

<!-- grupamento po Estado -->
    @if($Tipo != 'cancelado')
    <table class="table table-bordered">
        <tr class="topo">
            <td width="1">TCE</td>
            <td>Nome Estudante</td>
            <td>Concedente</td>
            <td>Data Inicio</td>
            <td>Data Fim</td>
        </tr>
        @foreach($Estudante as $tce)
            @if($Tipo == 'vencer')
                @if($tce->aditivos->count() > 0)
                    @if($tce->aditivos()->latest()->first()->dtInicio != Null)
                        <tr @if($tce->aditivos()->latest()->first()->dtFim < \Carbon\Carbon::now()->toDateString()) class="bg-danger" @endif>
                    @else
                        <tr @if($tce->dtFim < \Carbon\Carbon::now()->toDateString()) class="bg-danger" @endif>
                    @endif
                @else
                    <tr @if($tce->dtFim < \Carbon\Carbon::now()->toDateString()) class="bg-danger" @endif>
                @endif
            @else
                <tr>
            @endif

               @if($tce->aditivos->count() > 0)
                   <td>{{$tce->id}}/{{$tce->aditivos->where('dtInicio','<>',Null)->where('dtFim','<>',Null)->count()}}</td>
               @else
                   <td>{{$tce->id}}</td>
               @endif
               <td>{{$tce->estudante->nmEstudante}}</td>
               <td>{{$tce->concedente->nmRazaoSocial}}</td>
               <td>{{date('d/m/Y',strtotime($tce->dtInicio))}}</td>
               <td>
                   @if($tce->aditivos->count() > 0)
                       @if($tce->aditivos()->latest()->first()->dtInicio != Null)
                           {{date('d/m/Y',strtotime($tce->aditivos()->latest()->first()->dtFim))}}
                       @else
                           {{date('d/m/Y',strtotime($tce->dtFim))}}
                       @endif
                   @else
                       {{date('d/m/Y',strtotime($tce->dtFim))}}
                   @endif
               </td>
           </tr>
        @endforeach
    </table>

    @else
        <table class="table table-bordered">
            <tr class="topo">
                <td width="1">TCE</td>
                <td>Nome Estudante</td>
                <td>Concedente</td>
                <td>Data Inicio</td>
                <td>Data Cancelamento</td>
                <td>Usuário</td>
            </tr>
            @foreach($Estudante as $tce)
                    <tr>
                        <td>{{$tce->id}}</td>
                        <td>{{$tce->estudante->nmEstudante}}</td>
                        <td>{{$tce->concedente->nmRazaoSocial}}</td>
                        <td>{{date('d/m/Y',strtotime($tce->dtInicio))}}</td>
                        <td>{{date('d/m/Y',strtotime($tce->dtCancelamento))}}</td>
                        <td>{{$tce->usuarioCancelamento_id}}</td>
                    </tr>
                    @endforeach
        </table>
    @endif
<!-- FIM. Agrupamento po Estado -->

@include('relatorios.inc_rodape_html')
</body>
</html>