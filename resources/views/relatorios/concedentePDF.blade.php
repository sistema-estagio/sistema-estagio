<!DOCTYPE html>
<html>
<head>
    <title>Relatório Concedentes</title>
    <!-- Bootstrap 4-ONLINE -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb"
          crossorigin="anonymous" media="all">
    <link rel="stylesheet" href="http://localhost/projetosLaravel/freelancer/estagio/public/css/relatoriosPDF.css" />
</head>
<body>
<div class="row" style="margin-bottom: 30px;">
    <div style="width: 30%">
        <?php $image_path = '/img/relatorios/logo_default.png'; ?>
        <img src="{{ public_path() . $image_path }}" alt="Upa - Estagio" style="padding-left: 30px"/>
    </div>
    <div style="width: 70%; float: right">
        <h3 style="text-align: center; margin-top: 40px;">
            <strong>RELATÓRIO DE CONCEDENTES</strong>
        </h3>
    </div>
</div>

<!-- grupamento po Estado -->
@foreach($Estados as $estado)
    @if($estado->concedentes->count() > 0)
    <div class="estado-table">
        <h4><b>{{$estado->nmEstado}}</b></h4>
        @foreach($estado->concedentes->groupBy('cidade_id') as $Concedentes)
            <div class="cidades">
            @foreach($Concedentes->slice(0,1) as $cidade)
            <h5> - {{$cidade->cidade->nmCidade}}</h5>
            @endforeach
            <table class="table table-bordered">
                    <tr class="topo">
                        <td>Razão Social</td>
                        <td>Nome Fantasia</td>
                        <td>CNPJ</td>
                        <td>Data Fim Contrato</td>
                    </tr>
                    @foreach($Concedentes as $concedente)
                        <tr>
                            <td>{{$concedente->nmRazaoSocial}}</td>
                            <td>{{$concedente->nmFantasia}}</td>
                            <td>{{$concedente->cdCnpjCpf}}</td>
                            <td>
                                @if($concedente->dtFimContrato != null)
                                    {{date('d/m/Y',strtotime($concedente->dtFimContrato))}}
                                @else
                                    --
                                @endif

                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        @endforeach
    </div>
    @endif
@endforeach
<!-- FIM. Agrupamento po Estado -->
@include('relatorio.inc_rodape')
</body>
</html>