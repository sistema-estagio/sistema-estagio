@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Tce</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><i class="fa fa-industry"></i> Tce</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <form action="{{route('relatorio.tce.pdf')}}" method="GET" target="_blank">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="flTipo">*Tipo de TCE</label>
                                    <select class="form-control" name="tipo">
                                        <option value="todos">Todos Tces</option>
                                        <option value="vencer">Tces à Vencer</option>
                                        <option value="cancelado">Tces Cancelados</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="flTipo">*Ordenacao</label>
                                    <select class="form-control" name="ordenacao">
                                        <option value="id">TCE</option>
                                        <option value="estudante_id">NOME</option>
                                        <option value="dtFim">VENCIMENTO</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer text-left">
                            <button type="submit" class="btn btn-primary">Buscar</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.box -->
        </div>
    </div>
@section('post-script')

    <script type="text/javascript">

        $('select[name=estado_id]').change(function () {
            var idEstado = $(this).val();

            $.get('/sysEstagio/get-cidades/' + idEstado, function (cidades) {

                $('select[name=cidade_id]').empty();
                $('select[name=cidade_id]').append('<option value="">Todos</option>');
                $.each(cidades, function (key, value) {
                    $('select[name=cidade_id]').append('<option value=' + value.id + '>' + value.nmCidade + '</option>');
                });
            });
        });



    </script>
@endsection
@stop