@extends('adminlte::page') 
@if(!empty($titulo))
	@section('title_postfix', ' - '.$titulo)
@endif
@section('content_header')
<h1>Usuários</h1>
<ol class="breadcrumb">
	<li>
		<a href="{{route('home')}}">
			<i class="fa fa-dashboard"></i> Home</a>
	</li>
	<li class="active">
		<i class="fa fa-user"></i> Usuários</li>
</ol>
@stop 
@section('content') 
@if(session('success'))
<div class="alert alert-success alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h4>
		<i class="icon fa fa-check"></i> Alerta!</h4>
	{{session('success')}}
</div>
@endif 
@if(session('error'))
<div class="alert alert-danger alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h4>
		<i class="icon fa fa-ban"></i> Alerta!</h4>
	{{session('error')}}
</div>
@endif
<a href="{{route('usuario.create')}}" class="btn btn-success ad-click-event">Novo Usuário</a>
</br>
</br>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Listagem de Usuários do Sistema</h3>
				<div class="box-tools">
					<form action="{{route('usuario.search')}}" method="POST">
						{!! csrf_field() !!}
						<div class="input-group input-group-sm" style="width: 200px;">
							<input name="key_search" class="form-control pull-right" placeholder="Pesquisar" type="text">

							<div class="input-group-btn">
								<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<table class="table table-bordered">
					<tbody>
						<tr>
							<th style="width: 40px">#</th>
							<th>Usuários</th>
							<!--<th style="width: 40px">Cadastro</th>-->
							<th>E-mail</th>
							<th>Unidade</th>
						</tr>
						@forelse($Usuarios as $usuario)
						<tr>
							<td>{{$usuario->id}}</td>
							<td><a href="{{route('usuario.show',['usuario'=>$usuario->id])}}"> {{$usuario->name}}</a></td>
							<td>{{$usuario->email}}</td>
							@if($usuario->unidade != NULL)
								<td>{{$usuario->unidade->nmUnidade}}</td>
							@else
								<td>ADMINISTRAÇÃO</td>
							@endif
						</tr>
						@empty
						<tr>
							<td colspan="3" align="center">
								<span class="badge bg-red">Sem Registros no Banco de Dados</span>
							</td>
						</tr>
						@endforelse
					</tbody>
				</table>
			</div>
			<div class="box-footer clearfix">

			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				@if(isset($dataForm)) {!! $Usuarios->appends($dataForm)->links() !!} 
					@else {!! $Usuarios->links() !!} 
				@endif
			</div>
		</div>
		<!-- /.box -->
	</div>
	@stop