@extends('adminlte::page')

@if(!empty($titulo))
@section('title_postfix', ' - '.$titulo)
@endif

@section('content_header')
    <h1>Usuario</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('usuario.index')}}"><i class="fa fa-user"></i> Usuários</a></li>
        <li class="active"><i class="fa fa-file"></i> {{$Usuario->name}}</li>
    </ol>
@stop

@section('content')    
    <div class="row">
        <div class="col-md-12">
            <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">{{$Usuario->name}}</h3>
              <div class="box-tools pull-right">
                <b>Codigo:</b> {{$Usuario->id}}
              </div>
            </div>
            <!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-condensed table-striped">
                            <tbody>
                                <tr>
                                    <td colspan="4"><label>Nome Usuário:</label> {{$Usuario->name}}</td>
                                    <td colspan="4"><label>E-mail:</label> {{$Usuario->email}}</td>
                                </tr>
                                <tr>
                                    <td colspan="4"><label>Nível:</label> {{$Usuario->nivel}}</td>
                                    @if($Usuario->unidade != null)
                                        <td colspan="4"><label>Unidade:</label> {{$Usuario->unidade->nmUnidade}} </td>
                                    @else
                                        <td colspan="4"><label>Unidade:</label> Todas </td>
                                    @endif
                                </tr>
                                <tr>

                                </tr>
                            </tbody>
                        </table>

                    </div>
                    <!-- /.box-body -->

          </div>    
  
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3>Permissões Ativas</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    @foreach($Permissoes as $permissao)
                        <?php $Check = in_array($permissao->id, $relacionamentoPermissoes->pluck('id')->toArray());?>
                            @if($Check)
                                <div class="col-md-3">
                                    <label><input type='checkbox' name='permissao_id[]' value="{{$permissao->id}}"  checked disabled> {{$permissao->name}}</label>
                                </div>
                            @endif
                    @endforeach
                </div>
                <!-- /.box-body -->

                <div class="box-footer text-center">
                    <a href="{{route('usuario.edit', $Usuario->id)}}" class="btn btn-warning ad-click-event">Editar</a>
                </div>
            </div>

        </div>
    </div>
</br>

   
@stop