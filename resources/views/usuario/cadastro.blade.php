@extends('adminlte::page')
@if(!empty($titulo))
    @section('title_postfix', ' - '.$titulo)
@endif
@section('content_header')
<h1>Usuário</h1>
<ol class="breadcrumb">
	<li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="{{route('usuario.index')}}"><i class="fa fa-user"></i> Usuários</a></li>
    <li class="active"><i class="fa fa-file"></i> Cadastro de Usuário</li>
</ol>
@stop 
@section('content') 

@if(session('success'))
<div class="alert alert-success alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h4>
		<i class="icon fa fa-check"></i> Alerta!</h4>
	{{session('success')}}
</div>
@endif 

@if(session('error'))
<div class="alert alert-danger alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h4>
		<i class="icon fa fa-ban"></i> Alerta!</h4>
	{{session('error')}}
</div>
@endif

@if(isset($errors)&& $errors->any())
<div class="alert alert-danger alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<ul>
		@foreach($errors->all() as $error)
		<li>{{$error}}</li>
		@endforeach
	</ul>
</div>
@endif

<div class="row">
	<div class="col-md-12">
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">Formulário de Cadastro</h3>
			</div>
			<!-- /.box-header -->
			<!-- form start -->
			<form action="{{route('usuario.store')}}" method="POST">
				{!! csrf_field() !!}
				<div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">*Nome Usuario</label>
                                <input class="form-control" required="true" id="name" name="name" placeholder="Nome do Usuário" type="text">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="email">*E-mail acesso</label>
                                <input class="form-control" required="true" id="email" name="email" placeholder="E-mail de Acesso" type="email">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="password">*Senha</label>
                                <input class="form-control" required="true" id="password" name="password" type="password">
                            </div>
                        </div><!-- col-md-4-->

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="cidade_id">*Unidade</label>
                                <select class="form-control" name="unidade_id" id="unidade_id">
                                    <option value="">Escolha a Unidade</option>
                                    <option value="">Todas</option>
                                    @foreach($Unidades as $unidade)
                                        <option value="{{$unidade->id}}">{{$unidade->nmUnidade}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div><!-- col-md-4-->

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="cidade_id">*Nível</label>
                                <select class="form-control" name="nivel" id="nivel">
                                    <option value="admin">Admin</option>
                                </select>
                            </div>
                        </div><!-- col-md-4-->
                    </div><!-- Row -->

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="cidade_id">Permissões</label>
                                <div class="well" id="Atv">
                                    <div class="col-md-12" style="padding-bottom: 10px">
                                        <label><input type='checkbox' name='selTodos' id="selTodos" onclick="checkedAll ();"> SELECIONAR TODOS</label>
                                    </div>
                                    @foreach($Permissoes as $permissao)
                                        <div class="col-md-3">
                                            <label><input type='checkbox' name='permissao_id[]' class="permissoes" value="{{$permissao->name}}"> {{$permissao->name}}</label>
                                        </div>
                                    @endforeach
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>

				</div>
				<!-- /.box-body -->
				<div class="box-footer text-center">
					<button type="submit" class="btn btn-primary">Cadastrar</button>
				</div>
			</form>
		</div>

	</div>
</div>
<script>
    var checked=false;
    function checkedAll () {
        var aa =  document.getElementsByClassName("permissoes");
        checked = document.getElementById('selTodos').checked;

        for (var i =0; i < aa.length; i++)
        {
            aa[i].checked = checked;
        }
    }
</script>
@stop