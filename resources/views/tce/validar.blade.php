@extends('adminlte::page')
@if(!empty($titulo))
@section('title_postfix', ' - '.$titulo)
@endif
@section('content_header')

    <h1>Validar Tce</h1>
    <ol class="breadcrumb">
        {{--  {{route('estudante.index')}}  --}}
        <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('tce.index')}}"><i class="fa fa-files-o"></i>TCE</a></li>
        <li class="active"><i class="fa fa-check-square-o"></i> Validação de TCE</li>
    </ol>
@stop

@section('content')
    @if(session('success'))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> Alerta!</h4>
            {{session('success')}}
    </div>
    @endif

    @if(session('error'))
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Alerta!</h4>
            {{session('error')}}
    </div>
    @endif

    @if($errors->any())
        <ul class="alert-danger">
            <h3>Whoops, Houve algum erro!</h3>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    <div class="row">
        <div class="col-md-12">
            <div class="box box-default">
            <div class="box-header with-border">
            </div>
            <!-- /.box-header -->
            <!-- form start -->
                <form action="{{route('tce.validar.tce')}}" method="POST">
                {!! csrf_field() !!}
                {!! method_field('PUT') !!}
               
                    <div class="box-body">
                                       
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="tce">*Numero do TCE</label>
                                    <input class="form-control input-lg" id="tce" name="tce"  type="text" placeholder="N° TCE" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer text-center">
                        <button type="submit" class="btn btn-primary btn-lg">Buscar</button>
                    </div>
                </form>
          </div>    
  
        </div>
    </div>
@section('post-script')
<script>
    jQuery(function($){
        $("#dtCancelamento").mask("99/99/9999");
    });
</script>

@endsection
@stop