@extends('adminlte::page')
@if(!empty($titulo))
    @section('title_postfix', ' - '.$titulo)
@endif
@section('content_header')

    <h1>Tce</h1>
    <ol class="breadcrumb">
        {{--  {{route('estudante.index')}}  --}}
        <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('home')}}"><i class="fa fa-files-o"></i>TCE</a></li>
        <li><a href="{{route('tce.show', ['tce' => $tce->id])}}"><i class="fa fa-files-o"></i> {{$tce->id}}</a></li>
        <li class="active"><i class="fa fa-pencil"></i> Edição</li>
    </ol>
@stop

@section('content')
    @if(session('success'))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> Alerta!</h4>
            {{session('success')}}
    </div>
    @endif

    @if(session('error'))
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Alerta!</h4>
            {{session('error')}}
    </div>
    @endif

    @if($errors->any())
        <ul class="alert-danger">
            <h3>Whoops, Houve algum erro!</h3>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    <div class="row">
        <div class="col-md-12">
            <div class="box box-default">
            <div class="box-header with-border">
              <div class="box-title"><strong>Estudante:</strong> {{$tce->estudante->nmEstudante}} - <strong>CPF:</strong> {{$tce->estudante->cdCPF}} - <strong>Instituição de Ensino:</strong> {{$tce->estudante->instituicao->nmInstituicao}}
                <br><strong>Semestre/Ano Atual:</strong> {{$tce->estudante->nnSemestreAno}} {{$tce->estudante->cursoDaInstituicao->cursoDuracao->nmTipoDuracao}} - <strong>Curso:</strong> {{$tce->estudante->cursoDaInstituicao->curso->nmCurso}}({{$tce->estudante->cursoDaInstituicao->qtdDuracao}} {{$tce->estudante->cursoDaInstituicao->cursoDuracao->nmTipoDuracao}})</div>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
                <form action="{{route('tce.update', ['tce' => $tce->id])}}" method="POST">
                {!! csrf_field() !!}
                {!! method_field('PUT') !!}
                <!--<form action="{{route('estudante.tce.store', ['estudante' => $estudante->id])}}" method="POST">-->
                <input id="instituicao_id" name="instituicao_id" type="hidden" value="{{$tce->instituicao_id}}">
                <input id="curso_id" name="curso_id" type="hidden" value="{{$tce->cursoDaInstituicao->id}}">
                <input id="curso_atv_id" name="curso_atv_id" type="hidden" value="{{$tce->cursoDaInstituicao->curso->id}}">
                <input id="nnSemestreAno" name="nnSemestreAno" type="hidden" value="{{$tce->nnSemestreAno}}">
                <input id="estudante_id" name="estudante_id" type="hidden" value="{{$estudante->id}}">
                <input id="estado_id" name="estado_id" type="hidden" value="{{$estudante->estado_id}}">
                <input id="concedente_id" name="concedente_id" type="hidden" value="{{$tce->concedente_id}}">

                    <div class="box-body">

                            <legend>Dados do TCE</legend>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="relatorio_id">*Tipo TCE</label>
                                        <select class="form-control" name="relatorio_id" id="relatorio_id">
                                            <option value="{{$tce->relatorio_id}}" selected>{{$tce->relatorio->nmRelatorio}}</option>
                                            <option value="">----------</option>
                                            @foreach($relatorios as $relatorio)
                                            <option value="{{$relatorio['id']}}">{{$relatorio['nmRelatorio']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="dtIniio">*Inicio</label>
                                        <input class="form-control" id="dtInicio" name="dtInicio"  type="text" value="{{$tce->dtInicio->format("d/m/Y")}}">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="dtFim">*Fim</label>
                                        <input class="form-control" id="dtFim" name="dtFim" type="text" value="{{$tce->dtFim->format("d/m/Y")}}" >
                                    </div>
                                </div>

                                <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="dsApolice">Apolice</label>
                                            <input class="form-control" id="dsApolice" name="dsApolice" placeholder="Numero apolice" type="text" value="{{$tce->dsApolice}}">
                                        </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="periodo_id">*Periodo</label>
                                        <select class="form-control" id="periodo_id" name="periodo_id">
                                        <option value="{{$tce->periodo_id}}" selected>{{$tce->periodo->dsPeriodo}}</option>
                                        <option value="">----------</option>
                                        @foreach($periodos as $periodo)
                                            <option value="{{$periodo['id']}}">{{$periodo['dsPeriodo']}}</option>
                                        @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="dsPeriodo">Periodo Descriminado</label>
                                        <input class="form-control" id="dsPeriodo" name="dsPeriodo" type="text" value="{{$tce->dsPeriodo}}">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="hrInicio">Hora Inicial</label>
                                        <input class="form-control" id="hrInicio" name="hrInicio" type="text" value="{{$tce->hrInicio}}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="hrFim">Hora Final</label>
                                        <input class="form-control" id="hrFim" name="hrFim" type="text" value="{{$tce->hrFim}}">
                                    </div>
                                </div>
                                <!-- -->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="dsLocatao">Lotação do Estágio</label>
                                        <input class="form-control" id="dsLotacao" name="dsLotacao" type="text" value="{{$tce->dsLotacao}}">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="vlAuxilioMensal">Auxilio Mensal</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                            @if($tce->vlAuxilioMensal != NULL)
                                            <input class="form-control" type="text" id="vlAuxilioMensal" name="vlAuxilioMensal" type="text" value="{{number_format($tce->vlAuxilioMensal,2,",",".")}}">
                                            @else
                                            <input class="form-control" type="text" id="vlAuxilioMensal" name="vlAuxilioMensal" type="text">
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="vlAuxilioAlimentacao">Auxilio Alimentação</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                            @if($tce->vlAuxilioAlimentacao != NULL)
                                            <input class="form-control" id="vlAuxilioAlimentacao" name="vlAuxilioAlimentacao" type="text" value="{{number_format($tce->vlAuxilioAlimentacao,2,",",".")}}">
                                            @else
                                            <input class="form-control" id="vlAuxilioAlimentacao" name="vlAuxilioAlimentacao" type="text">
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="vlAuxilioTransporte">Auxilio Transporte</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                            @if($tce->vlAuxilioTransporte != NULL)
                                            <input class="form-control" id="vlAuxilioTransporte" name="vlAuxilioTransporte" type="text" value="{{number_format($tce->vlAuxilioTransporte,2,",",".")}}">
                                            @else
                                            <input class="form-control" id="vlAuxilioTransporte" name="vlAuxilioTransporte" type="text">
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="dsLocatao">Auxilio Descriminado</label>
                                        <input class="form-control" id="auxDescriminado" name="auxDescriminado" type="text" value="{{$tce->auxDescriminado}}">
                                    </div>
                                    <span class="help-block">Caso haja necessidade de descriminar de forma minunciosa o auxilio fornecido pela concedente</span>

                                </div>
                            </div>

                            <legend>Atividades</legend>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-success">
                                        <div class="panel-body">
                                            <h4><strong>Atividades Escolhidas</strong></h4>
                                        </div>
                                        <div class="panel-footer">
                                        @foreach($tce->atividades as $atividade)
                                            <label><input type='checkbox' name='atividades[]' value="{{$atividade['id']}}" checked> {{$atividade['atividade']}}</label><br>
                                        @endforeach
                                        </div>
                                    </div>
                                    <div class="well" id="Atv" style="height: 350px !important; overflow: auto !important; padding: 10px !important"></div>
                                </div>
                            </div>

                    <legend>Concedente</legend>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="concedente_id">*Concedente</label>
                                <br>{{$tce->concedente_id}} - {{$tce->concedente->nmRazaoSocial}}
                                {{--  <select class="form-control" id="concedente_id" name="concedente_id">
                                    <option value="{{$tce->concedente_id}}" selected>{{$tce->concedente_id}} - {{$tce->concedente->nmRazaoSocial}}</option>
                                    <option value="">----------</option>
                                    @foreach($concedentes as $concedente)
                                    <option value="{{$concedente['id']}}">{{$concedente['id']}} - {{$concedente['nmRazaoSocial']}}</option>
                                    @endforeach
                                </select>   --}}
                            </div>
                        </div>
                    </div>
                    @if($secretarias->count() > 0)
                    <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="sec_conc_id">Secretaria Concedente</label>
                                    <select class="form-control" id="sec_conc_id" name="sec_conc_id" value="{{$tce->sec_conc_id}}">
                                    @foreach($secretarias as $secretaria)
                                        <option value="{{ $secretaria['id'] }}" @if(old('sec_conc_id') == $secretaria['id']) {{ 'selected' }} @endif>{{ $secretaria['nmSecretaria'] }}</option>
                                    @endforeach
                                    </select>
                                </div>
                            </div>
                    </div>
                    @endif

                    <div class="row">
                      <div class="col-md-12" id="supervisoresSelect">
                          <div class="form-group">
                              <label for="sec_conc_id">Supervisor</label>
                              <select class="form-control" id="supervisor_id" name="supervisor_id" value="{{$tce->supervisor_id}}">
                                @foreach($supervisors as $supervisor)
                                @if($supervisor->validate($supervisor) == true)
                                <option value="{{$supervisor->id}}">{{$supervisor->nome}}</option>
                                @endif
                                @endforeach
                              </select>
                          </div>
                      </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="nmSupervisor">Supervisor Empresa</label>
                                <input class="form-control" id="nmSupervisor" name="nmSupervisor" type="text" value="{{$tce->nmSupervisor}}" disabled>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="cdCPFSupervisor">CPF Supervisor</label>
                                <input class="form-control" id="cdCPFSupervisor" name="cdCPFSupervisor" type="text" value="{{$tce->cdCPFSupervisor}}" disabled>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="dsSupervisorCargo">Cargo Supervisor Empresa</label>
                                <input class="form-control" id="dsSupervisorCargo" name="dsSupervisorCargo" type="text" value="{{$tce->dsSupervisorCargo}}" disabled>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nmSupervisorIe">Supervisor Instituição de Ensino</label>
                                <input class="form-control" id="nmSupervisorIe" name="nmSupervisorIe" type="text" value="{{$tce->nmSupervisorIe}}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="dsSupervisorIeCargo">Cargo Supervisor Instituição de Ensino</label>
                                <input class="form-control" id="dsSupervisorIeCargo" name="dsSupervisorIeCargo" type="text" value="{{$tce->dsSupervisorIeCargo}}">
                            </div>
                        </div>
                    </div>






                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer text-center">
                        <a class="btn btn-danger" href="{{route('tce.show', ['tce' => $tce->id])}}" role="button">Cancelar Ação</a>
                        <button type="submit" class="btn btn-success">Atualizar TCE</button>
                    </div>
                </form>
          </div>

        </div>
    </div>
@section('post-script')
<script>
    $(document).ready(function(){
        curso_id = $("#curso_atv_id").val();
        $.get('../../get-atividades/' + curso_id, function (atividades) {
                    $.each(atividades, function (key, value) {
                    //$('select[name=cidade_id]').append('<option value=' + value.id + '>' + value.nmCidade + '</option>');
                    $("#Atv").append("<label><input type='checkbox' name='atividades[]' value='" + value.id + "'> " + value.atividade + "</label><br>");
                    });
        });

    });
    jQuery(function($){
        $("#dtInicio").mask("99/99/9999");
        $("#dtFim").mask("99/99/9999");
        $("#hrInicio").mask("99:99");
        $("#hrFim").mask("99:99");
        $('#vlAuxilioMensal').maskMoney({thousands:'.',decimal:','});
        $('#vlAuxilioAlimentacao').maskMoney({thousands:'.',decimal:','});
        $('#vlAuxilioTransporte').maskMoney({thousands:'.',decimal:','});
        $("#cdCPFSupervisor").mask("999.999.999-99");
    });

    function escolheMeses() {
        dtInicio = $("#dtInicio").val();
        var mes = $("#escolheMes").val();
        if (mes != "") {
            var split = dtInicio.split('/');
            novadata = split[2] + "," +split[1]+","+split[0];

            data_americana = new XDate(novadata);
            data_americana.addMonths(mes);

            while ((data_americana.getDay() == 0) || (data_americana.getDay() == 6)) {
                data_americana.addDays(1);
            }
            $("#dtFim").val(data_americana.toString("dd/MM/yyyy"));
        }
    }

    $("select[name=sec_conc_id]").change(function(){
      var idSec = $(this).val();
      var idConcedente = {{$tce->concedente_id}};
      if(idSec != null && idSec != ""){
        $.get('../../../get-supervisor/' + idConcedente + '/' + idSec, function (response) {
            if(response != null){
              $('select[name=supervisor_id]').empty();
              if(response.length == 0){
                $('select[name=supervisor_id]').append('<option value=>Sem supervisor</option>');
              }else{
                $('select[name=supervisor_id]').append('<option value=>Selecione o supervisor</option>');
                $.each(response, function (key, value) {
                  $('select[name=supervisor_id]').append('<option value=' + value.id + ' data-nome="'+ value.nome +'" data-cpf="'+ value.cpf +'" data-cargo="'+ value.cargo +'">'  + value.nome + ' ('+value.cpf+')</option>');
                });
              }
            }
        });
      }
    });

    $("select[name=supervisor_id]").change(function(){
      var selected = $(this).find('option:selected');
      if(selected.data('cpf') != null){
        $("#nmSupervisor").val(selected.data('nome'));
        $("#cdCPFSupervisor").val(selected.data('cpf'));
        $("#dsSupervisorCargo").val(selected.data('cargo'));
      }
    });
</script>

@endsection
@stop
