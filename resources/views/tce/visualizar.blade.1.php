@extends('adminlte::page')
@if(!empty($titulo))
    @section('title_postfix', ' - '.$titulo)
@endif
@section('content_header')

    <h1>Tce</h1>
    <ol class="breadcrumb">
        {{--  {{route('estudante.index')}}  --}}
        <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('tce.index')}}"><i class="fa fa-files-o"></i>TCE</a></li>
        <li class="active"><i class="fa fa-file"></i> {{$tce->id}}</li>
    </ol>
@stop

@section('content')
    @if(session('success'))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> Alerta!</h4>
            {{session('success')}}
    </div>
    @endif

    @if(session('error'))
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Alerta!</h4>
            {{session('error')}}
    </div>
    @endif      
    <div class="row">
        <div class="col-md-12">
            <div class="box box-default">
            
            <!-- /.box-header -->
            <!-- form start -->
                <form action="" method="POST">
                {!! csrf_field() !!}
                    <div class="box-body">
                    @if($tce->dtCancelamento != NULL)
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> Aviso</h4>
                               Esse TCE foi cancelado em {{$tce->dtCancelamento->format('d/m/Y')}} por {{$tce->userCancelamento->name}}
                    </div>
                    @endif
                    <table class="table table-striped table-bordered table-condensed">      
                        <tbody>
                        <tr>
                            <td colspan="2"><label>Nome:</label><br><a href="{{route('estudante.show', ['estudante' => $tce->estudante->id])}}">{{$tce->estudante->nmEstudante}} </a></td>
                            <td colspan="2"><label>Instituição de Ensino:</label><br><a href="{{route('instituicao.show', ['instituicao' => $tce->instituicao->id])}}">{{$tce->instituicao->nmInstituicao}}</a></td>
                            <td colspan="2"><label>Concedente:</label><br><a href="{{route('concedente.show', ['concedente' => $tce->concedente->id])}}">{{$tce->concedente->nmRazaoSocial}}</a></td>
                        </tr>
                        <tr>
                            <td><label>Supervisor Concedente:</label><br>
                            @if ($tce->nmSupervisor === NULL)
                            -
                            @else
                                {{$tce->nmSupervisor}}
                            @endif
                            </td>
                            <td><label>Cargo Supervisor Concedente:</label><br>
                            @if ($tce->dsSupervisorCargo === NULL)
                            -
                            @else
                                {{$tce->dsSupervisorCargo}}
                            @endif
                            </td>
                            <td colspan="2"><label>Supervisor Ie:</label><br>
                            @if ($tce->nmSupervisorIe === NULL)
                            -
                            @else
                                {{$tce->nmSupervisorIe}}
                            @endif
                            </td>
                            <td colspan="2"><label>Cargo Supervisor Ie:</label><br>
                            @if ($tce->dsSupervisorIeCargo === NULL)
                            -
                            @else
                                {{$tce->dsSupervisorIeCargo}}
                            @endif
                            </td>
                        </tr>
                        <tr>
                            <td><label>Auxilio Mensal:</label><br>
                            @if ($tce->vlAuxilioMensal === NULL)
                            -
                            @else
                                R$ {{number_format($tce->vlAuxilioMensal, 2, ',', '.')}}
                            @endif
                            </td>
                            <td><label>Auxilio Alimentação:</label><br>
                            @if ($tce->vlAuxilioAlimentacao === NULL)
                            -
                            @else
                                R$ {{number_format($tce->vlAuxilioAlimentacao, 2, ',', '.')}}
                            @endif
                            </td>
                            <td colspan="2"><label>Auxilio Transporte:</label><br>
                            @if ($tce->vlAuxilioTransporte === NULL)
                            -
                            @else
                                R$ {{number_format($tce->vlAuxilioTransporte, 2, ',', '.')}}
                            @endif
                            </td>
                            <td colspan="1"><label>Total:</label><br>
                            @if ($tce->vlAuxilioMensal === NULL)
                            -
                            @else
                                R$ {{number_format($tce->vlAuxilioMensal+$tce->vlAuxilioAlimentacao+$tce->vlAuxilioTransporte, 2, ',', '.')}}
                            @endif
                            
                            </td>
                            
                        </tr>
                        <tr>
                            <td colspan="1"><label>Vigência Inicio:</label><br>{{$tce->dtInicio->format("d/m/Y")}}</td>
                            <td colspan="1"><label>Vigência Fim:</label><br>{{$tce->dtFim->format("d/m/Y")}}</td>
                            <td colspan="1"><label>Hora Inical de Estágio:</label><br>
                            @if ($tce->hrInicio === NULL)
                            -
                            @else
                                {{$tce->hrInicio}}
                            @endif
                            </td>
                            <td colspan="2"><label>Hora Final de Estágio:</label><br>
                            @if ($tce->hrFim === NULL)
                            -
                            @else
                                {{$tce->hrFim}}
                            @endif
                            </td>
                        </tr>
                    
                        <tr>
                            <td><label>Semestre/Ano TCE:</label><br>{{$tce->nnSemestreAno}} {{$tce->estudante->cursoDaInstituicao->cursoDuracao->nmTipoDuracao}}</td>
                            <td><label>Periodo Semanal:</label><br>{{$tce->periodo->dsPeriodo}}</td>
                            <td><label>Descrição do Periodo:</label><br>{{$tce->dsPeriodo}}</td>
                            <td colspan="3"><label>Número Apolice:</label><br>{{$tce->dsApolice}}</td> 
                        </tr>
                        <tr>
                           <td colspan="5"><label>Atividades:</label><br>
                            <ol type="1">
                            @foreach($tce->atividades as $atividade)	
                                <li>{{$atividade->atividade}}</li>
                            @endforeach
                            </ol>
                           </td> 
                        </tr>
                        </tbody>
                    </table>
                     <div class="box-footer text-center">
                        <a href="{{route('tce.relatorio.show', [$tce->id, $tce->relatorio_id])}}" target="_blank" class="btn btn-success ad-click-event">Ver TCE</a>
                        @if($tce->dtCancelamento == NULL)
                        <div class="btn-group">
                                <button type="button" class="btn btn-primary">Aditivo</button>
                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                  <span class="caret"></span>
                                  <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                        
                                        <li><a href="#">Criar Aditivo</a></li>
                                        <li class="divider"></li>
                                @forelse($aditivos as $aditivo)
                                  <li><a href="#">{{$aditivo->nnAditivo}}° Aditivo</a></li>
                                @empty
                                @endforelse
                                  <li><a href="#">Another action</a></li>
                                  <li><a href="#">Something else here</a></li>
                                  
                                </ul>
                        </div>
                        <a href="{{route('tce.aditivo.create', $tce->id)}}" class="btn btn-primary ad-click-event">Criar Aditivo</a>
                        <a href="{{route('tce.edit', $tce->id)}}" class="btn btn-warning ad-click-event">Editar TCE</a>
                        <a href="{{route('tce.cancelar.form', $tce->id)}}" class="btn btn-danger ad-click-event" onclick="return confirm('Deseja Realmente Cancelar o TCE {{$tce->id}} ?');">Cancelar TCE</a>
                        @else
                        <a href="{{route('tce.recisao.show', $tce->id)}}" target="_blank" class="btn btn-primary ad-click-event">Ver Recisão de TCE</a>
                        @endif
                    </div>
        <br>
        {{--  @if ($aditivos->count())
        <legend>Aditivos</legend>
        <table class="table table-striped">
                <tbody>
                <tr>
                  <th style="width: 22px;">Aditivo</th>
                  <th style="width: 90px;">Data Inicio</th>
                  <th style="width: 90px;">Data Fim</th>
                  <th>Outro</th>
                  <th style="width: 90px;">Ações</th>
                </tr>
                @foreach($aditivos as $aditivo)
                                    
                <tr>
                    <td>{{$aditivo->nnAditivo}} </td>
                    <td>
                        @if ($aditivo->dtInicio === NULL)
                        -
                        @else
                            {{$aditivo->dtInicio->format('d/m/Y')}}
                        @endif
                    </td>
                    <td>
                        @if ($aditivo->dtFim === NULL)
                        -
                        @else
                            {{$aditivo->dtFim->format('d/m/Y')}}
                        @endif
                    </td>
                    <td>
                        @if (($aditivo->aditivoOutro === NULL) OR ($aditivo->aditivoOutro === ""))
                        -
                        @else
                            {{$aditivo->aditivoOutro}}
                        @endif
                    </td>
                    <td>
                        @if($tce->dtCancelamento == NULL)
                        <a href="{{route('tce.aditivo.edit', [$tce->id, $aditivo->id])}}" class="btn btn-warning btn-xs ad-click-event" title="Editar"> <i class="fa fa-fw fa-edit"></i></a>
                        @endif
                        <a href="{{route('tce.aditivo.show', [$tce->id, $aditivo->id])}}" class="btn btn-success btn-xs ad-click-event" title="Visualizar"> <i class="fa fa-fw fa-folder-open"></i></a>
                    </td>
                </tr>
                @endforeach
               
                </tbody>
        </table>
        @endif                 --}}
                   
                    <!-- /.box-body -->

                   
                </form>
          </div>    
  
        </div>
    </div>
    
@section('post-script')
<script>
    jQuery(function($){
        $("#dtInicio").mask("99/99/9999");
        $("#dtFim").mask("99/99/9999");
        $("#hrInicial").mask("99:99");
        $("#hrFinal").mask("99:99");
    });
</script>

@endsection
@stop