@extends('adminlte::page')
@if(!empty($titulo))
@section('title_postfix', ' - '.$titulo)
@endif
@section('content_header')

<h1>Tce</h1>
<ol class="breadcrumb">
  <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
  <li><a href="{{route('estudante.index')}}"><i class="fa fa-users"></i>Estudantes</a></li>
  <li><a href="{{route('tce.index')}}"><i class="fa fa-files-o"></i>TCE</a></li>
  <li class="active"><i class="fa fa-file"></i> Novo TCE</li>
</ol>
@stop

@section('content')
@if(session('success'))
<div class="alert alert-success alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
  <h4><i class="icon fa fa-check"></i> Alerta!</h4>
  {{session('success')}}
</div>
@endif

@if(session('error'))
<div class="alert alert-danger alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
  <h4><i class="icon fa fa-ban"></i> Alerta!</h4>
  {{session('error')}}
</div>
@endif

@if($errors->any())
<div class="alert alert-warning alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
  <h4><i class="icon fa fa-ban"></i> Alerta!</h4>
  <ul class="alert-warning">
    <h3>:( Whoops, Houve algum erro! </h3>
    @foreach($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif
@if ($estudante->portabilidade()->count() != "0")
<div class="callout callout-warning">
  <h4>PORTABILIDADE!</h4>
  <p><strong>Inicio:</strong> {{$estudante->portabilidade->dtInicio->format("d/m/Y")}} e <strong>Fim:</strong> {{$estudante->portabilidade->dtFim->format("d/m/Y")}}
    <br>
    @php
    //Pegando a diferença de tempo entre a data inicio e fim da portabilidade.
    $tempoTotal = $estudante->portabilidade->dtInicio->diffInMonths($estudante->portabilidade->dtFim);
    echo $tempoTotal;
    @endphp
    Meses</p>
  </div>
  @endif
  <div class="row">
    <div class="col-md-12">
      <div class="box box-default">
        <div class="box-header with-border">
          <div class="box-title"><strong>Estudante:</strong> {{$estudante->nmEstudante}} - <strong>CPF:</strong> {{$estudante->cdCPF}} - <strong>Instituição de Ensino:</strong> {{$estudante->instituicao->nmInstituicao}}
            <br><strong>Semestre/Ano Atual:</strong> {{$estudante->nnSemestreAno}} {{$estudante->cursoDaInstituicao->cursoDuracao->nmTipoDuracao}} - <strong>Curso:</strong> {{$estudante->cursoDaInstituicao->curso->nmCurso}}({{$estudante->cursoDaInstituicao->qtdDuracao}} {{$estudante->cursoDaInstituicao->cursoDuracao->nmTipoDuracao}})</div>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form action="{{route('estudante.tce.store', ['estudante' => $estudante->id])}}" method="POST">
            <input id="instituicao_id" name="instituicao_id" type="hidden" value="{{$estudante->instituicao_id}}">
            <input id="curso_id" name="curso_id" type="hidden" value="{{$estudante->cursoDaInstituicao->id}}">
            <input id="curso_atv_id" name="curso_atv_id" type="hidden" value="{{$estudante->cursoDaInstituicao->curso->id}}">
            <input id="nnSemestreAno" name="nnSemestreAno" type="hidden" value="{{$estudante->nnSemestreAno}}">
            <input id="estudante_id" name="estudante_id" type="hidden" value="{{$estudante->id}}">
            <input id="estado_id" name="estado_id" type="hidden" value="{{$estudante->estado_id}}">
            {!! csrf_field() !!}
            <div class="box-body">
              @if(Auth::user()->unidade_id == null)
              {{-- MIGRAÇÃO --}}
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group has-error">
                    <label class="control-label" for="migracao">
                      <i class="fa fa-bell-o"></i> Numero TCE Antigo:
                    </label>
                    <input class="form-control" id="migracao" name="migracao" placeholder="Numero do TCE Antigo se houver" type="text"  value="{{old('migracao')}}">
                    <span class="help-block">*Caso o TCE seja um de Migração digite acima o numero, caso não deixe o campo em branco.</span>
                  </div>
                </div>
              </div>
              {{-- MIGRAÇÃO --}}
              @endif
              <legend>Dados do TCE</legend>
              <div class="row">
                <div class="col-md-3">
                  <div class="form-group">
                    <label for="relatorio_id">*Tipo TCE</label>

                    <select class="form-control" name="relatorio_id" id="relatorio_id">
                      <option value="" selected>Selecione...</option>
                      @foreach($relatorios as $relatorio)
                      <option value="{{ $relatorio['id'] }}" @if(old('relatorio_id') == $relatorio['id']) {{ 'selected' }} @endif>{{ $relatorio['nmRelatorio'] }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>

                <div class="col-md-2">
                  <div class="form-group">
                    <label for="dtInicio">*Inicio</label>
                    @if(old('dtInicio'))
                    <input class="form-control" id="dtInicio" name="dtInicio" placeholder="dd/mm/aaaa" type="text" value="{{old('dtInicio')}}">
                    @else
                    <input class="form-control" id="dtInicio" name="dtInicio" placeholder="dd/mm/aaaa" type="text" value="{{$dtHoje}}">
                    @endif
                  </div>
                </div>

                <div class="col-md-2">
                  <div class="form-group">
                    <label for="escolheMes">Meses</label>
                    <select class="form-control" id="escolheMes" name="escolheMes" onchange="escolheMeses();">
                      <option value="" selected>Selecione...</option>
                      <option value="1">1 MÊS</option>
                      <option value="3">3 MESES</option>
                      <option value="6">6 MESES</option>
                      <option value="12">12 MESES</option>
                      <!--<option value="24">24 MESES</option>-->
                    </select>
                  </div>
                </div>

                <div class="col-md-2">
                  <div class="form-group">
                    <label for="dtFim">*Fim</label>
                    <input class="form-control" id="dtFim" name="dtFim" placeholder="dd/mm/aaaa" type="text" value="{{old('dtFim')}}">
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="form-group">
                    <label for="dsApolice">*Apolice</label>
                    <input class="form-control" id="dsApolice" name="dsApolice" placeholder="Numero apolice" type="text" value="{{old('dsApolice')}}">
                  </div>
                </div>
                <!--<div class="col-md-2">
                <div class="form-group">
                <label for="dtFim">Data Limite</label>
                <br><span>DATA FIM MAXIMA</span>
              </div>
            </div>-->
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="periodo_id">*Periodo</label>
                <select class="form-control" id="periodo_id" name="periodo_id">
                  <option value="" selected>Selecione um periodo</option>
                  @foreach($periodos as $periodo)
                  <option value="{{ $periodo['id'] }}" @if(old('periodo_id') == $periodo['id']) {{ 'selected' }} @endif>{{ $periodo['dsPeriodo'] }}</option>
                  <!--<option value="{{$periodo['id']}}">{{$periodo['dsPeriodo']}}</option>-->
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="dsPeriodo">Periodo Descriminado</label>
                <input class="form-control" id="dsPeriodo" name="dsPeriodo" type="text" value="{{old('dsPeriodo')}}">
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label for="hrInicio">Hora Inicial</label>
                <input class="form-control" id="hrInicio" name="hrInicio" type="text" value="{{old('hrInicio')}}">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="hrFim">Hora Final</label>
                <input class="form-control" id="hrFim" name="hrFim" type="text" value="{{old('hrFim')}}">
              </div>
            </div>
            <!-- -->
            <div class="col-md-4">
              <div class="form-group">
                <label for="dsLocatao">Lotação do Estágio</label>
                <input class="form-control" id="dsLotacao" name="dsLotacao" type="text" value="{{old('dsLotacao')}}">
              </div>
            </div>

          </div>

          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label for="vlAuxilioMensal">Auxilio Mensal</label>
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-money"></i></span>
                  <input class="form-control" type="text" id="vlAuxilioMensal" name="vlAuxilioMensal" type="text" placeholder="Valor" value="{{old('vlAuxilioMensal')}}">
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="vlAuxilioAlimentacao">Auxilio Alimentação</label>
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-money"></i></span>
                  <input class="form-control" id="vlAuxilioAlimentacao" name="vlAuxilioAlimentacao" type="text" placeholder="Valor" value="{{old('vlAuxilioAlimentacao')}}">
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="vlAuxilioTransporte">Auxilio Transporte</label>
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-money"></i></span>
                  <input class="form-control" id="vlAuxilioTransporte" name="vlAuxilioTransporte" type="text" placeholder="Valor" value="{{old('vlAuxilioTransporte')}}">
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label for="dsLocatao">Auxilio Descriminado</label>
                <input class="form-control" id="auxDescriminado" name="auxDescriminado" type="text" value="{{old('auxDescriminado')}}">
              </div>
              <span class="help-block">Caso haja necessidade de descriminar de forma minunciosa o auxilio fornecido pela concedente</span>

            </div>
          </div>

          <legend>Atividades</legend>
          {{-- --}}
          <div class="row">
            <div class="col-md-12">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                <input class="form-control" id="chkfilter" type="text" placeholder="Filtre as atividades...">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="well" id="Atv" style="height: 350px !important; overflow: auto !important; padding: 10px !important">
                @foreach($atividades as $atividade)
                <label><input type='checkbox' id="{{$atividade['atividade']}}" name='atividades[]'  value="{{ $atividade['id'] }}" {{ ( is_array(old('atividades')) && in_array($atividade['id'], old('atividades')) ) ? 'checked ' : '' }}/> {{$atividade['atividade']}}</label><br>
                @endforeach
              </div>
            </div>
          </div>
          <legend>Concedente</legend>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label for="concedente_id">*Concedente</label>
                <select class="form-control" id="concedente_id" name="concedente_id">
                  @if($concedentes->count() == 0)
                  <option value="" selected>Sem registros no banco</option>
                  @else
                  <option value="" selected>Selecione...</option>
                  @endif
                  @foreach($concedentes as $concedente)
                  <option value="{{ $concedente['id'] }}" @if(old('concedente_id') == $concedente['id']) {{ 'selected' }} @endif>{{$concedente['id']}} - {{$concedente['nmRazaoSocial']}}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>

          <div class="row" id="secretariasConcedente">
            <div class="col-md-12">
              <div class="form-group">
                <label for="sec_conc_id">Secretaria Concedente</label>
                <select class="form-control" id="sec_conc_id" name="sec_conc_id">
                  @if(old('sec_conc_id'))
                  @foreach($secretarias as $secretaria)
                  <option value="{{ $secretaria['id'] }}" @if(old('sec_conc_id') == $secretaria['id']) {{ 'selected' }} @endif>{{ $secretaria['nmSecretaria'] }}</option>
                  @endforeach
                  @else
                  <option value="" selected>Sem Secretaria</option>
                  @endif
                </select>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12" id="supervisoresSelect" style="display:none;">
              <div class="form-group">
                <label for="sec_conc_id">Supervisor</label>
                <select class="form-control" id="supervisor_id" name="supervisor_id">
                </select>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label for="nmSupervisor">Supervisor</label>
                <input class="form-control" id="nmSupervisor" name="nmSupervisor" placeholder="Nome do Supervisor da Empresa" type="text" value="{{old('nmSupervisor')}}">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="cdCPFSupervisor">CPF Supervisor</label>
                <input class="form-control" id="cdCPFSupervisor" name="cdCPFSupervisor" placeholder="CPF do Supervisor" type="text" value="{{old('cdCPFSupervisor')}}">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="dsSupervisorCargo">Cargo Supervisor</label>
                <input class="form-control" id="dsSupervisorCargo" name="dsSupervisorCargo" placeholder="Cargo Supervisor da Empresa" type="text" value="{{old('dsSupervisorCargo')}}">
              </div>
            </div>
          </div>
          <legend>Instituição de ensino</legend>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="nmSupervisorIe">Supervisor</label>
                <input class="form-control" id="nmSupervisorIe" name="nmSupervisorIe" placeholder="Nome do Supervisor IE" type="text" value="{{old('nmSupervisorIe')}}">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="dsSupervisorIeCargo">Cargo Supervisor</label>
                <input class="form-control" id="dsSupervisorIeCargo" name="dsSupervisorIeCargo" placeholder="Cargo Supervisor IE" type="text" value="{{old('dsSupervisorIeCargo')}}">
              </div>
            </div>
          </div>
        </div>
        <!-- /.box-body -->

        <div class="box-footer text-center">
          <a href="javascript:history.back()" class="btn btn-danger">Cancelar Ação</a>
          <button type="submit" class="btn btn-success btn-submit">Gerar TCE</button>
        </div>
      </form>
    </div>

  </div>
</div>
@section('post-script')
<script>

$('#supervisor_id').select2({
  dropdownAutoWidth : true,
  width: '100%'
});
$(function() {

  $('#chkfilter').on('keyup', function() {
    // var str = $(this).val();
    //$("#textbox").val(str.toUpperCase());

    var query = this.value;

    $('[name="atividades[]"]').each(function(i, elem) {
      if (elem.id.indexOf(query) != -1) {
        $(this).closest('label').show();
      }else{
        $(this).closest('label').hide();
      }
    });
  });

});

jQuery(document).ready(function() {
  // verifica se existe algum elemento com a class
  if ($(".btn-submit").length){
    // adiciona evento desabilitando o botão
    $(".btn-submit").on('click', function(){
      $(this).attr({
        disabled: true
      });
      // descobrindo o form pai e enviando
      $(this).parents('form:first').submit();
    });
  }
});
jQuery(function($){
  $("#dtInicio").mask("99/99/9999");
  $("#dtFim").mask("99/99/9999");
  $("#hrInicio").mask("99:99");
  $("#hrFim").mask("99:99");
  $('#vlAuxilioMensal').maskMoney({thousands:'.',decimal:','});
  $('#vlAuxilioAlimentacao').maskMoney({thousands:'.',decimal:','});
  $('#vlAuxilioTransporte').maskMoney({thousands:'.',decimal:','});
  $("#cdCPFSupervisor").mask("999.999.999-99");
});

function escolheMeses() {
  dtInicio = $("#dtInicio").val();
  var mes = $("#escolheMes").val();
  if (mes != "") {
    var split = dtInicio.split('/');
    novadata = split[2] + "," +split[1]+","+split[0];
    /*if(mes == 12){
    data_americana = new XDate(novadata);
    data_americana.addYears(1);
  } else {
  data_americana = new XDate(novadata);
  data_americana.addMonths(mes);
}*/
data_americana = new XDate(novadata);
data_americana.addMonths(mes);
while ((data_americana.getDay() == 0) || (data_americana.getDay() == 6)) {
  data_americana.addDays(1);
}
$("#dtFim").val(data_americana.toString("dd/MM/yyyy"));
}
}


$('select[name=concedente_id]').change(function () {
  var idConcedente = $(this).val();
  $('select[name=sec_conc_id]').empty();
  $('select[name=supervisor_id]').empty();
  $('#supervisoresSelect').hide();
  clearInputs();
  if($(this).find('option:selected').val() != ""){
    $('select[name=supervisor_id]').empty();
    $('select[name=sec_conc_id]').empty();
    $.get('../../../get-secretarias/' + idConcedente, function (secretarias) {
      if(secretarias.length > 0){
        $('select[name=sec_conc_id]').append('<option value="">Selecione a secretaria</option>');
        $.each(secretarias, function (key, value) {
          $('select[name=sec_conc_id]').append('<option value=' + value.id + '>'  + value.nmSecretaria + '</option>');
        });
        $('#secretariasConcedente').show();
      }else{
        $('#secretariasConcedente').hide();
        $('#supervisoresSelect').show();
        $.get('../../../get-supervisor/' + idConcedente , function (supervisors) {
          if(supervisors != null){
            console.log(supervisors);
            $('select[name=supervisor_id]').append('<option value="">Selecione um supervisor</option>');
            $.each(supervisors, function (key, value) {
              $('select[name=supervisor_id]').append('<option value=' + value.id + ' data-nome="'+ value.nome +'" data-cpf="'+ value.cpf +'" data-cargo="'+ value.cargo +'">'  + value.nome + ' ('+value.cpf+')</option>');
            });
          }else{
            $('select[name=supervisor_id]').append('<option value=>Sem supervisor</option>');
          }
        });
      }
    });
  }
});

$("select[name=sec_conc_id]").change(function(){
  var idSec = $(this).val();
  var idConcedente = $('select[name=concedente_id]').val();
  if(idSec != null && idSec != ""){
    $.get('../../../get-supervisor/' + idConcedente + '/' + idSec, function (response) {
      if(response != null){
        $('select[name=supervisor_id]').empty();
        if(response[0]){
          $('select[name=supervisor_id]').append('<option value=>Selecione o supervisor</option>');
          $.each(response, function (key, value) {
            $('select[name=supervisor_id]').append('<option value=' + value.id + ' data-nome="'+ value.nome +'" data-cpf="'+ value.cpf +'" data-cargo="'+ value.cargo +'">'  + value.nome + ' ('+value.cpf+')</option>');
          });
        }else{
          $('select[name=supervisor_id]').append('<option value=>Sem supervisor</option>');
        }
        $('#secretariasConcedente').show();
        $('#supervisoresSelect').show();
      }
    });
  }
});

$("select[name=supervisor_id]").change(function(){
  var selected = $(this).find('option:selected');
  if(selected.data('cpf') != null){
    $("#nmSupervisor").attr('readonly','readonly').val(selected.data('nome'));
    $("#cdCPFSupervisor").attr('readonly','readonly').val(selected.data('cpf'));
    $("#dsSupervisorCargo").attr('readonly','readonly').val(selected.data('cargo'));
  }else{
    clearInputs();
  }
});

function clearInputs(){
  $("#nmSupervisor").val("");
  $("#cdCPFSupervisor").val("");
  $("#dsSupervisorCargo").val("");
  $("#nmSupervisor").removeAttr('readonly');
  $("#cdCPFSupervisor").removeAttr('readonly');
  $("#dsSupervisorCargo").removeAttr('readonly');
}
</script>

@endsection
@stop
