<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<title>Tce Ministerio da Infraestrutura - TCE
		@if($tce->migracao != NULL)
		{{$tce->migracao}}
		@else
		{{$tce->id}}
		@endif
	</title>

	<style>
	html { margin: 30px}
	body {
		font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif;
	}
	footer.fixar-rodape{
		bottom: 0;
		left: 0;
		height: 40px;
		position: fixed;
		width: 100%;
	}
	.novaPagina {
		page-break-before: always;
		/*page-break-after: always*/
	}
	div.body-content{
		/** Essa margem vai evitar que o conteudo fique por baixo do rodapé **/
		margin-bottom: 40px;
	}

	.col-12 {
		width: 100% !important;
	}

	.col-6 {
		width: 50% !important;
	}

	.col-6,
	.col-12 {
		float: left !important;
	}

	.table {
		border-collapse: separate;
		border-spacing: 0;
		min-width: 350px;
		margin-bottom: 10px;
	}

	.table tr th,
	.table tr td {
		border-right: 1px solid #bbb;
		border-bottom: 1px solid #bbb;
		padding: 3px;

	}

	.table tr th:first-child,
	.table tr th:last-child {
		border-top: solid 1px #bbb;
	}

	.table tr th:first-child,
	.table tr td:first-child {
		border-left: 1px solid #bbb;
	}

	.table tr th:first-child,
	.table tr td:first-child {
		border-left: 1px solid #bbb;
	}

	.table tr th {
		background: #eee;
		text-align: left;
	}

	.table.Info tr th,
	.table.Info tr:first-child td {
		border-top: 1px solid #bbb;
	}
	/* top-left border-radius */

	.table tr:first-child th:first-child,
	.table.Info tr:first-child td:first-child {
		border-top-left-radius: 6px;
	}
	/* top-right border-radius */

	.table tr:first-child th:last-child,
	.table.Info tr:first-child td:last-child {
		border-top-right-radius: 6px;
	}
	/* bottom-left border-radius */

	.table tr:last-child td:first-child {
		border-bottom-left-radius: 6px;
	}
	/* bottom-right border-radius */

	.table tr:last-child td:last-child {
		border-bottom-right-radius: 6px;
	}
	</style>
</head>

<body>

	<div id="body">

		<div id="section_header">
			<button type="button" name="button" role="button" style="margin-left: 710px;" onclick="download()">Baixar IMAGEM</button>
		</div>
		<br>
		<div id="content" style="width:850px !important;">
			<div class="page" style="font-size: 9pt">
				<div id="sec-1" style="height:1204px !important; padding-left:30px; padding-right:30px;">
					<table style="width: 100%;" class="header">
						<tr>
							<td>
								<img src="{{ asset('/img/relatorios/logo_default.png') }}" alt="Upa - Estagio" />
							</td>
							<td>
								<h2 style="text-align: left;text-decoration: underline;font-size:12px;">
									<strong>TERMO DE COMPROMISSO DE ESTÁGIO TCE
										@if($tce->migracao != NULL)
										{{$tce->migracao}}
										@else
										{{$tce->id}}
										@endif
										<br>
										 MINISTÉRIO DA INFRAESTRUTURA<br></h2>
										{{--<h3 style="font-size:10px;">(PROCESSO N°XX.XXX/AAAA-e)</h3> --}}
									</strong>

									</td>
								</tr>
							</table>

							<div class="row">
								<div class="col-md-12" style="margin-top: -10px;">
									<p style="margin: 10px;">As partes abaixo qualificadas celebram neste ato Termo de Compromisso de Estágio, conforme a Lei n°. 11.788/08.</p>
								</div>
							</div>

							@if($tce->instituicao->poloMatriz != NULL)
							<!-- SE INSTITUIÇÃO TIVER POLO MATRIZ DEFINIDA EXIBE ENDEREÇO E DADOS DO POLO MATRIZ -->
							<!--INSTITUIÇÃO DE ENSINO-->
							<table style="width: 100%;font-size: 8pt; margin-top: -5px;" class="table table-bordered table-condensed">
								<tr>
									<th colspan="2" style="text-align: center">
										<strong>DADOS DA INSTITUIÇÃO DE ENSINO</strong><span style="float: right;font-size: 6pt"> (Cod.: {{$tce->instituicao_id}})</span>
									</th>
								</tr>
								<tr>
									<td colspan="2">
										<strong>{{$tce->instituicao->poloMatriz->nmPolo}}({{$tce->instituicao->nmInstituicao}})</strong>
									</td>
								</tr>
								<tr>
									<td colspan="2">
										<strong>CNPJ:</strong> {{$tce->instituicao->poloMatriz->cdCNPJ}}
									</td>
								</tr>
								<tr>
									<td>
										<strong>ENDEREÇO: </strong>{{$tce->instituicao->poloMatriz->dsEndereco}} ,
										<strong>N°</strong> {{$tce->instituicao->poloMatriz->nnNumero}}
									</td>
									<td>
										<strong>CIDADE: </strong>{{$tce->instituicao->poloMatriz->cidade->nmCidade}} - {{$tce->instituicao->poloMatriz->cidade->estado->nmEstado}}</td>
									</tr>
									<tr>
										@if($tce->instituicao->poloMatriz->nmDiretor <> NULL)
										<td>
											<strong>REPRESENTANTE: </strong>{{$tce->instituicao->poloMatriz->nmDiretor}}
										</td>
										@else
										<td>
											<strong>REPRESENTANTE: </strong>{{$tce->instituicao->nmDiretor}}
										</td>
										@endif
										<td>
											<strong>TELEFONE: </strong> {{$tce->instituicao->poloMatriz->dsFone}}
										</td>
									</tr>
									<!--SE TIVER SIDO PREENCHIDO NMREITOR NO POLO-->
									@if($tce->instituicao->poloMatriz->nmReitor <> NULL)
									<tr>
										<td colspan="2">
											<strong>REITOR(A) MATRIZ: </strong>{{$tce->instituicao->poloMatriz->nmReitor}}
										</td>
									</tr>
									<!--SE NÂO TIVER PREENCHIDO NMREITOR NO POLO-->
									@else
										<!--SE TIVER PREENCHIDO NMREITOR NA INSTITUICAO-->
										@if($tce->instituicao->nmReitor <> NULL)
										<tr>
											<td colspan="2">
												<strong>REITOR(A): </strong>{{$tce->instituicao->nmReitor}}
											</td>
										</tr>
										@endif
									@endif

									@if(($tce->nmSupervisorIe) AND ($tce->dsSupervisorIeCargo) != NULL)
									<tr>
										<td>
											<strong>SUPERVISOR(A): </strong>{{$tce->nmSupervisorIe}}
										</td>
										<td>
											<strong>CARGO/FORMAÇÃO: </strong>{{$tce->dsSupervisorIeCargo}}
										</td>
									</tr>
									@else
										@if(($tce->instituicao->nmSupervisor) AND ($tce->instituicao->nmSupervisorCargo))
										<tr>
											<td>
												<strong>SUPERVISOR(A): </strong>{{$tce->instituicao->nmSupervisor}}
											</td>
											<td>
												<strong>CARGO/FORMAÇÃO: </strong>{{$tce->instituicao->nmSupervisorCargo}}
											</td>
										</tr>
										@endif
									@endif
								</table>
								@else
								<!-- SE INSTITUIÇÃO NÃO TIVER POLO MATRIZ DEFINIDA EXIBE ENDEREÇO E DADOS DO POLO MATRIZ -->
								<!--INSTITUIÇÃO DE ENSINO-->
								<table style="width: 100%; font-size: 8pt; margin-top: -5px;" class="table table-bordered table-condensed ">
									<tr>
										<th colspan="2" style="text-align: center">
											<strong>DADOS DA INSTITUIÇÃO DE ENSINO</strong><span style="float: right;font-size: 6pt"> (Cod.: {{$tce->instituicao_id}})</span>
										</th>
									</tr>
									<tr>
										<td colspan="2">
											<strong>{{$tce->instituicao->nmInstituicao}}</strong>
										</td>
									</tr>
									<tr>
										<td colspan="2">
											<strong>CNPJ:</strong> {{$tce->instituicao->cdCNPJ}}
										</td>
									</tr>
									<tr>
										<td>
											<strong>ENDEREÇO: </strong>{{$tce->instituicao->dsEndereco}} ,
											<strong>N°</strong> {{$tce->instituicao->nnNumero}}
										</td>
										<td>
											<strong>CIDADE: </strong>{{$tce->instituicao->cidade->nmCidade}} - {{$tce->instituicao->cidade->estado->nmEstado}}</td>
										</tr>
										<tr>
											<td>
												<strong>REPRESENTANTE: </strong>{{$tce->instituicao->nmDiretor}}
											</td>
											<td>
												<strong>TELEFONE: </strong> {{$tce->instituicao->dsFone}}
											</td>
										</tr>
										@if($tce->instituicao->nmReitor <> NULL)
										<tr>
											<td colspan="2">
												<strong>REITOR(A): </strong>{{$tce->instituicao->nmReitor}}
											</td>
										</tr>
										@endif
										@if(($tce->nmSupervisorIe) AND ($tce->dsSupervisorIeCargo) != NULL)
										<tr>
											<td>
												<strong>SUPERVISOR(A): </strong>{{$tce->nmSupervisorIe}}
											</td>
											<td>
												<strong>CARGO/FORMAÇÃO: </strong>{{$tce->dsSupervisorIeCargo}}
											</td>
										</tr>
										@else
										@if(($tce->instituicao->nmSupervisor) AND ($tce->instituicao->nmSupervisorCargo))
										<tr>
											<td>
												<strong>SUPERVISOR(A): </strong>{{$tce->instituicao->nmSupervisor}}
											</td>
											<td>
												<strong>CARGO/FORMAÇÃO: </strong>{{$tce->instituicao->nmSupervisorCargo}}
											</td>
										</tr>
										@endif
										@endif
									</table>
									@endif

									<!--CONCEDENTE-->
									<table style="width: 100%;font-size:8pt; margin-top: -6px;" class="table table-bordered table-condensed" >
										<tbody>
											<tr>
												<th colspan="3" style="text-align: center;">
													<strong>DADOS DA CONCEDENTE</strong><span style="float: right;font-size: 6pt"> (Cod.: {{$tce->concedente_id}})</span>
												</th>
											</tr>
											<tr>
												<td colspan="3">
													{{-- <strong>LOCAL DE REALIZAÇÃO DE ESTÁGIO:</strong> {{$tce->concedente->nmRazaoSocial}} --}}
													<strong>RAZÃO SOCIAL:</strong> {{$tce->concedente->nmRazaoSocial}}
												</td>
											</tr>
											<tr>
												<td colspan="3">
													<strong>CNPJ: </strong>{{$tce->concedente->cdCnpjCpf}}
												</td>
											</tr>
											<tr>
												<td colspan="2">
													<strong>ENDEREÇO: </strong> {{$tce->concedente->dsEndereco}} ,
													<strong>N°</strong> {{$tce->concedente->nnNumero}}
												</td>
												<td>
													@if($tce->concedente->nmBairro)<strong>BAIRRO: </strong>{{$tce->concedente->nmBairro}}@endif
												</td>
											</tr>
											{{--  @if($tce->concedente->dsComplemento)
											<tr>
												<td colspan="3">
													<strong>COMPLEMENTO: </strong> {{$tce->concedente->dsComplemento}}
												</td>
											</tr>
											@endif  --}}
											<tr>
												<td>
													<strong>CIDADE: </strong>{{$tce->concedente->cidade->nmCidade}} - {{$tce->concedente->cidade->estado->nmEstado}}
												</td>
												<td>
													<strong>CEP: </strong>{{$tce->concedente->cdCEP}}
												</td>
												<td>
													<strong>TELEFONE: </strong>{{$tce->concedente->dsFone}}
												</td>
											</tr>
											<tr>
												<td colspan="2">
													<strong>REPRESENTADO POR: </strong>{{$tce->concedente->nmResponsavel}}
												</td>
												<td>
													<strong>CARGO: </strong>{{$tce->concedente->dsResponsavelCargo}}
												</td>
											</tr>
											@if(($tce->nmSupervisor) AND ($tce->dsSupervisorCargo) != NULL)
											<tr>
												<td colspan="3">
													<strong>SUPERVISOR(A): </strong>{{$tce->nmSupervisor}}
												</td>
												{{--  <td>
													<strong>CARGO/FORMAÇÃO: </strong>{{$tce->dsSupervisorCargo}}
												</td>  --}}
											</tr>
											@endif

											@if($tce->dsLotacao)
											<tr>
												<td colspan="3">
													<strong>LOTAÇÃO:</strong> {{$tce->dsLotacao}}
												</td>
											</tr>
											@endif
										</tbody>
									</table>

									<!--ESTAGIARIO-->
									<table style="width: 100%;font-size:8pt; margin-top: -5px;" class="table table-bordered table-condensed">
										<tr>
											<th colspan="3" style="text-align: center">
												<strong>DADOS DO ESTÁGIARIO </strong><span style="float: right;font-size: 6pt">(Cod.: {{$tce->estudante_id}})</span>
											</th>
										</tr>
										<tr>
											<td colspan="">
												<strong>NOME: </strong>{{$tce->estudante->nmEstudante}}
											</td>
											<td>
												<strong>DATA NASC: </strong>{{$tce->estudante->dtNascimento->format('d/m/Y')}}
											</td>
											<td>
												<strong>CPF: </strong>{{$tce->estudante->cdCPF}}
											</td>
										</tr>
										<tr>
											<td>
												<strong>RG: </strong>{{$tce->estudante->cdRG}}
											</td>
											<td>
												<strong>ORG.: </strong>{{$tce->estudante->dsOrgaoRG}}
											</td>
											<td>
												@if($tce->estudante->dsEmail)<strong>EMAIL: </strong>{{$tce->estudante->dsEmail}} @endif
											</td>
										</tr>
										<tr>
											<td colspan="2">
												<strong>ENDEREÇO: </strong> {{$tce->estudante->dsEndereco}},
												<strong> N°</strong> {{$tce->estudante->nnNumero}}
											</td>
											<td>
												<strong>BAIRRO: </strong>{{$tce->estudante->nmBairro}}
											</td>
										</tr>
										{{--  @if($tce->estudante->dsComplemento)
										<tr>
											<td colspan="3">
												<strong>COMPLEMENTO: </strong> {{$tce->estudante->dsComplemento}}
											</td>
										</tr>
										@endif  --}}
										<tr>
											<td colspan="1">
												<strong>CIDADE: </strong>{{$tce->estudante->cidade->nmCidade}} - {{$tce->estudante->cidade->estado->nmEstado}}
											</td>
											<td>
												<strong>CEP: </strong>{{$tce->estudante->cdCEP}}
											</td>
											<td>
												@if($tce->estudante->dsFone)<strong>TELEFONE: </strong>{{$tce->estudante->dsFone}}@endif
											</td>
										</tr>
										<tr>
											<td>
												<strong>CURSO: </strong> {{$tce->cursoDaInstituicao->curso->nmCurso}}
											</td>
											<td>
												<strong>{{$tce->cursoDaInstituicao->cursoDuracao->nmTipoDuracao}}: </strong>
												{{$tce->nnSemestreAno}} {{$tce->cursoDaInstituicao->cursoDuracao->nmTipoDuracao}}
											</td>
											<td>
												<strong>TURNO: </strong>{{$tce->estudante->turno->nmTurno}}
											</td>
										</tr>
									</table>
									{{--  @include('tce.relatorio.inc_agente')  --}}
									<div class="row" id="cl1" style="margin-top: -5px;">
										<div class="col-sm-12">
											<strong>CLÁUSULA 1ª - DO OBJETIVO:</strong>
											<p style="text-align: justify; margin-top: 3px;" class="termo">
												O presente Termo de Compromisso formaliza as condições para realização de estágio de caráter não obrigatório, conforme a legislação vigente, sem caracterização de vínculo empregatício, visando a realização de atividades compatíveis com a programação curricular e projeto pedagógico do curso, devendo permitir ao ESTAGIÁRIO, regularmente matriculado, a prática complementar do aprendizado.
											</p>
										</div>
									</div>

									<div class="row" style="margin-top: -5px;">
										<div class="col-sm-12">
											<strong>CLÁUSULA 2ª - DA VIGÊNCIA:</strong>
											<p style="text-align: justify; margin-top: 3px;" class="termo">
												O presente Termo de Compromisso de Estágio vigerá do dia <strong>{{$tce->dtInicio->format("d/m/Y")}}</strong> até <strong>{{$tce->dtFim->format("d/m/Y")}}</strong>
											</p>
										</div>
									</div>

									<div class="row" style="margin-top: -5px;">
										<div class="col-sm-12">
											<strong class="justi">CLÁUSULA 3ª - DA JORNADA DE ESTÁGIO:</strong>
											<p style="text-align: justify; margin-top: 3px;" class="termo">
												A jornada de estágio será de {{substr($tce->periodo->nmPeriodo, -3, 2)}} ({{Extenso::valorPorExtenso(substr($tce->periodo->nmPeriodo, -3, 2), false, true)}}) horas semanais,
												@php
												if (!empty($tce->dsPeriodo)) {
													print $tce->dsPeriodo;
												}
												if (!empty($tce->hrInicio)) {
													print ' das ' . substr($tce->hrInicio, 0, 5) . ' às ' . substr($tce->hrFim, 0, 5);
												}
												@endphp
												na UNIDADE CONCEDENTE, salvo em situações excepcionais a critério da CONTRATANTE.
											</p>
										</div>
									</div>

									<div class="row" id="cl4" style="margin-top: -5px;">
										<strong>CLÁUSULA 4ª - DO VALOR DA BOLSA DE ESTÁGIO E OUTROS BENEFÍCIOS:</strong>
										<p style="text-align: justify; margin-top: 3px;" class="termo">
											O ESTAGIÁRIO que realizar a jornada completa prevista na Cláusula 3ª, terá direito a receber, mensalmente, uma bolsa de estágio no valor de
											<strong>R${{number_format($tce->vlAuxilioMensal, 2, ',', '.')}}</strong> ({{Extenso::Converter($tce->vlAuxilioMensal)}}) mais Auxílio-Transporte no valor de <strong>R$6,00</strong> (Seis Reais), devido apenas quanto aos dias de efetiva frequência ao estágio, conforme art.12º da Lei 11.788/2008.
										</p>
										<ol class="margin justi" style="text-align: justify; margin-top: -5px;">
											<li>Além da bolsa de estágio estabelecida no “caput” desta cláusula, o ESTAGIÁRIO fará jus a um período de recesso remunerado, proporcional ao tempo de duração, até no máximo 30 (trinta) dias por ano, devido apenas quanto aos dias de efetiva frequência ao estágio, conforme art.13º da Lei 11.788/2008.</li>
											<li>O ESTAGIÁRIO, durante a vigência do presente Termo de Compromisso de Estágio, estará segurado contra acidentes pessoais, conforme <strong>Apólice Coletiva de nº. 9.850-3 de Porto Seguro Cia de Seguros Gerais</strong>, a cargo da Universidade Patativa do Assaré – UPA, conforme inciso IV do art. 9º da Lei 11.788/2008.</li>
										</ol>
									</div>

									<div class="row" id="cl5" style="margin-top: -5px;">
										<strong>CLÁUSULA 5ª - SÃO OBRIGAÇÕES DA INSTITUIÇÃO DE ENSINO:</strong>
										<ol style="text-align: justify; margin-top: 3px;">
											<li>Avaliar as instalações da CONCEDENTE, através de instrumento próprio;</li>
											<li>Notificar a CONCEDENTE quando ocorrer a transferência, trancamento de curso, abandono ou outro fato impeditivo da continuidade do estágio;</li>
											<li>Indicar professor-orientador da área a ser desenvolvida no estágio, para acompanhar e avaliar as atividades do estagiário;</li>
											<li>Comunicar à CONCEDENTE, no início do período letivo, as datas de realização de avaliações escolares ou acadêmicas;</li>
											<li>Aprovar o ESTÁGIO de que trata o presente instrumento, considerando as condições de sua adequação à proposta pedagógica do curso, à etapa e modalidade da formação escolar do ESTAGIÁRIO e o horário e calendário escolar;</li>
											<li>Aprovar o Plano de Atividades de Estágio que consubstancie as condições/requisitos suficientes à exigência legal de adequação à etapa e modalidade da formação escolar do ESTAGIÁRIO.</li>
										</ol>
									</div>
									<div class="row" style="margin-top: -5px;">
											<strong>CLÁUSULA 6ª - SÃO OBRIGAÇÕES DO ESTAGIÁRIO:</strong>
											<ol style="text-align: justify; margin-top: 3px;">
												<li>Cumprir com empenho e interesse toda a programação estabelecida para seu estágio;</li>
												<li>Observar, obedecer e cumprir as normas internas da CONCEDENTE;</li>
												<li>Apresentar documentos comprobatórios da regularidade da sua situação escolar, sempre que solicitado pela CONCEDENTE;</li>
												<li>Informar imediatamente à INSTITUIÇÃO DE ENSINO a rescisão antecipada do presente termo, para que possa adotar as providências administrativas cabíveis;</li>
												<li>Informar, de imediato, à CONCEDENTE, qualquer alteração na sua situação escolar, tais como: trancamento de matrícula, abandono, conclusão de curso ou transferência de Instituição de Ensino;</li>
												<li>Manter atualizados, junto à CONCEDENTE, seus dados pessoais e escolares;</li>
												<li>Entregar, obrigatoriamente, à INSTITUIÇÃO DE ENSINO, uma via do presente instrumento, devidamente assinado pelas partes;</li>
												<li>Informar previamente à CONCEDENTE os períodos de avaliação na Instituição de Ensino, para fins de redução da jornada de estágio;</li>
												<li>Preencher os relatórios de estágio, a fim de subsidiar as instituições de ensino com informações sobre estágio.</li>
											</ol>
									</div>

								</div>

								<div id="sec-2" class="texto" style="font-size:9pt;height:760px !important; padding-left:30px; padding-right:30px;">

									<div class="row">
										<strong>CLÁUSULA 7ª - SÃO OBRIGAÇÕES DA CONCEDENTE:</strong>
										<ol style="text-align: justify; margin-top: 5px;">
											<li>Ofertar instalações que tenham condições de proporcionar ao educando atividades de aprendizagem social, profissional e cultural;</li>
											<li>Zelar pelo cumprimento do presente Termo de Compromisso;</li>
											<li>Designar um supervisor que seja funcionário do seu quadro de pessoal, com formação ou experiência profissional na área de conhecimento desenvolvida no curso do ESTAGIÁRIO, para orientá-lo e acompanhá-lo no desenvolvimento das atividades do estágio;</li>
											<li>Solicitar ao ESTAGIÁRIO, a qualquer tempo, documentos comprobatórios da sua situação escolar, uma vez que, trancamento de matrícula, abandono, conclusão de curso ou transferência de instituição de ensino constituem motivo de imediata rescisão;</li>
											<li>Pagar a bolsa-auxílio e auxílio-transporte ao ESTAGIÁRIO;</li>
											<li>Assegurar ao ESTAGIÁRIO recesso remunerado nos termos da lei 11.788/2008;</li>
											<li>Reduzir a jornada de estágio nos períodos de avaliação, previamente informados pelo ESTAGIÁRIO;</li>
											<li>Entregar, por ocasião do desligamento, Termo de Realização do Estágio;</li>
											<li>Manter os arquivos, e à disposição da fiscalização, os documentos firmados que comprovem a relação de estágio.</li>
										</ol>
									</div>

									<div class="row">
										<strong>CLÁUSULA 8ª - DAS OBRIGAÇÕES DO AGENTE DE INTEGRAÇÃO UNIVERSIDADE PATATIVA DO ASSARÉ - UPA:</strong>
										<ol style="text-align: justify; margin-top: 5px;">
											<li>Firmar convênio com as Instituições de Ensino, contendo as condições exigidas pelas mesmas para a caracterização e definição dos estágios de seus alunos;</li>
											<li>Obter da UNIDADE CONCEDENTE a identificação das oportunidades de estágio a serem concedidas;</li>
											<li>Encaminhar à CONCEDENTE os alunos interessados em estagiar, conforme necessidades definidas conjuntamente;</li>
											<li>Promover o ajuste das condições de estágio, definidas pela INSTITUIÇÃO DE ENSINO, com as condições e disponibilidades da CONCEDENTE, através de Termos Aditivos;</li>
											<li>Preparar e providenciar para que a Concedente e o estudante assinem o respectivo Termo de Compromisso de Estágio, nos termos da Lei 11.788, de 25 de setembro de 2008;</li>
											<li>Preparar toda a documentação legal referente ao estágio, bem como efetivar o Seguro conforme citado na Cláusula 4ª item 2;</li>
											<li>Fornecer à CONCEDENTE, sempre que entender necessário, ou quando solicitado, instruções específicas acerca da prática e supervisão dos estágios nela realizados;</li>
											<li>Propiciar instrumento de controle semestral dos relatórios de atividades preenchido pelo supervisor de estágio da CONCEDENTE e informar a INSTITUÇÃO DE ENSINO;</li>
											<li>Controlar e acompanhar a elaboração do relatório final de estágio, de responsabilidade da concedente;</li>
											<li>Avaliar as instalações de estágio da CONCEDENTE, subsidiando as Instituições de Ensino conforme a Lei.</li>
										</ol>
									</div>

									<div class="row" style="text-align: justify">
										<strong>CLÁUSULA 9ª - O PRESENTE TERMO DE COMPROMISSO DE ESTÁGIO E O PLANO DE ATIVIDADES DE ESTÁGIO SERÃO ALTERADOS OU PRORROGADOS ATRAVÉS DE TERMO ADITIVO, PODENDO O TERMO, NO ENTANTO, SER:</strong>
										<ol class="margin justi; margin-top: 5px;">
											<li>Extinto automaticamente ao término do estágio;</li>
											<li>Rescindido por deliberação da CONCEDENTE ou do estagiário;</li>
											<li>Rescindido por conclusão, abandono ou trancamento de matrícula do curso realizado pelo estagiário.</li>
										</ol>
									</div>

									<div class="row" style="text-align: justify">
										<strong>CLÁUSULA 10ª – A CONCEDENTE E O ESTAGIÁRIO, SIGNATÁRIOS DESTE INSTRUMENTO, DE COMUM ACORDO E PARA OS EFEITOS DA LEI Nº. 11.788/08, ELEGEM A UNIVERSIDADE PATATIVA DO ASSARÉ - UPA COMO SEU AGENTE DE INTEGRAÇÃO, A QUEM COMUNICARÃO A INTERRUPÇÃO OU EVENTUAIS MODIFICAÇÕES DO CONVENCIONADO NO PRESENTE INSTRUMENTO.</strong>
										<p style="text-align: justify" class="termo">
											E, por estarem de inteiro e comum acordo com o Plano de Atividades em anexo e com as demais condições estabelecidas neste TERMO DE COMPROMISSO DE ESTÁGIO-TCE, as partes os assinam em 4 (quatro) vias de igual teor.
										</p>
									</div>
									<br>
									<div class="row">
										<div class="col-6">
											<p style="" style="margin-top: 5px;">Brasilia-DF, _____ de ______________ de _______.</p>
											@php
												$nascimento = $tce->estudante->dtNascimento;
												$nascimento->addYears(18)->toDateString();
												$hoje = \Carbon\Carbon::now()->toDateString();
												if($nascimento > $hoje){
													echo '<div style="font-size: 10px;margin-top: 45px;border-top: 1px solid #333;width:80%;">
														<p class="p" style="margin-top: 3px;">REPRESENTANTE LEGAL DO ESTAGIÁRIO / CPF</p>
													</div>';
												}
											@endphp
										</div>
										<div class="col-6">
											<div class="pull-right">
												<div style="font-size: 10px;margin-top: 20px;border-top: 1px solid #333;width:80%;float:right;">
													<p class="p" style="margin-top: 3px;">{{$tce->estudante->nmEstudante}}</p>
												</div>

												{{-- <div style="font-size: 10px;margin-top: 15px;border-top: 1px solid #333;width:80%;float:right;">
													<p class="p">{{$tce->concedente->nmRazaoSocial}}</p>
												</div> --}}
												<div style="font-size: 10px;margin-top: 35px;border-top: 1px solid #333; width:80%;float:right;">
													<p class="p" style="margin-top: 3px;">{{$tce->instituicao->nmInstituicao}}</p>
												</div>
											</div>
										</div>
										<div style="clear: both"></div>
									</div>
									<div class="col-12">
										<div style="font-size: 10px;margin-top: 5px;border-top: 1px solid #333;">
											<p class="p" style="margin-top: -10px;">ASSINADO ELETRONICAMENTE</p>
										</div>
									</div>
								</div>
							</div>

							{{-- <div class="novaPagina" style="margin-top:200px;margin-bottom:200px;"></div> --}}
							<div id="sec-3" class="texto" style="font-size:9pt;height:1204px !important; padding: 30px;">

								<div class="row">
									<div class="col-md-12">
										<table style="width: 100%; margin-top: 40px;" class="header">
											<tr>
												<td>
													<img src="{{ asset('/img/relatorios/logo_default.png') }}" alt="Upa - Estagio" />
												</td>
												<td>
													<h2 style="text-align: left;">
														<strong>PLANO DE ATIVIDADES</strong>
													</h2>
												</td>
											</tr>
										</table>
										<br>
										<table style="width: 100%; font-size:9pt; margin-top: 20px;" class="table table-bordered table-condensed ">
											<tr>
												<th colspan="3" style="text-align: center">
													<strong>DADOS DA CONCEDENTE</strong>
												</th>
											</tr>
											<tr>
												<td colspan="3">
													<strong>Empresa(Concedente):</strong><br>
													{{$tce->concedente->nmRazaoSocial}}
												</td>
											</tr>
											<tr>

												<td colspan="2">
													<strong>Nome do Supervisor(a) do Estágio na Concedente:</strong><br>
													@if($tce->supervisor_id != null)
														{{$tce->supervisor->nome}}
													@else
														{{$tce->nmSupervisor}}
													@endif

												</td>
												<td>
													<strong>Telefone:</strong><br>
													@if($tce->supervisor_id != null)
													{{$tce->supervisor->telefone}}
													@else
														<font color="#fff">e</font>
													@endif
													{{-- @if($tce->Concedente->dsFone){{$tce->Concedente->dsFone}}@else <font color="#fff">c</font> @endif --}}
												</td>
											</tr>
											<tr>
												<td colspan="2">
													<strong>E-mail:</strong><br>
													@if($tce->supervisor_id != null)
														{{$tce->supervisor->email}}
													@else
														<font color="#fff">e</font>
													@endif
												</td>
												<td>
													<strong>Cargo:</strong><br>
													{{--  @if($tce->supervisor_id != null)
														{{$tce->supervisor->cargo}}
													@else
														<font color="#fff">e</font>
                                                    @endif  --}}
                                                    SUPERVISOR(A)
												</td>
											</tr>
										</table>

										<table style="width: 100%;font-size:9pt; margin-top: 20px;" class="table table-bordered table-condensed ">
											<tr>
												<th colspan="3" style="text-align: center">
													<strong>DADOS DO ESTAGIARIO</strong>
												</th>
											</tr>
											<tr>
												<td colspan="2">
													<strong>Nome do Estagiario:</strong><br>
													{{$tce->estudante->nmEstudante}}
												</td>
												<td>
													<strong>Curso:</strong><br>
													{{$tce->cursoDaInstituicao->curso->nmCurso}}
												</td>
											</tr>
											<tr>
												<td colspan="3">
													<strong>Instituição de Ensino:</strong><br>
													{{$tce->instituicao->nmInstituicao}}
												</td>
											</tr>
										</table>

										<center>Vigência: <strong>{{$tce->dtInicio->format("d/m/Y")}}</strong> até <strong>{{$tce->dtFim->format("d/m/Y")}}</strong></center><br>

										<table style="width: 100%;font-size:9pt;" class="table table-bordered table-condensed ">
											<tr>
												<th colspan="3" style="text-align: center">
													<strong>ATIVIDADES A DESENVOLVER NO ESTÁGIO:</strong>
												</th>
											</tr>
											<tr>
												<td colspan="3">
													<ol class="margin">
														@foreach($tce->atividades as $atividade)
														<li>{{$atividade->atividade}}</li>
														@endforeach
													</ol>
													<strong>Assinatura do Supervisor (a) do Estágio da Empresa:</strong>
												</td>
											</tr>
										</table>

										<table style="width: 100%;font-size:9pt; margin-top: 20px;" class="table table-bordered table-condensed ">
											<tr>
												<th colspan="3" style="text-align: center">
													<strong>PARECER DO(A) ORIENTADOR(A) PEDAGÓGICO(A):</strong>
												</th>
											</tr>
											<tr>
												<td colspan="3">
													<hr style="font-size: 10px;margin-top: 20px; margin-bottom: 0px; border-bottom: 1px solid #333 ;" />
													<hr style="margin-top: 20px; margin-bottom: 0px; border-bottom: 1px solid #333 ;" />
													<hr style="margin-top: 20px; margin-bottom: 0px; border-bottom: 1px solid #333 ;" />
													<br>
													<strong>Assinatura do(a) Orientador(a) Pedagógico(a):</strong>
												</td>
											</tr>
										</table>
										<br>
										<div class="row">
											<div class="col-6">
												<p style="" style="margin-top: 5px;">Brasilia-DF, _____ de ______________ de _______.</p>
												@php
													$nascimento = $tce->estudante->dtNascimento;
													$nascimento->addYears(18)->toDateString();
													$hoje = \Carbon\Carbon::now()->toDateString();
													if($nascimento > $hoje){
														echo '<div style="font-size: 10px;margin-top: 45px;border-top: 1px solid #333;width:80%;">
															<p class="p" style="margin-top: 3px;">REPRESENTANTE LEGAL DO ESTAGIÁRIO / CPF</p>
														</div>';
													}
													@endphp
											</div>
											<div class="col-6">
												<div class="pull-right">
													<div style="font-size: 10px;margin-top: 20px;border-top: 1px solid #333;width:80%;float:right;">
														<p class="p" style="margin-top: 3px;">{{$tce->estudante->nmEstudante}}</p>
													</div>

													<div style="font-size: 10px;margin-top: 35px;border-top: 1px solid #333; width:80%;float:right;">
														<p class="p" style="margin-top: 3px;">{{$tce->instituicao->nmInstituicao}}</p>
													</div>
												</div>
											</div>
											<div style="clear: both"></div>
										</div>
										<div class="col-12">
											<div style="font-size: 10px;margin-top: 15px;border-top: 1px solid #333;">
												<p class="p" style="margin-top: -10px;">ASSINADO ELETRONICAMENTE - CONCEDENTE / SUPERVISOR(A)</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- -->

							<a href="" value="" style="display:none;" id="img-1" download="TCE @if($tce->migracao != NULL){{$tce->migracao}}@else{{$tce->id}}@endif - {{$tce->estudante->nmEstudante}}-part1.JPG">

							<a href="" value="" style="display:none;" id="img-2" download="TCE @if($tce->migracao != NULL){{$tce->migracao}}@else{{$tce->id}}@endif - {{$tce->estudante->nmEstudante}}-part2.JPG">

							<a href="" value="" style="display:none;" id="img-3" download="TCE @if($tce->migracao != NULL){{$tce->migracao}}@else{{$tce->id}}@endif - {{$tce->estudante->nmEstudante}}-part3.JPG">

							{{-- <a href="" value="" style="display:none;" id="img-4" download="TCE @if($tce->migracao != NULL){{$tce->migracao}}@else{{$tce->id}}@endif -part4.png"> --}}
						<!-- -->
					</div>
				</div>
			</div>
			<script src="{{ asset('js/app.js') }}" type="text/javascript"></script>
			<script src="{{ asset('js/CanvasHertzen.min.js') }}" type="text/javascript"></script>
			<script type="text/javascript">

			function download(){
				html2canvas(document.getElementById('sec-1')).then(function(canvas) {
					var c1 = document.getElementById('bodyCanva-1');
					document.getElementById('img-1').href = canvas.toDataURL();
					document.getElementById('img-1').click();
				});
				html2canvas(document.getElementById('sec-2')).then(function(canvas) {
					var c1 = document.getElementById('bodyCanva-2');
					document.getElementById('img-2').href = canvas.toDataURL();
					document.getElementById('img-2').click();
				});
				html2canvas(document.getElementById('sec-3')).then(function(canvas) {
					var c1 = document.getElementById('bodyCanva-3');
					document.getElementById('img-3').href = canvas.toDataURL();
					document.getElementById('img-3').click();
				});
				/*html2canvas(document.getElementById('sec-4')).then(function(canvas) {
					var c1 = document.getElementById('bodyCanva-4');
					document.getElementById('img-4').href = canvas.toDataURL();
					document.getElementById('img-4').click();
				});*/
			}

			</script>
		</body>
		</html>
