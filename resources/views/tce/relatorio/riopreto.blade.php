<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<title>Tce Rio Preto/SP - TCE 
		@if($tce->migracao != NULL) 
			{{$tce->migracao}}
		@else 
			{{$tce->id}}
		@endif
	</title>
	<style>
		html { 
			margin-top: 20px;
			margin-bottom: 20px;
			margin-left: 30px;
			margin-right: 30px;
		}
		body {
			font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif;
		}
		footer.fixar-rodape{
			bottom: 0;
			left: 0;
			height: 40px;
			position: fixed;
			width: 100%;
		  }
		.novaPagina {
			page-break-before: always;
			page-break-after: always
		}
		div.body-content{
			/** Essa margem vai evitar que o conteudo fique por baixo do rodapé **/
			margin-bottom: 40px;
		}

		.col-12 {
			width: 100% !important;
		}

		.col-6 {
			width: 50% !important;
		}

		.col-6,
		.col-12 {
			float: left !important;
		}

		.table {
			border-collapse: separate;
			border-spacing: 0;
			min-width: 350px;
			margin-bottom: 10px;
		}

		.table tr th,
		.table tr td {
			border-right: 1px solid #bbb;
			border-bottom: 1px solid #bbb;
			padding: 3px;
			
		}

		.table tr th:first-child,
		.table tr th:last-child {
			border-top: solid 1px #bbb;
		}

		.table tr th:first-child,
		.table tr td:first-child {
			border-left: 1px solid #bbb;
		}

		.table tr th:first-child,
		.table tr td:first-child {
			border-left: 1px solid #bbb;
		}

		.table tr th {
			background: #eee;
			text-align: left;
		}

		.table.Info tr th,
		.table.Info tr:first-child td {
			border-top: 1px solid #bbb;
		}
		/* top-left border-radius */

		.table tr:first-child th:first-child,
		.table.Info tr:first-child td:first-child {
			border-top-left-radius: 6px;
		}
		/* top-right border-radius */

		.table tr:first-child th:last-child,
		.table.Info tr:first-child td:last-child {
			border-top-right-radius: 6px;
		}
		/* bottom-left border-radius */

		.table tr:last-child td:first-child {
			border-bottom-left-radius: 6px;
		}
		/* bottom-right border-radius */

		.table tr:last-child td:last-child {
			border-bottom-right-radius: 6px;
		}
	</style>
</head>

<body>

	<div id="body">

		<div id="content">

			<div class="page" style="font-size: 7pt">
				<table style="width: 100%;" class="header">
					<tr>
						<td>
							<?php $image_path = '/img/relatorios/logo_default.png'; ?>
							<img src="{{ public_path() . $image_path }}" alt="Upa - Estagio" />
						</td>
						<td>
							<h1 style="text-align: right">
								<strong>TERMO DE COMPROMISSO DE ESTÁGIO<br>TCE N°
									@if($tce->migracao != NULL) 
										{{$tce->migracao}}
									@else 
										{{$tce->id}}
									@endif
								</strong>
							</h1>
						</td>
					</tr>
				</table>

				<div class="row">
					<div class="span12">
						<p style="margin: 10px;margin-top: -2px;">As partes abaixo identificadas celebram, entre si, o presente TERMO DE COMPROMISSO DE ESTÁGIO, com base no artigo 3º, II, da Lei 11.788/08, que mutuamente acordam e aceitam, a saber:</p>
					</div>
				</div>

				@if($tce->instituicao->poloMatriz != NULL)
				<!-- SE INSTITUIÇÃO TIVER POLO MATRIZ DEFINIDA EXIBE ENDEREÇO E DADOS DO POLO MATRIZ -->
				<!--INSTITUIÇÃO DE ENSINO-->
				<table style="width: 100%;" class="table table-bordered table-condensed ">
						<tr>
							<th colspan="2" style="text-align: center">
								<strong>DADOS DA INSTITUIÇÃO DE ENSINO</strong><span style="float: right;font-size: 6pt"> (Cod.: {{$tce->instituicao_id}})</span>
							</th>
						</tr>
						<tr>
							<td colspan="2">
								<strong>{{$tce->instituicao->poloMatriz->nmPolo}}({{$tce->instituicao->nmInstituicao}})</strong>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<strong>CNPJ:</strong> {{$tce->instituicao->poloMatriz->cdCNPJ}}
							</td>
						</tr>
						<tr>
							<td>
								<strong>ENDEREÇO: </strong>{{$tce->instituicao->poloMatriz->dsEndereco}} ,
								<strong>N°</strong> {{$tce->instituicao->poloMatriz->nnNumero}}
							</td>
							<td>
								<strong>CIDADE: </strong>{{$tce->instituicao->poloMatriz->cidade->nmCidade}} - {{$tce->instituicao->poloMatriz->cidade->estado->nmEstado}}</td>
						</tr>
						<tr>
							@if($tce->instituicao->poloMatriz->nmDiretor <> NULL)
							<td>
								<strong>REPRESENTANTE: </strong>{{$tce->instituicao->poloMatriz->nmDiretor}}
							</td>
							@else
							<td>
								<strong>REPRESENTANTE: </strong>{{$tce->instituicao->nmDiretor}}
							</td>
							@endif
							<td>
								<strong>TELEFONE: </strong> {{$tce->instituicao->poloMatriz->dsFone}}
							</td>
						</tr>
						<!--SE TIVER SIDO PREENCHIDO NMREITOR NO POLO-->
						@if($tce->instituicao->poloMatriz->nmReitor <> NULL)
							<tr>
								<td colspan="2">
									<strong>REITOR(A) MATRIZ: </strong>{{$tce->instituicao->poloMatriz->nmReitor}}
								</td>
							</tr>
						<!--SE NÂO TIVER PREENCHIDO NMREITOR NO POLO-->
						@else
							<!--SE TIVER PREENCHIDO NMREITOR NA INSTITUICAO-->
							@if($tce->instituicao->nmReitor <> NULL)
							<tr>
								<td colspan="2">
									<strong>REITOR(A): </strong>{{$tce->instituicao->nmReitor}}
								</td>
							</tr>
							@endif
						@endif

							@if(($tce->nmSupervisorIe) AND ($tce->dsSupervisorIeCargo) != NULL)
							<tr>
								<td>
									<strong>SUPERVISOR(A): </strong>{{$tce->nmSupervisorIe}}
								</td>
								<td>
									<strong>CARGO/FORMAÇÃO: </strong>{{$tce->dsSupervisorIeCargo}}
								</td>
							</tr>
							@else 
								@if(($tce->instituicao->nmSupervisor) AND ($tce->instituicao->nmSupervisorCargo))
								<tr>
									<td>
										<strong>SUPERVISOR(A): </strong>{{$tce->instituicao->nmSupervisor}}
									</td>
									<td>
										<strong>CARGO/FORMAÇÃO: </strong>{{$tce->instituicao->nmSupervisorCargo}}
									</td>
								</tr>
								@endif 
							@endif
				</table>
				@else
				<!-- FIM SE INSTITUIÇÃO NÃO TIVER POLO MATRIZ DEFINIDA EXIBE ENDEREÇO E DADOS DO POLO MATRIZ -->
				<!--INSTITUIÇÃO DE ENSINO-->
				<table style="width: 100%; margin-top: -5px;" class="table table-bordered table-condensed">
						<tr>
							<th colspan="4" style="text-align: center">
								<strong>DADOS DA INSTITUIÇÃO DE ENSINO</strong><span style="float: right;font-size: 6pt"> (Cod.: {{$tce->instituicao_id}})</span>
							</th>
						</tr>
						<tr>
							<td colspan="4">
								<strong>{{$tce->instituicao->nmInstituicao}}</strong>
							</td>
                        </tr>
                        <tr>
							<td colspan="4">
								<strong>NOME FANTASIA:</strong> {{$tce->instituicao->nmFantasia}}
							</td>
						</tr>
						<tr>
							<td colspan="4">
								<strong>CNPJ:</strong> {{$tce->instituicao->cdCNPJ}}
							</td>
						</tr>
						<tr>
							<td>
								<strong>ENDEREÇO: </strong>{{$tce->instituicao->dsEndereco}} ,
								<strong>N°</strong> {{$tce->instituicao->nnNumero}}
							</td>
							<td><strong>CEP: </strong>{{$tce->instituicao->cdCEP}}</td>
							<td colspan="2">
								<strong>CIDADE: </strong>{{$tce->instituicao->cidade->nmCidade}} - {{$tce->instituicao->cidade->estado->nmEstado}}
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<strong>REPRESENTANTE: </strong>{{$tce->instituicao->nmDiretor}}
							</td>
							<td colspan="2">
								<strong>TELEFONE: </strong> {{$tce->instituicao->dsFone}}
							</td>
						</tr>
						@if($tce->instituicao->nmReitor <> NULL)
							<tr>
								<td colspan="4">
									<strong>REITOR(A): </strong>{{$tce->instituicao->nmReitor}}
								</td>
							</tr>
						@endif 
							@if(($tce->nmSupervisorIe) AND ($tce->dsSupervisorIeCargo) != NULL)
							<tr>
								<td colspan="2">
									<strong>SUPERVISOR(A): </strong>{{$tce->nmSupervisorIe}}
								</td>
								<td colspan="2">
									<strong>CARGO/FORMAÇÃO: </strong>{{$tce->dsSupervisorIeCargo}}
								</td>
							</tr>
							@else 
								@if(($tce->instituicao->nmSupervisor) AND ($tce->instituicao->nmSupervisorCargo))
								<tr>
									<td colspan="2">
										<strong>SUPERVISOR(A): </strong>{{$tce->instituicao->nmSupervisor}}
									</td>
									<td colspan="2">
										<strong>CARGO/FORMAÇÃO: </strong>{{$tce->instituicao->nmSupervisorCargo}}
									</td>
								</tr>
								@endif 
							@endif
				</table>
				@endif
				
				<!--CONCEDENTE-->
				<table style="width: 100%; margin-top: -5px;" class="table table-bordered table-condensed">
					<tbody>
						<tr>
							<th colspan="3" style="text-align: center;">
								<strong>DADOS DA CONCEDENTE</strong><span style="float: right;font-size: 6pt"> (Cod.: {{$tce->concedente_id}})</span>
							</th>
						</tr>
						<tr>
							<td colspan="3">
								<strong>LOCAL DE REALIZAÇÃO DE ESTÁGIO:</strong> {{$tce->concedente->nmRazaoSocial}}
							</td>
						</tr>
						<!--SE O TCE FOR DE UMA SECRETARIA DA CONCEDENTE - até linha 373-->
						@if($tce->sec_conc_id <> NULL)
							<tr>
								<td colspan="3">
									<strong>SECRETARIA:</strong> {{$tce->secConcedente->nmSecretaria}} <span style="float: right;font-size: 5pt"> (Cod.: {{$tce->concedente_id}}/{{$tce->secConcedente->id}})</span>
								</td>
							</tr>
							<tr>
								<td colspan="3">
									<strong>CNPJ: </strong>{{$tce->secConcedente->cdCnpjCpf}}
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<strong>ENDEREÇO: </strong> {{$tce->secConcedente->dsEndereco}} , 
									<strong>N°</strong> {{$tce->secConcedente->nnNumero}}
								</td>
								<td>
									@if($tce->secConcedente->nmBairro)<strong>BAIRRO: </strong>{{$tce->secConcedente->nmBairro}}@endif
								</td>
							</tr>
							@if($tce->secConcedente->dsComplemento)
							<tr>
								<td colspan="3">
									<strong>COMPLEMENTO: </strong> {{$tce->secConcedente->dsComplemento}}
								</td>
							</tr>
							@endif
							<tr>
								<td colspan="2">
									<strong>CIDADE: </strong>{{$tce->concedente->cidade->nmCidade}} - {{$tce->concedente->cidade->estado->nmEstado}}
								</td>
								<td>
									<strong>CEP: </strong>{{$tce->secConcedente->cdCEP}}
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<strong>TELEFONE: </strong>{{$tce->secConcedente->dsFone}}
								</td>
								<td>
									<strong>OUTRO: </strong> @if  ( $tce->secConcedente->dsOutroFone != NULL) {{$tce->secConcedente->dsOutroFone}} @else - @endif
								</td>
							</tr>
                            @if(($tce->secConcedente->nmResponsavel) AND ($tce->secConcedente->dsResponsavelCargo) != NULL)
                            <tr>
                                <td colspan="2">
                                    <strong>REPRESENTADO PELO SR(A): </strong>{{$tce->secConcedente->nmResponsavel}}
                                </td>
                                <td>
                                    <strong>CARGO: </strong>{{$tce->secConcedente->dsResponsavelCargo}}
                                </td>
                            </tr>
							@endif
							@if(($tce->nmSupervisor) AND ($tce->dsSupervisorCargo) != NULL)
							<tr>
								<td colspan="2">
									<em><strong>SUPERVISOR(A): </strong>{{$tce->nmSupervisor}}</em>
								</td>
								<td>
									<em><strong>CARGO/FORMAÇÃO: </strong>{{$tce->dsSupervisorCargo}}</em>
								</td>
							</tr>						
                            @endif
						<!--SE NÂO FOR DE SECRETARIA EXIBE OS DADOS DA CONCEDENTE-->
						@else
							@if($tce->concedente->flTipo == "PF")
							<tr>
								<td colspan="2">
									<strong>CPF: </strong>{{$tce->concedente->cdCnpjCpf}}
								</td>
								<td>
									<strong>ORGÃO REGULADOR: </strong>{{$tce->concedente->dsOrgaoRegulador}}
								</td>
							</tr>
							@else
							<tr>
								<td colspan="3">
									<strong>CNPJ: </strong>{{$tce->concedente->cdCnpjCpf}}
								</td>
							</tr>
							@endif
							<tr>
								<td colspan="2">
									<strong>ENDEREÇO: </strong> {{$tce->concedente->dsEndereco}} , 
									<strong>N°</strong> {{$tce->concedente->nnNumero}}
								</td>
								<td>
									@if($tce->concedente->nmBairro)<strong>BAIRRO: </strong>{{$tce->concedente->nmBairro}}@endif
								</td>
							</tr>
							@if($tce->concedente->dsComplemento)
							<tr>
								<td colspan="3">
									<strong>COMPLEMENTO: </strong> {{$tce->concedente->dsComplemento}}
								</td>
							</tr>
							@endif
							<tr>
								<td colspan="2">
									<strong>CIDADE: </strong>{{$tce->concedente->cidade->nmCidade}} - {{$tce->concedente->cidade->estado->nmEstado}}
								</td>
								<td>
									<strong>CEP: </strong>{{$tce->concedente->cdCEP}}
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<strong>TELEFONE: </strong>{{$tce->concedente->dsFone}}
								</td>
								<td>
									<strong>OUTRO: </strong> @if  ( $tce->concedente->dsOutroFone != NULL) {{$tce->concedente->dsOutroFone}} @else - @endif
								</td>
							</tr>
							<!--mostrar resp da concedente no tce-->
							@if(($tce->concedente->nmResponsavel) AND ($tce->concedente->dsResponsavelCargo) != NULL)
							<tr>
								<td colspan="2">
									<strong>REPRESENTADO PELO SR(A): </strong>{{$tce->concedente->nmResponsavel}}
								</td>
								<td>
									<strong>CARGO: </strong>{{$tce->concedente->dsResponsavelCargo}}
								</td>
							</tr> 
							@endif
							<!--mostrar resp da concedente no tce fim-->
							@if(($tce->nmSupervisor) AND ($tce->dsSupervisorCargo) != NULL)
							<tr>
								<td colspan="2">
									<strong>SUPERVISOR(A): </strong>{{$tce->nmSupervisor}}
								</td>
								<td>
									<strong>CARGO/FORMAÇÃO: </strong>{{$tce->dsSupervisorCargo}}
								</td>
							</tr> 
							@endif
				
						<!--ABAIXO FIM DO IF SE É SECRETARIA DE CONCEDENTE-->
						@endif
						@if($tce->dsLotacao)
						<tr>
							<td colspan="3">
								<strong>LOTAÇÃO:</strong> {{$tce->dsLotacao}}
							</td>
						</tr>
						@endif
					</tbody>
				</table>

				<!--ESTAGIARIO-->
				<table style="width: 100%;margin-top: -5px;" class="table table-bordered table-condensed">
					<tr>
						<th colspan="3" style="text-align: center">
							<strong>DADOS DO ESTÁGIARIO </strong><span style="float: right;font-size: 6pt">(Cod.: {{$tce->estudante_id}})</span>
						</th>
					</tr>
					<tr>
						<td colspan="">
							<strong>NOME: </strong>{{$tce->estudante->nmEstudante}}
						</td>
						<td>
							<strong>DATA NASC: </strong>{{$tce->estudante->dtNascimento->format('d/m/Y')}}
						</td>
						<td>
							<strong>CPF: </strong>{{$tce->estudante->cdCPF}}
						</td>
					</tr>
					<tr>
						<td>
							<strong>RG: </strong>{{$tce->estudante->cdRG}} 
						</td>
						<td>
                            <strong>ORG.: </strong>{{$tce->estudante->dsOrgaoRG}}
						</td>
						<td>
							@if($tce->estudante->dsEmail)<strong>EMAIL: </strong>{{$tce->estudante->dsEmail}} @endif
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<strong>ENDEREÇO: </strong> {{$tce->estudante->dsEndereco}},
							<strong> N°</strong> {{$tce->estudante->nnNumero}}
						</td>
						<td>
							<strong>BAIRRO: </strong>{{$tce->estudante->nmBairro}}
						</td>
					</tr>
					@if($tce->estudante->dsComplemento)
					<tr>
						<td colspan="3">
							<strong>COMPLEMENTO: </strong> {{$tce->estudante->dsComplemento}}
						</td>
					</tr>
					@endif
					<tr>
						<td colspan="1">
							<strong>CIDADE: </strong>{{$tce->estudante->cidade->nmCidade}} - {{$tce->estudante->cidade->estado->nmEstado}}
						</td>
						<td>
							<strong>CEP: </strong>{{$tce->estudante->cdCEP}}
						</td>
						<td>
							@if($tce->estudante->dsFone)<strong>TELEFONE: </strong>{{$tce->estudante->dsFone}}@endif
						</td>
					</tr>
					<tr>
						<td>
							<strong>CURSO: </strong> {{$tce->cursoDaInstituicao->curso->nmCurso}}
                        </td>
                        <!--semestre ou ano-->
						<td>
							<strong>{{$tce->cursoDaInstituicao->cursoDuracao->nmTipoDuracao}}: </strong>
							{{$tce->nnSemestreAno}} {{$tce->cursoDaInstituicao->cursoDuracao->nmTipoDuracao}}
                         </td>
                         <!--fim semestre ou ano-->
						<td>
							<strong>TURNO: </strong>{{$tce->estudante->turno->nmTurno}} 
						</td>
					</tr>
				</table>

				<div class="texto">
                        <div class="row" >
                                <p style="margin-top: -2px;">Celebram entre si o TERMO DE COMPROMISSO DE ESTÁGIO, convencionando as cláusulas e condições seguintes:</p>
                        </div>
                        
						<div class="row" id="cl1">
							<div class="col-sm-12" style="text-align: justify">
								<p style="text-align: justify;margin-top: -2px;" class="termo">
									<strong>CLÁUSULA PRIMEIRA – DO OBJETIVO –</strong> Este contrato de estágio tem por objetivo formalizar as condições básicas para a realização do estágio, junto a parte concedente do estágio, o qual, obrigatório ou não, deve ser de interesse curricular e pedagogicamente útil, e entendido como uma estratégia de profissionalização que integra o processo de ensino-aprendizagem, objetivando o desenvolvimento do educando para a vida cidadã e para o trabalho, como atendimento ao requisito previsto no artigo 3º, II, da lei 11.788, de 25 de setembro de 2008. 
								</p>
							</div>
						</div>
						
                        <div class="row" id="cl2">
                            <p style="text-align: justify;margin-top: -3px;" class="termo">
                                <strong>CLÁUSULA SEGUNDA – DO SEGURO –</strong> O Educando Estagiário será incluído na apólice de seguro de acidentes  pessoais conforme <strong>Apólice Coletiva de n°. 9.850-3 da Porto Seguro Cia de Seguros Gerais a cargo da Universidade patativa do Assaré - UPA</strong>. Em caso de acidente entrar em contato com a UPA imediatamente pelo telefone (88) 3512 2450.
							</p>
                        </div>
						
                        <div class="row" id="cl3">
                            <p style="text-align: justify;margin-top: -2px;" class="termo">
                                <strong>CLÁUSULA TERCEIRA – DO VÍNCULO -</strong> O Educando estagiário não terá, para quaisquer efeito, vínculo empregatício com a Parte Concedente do Estágio, competindo-lhe respeitar as normas internas e principalmente as que dizem respeito ao estágio.
							</p>
						</div>
						
						<div class="row" id="cl4">
								<div class="col-sm-12" style="text-align: justify;margin-top: -2px;">
									<strong>CLÁUSULA QUARTA – DAS OBRIGAÇÕES DA PARTE CONCEDENTE DO ESTÁGIO -</strong>  No desenvolvimento do estágio, caberá à parte Concedente do estágio:
                                    <ol type="a" style="margin-top: -2px;">
                                        <li style="list-style-type: upper-alpha">Proporcionar à Instituição de Ensino e à agente – UPA , aqui anuente, as condições de conhecimento das informações que mantenham os dados cadastrais atualizados, possibilitando o acompanhamento e a supervisão do ESTÁGIO;</li>
                                        <li style="list-style-type: upper-alpha">Pagar ao educando estagiário, valor a título de bolsa de complementação educacional: Bolsa-Auxílio mensal de:  <strong>R${{number_format($tce->vlAuxilioMensal, 2, ',', '.')}}</strong> ({{Extenso::Converter($tce->vlAuxilioMensal)}})
											@if (isset($tce->vlAuxilioAlimentacao) && !empty($tce->vlAuxilioAlimentacao) && !is_null($tce->vlAuxilioAlimentacao))
												@if ($tce->vlAuxilioTransporte)
												,  Auxílio-Alimentação de <strong>R${{number_format($tce->vlAuxilioAlimentacao, 2, ',', '.')}}</strong> ({{Extenso::Converter($tce->vlAuxilioAlimentacao)}})
												@else
												e  Auxílio-Alimentação de <strong>R${{number_format($tce->vlAuxilioAlimentacao, 2, ',', '.')}}</strong> ({{Extenso::Converter($tce->vlAuxilioAlimentacao)}})
												@endif
											@endif
											@if (isset($tce->vlAuxilioTransporte) && !empty($tce->vlAuxilioTransporte) && !is_null($tce->vlAuxilioTransporte))
												@if ($tce->vlAuxilioAlimentacao)
												e Auxílio-Transporte de <strong>R${{number_format($tce->vlAuxilioTransporte, 2, ',', '.')}}</strong> ({{Extenso::Converter($tce->vlAuxilioTransporte)}})
												@elseif(isset($tce->vlAuxilioAlimentacao) && isset($tce->vlAuxilioTransporte))
												e Auxílio-Transporte de <strong>R${{number_format($tce->vlAuxilioTransporte, 2, ',', '.')}}</strong> ({{Extenso::Converter($tce->vlAuxilioTransporte)}})
												@else
												e Auxílio-Transporte de <strong>R${{number_format($tce->vlAuxilioTransporte, 2, ',', '.')}}</strong> ({{Extenso::Converter($tce->vlAuxilioTransporte)}})
												@endif
											@endif
											@php
												$total = $tce->vlAuxilioMensal+$tce->vlAuxilioAlimentacao+$tce->vlAuxilioTransporte;
											@endphp
											@if(isset($tce->vlAuxilioAlimentacao) OR isset($tce->vlAuxilioTransporte))
											totalizando <strong>R${{number_format($total, 2, ',', '.')}}</strong>({{Extenso::Converter($total)}})
											@endif
                                            , sobre os quais incidirão os percentuais de reajustes dos servidores da Prefeitura, quando houver;</li>
                                        <li style="list-style-type: upper-alpha">Conceder ao estagiário, após um ano de estágio, recesso remunerado de 30 dias, para gozo preferencialmente  no período de recesso escolar, ou recesso remunerado proporcional , no caso de estágio inferior ao um ano;
                                        <li style="list-style-type: upper-alpha">Cumprir o disposto no artigo 9º da Lei, firmado com a  UPA, ora Anuente;
                                        <li style="list-style-type: upper-alpha">Proporcionar ao Educando Estagiário as atividades de aprendizagem social , profissional e cultural, através sua participação em situações reais de vida e trabalho de seu meio, objetivando seu desenvolvimento para a vida cidadã e para o trabalho, em atendimento aos objetivos consagrados na Lei nº. 11.788/08.
                                    </ol>
                                </div>
                        </div>

                        <div class="row" id="cl4">
                            <div class="col-sm-12" style="text-align: justify; margin-top: -2px;">
                                <strong>CLÁUSULA QUINTA – DAS OBRIGAÇÕES DO ESTAGIÁRIO -</strong> Compete ao Educando estagiário:
								<p>
								<ol type="a" class="margin justi" style="margin-top: -2px;">
                                    <li style="list-style-type: upper-alpha"> Cumprir o horário e toda programação estabelecida para seu ESTÁGIO;</li>
                                    <li style="list-style-type: upper-alpha"> Desenvolver, durante o período as seguintes atividades:</li>
									<ol>
                                        @foreach($tce->atividades as $atividade)	
                                            <li>{{$atividade->atividade}}</li>
                                        @endforeach
										</ol>
                                    <li style="list-style-type: upper-alpha"> Preencher, obrigatoriamente, o relatório de acompanhamento de estágio, quando solicitado.</li>
                                    <li style="list-style-type: upper-alpha"> Informar de imediato e por escrito, à parte Concedente e a UPA, a ocorrência de interrupção, suspensão ou cancelamento de sua matricula junto a INSTITUIÇÃO DE  ENSINO;</li>
                                    <li style="list-style-type: upper-alpha"> Manter atualizado, junto a UPA, seus dados cadastrais e escolares;</li>
                                    <li style="list-style-type: upper-alpha"> Encaminhar obrigatoriamente, à Instituição de ensino, à parte Concedente do Estágio e à Agente , UPA , uma via do presente instrumento , devidamente assinado;</li>
                                    <li style="list-style-type: upper-alpha"> Respeitar as normas internas da Parte Concedente do Estágio e, principalmente, as que dizem respeito ao estágio.</li>
                                    <li style="list-style-type: upper-alpha"> facultativamente inscrever-se e contribuir como segurado do Regime Geral de Previdência Social.</li> 
								</ol>
								</p>
                            </div>
                        </div>
						<br>
                        <div class="row">
                            <div class="col-sm-12" style="text-align: justify; margin-top: -2px;">
                                <strong>CLÁUSULA  SEXTA – DAS OBRIGAÇÕES DA UPA -</strong>  Compete a UPA:
                                <ol class="margin justi" type="a">
                                    <li style="list-style-type: upper-alpha"> Disponibilizar ao Educando estagiário e à Parte Concedente do Estagio, o Relatório de Acompanhamento de Estágio periodicamente;</li>
                                    <li style="list-style-type: upper-alpha"> Notificar a Parte Concedente do Estágio e o Educando Estagiário de rescisão do presente instrumento , quando solicitada pela Instituição de ensino;</li>
                                    <li style="list-style-type: upper-alpha"> Notificar a Parte Concedente do estágio, e o Educando Estagiário e a Instituição de Ensino, a cessação de suas responsabilidade legais, técnicas ou administrativas, inclusive quanto ao Seguro de Acidentes Pessoais dos Educados Estagiários, caso identifique violação dos compromissos aqui assumidos por quaisquer partes.</li>
                                </ol>
                            </div>
                        </div>
						<br>
                        <div class="row">
                            <div class="col-sm-12" style="text-align: justify; margin-top: -2px;">
                                <strong>CLAÚSULA SÉTIMA – DA RESCISÃO DO CONTRATO -</strong> Constitui motivo para interrupção automática a rescisão  do presente Termo de Compromisso de Estágio ,  independente de qualquer  notificação:                                                                                                                                                                                                                        
                                <ol class="margin justi" type="a">
                                    <li style="list-style-type: upper-alpha"> A conclusão de período de estágio;</li>
                                    <li style="list-style-type: upper-alpha"> O  abandono do estágio, pelo educando, bem como o descumprimento de qualquer outra cláusula deste termo de compromisso de estágio;</li>
                                </ol>
                                <p style="text-align: justify" class="termo">
                                    <strong>PARAGRAFO ÚNICO -</strong> A rescisão poderá ocorrer por vontade unilateral de qualquer das partes, a qualquer momento, desde que por comunicação escrita , sem prejuízo de parte a parte.                                                                                                           
                                </p>
                            </div>
                        </div>
						<br>
                        <div class="row">
                            <div class="col-sm-12" style="text-align: justify; margin-top: -2px;">
                            <strong>CLAÚSULA OITAVA – DO PRAZO DA VIGENCIA –</strong> O presente Termo de Compromisso de Estágio terá a vigência de: <strong>{{$tce->dtInicio->format("d/m/Y")}}</strong> até <strong>{{$tce->dtFim->format("d/m/Y")}}</strong>. Não podendo exceder 2(dois) anos;
                            </div>
                        </div>
						<br>
                        <div class="row">
                            <div class="col-sm-12" style="text-align: justify; margin-top: -2px;">
                                <strong>CLAÚSULA NONA – DO HORARIO DO ESTÁGIO</strong>
                                <ol class="margin justi" type="a">
                                    @if(substr($tce->periodo->dsPeriodo, -6) == "SENDO:")
                                        @if($tce->hrInicio != NULL)
                                            <li style="list-style-type: upper-alpha">A carga horária do estágio é de <strong>{{$tce->periodo->dsPeriodo}} {{$tce->dsPeriodo}} de {{substr($tce->hrInicio, 0, 5)}} as {{substr($tce->hrFim, 0, 5)}}</strong> e compatível com as atividades escolares.</li>
                                        @else
											<li style="list-style-type: upper-alpha">A carga horária do estágio é de <strong>{{$tce->periodo->dsPeriodo}} {{$tce->dsPeriodo}}</strong> e compatível com as atividades escolares.</li>
                                        @endif
                                    @else
										<li style="list-style-type: upper-alpha">A carga horária do estágio é <strong>{{$tce->periodo->dsPeriodo}} 
											@if($tce->hrInicio != NULL)
											de {{substr($tce->hrInicio, 0, 5)}} as {{substr($tce->hrFim, 0, 5)}}
											@endif
										</strong>
											{{-- @if($tce->dsPeriodo != NULL OR $tce->dsPeriodo != "") {{$tce->dsPeriodo}}, @endif --}}
											e compatível com as atividades escolares.</li>
                                    @endif            
                                    <li style="list-style-type: upper-alpha"> A carga horária do estágio poderá ser reduzida durante o período de avaliações escolares.</li>
                                </ol>                     
                                <p style="text-align: justify" class="termo">
                                    <strong> PARAGRAFO ÚNICO -</strong> NÃO PODENDO HAVER COMPENSAÇÃO DE HORÁRIO.
                                </p>
                            </div>
                        </div>
						<br>
                        <div class="row">
                            <div class="col-sm-12" style="text-align: justify; margin-top: -2px;">
                                <strong>CLAÚSULA DECIMA – DA FISCALIZAÇÃO -</strong> As partes reconhecem que a fiscalização do estágio será exercida conforme o Ofício Circular SRT nº .11/85 de 09 de setembro de 1985 e alterações da SRT nº. 008/87 de 29 de julho de 1987, que dispõem sobre instruções para a fiscalização de estágio.
                            </div>
                        </div>
						<br>
                        <div class="row">
                            <div class="col-sm-12" style="text-align: justify; margin-top: -2px;">
                                <strong>CLAÚSULA DECIMA PRIMEIRA -</strong> Este termo, decorrente da migração do Agente de Integração, passa a ter seus efeitos retroagidos a 21/05/2017, respeitada a data de vigência originalmente fixada.
                            </div>
                        </div>
						<br>
                        <div class="row">
                            <div class="col-sm-12" style="text-align: justify; margin-top: -2px;">
                                <strong>CLAÚSULA DECIMA SEGUNDA – DO FORO -</strong> As partes elegem o foro da Comarca de São José do Rio Preto – SP , para dirimir qualquer dúvida oriunda deste Termo de Compromisso de Estágio, que não seja ser resolvida amigavelmente.
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-12" style="text-align: justify; margin-top: -2px;">
                            <p>E, por estarem de inteiro e comum acordo com o Plano de Atividades de Estágio abaixo descrito e com as demais condições estabelecidas neste TERMO DE COMPROMISSO DE ESTÁGIO-TCE, as partes assinam em 4 vias de igual teor.</p>
                            </div>
                        </div>
                        <div class="row body-content">
                            <br><br>
							<div class="span12">
								{{-- <p style="margin: 10px; float: right;">___________________, _____ de ______________ de _______.</p> --}}
                                <p style="margin: 10px; float: right;"> ___________________________, {{$tce->created_at->formatLocalized('%d de %B de %Y')}} </p>
                            </div>
						</div>
						
						<div class="col-6">
								<div style="font-size: 10px;margin-top: 50px;border-top: 1px solid #333;width:80%;">
									<p class="p">Concedente de Estágio<br>(Assinatura e Carimbo)</p>
								</div>
								<div style="font-size: 10px;margin-top: 60px;border-top: 1px solid #333;width:80%;">
										<p class="p">Estagiário(a)<br>(Assinatura)</p>
								</div>
								@php
								$nascimento = $tce->estudante->dtNascimento;
								$nascimento->addYears(18)->toDateString();   
								$hoje = \Carbon\Carbon::now()->toDateString();
								if($nascimento > $hoje){
									echo '<div style="font-size: 10px;margin-top: 60px;border-top: 1px solid #333; width:80%;">
											<p class="p">REPRESENTANTE LEGAL DO ESTAGIÁRIO / CPF</p>
										</div>';
								}
                                @endphp
                            </div>
                            <div class="col-6">
								<div style="font-size: 10px;margin-top: 50px;border-top: 1px solid #333;">
									<p class="p">Instituição de Ensino<br>(Assinatura e Carimbo)</p>
								</div>
								<div style="font-size: 10px;margin-top: 50px;border-top: 1px solid #333;">
									<p class="p">Universidade Patativa do Assaré - UPA<br>(Assinatura e Carimbo)</p>
								</div>		
							</div>
							<div style="clear: both"></div>
					
                    </div>
						@include('tce.relatorio.inc_rodape')
					</div>
			</div>

		</div>
	</div>
</body>
</html>