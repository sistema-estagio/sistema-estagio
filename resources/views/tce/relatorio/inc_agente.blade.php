<!--INSTITUIÇÃO DE ENSINO-->
<table style="width: 100%;" class="table table-bordered table-condensed ">
        <tr>
            <th colspan="2" style="text-align: center">
                <strong>DADOS DO AGENTE DE INTEGRAÇÃO</strong><span style="float: right;font-size: 5pt"></span>
            </th>
        </tr>
        <tr>
            <td>
                <strong>UNIVERSIDADE PATATIVA DO ASSARÉ</strong>
            </td>
            <td>
                <strong>CNPJ:</strong> 05.342.580/0001-19
            </td>
        </tr>
        <tr>
            <td>
                <strong>ENDEREÇO: </strong>RUA MONSENHOR ESMERALDO,
                <strong>N°</strong> 36
            </td>
            <td>
                <strong>CIDADE: </strong>JUAZEIRO DO NORTE - CEARÁ</td>
        </tr>
        <tr>
            <td>
                <strong>REPRESENTANTE: </strong>FRANCISCO PALACIO LEITE
            </td>
            <td>
                <strong>CARGO: </strong> DIRETOR PRESIDENTE
            </td>
        </tr>
    </table>