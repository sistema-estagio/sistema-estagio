<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<title>Reatorio Tce 1</title>

	<!-- Bootstrap 4-ONLINE -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb"
	 crossorigin="anonymous" media="all">
	<style>
		html { margin: 10px}
		body {
			margin: 0px 50px !important;
			font-size: 11px;
		}
		
        #rodape {
        height: 50px; /*ajusta a altura do rodapé*/
        margin-top: -50px;/*estes números devem ser opostos*/
        }

		.novaPagina { page-break-before:always; page-break-after:always }

		[class*="texto"] {
			font-size: 10px;
		}

		@media print {

  [class*="col-sm-sm-"] {
    float: left;
  }

  [class*="col-sm-xs-"] {
    float: left;
  }

  .col-sm-sm-12, .col-sm-xs-12 {
    width:100% !important;
  }

  .col-sm-sm-11, .col-sm-xs-11 {
    width:91.66666667% !important;
  }

  .col-sm-sm-10, .col-sm-xs-10 {
    width:83.33333333% !important;
  }

  .col-sm-sm-9, .col-sm-xs-9 {
    width:75% !important;
  }

  .col-sm-sm-8, .col-sm-xs-8 {
    width:66.66666667% !important;
  }

  .col-sm-sm-7, .col-sm-xs-7 {
    width:58.33333333% !important;
  }

  .col-sm-sm-6, .col-sm-xs-6 {
    width:50% !important;
  }

  .col-sm-sm-5, .col-sm-xs-5 {
    width:41.66666667% !important;
  }

  .col-sm-sm-4, .col-sm-xs-4 {
    width:33.33333333% !important;
  }

  .col-sm-sm-3, .col-sm-xs-3 {
    width:25% !important;
  }

  .col-sm-sm-2, .col-sm-xs-2 {
    width:16.66666667% !important;
  }

  .col-sm-sm-1, .col-sm-xs-1 {
    width:8.33333333% !important;
  }

  .col-sm-sm-1,
  .col-sm-sm-2,
  .col-sm-sm-3,
  .col-sm-sm-4,
  .col-sm-sm-5,
  .col-sm-sm-6,
  .col-sm-sm-7,
  .col-sm-sm-8,
  .col-sm-sm-9,
  .col-sm-sm-10,
  .col-sm-sm-11,
  .col-sm-sm-12,
  .col-sm-xs-1,
  .col-sm-xs-2,
  .col-sm-xs-3,
  .col-sm-xs-4,
  .col-sm-xs-5,
  .col-sm-xs-6,
  .col-sm-xs-7,
  .col-sm-xs-8,
  .col-sm-xs-9,
  .col-sm-xs-10,
  .col-sm-xs-11,
  .col-sm-xs-12 {
  float: left !important;
  }

  body {
    margin: 0;
    padding: 0 !important;
    min-width: 768px;
  }

  .container {
    width: auto;
    min-width: 750px;
  }

  body {
    font-size: 11px;
  }

  a[href]:after {
    content: none;
  }

  .noprint,
  div.alert,
  header,
  .group-media,
  .btn,
  .footer,
  form,
  #comments,
  .nav,
  ul.links.list-inline,
  ul.action-links {
    display:none !important;
  }
}
		
	</style>
</head>

<body>
	<div class="wrapper">
		<div class="row">
			<div class="col-sm-4">
				<img src="{{URL::asset('img/relatorios/logo_default.png')}}" alt="Upa - Estagio" />        </div>
			<div class="col-sm-8">
				
					<h6 style="text-align: right"><strong>TERMO DE COMPROMISSO DE ESTÁGIO - TCE 6960</strong></h6>
					<span style="text-decoration: underline; text-align: right "><p><strong><br>CONTRATO NOVACAP N° 083/2017
					<br>(PROCESSO COMPANHIA URBANIZADORA DA NOVA CAPITAL DO BRASIL N°112.004.160/2016)
					</strong></p>
				</span>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">
				<p style="margin: 10px;text-align: center">As partes abaixo qualificadas celebram neste ato Termo de Compromisso de Estágio, conforme a Lei n°. 11.788/08.</p>
			</div>
		</div>
			
            <!--INSTITUIÇÃO DE ENSINO-->
			<div class="card">
				<h6 style="text-align: center; line-height: 0px;" class="card-header"><strong>DADOS DA INSTITUIÇÃO DE ENSINO</strong></h6>
				<div class="card-block">
					<div class="row">
						<div class="col-sm-12">
							<strong>CENTRO DE EDUCAÇÃO PROFISSIONAL-ESCOLA TÉCNICA DE SAÚDE DE PLANALTINA</strong>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<strong>CNPJ: </strong>00.394.676/0001-07
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<strong>ENDEREÇO: </strong>ENTRE AVENIDAS CONTORNO E INDEPENDÊNCIA , N° S/N
						</div>
						<div class="col-sm-6">
							<strong>CIDADE: </strong>BRASILIA - DISTRITO FEDERAL
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<strong>REPRESENTANTE: </strong>REPRESENTANTE DA INSTITUIÇÃO
						</div>
						<div class="col-sm-6">
							<strong>TELEFONE: </strong>(DDD)0000-0000
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<strong>SUPERVISOR(A): </strong>SUPERVISOR
						</div>
						<div class="col-sm-6">
							<strong>CARGO/FORMAÇÃO: </strong>CARGO OU FORMAÇÃO DO SUPERVISOR
						</div>
					</div>
				</div>
			</div>
            <!--ESTAGIARIOS-->
			<div class="card">
				<h6 style="text-align: center; line-height: 0px;" class="card-header"><strong>DADOS DO ESTÁGIARIO</strong></h6>
				<div class="card-block">
					<div class="row">
						<div class="col-sm-6">
							<strong>NOME: </strong>NOME DO ESTAGIARIO
						</div>
						<div class="col-sm-3">
							<strong>NASCIMENTO: </strong>dd/mm/aaaa
						</div>
						<div class="col-sm-3">
							<strong>CPF: </strong>000.000.000-00
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<strong>RG: </strong>0000000000
						</div>
						<div class="col-sm-3">
							<strong>ORG: </strong>SSP
						</div>
						<div class="col-sm-3">
							<strong>E-MAIL: </strong>fulano@algo.com
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<strong>ENDEREÇO: </strong>RUA XYZ N°xxx
						</div>
						<div class="col-sm-3">
							<strong>BAIRRO: </strong>BAIRRO
						</div>
						<div class="col-sm-3">
							<strong>CEP: </strong>00000-000
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<strong>CIDADE: </strong> NOME CIDADE - ESTADO
						</div>
						<div class="col-sm-6">
							<strong>TELEFONE: </strong>(ddd)0000-0000
						</div>
					</div>
					<div class="row">
						<div class="col-sm-4">
							<strong>CURSO: </strong> NOME DO CURSO
						</div>
						<div class="col-sm-4">
							<strong>SEMESTRE/ANO/MODULO: </strong>1 SEMESTRE
						</div>
						<div class="col-sm-4">
							<strong>TURNO: </strong>NOITE
						</div>
					</div>
				</div>
			</div>
			<!--CONCEDENTE-->
			<div class="card">
				<h6 style="text-align: center; line-height: 0px;" class="card-header"><strong>DADOS DA CONCEDENTE</strong></h6>
				<div class="card-block">
					<div class="row">
						<div class="col-sm-12">
							<strong>LOCAL DE REALIZAÇÃO DE ESTÁGIO: </strong>CENTRO DE EDUCAÇÃO PROFISSIONAL-ESCOLA TÉCNICA DE SAÚDE DE PLANALTINA
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<strong>CNPJ: </strong>00.394.676/0001-07
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<strong>ENDEREÇO: </strong>ENTRE AVENIDAS CONTORNO E INDEPENDÊNCIA , N° S/N
						</div>
						<div class="col-sm-6">
							<strong>BAIRRO: </strong>NOME DO BAIRRO
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<strong>CIDADE: </strong>CIDADE - ESTADO
						</div>
						<div class="col-sm-6">
							<strong>CEP: </strong>00000-000
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<strong>RESPONSAVEL: </strong>NOME DO RESPONSAVEL
						</div>
						<div class="col-sm-6">
							<strong>CARGO: </strong>CARGO DO RESPONSAVEL
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<strong>SUPERVISOR(A): </strong>NOME DO(A) SUPERVISOR(A)
						</div>
						<div class="col-sm-6">
							<strong>CARGO: </strong>CARGO DO(A) SUPERVISOR(A)
						</div>
					</div>
				</div>
			</div>
			<!--AGENTE DE INTEGRAÇÃO-->
			<div class="card">
				<h6 style="text-align: center; line-height: 0px;" class="card-header"><strong>DADOS DO AGENTE DE INTEGRAÇÃO</strong></h6>
				<div class="card-block">
					<div class="row">
						<div class="col-sm-12">
							<strong>UNIVERSIDADE PATATIVA DO ASSARÉ</strong>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<strong>CNPJ: </strong> 05.342.580/0001-19
						</div>
						<div class="col-sm-6">
							<strong>TELEFONE: </strong>(88) 3512-2450
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<strong>ENDEREÇO: </strong> RUA MONSENHOR ESMERALDO, Nº 36
						</div>
						<div class="col-sm-6">
							<strong>CIDADE: </strong>JUAZEIRO DO NORTE - CEARÁ
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<strong>REPRESENTANTE: </strong>FRANCISCO PALACIO LEITE
						</div>
						<div class="col-sm-6">
							<strong>CARGO: </strong>DIRETOR PRESIDENTE
						</div>
					</div>
				</div>
			</div>


		<div class="texto">
			<div class="row" id="cl1">
				<div class="col-sm-12">
					<p style="text-align: justify" class="termo"><strong>CLÁUSULA 1ª - DO OBJETIVO:</strong>
						<br>O presente Termo de Compromisso formaliza as condições para realização de estágio de caráter não obrigatório, conforme a legislação vigente, sem caracterização de vínculo empregatício, visando a realização de atividades compatíveis com a programação curricular e projeto pedagógico do curso, devendo permitir ao ESTAGIÁRIO, regularmente matriculado, a prática complementar do aprendizado.
					</p>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<p class="termo justi"><strong>CLÁUSULA 2ª - DA VIGÊNCIA:</strong>
						<!--<br>O presente Termo de Compromisso de Estágio vigerá do dia _____/_____/_________ até _____/_____/_________-->
						<br>O presente Termo de Compromisso de Estágio vigerá do dia
						<strong>18/12/2017</strong> até <strong>18/06/2018</strong>        </p>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<strong class="justi">CLÁUSULA 3ª - DA JORNADA DE ESTÁGIO:</strong> 
					<p class="justi"> A jornada de estágio será de 
						20 (Vinte)            horas semanais,
						das 08:00 às 12:00.            
						na UNIDADE CONCEDENTE, salvo em situações excepcionais a critério da CONTRATANTE. </p>
				</div>
			</div>
			<div class="row" id="cl4">
				<div class="col-sm-12">
					<strong class="justi">CLÁUSULA 4ª - DO VALOR DA BOLSA DE ESTÁGIO E OUTROS BENEFÍCIOS:</strong> 
					<p class="justi">O ESTAGIÁRIO que realizar a jornada completa prevista na Cláusula 3ª, terá direito a receber, mensalmente, uma bolsa de estágio no valor de
					<strong>
									R$500.00 (quinhentos reais)</strong>        , auxílio-alimentação no valor de R$ 235,93 (duzentos e trinta e cinco reais e noventa e três centavos) e auxílio-transporte no valor de <strong>R$ 8,00 (oito reais)</strong>, devido apenas quanto aos dias de efetiva frequência ao estágio, conforme art.12º da Lei 11.788/2008.</p>
					<ol class="margin justi">
						<li>Além da bolsa de estágio estabelecida no “caput” desta cláusula, o ESTAGIÁRIO fará jus a um período de recesso remunerado, proporcional ao tempo de duração, até no máximo 30 (trinta) dias por ano, devido apenas quanto aos dias de efetiva frequência ao estágio, conforme art.13º da Lei 11.788/2008.</li>
						<li>O ESTAGIÁRIO, durante a vigência do presente Termo de Compromisso de Estágio, estará segurado contra acidentes pessoais, conforme Apólice Coletiva de nº. 7.607-0 de Porto Seguro Cia de Seguros Gerais, a cargo da Universidade Patativa do Assaré – UPA, conforme inciso IV do art. 9º da Lei 11.788/2008.</li>
					</ol>
				</div>
			</div>
			<div class="row" id="cl5">
				<div class="col-sm-12 justi">
					<strong>CLÁUSULA 5ª - SÃO OBRIGAÇÕES DA INSTITUIÇÃO DE ENSINO:</strong>
					<ol class="margin">
						<li>Avaliar as instalações da CONCEDENTE, através de instrumento próprio;</li>
						<li>Notificar a CONCEDENTE quando ocorrer a transferência, trancamento de curso, abandono ou outro fato impeditivo da continuidade do estágio;</li>
						<li>Indicar professor-orientador da área a ser desenvolvida no estágio, para acompanhar e avaliar as atividades do estagiário;</li>
						<li>Comunicar à CONCEDENTE, no início do período letivo, as datas de realização de avaliações escolares ou acadêmicas;</li>
						<li>Aprovar o ESTÁGIO de que trata o presente instrumento, considerando as condições de sua adequação à proposta pedagógica do curso, à etapa e modalidade da formação escolar do ESTAGIÁRIO e o horário e calendário escolar;</li>
						<li>Aprovar o Plano de Atividades de Estágio que consubstancie as condições/requisitos suficientes à exigência legal de adequação à etapa e modalidade da formação escolar do ESTAGIÁRIO.</li>
					</ol>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 columns justi">        
					<strong>CLÁUSULA 6ª - SÃO OBRIGAÇÕES DO ESTAGIÁRIO: </strong>
					<ol>
						<li>Cumprir com empenho e interesse toda a programação estabelecida para seu estágio;</li>
						<li>Observar, obedecer e cumprir as normas internas da CONCEDENTE;</li>
						<li>Apresentar documentos comprobatórios da regularidade da sua situação escolar, sempre que solicitado pela CONCEDENTE;</li>
						<li>Informar imediatamente à INSTITUIÇÃO DE ENSINO a rescisão antecipada do presente termo, para que possa adotar as providências administrativas cabíveis;</li>
						<li>Informar, de imediato, à CONCEDENTE, qualquer alteração na sua situação escolar, tais como: trancamento de matrícula, abandono, conclusão de curso ou transferência de Instituição de Ensino;</li>
						<li>Manter atualizados, junto à CONCEDENTE, seus dados pessoais e escolares;</li>
						<!--<li>Manter rigorosamente atualizado seus dados cadastrais e escolares, junto à Concedente;</li>-->
						<li>Entregar, obrigatoriamente, à INSTITUIÇÃO DE ENSINO, uma via do presente instrumento, devidamente assinado pelas partes;</li>
						<li>Informar previamente à CONCEDENTE os períodos de avaliação na Instituição de Ensino, para fins de redução da jornada de estágio;</li>
						<li>Preencher os relatórios de estágio, a fim de subsidiar as instituições de ensino com informações sobre estágio. </li>
					</ol>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 justi">
					<strong>CLÁUSULA 7ª - SÃO OBRIGAÇÕES DA CONCEDENTE:</strong> 
					<ol>
						<li>Ofertar instalações que tenham condições de proporcionar ao educando atividades de aprendizagem social, profissional e cultural;</li>
						<li>Zelar pelo cumprimento do presente Termo de Compromisso;</li>
						<li>Designar um supervisor que seja funcionário do seu quadro de pessoal, com formação ou experiência profissional na área de conhecimento desenvolvida no curso do ESTAGIÁRIO, para orientá-lo e acompanhá-lo no desenvolvimento das atividades do estágio;</li>
						<li>Solicitar ao ESTAGIÁRIO, a qualquer tempo, documentos comprobatórios da sua situação escolar, uma vez que, trancamento de matrícula, abandono, conclusão de curso ou transferência de instituição de ensino constituem motivo de imediata rescisão;</li>
						<li>Pagar a bolsa-auxílio, auxilio-alimentação e auxílio-transporte ao ESTAGIÁRIO através de processo de pagamento administrado pela UPA;</li>
						<li>Assegurar ao ESTAGIÁRIO recesso remunerado nos termos da lei 11.788/2008;</li>
						<li>Reduzir a jornada de estágio nos períodos de avaliação, previamente informados pelo ESTAGIÁRIO;</li>
						<li>Entregar, por ocasião do desligamento, Termo de Realização do Estágio;</li>
						<li>Manter os arquivos, e à disposição da fiscalização, os documentos firmados que comprovem a relação de estágio.</li>
					</ol>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 justi">
					<strong>CLÁUSULA 8ª - DAS OBRIGAÇÕES DO AGENTE DE INTEGRAÇÃO UNIVERSIDADE PATATIVA DO ASSARÉ - UPA:</strong>
					<ol>
						<li>Firmar convênio com as Instituições de Ensino, contendo as condições exigidas pelas mesmas para a caracterização e definição dos estágios de seus alunos;</li>
						<li>Obter da UNIDADE CONCEDENTE a identificação das oportunidades de estágio a serem concedidas; </li>
						<li>Encaminhar à CONCEDENTE os alunos interessados em estagiar, conforme necessidades definidas conjuntamente;</li>
						<li>Promover o ajuste das condições de estágio, definidas pela INSTITUIÇÃO DE ENSINO, com as condições e disponibilidades da CONCEDENTE, através de Termos Aditivos;</li>
						<li>Preparar e providenciar para que a Concedente e o estudante assinem o respectivo Termo de Compromisso de Estágio, nos termos da Lei 11.788, de 25 de setembro de 2008;</li>
						<li>Preparar toda a documentação legal referente ao estágio, bem como efetivar o Seguro conforme citado na Cláusula 4ª item 2; </li>
						<li>Fornecer à CONCEDENTE, sempre que entender necessário, ou quando solicitado, instruções específicas acerca da prática e supervisão dos estágios nela realizados;</li>
						<li>Propiciar instrumento de controle semestral dos relatórios de atividades preenchido pelo supervisor de estágio da CONCEDENTE e informar a INSTITUÇÃO DE ENSINO;</li>
						<li>Controlar e acompanhar a elaboração do relatório final de estágio, de responsabilidade da concedente;</li>
						<li>Avaliar as instalações de estágio da CONCEDENTE, subsidiando as Instituições de Ensino conforme a Lei.</li>
					</ol>   
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<strong>CLÁUSULA 9ª -  O PRESENTE TERMO DE COMPROMISSO DE ESTÁGIO E O PLANO DE ATIVIDADES DE ESTÁGIO SERÃO ALTERADOS OU PRORROGADOS ATRAVÉS DE TERMO ADITIVO, PODENDO O TERMO, NO ENTANTO, SER:</strong><br>
					<ol>
						<li>Extinto automaticamente ao término do estágio;</li>
						<li>Rescindido por deliberação da CONCEDENTE ou do estagiário;</li>
						<li>Rescindido por conclusão, abandono ou trancamento de matrícula do curso realizado pelo estagiário.</li>
					</ol>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<p>
						<strong>CLÁUSULA 10ª – A CONCEDENTE e o ESTAGIÁRIO, signatários deste instrumento, de comum acordo e para os efeitos da Lei nº. 11.788/08, elegem a Universidade Patativa do Assaré - UPA como seu AGENTE DE INTEGRAÇÃO, a quem comunicarão a interrupção ou eventuais modificações do convencionado no presente instrumento.</strong> <br><br>
						E, por estarem de inteiro e comum acordo com o Plano de Atividades em anexo e com as demais condições estabelecidas neste TERMO DE COMPROMISSO DE ESTÁGIO-TCE, as partes os assinam em 4 (quatro) vias de igual teor. 
					</p>
				</div>
			</div>

			<div class="row" style="margin-top: 10px;">
				<div class="col-sm-12">
					<p style="float: right;">______________________, _____ de ______________ de _______. </p>
				</div>
			</div>
<div class="row" style="margin-top: 20px;" >

    <div class="col-sm-6">
        <div class="row">
            <div class="col-sm-8" style="font-size: 11px">
                <hr style=" margin-bottom: 0px; border-bottom: 1px solid #333 ;" />
                <p class="p"> COMPANHIA URBANIZADORA DA NOVA CAPITAL DO BRASIL - NOVACAP</p>
            </div>
            <div class="col-sm-8" style="font-size: 11px; margin-top: 40px;">
                <hr style=" margin-bottom: 0px; border-bottom: 1px solid #333 ;" />
                <p class="p">CENTRO DE EDUCAÇÃO PROFISSIONAL-ESCOLA TÉCNICA DE SAÚDE DE PLANALTINA</p>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="row">
            <div class="col-sm-8" style="font-size: 11px; float: right;">
                <hr style=" margin-bottom: 0px; border-bottom: 1px solid #333 ;" />
                <p class="p">ALINDE FEITOSA VIANNA </p>
            </div>
            <!--<div class="span4" style="font-size: 11px; margin-top: 39px;">
                <hr style=" margin-bottom: 0px; border-bottom: 1px solid #333 ;" />
                <p class="p"></p>
            </div>-->
        </div>
    </div>
		</div>
			
	

	

		<div class="row" id="rodape">
			<div class="col-sm-12" style="text-align: center; margin-top: 50px; ">
				<p class="p">Rua São Jorge 530, Centro - Juazeiro do Norte - CE - CEP 63010-470</p>
				<p class="p">Telefone: (88) 3512-2450 - www.universidadepatativa.com.br</p>
			</div>
		</div>


		
	</div>
</body>

</html>