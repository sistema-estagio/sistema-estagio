<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<title>Recisão TCE 
			@if($tce->migracao != NULL) 
				{{$tce->migracao}}
			@else 
				{{$tce->id}}
			@endif
	</title>
	<style>
		html { margin: 30px}
		body {
			font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif;
		}
		footer.fixar-rodape{
			
			bottom: 0;
			left: 0;
			height: 40px;
			position: fixed;
			width: 100%;
		  }
		.novaPagina {
			page-break-before: always;
			/*page-break-after: always*/
		}
		div.body-content{
			/** Essa margem vai evitar que o conteudo fique por baixo do rodapé **/
			margin-bottom: 40px;
		}

		.col-12 {
			width: 100% !important;
		}

		.col-6 {
			width: 50% !important;
		}

		.col-6,
		.col-12 {
			float: left !important;
		}

		.table {
			border-collapse: separate;
			border-spacing: 0;
			min-width: 350px;
			margin-bottom: 10px;
		}

		.table tr th,
		.table tr td {
			border-right: 1px solid #bbb;
			border-bottom: 1px solid #bbb;
			padding: 5px;
		}

		.table tr th:first-child,
		.table tr th:last-child {
			border-top: solid 1px #bbb;
		}

		.table tr th:first-child,
		.table tr td:first-child {
			border-left: 1px solid #bbb;
		}

		.table tr th:first-child,
		.table tr td:first-child {
			border-left: 1px solid #bbb;
		}

		.table tr th {
			background: #eee;
			text-align: left;
		}

		.table.Info tr th,
		.table.Info tr:first-child td {
			border-top: 1px solid #bbb;
		}
		/* top-left border-radius */

		.table tr:first-child th:first-child,
		.table.Info tr:first-child td:first-child {
			border-top-left-radius: 6px;
		}
		/* top-right border-radius */
		
		.table tr:first-child th:last-child,
		.table.Info tr:first-child td:last-child {
			border-top-right-radius: 6px;
		}

		/* bottom-left border-radius */
		.table tr:last-child td:first-child {
			border-bottom-left-radius: 6px;
		}

		/* bottom-right border-radius */
		.table tr:last-child td:last-child {
			border-bottom-right-radius: 6px;
		}
	</style>
</head>
<body>
	<div id="body">

		<div id="content">

			<div class="page" style="font-size: 7pt">
				<table style="width: 100%;" class="header">
					<tr>
						<td>
							<?php $image_path = '/img/relatorios/logo_default.png'; ?>
							<img src="{{ public_path() . $image_path }}" alt="Upa - Estagio" />
						</td>
						<td>
							<h1 style="text-align: right">
								<strong>TERMO DE RECISÃO DE ESTÁGIO<br>TCE 
									@if($tce->migracao != NULL) 
										{{$tce->migracao}}
									@else 
										{{$tce->id}}
									@endif
								</strong>
							</h1>
						</td>
					</tr>
				</table>

				<div class="row">
					<div class="span12">
						<p style="margin: 10px;">As partes abaixo qualificadas celebram neste ato Termo de Compromisso de Estágio, conforme a Lei n°. 11.788/08.</p>
					</div>
				</div>

				@if($tce->instituicao->poloMatriz != NULL)
				<!-- SE INSTITUIÇÃO TIVER POLO MATRIZ DEFINIDA EXIBE ENDEREÇO E DADOS DO POLO MATRIZ -->
				<!--INSTITUIÇÃO DE ENSINO-->
				<table style="width: 100%;" class="table table-bordered table-condensed ">
						<tr>
							<th colspan="2" style="text-align: center">
								<strong>DADOS DA INSTITUIÇÃO DE ENSINO</strong><span style="float: right;font-size: 5pt"> (Cod.: {{$tce->instituicao_id}})</span>
							</th>
						</tr>
						<tr>
							<td colspan="2">
								<strong>{{$tce->instituicao->poloMatriz->nmPolo}}({{$tce->instituicao->nmInstituicao}})</strong>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<strong>CNPJ:</strong> {{$tce->instituicao->poloMatriz->cdCNPJ}}
							</td>
						</tr>
						<tr>
							<td>
								<strong>ENDEREÇO: </strong>{{$tce->instituicao->poloMatriz->dsEndereco}} ,
								<strong>N°</strong> {{$tce->instituicao->poloMatriz->nnNumero}}
							</td>
							<td>
								<strong>CIDADE: </strong>{{$tce->instituicao->poloMatriz->cidade->nmCidade}} - {{$tce->instituicao->poloMatriz->cidade->estado->nmEstado}}</td>
						</tr>
						<tr>
							@if($tce->instituicao->poloMatriz->nmDiretor <> NULL)
							<td>
								<strong>REPRESENTANTE: </strong>{{$tce->instituicao->poloMatriz->nmDiretor}}
							</td>
							@else
							<td>
								<strong>REPRESENTANTE: </strong>{{$tce->instituicao->nmDiretor}}
							</td>
							@endif
							<td>
								<strong>TELEFONE: </strong> {{$tce->instituicao->poloMatriz->dsFone}}
							</td>
						</tr>
						<!--SE TIVER SIDO PREENCHIDO NMREITOR NO POLO-->
						@if($tce->instituicao->poloMatriz->nmReitor <> NULL)
							<tr>
								<td colspan="2">
									<strong>REITOR(A) MATRIZ: </strong>{{$tce->instituicao->poloMatriz->nmReitor}}
								</td>
							</tr>
						<!--SE NÂO TIVER PREENCHIDO NMREITOR NO POLO-->
						@else
							<!--SE TIVER PREENCHIDO NMREITOR NA INSTITUICAO-->
							@if($tce->instituicao->nmReitor <> NULL)
							<tr>
								<td colspan="2">
									<strong>REITOR(A): </strong>{{$tce->instituicao->nmReitor}}
								</td>
							</tr>
							@endif
						@endif

							@if(($tce->nmSupervisorIe) AND ($tce->dsSupervisorIeCargo) != NULL)
							<tr>
								<td>
									<em><strong>SUPERVISOR(A): </strong>{{$tce->nmSupervisorIe}}</em>
								</td>
								<td>
									<em><strong>CARGO/FORMAÇÃO: </strong>{{$tce->dsSupervisorIeCargo}}</em>
								</td>
							</tr>
							@else 
								@if(($tce->instituicao->nmSupervisor) AND ($tce->instituicao->nmSupervisorCargo))
								<tr>
									<td>
										<strong>SUPERVISOR(A): </strong>{{$tce->instituicao->nmSupervisor}}
									</td>
									<td>
										<strong>CARGO/FORMAÇÃO: </strong>{{$tce->instituicao->nmSupervisorCargo}}
									</td>
								</tr>
								@endif 
							@endif
				</table>
				@else
				<!-- SE INSTITUIÇÃO NÃO TIVER POLO MATRIZ DEFINIDA EXIBE ENDEREÇO E DADOS DO POLO MATRIZ -->
				<!--INSTITUIÇÃO DE ENSINO-->
				<table style="width: 100%;" class="table table-bordered table-condensed ">
						<tr>
							<th colspan="2" style="text-align: center">
								<strong>DADOS DA INSTITUIÇÃO DE ENSINO</strong><span style="float: right;font-size: 5pt"> (Cod.: {{$tce->instituicao_id}})</span>
							</th>
						</tr>
						<tr>
							<td colspan="2">
								<strong>{{$tce->instituicao->nmInstituicao}}</strong>
							</td>
						</tr>
						<tr>
							<td colspan="2"><strong>CNPJ:</strong> {{$tce->instituicao->cdCNPJ}}</td>
						</tr>
						<tr>
							<td>
								<strong>ENDEREÇO: </strong>{{$tce->instituicao->dsEndereco}},<strong> N°</strong> {{$tce->instituicao->nnNumero}}
							</td>
							<td>
								<strong>CIDADE: </strong>{{$tce->instituicao->cidade->nmCidade}} - {{$tce->instituicao->cidade->estado->nmEstado}}</td>
						</tr>
						<tr>
							<td><strong>REPRESENTANTE: </strong>{{$tce->instituicao->nmDiretor}}</td>
							<td><strong>TELEFONE: </strong> {{$tce->instituicao->dsFone}}</td>
						</tr>
						@if($tce->instituicao->nmReitor <> NULL)
							<tr>
								<td colspan="2">
									<strong>REITOR(A): </strong>{{$tce->instituicao->nmReitor}}
								</td>
							</tr>
						@endif 
							@if(($tce->nmSupervisorIe) AND ($tce->dsSupervisorIeCargo) != NULL)
							<tr>
								<td>
									<em><strong>SUPERVISOR(A): </strong>{{$tce->nmSupervisorIe}}</em>
								</td>
								<td>
									<em><strong>CARGO/FORMAÇÃO: </strong>{{$tce->dsSupervisorIeCargo}}</em>
								</td>
							</tr>
							@else 
								@if(($tce->instituicao->nmSupervisor) AND ($tce->instituicao->nmSupervisorCargo))
								<tr>
									<td>
										<strong>SUPERVISOR(A): </strong>{{$tce->instituicao->nmSupervisor}}
									</td>
									<td>
										<strong>CARGO/FORMAÇÃO: </strong>{{$tce->instituicao->nmSupervisorCargo}}
									</td>
								</tr>
								@endif 
							@endif
				</table>
				@endif

				<!--CONCEDENTE-->
				<table style="width: 100%;" class="table table-bordered table-condensed">
						<tbody>
							<tr>
								<th colspan="3" style="text-align: center;">
									<strong>DADOS DA CONCEDENTE</strong><span style="float: right;font-size: 5pt"> (Cod.: {{$tce->concedente_id}})</span>
								</th>
							</tr>
							<tr>
								<td colspan="3">
									<strong>LOCAL DE REALIZAÇÃO DE ESTÁGIO:</strong> {{$tce->concedente->nmRazaoSocial}}
								</td>
							</tr>
							<!--SE O TCE FOR DE UMA SECRETARIA DA CONCEDENTE-->
							@if($tce->sec_conc_id <> NULL)
								<tr>
									<td colspan="3"><strong>SECRETARIA:</strong> {{$tce->secConcedente->nmSecretaria}}</td>
								</tr>
								<tr>
									<td colspan="3"><strong>CNPJ: </strong>{{$tce->secConcedente->cdCnpjCpf}}</td>
								</tr>
								<tr>
									<td colspan="2">
										<strong>ENDEREÇO: </strong> {{$tce->secConcedente->dsEndereco}},<strong> N°</strong> {{$tce->secConcedente->nnNumero}}
									</td>
									<td><strong>BAIRRO: </strong>{{$tce->secConcedente->nmBairro}}</td>
								</tr>
								@if($tce->secConcedente->dsComplemento)
								<tr>
									<td colspan="3">
										<strong>COMPLEMENTO: </strong> {{$tce->secConcedente->dsComplemento}}
									</td>
								</tr>
								@endif
								<tr>
									<td colspan="2">
										<strong>CIDADE: </strong>{{$tce->concedente->cidade->nmCidade}} - {{$tce->concedente->cidade->estado->nmEstado}}
									</td>
									<td><strong>CEP: </strong>{{$tce->secConcedente->cdCEP}}</td>
								</tr>
								<tr>
									<td colspan="2"><strong>TELEFONE: </strong>{{$tce->secConcedente->dsFone}}</td>
									<td>
										<strong>OUTRO: </strong> @if  ( $tce->secConcedente->dsOutroFone != NULL) {{$tce->secConcedente->dsOutroFone}} @else - @endif
									</td>
								</tr>
								
								@if(($tce->concedente->nmResponsavel) AND ($tce->concedente->dsResponsavelCargo) != NULL)
								<tr>
									<td colspan="2">
										<strong>REPRESENTADO PELO SR(A): </strong>{{$tce->concedente->nmResponsavel}}
									</td>
									<td>
										<strong>CARGO: </strong>{{$tce->concedente->dsResponsavelCargo}}
									</td>
								</tr>
								@endif
								@if(($tce->secConcedente->nmResponsavel) AND ($tce->secConcedente->dsResponsavelCargo) != NULL)
								<tr>
									<td colspan="2">
										<strong>RESP. ADMINISTRATIVO(A): </strong>{{$tce->secConcedente->nmResponsavel}}
									</td>
									<td>
										<strong>CARGO: </strong>{{$tce->secConcedente->dsResponsavelCargo}}
									</td>
								</tr>
								@endif
								@if(($tce->nmSupervisor) AND ($tce->dsSupervisorCargo) != NULL)
								<tr>
									<td colspan="2">
										<em><strong>SUPERVISOR(A): </strong>{{$tce->nmSupervisor}}</em>
									</td>
									<td>
										<em><strong>CARGO/FORMAÇÃO: </strong>{{$tce->dsSupervisorCargo}}</em>
									</td>
								</tr>
								@endif
							<!--SE NÂO FOR DE SECRETARIA EXIBE OS DADOS DA CONCEDENTE-->
							@else
								@if($tce->concedente->flTipo == "PF")
								<tr>
									<td colspan="2">
										<strong>CPF: </strong>{{$tce->concedente->cdCnpjCpf}}
									</td>
									<td>
										<strong>ORGÃO REGULADOR: </strong>{{$tce->concedente->dsOrgaoRegulador}}
									</td>
								</tr>
								@else
								<tr>
									<td colspan="3">
										<strong>CNPJ: </strong>{{$tce->concedente->cdCnpjCpf}}
									</td>
								</tr>
								@endif
								<tr>
									<td colspan="2">
										<strong>ENDEREÇO: </strong> {{$tce->concedente->dsEndereco}} , 
										<strong>N°</strong> {{$tce->concedente->nnNumero}}
									</td>
									<td>
										<strong>BAIRRO: </strong>{{$tce->concedente->nmBairro}}
									</td>
								</tr>
								@if($tce->concedente->dsComplemento)
								<tr>
									<td colspan="3">
										<strong>COMPLEMENTO: </strong> {{$tce->concedente->dsComplemento}}
									</td>
								</tr>
								@endif
								<tr>
									<td colspan="2">
										<strong>CIDADE: </strong>{{$tce->concedente->cidade->nmCidade}} - {{$tce->concedente->cidade->estado->nmEstado}}
									</td>
									<td>
										<strong>CEP: </strong>{{$tce->concedente->cdCEP}}
									</td>
								</tr>
								<tr>
									<td colspan="2">
										<strong>TELEFONE: </strong>{{$tce->concedente->dsFone}}
									</td>
									<td>
										<strong>OUTRO: </strong> @if  ( $tce->concedente->dsOutroFone != NULL) {{$tce->concedente->dsOutroFone}} @else - @endif
									</td>
								</tr>
								
								<!--mostrar resp da concedente no tce-->
								@if(($tce->concedente->nmResponsavel) AND ($tce->concedente->dsResponsavelCargo) != NULL)
								<tr>
									<td colspan="2">
										<strong>REPRESENTADO PELO SR(A): </strong>{{$tce->concedente->nmResponsavel}}
									</td>
									<td>
										<strong>CARGO: </strong>{{$tce->concedente->dsResponsavelCargo}}
									</td>
								</tr> 
								@endif
								<!--mostrar resp da concedente no tce fim-->
								@if(($tce->nmSupervisor) AND ($tce->dsSupervisorCargo) != NULL)
								<tr>
									<td colspan="2">
										<strong>SUPERVISOR(A): </strong>{{$tce->nmSupervisor}}
									</td>
									<td>
										<strong>CARGO/FORMAÇÃO: </strong>{{$tce->dsSupervisorCargo}}
									</td>
								</tr> 
								@endif
							<!--ABAIXO FIM DO IF SE É SECRETARIA DE CONCEDENTE-->
							@endif
							@if($tce->dsLotacao)
							<tr>
								<td colspan="3">
									<strong>LOTAÇÃO:</strong> {{$tce->dsLotacao}}
								</td>
							</tr>
							@endif
						</tbody>
					</table>

				<!--ESTAGIARIO-->
				<table style="width: 100%;" class="table table-bordered table-condensed">
					<tr>
						<th colspan="3" style="text-align: center">
							<strong>DADOS DO ESTÁGIARIO </strong><span style="float: right;font-size: 5pt">(Cod.: {{$tce->estudante_id}})</span>
						</th>
					</tr>
					<tr>
						<td colspan="">
							<strong>NOME: </strong>{{$tce->estudante->nmEstudante}}
						</td>
						<td>
							<strong>DATA NASC: </strong>{{$tce->estudante->dtNascimento->format('d/m/Y')}}
						</td>
						<td>
							<strong>CPF: </strong>{{$tce->estudante->cdCPF}}
						</td>
					</tr>
					<tr>
						<td>
							<strong>RG: </strong>{{$tce->estudante->cdRG}}
						</td>
						<td>
							<strong>ORG.: </strong>{{$tce->estudante->dsOrgaoRG}}
						</td>
						<td>
							<strong>EMAIL: </strong>{{$tce->estudante->dsEmail}}
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<strong>ENDEREÇO: </strong> {{$tce->estudante->dsEndereco}},
							<strong> N°</strong> {{$tce->estudante->nnNumero}}
						</td>
						<td>
							<strong>BAIRRO: </strong>{{$tce->estudante->nmBairro}}
						</td>
					</tr>
					<tr>
						<td colspan="1">
							<strong>CIDADE: </strong>{{$tce->estudante->cidade->nmCidade}} - {{$tce->estudante->cidade->estado->nmEstado}}
						</td>
						<td>
							<strong>CEP: </strong>{{$tce->estudante->cdCEP}}
						</td>
						<td>
							<strong>TELEFONE: </strong>{{$tce->estudante->dsFone}}
						</td>
					</tr>
					<tr>
						<td>
							<strong>CURSO: </strong> {{$tce->cursoDaInstituicao->curso->nmCurso}}
						</td>
						<td>
							<strong>{{$tce->cursoDaInstituicao->cursoDuracao->nmTipoDuracao}}: </strong>
							{{$tce->nnSemestreAno}} {{$tce->cursoDaInstituicao->cursoDuracao->nmTipoDuracao}}
						 </td>
						<td>
							<strong>TURNO: </strong>{{$tce->estudante->turno->nmTurno}} 
						</td>
					</tr>
				</table>

                @include('tce.relatorio.inc_agente')

				<div class="texto">
						<div class="row" id="cl1">
							<div class="col-sm-12">
								<p style="text-align: justify" class="termo">
                                    De acordo com a informação notificada pela concedente, a partir de <strong>{{$tce->dtCancelamento->format('d/m/Y')}}</strong> encerra-se o TCE ou último Termo Aditivo, firmado entre as partes supra, para o período compreendido entre <strong>{{$tce->dtInicio->format('d/m/Y')}}</strong> à 
                                    @if($getAditivo == NULL)
                                        <strong>{{$tce->dtFim->format('d/m/Y')}}</strong>
                                    @else
                                        <strong>{{$getAditivo->dtFim->format('d/m/Y')}}</strong>
                                    @endif
                                    nos termos do que dispõem a Lei nº 11.788/08.
                                </p>
                                <p style="text-align: justify" class="termo">
                                    Para todos os fins e efeitos de direito e por estarem de acordo com a condição estabelecida, as partes o assinam em 4 (quatro) vias de igual teor.
								</p>
							</div>
						</div>
						
						
						
						<div class="row body-content">
							<div class="span12">
								<p style="margin: 10px; float: right;">___________________, _____ de ______________ de _______.</p>
							</div>
						</div>
					

						<div class="col-12" style=" margin-top: 50px;">
								
												<div class="col-6">
														<div style="font-size: 11px;margin-top: 50px;border-top: 1px solid #333;">
																<p class="p">{{$tce->estudante->nmEstudante}}</p>
														</div>
														@php
														$nascimento = $tce->estudante->dtNascimento;
														$nascimento->addYears(18)->toDateString();   
														$hoje = \Carbon\Carbon::now()->toDateString();
														if($nascimento > $hoje){
															echo '<div style="font-size: 11px;margin-top: 30px;border-top: 1px solid #333;">
																	<p class="p">REPRESENTANTE LEGAL DO ESTAGIÁRIO / CPF</p>
																</div>';
														}
														@endphp
														<div style="font-size: 11px;margin-top: 30px;border-top: 1px solid #333;">
															<p class="p">{{$tce->concedente->nmRazaoSocial}}</p>
														</div>
														<div style="font-size: 11px;margin-top: 30px;border-top: 1px solid #333;">
															<p class="p">{{$tce->instituicao->nmInstituicao}}</p>
														</div>
													
												</div>
				
												<div style="clear: both"></div>
											</div>

						
						<!--<div class="row body-content">
						<table width="100%" border="0" style="margin-top:100px;">
							<tr>
								<td width="50%">
									<div style="font-size: 11px;border-top: 1px solid #333;">
										<p class="p">{{$tce->concedente->nmRazaoSocial}}</p>
									</div>  
								</td>
								<td width="50%">
									<div style="width=20px; font-size: 11px; border-top: 1px solid #333;">
										<p class="p">{{$tce->estudante->nmEstudante}}</p>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<div style="font-size: 11px; margin-top: 20px;border-top: 1px solid #333;">
										<p class="p">{{$tce->instituicao->nmInstituicao}}</p>
									</div>
								</td>
								<td>&nbsp;</td>
							</tr>
						</table>
					</div>-->
						
			
						
						
						@include('tce.relatorio.inc_rodape')
					</div>

			</div>

			<div class="novaPagina"></div>
			<!-- -->
			<div class="page" style="font-size: 7pt">
					<table style="width: 100%;" class="header">
						<tr>
							<td>
								<?php $image_path = '/img/relatorios/logo_default.png'; ?>
								<img src="{{ public_path() . $image_path }}" alt="Upa - Estagio" />
							</td>
							<td>
								<h1 style="text-align: right">
									<strong>
										RELATÓRIO DE ATIVIDADES - ESTAGIÁRIO
										<br>{{$tce->estudante->nivel->nmNivel}} 
									</strong>
								</h1>
							</td>
						</tr>
					</table>
					<br>
					<!--ESTAGIARIO-->
					<table style="width: 100%;" class="table table-bordered table-condensed">
							<thead> 
								<tr> 
									<th colspan="3" style="text-align: center">
										<strong>INFORMAÇÕES</strong>
									</th>
								</tr> 
							</thead>
						<tr>
							<td colspan="2">
								<strong>ESTÁGIARIO: </strong>{{$tce->estudante->nmEstudante}}
							</td>
							<td>
								<strong>TCE:</strong>
								@if($tce->migracao != NULL) 
									{{$tce->migracao}}
								@else 
									{{$tce->id}}
								@endif
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<strong>CPF: </strong>{{$tce->estudante->cdCPF}}
							</td>
							<td>
								<strong>COMPETÊNCIA (MÊS/ANO):</strong>
								@php
								$hoje = \Carbon\Carbon::now();
								echo $hoje->month.'/'.$hoje->year;
								@endphp
							</td>
						</tr>
						<tr>
							<td colspan="3">
								<strong>CONCEDENTE: </strong> {{$tce->concedente->nmRazaoSocial}}
								({{$tce->concedente->nmFantasia}})
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<strong>ENDEREÇO: </strong> {{$tce->estudante->dsEndereco}},
								<strong> N°</strong> {{$tce->estudante->nnNumero}}
							</td>
							<td>
								<strong>BAIRRO: </strong>{{$tce->estudante->nmBairro}}
							</td>
						</tr>
						<tr>
							<td colspan="1">
								<strong>CIDADE: </strong>{{$tce->estudante->cidade->nmCidade}} - {{$tce->estudante->cidade->estado->nmEstado}}
							</td>
							<td>
								<strong>E-MAIL: </strong>{{$tce->estudante->dsEmail}}
							</td>
							<td>
								<strong>TELEFONE: </strong>{{$tce->estudante->dsFone}}
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<strong>CURSO: </strong> {{$tce->cursoDaInstituicao->curso->nmCurso}}
							</td>
							<td>
								<strong>{{$tce->cursoDaInstituicao->cursoDuracao->nmTipoDuracao}}: </strong>
								{{$tce->nnSemestreAno}} {{$tce->cursoDaInstituicao->cursoDuracao->nmTipoDuracao}}
							 </td>
						</tr>
					</table>
	
					<div class="texto" style="font-size: 8pt">
							<div class="row" id="cl1">
								<div class="col-sm-12">
									<p style="text-align: justify" class="termo">Caro Estagiário,</p>
									<p style="text-align: justify" class="termo">
										A Lei 11.788/2008 dispõe que a Instituição de Ensino deve exigir do educando o Relatório de Atividades de Estágio em prazo não superior a 6 meses.
									</p>
									<p style="text-align: justify" class="termo">
											A UPA, a fim de subsidiar as Instituições de Ensino na supervisão e avaliação do estágio de seus alunos, elaborou este documento, cujo preenchimento é obrigatório, conforme consta no Termo de Compromisso de estágio, assinado no início do estágio.
									</p>
									<ol class="margin justi"  >
										<li style="margin-top:10px; ">
											As atividades que você desenvolve estão de acordo com as descritas no Termo de Compromisso de Estagio?
											<br> (    ) Sim             (    ) Não
										</li>
										<li style="margin-top:10px; ">
											Você tem encontrado facilidade para solicitar orientações durante o estagio, e, quando as solicita, esclarecimentos são suficientes?
											<br> (    ) Sim       (    ) Eventualmente      (    ) Não
										</li>
										<li style="margin-top:10px; ">
											O estágio tem contribuído para melhorar a compreensão de seus direitos e deveres na sociedade?
											<br> (    ) Sim (    ) Em parte (    ) Não
										</li>
										<li style="margin-top:10px; ">
											O estágio está proporcionando a busca de novos conhecimentos?
											<br> (    ) Sim (    ) Em parte (    ) Não
										</li>
										<li style="margin-top:10px; ">
											O ambiente de estágio é adequado para o desenvolvimento de suas atividades?
											<br> (    ) Sim  (    ) Não
										</li>
										<li style="margin-top:10px; ">
											Você avalia que o estágio tem auxiliado na escolha de uma futura carreira profissional?
											<br>( ) Sim ( ) Não
										</li>
										<li style="margin-top:10px; ">
											O estágio tem permitido que você adquira uma melhor preparação para o mercado de trabalho?
											<br>( ) Sim ( ) Não
										</li>
										<li style="margin-top:10px; ">
											O estágio está sendo um fator determinante na motivação para a continuidade dos estudos?
											<br>( ) Sim ( ) Não
										</li>
										<li style="margin-top:10px; ">
											Caso haja possibilidade, você teria interesse em ser contratado pela empresa?
											<br>( ) Sim ( ) Não
										</li>
										</ol>
									<p style="text-align: justify" class="termo">
											
									</p>
								</div>
							</div>
							
							
							<br><br><br><br>
							<div class="row body-content">
									<div class="span6">
											<p style="margin: 10px; float: left;">Assinatura: ____________________________________</p>
										</div>
								<div class="span6">
									<p style="margin: 10px; float: right;">___________________, _____ de ______________ de _______.</p>
								</div>
							</div>
							<br><br><br><br>
							<div class="texto" style="font-size: 8pt">
									<div class="row" id="cl1">
										<div class="col-sm-12">
											<p style="text-align: center" class="termo">Preencher em caso de ALTERAÇÃO DE DADOS</p>
											<p style="text-align: center" class="termo">Envio relatório por e-mail – <a href="mailto:estagio@universidadepatativa.com.br">estagio@universidadepatativa.com.br</a></p>
										</div>
									</div>
							</div>
							
	
								
	
							
							
				
							
							
							@include('tce.relatorio.inc_rodape')
						</div>
	
				</div>
			<!--   -->

		</div>
	</div>



</body>

</html>