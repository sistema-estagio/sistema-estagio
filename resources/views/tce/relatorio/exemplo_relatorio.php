<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<title>Reatorio Tce Normal - TCE {{$tce->id}}</title>
	<style>
		body {
			font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif;
		}
		/*    
		.table {
			width: 100%;
			margin-bottom: 10px;
		}

		.table thead {
			background: #CFCFCF;
		}

		.table-bordered {
			border: 1px solid #dddddd;
			border-left-width: 1px;
			border-left-style: solid;
			border-left-color: rgb(221, 221, 221);
			border-left: 1px solid #ddd;
			border-collapse: collapse;
			border-left: 0;
			-webkit-border-radius: 6px;
			-moz-border-radius: 6px;
			border-radius: 6px;
		}

		.table th,
		.table td {

			padding: 1px;
			line-height: 20px;
			text-align: left;
			vertical-align: top;
			border: 1px solid #ddd;
		}

		.table th {
			font-weight: bold;
		}

		.table thead th {
			vertical-align: bottom;
        }
        */

		.table {
			border-collapse: separate;
			border-spacing: 0;
			min-width: 350px;
			margin-bottom: 10px;
		}

		.table tr th,
		.table tr td {
			border-right: 1px solid #bbb;
			border-bottom: 1px solid #bbb;
			padding: 5px;
		}

		.table tr th:first-child,
		.table tr th:last-child {
			border-top: solid 1px #bbb;
		}

		.table tr th:first-child,
		.table tr td:first-child {
			border-left: 1px solid #bbb;
		}

		.table tr th:first-child,
		.table tr td:first-child {
			border-left: 1px solid #bbb;
		}

		.table tr th {
			background: #eee;
			text-align: left;
		}

		.table.Info tr th,
		.table.Info tr:first-child td {
			border-top: 1px solid #bbb;
		}
		/* top-left border-radius */

		.table tr:first-child th:first-child,
		.table.Info tr:first-child td:first-child {
			border-top-left-radius: 6px;
		}
		/* top-right border-radius */

		.table tr:first-child th:last-child,
		.table.Info tr:first-child td:last-child {
			border-top-right-radius: 6px;
		}
		/* bottom-left border-radius */

		.table tr:last-child td:first-child {
			border-bottom-left-radius: 6px;
		}
		/* bottom-right border-radius */

		.table tr:last-child td:last-child {
			border-bottom-right-radius: 6px;
		}
	</style>
</head>

<body>

	<div id="body">

		<div id="section_header">
		</div>

		<div id="content">

			<div class="page" style="font-size: 7pt">
				<table style="width: 100%;" class="header">
					<tr>
						<td>
							<?php $image_path = '/img/relatorios/logo_default.png'; ?>
							<img src="{{ public_path() . $image_path }}" alt="Upa - Estagio" />
						</td>
						<td>
							<h1 style="text-align: right">
								<strong>TERMO DE COMPROMISSO DE ESTÁGIO
									<br>TCE {{$tce->id}}</strong>
							</h1>
						</td>
					</tr>
				</table>

				<div class="row">
					<div class="span12">
						<p style="margin: 10px;">As partes abaixo qualificadas celebram neste ato Termo de Compromisso de Estágio, conforme a Lei n°. 11.788/08.</p>
					</div>
				</div>

				<!--INSTITUIÇÃO DE ENSINO-->
				<table style="width: 100%;" class="table table-bordered table-condensed ">
					<tr>
						<th colspan="2" style="text-align: center">
							<strong>DADOS DA INSTITUIÇÃO DE ENSINO</strong>
						</th>
					</tr>
					<tr>
						<td colspan="2">
							<strong>{{$tce->instituicao->nmInstituicao}}</strong>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<strong>CNPJ:</strong> {{$tce->instituicao->cdCNPJ}}</td>
					</tr>
					<tr>
						<td>
							<strong>ENDEREÇO: </strong>{{$tce->instituicao->dsEndereco}} ,
							<strong>N°</strong> {{$tce->instituicao->nnNumero}} </td>
						<td>
							<strong>CIDADE: </strong>{{$tce->instituicao->cidade->nmCidade}} - {{$tce->instituicao->cidade->estado->nmEstado}}</td>
					</tr>
					<tr>
						<td>
							<strong>REPRESENTANTE: </strong>{{$tce->instituicao->nmDiretor}}</td>
						<td>
							<strong>TELEFONE: </strong> {{$tce->instituicao->dsFone}}</td>
					</tr>
					@if($tce->instituicao->nmReitor <> NULL)
						<tr>
							<td colspan="2">
								<strong>REITOR(A): </strong>{{$tce->instituicao->nmReitor}}
							</td>
						</tr>
                    @endif 
                        @if(($tce->nmSupervisorIe) AND ($tce->dsSupervisorIeCargo) != NULL)
						<tr>
							<td>
								<strong>SUPERVISOR(A): </strong>{{$tce->nmSupervisorIe}}</td>
							<td>
								<strong>CARGO/FORMAÇÃO: </strong>{{$tce->dsSupervisorIeCargo}}</td>
						</tr>
                        @else 
                            @if(($tce->instituicao->nmSupervisor) AND ($tce->instituicao->nmSupervisorCargo))
                            <tr>
                                <td>
                                    <strong>SUPERVISOR(A): </strong>{{$tce->instituicao->nmSupervisor}}</td>
                                <td>
                                    <strong>CARGO/FORMAÇÃO: </strong>{{$tce->instituicao->nmSupervisorCargo}}</td>
                            </tr>
                            @endif 
                        @endif
				</table>

				<!--CONCEDENTE-->
				<table style="width: 100%;" class="table table-bordered table-condensed">
					<tbody>
						<tr>
							<th colspan="3" style="text-align: center;">
								<strong>DADOS DA CONCEDENTE</strong>
							</th>
						</tr>
						<tr>
							<td colspan="3">
								<strong>LOCAL DE REALIZAÇÃO DE ESTÁGIO:</strong> {{$tce->concedente->nmRazaoSocial}}</td>
						</tr>
						@if($tce->concedente->flTipo == "PF")
						<tr>
							<td colspan="2">
								<strong>CPF: </strong>{{$tce->concedente->cdCnpjCpf}}
							</td>
							<td>
								<strong>ORGÃO REGULADOR: </strong>{{$tce->concedente->dsOrgaoRegulador}}
							</td>
						</tr>
						@else
						<tr>
							<td colspan="3">
								<strong>CNPJ: </strong>{{$tce->concedente->cdCnpjCpf}}
							</td>
						</tr>
						@endif
						<tr>
							<td colspan="2">
								<strong>ENDEREÇO: </strong> {{$tce->concedente->dsEndereco}} , <strong>N°</strong> {{$tce->concedente->nnNumero}} </td>
							<td>
								<strong>BAIRRO: </strong>{{$tce->concedente->nmBairro}}</td>
						</tr>
						<tr>
							<td colspan="2">
								<strong>CIDADE: </strong>{{$tce->concedente->cidade->nmCidade}} - {{$tce->concedente->cidade->estado->nmEstado}}</td>
							<td>
								<strong>CEP: </strong>{{$tce->concedente->cdCEP}}</td>
						</tr>
						<tr>
							<td colspan="2">
								<strong>TELEFONE: </strong>{{$tce->concedente->dsFone}}</td>
							<td>
								<strong>OUTRO: </strong> @if  ( $tce->concedente->dsOutroFone != NULL) {{$tce->concedente->dsOutroFone}} @else - @endif
							</td>
						</tr>
                        
                        @if(($tce->nmSupervisor) AND ($tce->dsSupervisorCargo) != NULL)
						<tr>
							<td>
								<strong>SUPERVISOR(A): </strong>{{$tce->nmSupervisor}}</td>
							<td>
								<strong>CARGO/FORMAÇÃO: </strong>{{$tce->dsSupervisor}}</td>
						</tr> 
                        @endif

					</tbody>
				</table>
				<table style="width: 100%;" class="table table-bordered table-condensed">
					<tr>
						<th colspan="3" style="text-align: center">
							<strong>DADOS DO ESTÁGIARIO </strong>
						</th>
					</tr>
					<tr>
						<td colspan="">
							<strong>NOME: </strong>ALYSSON DE BARROS GONÇALVES LIMA</td>
						<td>
							<strong>DATA NASC: </strong>28/06/2000</td>
						<td>
							<strong>CPF: </strong>055.067.231-13</td>
					</tr>
					<tr>
						<td>
							<strong>RG: </strong>3284105</td>
						<td>
							<strong>ORG.: </strong>SSP DF</td>
						<td>
							<strong>Email: </strong>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<strong>Endereço: </strong> COM RUA ACRE Q 5 CH 3 GL C, 60,CASA 60, N° 60, CHÁCARA</td>
						<td>
							<strong>Bairro: </strong>ANHANGUERA</td>
					</tr>
					<tr>
						<td colspan="1">
							<strong>Cidade: </strong>VALPARAISO DE GOIAS - GOIAS</td>
						<td>
							<strong>Telefone: </strong>(61) 9920-30088</td>
						<td>
							<strong>CEP: </strong>72870-508</td>
					</tr>
					<!--<tr><td><strong>Telefone: </strong>(61) 9920-30088</td><td><strong>Celular: </strong></td><td><strong>Outro: </strong> </td></tr>-->
					<tr>
						<td>
							<strong>Curso: </strong> ENSINO MEDIO</td>
						<td>
							<strong>Semestre: </strong>
							3º ano </td>
						<td>
							<strong>Turno: </strong>MANHÃ </td>
					</tr>
				</table>




				<table style="width: 100%; font-size: 8pt;">
					<tr>
						<td>Job:
							<strong>132-003</strong>
						</td>
						<td>Purchasers(s):
							<strong>Palmer</strong>
						</td>
					</tr>

					<tr>
						<td>Created:
							<strong>2004-08-13</strong>
						</td>
						<td>Last Change:
							<strong>2004-08-16 9:28 AM</strong>
						</td>
					</tr>

					<tr>
						<td>Address:
							<strong>667 Pine Lodge Dr.</strong>
						</td>
						<td>Legal:
							<strong>N/A</strong>
						</td>
					</tr>
				</table>

				<table style="width: 100%; border-top: 1px solid black; border-bottom: 1px solid black; font-size: 8pt;">

					<tr>
						<td>Model:
							<strong>Franklin</strong>
						</td>
						<td>Elevation:
							<strong>B</strong>
						</td>
						<td>Size:
							<strong>1160 Cu. Ft.</strong>
						</td>
						<td>Style:
							<strong>Reciprocating</strong>
						</td>
					</tr>

				</table>

				<table class="change_order_items">

					<tr>
						<td colspan="6">
							<h2>Standard Items:</h2>
						</td>
					</tr>

					<tbody>
						<tr>
							<th>Item</th>
							<th>Description</th>
							<th>Quantity</th>
							<th colspan="2">Unit Cost</th>
							<th>Total</th>
						</tr>

						<tr class="even_row">
							<td style="text-align: center">1</td>
							<td>Sprockets (13 tooth)</td>
							<td style="text-align: center">50</td>
							<td style="text-align: right; border-right-style: none;">$10.00</td>
							<td class="change_order_unit_col" style="border-left-style: none;">Ea.</td>
							<td class="change_order_total_col">$5,000.00</td>
						</tr>


						<tr class="odd_row">
							<td style="text-align: center">2</td>
							<td>Cogs (Cylindrical)</td>
							<td style="text-align: center">45</td>
							<td style="text-align: right; border-right-style: none;">$25.00</td>
							<td class="change_order_unit_col" style="border-left-style: none;">Ea.</td>
							<td class="change_order_total_col">$1125.00</td>
						</tr>

						<tr class="even_row">
							<td style="text-align: center">3</td>
							<td>Gears (15 tooth)</td>
							<td style="text-align: center">32</td>
							<td style="text-align: right; border-right-style: none;">$19.00</td>
							<td class="change_order_unit_col" style="border-left-style: none;">Ea.</td>
							<td class="change_order_total_col">$608.00</td>
						</tr>

						<tr class="odd_row">
							<td style="text-align: center">4</td>
							<td>Leaf springs (13 N/m)</td>
							<td style="text-align: center">6</td>
							<td style="text-align: right; border-right-style: none;">$125.00</td>
							<td class="change_order_unit_col" style="border-left-style: none;">Ea.</td>
							<td class="change_order_total_col">$750.00</td>
						</tr>

						<tr class="even_row">
							<td style="text-align: center">5</td>
							<td>Coil springs (6 N/deg)</td>
							<td style="text-align: center">7</td>
							<td style="text-align: right; border-right-style: none;">$11.00</td>
							<td class="change_order_unit_col" style="border-left-style: none;">Ea.</td>
							<td class="change_order_total_col">$77.00</td>
						</tr>

					</tbody>




					<tr>
						<td colspan="3" style="text-align: right;">(Tax is not included; it will be collected on closing.)</td>
						<td colspan="2" style="text-align: right;">
							<strong>GRAND TOTAL:</strong>
						</td>
						<td class="change_order_total_col">
							<strong>$7560.00</strong>
						</td>
					</tr>
				</table>

				<table class="sa_signature_box" style="border-top: 1px solid black; padding-top: 2em; margin-top: 2em;">

					<tr>
						<td>WITNESS:</td>
						<td class="written_field" style="padding-left: 2.5in">&nbsp;</td>
						<td style="padding-left: 1em">PURCHASER:</td>
						<td class="written_field" style="padding-left: 2.5in; text-align: right;">X</td>
					</tr>
					<tr>
						<td colspan="3" style="padding-top: 0em">&nbsp;</td>
						<td style="text-align: center; padding-top: 0em;">Mr. Leland Palmer</td>
					</tr>

					<tr>
						<td colspan="4" style="white-space: normal">
							This change order shall have no force or effect until approved and signed by an authorizing signing officer of the supplier.
							Any change or special request not noted on this document is not contractual.
						</td>
					</tr>

					<tr>
						<td colspan="2">ACCEPTED THIS
							<span class="written_field" style="padding-left: 4em">&nbsp;</span>
							DAY OF
							<span class="written_field" style="padding-left: 8em;">&nbsp;</span>, 20
							<span class="written_field" style="padding-left: 4em">&nbsp;</span>.
						</td>

						<td colspan="2" style="padding-left: 1em;">TWIN PEAKS SUPPLY LTD.
							<br/>
							<br/> PER:
							<span class="written_field" style="padding-left: 2.5in">&nbsp;</span>
						</td>
					</tr>
				</table>

			</div>

		</div>
	</div>

	<script type="text/php">

		if ( isset($pdf) ) { $font = Font_Metrics::get_font("verdana"); // If verdana isn't available, we'll use sans-serif. if (!isset($font))
		{ Font_Metrics::get_font("sans-serif"); } $size = 6; $color = array(0,0,0); $text_height = Font_Metrics::get_font_height($font,
		$size); $foot = $pdf->open_object(); $w = $pdf->get_width(); $h = $pdf->get_height(); // Draw a line along the bottom $y
		= $h - 2 * $text_height - 24; $pdf->line(16, $y, $w - 16, $y, $color, 1); $y += $text_height; $text = "Job: 132-003"; $pdf->text(16,
		$y, $text, $font, $size, $color); $pdf->close_object(); $pdf->add_object($foot, "all"); global $initials; $initials = $pdf->open_object();
		// Add an initals box $text = "Initials:"; $width = Font_Metrics::get_text_width($text, $font, $size); $pdf->text($w -
		16 - $width - 38, $y, $text, $font, $size, $color); $pdf->rectangle($w - 16 - 36, $y - 2, 36, $text_height + 4, array(0.5,0.5,0.5),
		0.5); $pdf->close_object(); $pdf->add_object($initials); // Mark the document as a duplicate $pdf->text(110, $h - 240,
		"DUPLICATE", Font_Metrics::get_font("verdana", "bold"), 110, array(0.85, 0.85, 0.85), 0, 0, -52); $text = "Page {PAGE_NUM}
		of {PAGE_COUNT}"; // Center the text $width = Font_Metrics::get_text_width("Page 1 of 2", $font, $size); $pdf->page_text($w
		/ 2 - $width / 2, $y, $text, $font, $size, $color); }
	</script>

</body>

</html>