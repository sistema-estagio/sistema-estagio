<!DOCTYPE html>
<html>
<head>
	<title>Reatorio Tce Normal</title>
	<!-- Bootstrap 4-ONLINE -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb"
		  crossorigin="anonymous" media="all">
	<style>

		body {
            margin: 10px 25px;
			/*margin: 0px 50px !important;*/
			font-size: 11px;
		}

		footer {
			width: 100%;
			height: 100px;
			position: absolute;
			bottom: -10px;
		}

		.novaPagina {
			page-break-before: always;
			page-break-after: always
		}

		[class*="texto"] {
			font-size: 10px;
		}

		@media print {
			[class*="col-sm-sm-"] {
				float: left;
			}

			[class*="col-sm-xs-"] {
				float: left;
			}

			.col-sm-sm-12,
			.col-sm-xs-12 {
				width: 100% !important;
			}

			.col-sm-sm-11,
			.col-sm-xs-11 {
				width: 91.66666667% !important;
			}

			.col-sm-sm-10,
			.col-sm-xs-10 {
				width: 83.33333333% !important;
			}

			.col-sm-sm-9,
			.col-sm-xs-9 {
				width: 75% !important;
			}

			.col-sm-sm-8,
			.col-sm-xs-8 {
				width: 66.66666667% !important;
			}

			.col-sm-sm-7,
			.col-sm-xs-7 {
				width: 58.33333333% !important;
			}

			.col-sm-sm-6,
			.col-sm-xs-6 {
				width: 50% !important;
			}

			.col-sm-sm-5,
			.col-sm-xs-5 {
				width: 41.66666667% !important;
			}

			.col-sm-sm-4,
			.col-sm-xs-4 {
				width: 33.33333333% !important;
			}

			.col-sm-sm-3,
			.col-sm-xs-3 {
				width: 25% !important;
			}

			.col-sm-sm-2,
			.col-sm-xs-2 {
				width: 16.66666667% !important;
			}

			.col-sm-sm-1,
			.col-sm-xs-1 {
				width: 8.33333333% !important;
			}

			.col-sm-sm-1,
			.col-sm-sm-2,
			.col-sm-sm-3,
			.col-sm-sm-4,
			.col-sm-sm-5,
			.col-sm-sm-6,
			.col-sm-sm-7,
			.col-sm-sm-8,
			.col-sm-sm-9,
			.col-sm-sm-10,
			.col-sm-sm-11,
			.col-sm-sm-12,
			.col-sm-xs-1,
			.col-sm-xs-2,
			.col-sm-xs-3,
			.col-sm-xs-4,
			.col-sm-xs-5,
			.col-sm-xs-6,
			.col-sm-xs-7,
			.col-sm-xs-8,
			.col-sm-xs-9,
			.col-sm-xs-10,
			.col-sm-xs-11,
			.col-sm-xs-12 {
				float: left !important;
			}

			body {
				margin: 0;
				padding: 0 !important;
				min-width: 768px;
			}

			.container {
				width: auto;
				min-width: 750px;
			}

			body {
				font-size: 11px;
			}

			a[href]:after {
				content: none;
			}

			.noprint,
			div.alert,
			header,
			.group-media,
			.btn,
			form,
			#comments,
			.nav,
			ul.links.list-inline,
			ul.action-links {
				display: none !important;
			}
		}
	</style>
</head>

<body>
	<div class="wrapper">
		<div class="row">
			<div class="col-sm-4">
				<img src="{{URL::asset('/img/relatorios/logo_default.png')}}" alt="Upa - Estagio" />
			</div>
			<div class="col-sm-8">

				<h6 style="text-align: right; margin-top: 40px;">
					<strong>TERMO DE COMPROMISSO DE ESTÁGIO - TCE {{$tce->id}}</strong>
				</h6>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">
				<p style="margin: 10px;text-align: center">As partes abaixo qualificadas celebram neste ato Termo de Compromisso de Estágio, conforme a Lei n°. 11.788/08.</p>
			</div>
		</div>

		<!--INSTITUIÇÃO DE ENSINO-->
		<div class="card">
			<h6 style="text-align: center; line-height: 0px;" class="card-header">
				<strong>DADOS DA INSTITUIÇÃO DE ENSINO</strong>
			</h6>
			<div class="card-block">
				<div class="row">
					<div class="col-sm-6">
						<strong>{{$tce->instituicao->nmInstituicao}}</strong>
					</div>
                    <div class="col-sm-6">
						<strong>CNPJ: </strong>{{$tce->instituicao->cdCNPJ}}
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<strong>ENDEREÇO: </strong>{{$tce->instituicao->dsEndereco}} , <strong>N°</strong> {{$tce->instituicao->nnNumero}}
					</div>
					<div class="col-sm-6">
						<strong>CIDADE: </strong>{{$tce->instituicao->cidade->nmCidade}} - {{$tce->instituicao->cidade->estado->nmEstado}}
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<strong>REPRESENTANTE: </strong>{{$tce->instituicao->nmDiretor}}
					</div>
					<div class="col-sm-6">
						<strong>TELEFONE: </strong>{{$tce->instituicao->dsFone}}
					</div>
				</div>
                @if($tce->instituicao->nmReitor <> NULL)
                <div class="row">
					<div class="col-sm-12">
						<strong>REITOR: </strong>{{$tce->instituicao->nmReitor}}
					</div>
				</div>
                @endif
				<div class="row">
                    @if(($tce->nmSupervisorIe) AND ($tce->dsSupervisorIeCargo))
					<div class="col-sm-6">
						<strong>SUPERVISOR(A): </strong>{{$tce->nmSupervisorIe}}
					</div>
					<div class="col-sm-6">
						<strong>CARGO/FORMAÇÃO: </strong>{{$tce->dsSupervisorIeCargo}}
					</div>
                    @else
                        @if(($tce->instituicao->nmSupervisor) AND ($tce->instituicao->nmSupervisorCargo))
                        <div class="col-sm-6">
						    <strong>SUPERVISOR(A): </strong>{{$tce->instituicao->nmSupervisor}}
                        </div>
                        <div class="col-sm-6">
                            <strong>CARGO/FORMAÇÃO: </strong>{{$tce->instituicao->nmSupervisorCargo}}
                        </div>
                        @endif
                    @endif
				</div>
			</div>
		</div>

		<!--CONCEDENTE-->
		<div class="card">
			<h6 style="text-align: center; line-height: 0px;" class="card-header">
				<strong>DADOS DA CONCEDENTE</strong>
			</h6>
			<div class="card-block">
				<div class="row">
					<div class="col-sm-12">
						<strong>LOCAL DE REALIZAÇÃO DE ESTÁGIO: </strong>{{$tce->concedente->nmRazaoSocial}}
					</div>
				</div>
				<div class="row">
                    @if($tce->concedente->flTipo == "PF")
					<div class="col-sm-6">
						<strong>CPF: </strong>{{$tce->concedente->cdCnpjCpf}}
					</div>
                    <div class="col-sm-6">
						<strong>ORGÃO REGULADOR: </strong>{{$tce->concedente->dsOrgaoRegulador}}
					</div>
                    @else
                    <div class="col-sm-6">
						<strong>CNPJ: </strong>{{$tce->concedente->cdCnpjCpf}}
					</div>
                    @endif
				</div>
				<div class="row">
					<div class="col-sm-6">
						<strong>ENDEREÇO: </strong>{{$tce->concedente->dsEndereco}} , <strong>N°</strong> {{$tce->concedente->nnNumero}}
					</div>
					<div class="col-sm-6">
						<strong>BAIRRO: </strong>{{$tce->concedente->nmBairro}}
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<strong>CIDADE: </strong>{{$tce->concedente->cidade->nmCidade}} - {{$tce->concedente->cidade->estado->nmEstado}}
					</div>
					<div class="col-sm-6">
						<strong>CEP: </strong>{{$tce->concedente->cdCEP}}
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<strong>RESPONSAVEL: </strong>{{$tce->concedente->nmResponsavel}}
					</div>
					<div class="col-sm-6">
						<strong>CARGO: </strong>{{$tce->concedente->dsResponsavelCargo}}
					</div>
				</div>
                @if(($tce->nmSupervisor) AND ($tce->dsSupervisorCargo))
				<div class="row">
					<div class="col-sm-6">
						<strong>SUPERVISOR(A): </strong>{{$tce->nmSupervisor}}
					</div>
					<div class="col-sm-6">
						<strong>CARGO: </strong>{{$tce->dsSupervisorCargo}}
					</div>
				</div>
                @endif
			</div>
		</div>
		<!--ESTAGIARIOS-->
		<div class="card">
			<h6 style="text-align: center; line-height: 0px;" class="card-header">
				<strong>DADOS DO ESTÁGIARIO</strong>
			</h6>
			<div class="card-block">
				<div class="row">
					<div class="col-sm-6">
						<strong>NOME: </strong>{{$tce->estudante->nmEstudante}}
					</div>
					<div class="col-sm-3">
						<strong>NASCIMENTO: </strong>{{$tce->estudante->dtNascimento->format('d/m/Y')}}
					</div>
					<div class="col-sm-3">
						<strong>CPF: </strong>{{$tce->estudante->cdCPF}}
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<strong>RG: </strong>{{$tce->estudante->cdRG}}
					</div>
					<div class="col-sm-3">
						<strong>ORG: </strong>{{$tce->estudante->dsOrgaoRG}}
					</div>
					<div class="col-sm-3">
						<strong>E-MAIL: </strong>{{$tce->estudante->dsEmail}}
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<strong>ENDEREÇO: </strong>{{$tce->estudante->dsEndereco}} <strong>N°</strong> {{$tce->estudante->nnNumero}}
					</div>
					<div class="col-sm-3">
						<strong>BAIRRO: </strong>{{$tce->estudante->nmBairro}}
					</div>
					<div class="col-sm-3">
						<strong>CEP: </strong>{{$tce->estudante->cdCEP}}
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<strong>CIDADE: </strong> {{$tce->estudante->cidade->nmCidade}} - {{$tce->estudante->cidade->estado->nmEstado}}
					</div>
					<div class="col-sm-6">
						<strong>TELEFONE: </strong>{{$tce->estudante->dsFone}}
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4">
						<strong>CURSO: </strong> {{$tce->cursoDaInstituicao->curso->nmCurso}}
					</div>
					<div class="col-sm-4">
						<strong>SEMESTRE/ANO/MODULO: </strong>{{$tce->nnSemestreAno}} {{$tce->cursoDaInstituicao->cursoDuracao->nmTipoDuracao}}
					</div>
					<div class="col-sm-4">
						<strong>TURNO: </strong>{{$tce->estudante->turno->nmTurno}}
					</div>
				</div>
			</div>
		</div>

		<div class="texto">
			<div class="row" id="cl1">
				<div class="col-sm-12">
					<p style="text-align: justify" class="termo">
						<strong>CLÁUSULA 1ª -</strong>
						Este instrumento tem por objetivo formalizar as condições para a realização de ESTÁGIO DE ESTUDANTE e particularizar a relação jurídica especial existente entre o ESTUDANTE, a CONCEDENTE e a INSTITUIÇÃO DE ENSINO caracterizando a não vinculação empregatícia, nos termos da Lei nº. 11.788, de 25 de setembro de 2008 e demais disposições vigentes. 
					</p>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
						<strong>CLÁUSULA 2ª - FICAM COMPROMISSADAS ENTRE AS PARTES AS SEGUINTES CONDIÇÕES PARA REALIZAÇÃO DO ESTÁGIO:</strong>
						<!--<br>O presente Termo de Compromisso de Estágio vigerá do dia _____/_____/_________ até _____/_____/_________-->
						<ol class="margin justi">
                            <li>Vigência de : <strong>{{$tce->dtInicio->format("d/m/Y")}} até {{$tce->dtFim->format("d/m/Y")}}</strong></li>
                            <li>Horário: varíavel em 30(Trinta) horas semanais</li>
                            <li>Bolsa-Auxílio mensal de: <strong>R${{number_format($tce->vlAuxilioMensal, 2, ',', '.')}}</strong> 
                    
                                @if (isset($tce->vlAuxilioAlimentacao) && !empty($tce->vlAuxilioAlimentacao) && !is_null($tce->vlAuxilioAlimentacao))
                                    @if ($tce->vlAuxilioTransporte)
                                    ,  Auxílio-Alimentação de <strong>R${{number_format($tce->vlAuxilioAlimentacao, 2, ',', '.')}}</strong>
                                    @else
                                    e  Auxílio-Alimentação de <strong>R${{number_format($tce->vlAuxilioAlimentacao, 2, ',', '.')}}</strong>
                                    @endif
                                @endif
                                @if (isset($tce->vlAuxilioTransporte) && !empty($tce->vlAuxilioTransporte) && !is_null($tce->vlAuxilioTransporte))
                                    @if ($tce->vlAuxilioAlimentacao)
                                    e Auxílio-Transporte de <strong>R${{number_format($tce->vlAuxilioTransporte, 2, ',', '.')}}</strong>
                                    @elseif(isset($tce->vlAuxilioAlimentacao) && isset($tce->vlAuxilioTransporte))
                                    e Auxílio-Transporte de <strong>R${{number_format($tce->vlAuxilioTransporte, 2, ',', '.')}}</strong>
                                    @else
                                    e Auxílio-Transporte de <strong>R${{number_format($tce->vlAuxilioTransporte, 2, ',', '.')}}</strong>
                                    @endif
                                @endif
                                @php
                                    $total = $tce->vlAuxilioMensal+$tce->vlAuxilioAlimentacao+$tce->vlAuxilioTransporte;
                                @endphp
                                @if(isset($tce->vlAuxilioAlimentacao) OR isset($tce->vlAuxilioTransporte))
                                totalizando <strong>R${{number_format($total, 2, ',', '.')}}.</strong>
                                @endif
                                </li>
                            <li>O ESTAGIÁRIO durante a vigência do presente Termo de Compromisso de Estágio estará segurando contra acidentes pessoais conforme Apólice Coletiva de n°. 7.607-0 da Porto Seguro Cia de Seguros Gerais a cargo da Universidade Patativa do Assaré - UPA.</li>
                        </ol>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<strong class="justi">CLÁUSULA 3ª - CABE À INSTITUIÇÃO DE ENSINO</strong>
					<ol class="margin justi">
                            <li>Aprovar o ESTÁGIO de que trata o presente instrumento, considerando as condições de sua adequação à proposta pedagógica do curso, à etapa e modalidade de formação escolar do ESTAGIÁRIO e ao horário e calendário escolar;</li>
                            <li>Aprovar o Plano de Atividades de Estágio que consubstancie as condições / requisitos suficientes à exigência legal de adequação à etapa e modalidade da formação escolar do ESTAGIÁRIO;</li>
                            <li>Avaliar as instalações da CONCEDENTE;</li>
                            <li>Indicar professor orientador, da área a ser desenvolvida no ESTÁGIO, como responsável pelo acompanhamento e avaliação das atividades do ESTAGIÁRIO;</li>
                            <li>Comunicar à parte concedente do estágio, no início do período letivo, as datas de realização de avaliações escolares ou acadêmicas, caso não adote outro sistema de avaliação.</li>
                    </ol>
				</div>
			</div>
			<div class="row" id="cl4">
				<div class="col-sm-12">
					<strong class="justi">CLÁUSULA 4ª - CABE À CONCEDENTE</strong>
                    <ol class="margin justi">
                        <li>Zelar pelo cumprimento do presente Termo de Compromisso de Estágio;</li>
                        <li>Proporcionar ao ESTAGIÁRIO instalações que tenham condições para o exercício das atividades práticas compatíveis com plano de atividades de estágio;</li>
                        <li>Designar um supervisor que seja funcionário de seu quadro de pessoal, com formação ou experiência profissonal na área de conhecimento desenvolvida no curso do ESTAGIÁRIO, para orientá-lo e acompanhá-lo no desenvolvimento das atividades do estágio;</li>
                        <li>Solicitar ao ESTAGIÁRIO, a qualquer tempo, documentos comprobátorios da regularidade da situação escolar, uma vez que trancar a matrícula, abandono, conclusão de curso ou transferência de Instituição de Ensino constituem motivos de imediata rescisão;</li>
                        <li>Efetuar o pagamento da bolsa-auxílio e conceder auxílio transporte diretamente ao ESTAGIÁRIO;</li>
                        <li>Conceder recesso remunerado a ser gozado preferenciamente durante as férias escolares, no termos da legislação vigente 11788/08;</li>
                        <li>Reduzir a jornada de estágio nos períodos de avaliação, previamente informados pelo ESTAGIÁRIO;</li>
                        <li>Elaborar os Rélatorios de Atividades semestrais para encaminhamento a Instituição de Ensino com vista obrigatória do ESTAGIÁRIO;</li>
                        <li>Entregar, por ocasião do desligamento, Termo de Realização do Estágio com indicação resumida das atividades desenvolvidas, dos períodos e da avaliação de desempenho;</li>
                        <li>Manter em arquivo e à disposição da fiscalização os documentos firmados que comprovem a relação de estágio;</li>
                        <li>Informar a Universidade Patativa do Assaré - UPA a rescisão antecipada deste instrumento, para as devidas providencias administrativas que se fizerem necessárias;</li>
                        <li>Permitir o início das atividades de estágio apenas após o recebimento deste instrumento assinado pelas 3 (três) partes signatárias.</li>
                    </ol>
                </div>
			</div>
			<div class="row" id="cl5">
				<div class="col-sm-12 justi">
					<strong>CLÁUSULA 5ª - CABE AO ESTAGIÁRIO</strong>
					<ol class="margin">
						<li>Cumprir, com todo empenho e interesse, toda programação estabelecida para seu ESTÁGIO;</li>
                        <li>Observar, obedecer e cumprir as normas internas da CONCEDENTE, preservando o sigilo e a confidencialidade das informações que tiver acesso;</li>
                        <li>Apresentar documentos comprobatórios da regularidade de sua situação escolar, sempre que solicitado pela CONCEDENTE;</li>
                        <li>Manter rigorosamente atualizados seus dados cadastrais e escolares, junto à Concedente e a Instituição de Ensino;</li>
                        <li>Informar de imediato, qualquer alteração na sua situação escolar, tais como: trancamento de matrícula, abandono, conclusão de curso ou transferência de Instituição de Ensino;</li>
                        <li>Entregar, obrigatoriamente, à Instituição de Ensino, à Concedente uma via do presente instrumento, devidamente assinado pelas partes;</li>
                        <li>Informar previamente à CONCEDENTE os períodos de avaliação na Instituição de Ensino, para fins de redução da jornada de estágio;</li>
                        <li>Fornecer semestralmente os Rélatórios de Atividades previstos na lei 11.788, de 25 de setembro de 2008 e demais disposições vigentes.</li>
					</ol>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 columns justi">
					<strong>CLÁUSULA 6ª -</strong> O presente instrumento e o Plano de Atividades de Estágio serão alterados ou prorrogados através dos TERMOS ADITIVOS.
                    <p>
                        <strong>PARÁGRAFO PRIMEIRO: </strong>O presente Termo de Compromisso de Estágio pode ser denunciado, a qualquer tempo, mediante a comunicação escrita, pela Instituição de Ensino, pela Concedente ou pelo Estagiário.<br>
                        <strong>PARÁGRAFO SEGUNDO: </strong>O não cumprimento de quaisquer cláusulas do presente TERMO DE COMPROMISSO DE ESTÁGIO constitui motivos de imediata rescisão.
                    </p>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 justi">
					<strong>CLÁUSULA 7ª -</strong> A INSTITUIÇÃO DE ENSINO, a CONCEDENTE e o ESTAGIÁRIO, signatários deste instrumento, de comum acordo e para os efeitos da Lei n°. 11.788/08 elegem a Universidade Patativa do Assaré - UPA como seu AGENTE DE INTEGRAÇÃO aquem comunicarão a interrupção ou eventuais modificações do convencionamento no presente instrumento.
						<br>
						<br> 
                        E, por estarem de inteiro e comum acordo com o Plano de Atividades em anexo e com as demais condições estabelecidas
						neste TERMO DE COMPROMISSO DE ESTÁGIO-TCE, as partes os assinam em 4 (quatro) vias de igual teor.
				</div>
			</div>

            <div class="row">
				<div class="col-sm-12 justi">
					<strong>PLANO DE ATIVIDADES DE ESTÁGIO</strong>
						<ol class="margin">
                            @foreach($tce->atividades as $atividade)	
                                <li>{{$atividade->atividade}}</li>
                            @endforeach
                        </ol>
				</div>
			</div>
            
			<div class="row" style="margin-top: 10px;">
				<div class="col-sm-12">
					<p style="float: right;">______________________, _____ de ______________ de _______. </p>
				</div>
			</div>



			<div class="row " style="margin-top: 20px;">

				<div class="col-sm-6">
					<div class="row">
						<div class="col-sm-8" style="font-size: 11px">
							<hr style=" margin-bottom: 0px; border-bottom: 1px solid #333 ;" />
							<p class="p">{{$tce->concedente->nmRazaoSocial}}</p>
						</div>
						<div class="col-sm-8" style="font-size: 11px; margin-top: 40px;">
							<hr style=" margin-bottom: 0px; border-bottom: 1px solid #333 ;" />
							<p class="p">{{$tce->instituicao->nmInstituicao}}</p>
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="row">
						<div class="col-sm-8" style="font-size: 11px; float: right;">
							<hr style=" margin-bottom: 0px; border-bottom: 1px solid #333 ;" />
							<p class="p">{{$tce->estudante->nmEstudante}} </p>
						</div>
						<!--<div class="span4" style="font-size: 11px; margin-top: 39px;">
						<hr style=" margin-bottom: 0px; border-bottom: 1px solid #333 ;" />
						<p class="p"></p>
					</div>-->
					</div>
				</div>
			</div>
			<div class="row" id="rodape">		
			    
            </div>
            @include('tce.relatorio.inc_rodape')
		</div>
</body>

</html>