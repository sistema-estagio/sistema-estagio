@extends('adminlte::page')
@if(!empty($titulo))
    @section('title_postfix', ' - '.$titulo)
@endif
@section('content_header')

    <h1>Aditivo</h1>
    <ol class="breadcrumb">
        {{--  {{route('estudante.index')}}  --}}
        <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('tce.index')}}"><i class="fa fa-files-o"></i>TCE</a></li>
        <li><a href="{{route('tce.show', ['tce' => $aditivo->tce_id])}}"><i class="fa fa-files-o"></i> TCE {{$aditivo->tce_id}}</a></li>
        <li class="active"><i class="fa fa-file-text"></i> Aditivo {{$aditivo->nnAditivo}}</li>
    </ol>
@stop

@section('content')
    @if(session('success'))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> Alerta!</h4>
            {{session('success')}}
    </div>
    @endif

    @if(session('error'))
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Alerta!</h4>
            {{session('error')}}
    </div>
    @endif    
    
    <h4 class="page-header"><strong>TCE:</strong> {{$aditivo->tce_id}} - <strong>Estudante:</strong> {{$aditivo->tce->estudante->nmEstudante}} - <strong>Instituição de Ensino:</strong> {{$aditivo->tce->instituicao->nmInstituicao}} - <strong>Curso:</strong> {{$aditivo->tce->cursoDaInstituicao->curso->nmCurso}}</h4>
    <!-- form start -->
    <form action="{{route('tce.aditivo.update', ['tce' => $aditivo->tce_id, 'aditivo' => $aditivo->id])}}" method="POST">
    <input id="curso_atv_id" name="curso_atv_id" type="hidden" value="{{$aditivo->tce->cursoDaInstituicao->curso->id}}">
    <input id="estudante_id" name="estudante_id" type="hidden" value="{{$aditivo->tce->estudante_id}}">
    <input id="tce_id" name="tce_id" type="hidden" value="{{$aditivo->tce_id}}">
    <input id="estado_id" name="estado_id" type="hidden" value="{{$aditivo->tce->estudante->estado_id}}">
    {!! csrf_field() !!}
    {!! method_field('PUT') !!}
    <div class="box box-default">
        <div class="box-body">
            <div class="row ">
                <div class="col-md-2">
                    <div class="form-group">
                        <label>Data do Aditivo:</label>
                        <input class="form-control" id="dtAditivo" name="dtAditivo" type="text" value="{{$aditivo->dtAditivo->format("d/m/Y")}}">
                    </div>
                </div>
                <div class="col-md-10">
                    <div class="form-group">
                        <label>Outro Motivo de Aditivo:</label>
                        <input class="form-control" id="aditivoOutro" name="aditivoOutro" type="text" value="{{$aditivo->aditivoOutro}}">
                        <span id="helpBlock" class="help-block h6">Caso seu aditivo não se enquadre em nenhuma situação seguinte, descreva seu aditivo. Ex.: Alteração dados Estudante.</span>
                    </div>
                </div>
                
                
            </div>
            <!-- /.row -->
        </div>
    </div>
    <div class="box box-default">
        <div class="box-body">
        
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="nmSupervisor">Supervisor Empresa</label>
                        <input class="form-control" id="nmSupervisor" name="nmSupervisor" type="text" placeholder="Nome do Supervisor da Empresa" value="{{$aditivo->nmSupervisor}}">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="dsSupervisorCargo">Cargo Supervisor Empresa</label>
                        <input class="form-control" id="dsSupervisorCargo" name="dsSupervisorCargo" type="text" placeholder="Cargo do Supervisor da Empresa" value="{{$aditivo->dsSupervisorCargo}}">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="nmSupervisorIe">Supervisor Instituição de Ensino</label>
                        <input class="form-control" id="nmSupervisorIe" name="nmSupervisorIe" type="text" placeholder="Nome do Supervisor da Instituição" value="{{$aditivo->nmSupervisorIe}}">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="dsSupervisorIeCargo">Cargo Supervisor Instituição de Ensino</label>
                        <input class="form-control" id="dsSupervisorIeCargo" name="dsSupervisorIeCargo" type="text" placeholder="Cargo do Supervisor da Instituição" value="{{$aditivo->dsSupervisorIeCargo}}">
                    </div>
                </div>
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="dtIniio">*Inicio</label>
                        @if($aditivo->dtInicio)
                        <input class="form-control" id="dtInicio" name="dtInicio" type="text" value="{{$aditivo->dtInicio->format("d/m/Y")}}">
                        @else
                        <input class="form-control" id="dtInicio" name="dtInicio" type="text">
                        @endif
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="escolheMes">Meses</label>
                        <select class="form-control" id="escolheMes" name="escolheMes" onchange="escolheMeses();">
                            <option value="" selected>Selecione...</option>
                            <option value="1">1 MÊS</option>
                            <option value="3">3 MESES</option>
                            <option value="6">6 MESES</option>
                            <option value="12">12 MESES</option>
                            <option value="24">24 MESES</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="dtFim">*Fim</label>
                        @if($aditivo->dtInicio)
                        <input class="form-control" id="dtFim" name="dtFim" type="text" value="{{$aditivo->dtFim->format("d/m/Y")}}">
                        @else
                        <input class="form-control" id="dtFim" name="dtFim" type="text">
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="dsLocatao">Lotação do Estágio</label>
                        <input class="form-control" id="dsLotacao" name="dsLotacao" type="text" placeholder="Lotação do Estágio" value="{{$aditivo->dsLotacao}}">
                    </div>
                </div>
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="vlAuxilioMensal">Auxilio Mensal</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                            <input class="form-control" type="text" placeholder="Valor" id="vlAuxilioMensal" name="vlAuxilioMensal" type="text" value="@if($aditivo->vlAuxilioMensal){{number_format($aditivo->vlAuxilioMensal,2,",",".")}}@endif">
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="vlAuxilioAlimentacao">Auxilio Alimentação</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                            <input class="form-control" placeholder="Valor" id="vlAuxilioAlimentacao" name="vlAuxilioAlimentacao" value="@if($aditivo->vlAuxilioAlimentacao){{number_format($aditivo->vlAuxilioAlimentacao,2,",",".")}}@endif">
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="vlAuxilioTransporte">Auxilio Transporte</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                            <input class="form-control" placeholder="Valor" id="vlAuxilioTransporte" name="vlAuxilioTransporte" type="text" value="@if($aditivo->vlAuxilioTransporte){{number_format($aditivo->vlAuxilioTransporte,2,",",".")}}@endif">
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box box-default  -->

    <div class="box box-default">
        <div class="box-body">
            
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        <label>Semestre/Ano do TCE:</label>
                        <br>{{$aditivo->tce->nnSemestreAno}}
                    </div>
                </div>
                @foreach($aditivosSemestre as $aditivoSemestre)
                <div class="col-md-3">
                    <div class="form-group">
                        {{$aditivoSemestre->dtAditivo->format('d/m/Y')}}<br>
                        <label>Semestre/Ano Aditivo({{$aditivoSemestre->nnAditivo}}):</label>
                        <br>{{$aditivoSemestre->nnSemestreAno}}
                    </div>
                </div>
                @endforeach
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="nnSemestreAno">Semestre Atual:</label>
                        <div class="input-group">
                            <input class="form-control" id="nnSemestreAno" name="nnSemestreAno" type="text" placeholder="Semestre ou Ano">
                        </div>
                    </div>
                </div>
                
            </div>
            <!-- /.row -->
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box box-default  -->

    <div class="box box-default collapsed-box">
        <div class="box-header with-border">
            <h3 class="box-title" data-widget="collapse"><strong>Atividades Aditivo</strong></h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                    <i class="fa fa-plus"></i>
                </button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body" style="display: none;">
            
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-success">
                        <div class="panel-body">
                            <h4><strong>Atividades Escolhidas no Aditivo</strong></h4>
                        </div>
                        <div class="panel-footer">                            
                            @foreach($aditivo->atividades as $atividade)
                            <label><input type='checkbox' name='atividades[]' value="{{$atividade['id']}}" checked> {{$atividade['atividade']}}</label><br>
                            @endforeach
                        </div>
                    </div>
                    <div class="well" id="dvAtividades"></div>
                </div>
            </div>
            <!-- /.row -->

        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box box-default collapsed-box -->


    <div class="text-center">
        <button type="submit" class="btn btn-danger">Cancelar</button>
        <button type="submit" class="btn btn-success">Salvar Aditivo</button>
    </div>
</form>
@section('post-script')
<script>
    $(document).ready(function(){
        curso_id = $("#curso_atv_id").val();
        $.get('../../../../get-atividades/' + curso_id, function (atividades) {
                    $.each(atividades, function (key, value) {
                    //$('select[name=cidade_id]').append('<option value=' + value.id + '>' + value.nmCidade + '</option>');
                    $("#dvAtividades").append("<label><input type='checkbox' name='atividades[]' value='" + value.id + "'> " + value.atividade + "</label><br>");
                    });
        });
    
    });
    jQuery(function($){
        $("#dtAditivo").mask("99/99/9999");
        $("#dtInicio").mask("99/99/9999");
        $("#dtFim").mask("99/99/9999");
        $('#vlAuxilioMensal').maskMoney({thousands:'.',decimal:','});
        $('#vlAuxilioAlimentacao').maskMoney({thousands:'.',decimal:','});
        $('#vlAuxilioTransporte').maskMoney({thousands:'.',decimal:','});
    });
    
    function escolheMeses() {
        dtInicio = $("#dtInicio").val();
        var mes = $("#escolheMes").val();
        if (mes != "") {
            var split = dtInicio.split('/');
            novadata = split[2] + "," +split[1]+","+split[0];
            
            data_americana = new XDate(novadata);
            data_americana.addMonths(mes);

            while ((data_americana.getDay() == 0) || (data_americana.getDay() == 6)) {
                data_americana.addDays(1);
            }
            $("#dtFim").val(data_americana.toString("dd/MM/yyyy"));
        }
    }
</script>

@endsection
@stop