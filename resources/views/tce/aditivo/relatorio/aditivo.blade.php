<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<title>Aditivo {{$aditivo->nnAditivo}} - TCE
		@if($aditivo->tce->migracao != NULL)
			{{$aditivo->tce->migracao}}
		@else
			{{$aditivo->tce_id}}
		@endif
	</title>
	<style>
		html { margin: 30px}
		body {
			font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif;
		}
		footer.fixar-rodape{

			bottom: 0;
			left: 0;
			height: 40px;
			position: fixed;
			width: 100%;
		  }
		.novaPagina {
			page-break-before: always;
			/**page-break-after: always*/
		}
		div.body-content{
			/** Essa margem vai evitar que o conteudo fique por baixo do rodapé **/
			margin-bottom: 40px;
		}

		.col-12 {
			width: 100% !important;
		}

		.col-6 {
			width: 50% !important;
		}
		.col-4 {
			width: 30% !important;
		}
		.col-4-2 {
			width: 30% !important;
		}
		.col-4,
		.col-6,
		.col-12 {
			float: left !important;
		}
		.col-4-2{
			float: right !important;
		}
		hr.linha{width:80%;}
		.table {
			border-collapse: separate;
			border-spacing: 0;
			min-width: 350px;
			margin-bottom: 10px;
		}

		.table tr th,
		.table tr td {
			border-right: 1px solid #bbb;
			border-bottom: 1px solid #bbb;
			padding: 3px;
		}

		.table tr th:first-child,
		.table tr th:last-child {
			border-top: solid 1px #bbb;
		}

		.table tr th:first-child,
		.table tr td:first-child {
			border-left: 1px solid #bbb;
		}

		.table tr th:first-child,
		.table tr td:first-child {
			border-left: 1px solid #bbb;
		}

		.table tr th {
			background: #eee;
			text-align: left;
		}

		.table.Info tr th,
		.table.Info tr:first-child td {
			border-top: 1px solid #bbb;
		}
		/* top-left border-radius */

		.table tr:first-child th:first-child,
		.table.Info tr:first-child td:first-child {
			border-top-left-radius: 6px;
		}
		/* top-right border-radius */

		.table tr:first-child th:last-child,
		.table.Info tr:first-child td:last-child {
			border-top-right-radius: 6px;
		}
		/* bottom-left border-radius */

		.table tr:last-child td:first-child {
			border-bottom-left-radius: 6px;
		}
		/* bottom-right border-radius */

		.table tr:last-child td:last-child {
			border-bottom-right-radius: 6px;
		}
	</style>
</head>

<body>

	<div id="body">

		<div id="content">

			<div class="page" style="font-size: 7pt">
				<table style="width: 100%;" class="header">
					<tr>
						<td>
							<?php $image_path = '/img/relatorios/logo_default.png'; ?>
							<img src="{{ public_path() . $image_path }}" alt="Upa - Estagio" />
						</td>
						<td>
							<h1 style="text-align: right">
								<strong>TERMO ADITIVO - TCE
									@if($aditivo->tce->migracao != NULL)
									{{$aditivo->tce->migracao}}/{{$aditivo->nnAditivo}}
									@else
									{{$aditivo->tce_id}}/{{$aditivo->nnAditivo}}
									@endif
								</strong>
							</h1>
						</td>
					</tr>
				</table>

				<div class="row">
					<div class="span12">
						<p style="margin: 10px;">As partes abaixo qualificadas celebram neste ato Termo de Aditivo de Estágio, conforme a Lei n°. 11.788/08.</p>
					</div>
				</div>

				@if($aditivo->tce->instituicao->poloMatriz != NULL)
				<!-- SE INSTITUIÇÃO TIVER POLO MATRIZ DEFINIDA EXIBE ENDEREÇO E DADOS DO POLO MATRIZ -->
				<!--INSTITUIÇÃO DE ENSINO COM POLO-->
				<table style="width: 100%;" class="table table-bordered table-condensed ">
						<tr>
							<th colspan="2" style="text-align: center">
								<strong>DADOS DA INSTITUIÇÃO DE ENSINO</strong><span style="float: right;font-size: 5pt"> (Cod.: {{$aditivo->tce->instituicao_id}})</span>
							</th>
						</tr>
						<tr>
							<td colspan="2">
								<strong>{{$aditivo->tce->instituicao->poloMatriz->nmPolo}}({{$aditivo->tce->instituicao->nmInstituicao}})</strong>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<strong>CNPJ:</strong> {{$aditivo->tce->instituicao->poloMatriz->cdCNPJ}}
							</td>
						</tr>
						<tr>
							<td>
								<strong>ENDEREÇO: </strong>{{$aditivo->tce->instituicao->poloMatriz->dsEndereco}} ,
								<strong>N°</strong> {{$aditivo->tce->instituicao->poloMatriz->nnNumero}}
							</td>
							<td>
								<strong>CIDADE: </strong>{{$aditivo->tce->instituicao->poloMatriz->cidade->nmCidade}} - {{$aditivo->tce->instituicao->poloMatriz->cidade->estado->nmEstado}}</td>
						</tr>
						<tr>
							@if($aditivo->tce->instituicao->poloMatriz->nmDiretor <> NULL)
							<td>
								<strong>REPRESENTANTE: </strong>{{$aditivo->tce->instituicao->poloMatriz->nmDiretor}}
							</td>
							@else
							<td>
								<strong>REPRESENTANTE: </strong>{{$aditivo->tce->instituicao->nmDiretor}}
							</td>
							@endif
							<td>
								<strong>TELEFONE: </strong> {{$aditivo->tce->instituicao->poloMatriz->dsFone}}
							</td>
						</tr>
						<!--SE TIVER SIDO PREENCHIDO NMREITOR NO POLO-->
						@if($aditivo->tce->instituicao->poloMatriz->nmReitor <> NULL)
							<tr>
								<td colspan="2">
									<strong>REITOR(A) MATRIZ: </strong>{{$aditivo->tce->instituicao->poloMatriz->nmReitor}}
								</td>
							</tr>
						<!--SE NÂO TIVER PREENCHIDO NMREITOR NO POLO-->
						@else
							<!--SE TIVER PREENCHIDO NMREITOR NA INSTITUICAO-->
							@if($aditivo->tce->instituicao->nmReitor <> NULL)
							<tr>
								<td colspan="2">
									<strong>REITOR(A): </strong>{{$aditivo->tce->instituicao->nmReitor}}
								</td>
							</tr>
							@endif
						@endif

						@if(($aditivo->tce->nmSupervisorIe) AND ($aditivo->tce->dsSupervisorIeCargo) != NULL)
							<tr>
								<td>
									<strong>SUPERVISOR(A): </strong>{{$aditivo->tce->nmSupervisorIe}}
								</td>
								<td>
									<strong>CARGO/FORMAÇÃO: </strong>{{$aditivo->tce->dsSupervisorIeCargo}}
								</td>
							</tr>
						@else
								@if(($aditivo->tce->instituicao->nmSupervisor) AND ($aditivo->tce->instituicao->nmSupervisorCargo))
								<tr>
									<td>
										<strong>SUPERVISOR(A): </strong>{{$aditivo->tce->instituicao->nmSupervisor}}
									</td>
									<td>
										<strong>CARGO/FORMAÇÃO: </strong>{{$aditivo->tce->instituicao->nmSupervisorCargo}}
									</td>
								</tr>
								@endif
						@endif
				</table>
				@else
				<!-- FIM SE INSTITUIÇÃO NÃO TIVER POLO MATRIZ DEFINIDA EXIBE ENDEREÇO E DADOS DO POLO MATRIZ -->
				<!--INSTITUIÇÃO DE ENSINO-->
				<table style="width: 100%;" class="table table-bordered table-condensed ">
						<tr>
							<th colspan="4" style="text-align: center">
								<strong>DADOS DA INSTITUIÇÃO DE ENSINO</strong><span style="float: right;font-size: 5pt"> (Cod.: {{$aditivo->tce->instituicao_id}})</span>
							</th>
						</tr>
						<tr>
							<td colspan="4">
								<strong>{{$aditivo->tce->instituicao->nmInstituicao}}</strong>
							</td>
						</tr>
						<tr>
							<td colspan="4">
								<strong>CNPJ:</strong> {{$aditivo->tce->instituicao->cdCNPJ}}
							</td>
						</tr>
						<tr>
							<td>
								<strong>ENDEREÇO: </strong>{{$aditivo->tce->instituicao->dsEndereco}} ,
								<strong>N°</strong> {{$aditivo->tce->instituicao->nnNumero}}
							</td>
							<td><strong>CEP: </strong>{{$aditivo->tce->instituicao->cdCEP}} </td>
							<td colspan="2">
								<strong>CIDADE: </strong>{{$aditivo->tce->instituicao->cidade->nmCidade}} - {{$aditivo->tce->instituicao->cidade->estado->nmEstado}}
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<strong>REPRESENTANTE: </strong>{{$aditivo->tce->instituicao->nmDiretor}}
							</td>
							<td colspan="2">
								<strong>TELEFONE: </strong> {{$aditivo->tce->instituicao->dsFone}}
							</td>
						</tr>
						@if($aditivo->tce->instituicao->nmReitor <> NULL)
							<tr>
								<td colspan="4">
									<strong>REITOR(A): </strong>{{$aditivo->tce->instituicao->nmReitor}}
								</td>
							</tr>
						@endif

						@if((($aditivo->nmSupervisorIe) AND ($aditivo->dsSupervisorIeCargo) != NULL) OR (($aditivo->nmSupervisorIe) AND ($aditivo->dsSupervisorIeCargo) != ""))
							<tr>
								<td colspan="2">
									<strong>SUPERVISOR(A): </strong>{{$aditivo->nmSupervisorIe}}
								</td>
								<td colspan="2">
									<strong>CARGO/FORMAÇÃO: </strong>{{$aditivo->dsSupervisorIeCargo}}
								</td>
							</tr>
						@elseif ($aditivo->tce->aditivos()->where('nmSupervisorIe','<>','')->latest()->first())
							<tr>
								<td colspan="2">
									<strong>SUPERVISOR(A): </strong>{{$aditivo->tce->aditivos()->where('nmSupervisorIe','<>','')->latest()->first()->nmSupervisorIe}}
								</td>
								<td colspan="2">
									<strong>CARGO/FORMAÇÃO: </strong>{{$aditivo->tce->aditivos()->where('dsSupervisorIeCargo','<>','')->latest()->first()->dsSupervisorIeCargo}}
								</td>
							</tr>
						@else
							@if(($aditivo->tce->nmSupervisorIe) AND ($aditivo->tce->dsSupervisorIeCargo))
								<tr>
									<td colspan="2">
										<strong>SUPERVISOR(A): </strong>{{$aditivo->tce->nmSupervisorIe}}
									</td>
									<td colspan="2">
										<strong>CARGO/FORMAÇÃO: </strong>{{$aditivo->tce->dsSupervisorIeCargo}}
									</td>
								</tr>
							@else
								@if(($aditivo->tce->instituicao->nmSupervisor) AND ($aditivo->tce->instituicao->nmSupervisorCargo))
								<tr>
									<td colspan="2">
										<strong>SUPERVISOR(A): </strong>{{$aditivo->tce->instituicao->nmSupervisor}}
									</td>
									<td colspan="2">
										<strong>CARGO/FORMAÇÃO: </strong>{{$aditivo->tce->instituicao->nmSupervisorCargo}}
									</td>
								</tr>
								@endif
							@endif
						@endif

				</table>
				@endif
				<!--CONCEDENTE-->
				<table style="width: 100%;" class="table table-bordered table-condensed">
					<tbody>
						<tr>
							<th colspan="3" style="text-align: center;">
								<strong>DADOS DA CONCEDENTE</strong><span style="float: right;font-size: 5pt"> (Cod.: {{$aditivo->tce->concedente_id}})</span>
							</th>
						</tr>
						<tr>
							<td colspan="3">
								<strong>LOCAL DE REALIZAÇÃO DE ESTÁGIO:</strong> {{$aditivo->tce->concedente->nmRazaoSocial}}
							</td>
						</tr>

						<!--SE O TCE FOR DE UMA SECRETARIA DA CONCEDENTE-->
						@if($aditivo->tce->sec_conc_id <> NULL)
						<tr>
							<td colspan="3">
								{{-- <strong>SECRETARIA:</strong> {{$aditivo->tce->secConcedente->nmSecretaria}} --}}
								<strong>UNIDADE:</strong> {{$aditivo->tce->secConcedente->nmSecretaria}}
							</td>
						</tr>
						<tr>
							<td colspan="3">
								<strong>CNPJ: </strong>{{$aditivo->tce->secConcedente->cdCnpjCpf}}
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<strong>ENDEREÇO: </strong> {{$aditivo->tce->secConcedente->dsEndereco}} ,
								<strong>N°</strong> {{$aditivo->tce->secConcedente->nnNumero}}
							</td>
							<td>
								<strong>BAIRRO: </strong>{{$aditivo->tce->secConcedente->nmBairro}}
							</td>
						</tr>
						@if($aditivo->tce->secConcedente->dsComplemento)
						<tr>
							<td colspan="3">
								<strong>COMPLEMENTO: </strong> {{$aditivo->tce->secConcedente->dsComplemento}}
							</td>
						</tr>
						@endif
						<tr>
							<td colspan="2">
								<strong>CIDADE: </strong>{{$aditivo->tce->concedente->cidade->nmCidade}} - {{$aditivo->tce->concedente->cidade->estado->nmEstado}}
							</td>
							<td>
								<strong>CEP: </strong>{{$aditivo->tce->secConcedente->cdCEP}}
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<strong>TELEFONE: </strong>{{$aditivo->tce->secConcedente->dsFone}}
							</td>
							<td>
								<strong>OUTRO: </strong> @if  ( $aditivo->tce->secConcedente->dsOutroFone != NULL) {{$aditivo->tce->secConcedente->dsOutroFone}} @else - @endif
							</td>
						</tr>

						{{--  RESPONSAVEL SECRETARIA  --}}
						@if(($aditivo->tce->secConcedente->nmResponsavel) != NULL)
							<tr>
								<td colspan="3">
								<strong>REPRESENTADO PELO SR(A): </strong>{{$aditivo->tce->secConcedente->nmResponsavel}}
							</td>
							{{-- <td>
								<strong>CARGO: </strong>{{$aditivo->tce->secConcedente->dsResponsavelCargo}}
							</td> --}}
						@else
							<tr>
								<td colspan="3">
								<strong>REPRESENTADO PELO SR(A): </strong>{{$aditivo->tce->concedente->nmResponsavel}}
							</td>
							{{-- <td>
								<strong>CARGO: </strong>{{$aditivo->tce->concedente->dsResponsavelCargo}}
							</td> --}}
						@endif

						@if((($aditivo->nmSupervisor) AND ($aditivo->dsSupervisorCargo) != NULL OR $aditivo->dsSupervisorCargo) != "") && (($aditivo->nmSupervisor) AND array_key_exists('dsSupervisorCargo', $aditivo->tce) == true))
						<tr>
							<td colspan="2">
								<strong>SUPERVISOR(A): </strong>{{$aditivo->nmSupervisor}}
							</td>
							<td>
								<strong>CARGO/FORMAÇÃO: </strong>{{$aditivo->dsSupervisorCargo}}
							</td>
						</tr>
						@elseif ($tce->aditivos()->where('nmSupervisor','<>','')->latest()->first())
							<tr>
								<td colspan="2">
									{{-- <strong>SUPERVISOR(A): </strong>- {{$tce->aditivos->where('nmSupervisor','<>','')->last()->nmSupervisor}} --}}
									<strong>SUPERVISOR(A): </strong>- {{$tce->aditivos()->where('nmSupervisor','<>','')->latest()->first()->nmSupervisor}}
								</td>
								<td>
									{{-- <strong>CARGO/FORMAÇÃO: </strong>- {{array_key_exists('dsSupervisorCargo',$tce->aditivos()->where('dsSupervisorCargo','<>','')->latest()->first()) == true ? $tce->aditivos()->where('dsSupervisorCargo','<>','')->latest()->first()->dsSupervisorCargo  : '---'}}--}}
								</td>
							</tr>
						@else
							@if((($aditivo->tce->nmSupervisor) AND ($aditivo->tce->dsSupervisorCargo) != NULL OR $aditivo->tce->nmSupervisor != "") && (($aditivo->tce->nmSupervisor) AND array_key_exists('dsSupervisorCargo', $aditivo->tce) == true))
							<tr>
								<td colspan="2">
									<strong>SUPERVISOR(A): </strong>- {{$aditivo->tce->nmSupervisor}}
								</td>
								<td>
									<strong>CARGO/FORMAÇÃO: </strong>- {{$aditivo->tce->dsSupervisorCargo != null ? $aditivo->tce->dsSupervisorCargo : '----'}}
								</td>
							</tr>
							@endif
						@endif
						{{-- @if(($aditivo->nmSupervisor) AND ($aditivo->dsSupervisorCargo) != NULL) OR ($aditivo->nmSupervisor) AND array_key_exists('dsSupervisorCargo', $aditivo->tce) == true)
						<tr>
							<td colspan="2">
								<em><strong>SUPERVISOR(A): </strong>{{$aditivo->tce->nmSupervisor}}</em>
							</td>
							<td>
								<em><strong>CARGO/FORMAÇÃO: </strong>{{$aditivo->tce->dsSupervisorCargo != null ? $aditivo->tce->dsSupervisorCargo : '----'}}</em>
							</td>
						</tr>
						@else
							@if(($aditivo->tce->nmSupervisor) AND array_key_exists('dsSupervisorCargo', $aditivo->tce->dsSupervisorCargo) == true)
							<tr>
								<td colspan="2">
									<em><strong>SUPERVISOR(A): </strong>{{$aditivo->tce->nmSupervisor}}</em>
								</td>
								<td>
									<em><strong>CARGO/FORMAÇÃO: </strong>{{$aditivo->tce->dsSupervisorCargo != null ? $aditivo->tce->dsSupervisorCargo : '----'}}</em>
								</td>
							</tr>
							@else
								@if(($aditivo->tce->secConcedente->nmResponsavel) AND ($aditivo->tce->secConcedente->dsResponsavelCargo) != NULL)
								<tr>
									<td colspan="2">
										<strong>SUPERVISOR(A): </strong>{{$aditivo->tce->secConcedente->nmResponsavel}}
									</td>
									<td>
										<strong>CARGO/FORMAÇÃO: </strong>{{$aditivo->tce->secConcedente->dsResponsavelCargo}}
									</td>
								</tr>
								@endif
							@endif
						@endif--}}

						<!--SE NÂO FOR DE SECRETARIA EXIBE OS DADOS DA CONCEDENTE CODIGO ABAIXO-->
						@else
							@if($aditivo->tce->concedente->flTipo == "PF")
							<tr>
								<td colspan="2">
									<strong>CPF: </strong>{{$aditivo->tce->concedente->cdCnpjCpf}}
								</td>
								<td>
									<strong>ORGÃO REGULADOR: </strong>{{$aditivo->tce->concedente->dsOrgaoRegulador}}
								</td>
							</tr>
							@else
							<tr>
								<td colspan="3">
									<strong>CNPJ: </strong>{{$aditivo->tce->concedente->cdCnpjCpf}}
								</td>
							</tr>
							@endif
							<tr>
								<td colspan="2">
									<strong>ENDEREÇO: </strong> {{$aditivo->tce->concedente->dsEndereco}} ,
									<strong>N°</strong> {{$aditivo->tce->concedente->nnNumero}}
								</td>
								<td>
									<strong>BAIRRO: </strong>{{$aditivo->tce->concedente->nmBairro}}
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<strong>CIDADE: </strong>{{$aditivo->tce->concedente->cidade->nmCidade}} - {{$aditivo->tce->concedente->cidade->estado->nmEstado}}
								</td>
								<td>
									<strong>CEP: </strong>{{$aditivo->tce->concedente->cdCEP}}
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<strong>TELEFONE: </strong>{{$aditivo->tce->concedente->dsFone}}
								</td>
								<td>
									<strong>OUTRO: </strong> @if  ( $aditivo->tce->concedente->dsOutroFone != NULL) {{$aditivo->tce->concedente->dsOutroFone}} @else - @endif
								</td>
							</tr>

							{{--  RESPONSAVEL CONCEDENTE  --}}
                            @if(($aditivo->tce->concedente->nmResponsavel) != NULL)
								<tr>
									<td colspan="2">
									<strong>REPRESENTADO PELO SR(A): </strong>{{$aditivo->tce->concedente->nmResponsavel}}
								</td>
								<td>
									<strong>CARGO: </strong>{{$aditivo->tce->concedente->dsResponsavelCargo}}
								</td>
							@endif

							@if((($aditivo->nmSupervisor) AND ($aditivo->dsSupervisorCargo) != NULL) OR (($aditivo->nmSupervisor) AND ($aditivo->dsSupervisorCargo) != ""))
							<tr>
								<td colspan="2">
									<strong>SUPERVISOR(A): </strong>{{$aditivo->nmSupervisor}}
								</td>
								<td>
									<strong>CARGO/FORMAÇÃO: </strong>{{$aditivo->dsSupervisorCargo}}
								</td>
							</tr>
							@elseif($tce->aditivos->where('nmSupervisor','!=','')->count() > 0)
								<tr>
									<td colspan="2">
										<strong>SUPERVISOR(A): </strong>- {{$tce->aditivos->where('nmSupervisor','!=','')->last() != null ? $tce->aditivos->where('nmSupervisor','!=','')->last()->nmSupervisor : '-'}}
									</td>
									<td>
										<strong>CARGO/FORMAÇÃO: </strong>- {{$tce->aditivos->where('dsSupervisorCargo','!=','')->last() != null ? $tce->aditivos->where('dsSupervisorCargo','!=','')->last()->dsSupervisorCargo : '-'}}
									</td>
								</tr>
							@else
								@if((($aditivo->tce->nmSupervisor) AND ($aditivo->tce->dsSupervisorCargo) != NULL) OR (($aditivo->tce->nmSupervisor) AND ($aditivo->tce->dsSupervisorCargo) != ""))
								<tr>
									<td colspan="2">
										<strong>SUPERVISOR(A): </strong>- {{$aditivo->tce->nmSupervisor}}
									</td>
									<td>
										<strong>CARGO/FORMAÇÃO: </strong>- {{$aditivo->tce->dsSupervisorCargo}}
									</td>
								</tr>
								@endif
							@endif
						<!--ABAIXO FIM DO IF SE É SECRETARIA DE CONCEDENTE-->
						@endif
						@if($aditivo->dsLotacao)
						<tr>
							<td colspan="3">
								<strong>LOTAÇÃO:</strong> {{$aditivo->dsLotacao}}
							</td>
						</tr>
						@elseif($tce->aditivos->where('dsLotacao','<>',NULL)->last())
						<tr>
							<td colspan="3">
								<strong>LOTAÇÃO:</strong>
									- {{$tce->aditivos->where('dsLotacao','<>',NULL)->last()->dsLotacao}}
							</td>
						</tr>
						@elseif($tce->dsLotacao)
						<tr>
							<td colspan="3">
								<strong>LOTAÇÃO:</strong>
									- {{$tce->dsLotacao}}
							</td>
						</tr>
						@endif
					</tbody>
				</table>

				<!--ESTAGIARIO-->
				<table style="width: 100%;" class="table table-bordered table-condensed">
					<tr>
						<th colspan="3" style="text-align: center">
							<strong>DADOS DO ESTÁGIARIO </strong><span style="float: right;font-size: 5pt">(Cod.: {{$aditivo->tce->estudante_id}})</span>
						</th>
					</tr>
					<tr>
						<td colspan="">
							<strong>NOME: </strong>{{$aditivo->tce->estudante->nmEstudante}}
						</td>
						<td>
							<strong>DATA NASC: </strong>{{$aditivo->tce->estudante->dtNascimento->format('d/m/Y')}}
						</td>
						<td>
							<strong>CPF: </strong>{{$aditivo->tce->estudante->cdCPF}}
						</td>
					</tr>
					<tr>
						<td colspan="1">
							<strong>RG: </strong>{{$aditivo->tce->estudante->cdRG}}
						</td>
						<td>
							<strong>ORG.: </strong>{{$aditivo->tce->estudante->dsOrgaoRG}}
						</td>
						<td>
							<strong>EMAIL: </strong>{{$aditivo->tce->estudante->dsEmail}}
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<strong>ENDEREÇO: </strong> {{$aditivo->tce->estudante->dsEndereco}},
							<strong> N°</strong> {{$aditivo->tce->estudante->nnNumero}}
						</td>
						<td>
							<strong>BAIRRO: </strong>{{$aditivo->tce->estudante->nmBairro}}
						</td>
					</tr>
					<tr>
						<td colspan="1">
							<strong>CIDADE: </strong>{{$aditivo->tce->estudante->cidade->nmCidade}} - {{$aditivo->tce->estudante->cidade->estado->nmEstado}}
						</td>
						<td>
							<strong>CEP: </strong>{{$aditivo->tce->estudante->cdCEP}}
						</td>
						<td>
							<strong>TELEFONE: </strong>{{$aditivo->tce->estudante->dsFone}}
						</td>
					</tr>
					<tr>
						<td>
							<strong>CURSO: </strong> {{$aditivo->tce->cursoDaInstituicao->curso->nmCurso}}
						</td>
						<td>
							<strong>{{$aditivo->tce->cursoDaInstituicao->cursoDuracao->nmTipoDuracao}}: </strong>
							@if($aditivo->nnSemestreAno != NULL)
								{{$aditivo->nnSemestreAno}}
							@else
								{{$aditivo->tce->nnSemestreAno}}
							@endif
							{{$aditivo->tce->cursoDaInstituicao->cursoDuracao->nmTipoDuracao}}
						 </td>
						<td>
							<strong>TURNO: </strong>{{$aditivo->tce->estudante->turno->nmTurno}}
						</td>
					</tr>
				</table>

				<div class="texto">
						<div class="row" id="cl1">
							<div class="col-sm-12">

								<p style="text-align: justify" class="termo">
									<strong>Resolvem celebrar entre si o aditamento ao TERMO DE COMPROMISSO DE ESTÁGIO – TCE, firmado entre a Unidade Concedente, o Estagiário e a Instituição de Ensino, acima qualificados na data {{$aditivo->dtAditivo->format('d/m/Y')}}</strong>. Vigência Inicial do Termo de Compromisso: @if($aditivo->tce->estudante->portabilidade()->count() != "0") {{$aditivo->tce->estudante->portabilidade->dtInicio->format('d/m/Y')}} (portabilidade) @else {{$aditivo->tce->dtInicio->format('d/m/Y')}} @endif .
								</p>
								<p style="text-align: justify" class="termo">
									@if($aditivo->tce->migracao != NULL)
									<strong>CLÁUSULA 1ª: </strong>
									Este instrumento Aditivo ao Termo de Compromisso nº {{$aditivo->tce->migracao}} tem por objetivo formalizar as condições para a realização de ESTÁGIO DE ESTUDANTE e particularizar a relação jurídica especial existente entre o ESTUDANTE, a CONCEDENTE e a INSTITUIÇÃO DE ENSINO caracterizando a não vinculação empregatícia, nos termos da Lei nº. 11.788, de 25 de setembro de 2008 e demais disposições vigentes.
									@else
									<strong>CLÁUSULA 1ª: </strong>
									Este instrumento Aditivo ao Termo de Compromisso nº {{$aditivo->tce_id}} tem por objetivo formalizar as condições para a realização de ESTÁGIO DE ESTUDANTE e particularizar a relação jurídica especial existente entre o ESTUDANTE, a CONCEDENTE e a INSTITUIÇÃO DE ENSINO caracterizando a não vinculação empregatícia, nos termos da Lei nº. 11.788, de 25 de setembro de 2008 e demais disposições vigentes.
										{{$aditivo->tce_id}}
									@endif
								</p>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
									<strong>CLÁUSULA 2ª - Ficam alteradas as seguintes condições do estágio inicialmente estabelecidas no referido TERMO DE COMPROMISSO DE ESTÁGIO – TCE:</strong>
									<!--<br>O presente Termo de Compromisso de Estágio vigerá do dia _____/_____/_________ até _____/_____/_________-->
									<ol class="margin justi">
										@if(($aditivo->dtInicio) AND ($aditivo->dtFim) != NULL)
											<li>Vigência de: <strong>{{$aditivo->dtInicio->format("d/m/Y")}}</strong> até <strong>{{$aditivo->dtFim->format("d/m/Y")}}</strong></li>
										@endif

										@if($aditivo->periodo_id != NULL)
											{{-- @if(substr($aditivo->periodo->dsPeriodo, -6) == "SENDO:")
											<li>Horário: {{$aditivo->periodo->dsPeriodo}} {{$aditivo->dsPeriodo}} de {{substr($aditivo->hrInicio, 0, 5)}} as {{substr($aditivo->hrFim, 0, 5)}}</li>
											@else
											<li>Horário: {{$aditivo->periodo->dsPeriodo}}.</li>
											@endif
										@endif --}}
												@if($aditivo->hrInicio != NULL)
												<li>Horário: <strong>{{$aditivo->periodo->dsPeriodo}} {{$aditivo->dsPeriodo}} de {{substr($aditivo->hrInicio, 0, 5)}} as {{substr($aditivo->hrFim, 0, 5)}}</strong>.</li>
											@else
												<li>Horário: <strong>{{$aditivo->periodo->dsPeriodo}} {{$aditivo->dsPeriodo}}</strong>.</li>
											@endif
										@endif

										@if($aditivo->vlAuxilioMensal != NULL)
										<li>Bolsa-Auxílio mensal de: <strong>R${{number_format($aditivo->vlAuxilioMensal, 2, ',', '.')}}</strong> ({{Extenso::Converter($aditivo->vlAuxilioMensal)}})
											@if (isset($aditivo->vlAuxilioAlimentacao) && !empty($aditivo->vlAuxilioAlimentacao) && !is_null($aditivo->vlAuxilioAlimentacao))
												@if ($aditivo->vlAuxilioTransporte)
												,  Auxílio-Alimentação de <strong>R${{number_format($aditivo->vlAuxilioAlimentacao, 2, ',', '.')}}</strong> ({{Extenso::Converter($aditivo->vlAuxilioAlimentacao)}})
												@else
												e  Auxílio-Alimentação de <strong>R${{number_format($aditivo->vlAuxilioAlimentacao, 2, ',', '.')}}</strong> ({{Extenso::Converter($aditivo->vlAuxilioAlimentacao)}})
												@endif
											@endif
											@if (isset($aditivo->vlAuxilioTransporte) && !empty($aditivo->vlAuxilioTransporte) && !is_null($aditivo->vlAuxilioTransporte))
												@if ($aditivo->vlAuxilioAlimentacao)
												e Auxílio-Transporte de <strong>R${{number_format($aditivo->vlAuxilioTransporte, 2, ',', '.')}}</strong> ({{Extenso::Converter($aditivo->vlAuxilioTransporte)}})
												@elseif(isset($aditivo->vlAuxilioAlimentacao) && isset($aditivo->vlAuxilioTransporte))
												e Auxílio-Transporte de <strong>R${{number_format($aditivo->vlAuxilioTransporte, 2, ',', '.')}}</strong> ({{Extenso::Converter($aditivo->vlAuxilioTransporte)}})
												@else
												e Auxílio-Transporte de <strong>R${{number_format($aditivo->vlAuxilioTransporte, 2, ',', '.')}}</strong> ({{Extenso::Converter($aditivo->vlAuxilioTransporte)}})
												@endif
											@endif
											@php
												$total = $aditivo->vlAuxilioMensal+$aditivo->vlAuxilioAlimentacao+$aditivo->vlAuxilioTransporte;
											@endphp
											@if(isset($aditivo->vlAuxilioAlimentacao) OR isset($aditivo->vlAuxilioTransporte))
											totalizando <strong>R${{number_format($total, 2, ',', '.')}}.</strong>({{Extenso::Converter($total)}})
											@endif
										</li>
										@endif
										@if($aditivo->aditivoOutro != NULL)
											<li>{{$aditivo->aditivoOutro}}</li>
										@endif
									</ol>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<strong class="justi">CLÁUSULA 3ª - </strong>Permanecem inalteradas todas as demais disposições do TCE, do qual este Termo Aditivo passa a fazer parte integrante.
								<br>
								<br>
								E, por estarem de inteiro e comum acordo com as condições e dizeres deste Termo Aditivo, as partes assinam em 4 vias de igual teor.
							</div>
						</div>

						<div class="row body-content">
							<div class="span12" style=" margin-top:10px;">
								<p style="margin: 10px; float: right;">_____________________, ____ de ______________ de _______.</p>
							</div>
						</div>


						<div class="col-6" >
								<div style="font-size: 09px;margin-top: 100px;border-top: 1px solid #333;width:80%;">
									<p class="p">Estagiário(a): {{$aditivo->tce->estudante->nmEstudante}}</p>
								</div>
								@php
								$nascimento = $aditivo->tce->estudante->dtNascimento;
								$nascimento->addYears(18)->toDateString();
								$hoje = \Carbon\Carbon::now()->toDateString();
									if($nascimento > $hoje){
										echo '<div style="font-size: 09px;margin-top: 60px;border-top: 1px solid #333;width:80%;">
												<p class="p">REPRESENTANTE LEGAL DO ESTAGIÁRIO / CPF</p>
											</div>';
									}
								@endphp
								<div style="font-size: 09px;margin-top: 60px;border-top: 1px solid #333;width:80%;">
									<p class="p">Concedente: {{$aditivo->tce->concedente->nmRazaoSocial}}</p>
								</div>
						</div>


						<div class="col-6">
							@if($aditivo->tce->concedente->assUpaADITIVO == "S")
							<div style="font-size: 09px;margin-top: 100px;border-top: 1px solid #333;width:80%;">
								<p class="p">UNIVERSIDADE PATATIVA DO ASSARÉ - UPA</p>
							</div>
							<div style="font-size: 09px;margin-top: 60px;border-top: 1px solid #333;width:80%;">
									<p class="p">Instituição: {{$aditivo->tce->instituicao->nmInstituicao}}</p>
							</div>
							@else
							<div style="font-size: 09px;margin-top: 100px;border-top: 1px solid #333;width:80%;">
									<p class="p">{{$aditivo->tce->instituicao->nmInstituicao}}</p>
							</div>
							@endif
						</div>
						<div style="clear: both"></div>
							@include('tce.relatorio.inc_rodape_aditivo')
						</div>

						{{-- Folha de atividades --}}
						@if($aditivo->atividades->count())

						<div class="novaPagina"></div>

						<table style="width: 100%;" class="header">
							<tr>
								<td>
									<?php $image_path = '/img/relatorios/logo_default.png'; ?>
									<img src="{{ public_path() . $image_path }}" alt="Upa - Estagio" />
								</td>
								<td>
									<h2 style="text-align: left;">
										<strong>PLANO DE ATIVIDADES</strong>
									</h2>
								</td>
							</tr>
						</table>
						<br>
						<table style="width: 100%;" class="table table-bordered table-condensed ">
								<tr>
									<th colspan="3" style="text-align: center">
										<strong>DADOS DA CONCEDENTE</strong>

									</th>
								</tr>
								<tr>
									<td colspan="3">
										<strong>Empresa(Concedente):</strong><br>
										{{$aditivo->tce->concedente->nmRazaoSocial}}
									</td>
								</tr>
								<tr>

									<td colspan="2">
										<strong>Nome do Supervisor (a) do Estágio na Concedente:</strong><br>
										@if(($aditivo->nmSupervisor != NULL) OR ($aditivo->nmSupervisor != ""))
										{{$aditivo->nmSupervisor}}
										@else
										{{$aditivo->tce->nmSupervisor}}
										@endif

									</td>
									<td>
										<strong>Telefone:</strong><br>
										@if($aditivo->tce->concedente->dsFone){{$aditivo->tce->concedente->dsFone}}@else <font color="#fff">c</font> @endif
									</td>
								</tr>
								<tr>
									<td colspan="2">
										<strong>E-mail:</strong><br>
										<font color="#fff">e</font>
									</td>
									{{-- <td>
										<strong>Formação Academica:</strong><br>
										<font color="#fff">e</font>
									</td> --}}
									<td>
										<strong>Cargo:</strong><br>
										@if(($aditivo->dsSupervisorCargo != NULL) OR ($aditivo->dsSupervisorCargo != ""))
										{{$aditivo->dsSupervisorCargo}}
										@else
										{{$aditivo->tce->dsSupervisorCargo}}
										@endif
									</td>
								</tr>
						</table>

						<table style="width: 100%;" class="table table-bordered table-condensed ">
								<tr>
									<th colspan="3" style="text-align: center">
										<strong>DADOS DO ESTAGIARIO</strong>
									</th>
								</tr>
								<tr>
									<td colspan="2">
										<strong>Nome do Estagiario:</strong><br>
										{{$aditivo->tce->estudante->nmEstudante}}
									</td>
									<td>
										<strong>Telefone:</strong><br>
										@if($aditivo->tce->estudante->dsFone){{$aditivo->tce->estudante->dsFone}}@else <font color="#fff">c</font> @endif
									</td>
								</tr>
								<tr>
									<td colspan="2">
										<strong>Curso:</strong><br>
											{{$aditivo->tce->cursoDaInstituicao->curso->nmCurso}}
									</td>
									<td>
										<strong>Email:</strong><br>
										@if($aditivo->tce->estudante->dsEmail){{$aditivo->tce->estudante->dsEmail}} @else <font color="#fff">c</font>@endif
									</td>
								</tr>
						</table>

						{{-- <center>Vigência: <strong>{{$tce->dtInicio->format("d/m/Y")}}</strong> até <strong>{{$tce->dtFim->format("d/m/Y")}}</strong></center><br> --}}
						@if($aditivo->dtInicio OR $aditivo->dtFim)
							<center>Vigência: <strong>{{$aditivo->dtInicio->format("d/m/Y")}}</strong> até <strong>{{$aditivo->dtFim->format("d/m/Y")}}</strong></center>
						@else
							<center>Vigência TCE: <strong>{{$aditivo->tce->dtInicio->format("d/m/Y")}}</strong> até <strong>{{$aditivo->tce->dtFim->format("d/m/Y")}}</strong></center>
						@endif

						<table style="width: 100%;" class="table table-bordered table-condensed ">
								<tr>
										<th colspan="3" style="text-align: center">
											<strong>ATIVIDADES A DESENVOLVER NO ESTÁGIO:</strong>
										</th>
								</tr>
								<tr>
									<td colspan="3">
										<ol class="margin">
												@foreach($aditivo->atividades as $atividade)
													<li>{{$atividade->atividade}}</li>
												@endforeach
										</ol>
										<strong>Assinatura do Supervisor (a) do Estágio da Empresa:</strong>
									</td>
								</tr>
						</table>

						<table style="width: 100%;" class="table table-bordered table-condensed ">
								<tr>
										<th colspan="3" style="text-align: center">
											<strong>PARECER DO(A) ORIENTADOR(A) PEDAGÓGICO(A):</strong>
										</th>
								</tr>
								<tr>
									<td colspan="3">
											<hr style="font-size: 10px;margin-top: 20px; margin-bottom: 0px; border-bottom: 1px solid #333 ;" />
											<hr style="margin-top: 20px; margin-bottom: 0px; border-bottom: 1px solid #333 ;" />
											<hr style="margin-top: 20px; margin-bottom: 0px; border-bottom: 1px solid #333 ;" />
											<hr style="margin-top: 20px; margin-bottom: 0px; border-bottom: 1px solid #333 ;" />
											<hr style="margin-top: 20px; margin-bottom: 0px; border-bottom: 1px solid #333 ;" />
										<br>
										<strong>Assinatura do(a) Orientador(a) Pedagógico(a):</strong>
									</td>
								</tr>
						</table>
						<div class="row body-content">
                            @if($aditivo->tce->relatorio_id == "6")
                                <div class="span12">
                                    <p style="margin: 10px; float: right;">Recife-PE, _____ de ______________ de _______.</p>
                                </div>
                            @else
                                <div class="span12">
                                    <p style="margin: 10px; float: right;">Brasilia-DF, _____ de ______________ de _______.</p>
                                </div>
                            @endif

						</div>
						{{-- <div class="col-12" style=" margin-top: 90px;">
							<div class="col-6">
								<div style="font-size: 10px;margin-top: 60px;border-top: 1px solid #333;">
									<p class="p">{{$aditivo->tce->concedente->nmRazaoSocial}}</p>
								</div>
								<div style="font-size: 10px;margin-top: 60px;border-top: 1px solid #333;">
									<p class="p">{{$aditivo->tce->estudante->nmEstudante}}</p>
								</div>
								@php
								$nascimento = $aditivo->tce->estudante->dtNascimento;
								$nascimento->addYears(16)->toDateString();
								$hoje = \Carbon\Carbon::now()->toDateString();
									if($nascimento > $hoje){
										echo '<div style="font-size: 10px;margin-top: 60px;border-top: 1px solid #333;">
												<p class="p">REPRESENTANTE LEGAL DO ESTAGIÁRIO / CPF</p>
											</div>';
									}
								@endphp
								<div style="font-size: 10px;margin-top: 60px;border-top: 1px solid #333;">
									<p class="p">{{$aditivo->tce->instituicao->nmInstituicao}}</p>
								</div>
							</div>
							<div style="clear: both"></div>
						</div> --}}
						<div class="col-6" >
								<div class="col-12">
									<div style="font-size: 09px;margin-top: 120px;border-top: 1px solid #333;">
										<p class="p">{{$aditivo->tce->estudante->nmEstudante}}</p>
									</div>
									@php
									$nascimento = $aditivo->tce->estudante->dtNascimento;
									$nascimento->addYears(18)->toDateString();
									$hoje = \Carbon\Carbon::now()->toDateString();
										if($nascimento > $hoje){
											echo '<div style="font-size: 10px;margin-top: 60px;border-top: 1px solid #333;">
													<p class="p">REPRESENTANTE LEGAL DO ESTAGIÁRIO / CPF</p>
												</div>';
										}
									@endphp
									<div style="font-size: 09px;margin-top: 60px;border-top: 1px solid #333;">
										<p class="p">{{$aditivo->tce->concedente->nmRazaoSocial}}</p>
									</div>
								</div>

								<div style="clear: both"></div>
							</div>
							<div class="col-6" style="margin-left: 20px;">
								@if($aditivo->tce->concedente->assUpaADITIVO == "S")
								<div style="font-size: 09px;margin-top: 120px;border-top: 1px solid #333;">
									<p class="p">UNIVERSIDADE PATATIVA DO ASSARÉ - UPA</p>
								</div>
								<div style="font-size: 09px;margin-top: 60px;border-top: 1px solid #333;">
										<p class="p">{{$aditivo->tce->instituicao->nmInstituicao}}</p>
								</div>
								@else
								<div style="font-size: 09px;margin-top: 120px;border-top: 1px solid #333;">
										<p class="p">{{$aditivo->tce->instituicao->nmInstituicao}}</p>
								</div>
								@endif

							</div>
						@include('tce.relatorio.inc_rodape_aditivo')
						@endif
						{{-- FIM FOLHA ATIVIDADES --}}

					</div>

			</div>

		</div>
	</div>

</body>
</html>
