@extends('adminlte::page')
@if(!empty($titulo))
    @section('title_postfix', ' - '.$titulo)
@endif
@section('content_header')

    <h1>Aditivo</h1>
    <ol class="breadcrumb">
        {{--  {{route('estudante.index')}}  --}}
        <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('tce.index')}}"><i class="fa fa-files-o"></i>TCE</a></li>
        <li><a href="{{route('tce.show', ['tce' => $tce->id])}}"><i class="fa fa-files-o"></i> {{$tce->id}}</a></li>
        <li class="active"><i class="fa fa-file"></i> Criar Aditivo</li>
    </ol>
@stop

@section('content')
    @if(session('success'))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> Alerta!</h4>
            {{session('success')}}
    </div>
    @endif

    @if(session('error'))
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Alerta!</h4>
            {{session('error')}}
    </div>
    @endif

    @if($errors->any())
    <div class="alert alert-warning alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Alerta!</h4>
        <ul class="alert-warning">
            <h3>:( Whoops, Houve algum erro! </h3>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <h4 class="page-header"><strong>TCE:</strong> {{$tce->id}} - <strong>Estudante:</strong> {{$tce->estudante->nmEstudante}} - <strong>Instituição de Ensino:</strong>{{$tce->instituicao->nmInstituicao}} - <strong>Curso:</strong> {{$tce->cursoDaInstituicao->curso->nmCurso}}</h4>
    <!-- form start -->
    <form action="{{route('tce.aditivo.store', ['tce' => $tce->id])}}" method="POST">
    <input id="instituicao_id" name="instituicao_id" type="hidden" value="{{$tce->instituicao_id}}">
    <input id="curso_id" name="curso_id" type="hidden" value="{{$tce->cursoDaInstituicao->id}}">
    <input id="curso_atv_id" name="curso_atv_id" type="hidden" value="{{$tce->cursoDaInstituicao->curso->id}}">
    <input id="estudante_id" name="estudante_id" type="hidden" value="{{$tce->estudante_id}}">
    <input id="tce_id" name="tce_id" type="hidden" value="{{$tce->id}}">
    <input id="estado_id" name="estado_id" type="hidden" value="{{$tce->estudante->estado_id}}">
    {!! csrf_field() !!}
    <div class="box box-default">
        <div class="box-body">
            <div class="row ">
                <div class="col-md-2">
                    <div class="form-group">
                        <label>*Data do Aditivo:</label>
                        @if(old('dtAditivo'))
                            <input class="form-control" id="dtAditivo" name="dtAditivo" placeholder="dd/mm/aaaa" type="text" value="{{old('dtAditivo')}}">
                        @else
                            <input class="form-control" id="dtAditivo" name="dtAditivo" placeholder="dd/mm/aaaa" type="text" value="{{$dtHoje}}">
                        @endif
                        {{-- <input class="form-control" id="dtAditivo" name="dtAditivo" placeholder="dd/mm/aaaa" type="text" value={{$dtHoje}}> --}}
                    </div>
                </div>
                <div class="col-md-10">
                    <div class="form-group">
                        <label>Outro Motivo de Aditivo:</label>
                        <input class="form-control" id="aditivoOutro" name="aditivoOutro" placeholder="Motivo do Aditivo caso não seja alguma situação abaixo" value="{{old('aditivoOutro')}}" type="text">
                        <span id="helpBlock" class="help-block h6">Caso seu aditivo não se enquadre em nenhuma situação seguinte, descreva seu aditivo. Ex.: Alteração dados Estudante.</span>
                    </div>
                </div>


            </div>
            <!-- /.row -->
        </div>
    </div>
    <div class="box box-default collapsed-box">
        <div class="box-header with-border">
            <h3 class="box-title" data-widget="collapse"><strong>Alteração dados TCE e Vigência</strong></h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                    <i class="fa fa-plus"></i>
                </button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body" style="display: none;">
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="dtIniio">Inicio</label>
                        <input class="form-control" id="dtInicio" name="dtInicio" value="{{old('dtInicio')}}" placeholder="dd/mm/aaaa" type="text">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="escolheMes">Meses</label>
                        <select class="form-control" id="escolheMes" name="escolheMes" onchange="escolheMeses();">
                            <option value="" selected>Selecione...</option>
                            <option value="1">1 MÊS</option>
                            <option value="3">3 MESES</option>
                            <option value="6">6 MESES</option>
                            <option value="12">12 MESES</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="dtFim">Fim</label>
                        <input class="form-control" id="dtFim" name="dtFim" value="{{old('dtFim')}}" placeholder="dd/mm/aaaa" type="text">
                    </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="" class="control-label">Supervisor</label>
                    <br>
                    <select class="form-control" name="supervisor_id" id="supervisor_id">
                      <option value="">Selecione um supervisor</option>
                      @foreach($supervisors as $supervisor)
                      <option value="{{$supervisor->id}}" data-nome="{{$supervisor->nome}}" data-cargo="{{$supervisor->cargo}}">{{$supervisor->nome}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <!--
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="dsLocatao">Lotação do Estágio</label>
                        <input class="form-control" id="dsLotacao" name="dsLotacao" type="text" placeholder="Lotação se houver">
                    </div>
                </div>
                -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="nmSupervisorIe">Supervisor Instituição de Ensino</label>
                        <input class="form-control" id="nmSupervisorIe" name="nmSupervisorIe" value="{{old('nmSupervisorIe')}}" placeholder="Nome do Supervisor IE" type="text">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="dsSupervisorIeCargo">Cargo Supervisor Instituição de Ensino</label>
                        <input class="form-control" id="dsSupervisorIeCargo" name="dsSupervisorIeCargo" value="{{old('dsSupervisorIeCargo')}}" placeholder="Cargo Supervisor IE" type="text">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="nmSupervisor">Supervisor Empresa</label>
                        <input class="form-control" id="nmSupervisor" name="nmSupervisor" value="{{old('nmSupervisor')}}" placeholder="Nome do Supervisor da Empresa" type="text">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="dsSupervisorCargo">Cargo Supervisor Empresa</label>
                        <input class="form-control" id="dsSupervisorCargo" name="dsSupervisorCargo" value="{{old('dsSupervisorcargo')}}" placeholder="Cargo Supervisor da Empresa" type="text">
                    </div>
                </div>
            </div>
            <!-- /.row -->

            <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="periodo_id">*Periodo</label>
                            <select class="form-control" id="periodo_id" name="periodo_id">
                            <option value="" selected>Selecione um periodo</option>
                            @foreach($periodos as $periodo)
                                <option value="{{ $periodo['id'] }}" @if(old('periodo_id') == $periodo['id']) {{ 'selected' }} @endif>{{ $periodo['dsPeriodo'] }}</option>
                                <!--<option value="{{$periodo['id']}}">{{$periodo['dsPeriodo']}}</option>-->
                            @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="dsPeriodo">Periodo Descriminado</label>
                            <input class="form-control" id="dsPeriodo" name="dsPeriodo" type="text" value="{{old('dsPeriodo')}}">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="hrInicio">Hora Inicial</label>
                            <input class="form-control" id="hrInicio" name="hrInicio" type="text" value="{{old('hrInicio')}}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="hrFim">Hora Final</label>
                            <input class="form-control" id="hrFim" name="hrFim" type="text" value="{{old('hrFim')}}">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="dsLocatao">Lotação do Estágio</label>
                            <input class="form-control" id="dsLotacao" name="dsLotacao" type="text" value="{{old('dsLotacao')}}">
                        </div>
                    </div>

                </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="vlAuxilioMensal">Auxilio Mensal</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-money"></i></span>
                            <input class="form-control" type="text" id="vlAuxilioMensal" value="{{old('vlAuxilioMensal')}}" name="vlAuxilioMensal" type="text" placeholder="Valor">
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="vlAuxilioAlimentacao">Auxilio Alimentação</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-money"></i></span>
                            <input class="form-control" id="vlAuxilioAlimentacao" name="vlAuxilioAlimentacao" value="{{old('vlAuxilioAlimentacao')}}" type="text" placeholder="Valor">
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="vlAuxilioTransporte">Auxilio Transporte</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-money"></i></span>
                            <input class="form-control" id="vlAuxilioTransporte" name="vlAuxilioTransporte" value="{{old('vlAuxilioTransporte')}}" type="text" placeholder="Valor">
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box box-default collapsed-box -->

    <div class="box box-default collapsed-box">
        <div class="box-header with-border">
            <h3 class="box-title" data-widget="collapse"><strong>Semestre/Ano do Estudante</strong></h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                    <i class="fa fa-plus"></i>
                </button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body" style="display: none;">

            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        <label>Semestre/Ano do TCE:</label>
                        <br>{{$tce->nnSemestreAno}}
                    </div>
                </div>
                @foreach($aditivosSemestre as $aditivoSemestre)
                <div class="col-md-3">
                    <div class="form-group">
                        {{$aditivoSemestre->dtAditivo->format('d/m/Y')}}<br>
                        <label>Semestre/Ano Aditivo({{$aditivoSemestre->nnAditivo}}):</label>
                        <br>{{$aditivoSemestre->nnSemestreAno}}
                    </div>
                </div>
                @endforeach
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="nnSemestreAno">Semestre Atual:</label>
                        <div class="input-group">
                            <input class="form-control" id="nnSemestreAno" name="nnSemestreAno" type="text" placeholder="Semestre ou Ano">
                        </div>
                    </div>
                </div>

            </div>
            <!-- /.row -->
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box box-default collapsed-box -->

    <div class="box box-default collapsed-box">
        <div class="box-header with-border">
            <h3 class="box-title" data-widget="collapse"><strong>Atividades Aditivo</strong></h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                    <i class="fa fa-plus"></i>
                </button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body" style="display: none;">

            <!--<div class="row">
                <div class="col-md-12">
                    <div class="well" id="Atv">
                        @foreach($tce->atividades as $atividade)
                            <label><input type='checkbox' name='atividades[]' value="{{$atividade['id']}}"> {{$atividade['atividade']}}</label><br>
                        @endforeach
                    </div>
                </div>
            </div>
            -->
            <!-- /.row -->

            <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-success">
                            <div class="panel-body">
                                <h4><strong>Atividades Escolhidas no TCE</strong></h4>
                            </div>
                            <div class="panel-footer">
                            @foreach($tce->atividades as $atividade)
                                {{-- <label><input type='checkbox' name='atividades[]' value="{{$atividade['id']}}"> {{$atividade['atividade']}}</label><br> --}}
                                <label><input type='checkbox' name='atividades[]'  value="{{ $atividade['id'] }}" {{ ( is_array(old('atividades')) && in_array($atividade['id'], old('atividades')) ) ? 'checked ' : '' }}/> {{$atividade['atividade']}}</label><br>
                            @endforeach
                            </div>
                        </div>

                        <div class="well" id="Atv" style="height: 350px !important; overflow: auto !important; padding: 10px !important"></div>
                    </div>
            </div>

        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box box-default collapsed-box -->


    <div class="text-center">
        <button type="submit" class="btn btn-danger">Cancelar</button>
        <button type="submit" class="btn btn-success btn-submit">Gerar Aditivo</button>
    </div>
</form>
@section('post-script')
<script>
$('#supervisor_id').select2({
  dropdownAutoWidth : true,
  width: '100%'
});
    jQuery(document).ready(function() {
            // verifica se existe algum elemento com a class
            if ($(".btn-submit").length){
              // adiciona evento desabilitando o botão
              $(".btn-submit").on('click', function(){
                $(this).attr({
                  disabled: true
                });
                // descobrindo o form pai e enviando
                $(this).parents('form:first').submit();
              });
            }
    });

    jQuery(function($){
        $("#dtInicio").mask("99/99/9999");
        $("#dtFim").mask("99/99/9999");
        $("#hrInicio").mask("99:99");
        $("#hrFim").mask("99:99");
        $('#vlAuxilioMensal').maskMoney({thousands:'.',decimal:','});
        $('#vlAuxilioAlimentacao').maskMoney({thousands:'.',decimal:','});
        $('#vlAuxilioTransporte').maskMoney({thousands:'.',decimal:','});
        $("#cdCPFSupervisor").mask("999.999.999-99");
    });


    $(document).ready(function(){
            curso_id = $("#curso_atv_id").val();
            $.get('../../../get-atividades/' + curso_id, function (atividades) {
                        $.each(atividades, function (key, value) {
                        //$('select[name=cidade_id]').append('<option value=' + value.id + '>' + value.nmCidade + '</option>');
                        $("#Atv").append("<label><input type='checkbox' name='atividades[]' value='" + value.id + "'> " + value.atividade + "</label><br>");
                        });
            });

    });

    jQuery(function($){
        $("#dtAditivo").mask("99/99/9999");
        $("#dtInicio").mask("99/99/9999");
        $("#dtFim").mask("99/99/9999");
        $('#vlAuxilioMensal').maskMoney({thousands:'.',decimal:','});
        $('#vlAuxilioAlimentacao').maskMoney({thousands:'.',decimal:','});
        $('#vlAuxilioTransporte').maskMoney({thousands:'.',decimal:','});
    });

    function escolheMeses() {
        dtInicio = $("#dtInicio").val();
        var mes = $("#escolheMes").val();
        if (mes != "") {
            var split = dtInicio.split('/');
            novadata = split[2] + "," +split[1]+","+split[0];

            data_americana = new XDate(novadata);
            data_americana.addMonths(mes);

            while ((data_americana.getDay() == 0) || (data_americana.getDay() == 6)) {
                data_americana.addDays(1);
            }
            $("#dtFim").val(data_americana.toString("dd/MM/yyyy"));
        }
    }

    $("select[name=supervisor_id]").change(function(){
      var selected = $(this).find('option:selected');
      if(selected.data('nome') != null){
        $("#nmSupervisor").attr('readonly','readonly').val(selected.data('nome'));
        $("#dsSupervisorCargo").attr('readonly','readonly').val(selected.data('cargo'));
      }else{
        $("#nmSupervisor").val("");
        $("#dsSupervisorCargo").val("");
        $("#nmSupervisor").removeAttr('readonly');
        $("#dsSupervisorCargo").removeAttr('readonly');
      }
    });
</script>

@endsection
@stop
