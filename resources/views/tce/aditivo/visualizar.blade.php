@extends('adminlte::page')
@if(!empty($titulo))
    @section('title_postfix', ' - '.$titulo)
@endif
@section('content_header')

    <h1>Aditivo</h1>
    <ol class="breadcrumb">
        {{--  {{route('estudante.index')}}  --}}
        <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('tce.index')}}"><i class="fa fa-files-o"></i>TCEs</a></li>
        <li><a href="{{route('tce.show', ['tce' => $aditivo->tce_id])}}"><i class="fa fa-files-o"></i> TCE {{$aditivo->tce_id}}</a></li>
        <li class="active"><i class="fa fa-file-text"></i> Aditivo {{$aditivo->nnAditivo}}</li>
    </ol>
@stop

@section('content')
    @if(session('success'))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> Alerta!</h4>
            {{session('success')}}
    </div>
    @endif

    @if(session('error'))
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Alerta!</h4>
            {{session('error')}}
    </div>
    @endif      
    <div class="row">
        <div class="col-md-12">
            <div class="box box-default">
            
            <!-- /.box-header -->
            <!-- form start -->
                <form action="" method="POST">
                {!! csrf_field() !!}
                    <div class="box-body">
                    <table class="table table-striped table-bordered table-condensed">      
                        <tbody>
                        <tr>
                            <td colspan="4"><label>Nome:</label><br><a href="{{route('estudante.show', ['estudante' => $aditivo->tce->estudante->id])}}">{{$aditivo->tce->estudante->nmEstudante}} </a></td>
                            <td colspan="2"><label>TCE:</label><br><a href="{{route('tce.show', ['tce' => $aditivo->tce_id])}}">{{$aditivo->tce_id}}</a></td>
                        </tr>
                        <tr>
                            <td><label>Supervisor Concedente:</label><br>
                                @if ($aditivo->nmSupervisor === NULL OR $aditivo->nmSupervisor === "")
                                -
                                @else
                                    {{$aditivo->nmSupervisor}}
                                @endif
                            </td>
                            <td><label>Cargo Supervisor Concedente:</label><br>
                                @if ($aditivo->dsSupervisorCargo === NULL OR $aditivo->dsSupervisorCargo === "")
                                -
                                @else
                                    {{$aditivo->dsSupervisorCargo}}
                                @endif
                            </td>
                            <td colspan="2"><label>Supervisor Ie:</label><br>
                                @if ($aditivo->nmSupervisorIe === NULL)
                                -
                                @else
                                    {{$aditivo->nmSupervisorIe}}
                                @endif
                            </td>
                            <td colspan="2"><label>Cargo Supervisor Ie:</label><br>
                                @if ($aditivo->dsSupervisorIeCargo === NULL)
                                -
                                @else
                                    {{$aditivo->dsSupervisorIeCargo}}
                                @endif
                            </td>
                        </tr>

                        <tr>
                                <td><label>Periodo Semanal:</label><br>
                                    @if ($aditivo->periodo_id === NULL)
                                    -
                                    @else
                                        {{$aditivo->periodo->dsPeriodo}}
                                    @endif
                                </td>
                                <td><label>Descrição do Periodo:</label><br>
                                    @if ($aditivo->dsPeriodo === NULL)
                                    -
                                    @else
                                        {{$aditivo->dsPeriodo}}
                                    @endif
                                </td>
                                <td colspan="2"><label>Hora Inical de Estágio:</label><br>
                                    @if ($aditivo->hrInicio === NULL)
                                    -
                                    @else
                                        {{$aditivo->hrInicio}}
                                        @endif
                                </td>
                                <td colspan="2"><label>Hora Final de Estágio:</label><br>
                                    @if ($aditivo->hrFim === NULL)
                                    -
                                    @else
                                        {{$aditivo->hrFim}}
                                    @endif
                                </td>
                            </tr>

                        <tr>
                            <td><label>Auxilio Mensal:</label><br>
                                @if ($aditivo->vlAuxilioMensal === NULL)
                                -
                                @else
                                    R$ {{number_format($aditivo->vlAuxilioMensal, 2, ',', '.')}}
                                @endif
                            </td>
                            <td><label>Auxilio Alimentação:</label><br>
                                @if ($aditivo->vlAuxilioAlimentacao === NULL)
                                -
                                @else
                                    R$ {{number_format($aditivo->vlAuxilioAlimentacao, 2, ',', '.')}}
                                @endif
                            </td>
                            <td colspan="2"><label>Auxilio Transporte:</label><br>
                                @if ($aditivo->vlAuxilioTransporte === NULL)
                                -
                                @else
                                    R$ {{number_format($aditivo->vlAuxilioTransporte, 2, ',', '.')}}
                                @endif
                            </td>
                            <td colspan="1"><label>Total:</label><br>
                                @if ($aditivo->vlAuxilioMensal === NULL)
                                -
                                @else
                                    R$ {{number_format($aditivo->vlAuxilioMensal+$aditivo->vlAuxilioAlimentacao+$aditivo->vlAuxilioTransporte, 2, ',', '.')}}
                                @endif
                            </td>
                            
                        </tr>
                        <tr>
                            <td colspan="1"><label>Vigência Inicio:</label><br>
                                @if ($aditivo->dtInicio === NULL)
                                -
                                @else
                                    {{$aditivo->dtInicio->format("d/m/Y")}}
                                @endif
                            </td>
                            <td colspan="1"><label>Vigência Fim:</label><br>
                                @if ($aditivo->dtFim === NULL)
                                -
                                @else
                                    {{$aditivo->dtFim->format("d/m/Y")}}
                                @endif
                            </td>
                            
        
                        </tr>
                    
                        <tr>
                            <td><label>Semestre/Ano:</label><br>
                                @if ($aditivo->nnSemestreAno === NULL)
                                -
                                @else
                                    {{$aditivo->nnSemestreAno}} 
                                    {{$aditivo->tce->estudante->cursoDaInstituicao->cursoDuracao->nmTipoDuracao}}
                                @endif
                            </td>  
                            <td colspan="6"><label>Motivo Aditivo:</label><br>
                                @if (($aditivo->aditivoOutro === NULL) OR ($aditivo->aditivoOutro === ""))
                                -
                                @else
                                {{$aditivo->aditivoOutro}}
                                @endif
                            </td> 
                        </tr>
                       
                        <tr>
                           <td colspan="5"><label>Atividades:</label><br>
                            <ol type="1">
                            @foreach($aditivo->atividades as $atividade)	
                                <li>{{$atividade->atividade}}</li>
                            @endforeach
                            </ol>
                           </td> 
                        </tr>
                        <tr>
                        <td colspan="6" align="right">
                            <strong>Data do Aditivo:</strong> {{$aditivo->dtAditivo->format("d/m/Y")}}
                            <br><strong>Data de Criação:</strong> {{$aditivo->created_at->format("d/m/Y")}}
                            <br><strong>Cadastrado por:</strong> {{$aditivo->userCadastro->name}}
                        </td>
                        </tr>
                        </tbody>
                    </table>
                     <div class="box-footer text-center">
                        <!-- <a href="{{route('tce.aditivo.edit', [$aditivo->tce_id, $aditivo->id])}}" class="btn btn-warning ad-click-event">Editar Aditivo</a> -->
                        <a href="{{route('aditivo.relatorio.show', [$aditivo->tce_id, $aditivo->id])}}" target="_blank" class="btn btn-success ad-click-event">Ver Aditivo</a>
                    </div>
        <br>       
                   
                    <!-- /.box-body -->

                   
                </form>
          </div>    
  
        </div>
    </div>
    
@section('post-script')
<script>
    jQuery(function($){
        $("#dtInicio").mask("99/99/9999");
        $("#dtFim").mask("99/99/9999");
        $("#hrInicial").mask("99:99");
        $("#hrFinal").mask("99:99");
    });
</script>

@endsection
@stop