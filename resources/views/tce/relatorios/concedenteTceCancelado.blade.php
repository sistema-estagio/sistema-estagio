@extends('adminlte::page')

@if(!empty($titulo))
    @section('title_postfix', ' - '.$titulo)
@endif

@section('content_header')
<h1>Relatórios TCE</h1>
<ol class="breadcrumb">
    <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="{{route('tce.index')}}"><i class="fa fa-file-text-o"></i> Tce</a></li>
    <li class="active"><i class="fa fa-bar-chart"></i> Relatório de Tces</li>
</ol>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <form action="{{route('relatorio.concedente.cancelado.tce.pdf')}}" method="GET" target="_blank">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="flTipo">*Concedente</label>
                                    <select class="form-control" name="concedente_id" id="concedente_id" required="required">
                                        <option value="" selected>Selecione um Concedente...</option>
                                        @foreach($Concedentes as $concedente)
                                            <option value="{{$concedente->id}}">{{$concedente->nmRazaoSocial}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="sec_conc_id">*Secretaria Concedente</label>
                                        <select class="form-control" id="sec_conc_id" name="sec_conc_id">
                                            <option value="" selected>Selecione a Concedente</option> 
                                        </select> 
                                    </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="flTipo">*Ordenacao</label>
                                    <select class="form-control" name="ordenacao">
                                        <option value="id">TCE</option>
                                        <option value="estudante_id">NOME</option>
                                        <option value="dtFim">VENCIMENTO</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="dataInicial">Data Inicial</label>
                                    <input id="dataInicial" name="dataInicial" class="form-control dataMask" placeholder="01/01/2018" type="text" id="dataInicial">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="dataFinal">Data Final</label>
                                    <input id="dataFinal" name="dataFinal" class="form-control dataMask" placeholder="31/12/2018" type="text" id="dataFinal">
                                    <small class="badge badge-danger" id="dataAlert" style="background-color:red;"></small>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer text-left">
                            <button type="submit" class="btn btn-primary">Buscar</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.box -->
        </div>
    </div>
@section('post-script')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#concedente_id').select2();
            $('#sec_conc_id').select2();
        });
        
        $('select[name=concedente_id]').change(function () {
            var idConcedente = $(this).val();
          
            $.get('../../get-secretarias/' + idConcedente, function (secretarias) {
               if(secretarias.length > 0){
                //console.log(secretarias);
                $('select[name=sec_conc_id]').empty();
                $('select[name=sec_conc_id]').append('<option value=>Selecione a Secretaria</option>');
                $('select[name=sec_conc_id]').append('<option>=============</option>');
               } else {
                //console.log('SEM SECRETARIA');
                $('select[name=sec_conc_id]').empty();
                $('select[name=sec_conc_id]').append('<option value=>Sem Secretaria</option>');
               }
                
                $.each(secretarias, function (key, value) {
                    $('select[name=sec_conc_id]').append('<option value=' + value.id + '>'  + value.nmSecretaria + '</option>');
                });
                
            });
        });

        $('.dataMask').mask('00/00/0000')
    </script>
@endsection
@stop