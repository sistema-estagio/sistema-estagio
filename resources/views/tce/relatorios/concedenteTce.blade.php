@extends('adminlte::page')

@if(!empty($titulo))
@section('title_postfix', ' - '.$titulo)
@endif

@section('content_header')
<h1>Relatórios TCE</h1>
<ol class="breadcrumb">
  <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
  <li><a href="{{route('tce.index')}}"><i class="fa fa-file-text-o"></i> Tce</a></li>
  <li class="active"><i class="fa fa-bar-chart"></i> Relatório de Tces</li>
</ol>
@stop

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="box">
      <form action="{{route('relatorio.concedente.tce.pdf')}}" method="GET" target="_blank">
        <div class="box-body">
          <div class="row">
            <div class="col-md-8">
              <div class="form-group">
                <label for="flTipo">*Concedente</label>
                <select class="form-control" name="concedente_id" id="concedente_id" required="required">
                  <option value="" selected>Selecione um Concedente...</option>
                  <!--<option value="todos" selected>Todos</option>-->
                  @foreach($Concedentes as $concedente)
                  <option value="{{$concedente->id}}">{{$concedente->id}} - {{$concedente->nmRazaoSocial}}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label for="flTipo">*Modelo de Relatório</label>
                <select class="form-control" name="modelo" required="required">
                  <option value="" selected>Escolha...</option>
                  <option value="sim">Concedente</option>
                  <option value="nao">Upa</option>
                </select>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label for="flTipo">*Ordenacao</label>
                <select class="form-control" name="ordenacao">
                  <option value="id">TCE</option>
                  <option value="estudante_id">NOME</option>
                  <option value="dtFim">VENCIMENTO</option>
                </select>
              </div>
            </div>

          </div>
          <div class="box-footer text-left">
            <button type="submit" class="btn btn-primary">Buscar</button>
          </div>
        </div>
      </form>
    </div>
    <!-- /.box -->
  </div>
</div>
<h3>Relatórios TCE Concedentes e Secretarias</h3>
<div class="row">
  <div class="col-md-12">
    <div class="box">
      <form action="{{route('relatorio.concedente.sec.tce.pdf')}}" method="GET" target="_blank">
        <div class="box-body">
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label for="flTipo">*Concedente</label>
                <select class="form-control" name="sec_concedente_id" id="sec_concedente_id" required="required">
                  <option value="" selected>Selecione um Concedente...</option>
                  <!--<option value="todos" selected>Todos</option>-->
                  @foreach($Concedentes as $concedente)
                  <option value="{{$concedente->id}}">{{$concedente->nmRazaoSocial}}</option>
                  @endforeach
                </select>
              </div>
            </div>

            <div class="col-md-8">
              <div class="form-group">
                <label for="sec_conc_id">*Secretaria Concedente</label>
                <select class="form-control" id="sec_conc_id" name="sec_conc_id">
                  <option value="" selected>Selecione a Concedente</option>
                </select>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-3">
              <div class="form-group">
                <label for="flTipo">*Modelo de Relatório</label>
                <select class="form-control" name="modelo" required="required">
                  <option value="" selected>Escolha...</option>
                  <option value="seguro">Seguro</option>
                  <option value="sim">Concedente</option>
                  <option value="nao">Upa</option>
                </select>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="flTipo">*Ordenacao</label>
                <select class="form-control" name="ordenacao">
                  <option value="id">TCE</option>
                  <option value="estudante_id">NOME</option>
                  <option value="dtFim">VENCIMENTO</option>
                </select>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="">De</label>
                <input type="text" class="form-control dates" name="dataInicial" placeholder="dd/mm/aaaa" value="">
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="">Até</label>
                <input type="text" class="form-control dates" name="dataFinal" placeholder="dd/mm/aaaa" value="{{\Carbon\Carbon::now()->format('d/m/Y')}}">
              </div>
            </div>
          </div>
          <div class="box-footer text-left">
            <button type="submit" class="btn btn-primary" name="tipo" value="PDF">Buscar</button>
            {{--  <button type="submit" class="btn btn-success" name="tipo" value="HTML">HTML</button>  --}}
            <button type="submit" class="btn btn-success" name="protocolo" value="protocolo">Protocolo</button>
          </div>
        </div>
      </form>
    </div>
    <!-- /.box -->
  </div>
</div>
@section('post-script')
<script type="text/javascript">
$(".dates").mask("99/99/9999");
$(document).ready(function(){
  $('#concedente_id').select2();
  $('#sec_concedente_id').select2();
});
$('select[name=sec_concedente_id]').change(function () {
  var idConcedente = $(this).val();

  $.get('../../get-secretarias/' + idConcedente, function (secretarias) {

    $('select[name=sec_conc_id]').empty();
    $('select[name=sec_conc_id]').append('<option value=>Selecione a Secretaria</option>');
    $('select[name=sec_conc_id]').append('<option>=============</option>');
    $.each(secretarias, function (key, value) {
      $('select[name=sec_conc_id]').append('<option value=' + value.id + '>'  + value.nmSecretaria + '</option>');
    });

  });
});
</script>
@endsection
@stop
