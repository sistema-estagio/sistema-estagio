@extends('adminlte::page')

@if(!empty($titulo))
@section('title_postfix', ' - '.$titulo)
@endif

@section('content_header')
    <h1>Relatórios Tce</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('tce.index')}}"><i class="fa fa-file-text-o"></i> Tce</a></li>
        <li class="active"><i class="fa fa-bar-chart"></i> Relatório de Tces</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <form action="{{route('relatorio.tce.pdf')}}" method="GET" target="_blank">
                    <div class="box-body">
                        <div class="row">
                            {{-- <div class="col-md-6">
                                <div class="form-group">
                                    <label for="flTipo">*Tipo de TCE</label>
                                    <select class="form-control" name="tipo">
                                        <!--<option value="todos">Todos Tces</option>-->
                                        <option value="vencer">Tces à Vencer</option>
                                        <option value="cancelado">Tces Cancelados</option>
                                    </select>
                                </div>
                            </div> --}}
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="flTipo">*Ordenacao</label>
                                    <select class="form-control" name="ordenacao">
                                        <option value="id">TCE</option>
                                        <option value="estudante">NOME</option>
                                        <!--<option value="dtFim">VENCIMENTO</option>-->
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer text-left">
                            <button type="submit" class="btn btn-primary" name="tipo_relatorio" value="html">Buscar</button>
                            <!--<button type="submit" class="btn btn-success" name="tipo_relatorio" value="excel">Excel</button>
                            <button type="submit" class="btn btn-danger" name="tipo_relatorio" value="pdf">PDF</button>-->
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.box -->
        </div>
    </div>
@section('post-script')

    <script type="text/javascript">

        $('select[name=estado_id]').change(function () {
            var idEstado = $(this).val();

            $.get('/sysEstagio/get-cidades/' + idEstado, function (cidades) {

                $('select[name=cidade_id]').empty();
                $('select[name=cidade_id]').append('<option value="">Todos</option>');
                $.each(cidades, function (key, value) {
                    $('select[name=cidade_id]').append('<option value=' + value.id + '>' + value.nmCidade + '</option>');
                });
            });
        });



    </script>
@endsection
@stop