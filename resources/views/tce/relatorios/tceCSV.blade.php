<!DOCTYPE html>
<html>
<head>
    <title>Relatório</title>
</head>
    
    <body>
           
    <table>
        <thead>
            {{-- <tr colspan="7">
                <th style="background-color: #666; color: #fff;">RELAÇÃO DE TCE'S - Gerado em: {{$dtHoje->format('d/m/Y')}}</th>
            </tr> --}}
            <tr>
                    {{--  <td>
                        <?php $image_path = '/img/relatorios/logo_default.png'; ?>
                        <img src="{{ public_path() . $image_path }}" alt="Upa - Estagio" />
                    </td>  --}}
                    <td>
                        <h1 style="text-align: right">
                            <strong>RELATÓRIO TCE´S</strong>
                        </h1>
                    </td>
                </tr>
        </thead>
        <thead>
        <tr>
            <th>#</th>
            <th>MIGRAÇÃO</th>
            <th>TCE</th>
            <th>NOME</th>
            <th>CONCEDENTE</th>
            <th>DT.INICIO</th>
            <th>DT.FIM</th>
        </tr>
        </thead>
        <tbody>
        @php $total = 1; @endphp
        @foreach($Estudante as $estudante)
        @foreach($estudante->tce->where('dtCancelamento',NULL) as $tce)
        {{-- //Esquema Mudar Cor dos vencidos --}}
        
        @if($tce->aditivos->count() > 0)
            @if($tce->aditivos()->latest()->first()->dtInicio != NULL)
                <tr @if($tce->aditivos()->latest()->first()->dtFim <= \Carbon\Carbon::now()->toDateString()) class="bg-danger" style="color:red;background-color:#f7f7f7;" @endif>
            @else
                <tr @if($tce->dtFim < \Carbon\Carbon::now()->toDateString()) class="bg-danger" style="color:red;background-color:#f7f7f7;" @endif>
            @endif
        @else
            <tr @if($tce->dtFim < \Carbon\Carbon::now()->toDateString()) class="bg-danger" style="color:red;background-color:#f7f7f7;" @endif>
        @endif
        {{-- //Fim Esquema mudança de cor dos vencidos --}}
                <td>{{$total}}</td>
                @if($tce->migracao)
                    <td>{{$tce->migracao}}</td>
                @else
                    <td>-</td>
                @endif
                @if($tce->aditivos->where('dtInicio','<>',NULL)->count() > 0)
                    <td>{{$tce->id}}/{{$tce->aditivos->where('dtInicio','<>',NULL)->where('dtFim','<>',Null)->count()}}</td>
                @else
                    <td>{{$tce->id}}</td>
                @endif
                <td>{{$tce->estudante->nmEstudante}}</td>
                <td>{{$tce->concedente->nmRazaoSocial}}</td>
                <td>{{$tce->dtInicio->format('d/m/Y')}}</td>
                <td>
                    @if($tce->aditivos->count() > 0)
                        @if($tce->aditivos()->latest()->first()->dtInicio != NULL)
                            {{$tce->aditivos()->latest()->first()->dtFim->format('d/m/Y')}}
                        @else
                            {{$tce->dtFim->format('d/m/Y')}}
                        @endif
                    @else
                        {{$tce->dtFim->format('d/m/Y')}}
                    @endif
                </td>
            </tr>
            @endforeach
        @php $total++; @endphp
        @endforeach
        </tbody>
    </table>
</body>
</html>