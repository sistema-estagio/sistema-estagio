<!DOCTYPE html>
<html>
<head>
    <title>Relatório Tce´s</title>
    <style>
            html { margin: 30px}
            body {
                font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif;
            }
    
            footer.fixar-rodape {
                bottom: 0;
                left: 0;
                height: 40px;
                position: fixed;
                width: 100%;
            }
    
            .novaPagina {
                page-break-before: always;
                page-break-after: always
            }
    
            div.body-content {
                /** Essa margem vai evitar que o conteudo fique por baixo do rodapé **/
                margin-bottom: 40px;
            }
    
            .col-12 {
                width: 100% !important;
            }
    
            .col-6 {
                width: 50% !important;
            }
    
            .col-6,
            .col-12 {
                float: left !important;
            }
    
            .table {
                border-collapse: separate;
                border-spacing: 0;
                min-width: 350px;
                margin-bottom: 10px;
            }
    
            .table tr th,
            .table tr td {
                border-right: 1px solid #bbb;
                border-bottom: 1px solid #bbb;
                padding: 5px;
            }
    
            .table th {
                border-top: solid 1px #bbb;
            }
    
            .table tr th:first-child,
            .table tr th:last-child {
                border-top: solid 1px #bbb;
            }
    
            .table tr th:first-child,
            .table tr td:first-child {
                border-left: 1px solid #bbb;
            }
    
            .table tr th:first-child,
            .table tr td:first-child {
                border-left: 1px solid #bbb;
            }
    
            .table tr th {
                background: #eee;
                text-align: left;
            }
    
            .table.Info tr th,
            .table.Info tr:first-child td {
                border-top: 1px solid #bbb;
            }
            /* top-left border-radius */
    
            .table tr:first-child th:first-child,
            .table.Info tr:first-child td:first-child {
                border-top-left-radius: 6px;
            }
            /* top-right border-radius */
    
            .table tr:first-child th:last-child,
            .table.Info tr:first-child td:last-child {
                border-top-right-radius: 6px;
            }
            /* bottom-left border-radius */
    
            .table tr:last-child td:first-child {
                border-bottom-left-radius: 6px;
            }
            /* bottom-right border-radius */
    
            .table tr:last-child td:last-child {
                border-bottom-right-radius: 6px;
            }
        </style>
    </head>
    
    <body>
        <div id="body">
            <div id="content">
                <div class="page" style="font-size: 7pt">
                    <table style="width: 100%;" class="header">
                        <tr>
                            <td>
                                @php $image_path = 'http://teste.universidadepatativa.com.br/sysEstagio/img/relatorios/logo_default.png'; @endphp
                                <img src="{{ $image_path }}" alt="Upa - Estagio" width="170px" height="80px"/>
                            </td>
                            <td>
                                <h1 style="text-align: right">
                                    <strong>RELATÓRIO TCE´S</strong>
                                </h1>
                            </td>
                        </tr>
                    </table>
                    <hr style=" margin-bottom: 0px; border-bottom: 1px solid #DCDCDC ;">
                    <br>

<!-- grupamento po Estado -->

    <table id="tabela" class="table table-bordered" style="width: 100%;">
        <tr>
            <th width="1">MIGRAÇÃO</th>
            <th width="1">TCE</th>
            <th>NOME</th>
            <th>CONCEDENTE</th>
            <th>DATA INICIO</th>
            <th>DATA FIM</th>
        </tr>
        @php $totalTCE = 0; @endphp
        @foreach($Estudante as $estudante)
                
                @foreach($estudante->tce->where('dtCancelamento',NULL) as $tce)
                {{-- //Esquema Mudar Cor dos vencidos --}}
                @if($tce->aditivos->count() > 0)
                    @if($tce->aditivos()->latest()->first()->dtInicio != NULL)
                        <tr @if($tce->aditivos()->latest()->first()->dtFim <= \Carbon\Carbon::now()->toDateString()) class="bg-danger" style="color:red;background-color:#f7f7f7;" @endif>
                    @else
                        <tr @if($tce->dtFim < \Carbon\Carbon::now()->toDateString()) class="bg-danger" style="color:red;background-color:#f7f7f7;" @endif>
                    @endif
                @else
                    <tr @if($tce->dtFim < \Carbon\Carbon::now()->toDateString()) class="bg-danger" style="color:red;background-color:#f7f7f7;" @endif>
                @endif
                {{-- //Fim Esquema mudança de cor dos vencidos --}}
                        @if($tce->migracao)
                            <td>{{$tce->migracao}}</td>
                        @else
                            <td>-</td>
                        @endif
                        @if($tce->aditivos->where('dtInicio','<>',NULL)->count() > 0)
                            <td>{{$tce->id}}/{{$tce->aditivos->where('dtInicio','<>',NULL)->where('dtFim','<>',Null)->count()}}</td>
                        @else
                            <td>{{$tce->id}}</td>
                        @endif
                        <td>{{$tce->estudante->nmEstudante}}</td>
                        <td>{{$tce->concedente->nmRazaoSocial}}</td>
                        <td>{{$tce->dtInicio->format('d/m/Y')}}</td>
                        <td>
                            @if($tce->aditivos->count() > 0)
                                @if($tce->aditivos()->latest()->first()->dtInicio != NULL)
                                    {{$tce->aditivos()->latest()->first()->dtFim->format('d/m/Y')}}
                                @else
                                    {{$tce->dtFim->format('d/m/Y')}}
                                @endif
                            @else
                                {{$tce->dtFim->format('d/m/Y')}}
                            @endif
                        </td>
                    </tr>
                @php $totalTCE++; @endphp
                @endforeach
        @endforeach
    </table>
    <div style="margin-top:-10px;">Total de Resultados: {{$totalTCE}}</div>
<!-- FIM. Agrupamento po Estado -->
@include('relatorios.inc_rodape_html')

    </div>
</div>

</body>
</html>