<!DOCTYPE html>
<html>
<head>
    <title>Relatório</title>
</head>
    
    <body>
           
    <table>
        <thead>
            {{-- <tr colspan="7">
                <th style="background-color: #666; color: #fff;">RELAÇÃO DE TCE'S - Gerado em: {{$dtHoje->format('d/m/Y')}}</th>
            </tr> --}}
            <tr>
                    {{--  <td>
                        <?php $image_path = '/img/relatorios/logo_default.png'; ?>
                        <img src="{{ public_path() . $image_path }}" alt="Upa - Estagio" />
                    </td>  --}}
                    <td>
                        <h1 style="text-align: right">
                            <strong>RELATÓRIO TCE´S</strong>
                        </h1>
                    </td>
                </tr>
        </thead>
        <thead>
        <tr>
            {{--  concedente  --}}
            <th>CNPJ</th>
            <th>RAZÃO SOCIAL</th>
            <th>REP.</th>
            <th>CIDADE/UF</th>
            <th>END.</th>
            <th>BAIRRO</th>
            <th>CEP</th>
            <th>TELEFONE</th>
            {{--  instituicao de ensino  --}}
            <th>CNPJ IE</th>
            <th>INSTITUIÇÂO</th>
            <th>CURSO</th>
            <th>RESP.</th>
            <th>CIDADE/UF</th>
            <th>END.</th>
            <th>BAIRRO</th>
            <th>CEP</th>
            <th>TELEFONE</th>
            {{--  Estudante  --}}
            <th>CPF</th>
            <th>RG</th>
            <th>ESTUDANTE</th>
            <th>NASCIMENTO</th>
            <th>TELEFONE</th>
            <th>CIDADE/UF</th>
            <th>ENDEREÇO</th>
            <th>BAIRRO</th>
            <th>CEP</th>
            {{--  TCE  --}}
            <th>TCE</th>
            <th>ATIVIDADES</th>
            <th>DT.INICIO</th>
            <th>DT.FIM</th>
            <th>SUPERVISOR</th>
        </tr>
        </thead>
        <tbody>
        @php $total = 1; @endphp
        @foreach($Estudante as $estudante)
        @foreach($estudante->tce->where('dtCancelamento',NULL) as $tce)
        {{-- //Esquema Mudar Cor dos vencidos --}}
        
        {{--  @if($tce->aditivos->count() > 0)
            @if($tce->aditivos()->latest()->first()->dtInicio != NULL)
                <tr @if($tce->aditivos()->latest()->first()->dtFim <= \Carbon\Carbon::now()->toDateString()) class="bg-danger" style="color:red;background-color:#f7f7f7;" @endif>
            @else
                <tr @if($tce->dtFim < \Carbon\Carbon::now()->toDateString()) class="bg-danger" style="color:red;background-color:#f7f7f7;" @endif>
            @endif
        @else
            <tr @if($tce->dtFim < \Carbon\Carbon::now()->toDateString()) class="bg-danger" style="color:red;background-color:#f7f7f7;" @endif>
        @endif  --}}
        {{-- //Fim Esquema mudança de cor dos vencidos --}}
                {{--  Concedente  --}}
                <td>{{$tce->concedente->cdCnpjCpf}}</td>
                <td>{{$tce->concedente->nmRazaoSocial}}</td>
                <td>{{$tce->concedente->nmResponsavel}}</td>
                <td>{{$tce->concedente->cidade->nmCidade}}/{{$tce->concedente->cidade->estado->cdUF}}</td>
                <td>{{$tce->concedente->dsEndereco}}, {{$tce->concedente->nnNumero}}</td>
                <td>{{$tce->concedente->nmBairro}}</td>
                <td>{{$tce->concedente->cdCEP}}</td>
                <td>{{$tce->concedente->dsFone}}</td>
                {{--  Instituição de Ensino  --}}
                <td>{{$tce->instituicao->cdCNPJ}}</td>
                <td>{{$tce->instituicao->nmInstituicao}}</td>
                <td>{{$tce->instituicao->cdCNPJ}}</td>
                <td>{{$tce->cursoDaInstituicao->curso->nmCurso}}</td>
                <td>{{$tce->instituicao->nmDiretor}}</td>
                <td>{{$tce->instituicao->cidade->nmCidade}}/{{$tce->instituicao->cidade->estado->cdUF}}</td>
                <td>{{$tce->instituicao->dsEndereco}}, {{$tce->instituicao->nnNumero}}</td>
                <td>{{$tce->instituicao->nmBairro}}</td>
                <td>{{$tce->instituicao->cdCEP}}</td>
                <td>{{$tce->instituicao->dsFone}}</td>
                {{--  Estudante  --}}
                <td>{{$tce->estudante->cdCPF}}</td>
                <td>{{$tce->estudante->cdRG}}</td>
                <td>{{$tce->estudante->nmEstudante}}</td>
                <td>{{$tce->estudante->dtNascimento}}</td>
                <td>{{$tce->estudante->dsFone}}</td>
                <td>{{$tce->estudante->cidade->nmcidade}}/{{$tce->estudante->cidade->estado->cdUF}}</td>
                <td>{{$tce->estudante->dsEndereco}}, {{$tce->estudante->nnNumero}}</td>
                <td>{{$tce->estudante->nmBairro}}</td>
                <td>{{$tce->estudante->cdCEP}}</td>
                {{--  Tce  --}}
                @if($tce->migracao)
                    <td>{{$tce->migracao}}</td>
                @else
                <td>{{$tce->id}}</td>
                @endif
                <td>
                @foreach($tce->atividades as $atividade)	
                    {{$atividade->atividade}};
                @endforeach
                </td>
                <td>{{$tce->dtInicio->format('d/m/Y')}}</td>
                <td>
                    @if($tce->aditivos->count() > 0)
                        @if($tce->aditivos()->latest()->first()->dtInicio != NULL)
                            {{$tce->aditivos()->latest()->first()->dtFim->format('d/m/Y')}}
                        @else
                            {{$tce->dtFim->format('d/m/Y')}}
                        @endif
                    @else
                        {{$tce->dtFim->format('d/m/Y')}}
                    @endif
                </td>
                <td>
                        @if($tce->aditivos->count() > 0)
                            @if($tce->aditivos()->latest()->first()->nmSupervisor != "")
                                {{$tce->aditivos()->latest()->first()->nmSupervisor}}
                            @else
                                {{$tce->nmSupervisor}}
                            @endif
                        @else
                            {{$tce->nmSupervisor}}
                        @endif
                </td>
            </tr>
            @endforeach
        @php $total++; @endphp
        @endforeach
        </tbody>
    </table>
</body>
</html>