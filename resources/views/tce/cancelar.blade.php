@extends('adminlte::page')
@if(!empty($titulo))
@section('title_postfix', ' - '.$titulo)
@endif
@section('content_header')

    <h1>Tce</h1>
    <ol class="breadcrumb">
        {{--  {{route('estudante.index')}}  --}}
        <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('tce.index')}}"><i class="fa fa-files-o"></i>TCE</a></li>
        <li><a href="{{route('tce.show', ['tce' => $tce->id])}}"><i class="fa fa-files-o"></i> {{$tce->id}}</a></li>
        <li class="active"><i class="fa fa-remove"></i> Cancelamento de TCE</li>
    </ol>
@stop

@section('content')
    @if(session('success'))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> Alerta!</h4>
            {{session('success')}}
    </div>
    @endif

    @if(session('error'))
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Alerta!</h4>
            {{session('error')}}
    </div>
    @endif

    @if($errors->any())
        <ul class="alert-danger">
            <h3>Whoops, Houve algum erro!</h3>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    <div class="row">
        <div class="col-md-12">
            <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title"><strong>TCE: </strong>{{$tce->id}} - <strong>Estudante: </strong>{{$tce->estudante->nmEstudante}}</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
                <form action="{{route('tce.cancelar', ['tce' => $tce->id])}}" method="POST">
                {!! csrf_field() !!}
                {!! method_field('PUT') !!}
               
                    <div class="box-body">
                                       
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="dtCancelamento">*Data de Encerramento</label>
                                <input class="form-control" id="dtCancelamento" name="dtCancelamento"  type="text" placeholder="dd/mm/aaaa">
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="form-group">
                                <label for="motivoCancelamento_id">*Motivo do Cancelamento</label>
                                <select class="form-control" id="motivoCancelamento_id" name="motivoCancelamento_id">
                                    @foreach($motivosCancelamento as $motivo)
                                        <option value="{{$motivo['id']}}">{{$motivo['dsMotivoCancelamento']}}</option>
                                    @endforeach
                                </select> 
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="dtIniio">Descrição do Motivo</label>
                                <input class="form-control" id="dsMotivoCancelamento" name="dsMotivoCancelamento" placeholder="Caso queira detalhar melhor..." type="text">
                                <span class="help-block">Digite aqui, os motivos que levaram ao cancelamento deste TCE</span>
                            </div>
                        </div>
                    </div>

                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer text-center">
                        <button type="submit" class="btn btn-danger" onclick="return confirm('Você está cancelando o TCE {{$tce->id}}');">Cancelar TCE</button>
                    </div>
                </form>
          </div>    
  
        </div>
    </div>
@section('post-script')
<script>
    jQuery(function($){
        $("#dtCancelamento").mask("99/99/9999");
    });
</script>

@endsection
@stop