@extends('adminlte::page')
@if(!empty($titulo))
@section('title_postfix', ' - '.$titulo)
@endif
@section('content_header')

<h1>Tce
  @if($tce->migracao != NULL)
  Migração N° {{$tce->migracao}}
  @else
  N° {{$tce->id}}
  @endif
</h1>
<ol class="breadcrumb">
  {{--  {{route('estudante.index')}}  --}}
  <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
  <li><a href="{{route('tce.index')}}"><i class="fa fa-files-o"></i>TCE</a></li>
  <li class="active"><i class="fa fa-file"></i> {{$tce->id}}</li>
</ol>
@stop

@section('content')
@if(session('success'))
<div class="alert alert-success alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
  <h4><i class="icon fa fa-check"></i> Alerta!</h4>
  {{session('success')}}
</div>
@endif

@if(session('error'))
<div class="alert alert-danger alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
  <h4><i class="icon fa fa-ban"></i> Alerta!</h4>
  {{session('error')}}
</div>
@endif
<div class="row">
  <div class="col-md-12">
    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title"></h3>

        <div class="box-tools pull-right">
          <b>Tipo TCE:</b> {{$tce->relatorio->nmRelatorio}}
        </div>
      </div>
      <!-- /.box-header -->
      <!-- form start -->
      <form action="" method="POST">
        {!! csrf_field() !!}
        <div class="box-body">
          <!-- Mostra detalhe de TCE Cancelado-->
          @if($tce->dtCancelamento != NULL)
          <div class="alert alert-danger alert-dismissible">
            {{-- <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> --}}
            <h4><i class="icon fa fa-ban"></i> Aviso</h4>
            Esse TCE foi cancelado
            @if(isset($tce->cancelado_at))
            em {{$tce->cancelado_at->format('d/m/Y')}}
            @endif
            por
            @if(isset($tce->userCancelamento->name))
            {{$tce->userCancelamento->name}}
            @else
            Propia Concedente
            @endif
            <br>
            <b>Data Recisão:</b> {{$tce->dtCancelamento->format('d/m/Y')}}
            <br>
            <b>Motivo Cancelamento:</b> {{$tce->MotivoCancelamento->dsMotivoCancelamento}}
            @if($tce->dsMotivoCancelamento != "")
            <br> <b>Descrição Motivo Cancelamento: </b> {{$tce->dsMotivoCancelamento}}
            @endif
          </div>
          @endif
          <table class="table table-striped table-bordered table-condensed">
            <tbody>
              @if($tce->dt4Via != NULL)
              <tr class="bg-green disabled color-palette">
                <td colspan="5"><center><b>Tce Validado</b></center></td>
              </tr>
              @else
              <tr class="bg-red disabled color-palette">
                <td colspan="5"><center><b>Tce Não Validado</b></center></td>
              </tr>
              @endif
              <tr>
                <td colspan="3"><label>Nome:</label><br><a href="{{route('estudante.show', ['estudante' => $tce->estudante->id])}}">{{$tce->estudante->nmEstudante}} </a></td>
                <td colspan="2"><label>CPF:</label><br>{{$tce->estudante->cdCPF}}</a></td>
              </tr>
              <tr>
                <td colspan="2"><label>Instituição de Ensino:</label><br><a href="{{route('instituicao.show', ['instituicao' => $tce->instituicao->id])}}">{{$tce->instituicao->nmInstituicao}}</a></td>
                <td colspan="2"><label>Curso:</label><br>{{$tce->cursoDaInstituicao->curso->nmCurso}}</td>
                <td><label>Semestre/Ano TCE:</label><br>{{$tce->nnSemestreAno}} {{$tce->cursoDaInstituicao->cursoDuracao->nmTipoDuracao}}</td>

              </tr>
              @if($tce->sec_conc_id <> NULL)
              <tr>
                <td colspan="2"><label>Concedente:</label><br><a href="{{route('concedente.show', ['concedente' => $tce->concedente->id])}}">{{$tce->concedente->nmRazaoSocial}}</a></td>
                <td colspan="3"><label>Secretaria:</label><br>{{$tce->secConcedente->nmSecretaria}}</td>
              </tr>
              @else
              <tr>
                <td colspan="5"><label>Concedente:</label><br><a href="{{route('concedente.show', ['concedente' => $tce->concedente->id])}}">{{$tce->concedente->nmRazaoSocial}}</a></td>
              </tr>
              @endif
              @if($tce->dsLotacao <> NULL OR $tce->dsLotacao <> "")
              <tr>
                <td colspan="5"><label>Lotação:</label><br>{{$tce->dsLotacao}}</a></td>
              </tr>
              @endif
              <!--Inicio se houver informado nomes e cargos supervisores no TCE.-->
              @if(($tce->nmSupervisor === NULL OR $tce->nmSupervisor === "") AND ($tce->nmSupervisorIe === NULL OR $tce->nmSupervisorIe === ""))
              @else
              <tr>
                <td><label>Supervisor Concedente:</label><br>
                  @if ($tce->nmSupervisor === NULL OR $tce->nmSupervisor === "")
                  -
                  @else
                  {{$tce->nmSupervisor}}
                  @if($tce->cdCPFSupervisor != NULL)
                  <br><i>CPF: {{$tce->cdCPFSupervisor}}</i>
                  @endif
                  @endif
                </td>
                <td><label>Cargo Supervisor Concedente:</label><br>
                  @if ($tce->dsSupervisorCargo === NULL OR $tce->dsSupervisorCargo === "")
                  -
                  @else
                  {{$tce->dsSupervisorCargo}}
                  @endif
                </td>
                <td colspan="2"><label>Supervisor Ie:</label><br>
                  @if ($tce->nmSupervisorIe === NULL OR $tce->nmSupervisorIe === "")
                  -
                  @else
                  {{$tce->nmSupervisorIe}}
                  @endif
                </td>
                <td colspan="2"><label>Cargo Supervisor Ie:</label><br>
                  @if ($tce->dsSupervisorIeCargo === NULL OR $tce->dsSupervisorIeCargo === "")
                  -
                  @else
                  {{$tce->dsSupervisorIeCargo}}
                  @endif
                </td>
              </tr>
              @endif
              <!--Fim se não houver informado nomes e cargos supervisores no TCE.-->
              <tr>
                <td><label>Auxilio Mensal:</label><br>
                  @if ($tce->vlAuxilioMensal === NULL)
                  -
                  @else
                  R$ {{number_format($tce->vlAuxilioMensal, 2, ',', '.')}}
                  @endif
                </td>
                <td><label>Auxilio Alimentação:</label><br>
                  @if ($tce->vlAuxilioAlimentacao === NULL)
                  -
                  @else
                  R$ {{number_format($tce->vlAuxilioAlimentacao, 2, ',', '.')}}
                  @endif
                </td>
                <td colspan="2"><label>Auxilio Transporte:</label><br>
                  @if ($tce->vlAuxilioTransporte === NULL)
                  -
                  @else
                  R$ {{number_format($tce->vlAuxilioTransporte, 2, ',', '.')}}
                  @endif
                </td>
                <td colspan="1"><label>Total:</label><br>
                  @if ($tce->vlAuxilioMensal === NULL)
                  -
                  @else
                  R$ {{number_format($tce->vlAuxilioMensal+$tce->vlAuxilioAlimentacao+$tce->vlAuxilioTransporte, 2, ',', '.')}}
                  @endif

                </td>

              </tr>
              @if($tce->auxDescriminado)
              <tr>
                <td colspan="5"><label>Auxilio Descriminado:</label> {{$tce->auxDescriminado}}
                </tr>
                @endif
                <tr>
                  <td colspan="1"><label>Vigência Inicio:</label><br>{{$tce->dtInicio->format("d/m/Y")}}</td>
                  <td colspan="1"><label>Vigência Fim:</label><br>{{$tce->dtFim->format("d/m/Y")}}</td>
                  <td colspan="1"><label>Hora Inical de Estágio:</label><br>
                    @if ($tce->hrInicio === NULL)
                    -
                    @else
                    {{$tce->hrInicio}}
                    @endif
                  </td>
                  <td colspan="2"><label>Hora Final de Estágio:</label><br>
                    @if ($tce->hrFim === NULL)
                    -
                    @else
                    {{$tce->hrFim}}
                    @endif
                  </td>
                </tr>

                <tr>
                  <td><label>Semestre/Ano TCE:</label><br>{{$tce->nnSemestreAno}} {{$tce->cursoDaInstituicao->cursoDuracao->nmTipoDuracao}}</td>
                  <td><label>Periodo Semanal:</label><br>{{$tce->periodo->dsPeriodo}}</td>
                  <td><label>Descrição do Periodo:</label><br>{{$tce->dsPeriodo}}</td>
                  <td colspan="3"><label>Número Apolice:</label><br>{{$tce->dsApolice}}</td>
                </tr>
                <tr>
                  <td colspan="5"><label>Atividades:</label><br>
                    <ol type="1">
                      @foreach($tce->atividades as $atividade)
                      <li>{{$atividade->atividade}}</li>
                      @endforeach
                    </ol>
                  </td>
                </tr>

                <tr>
                  <td colspan="6" align="right">
                    <label>Criado por: </label>
                    {{$tce->userCadastro->name}}<b> Em:</b> {{$tce->created_at->format("d/m/Y")}}
                    @if($tce->dt4Via != NULL)
                    <br><label>Validado por: </label>
                    {{$tce->userValidacao->name}} <b> Em:</b> {{$tce->dt4Via->format("d/m/Y")}}
                    @endif
                  </td>
                </tr>

              </tbody>
            </table>
            <div class="box-footer text-center">
              <a href="{{route('tce.relatorio.show', [$tce->id, $tce->relatorio_id])}}" target="_blank" class="btn btn-success ad-click-event">Ver TCE</a>
              @if($tce->dtCancelamento == NULL)
              <div class="btn-group">
                <button type="button" class="btn btn-primary">Aditivo</button>
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                  <span class="caret"></span>
                  <span class="sr-only">Toggle Dropdown</span>
                </button>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="{{route('tce.aditivo.create', $tce->id)}}" onclick="return confirm('Deseja Realmente Aditivar o TCE {{$tce->id}} ?');">Criar Aditivo</a></li>
                  <li class="divider"></li>
                  @forelse($aditivos as $aditivo)
                  <li><a href="{{route('tce.aditivo.show', [$tce->id, $aditivo->id])}}">{{$aditivo->nnAditivo}}° Aditivo</a></li>
                  @empty
                  <li><strong style="padding:20px;">Sem Aditivo</strong></li>
                  @endforelse
                </ul>
              </div>
              <!--<a href="{{route('tce.aditivo.create', $tce->id)}}" class="btn btn-primary ad-click-event">Criar Aditivo</a>-->
              <a href="{{route('tce.edit', $tce->id)}}" class="btn btn-warning ad-click-event">Editar TCE</a>
              <a href="{{route('tce.cancelar.form', $tce->id)}}" class="btn btn-danger ad-click-event" onclick="return confirm('Deseja Realmente Cancelar o TCE {{$tce->id}} ?');">Cancelar TCE</a>
              @else
              <a href="{{route('tce.recisao.show', $tce->id)}}" target="_blank" class="btn btn-primary ad-click-event">Ver Recisão de TCE</a>
              <a href="{{route('tce.declaracao.show', $tce->id)}}" target="_blank" class="btn btn-default ad-click-event">Ver Declaração de Estágio</a>
              @endif
            </div>
            <br>
            <!--ADITIVOS-->
            @if ($aditivos->count())
            <legend>Aditivos</legend>
            <table class="table table-striped">
              <tbody>
                <tr>
                  <th style="width: 22px;">Aditivo</th>
                  <th style="width: 90px;">Data Inicio</th>
                  <th style="width: 90px;">Data Fim</th>
                  <th>Outro</th>
                </tr>
                @foreach($aditivos as $aditivo)

                <tr>
                  <td>{{$aditivo->nnAditivo}} </td>
                  <td>
                    @if ($aditivo->dtInicio === NULL)
                    -
                    @else
                    {{$aditivo->dtInicio->format('d/m/Y')}}
                    @endif
                  </td>
                  <td>
                    @if ($aditivo->dtFim === NULL)
                    -
                    @else
                    {{$aditivo->dtFim->format('d/m/Y')}}
                    @endif
                  </td>
                  <td>
                    @if (($aditivo->aditivoOutro === NULL) OR ($aditivo->aditivoOutro === ""))
                    -
                    @else
                    {{$aditivo->aditivoOutro}}
                    @endif
                  </td>
                </tr>
                @endforeach

              </tbody>
            </table>
            @endif
            <!-- ADITIVO FIM -->

            <!-- /.box-body -->


          </form>
        </div>

      </div>
    </div>

    @section('post-script')
    <script>
    jQuery(function($){
      $("#dtInicio").mask("99/99/9999");
      $("#dtFim").mask("99/99/9999");
      $("#hrInicial").mask("99:99");
      $("#hrFinal").mask("99:99");
    });
  </script>

  @endsection
  @stop
