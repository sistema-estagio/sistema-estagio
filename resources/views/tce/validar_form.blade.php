@extends('adminlte::page')
@if(!empty($titulo))
@section('title_postfix', ' - '.$titulo)
@endif
@section('content_header')

    <h1>Validar Tce</h1>
    <ol class="breadcrumb">
        {{--  {{route('estudante.index')}}  --}}
        <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('tce.index')}}"><i class="fa fa-files-o"></i>TCE</a></li>
        <li class="active"><i class="fa fa-check-square-o"></i> Validação de TCE</li>
    </ol>
@stop

@section('content')
    @if(session('success'))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> Alerta!</h4>
            {{session('success')}}
    </div>
    @endif

    @if(session('error'))
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Alerta!</h4>
            {{session('error')}}
    </div>
    @endif

    @if($errors->any())
        <ul class="alert-danger">
            <h3>Whoops, Houve algum erro!</h3>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    <div class="row">
        <div class="col-md-12">
            <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title"><strong>TCE: </strong>{{$tce->id}} - <strong>Estudante: </strong>{{$tce->estudante->nmEstudante}} - <strong>CPF: </strong>{{$tce->estudante->cdCPF}}</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            @if($tce->dt4Via === NULL)
                <form action="{{route('tce.validando', ['tce' => $tce->id])}}" method="POST">
                {!! csrf_field() !!}
                {!! method_field('PUT') !!}

                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="dt4Via">*Data Devolução</label>
                                    <input class="form-control input-lg" id="dt4Via" name="dt4Via"  type="text" placeholder="dd/mm/aaaa" value="{{ \Carbon\Carbon::now()->format('d/m/Y')}}" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer text-center">
                        <button type="submit" class="btn btn-success btn-lg" onclick="return confirm('Você confirma a validação do Tce {{$tce->id}} na data informada acima? ');">Validar</button>
                    </div>
                </form>
            @else
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="callout callout-danger">
                                <h4><i class="icon fa fa-warning"></i> TCE já Validado</h4>
                                <p>Esse TCE foi Validado em <strong>{{$tce->dt4Via_at->format('d/m/Y H:i:s')}}</strong> com data de validação <strong>{{$tce->dt4Via->format('d/m/Y')}}</strong> por <strong>{{$tce->user4Via->name}}</strong>!</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <a class="btn btn-primary btn-lg" href="{{route('tce.validar')}}" role="button">Buscar Outro</a>
                    </div>
                </div>
            </div>
            @endif
          </div>

        </div>
    </div>
@section('post-script')
<script>
    jQuery(function($){
        $("#dt4Via").mask("99/99/9999");
    });
</script>

@endsection
@stop
