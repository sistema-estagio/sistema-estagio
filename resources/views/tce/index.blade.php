@extends('adminlte::page')
@if(!empty($titulo))
@section('title_postfix', ' - '.$titulo)
@endif
@section('content_header')
@if(isset($tcesCancelados))
<h1>Tces Cancelados</h1>
@else
<h1>Tces</h1>
@endif
<ol class="breadcrumb">
	<li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
	@if(isset($tcesCancelados))
	<li class="active"><a href="{{route('tce.index.cancelados')}}"><i class="fa fa-files-o"></i>TCES Cancelados</a></li>
	@else
	<li class="active"><a href="{{route('tce.index')}}"><i class="fa fa-files-o"></i>TCES</a></li>
	@endif

</ol>
@stop
@section('content')
@if(session('success'))
<div class="alert alert-success alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h4>
		<i class="icon fa fa-check"></i> Alerta!</h4>
		{{session('success')}}
	</div>
	@endif
	@if(session('error'))
	<div class="alert alert-danger alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<h4>
			<i class="icon fa fa-ban"></i> Alerta!</h4>
			{{session('error')}}
		</div>
		@endif

		<br>
		@if(isset($tces))
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-body">

						<form action="{{route('tce.search')}}" method="POST">
							{!! csrf_field() !!}
							<div class="input-group input-group-sm" style="width: 600px;">
								<div class="col-md-4">
									<div class="form-group">
										<label for="tipo">*Tipo:</label>
										<div class="input-group input-group-sm" style="width: 200px;">
											<select class="form-control pull-right" name="tipo" id="tipo">
												<option value="migracao">Tce Antigo</option>
												<option value="tce" >Tce</option>
												<option value="estudante" selected>Estudante</option>
												<option value="concedente">Concedente</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-5">
									<div class="form-group">
										<label for="key_search">*Buscar por:</label>
										<div class="input-group input-group-sm" style="width: 300px;">
											<input name="key_search" class="form-control pull-right" placeholder="Pesquisar" type="text">
											<div class="input-group-btn">
												<button type="submit" class="btn btn-primary">
													<i class="fa fa-search"></i> Buscar
												</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Listagem de Tces</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<table id="tces" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th style="width: 40px">MIGRAÇÃO</th>
									<th style="width: 40px">TCE</th>
									<th>ESTUDANTE</th>
									<th>CONCEDENTE</th>
									<th class="no-sort">INICIO VIGÊNCIA</th>
									<th class="no-sort">FIM VIGÊNCIA</th>
									{{--  <th>Cadastro</th>
										<th>TCE</th>
										<th>Ações</th>  --}}
									</tr>
								</thead>
								<tbody>
									@forelse($tces as $tce)
									<tr>
										<td>
											@if($tce->migracao == NULL)
											-
											@else
											{{$tce->migracao}}
											@endif
										</td>
										@if($tce->aditivos->count() > 0)
										<td>{{$tce->id}}/{{$tce->aditivos->count()}}</td>
										@else
										<td>{{$tce->id}}</td>
										@endif
										{{--CONTANDO APENAS ADITIVOS DE DATAS
											@if($tce->aditivos->count() > 0)
											@if($tce->aditivos()->latest()->first()->dtInicio != Null)
											<td>{{$tce->id}}/{{$tce->aditivos()->latest()->first()->nnAditivo}}</td>
											@else
											<td>{{$tce->id}}</td>
											@endif
											@else
											<td>{{$tce->id}}</td>
											@endif  --}}
											<!--<td>{{$tce->id}}</td>-->
											<td>
												<a href="{{route('tce.show', ['tce' => $tce->id])}}">{{$tce->estudante->nmEstudante}}</a>
											</td>
											<td>{{$tce->concedente->nmRazaoSocial}}</td>
											<td>{{$tce->dtInicio->format('d/m/Y')}}</td>
											<td>
												@if($tce->aditivos->count() > 0)
												@if($tce->aditivos()->latest()->first()->dtInicio != Null)
												{{$tce->aditivos()->latest()->first()->dtFim->format('d/m/Y')}}
												@else
												{{$tce->dtFim->format('d/m/Y')}}
												@endif
												@else
												{{$tce->dtFim->format('d/m/Y')}}
												@endif
											</td>
											<!-- ->route('estudante.tce.show', ['estudante' => $formulario['estudante_id'],'tce' => $new->id]); -->
										</tr>
										@empty
										<tr>
											<td colspan="6" class="text-center">
												<span class="badge bg-red">Não existe registros no banco!</span>
											</td>
										</tr>
										@endforelse

									</tbody>
								</table>
							</div>
							<div class="box-footer clearfix">

							</div>
							<!-- /.box-body -->
							{{--
								<div class="box-footer clearfix">
									@if(isset($dataForm)) {!! $tce->appends($dataForm)->links() !!}
									@else {!! $tces->links() !!}
									@endif
								</div>--}}
							</div>
							<!-- /.box -->
						</div>
						@endif

						@if(isset($tcesCancelados))
						<!-- Parte listagem tce cancelados-->
						<div class="row">
							<div class="box">
								<div class="box-body">
									<div class="col-md-9">
										<form action="{{route('tce.search.cancelados')}}" method="POST">
											{!! csrf_field() !!}
											<div class="input-group input-group-sm" style="width: 600px;">
												<div class="col-md-4">
													<div class="form-group">
														<label for="tipo">*Tipo:</label>
														<div class="input-group input-group-sm" style="width: 200px;">
															<select class="form-control pull-right" name="tipo" id="tipo">
																{{--  <option value="" >Selecione...</option>  --}}
																<option value="migracao">Tce Antigo</option>
																<option value="tce" >Tce</option>
																<option value="estudante" selected>Estudante</option>
																<option value="concedente">Concedente</option>
															</select>
														</div>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label for="key_search">*Buscar por:</label>
														<div class="input-group input-group-sm" style="width: 300px;">
															<input name="key_search" class="form-control pull-right" placeholder="Pesquisar" type="text">
															<div class="input-group-btn">
																<button type="submit" class="btn btn-primary">
																	<i class="fa fa-search"></i> Buscar
																</button>
															</div>
														</div>
													</div>
												</div>
											</div>
										</form>
									</div>
									<div class="col-md-3">
										<button type="button" role="button" class="btn bg-gray btn-flat btn-lg pull-right" data-toggle="modal"  data-target="#modelModelo" style="margin-top:15px;"><i class="fa fa-file-pdf-o"></i> Relatório</button>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<div class="box">
									<div class="box-header with-border">
										<h3 class="box-title">Listagem de Tces Cancelados</h3>
									</div>
									<!-- /.box-header -->
									<div class="box-body">
										<table class="table table-bordered">
											<tbody>
												<tr>
													<th style="width: 40px">MIGRAÇÃO</th>
													<th style="width: 40px">TCE</th>
													<th>Estudante</th>
													<th>Concedente</th>
													<th>Cancelado em por</th>
													{{-- <th>TCE</th>
														<th>Ações</th>  --}}
													</tr>
													@forelse($tcesCancelados as $tce)
													<tr>
														<td>{{$tce->migracao}}</td>
														<td>{{$tce->id}}</td>
														<td>
															<a href="{{route('tce.show', ['tce' => $tce->id])}}">{{$tce->estudante->nmEstudante}}</a>
														</td>
														<td>{{$tce->concedente->nmRazaoSocial}}</td>
														<td>{{$tce->dtCancelamento->format('d/m/Y')}}
															@if(isset($tce->userCancelamento->name))
															{{$tce->userCancelamento->name}}
															@else
															Própria Concedente
															@endif

														</td>
														<!-- ->route('estudante.tce.show', ['estudante' => $formulario['estudante_id'],'tce' => $new->id]); -->
													</tr>
													@empty
													<tr>
														<td colspan="3" align="center">
															<span class="badge bg-red">Não existe registros no banco!</span>
														</td>
													</tr>
													@endforelse

												</tbody>
											</table>
										</div>
										<div class="box-footer clearfix">

										</div>
										<!-- /.box-body -->
										<div class="box-footer clearfix">
											@if(isset($dataForm)) {!! $tceCancelados->appends($dataForm)->links() !!}
											@else {!! $tcesCancelados->links() !!}
											@endif
										</div>
									</div>
									<!-- /.box -->
								</div>
								<!-- Fim Parte listagem tce cancelados-->

								<!-- Modal de Relatório -->
								<div class="modal" tabindex="-1" role="dialog" id="modelModelo">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title">Relatório TCEs Cancelados</h5>
											</div>
											<form action="#" method="post" id="form-relatorio-post">
												{!! csrf_field() !!}
												<div class="modal-body">
													<div class="form-group row">
														<div class="col-lg-6">
															<label for="selOrdenacao">Data Início</label><br>
															<input type="text" class="form-control dataMask dates" name="dataInicial" placeholder="01/01/2018" id="dataInicial">
														</div>
														<div class="col-lg-6">
															<label for="selOrdenacao">Data Fim</label><br>
															<input type="text" class="form-control dataMask dates" name="dataFinal" placeholder="31/01/2018" id="dataFinal">
															<small class="badge badge-danger" id="dataAlert" style="background-color:red;"></small>
														</div>
													</div>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
													<button type="submit" id="relatorioSubmit" class="btn btn-primary">Confirmar</button>
												</div>
											</form>
										</div>
									</div>
								</div>
								@endif
								@section('post-script')
								<script>
								@if(isset($tcesCancelados))
								$('#relatorioSubmit').submit(function(){
									let dataInicial = $('#dataInicial').val();
									let dataFinal = $('#dataFinal').val();
									if(dataInicial != "" && dataFinal != "") {
										if(dataInicial <= dataFinal) {
											$('#dataAlert').hide();
											@if($secretaria != null)
											$.post("{{ route('concedente.secretaria.tces.cancelados.pdf', ['idConcedente'=>$concedente->id,'id'=>$secretaria->id]) }}", { "_token": "{{ csrf_token() }}", "dataInicial": dataInicial, "dataFinal":dataFinal },function(response){
											@else
											$.post("{{ route('concedente.tces.cancelados.pdf', ['idConcedente'=>$concedente->id]) }}", { "_token": "{{ csrf_token() }}", "dataInicial": dataInicial, "dataFinal":dataFinal },function(response){
											@endif
												if(response != null) {
													if(response.result == false) {
														toastr.warning('Não foi possível adicionar o lote feito');
													}
												}
											});
										} else {
											$('#dataAlert').html("Coloque a data final maior que a inicial");
										}
									} else {

									}
									return false;
								});
								@endif
								$(function () {
									$('#tces').DataTable({
										"columnDefs": [
											{ "orderable": false, "targets": 'no-sort' }
										],
										"pageLength": 25,
										"order": [[ 1, "desc" ]],
										"language": {
											"sEmptyTable": "Nenhum registro encontrado",
											"sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
											"sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
											"sInfoFiltered": "(Filtrados de _MAX_ registros)",
											"sInfoPostFix": "",
											"sInfoThousands": ".",
											"sLengthMenu": "_MENU_ resultados por página",
											"sLoadingRecords": "Carregando...",
											"sProcessing": "Processando...",
											"sZeroRecords": "Nenhum registro encontrado",
											"sSearch": "Pesquisar",
											"oPaginate": {
												"sNext": "Próximo",
												"sPrevious": "Anterior",
												"sFirst": "Primeiro",
												"sLast": "Último"
											},
											"oAria": {
												"sSortAscending": ": Ordenar colunas de forma ascendente",
												"sSortDescending": ": Ordenar colunas de forma descendente"
											}
										}
									})

								});

								$('.dataMask').mask('00/00/0000')
							</script>
							@endsection
							@stop
