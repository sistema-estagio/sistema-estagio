@extends('adminlte::page')

@section('title_postfix', ' - '.$titulo)

@section('content_header')
<h1>Dashboard</h1>
@stop

@section('css')
<style media="screen">
  .cardsStyles a {
    color: black;
  }
  
  .cardsStyles:hover {
    color: black;
    text-decoration: none;
  }
</style>
@endsection
@section('content')
<p>Você está Logado!</p>

<div class="row" id="secUm">
  <div class="col-md-3 col-sm-6 col-xs-12 cardsStyles" id="card-instituicoes">
    <a href="javascript:void(0);" role="button" data-toggle="tooltip" data-placement="bottom" title="Clique para ver mais informações">
      <div class="info-box">
        <span class="info-box-icon bg-aqua"><i class="fa fa-institution"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Instituições</span>
          <span class="info-box-number">{{$todasInstituicoes->count()}}</span>
          <span>Ver mais</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </a>
  </div>
  <!-- /.col -->
  
  <div class="col-md-3 col-sm-6 col-xs-12 cardsStyles" id="card-estudantes">
    <a href="javascript:void(0);" role="button" data-toggle="tooltip" data-placement="bottom" title="Clique para ver mais informações">
      <div class="info-box">
        <span class="info-box-icon bg-yellow"><i class="fa fa-users"></i></span>
        
        <div class="info-box-content">
          <span class="info-box-text">Estudantes</span>
          <span class="info-box-number">{{$todosEstudantes->count()}}</span>
          <span>Ver mais</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </a>
  </div>
  <!-- /.col -->
  
  <div class="col-md-3 col-sm-6 col-xs-12 cardsStyles" id="card-concedentes">
    <a href="javascript:void(0);" role="button" data-toggle="tooltip" data-placement="bottom" title="Clique para ver mais informações">
      <div class="info-box">
        <span class="info-box-icon bg-red"><i class="fa fa-industry"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Concedentes</span>
          <span class="info-box-number">{{$todosConcedentes->count()}}</span>
          <span>Ver mais</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </a>
  </div>
  <!-- /.col -->
  
  <!-- fix for small devices only -->
  <div class="clearfix visible-sm-block"></div>
  
  <div class="col-md-3 col-sm-6 col-xs-12 cardsStyles" id="card-tces">
    <a href="javascript:void(0);" role="button" data-toggle="tooltip" data-placement="bottom" title="Clique para ver mais informações">
      <div class="info-box">
        <span class="info-box-icon bg-green"><i class="fa fa-file-text-o"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">TCES</span>
          <span class="info-box-number">{{$todosTces->count()}}</span>
          <span>Ver mais</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </a>
  </div>
  <!-- /.col -->
</div>

<div class="secDois">
  <div class="row" id="card-more-tces" style="display:none;">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h4 class="box-title">TCES</h4>
        </div>
        <div class="box-body">
          <div class="col-md-4">
            <a href="javascript:void(0);" role="button" id="card-tces-pendentes">
              <div class="info-box bg-aqua">
                <span class="info-box-icon"><i class="fa fa-file-text-o"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Pendentes</span>
                  <span class="info-box-number">{{$todosTces->where('dt4Via', null)->where('dtCancelamento', null)->count()}}</span>
                </div>
                <!-- /.info-box-content -->
              </div>
            </a>
          </div>
          
          <div class="col-md-4">
            <a href="javascript:void(0);" role="button" id="card-tces-validados">
              <div class="info-box bg-gray">
                <span class="info-box-icon"><i class="fa fa-file-text-o"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Validados no Mês</span>
                  <span class="info-box-number">{{$todosTces->where('dt4Via_at', \Carbon\Carbon::now()->format('Y-m'))->count()}}</span>
                </div>
                <!-- /.info-box-content -->
              </div>
            </a>
          </div>
          
          <div class="col-md-4">
            <a href="javascript:void(0);" role="button" id="card-tces-ativos">
              <div class="info-box bg-green">
                <span class="info-box-icon"><i class="fa fa-file-text-o"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Ativos</span>
                  <span class="info-box-number">{{$todosTces->where('dt4Via', '!=',null)->where('dtCancelamento', null)->count()}}</span>
                </div>
                <!-- /.info-box-content -->
              </div>
            </a>
          </div>
          
          <div class="col-md-4">
            <a href="javascript:void(0);" role="button" id="card-tces-cancelados">
              <div class="info-box bg-red">
                <span class="info-box-icon"><i class="fa fa-file-text-o"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Cancelados</span>
                  <span class="info-box-number">{{$todosTces->where('dtCancelamento', '!=', null)->count()}}</span>
                </div>
                <!-- /.info-box-content -->
              </div>
            </a>
          </div>
          
          <div class="col-md-4">
            <a href="javascript:void(0);" role="button" id="card-tces-proxVencidos">
              <div class="info-box bg-yellow">
                <span class="info-box-icon"><i class="fa fa-file-text-o"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Próximo do Vencimento</span>
                  <span class="info-box-number">{{$todosTces->where('dt4Via', '!=',null)->where('dtCancelamento', null)->where('dtFim', '<=', \Carbon\Carbon::now()->format('Y-m'))->count()}}</span>
                </div>
                <!-- /.info-box-content -->
              </div>
            </a>
          </div>
          
          <div class="col-md-4">
            <a href="javascript:void(0);" role="button" id="card-tces-vencidos">
              <div class="info-box bg-black">
                <span class="info-box-icon"><i class="fa fa-file-text-o"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Vencidos</span>
                  <span class="info-box-number">{{$todosTces->where('dt4Via', '!=',null)->where('dtCancelamento', null)->where('dtFim', '>=', \Carbon\Carbon::now()->format('Y-m'))->count()}}</span>
                </div>
                <!-- /.info-box-content -->
              </div>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <div class="row" id="card-more-concedentes" style="display:none;">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h4 class="box-title">Concedentes</h4>
        </div>
        <div class="box-body">
          <div class="col-md-4">
            <a href="javascript:void(0);" role="button" id="card-tces-pendentes">
              <div class="info-box bg-aqua">
                <span class="info-box-icon"><i class="fa fa-file-text-o"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Secretarias</span>
                  <span class="info-box-number">{{$todasSecretarias->count()}}</span>
                </div>
                <!-- /.info-box-content -->
              </div>
            </a>
          </div>
          
          <div class="col-md-4">
            <div class="info-box bg-gray">
              <span class="info-box-icon"><i class="fa fa-file-text-o"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Folhas geradas</span>
                <span class="info-box-number">{{$todosTces->where('dt4Via_at', \Carbon\Carbon::now()->format('Y-m'))->count()}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
          </div>
          
          <div class="col-md-4">
            <a href="javascript:void(0);" role="button" id="card-tces-ativos">
              <div class="info-box bg-green">
                <span class="info-box-icon"><i class="fa fa-file-text-o"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Usuários das concedentes</span>
                  <span class="info-box-number">{{$todosUsersConcedente->count()}}</span>
                </div>
                <!-- /.info-box-content -->
              </div>
            </a>
          </div>
          
          <div class="col-md-4">
            <a href="javascript:void(0);" role="button" id="card-tces-cancelados">
              <div class="info-box bg-red">
                <span class="info-box-icon"><i class="fa fa-file-text-o"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Supervisores</span>
                  <span class="info-box-number">{{$todosSupervisores->count()}}</span>
                </div>
                <!-- /.info-box-content -->
              </div>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <div class="row" id="card-more-estudantes" style="display:none;">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h4 class="box-title">Estudantes</h4>
        </div>
        <div class="box-body">
          <div class="col-md-4">
            <a href="javascript:void(0);" role="button" id="card-tces-pendentes">
              <div class="info-box bg-aqua">
                <span class="info-box-icon"><i class="fa fa-file-text-o"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Alunos com estágio</span>
                  <span class="info-box-number">{{$todosTces->where('dt4Via', null)->where('dtCancelamento', null)->count()}}</span>
                </div>
                <!-- /.info-box-content -->
              </div>
            </a>
          </div>
          
          <div class="col-md-4">
            <div class="info-box bg-gray">
              <span class="info-box-icon"><i class="fa fa-file-text-o"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Alunos sem estágio</span>
                <span class="info-box-number">{{$todosTces->where('dt4Via_at', \Carbon\Carbon::now()->format('Y-m'))->count()}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
          </div>
          
          <div class="col-md-4">
            <a href="javascript:void(0);" role="button" id="card-tces-ativos">
              <div class="info-box bg-green">
                <span class="info-box-icon"><i class="fa fa-file-text-o"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Estagiarios com acesso</span>
                  <span class="info-box-number">{{$todosTces->where('dt4Via', '!=',null)->where('dtCancelamento', null)->count()}}</span>
                </div>
                <!-- /.info-box-content -->
              </div>
            </a>
          </div>
          
          <div class="col-md-4">
            <a href="javascript:void(0);" role="button" id="card-tces-cancelados">
              <div class="info-box bg-red">
                <span class="info-box-icon"><i class="fa fa-file-text-o"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Estagiarios sem acesso</span>
                  <span class="info-box-number">{{$todosTces->where('dtCancelamento', '!=', null)->count()}}</span>
                </div>
                <!-- /.info-box-content -->
              </div>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <div class="row" id="card-more-instituicoes" style="display:none;">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h4 class="box-title">Instituições</h4>
        </div>
        <div class="box-body">
          <div class="col-md-4">
            <a href="javascript:void(0);" role="button" id="card-tces-pendentes">
              <div class="info-box bg-aqua">
                <span class="info-box-icon"><i class="fa fa-file-text-o"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Secretarias</span>
                  <span class="info-box-number">{{$todasSecretarias->count()}}</span>
                </div>
                <!-- /.info-box-content -->
              </div>
            </a>
          </div>
          
          <div class="col-md-4">
            <div class="info-box bg-gray">
              <span class="info-box-icon"><i class="fa fa-file-text-o"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Folhas geradas</span>
                <span class="info-box-number">{{$todosTces->where('dt4Via_at', \Carbon\Carbon::now()->format('Y-m'))->count()}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
          </div>
          
          <div class="col-md-4">
            <a href="javascript:void(0);" role="button" id="card-tces-ativos">
              <div class="info-box bg-green">
                <span class="info-box-icon"><i class="fa fa-file-text-o"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Usuários das concedentes</span>
                  <span class="info-box-number">{{$todosTces->where('dt4Via', '!=',null)->where('dtCancelamento', null)->count()}}</span>
                </div>
                <!-- /.info-box-content -->
              </div>
            </a>
          </div>
          
          <div class="col-md-4">
            <a href="javascript:void(0);" role="button" id="card-tces-cancelados">
              <div class="info-box bg-red">
                <span class="info-box-icon"><i class="fa fa-file-text-o"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Supervisores</span>
                  <span class="info-box-number">{{$todosTces->where('dtCancelamento', '!=', null)->count()}}</span>
                </div>
                <!-- /.info-box-content -->
              </div>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="secTres">
  <div class="row" id="card-tables-tces" style="display:none;">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h4 class="box-title">Lista de TCES</h4>
        </div>
        <div class="box-body">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-striped tables-cards" id="table-tces-pendentes" style="display:none;">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>ESTUDANTE</th>
                    <th>CONCEDENTE/SECRETARIA</th>
                    <th>CANCELADO EM</th>
                    <th>POR</th>
                    <th>MOTIVO</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td colspan="6"><p class="text-center">Todos os TCE's pendentes</p></td>
                  </tr>
                </tbody>
              </table>
              <!-- -->
              <table class="table table-striped tables-cards" id="table-tces-ativos" style="display:none;">
                <thead>
                  <tr>
                    <th>TCE</th>
                    <th>ESTUDANTE</th>
                    <th>CONCEDENTE/SECRETARIA</th>
                    <th>DATA INICIAL</th>
                    <th>DATA FINAL</th>
                  </tr>
                </thead>
                <tbody>
                  
                </tbody>
              </table>
              <!-- -->
              <table class="table table-striped tables-cards" id="table-tces-cancelados" style="display:none;">
                <thead>
                  <tr>
                    <th>TCE</th>
                    <th>ESTUDANTE</th>
                    <th>CONCEDENTE/SECRETARIA</th>
                    <th>CANCELADO EM</th>
                    <th>POR</th>
                    <th>MOTIVO</th>
                  </tr>
                </thead>
                <tbody>
                  
                </tbody>
              </table>
              <!-- -->
              <table class="table table-striped tables-cards" id="table-tces-vencidos" style="display:none;">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>ESTUDANTE</th>
                    <th>CONCEDENTE/SECRETARIA</th>
                    <th>CANCELADO EM</th>
                    <th>POR</th>
                    <th>MOTIVO</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td colspan="6"><p class="text-center">Todos os TCE's vencidos</p></td>
                  </tr>
                </tbody>
              </table>
              <!-- -->
              <table class="table table-striped tables-cards" id="table-tces-proxVencidos" style="display:none;">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>ESTUDANTE</th>
                    <th>CONCEDENTE/SECRETARIA</th>
                    <th>CANCELADO EM</th>
                    <th>POR</th>
                    <th>MOTIVO</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td colspan="6"><p class="text-center">Todos os TCE's próximos do vencimento</p></td>
                  </tr>
                </tbody>
              </table>
              <!-- -->
              <table class="table table-striped tables-cards" id="table-tces-validados" style="display:none;">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>ESTUDANTE</th>
                    <th>CONCEDENTE/SECRETARIA</th>
                    <th>CANCELADO EM</th>
                    <th>POR</th>
                    <th>MOTIVO</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td colspan="6"><p class="text-center">Todos os TCE's validados no mês</p></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!--ULTIMOS ESTUDANTES-->
<div class="row">
  <div class="col-lg-6 col-sm-12">
    <div class="box box-warning">
      <div class="box-header with-border">
        <h3 class="box-title">Últimos Estudantes</h3>
        <!--<div class="box-tools">
          <div class="input-group input-group-sm" style="width: 200px;">
            <input name="key_search" class="form-control pull-right" placeholder="Pesquisar" type="text">
            
            <div class="input-group-btn">
              <button type="submit" class="btn btn-default">
                <i class="fa fa-search"></i>
              </button>
            </div>
          </div>
        </form>
      </div> -->
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <div class="table-responsive">
        <table class="table table-bordered" style="font-size:12px !important;">
          <tbody>
            <tr>
              <!--<th style="width: 40px">Cadastro</th>-->
              <th style="width:27%;">ESTUDANTE</th>
              <th style="width:33%;">INSTITUIÇÃO DE ENSINO</th>
              <th style="width:25%;">CURSO</th>
              <th style="width:15%;">CADASTRADO EM</th>
            </tr>
            @forelse($estudantes as $estudante)
            <tr>
              <td><a href="{{route('estudante.show', ['estudante' => $estudante->id])}}">{{$estudante->nmEstudante}}</a>
                {{--  <a href="{{route('instituicao.show', ['instituicao' => $instituicao->id])}}">{{$instituicao->nmInstituicao}}</a>  --}}
              </td>
              <td>{{$estudante->instituicao->nmInstituicao}}</td>
              <td>{{$estudante->cursoDaInstituicao->curso->nmCurso}}</td>
              <td>{{$estudante->created_at->format("d/m/Y h:m")}}</td>
            </tr>
            @empty
            <tr>
              <td colspan="4" align="center">
                <span class="badge bg-red">Sem registros no banco de dados!</span>
              </td>
            </tr>
            
            @endforelse
          </tbody>
        </table>
      </div>
    </div>
    <div class="box-footer text-center">
      <a href="{{route('estudante.index')}}" class="uppercase">Ver todos Estudantes</a>
    </div>
    <!-- /.box-body -->
    
  </div>
  <!-- /.box -->
</div>
<div class="col-lg-6 col-sm-12">
  <div class="box box-danger">
    <div class="box-header with-border">
      <h3 class="box-title">Últimos TCE'S Cadastrados</h3>
      <!--<div class="box-tools">
        <div class="input-group input-group-sm" style="width: 200px;">
          <input name="key_search" class="form-control pull-right" placeholder="Pesquisar" type="text">
          
          <div class="input-group-btn">
            <button type="submit" class="btn btn-default">
              <i class="fa fa-search"></i>
            </button>
          </div>
        </div>
      </form>
    </div>-->
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <div class="table-responsive">
      <table class="table table-bordered" style="font-size:12px !important;">
        <tbody>
          <tr>
            <th style="width: 5%;">#</th>
            <th style="width: 27%;">ESTUDANTE</th>
            <!--<th style="width: 40px">Cadastro</th>-->
            <th style="width: 15%;">VIGÊNCIA INICIAL</th>
            <th style="width: 15%;">VIGÊNCIA FINAL</th>
            <th style="width: 28%;">CONCEDENTE</th>
          </tr>
          @forelse($tces as $tce)
          <tr>
            @if($tce->aditivos->count() > 0)
            <td>{{$tce->id}}/{{$tce->aditivos->count()}}</td>
            @else
            <td>{{$tce->id}}</td>
            @endif
            <td>{{$tce->estudante->nmEstudante}}
              {{--  <a href="{{route('instituicao.show', ['instituicao' => $instituicao->id])}}">{{$instituicao->nmInstituicao}}</a>  --}}
            </td>
            <td>{{$tce->dtInicio->format("d/m/Y")}}</td>
            <td>
              @if($tce->aditivos->count() > 0)
              @if($tce->aditivos()->latest()->first()->dtInicio != Null)
              {{$tce->aditivos()->latest()->first()->dtFim->format('d/m/Y')}}
              @else
              {{$tce->dtFim->format('d/m/Y')}}
              @endif
              @else
              {{--  {{date('d/m/Y',strtotime($tce->dtFim))}}  --}}
              {{$tce->dtFim->format('d/m/Y')}}
              @endif
            </td>
            <!--<td>{{$tce->dtFim->format("d/m/Y")}}</td>-->
            <td>{{$tce->concedente->nmRazaoSocial}}</td>
          </tr>
          @empty
          <tr>
            <td colspan="5" align="center">
              <span class="badge bg-red">Sem registros no banco de dados!</span>
            </td>
          </tr>
          
          @endforelse
        </tbody>
      </table>
    </div>
  </div>
  <div class="box-footer text-center">
    <a href="{{route('tce.index')}}" class="uppercase">Ver todos Tces</a>
  </div>
  
  <!-- /.box-body -->
  
</div>
<!-- /.box -->
</div>
</div>
<!--FIM ULTIMOS ESTUDANTES-->

<!--ULTIMOS TCES-->
<div class="row">
  
</div>
<!--FIM ULTIMOS TCES-->
@stop
@section('js')
<script type="text/javascript">
  
  $(document).ready(function(){
    let rowContainer = document.getElementById('secUm');
    let cards = rowContainer.getElementsByClassName('cardsStyles');
    
    for (var i = 0; i < cards.length; i++) {
      cards[i].addEventListener("click", function() {
        var current = document.getElementsByClassName("active");
        if (current.length > 0) { 
          current[0].className = current[0].className.replace(" active", "");
        }
        this.className += " active";
        
        if($("#card-instituicoes").hasClass("active")){
          if($("#card-more-instituicoes").css('display') == 'none'){
            $("#card-more-instituicoes").fadeIn(500).show();
          } else{
            $("#card-more-instituicoes").fadeOut(1000).hide(); 
          }
        } else {
          $("#card-more-instituicoes").fadeOut(1000).hide();
        }
        
        if($("#card-estudantes").hasClass("active")) {
          if($("#card-more-estudantes").css('display') == 'none'){
            $("#card-more-estudantes").fadeIn(500).show();
          } else{
            $("#card-more-estudantes").fadeOut(1000).hide();
          }
        } else {
          $("#card-more-estudantes").fadeOut(1000).hide();
        }
        
        if($("#card-concedentes").hasClass("active")) {
          if($("#card-more-concedentes").css('display') == 'none'){
            $("#card-more-concedentes").fadeIn(500).show();
          } else{
            $("#card-more-concedentes").fadeOut(1000).hide();
          }
        } else{
          $("#card-more-concedentes").fadeOut(1000).hide();
        }
        
        if($("#card-tces").hasClass("active")) {
          if($("#card-more-tces").css('display') == 'none'){
            $("#card-more-tces").fadeIn(500).show();
          } else{
            $("#card-more-tces").fadeOut(1000).hide();
          }
        } else{
          $("#card-more-tces").fadeOut(1000).hide();
        }
        
        
        // TCES
        
        $("#card-tces-validados").click(function(){
          $(".tables-cards").hide();
          $("#card-tables-tces").show();
          if($("#table-tces-validados").css('display') == 'none'){
            $("#table-tces-validados").fadeIn(1000).show();
          }else{
            $("#table-tces-validados").fadeOut(1000).hide();
            $("#card-tables-tces").fadeOut(1000).hide();
          }
        });
        
        $("#card-tces-pendentes").click(function(){
          $(".tables-cards").hide();
          $("#card-tables-tces").show();
          if($("#table-tces-pendentes").css('display') == 'none'){
            $("#table-tces-pendentes").fadeIn(1000).show();
            $.ajax({
                url: 'home/get-tces',
                type: 'GET',
                data: { tipo: 'pendentes' },
                success: function(tces)
                {
                  if(tces == null) {
                    $('#table-tces-pendentes').append(
                      "<tr><td colspan='6'><p class='text-center'>Todos os TCE's pendentes</p></td></tr>"
                    );
                  } else {
                    tces.forEach(tce => {
                      
                      tce.dtCancelamento = getFormattedDate(new Date(tce.dtCancelamento));
                      
                      let secOrCon;
                      let numTce;

                      if(tce.nmSecretaria) secOrCon = tce.nmSecretaria;
                      else secOrCon = tce.nmFantasia;

                      if (tce.migracao != null) numTce = tce.migracao;
                      else numTce = tce.id;

                      let url = '{{route("tce.show", ["id" => ":id"])}}';
                      url = url.replace(':id', tce.id);

                      $('#table-tces-pendentes').append(
                      '<tr><td>'+ numTce +
                      '</td><td><a href="'+ url +'">' + tce.nmEstudante +
                      '</td><td>'+ secOrCon +
                      '</td><td>'+ tce.dtCancelamento +
                      '</td><td>'+ tce.dtFim +
                      '</td><td>'+ tce.dtFim +
                      '</td></tr>'
                      );
                    });
                  }
                }
            });
          }else{
            $("#table-tces-pendentes").fadeOut(1000).hide();
            $("#card-tables-tces").fadeOut(1000).hide();
          }
        });
        
        $("#card-tces-ativos").click(function(){
          $(".tables-cards").hide();
          $("#card-tables-tces").show();
          if($("#table-tces-ativos").css('display') == 'none'){
            $("#table-tces-ativos").fadeIn(1000).show();

            $.ajax({
                url: 'home/get-tces',
                type: 'GET',
                data: { tipo: 'ativos' },
                success: function(tces)
                {
                  if(tces == null) {
                    $('#table-tces-ativos').append(
                      "<tr><td colspan='6'><p class='text-center'>Todos os TCE's ativos</p></td></tr>"
                    );
                  } else {
                    tces.forEach(tce => {
                      
                      tce.dtCancelamento = getFormattedDate(new Date(tce.dtCancelamento));
                      
                      let secOrCon;
                      let numTce;

                      if(tce.nmSecretaria) secOrCon = tce.nmSecretaria;
                      else secOrCon = tce.nmFantasia;

                      if (tce.migracao != null) numTce = tce.migracao;
                      else numTce = tce.id;

                      let url = '{{route("tce.show", ["id" => ":id"])}}';
                      url = url.replace(':id', tce.id);

                      $('#table-tces-ativos').append(
                      '<tr><td>'+ numTce +
                      '</td><td><a href="'+ url +'">' + tce.nmEstudante +
                      '</td><td>'+ secOrCon +
                      '</td><td>'+ tce.dtCancelamento +
                      '</td><td>'+ tce.dtFim +
                      '</td><td>'+ tce.dtFim +
                      '</td></tr>'
                      );
                    });
                  }
                }
            });
          }else{
            $("#table-tces-ativos").fadeOut(1000).hide();
            $("#card-tables-tces").fadeOut(1000).hide();
          }
        });
        
        $("#card-tces-cancelados").click(function(){
          $(".tables-cards").hide();
          $("#card-tables-tces").show();
          if($("#table-tces-cancelados").css('display') == 'none'){
            $("#table-tces-cancelados").fadeIn(1000).show();
            $.ajax({
                url: 'home/get-tces',
                type: 'GET',
                data: { tipo: 'cancelados' },
                success: function(tces)
                {
                  if(tces == null) {
                    $('#table-tces-cancelados').append(
                      "<tr><td colspan='6'><p class='text-center'>Todos os TCE's ativos</p></td></tr>"
                    );
                  } else {

                    tces.forEach(tce => {
                      console.log(tce.id)
                      tce.dtInicio = getFormattedDate(new Date(tce.dtInicio));
                      tce.dtFim = getFormattedDate(new Date(tce.dtFim));
                      
                      let secOrCon;
                      let numTce;

                      if(tce.nmSecretaria) secOrCon = tce.nmSecretaria;
                      else secOrCon = tce.nmFantasia;

                      if (tce.migracao != null) numTce = tce.migracao;
                      else numTce = tce.id;

                      let url = '{{route("tce.show", ["id" => ":id"])}}';
                      url = url.replace(':id', tce.id);

                      $('#table-tces-cancelados').append(
                      '<tr><td>' + numTce +
                      '</td><td><a href="'+ url +'">' + tce.nmEstudante +
                      '</a></td><td>' + secOrCon +
                      '</td><td>' + tce.dtInicio +
                      '</td><td>' + tce.name +
                      '</td><td>' + tce.dsMotivoCancelamento +
                      '</td></tr>'
                      );
                    });
                  }
                }
            });
          }else{
            $("#table-tces-cancelados").fadeOut(1000).hide();
            $("#card-tables-tces").fadeOut(1000).hide();
          }
        });
        
        $("#card-tces-vencidos").click(function(){
          $(".tables-cards").hide();
          $("#card-tables-tces").show();
          if($("#table-tces-vencidos").css('display') == 'none'){
            $("#table-tces-vencidos").fadeIn(1000).show();
          }else{
            $("#table-tces-vencidos").fadeOut(1000).hide();
            $("#card-tables-tces").fadeOut(1000).hide();
          }
        });
        
        $("#card-tces-proxVencidos").click(function(){
          $(".tables-cards").hide();
          $("#card-tables-tces").show();
          if($("#table-tces-proxVencidos").css('display') == 'none'){
            $("#table-tces-proxVencidos").fadeIn(1000).show();
          }else{
            $("#table-tces-proxVencidos").fadeOut(1000).hide();
            $("#card-tables-tces").fadeOut(1000).hide();
          }
        });
        //ENDTCES
      })
    }
  });

  function getFormattedDate(date) {
    let year = date.getFullYear();
    let month = (1 + date.getMonth()).toString().padStart(2, '0');
    let day = date.getDate().toString().padStart(2, '0');
  
    return month + '/' + day + '/' + year;
  };
</script>
@endsection
