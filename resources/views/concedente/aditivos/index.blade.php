@extends('adminlte::page')
@if(!empty($titulo))
@section('title_postfix', ' - '.$titulo)
@endif
@section('content_header')
<h1>Histórico de Criação de Aditivos</h1>

<ol class="breadcrumb">
  <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
  <li class="active"><a href="{{route('tce.index.cancelados')}}"><i class="fa fa-files-o"></i>Aditivos</a></li>
</ol>
@stop
@section('content')
@if(session('success'))
<div class="alert alert-success alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
  <h4>
    <i class="icon fa fa-check"></i> Alerta!</h4>
    {{session('success')}}
  </div>
  @endif
  @if(session('error'))
  <div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4>
      <i class="icon fa fa-ban"></i> Alerta!</h4>
      {{session('error')}}
    </div>
    @endif

    <br>
    <div class="row" style="display:none;">
      <div class="col-md-12">
        <div class="box">
          <div class="box-body">
            <div class="row">
              <div class="col-md-6">
                <div class="pull-left">
                  <label for="">Filtrar por:</label>
                  <div class="input-group sm" style="width:300px;">
                    <select class="form-control input-sm" name="options">
                      <option value="">Todos</option>
                      <option value="dataCresc">Ordernar por data cresc.</option>
                      <option value="dataDesc">Ordernar por data desc.</option>
                      <option value="numero">Ordernar por n° Aditivo</option>
                    </select>
                    <span class="input-group-btn">
                      <button type="button" name="btn-filter" class="btn btn-primary btn-sm">Fitlrar</button>
                    </span>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group pull-right">
                  <label for="key_search">Buscar por:</label>
                  <div class="input-group input-group-sm" style="width: 300px;">
                    <input type="search" name="search" class="form-control" placeholder="Pesquisar por nome/cpf/tce..." >
                    <div class="input-group-addon">
                      <i class="fa fa-search"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Listagem de Aditivos</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table id="tces" class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th style="width: 5%;">MIGRAÇÃO</th>
                  <th style="width: 5%;">TCE</th>
                  <th style="width: 5%;">ADITIVO</th>
                  <th style="width: 35%;">ESTUDANTE</th>
                  <th class="no-sort" style="width: 10%;">INICIO VIGÊNCIA</th>
                  <th class="no-sort" style="width: 10%;">FIM VIGÊNCIA</th>
                  <th style="width:10%;">Criado por:</th>
                  <th style="width:10%;">Criado em:</th>
                </tr>
              </thead>
              <tbody>
                @forelse($aditivos as $aditivo)
                <tr class="objects" data-nome="{{$aditivo->tce->estudante->nmEstudante}}" data-tce="{{$aditivo->tce_id}}" data-cpf="{{str_replace(array('.','-'),'',$aditivo->tce->estudante->cdCPF)}}" data-dtaditivo="{{$aditivo->created_at}}" data-nm="{{$aditivo->nmAditivo}}">
                  <td>
                    @if($aditivo->tce->migracao == NULL)
                    -
                    @else
                    {{$aditivo->tce->migracao}}
                    @endif
                  </td>
                  <td>{{$aditivo->tce->id}}</td>
                  <td>{{$aditivo->nnAditivo}}</td>
                  <td>
                    <a href="{{route('tce.show', ['tce' => $aditivo->tce->id])}}">{{$aditivo->tce->estudante->nmEstudante}}</a>
                  </td>
                  <td>{{\Carbon\Carbon::parse($aditivo->dtInicio)->format('d/m/Y')}}</td>
                  <td>
                    {{\Carbon\Carbon::parse($aditivo->dtFim)->format('d/m/Y')}}
                  </td>
                  <td>
                    {{$aditivo->userCadastro->name}}
                  </td>
                  <td>
                    {{$aditivo->created_at->format('d/m/Y H:m')}}
                  </td>
                  <!-- ->route('estudante.tce.show', ['estudante' => $formulario['estudante_id'],'tce' => $new->id]); -->
                </tr>
                @empty
                <tr>
                  <td colspan="8" class="text-center">
                    <span class="badge bg-red">Não existe registros no banco!</span>
                  </td>
                </tr>
                @endforelse
              </tbody>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      @stop
      @section('post-script')
      <script type="text/javascript">
      $('input[name=search]').keyup(function(){
        var tces = $(".objects");
        var nome = removeSpecialChar($(this).val());
        search(nome,tces);
      });
      function search(nome,tces){
        tces.show();
        if (nome != "") {
          for (var i = 0; i < tces.length; i++) {
            if (tces[i].dataset.nome.toLowerCase().search(nome.toLowerCase()) && tces[i].dataset.tce.toLowerCase().search(nome.toLowerCase()) && tces[i].dataset.cpf.search(nome)) {
              tces[i].style.display = "none";
            }else{
              count++;
            }
          }
          filtrar();
        }
      }

      $("button[name=btn-filter]").click(function(){
        filtrar();
      });

      function filtrar(){
        var tces = $(".objects");
        var filtro = $('select[name=options]').val();
        let row = [];
        let order = [];
        if (filtro == 'dataCresc') {
          for (let i = 0; i < tces.length; i++) {
            let tr = tces[i];
            row.push(tr)
            order = row.sort(function (a, b) {
              // '01/03/2014'.split('/')
              // gives ["01", "03", "2014"]
              c = a.dataset.dtaditivo.split('/');
              d = b.dataset.dtaditivo.split('/');

              return c[2] - d[2] || c[1] - d[1] || c[0] - d[0];
            });
          }

          $('#tces').html(order);

        }else if(filtro == 'dataDesc'){
          for (let i = 0; i < tces.length; i++) {
            let tr = tces[i];
            row.push(tr)
            order = row.sort(function (a, b) {
              // '01/03/2014'.split('/')
              // gives ["01", "03", "2014"]
              c = a.dataset.dtaditivo.split('/');
              d = b.dataset.dtaditivo.split('/');

              return c[2] - d[2] || c[1] - d[1] || c[0] - d[0];
            });
          }
          order.reverse();
          $('#tces').html(order);
        }else if(filtro == 'numero'){
          for (let i = 0; i < tces.length; i++) {
            let tr = tces[i];
            row.push(tr)
            order = row.sort(function(a, b) {
              if(a.dataset.nm < b.dataset.nm) {
                return -1;
              }
              if(a.dataset.nm > b.dataset.nm) {
                return 1;
              }
              return 0;
            })
          }
          $('#tces').html(order);
        }
      }
      </script>
      @endsection
