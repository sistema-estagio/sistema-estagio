@extends('adminlte::page')
@if(!empty($titulo))
    @section('title_postfix', ' - '.$titulo)
@endif
@section('content_header')
<h1>Concedentes</h1>
<ol class="breadcrumb">
	<li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="{{route('concedente.index')}}"><i class="fa fa-industry"></i> Concedentes</a></li>
    <li class="active"><i class="fa fa-file"></i> Cadastro de Concedente</li>
</ol>
@stop 
@section('content') 

@if(session('success'))
<div class="alert alert-success alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h4>
		<i class="icon fa fa-check"></i> Alerta!</h4>
	{{session('success')}}
</div>
@endif 

@if(session('error'))
<div class="alert alert-danger alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h4>
		<i class="icon fa fa-ban"></i> Alerta!</h4>
	{{session('error')}}
</div>
@endif

@if($errors->any())
<div class="alert alert-warning alert-dismissible">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-ban"></i> Alerta!</h4>
    <ul class="alert-warning">
        <h3>:( Whoops, Houve algum erro! </h3>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="row">
	<div class="col-md-12">
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">Formulário de Cadastro</h3>
			</div>
			<!-- /.box-header -->
			<!-- form start -->
			<form action="{{route('concedente.store')}}" method="POST">
				{!! csrf_field() !!}
				<div class="box-body">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="flTipo">*Tipo</label>
                                <select id="flTipo" name="flTipo" class="form-control" required="true">
                                    <option value="" selected>Selecione</option>
                                    <option value="PF" @if (old('flTipo') == "PF") {{ 'selected' }} @endif>Pessoa Física</option>
                                    <option value="PJ" @if (old('flTipo') == "PJ") {{ 'selected' }} @endif>Pessoa Jurídica</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-10">
                            <div class="form-group">
                                <label for="nmRazaoSocial">*Razão Social</label>
                                <input class="form-control" id="nmRazaoSocial" name="nmRazaoSocial" placeholder="Nome do Concedente" type="text" value="{{old('nmRazaoSocial')}}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nmFantasia">*Nome Fantasia</label>
                                <input class="form-control" id="nmFantasia" name="nmFantasia" placeholder="Nome Fantasia da Concedente" type="text" value="{{old('nmFantasia')}}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="cdCnpjCpf">*CNPJ/CPF</label>
                                <input class="form-control" id="cdCnpjCpf" name="cdCnpjCpf" placeholder="Documento" type="text" value="{{old('cdCnpjCpf')}}">
                            </div>
                        </div>
                        <div class="col-md-2" id="orgaoRegulador">
                            <div class="form-group">
                                <label for="dsOrgaoRegulador">Orgão Regulador</label>
                                <input class="form-control" id="dsOrgaoRegulador" name="dsOrgaoRegulador" placeholder="OAB-0000" type="text" value="{{old('dsOrgaoRegulador')}}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nmResponsavel">*Nome Responsável</label>
                                <input class="form-control" id="nmResponsavel" name="nmResponsavel" placeholder="Nome Responsável" type="text" value="{{old('nmResponsavel')}}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="dsResponsavelCargo">*Cargo Responsável</label>
                                <input class="form-control" id="dsResponsavelCargo" name="dsResponsavelCargo" placeholder="Cargo do Resposável" type="text" value="{{old('dsResponsavelCargo')}}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nmAdministrador">Nome Administrador</label>
                                <input class="form-control" id="nmAdministrador" name="nmAdministrador" placeholder="Nome Administrador" type="text" value="{{old('nmAdministrador')}}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="dsAdministradorCargo">Cargo do Administrador</label>
                                <input class="form-control" id="dsAdministradorCargo" name="dsAdministradorCargo" placeholder="Cargo do Administrador" type="text" value="{{old('dsAdministradorCargo')}}">
                            </div>
                        </div>
                    </div>
                    <legend>Convenio e Valor</legend>
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="dtInicioContrato">Data Inicio Convenio</label>
                                <input class="form-control" id="dtInicioContrato" name="dtInicioContrato" placeholder="" type="text" value="{{old('dtInicioContrato')}}">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="dtFimContrato">Data Fim Convenio</label>
                                <input class="form-control" id="dtFimContrato" name="dtFimContrato" placeholder="" type="text" value="{{old('dtFimContrato')}}">
                            </div>
                        </div>
                        
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="vlEstagio">Valor CI</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                    <input class="form-control" id="vlEstagio" name="vlEstagio" type="text" value="{{old('vlEstagio')}}">
                                </div>
                            </div>
                        </div>
					</div>
                    
					<legend>Endereço e Contatos</legend>
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="cdCEP">*CEP</label>
                                <input class="form-control" id="cdCEP" name="cdCEP" placeholder="CEP" type="text" value="{{old('cdCEP')}}">
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <label for="dsEndereco">*Endereço</label>
                                <input class="form-control" id="dsEndereco" name="dsEndereco" placeholder="Endereço" type="text" value="{{old('dsEndereco')}}">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="nnNumero">*Numero</label>
                                <input class="form-control" id="nnNumero" name="nnNumero" placeholder="000" type="text" value="{{old('nnNumero')}}">
                            </div>
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                                <div class="form-group">
                                    <label for="nmBairro">*Bairro</label>
                                    <input class="form-control" id="nmBairro" name="nmBairro" placeholder="Bairro" type="text" value="{{old('nmBairro')}}">
                                </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="dsComplemento">Complemento</label>
                                <input class="form-control" id="dsComplemento" name="dsComplemento" placeholder="Complemento" type="text" value="{{old('dsComplemento')}}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="cidade_id">*Estado</label>
                                <select class="form-control" name="estado_id" id="estado_id">
                                    <option value="" selected>Selecione...</option>
                                    @foreach($estados as $estado)
                                    <option value="{{ $estado['id'] }}" @if(old('estado_id') == $estado['id']) {{ 'selected' }} @endif>{{ $estado['nmEstado'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="cidade_id">*Cidade</label>
                                <select class="form-control" id="cidade_id" name="cidade_id">
                                    @if(old('estado_id'))
                                        @foreach($cidades as $cidade)
                                            <option value="{{ $cidade['id'] }}" @if(old('cidade_id') == $cidade['id']) {{ 'selected' }} @endif>{{ $cidade['nmCidade'] }}</option>
                                        @endforeach
                                    @else
                                        <option value="" selected>Selecione Estado</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="dsFone">*Telefone</label>
                                <input class="form-control" id="dsFone" name="dsFone" placeholder="(88)3535-12345" type="text" value="{{old('dsFone')}}">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="dsOutroFone">Outro Telefone</label>
                                <input class="form-control" id="dsOutroFone" name="dsOutroFone" placeholder="(88)98801-1234" type="text" value="{{old('dsOutroFone')}}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="dsEmail">*E-mail</label>
                                <input class="form-control" id="dsEmail" name="dsEmail" placeholder="email@email.com.br" type="text" value="{{old('dsEmail')}}">
                            </div>
                        </div>
                        <!--
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="unidade_id">Unidade Upa</label>
                                <input class="form-control" id="unidade_id" name="unidade_id" placeholder="Unidade" type="text">
                            </div>
                        </div>
                        -->
                    </div>
				</div>
				<!-- /.box-body -->
				<div class="box-footer text-center">
					<button type="submit" class="btn btn-success">Cadastrar</button>
				</div>
			</form>
		</div>

	</div>
</div>

@section('post-script')

<script type="text/javascript">
    $(document).ready(function(){
        $('#cidade_id').select2();
        $('#estado_id').select2();
        //maskara nova de telefones
        var SPMaskBehavior = function (val) {
            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
          },
          spOptions = {
            onKeyPress: function(val, e, field, options) {
                field.mask(SPMaskBehavior.apply({}, arguments), options);
              }
          };
        
          $('#dsFone').mask(SPMaskBehavior, spOptions);
          $('#dsOutroFone').mask(SPMaskBehavior, spOptions);
    var valorEscolhido;
    
    $("#flTipo").change(function(){
    //$(document).ready(function(){
       valorEscolhido = $("#flTipo option:selected").val();
       if(valorEscolhido == 'PF'){
                //alert(valorEscolhido);
                jQuery(function($){
                    $("#cdCnpjCpf").mask("999.999.999-99");
                    $("#orgaoRegulador").show("slow");
                });
        } else {
                jQuery(function($){
                    $("#cdCnpjCpf").mask("99.999.999/9999-99");
                    $("#orgaoRegulador").hide("slow");
                });
        }      
    });
 });

        $('select[name=estado_id]').change(function () {
            var idEstado = $(this).val();
          
            $.get('../get-cidades/' + idEstado, function (cidades) {
               
                $('select[name=cidade_id]').empty();
                $.each(cidades, function (key, value) {
                    $('select[name=cidade_id]').append('<option value=' + value.id + '>' + value.nmCidade + '</option>');
                });
            });
        });

jQuery(function($){
        $("#cdCEP").mask("99999-999");
        $("#dtFimContrato").mask("99/99/9999");
        $("#dtInicioContrato").mask("99/99/9999");
        $('#vlEstagio').maskMoney({thousands:'.',decimal:','});
    });
    /*jQuery("#dsFone, #dsOutroFone")
        .mask("(99) 9999-9999?9")
        .focusout(function (event) {  
            var target, phone, element;  
            target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
            phone = target.value.replace(/\D/g, '');
            element = $(target);  
            element.unmask();  
            if(phone.length > 10) {  
                element.mask("(99) 99999-999?9");  
            } else {  
                element.mask("(99) 9999-9999?9");  
            }  
        });
        */
</script>
        <!-- Adicionando Javascript -->
        <script type="text/javascript" >
    
            $(document).ready(function() {
    
                function limpa_formulário_cep() {
                    // Limpa valores do formulário de cep.
                    $("#dsEndereco").val("");
                    $("#nmBairro").val("");
                }
                
                //Quando o campo cep perde o foco.
                $("#cdCEP").blur(function() {
    
                    //Nova variável "cep" somente com dígitos.
                    var cep = $(this).val().replace(/\D/g, '');
    
                    //Verifica se campo cep possui valor informado.
                    if (cep != "") {
    
                        //Expressão regular para validar o CEP.
                        var validacep = /^[0-9]{8}$/;
    
                        //Valida o formato do CEP.
                        if(validacep.test(cep)) {
    
                            //Preenche os campos com "..." enquanto consulta webservice.
                            $("#dsEndereco").val("...");
                            $("#nmBairro").val("...");
    
                            //Consulta o webservice viacep.com.br/
                            $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {
    
                                if (!("erro" in dados)) {
                                    //Atualiza os campos com os valores da consulta.
                                    $("#dsEndereco").val(dados.logradouro);
                                    $("#nmBairro").val(dados.bairro);
                                } //end if.
                                else {
                                    //CEP pesquisado não foi encontrado.
                                    limpa_formulário_cep();
                                    alert("CEP não encontrado.");
                                }
                            });
                        } //end if.
                        else {
                            //cep é inválido.
                            limpa_formulário_cep();
                            alert("Formato de CEP inválido.");
                        }
                    } //end if.
                    else {
                        //cep sem valor, limpa formulário.
                        limpa_formulário_cep();
                    }
                });
            });
        </script>
@endsection 
@stop