@extends('adminlte::page')
@if(!empty($titulo))
@section('title_postfix', ' - '.$titulo)
@endif
@section('content_header')
<h1>Tces Vencidos</h1>
<ol class="breadcrumb">
  <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
  <li><a href="{{route('concedente.index')}}"><i class="fa fa-industry"></i> Concedentes</a></li>
  <li><a href="{{route('concedente.show', ['id'=>$concedente->id])}}"><i class="fa fa-industry"></i> {{$concedente->nmRazaoSocial}}</a></li>
  @if($secretaria != null)
  <li><a href="{{route('concedente.secretaria.show', ['idConcedente'=>$concedente->id, 'id' => $secretaria])}}"><i class="fa fa-industry"></i> {{$secretaria->nmSecretaria}}</a></li>
  @endif
  <li class="active"><i class="fa fa-files-o"></i> TCES Vencidos</li>
</ol>
@stop
@section('css')
<link href="{{ asset('assets/vendor/toast/toastr.css')}}" rel="stylesheet">
<style>
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input {
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>
@endsection
@section('content')
@if(session('success'))
<div class="alert alert-success alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
  <h4>
    <i class="icon fa fa-check"></i> Alerta!</h4>
    {{session('success')}}
  </div>
  @endif
  @if(session('error'))
  <div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4>
      <i class="icon fa fa-ban"></i> Alerta!</h4>
      {{session('error')}}
    </div>
    @endif
    <br>
    <div class="row" style="display:none;">
      <div class="col-md-12">
        <div class="box">
          <div class="box-body">

            <form action="{{route('tce.search')}}" method="POST">
              {!! csrf_field() !!}
              <div class="input-group input-group-sm" style="width: 600px;">
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="tipo">*Tipo:</label>
                    <div class="input-group input-group-sm" style="width: 200px;">
                      <select class="form-control pull-right" name="tipo" id="tipo">
                        <option value="migracao">Tce Antigo</option>
                        <option value="tce" >Tce</option>
                        <option value="estudante" selected>Estudante</option>
                        <option value="concedente">Concedente</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-md-5">
                  <div class="form-group">
                    <label for="key_search">*Buscar por:</label>
                    <div class="input-group input-group-sm" style="width: 300px;">
                      <input name="key_search" class="form-control pull-right" placeholder="Pesquisar" type="text">
                      <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">
                          <i class="fa fa-search"></i> Buscar
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header with-border">
            <div class="row">
              <div class="col-md-6">
                <h3 class="box-title">Listagem de Tces</h3>
              </div>
              <div class="col-md-6">
                <div class="btn-group pull-right">
                  <button type="button" name="btn-cancelar-operacao" class="btn btn-primary btn-sm" style="display:none;margin-right:20px;">Voltar</button>
                  <button type="button" name="btn-cancelamento-lote" class="btn btn-danger btn-sm pull-right" value="0"><i class="fa fa-times"></i> Cancelamento em Lote</button>
                </div>
              </div>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="table-responsive">
              <table id="tces" class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th class="no-sort" id="th-include" style="width: 3%;display:none;"><button type="button" name="btn-include" class="btn btn-sm btn-primary">Incluir</button></th>
                    <th style="width: 3%;">MIGRAÇÃO</th>
                    <th style="width: 5%;">TCE</th>
                    <th style="width: 60%;">ESTUDANTE</th>
                    <th style="width: 10%;" class="no-sort">INICIO VIGÊNCIA</th>
                    <th style="width: 10%;" class="no-sort">FIM VIGÊNCIA</th>
                  </tr>
                </thead>
                <tbody>
                  @forelse($tces as $tce)
                  <tr id="tce-{{$tce->id}}">
                    <td class="text-center object" style="display:none;">
                      <label class="switch">
                        <input type="checkbox" class="tces" value="{{$tce->id}}">
                        <span class="slider round"></span>
                      </label>
                    </td>
                    <td>
                      @if($tce->migracao == NULL)
                      -
                      @else
                      {{$tce->migracao}}
                      @endif
                    </td>
                    @if($tce->aditivos->count() > 0)
                    <td>{{$tce->id}}/{{$tce->aditivos->count()}}</td>
                    @else
                    <td>{{$tce->id}}</td>
                    @endif
                    <td>
                      <a href="{{route('tce.show', ['tce' => $tce->id])}}">{{$tce->estudante->nmEstudante}}</a>
                    </td>
                    <td>{{$tce->dtInicio->format('d/m/Y')}}</td>
                    <td>
                      @if($tce->aditivos->count() > 0)
                      @if($tce->aditivos()->latest()->first()->dtInicio != Null)
                      {{$tce->aditivos()->latest()->first()->dtFim->format('d/m/Y')}}
                      @else
                      {{$tce->dtFim->format('d/m/Y')}}
                      @endif
                      @else
                      {{$tce->dtFim->format('d/m/Y')}}
                      @endif
                    </td>
                    <!-- route('estudante.tce.show', ['estudante' => $formulario['estudante_id'],'tce' => $new->id]); -->
                  </tr>
                  @empty
                  <tr>
                    <td colspan="6" class="text-center">
                      <span class="badge bg-red">Não existe registros no banco!</span>
                    </td>
                  </tr>
                  @endforelse
                </tbody>
              </table>
            </div>
          </div>
          <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="modalConfirmCancelamentoLote">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title" id="myModalLabel">
                    Cancelamento em Lote
                  </h4>
                </div>
                <form action="javascript:void(0);" method="POST" name="form-cancelar-lote">
                  <div class="modal-body">
                    <div class="box-body">
                      <div class="row">
                        <div class="col-md-5">
                          <div class="form-group">
                            <label for="dtCancelamento">*Data de Encerramento</label>
                            <input class="form-control" id="dtCancelamento" name="dtCancelamento" type="text" value="{{\Carbon\Carbon::now()->format('d/m/Y')}}">
                          </div>
                        </div>
                        <div class="col-md-7">
                          <div class="form-group">
                            <label for="motivoCancelamento_id">*Motivo do Cancelamento</label>
                            <select class="form-control" id="motivoCancelamento_id" name="motivoCancelamento_id">
                              @foreach($motivosCancelamento->where('tipo', 'c') as $motivo)
                              <option value="{{$motivo['id']}}">{{$motivo['dsMotivoCancelamento']}}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label for="dtIniio">Descrição do Motivo</label>
                            <input class="form-control" id="dsMotivoCancelamento" name="dsMotivoCancelamento" placeholder="Caso queira detalhar melhor..." type="text">
                            <span class="help-block">Digite aqui, os motivos que levaram ao cancelamento deste TCE</span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
                    <button type="submit" class="btn btn-danger">Cancelar</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      @section('post-script')
      <script type="text/javascript" src="{{ asset('assets/vendor/toast/toastr.js')}}"></script>
      <script>
      toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-bottom-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "5000",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
      }

      $("button[name=btn-cancelar-operacao]").click(function(){
        hideBtnCancelamentoLoteConfirm();
      });

      $("button[name=btn-cancelamento-lote]").click(function(){
        if($(this).val() == 0){
          $("button[name=btn-cancelar-operacao]").show();
          $(this).removeClass('btn-danger').addClass('btn-success');
          $(this).html("<i class='fa fa-times'></i> Confirmar Cancelamento");
          $("#th-include").show();
          $(".object").show();
          $(this).val(1);
        }else{
          if(getTces().length == 0){
            toastr.info('Selecione no mínimo um tce para realizar o cancelamento');
          }else{
            $(this).attr('data-toggle',"modal");
            $(this).attr('data-target',"#modalConfirmCancelamentoLote");
          }
        }
      });

      function hideBtnCancelamentoLoteConfirm(){
        $("button[name=btn-cancelar-operacao]").hide();
        $('button[name=btn-cancelamento-lote').removeClass('btn-success').addClass('btn-danger');
        $('button[name=btn-cancelamento-lote').html("<i class='fa fa-times'></i> Cancelamento em lote");
        $('button[name=btn-cancelamento-lote').attr('data-toggle',"");
        $('button[name=btn-cancelamento-lote').attr('data-target',"");
        $("#th-include").hide();
        $(".object").hide();
        $('button[name=btn-cancelamento-lote').val(0);
      }

      $("form[name=form-cancelar-lote]").submit(function(){
        var tces = getTces();
        $.post("{{ route('tces.vencidos.cancelar.submit') }}", { 'tces':tces , 'dtCancelamento': $(this).find('input[name=dtCancelamento]').val(), 'motivoCancelamento_id': $(this).find('select[name=motivoCancelamento_id]').val(), 'dsMotivoCancelamento': $(this).find('input[name=dsMotivoCancelamento]').val(), '_token': '{{csrf_token()}}' }, function(response){
          if(response != null){
            console.log(response);
            if(response.status == "success"){
              for(var i = 0; i < tces.length; i++){
                $("#tce-"+tces[i]).remove();
              }
              hideBtnCancelamentoLoteConfirm();
              $('#modalConfirmCancelamentoLote').modal('hide');
              toastr.success('Cancelamentos realizados com sucesso');
            }else{
              toastr.error('Não foi possível realizar o cancelamento do lote');
            }
          }
        });
      });

      function getTces(){
        var switchs = $('.tces');
        var tces = [];
        if(switchs.length > 0){
          for(var i = 0; i < switchs.length; i++){
            if(switchs[i].checked == true){
              tces.push(switchs[i].value);
            }
          }
        }

        return tces;
      }


      $(function () {
        $('#tces').DataTable({
          "columnDefs": [
            { "orderable": false, "targets": 'no-sort' }
          ],
          "pageLength": 25,
          "order": [[ 1, "desc" ]],
          "language": {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_ resultados por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar",
            "oPaginate": {
              "sNext": "Próximo",
              "sPrevious": "Anterior",
              "sFirst": "Primeiro",
              "sLast": "Último"
            },
            "oAria": {
              "sSortAscending": ": Ordenar colunas de forma ascendente",
              "sSortDescending": ": Ordenar colunas de forma descendente"
            }
          }
        })

      });

      $('.dataMask').mask('00/00/0000')
      </script>
      @endsection
      @stop
