@extends('adminlte::page') 
@if(!empty($titulo))
	@section('title_postfix', ' - '.$titulo)
@endif
@section('content_header')

<h1>Edição de Concedente</h1>
<ol class="breadcrumb">
	<li>
		<a href="{{route('home')}}">
			<i class="fa fa-dashboard"></i> Home</a>
	</li>
	<li>
		<a href="{{route('concedente.index')}}">
			<i class="fa fa-industry"></i> Concedentes</a>
	</li>
	<li class="active">
		<i class="fa fa-pencil"></i> {{$concedente->nmRazaoSocial}}</li>
</ol>
@stop 
@section('content') 
@if(session('success'))
<div class="alert alert-success alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h4>
		<i class="icon fa fa-check"></i> Alerta!</h4>
	{{session('success')}}
</div>
@endif 
@if(session('error'))
<div class="alert alert-danger alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h4>
		<i class="icon fa fa-ban"></i> Alerta!</h4>
	{{session('error')}}
</div>
@endif
@if(isset($errors)&& $errors->any())
<div class="alert alert-danger alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<ul>
		@foreach($errors->all() as $error)
		<li>{{$error}}</li>
		@endforeach
	</ul>
</div>
@endif
<div class="row">
	<div class="col-md-12">
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">{{$concedente->nmRazaoSocial}}</h3>
			</div>
			<!-- /.box-header -->
			<!-- form start -->
			<form action="{{route('concedente.update', ['concedente' => $concedente->id])}}" method="POST">
				{!! csrf_field() !!} {!! method_field('PUT') !!}
				<div class="box-body">
                <div class="row">
					<div class="col-md-2">
						
						<label for="flTipo">*Tipo</label>
							<div class="form-group">
                                @if($concedente->flTipo === "PF")
                                    Pessoa Física
                                @else
                                    Pessoa Jurídica
                                @endif
                            </div>					
					</div>
					<div class="col-md-10">
						<div class="form-group">
							<label for="nmRazaoSocial">*Razão Social do concedente</label>
							<input class="form-control" id="nmRazaoSocial" name="nmRazaoSocial" value="{{$concedente->nmRazaoSocial}}" placeholder="Nome do Concedente"
							 type="text">
						</div>
					</div>
                </div>
                <div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="nmFantasia">*Nome Fantasia</label>
							<input class="form-control" id="nmFantasia" name="nmFantasia" value="{{$concedente->nmFantasia}} " placeholder="Nome Fantasia da Concedente"
							 type="text">
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label for="cdcnpjCpf">*CNPJ/CPF</label>
							<input class="form-control" id="cdCnpjCpf" name="cdCnpjCpf" value="{{$concedente->cdCnpjCpf}} " placeholder="CNPJ" type="text" readonly>
						</div>
					</div>
					@if($concedente->flTipo === "PF")
					<div class="col-md-2">
						<div class="form-group">
							<label for="dsOrgaoRegulador">Orgão Regulador</label>
							<input class="form-control" id="dsOrgaoRegulador" name="dsOrgaoRegulador" value="{{$concedente->dsOrgaoRegulador}} " placeholder="Orgão Regulador"
								 type="text">
						</div>
					</div>
					@endif
					
                </div>
                <div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="nmResponsavel">Nome Responsável</label>
							<input class="form-control" id="nmResponsavel" name="nmResponsavel" value="{{$concedente->nmResponsavel}}" placeholder="Nome Responsável"
							 type="text">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="dsResponsavelCargo">Cargo Responsável</label>
							<input class="form-control" id="dsResponsavelCargo" name="dsResponsavelCargo" value="{{$concedente->dsResponsavelCargo}}"
							 placeholder="Cargo do Resposável" type="text">
						</div>
					</div>
                </div>
                <div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="nmAdministrador">Nome Administrador</label>
							<input class="form-control" id="nmAdministrador" name="nmAdministrador" value="{{$concedente->nmAdministrador}}" placeholder="Nome Administrador"
							 type="text">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="dsAdministradorCargo">Cargo do Administrador</label>
							<input class="form-control" id="dsAdministradorCargo" name="dsAdministradorCargo" value="{{$concedente->dsAdministradorCargo}}"
							 placeholder="Cargo do Reitor" type="text">
						</div>
					</div>
                </div>

                <legend>Convênio e Valor</legend>
                <div class="row">
					<div class="col-md-2">
						<div class="form-group">
							<label for="dtInicioContrato">Data Inicio Convênio</label>
							@if($concedente->dtInicioContrato)
							<input class="form-control" id="dtInicioContrato" name="dtInicioContrato" type="text" value="{{$concedente->dtInicioContrato->format('d/m/Y')}}">
							@else
							<input class="form-control" id="dtInicioContrato" name="dtInicioContrato" type="text">
							@endif
						</div>
					</div>
                    <div class="col-md-2">
                        <div class="form-group">
							<label for="dtFimContrato">Data Fim Convênio</label>
							@if($concedente->dtFimContrato)
                            <input class="form-control" id="dtFimContrato" name="dtFimContrato" type="text" value="{{$concedente->dtFimContrato->format('d/m/Y')}}">
							@else
							<input class="form-control" id="dtFimContrato" name="dtFimContrato" type="text">							
							@endif
						</div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
							<label for="vlEstagio">Valor CI</label>
							<div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
								@if($concedente->vlEstagio)
								<input class="form-control" id="vlEstagio" name="vlEstagio" type="text" value="{{number_format($concedente->vlEstagio,2,",",".")}}">
								@else
								<input class="form-control" id="vlEstagio" name="vlEstagio" type="text">								
								@endif
							</div>
						</div>
                    </div>
				</div>

				<legend>Endereço e Contatos</legend>
                <div class="row">
					<div class="col-md-2">
						<div class="form-group">
							<label for="cdCEP">*CEP</label>
							<input class="form-control" id="cdCEP" name="cdCEP" value="{{$concedente->cdCEP}}" placeholder="CEP" type="text">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="dsEndereco">*Endereço</label>
							<input class="form-control" id="dsEndereco" name="dsEndereco" value="{{$concedente->dsEndereco}}" placeholder="Endereço"
							 type="text">
						</div>
					</div>
					<div class="col-md-1">
						<div class="form-group">
							<label for="nnNumero">*Numero</label>
							<input class="form-control" id="nnNumero" name="nnNumero" value="{{$concedente->nnNumero}}" placeholder="000" type="text">
						</div>
					</div>
                    <div class="col-md-3">
						<div class="form-group">
							<label for="nmBairro">*Bairro</label>
							<input class="form-control" id="nmBairro" name="nmBairro" value="{{$concedente->nmBairro}}" placeholder="Bairro" type="text">
						</div>
					</div>
                </div>
                <div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="dsComplemento">Complemento</label>
							<input class="form-control" id="dsComplemento" name="dsComplemento" value="{{$concedente->dsComplemento}}" placeholder="Complemento"
							 type="text">
						</div>
					</div>
					<div class="col-md-2">
                        <div class="form-group">
                            <label for="estado_id">*Estado</label>
                            <select class="form-control" name="estado_id" id="estado_id">
                                <option value="{{$concedente->cidade->estado_id}}" selected>{{$concedente->cidade->estado->nmEstado}}</option>
                                <option value="">----------</option>
                                @foreach($estados as $estado)  
                                <option value="{{$estado['id']}}">{{$estado['nmEstado']}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="cidade_id">*Cidade</label>
                            <select class="form-control" id="cidade_id" name="cidade_id">
                            <option value="{{$concedente->cidade->id}}" selected>{{$concedente->cidade->nmCidade}}</option>
                            </select>
                        </div>
                    </div>
					
                </div>
                <div class="row">
					<div class="col-md-2">
						<div class="form-group">
							<label for="dsFone">*Telefone</label>
							<input class="form-control" id="dsFone" name="dsFone" value="{{$concedente->dsFone}}" placeholder="(88)3535-12345" type="text">
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">
						<label for="dsOutroFone">Outro Telefone</label>
							<input class="form-control" id="dsOutroFone" name="dsOutroFone" value="{{$concedente->dsOutroFone}}" placeholder="(88)98801-1234" type="text">
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label for="dsEmail">*E-mail</label>
							<input class="form-control" id="dsEmail" name="dsEmail" value="{{$concedente->dsEmail}}" placeholder="email@email.com.br"
							 type="text">
						</div>
					</div>
					<!--<div class="col-md-4">
						<div class="form-group">
							<label for="unidade_id">Unidade Upa</label>
							<input class="form-control" id="unidade_id" name="unidade_id" value="{{$concedente->unidade_id}}" placeholder="Unidade" type="text">
						</div>
					</div>-->
				</div>
				<legend>Opções Concedentes e Usuários da Concedente</legend>
				<div class="row">
					<div class="col-md-2">
						<div class="form-group">
							<label for="opCancelamento">Usuário Cancela TCE?</label>
							<select class="form-control" id="opCancelamento" name="opCancelamento">
								<option value="{{$concedente->opCancelamento}}" selected>{{$concedente->opCancelamento}}</option>
								<option value="">----------</option>
								<option value="SIM">SIM</option>
								<option value="NAO">NÃO</option>
							</select>
						</div>
					</div>

					<div class="col-md-2">
							<div class="form-group">
								<label for="assUpaTCE">Ass. UPA no TCE?</label>
								<select class="form-control" id="assUpaTCE" name="assUpaTCE">
									<option value="{{$concedente->assUpaTCE}}" selected>{{$concedente->assUpaTCE}}</option>
									<option value="">----------</option>
									<option value="S">SIM</option>
									<option value="N">NÃO</option>
								</select>
							</div>
					</div>

					<div class="col-md-2">
							<div class="form-group">
								<label for="assUpaADITIVO">Ass. UPA no ADITIVO?</label>
								<select class="form-control" id="assUpaADITIVO" name="assUpaADITIVO">
									<option value="{{$concedente->assUpaADITIVO}}" selected>{{$concedente->assUpaADITIVO}}</option>
									<option value="">----------</option>
									<option value="S">SIM</option>
									<option value="N">NÃO</option>
								</select>
							</div>
					</div>
				</div>
					
				</div>
				<!-- /.box-body -->

				<div class="box-footer text-center">
					<button type="submit" class="btn btn-success">Atualizar</button>
				</div>
			</form>
		</div>

	</div>
</div>
@section('post-script')
<script type="text/javascript">
	$(document).ready(function() {
		$('#cidade_id').select2();
		//maskara nova de telefones
        var SPMaskBehavior = function (val) {
            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
          },
          spOptions = {
            onKeyPress: function(val, e, field, options) {
                field.mask(SPMaskBehavior.apply({}, arguments), options);
              }
          };
        
          $('#dsFone').mask(SPMaskBehavior, spOptions);
          $('#dsOutroFone').mask(SPMaskBehavior, spOptions);
    var valorEscolhido;
  
    $("#flTipo").change(function(){
       valorEscolhido = $("#flTipo option:selected").val();
       if(valorEscolhido == 'PF'){
                //alert(valorEscolhido);
                jQuery(function($){
                    $("#cdCnpjCpf").mask("999.999.999-99");
                });
            } else {
                jQuery(function($){
                    $("#cdCnpjCpf").mask("99.999.999/9999-99");
                });
            }      
    });
 });

        $('select[name=estado_id]').change(function () {
            var idEstado = $(this).val();
          
            $.get('../../get-cidades/' + idEstado, function (cidades) {
               
                $('select[name=cidade_id]').empty();
                $.each(cidades, function (key, value) {
                    $('select[name=cidade_id]').append('<option value=' + value.id + '>' + value.nmCidade + '</option>');
                });
            });
        });

jQuery(function($){
		   $("#cdCEP").mask("99999-999");
		   $("#dtInicioContrato").mask("99/99/9999");
		   $("#dtFimContrato").mask("99/99/9999");
		   $('#vlEstagio').maskMoney({thousands:'.',decimal:','});
    });
    /*jQuery("#dsFone, #dsOutroFone")
        .mask("(99) 9999-9999?9")
        .focusout(function (event) {  
            var target, phone, element;  
            target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
            phone = target.value.replace(/\D/g, '');
            element = $(target);  
            element.unmask();  
            if(phone.length > 10) {  
                element.mask("(99) 99999-999?9");  
            } else {  
                element.mask("(99) 9999-9999?9");  
            }  
		});
		*/
</script>
<!-- Adicionando Javascript -->
<script type="text/javascript" >
    
            $(document).ready(function() {
    
                function limpa_formulário_cep() {
                    // Limpa valores do formulário de cep.
                    $("#dsEndereco").val("");
                    $("#nmBairro").val("");
                }
                
                //Quando o campo cep perde o foco.
                $("#cdCEP").blur(function() {
    
                    //Nova variável "cep" somente com dígitos.
                    var cep = $(this).val().replace(/\D/g, '');
    
                    //Verifica se campo cep possui valor informado.
                    if (cep != "") {
    
                        //Expressão regular para validar o CEP.
                        var validacep = /^[0-9]{8}$/;
    
                        //Valida o formato do CEP.
                        if(validacep.test(cep)) {
    
                            //Preenche os campos com "..." enquanto consulta webservice.
                            $("#dsEndereco").val("...");
                            $("#nmBairro").val("...");
    
                            //Consulta o webservice viacep.com.br/
                            $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {
    
                                if (!("erro" in dados)) {
                                    //Atualiza os campos com os valores da consulta.
                                    $("#dsEndereco").val(dados.logradouro);
                                    $("#nmBairro").val(dados.bairro);
                                } //end if.
                                else {
                                    //CEP pesquisado não foi encontrado.
                                    limpa_formulário_cep();
                                    alert("CEP não encontrado.");
                                }
                            });
                        } //end if.
                        else {
                            //cep é inválido.
                            limpa_formulário_cep();
                            alert("Formato de CEP inválido.");
                        }
                    } //end if.
                    else {
                        //cep sem valor, limpa formulário.
                        limpa_formulário_cep();
                    }
                });
            });
        </script>
@endsection 
@stop