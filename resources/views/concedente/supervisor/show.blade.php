@extends('adminlte::page')
@if(!empty($titulo))
	@section('title_postfix', ' - '.$titulo)
@endif
@section('content_header')

<h1>Cadastrar Supervisor em Secretaria</h1>
<ol class="breadcrumb">
	<li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="{{route('concedente.index')}}"><i class="fa fa-industry"></i> Concedentes</a></li>
	<li><a href="{{route('concedente.show', ['concedente' => $concedente->id])}}"><i class="fa fa-industry"></i> {{$concedente->nmRazaoSocial}}</a></li>
	<li><a href="{{route('concedente.show.supervisor', ['idConcedente' => $concedente->id])}}"<i class="fa fa-file"></i> Supervisores</a></li>
  <li class="active"><i class="fa fa-pencil"></i> {{$supervisor->nome}}</li>
</ol>
@stop
@section('content')
  @if(session('success'))
  <div class="alert alert-success alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h4><i class="icon fa fa-check"></i> Alerta!</h4>
          {{session('success')}}
  </div>
  @endif

  @if(session('error'))
  <div class="alert alert-warning alert alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h4><i class="icon fa fa-ban"></i> Alerta!</h4>
          {{session('error')}}
  </div>
  @endif

  @if($errors->any())
  <div class="alert alert-warning alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h4><i class="icon fa fa-ban"></i> Alerta!</h4>
      <ul class="alert-warning">
          <h3>:( Whoops, Houve algum erro! </h3>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
      </ul>
  </div>
  @endif
<div class="row">
	<div class="col-md-12">
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">{{$supervisor->nome}}</h3>
				<div class="box-tools pull-right">
					@if ($supervisor->validate($supervisor) == true)
					<span class="label label-success">Disponível</span>
					@else
					<span class="label label-danger">Indisponível</span>
					@endif
				</div>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<table class="table table-condensed table-striped">
					<tbody>
						<tr>
							<td colspan="1"><label>CPF:</label> {{$supervisor->cpf}}</td>
							<td colspan="1"><label>Cargo:</label> {{$supervisor->cargo}}</td>
						</tr>
						<tr>
							<td colspan="1"><label>Telefone:</label> {{$supervisor->telefone}}</td>
							<td colspan="2"><label>E-mail:</label> {{$supervisor->email}}</td>
						</tr>
						<tr>
							<td colspan="3">
								<label>Cadastrado em: </label>
								{{$supervisor->created_at->format("d/m/Y H:i:s")}}
								@if ($supervisor->created_at->format("d/m/Y H:i:s") != $supervisor->updated_at->format("d/m/Y H:i:s"))
								<br><label>Atualizado em:</label> {{$supervisor->updated_at->format("d/m/Y H:i:s")}}
								@endif
							</td>

						</tr>
					</tbody>
				</table>

			</div>
			<!-- /.box-body -->

			<div class="box-footer text-center">
				<a href="{{route('supervisor.edit', $supervisor->id)}}" class="btn btn-warning ad-click-event">Editar</a>
			</div>
		</div>

	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="box box-info">
			<div class="box-header">
				<h5 class="float-left">TCE's Pendentes</h5>
			</div>
			<div class="box-body">
				<table class="table table-bordered">
					<tbody>
						<tr>
							<th style="width: 40px">TCE</th>
							<th>Estudante</th>
							<th>Instituição</th>
							<th>Duração</th>
							<th class="text-center">Ação</th>
						</tr>
						@forelse(\App\Models\Supervisor::getTcesAditivos($supervisor)->where('dtCancelamento',null)->where('dt4Via',null) as $tce)
						<tr>
							<td>{{$tce->id}}</td>
							<td>{{$tce->estudante->nmEstudante}}</td>
							<td>{{$tce->instituicao->nmInstituicao}}</td>
							<td>{{$tce->dtInicio->format('d/m/Y')}} até
								@if($tce->aditivos->count() > 0)
								@if($tce->aditivos()->latest()->first()->dtInicio != Null)
								{{$tce->aditivos()->latest()->first()->dtFim->format('d/m/Y')}}
								@else
								{{$tce->dtFim->format('d/m/Y')}}
								@endif
								@else
								{{--  {{date('d/m/Y',strtotime($tce->dtFim))}}  --}}
								{{$tce->dtFim->format('d/m/Y')}}
								@endif</td>
								<td class="text-center"><a href="{{route('tce.show', ['tce' => $tce->id])}}" target="_blank" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Ver TCE">Ver</a></td>
								<!-- ->route('estudante.tce.show', ['estudante' => $formulario['estudante_id'],'tce' => $new->id]); -->
							</tr>
							@empty
							<tr>
								<td colspan="5" class="text-center">Nenhum tce relacionado ao supervisor</td>
							</tr>
							@endforelse

						</tbody>
					</table>
				</div>
				<!-- /.box-body -->
			</div>
	</div>
	<div class="col-md-12">
		<div class="box box-success">
			<div class="box-header">
				<h5 class="float-left">TCE's Ativos</h5>
			</div>
			<div class="box-body">
				<table class="table table-bordered">
					<tbody>
						<tr>
							<th style="width: 40px">TCE</th>
							<th>Estudante</th>
							<th>Instituição</th>
							<th>Duração</th>
							<th class="text-center">Ação</th>
						</tr>
						@forelse(\App\Models\Supervisor::getTcesAditivos($supervisor)->where('dtCancelamento',null)->where('dt4Via','!=',null) as $tce)
						<tr>
							<td>{{$tce->id}}</td>
							<td>{{$tce->estudante->nmEstudante}}</td>
							<td>{{$tce->instituicao->nmInstituicao}}</td>
							<td>{{$tce->dtInicio->format('d/m/Y')}} até
								@if($tce->aditivos->count() > 0)
								@if($tce->aditivos()->latest()->first()->dtInicio != Null)
								{{$tce->aditivos()->latest()->first()->dtFim->format('d/m/Y')}}
								@else
								{{$tce->dtFim->format('d/m/Y')}}
								@endif
								@else
								{{--  {{date('d/m/Y',strtotime($tce->dtFim))}}  --}}
								{{$tce->dtFim->format('d/m/Y')}}
								@endif</td>
								<td class="text-center"><a href="{{route('tce.show', ['tce' => $tce->id])}}" target="_blank" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Ver TCE">Ver</a></td>
								<!-- ->route('estudante.tce.show', ['estudante' => $formulario['estudante_id'],'tce' => $new->id]); -->
							</tr>
							@empty
							<tr>
								<td colspan="5" class="text-center">Nenhum tce relacionado ao supervisor</td>
							</tr>
							@endforelse

						</tbody>
					</table>
				</div>
				<!-- /.box-body -->
			</div>
	</div>
	<div class="col-md-12">
		<div class="box box-danger">
			<div class="box-header">
				<h5 class="float-left">TCE's Cancelados</h5>
			</div>
			<div class="box-body">
				<table class="table table-bordered">
					<tbody>
						<tr>
							<th style="width: 40px">TCE</th>
							<th>Estudante</th>
							<th>Instituição</th>
							<th>Duração</th>
							<th class="text-center">Ação</th>
						</tr>
						@forelse(\App\Models\Supervisor::getTcesAditivos($supervisor)->where('dtCancelamento','!=',null) as $tce)
						<tr>
							<td>{{$tce->id}}</td>
							<td>{{$tce->estudante->nmEstudante}}</td>
							<td>{{$tce->instituicao->nmInstituicao}}</td>
							<td>{{$tce->dtInicio->format('d/m/Y')}} até
								@if($tce->aditivos->count() > 0)
								@if($tce->aditivos()->latest()->first()->dtInicio != Null)
								{{$tce->aditivos()->latest()->first()->dtFim->format('d/m/Y')}}
								@else
								{{$tce->dtFim->format('d/m/Y')}}
								@endif
								@else
								{{--  {{date('d/m/Y',strtotime($tce->dtFim))}}  --}}
								{{$tce->dtFim->format('d/m/Y')}}
								@endif</td>
								<td class="text-center"><a href="{{route('tce.show', ['tce' => $tce->id])}}" target="_blank" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Ver TCE">Ver</a></td>
								<!-- ->route('estudante.tce.show', ['estudante' => $formulario['estudante_id'],'tce' => $new->id]); -->
							</tr>
							@empty
							<tr>
								<td colspan="5" class="text-center">Nenhum tce relacionado ao supervisor</td>
							</tr>
							@endforelse

						</tbody>
					</table>
				</div>
				<!-- /.box-body -->
			</div>
	</div>
</div>

@endsection
