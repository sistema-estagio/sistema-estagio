@extends('adminlte::page')
@if(!empty($titulo))
	@section('title_postfix', ' - '.$titulo)
@endif
@section('content_header')
<h1>Supervisores</h1>
<ol class="breadcrumb">
	<li>
		<a href="{{route('home')}}">
			<i class="fa fa-dashboard"></i> Home</a>
	</li>
	<li><a href="{{route('concedente.index')}}"><i class="fa fa-industry"></i> Concedentes</a></li>
		<li><a href="{{route('concedente.show', ['id'=>$concedente->id])}}"><i class="fa fa-industry"></i> {{$concedente->nmRazaoSocial}}</a></li>
	<li class="active">
		<i class="fa fa-graduation-cap"></i> Supervisores</li>
</ol>
@stop
@section('content')
@if(session('success'))
<div class="alert alert-success alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h4>
		<i class="icon fa fa-check"></i> Alerta!</h4>
	{{session('success')}}
</div>
@endif
@if(session('error'))
<div class="alert alert-danger alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h4>
		<i class="icon fa fa-ban"></i> Alerta!</h4>
	{{session('error')}}
</div>
@endif
<a href="{{route('concedente.show.supervisor.create', ['id' => $concedente->id])}}" class="btn btn-success ad-click-event">Novo Supervisor</a>
</br>
</br>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Listagem de Supervisores</h3>
				{{-- <div class="box-tools">
					<form action="{{route('estudante.search')}}" method="POST">
						{!! csrf_field() !!}
						<div class="input-group input-group-sm" style="width: 200px;">
							<input name="key_search" class="form-control pull-right" placeholder="Pesquisar" type="text">

							<div class="input-group-btn">
								<button type="submit" class="btn btn-default">
									<i class="fa fa-search"></i>
								</button>
							</div>
						</div>
					</form>
				</div>--}}

			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<table id="supervisors" class="table table-bordered table-hover">
						<thead>
						<tr>
							<th style="width: 40px;">#</th>
							<th>Nome</th>
							<th>Cargo</th>
							<th style="width: 80px;">Telefone</th>
							<th style="width: 80px;">TCE's</th>
							<th style="width: 80px;" class="no-sort">Cadastro</th>
						</tr>
						</thead>
						<tbody>
						@forelse($supervisors as $supervisor)
						<tr>
							<td>{{$supervisor->id}}</td>
							<td>
								<a href="{{route('supervisor.show', ['supervisor' => $supervisor->id])}}">{{$supervisor->nome}}</a>
								<br><span class="direct-chat-timestamp pull-left"><b>CPF:</b> {{$supervisor->cpf}}</span>
							</td>
							<td>{{$supervisor->cargo}}</td>
							<td>{{$supervisor->telefone}}</td>
							<td>{{$supervisor->contagem($supervisor)}}</td>
							<td>{{$supervisor->created_at->format("d/m/Y")}}</td>
						</tr>
						@empty
						<tr>
							<td colspan="6" align="center">
								<span class="badge bg-red">Sem registros no banco de dados!</span>
							</td>
						</tr>
						@endforelse
					</tbody>
				</table>
			</div>
			<div class="box-footer clearfix">

			</div>
			<!-- /.box-body -->
			{{-- <div class="box-footer clearfix">
				@if(isset($dataForm)) {!! $supervisors->appends($dataForm)->links() !!} @else {!! $supervisors->links() !!} @endif
			</div> --}}
		</div>
		<!-- /.box -->
	</div>
	@section('post-script')
    <script>
        $(function () {
          $('#supervisors').DataTable({
		  "columnDefs": [
		  	{ "orderable": false, "targets": 'no-sort' }
		  ],
          "pageLength": 25,
          /*"order": [[ 1, "asc" ]],*/
          "language": {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_ resultados por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar",
            "oPaginate": {
              "sNext": "Próximo",
              "sPrevious": "Anterior",
              "sFirst": "Primeiro",
              "sLast": "Último"
            },
            "oAria": {
              "sSortAscending": ": Ordenar colunas de forma ascendente",
              "sSortDescending": ": Ordenar colunas de forma descendente"
            }
          }
          })

        })
    </script>
    @endsection
	@stop
