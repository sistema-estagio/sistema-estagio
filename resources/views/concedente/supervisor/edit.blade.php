@extends('adminlte::page')
@if(!empty($titulo))
	@section('title_postfix', ' - '.$titulo)
@endif
@section('content_header')

<h1>Editar Supervisor</h1>
<ol class="breadcrumb">
	<li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="{{route('concedente.index')}}"><i class="fa fa-industry"></i> Concedentes</a></li>
	<li><a href="{{route('concedente.show', ['concedente' => $concedente->id])}}"><i class="fa fa-industry"></i> {{$concedente->nmRazaoSocial}}</a></li>
  <li><a href="{{route('concedente.show.supervisor', ['id' => $concedente->id])}}"<i class="fa fa-file"></i> Supervisores</a></li>
  <li><a href="{{route('supervisor.show', $supervisor->id)}}"<i class="fa fa-file"></i> {{$supervisor->nome}}</a></li>
  <li class="active"><i class="fa fa-pencil"></i> Editar</li>
</ol>
@stop
@section('content')
  @if(session('success'))
  <div class="alert alert-success alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h4><i class="icon fa fa-check"></i> Alerta!</h4>
          {{session('success')}}
  </div>
  @endif

  @if(session('error'))
  <div class="alert alert-warning alert alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h4><i class="icon fa fa-ban"></i> Alerta!</h4>
          {{session('error')}}
  </div>
  @endif

  @if($errors->any())
  <div class="alert alert-warning alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h4><i class="icon fa fa-ban"></i> Alerta!</h4>
      <ul class="alert-warning">
          <h3>:( Whoops, Houve algum erro! </h3>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
      </ul>
  </div>
  @endif
  <form action="{{ route('supervisor.update', $supervisor->id) }}" method="post">
		{{ csrf_field() }}
    {!! method_field('PUT') !!}
    <div class="box box-succes">
        <div class="box-header with-border">
            <h3 class="box-title text-yellow" data-widget="collapse">Edição de Supervisor</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
    <div class="form-group row">
      <div class="col-md-6">
        <label for="">Nome</label>
        <input type="text" name="nome"  class="form-control" placeholder="Digite o nome" value="{{$supervisor->nome}}" required>
      </div>
      <div class="col-md-6">
        <label for="">CPF</label>
        <input type="tel" name="cpf"  class="form-control" id="cpf" placeholder="Digite o cpf" onchange="" value="{{$supervisor->cpf}}" required>
      </div>
    </div>
		<div class="form-group row">
      <div class="col-md-4">
        <label for="">E-mail</label>
        <input type="email" name="email"  class="form-control" placeholder="Digite o e-mail" value="{{$supervisor->email}}">
      </div>
			<div class="col-md-4">
        <label for="">Cargo</label>
        <input type="text" name="cargo"  class="form-control" placeholder="Digite o cargo" value="{{$supervisor->cargo}}" required>
      </div>
      <div class="col-md-4">
        <label for="">Telefone</label>
        <input type="text" name="telefone"  class="form-control" id="telefone" value="{{$supervisor->telefone}}" placeholder="Digite o telefone">
      </div>
    </div>
    <center><button type="submit" class="btn btn-success">Salvar</button></center>
  </div>
  </div>
</form>
@endsection
@section('post-script')
<script>
		$("#cpf").mask("000.000.000-00");
		$("#telefone").mask("(99) 9 9999-9999");
</script>
@endsection
