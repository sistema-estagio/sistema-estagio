@extends('adminlte::page')

@if(!empty($titulo))
@section('title_postfix', ' - '.$titulo)
@endif

@section('content_header')
<h1>Secretaria</h1>
<ol class="breadcrumb">
  <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
  <li><a href="{{route('concedente.index')}}"><i class="fa fa-industry"></i> Concedentes</a></li>
  <li><a href="{{route('concedente.show', ['id'=>$secConcedente->concedente_id])}}"><i class="fa fa-industry"></i> {{$secConcedente->concedente->nmRazaoSocial}}</a></li>
  <li class="active"><i class="fa fa-file"></i> {{$secConcedente->nmSecretaria}}</li>
</ol>
@stop

@section('content')

@if(session('success'))
<div class="alert alert-success alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
  <h4><i class="icon fa fa-check"></i> Alerta!</h4>
  {{session('success')}}
</div>
@endif

@if(session('error'))
<div class="alert alert-danger alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
  <h4><i class="icon fa fa-ban"></i> Alerta!</h4>
  {{session('error')}}
</div>
@endif

<div class="row">
  <div class="col-md-4 col-sm-3 col-xs-12">
    <div class="info-box">
      <span class="info-box-icon bg-aqua"><i class="fa fa-file-text-o"></i></span>
      <div class="info-box-content">
        <span class="info-box-text">TCES Pendentes</span>
        <span class="info-box-number">{{$tcesAtivos->where('dt4Via', null)->count()}}</span>
        <a href="{{route('concedente.secretaria.tces.pendentes.show', [$secConcedente->concedente_id,$secConcedente->id])}}">
            <span class="label label-info">Listar</span>
        </a>
        {{-- <a href="{{route('relatorio.concedente.tce.pdf', ['concedente_id' => $secConcedente->id, 'modelo' => 'sim', 'ordenacao' => 'estudante_id'])}}" target="_blank">Relatório</a> --}}
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
  <div class="col-md-4 col-sm-3 col-xs-12">
    <div class="info-box">
      <span class="info-box-icon bg-green"><i class="fa fa-check-square-o"></i></span>
      <div class="info-box-content">
        <span class="info-box-text">TCES ATIVOS</span>
        <span class="info-box-number">{{$tcesAtivos->where('dt4Via', '<>', null)->count()}}</span>
        <a href="{{route('concedente.secretaria.tces.ativos.show', [$secConcedente->concedente_id,$secConcedente->id])}}">
            <span class="label label-success">Listar</span>
        </a>
        {{-- <a href="{{route('relatorio.concedente.tce.pdf', ['concedente_id' => $secConcedente->id, 'modelo' => 'sim', 'ordenacao' => 'estudante_id'])}}" target="_blank">Relatório</a> --}}
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
  <!-- /.col -->
  <div class="col-md-4 col-sm-3 col-xs-12">
    <div class="info-box">
      <span class="info-box-icon bg-red"><i class="fa fa-times"></i></span>
      <div class="info-box-content">
        <span class="info-box-text">TCES CANCELADOS</span>
        <span class="info-box-number">{{$tcesCancelados->count()}}</span>
        <a href="{{route('concedente.secretaria.tces.cancelados.show', [$secConcedente->concedente_id,$secConcedente->id])}}">
            <span class="label label-danger">Listar</span>
        </a>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
  <!-- /.col -->
  <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
    <div class="info-box">
      <span class="info-box-icon bg-blue"><i class="fa fa-money"></i></span>
      <div class="info-box-content">
        <span class="info-box-text">Financeiro</span>
        <span class="info-box-number">{{$folhas->count()}}</span>
        <a href="{{route('concedente.secretaria.financeiro', ['id'=>$secConcedente->id,'idConcedente'=>$secConcedente->concedente_id])}}" target="_blank">Acessar</a>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
  <!-- /.col -->
  <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
    <div class="info-box">
      <span class="info-box-icon bg-orange"><i class="fa fa-graduation-cap"></i></span>
      <div class="info-box-content">
        <span class="info-box-text">Supervisores</span>
        <span class="info-box-number">{{$supervisors}}</span>
        <a href="{{route('concedente.secretaria.show.supervisor', ['id'=>$secConcedente->id,'idConcedente'=>$secConcedente->concedente_id])}}" target="_blank">Ver</a>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
  <!-- /.col -->
  <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
    <div class="info-box">
      <span class="info-box-icon bg-default"><i class="fa fa-files-o"></i></span>
      <div class="info-box-content">
        <span class="info-box-text">Aditivos</span>
        <span class="info-box-number">{{$aditivos}}</span>
        <a href="{{route('concedente.secretaria.show.aditivos', ['id'=>$secConcedente->id,'idConcedente'=>$secConcedente->concedente_id])}}" target="_blank">Ver</a>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
  <!-- /.col -->
  <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
    <div class="info-box">
      <span class="info-box-icon bg-black"><i class="fa fa-files-o"></i></span>
      <div class="info-box-content">
        <span class="info-box-text">TCE's Vencidos</span>
        <span class="info-box-number">{{$tcesAtivos->where('dtCancelamento', null)->where('dtFim', '<',\Carbon\Carbon::now()->format('Y-m-d'))->count()}}</span>
        <a href="{{route('concedente.secretaria.tces.vencidos.show', ['idConcedente'=>$secConcedente->concedente_id, 'id'=>$secConcedente->id])}}" target="_blank">Ver</a>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">{{$secConcedente->concedente->nmRazaoSocial}}</h3>
        <div class="box-tools pull-right">
          <b>Codigo:</b> {{$secConcedente->id}}
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table class="table table-condensed table-striped">
          <tbody>
            <tr>
              <td colspan="4"><label>Nome:</label> {{$secConcedente->nmSecretaria}}</td>
              <td colspan="2"><label>CNPJ:</label> {{$secConcedente->cdCnpjCpf}}
                @if($secConcedente->flTipo == "PF")
                <td colspan="2"><label>Orgão Regulador:</label> {{$secConcedente->dsOrgaoRegulador}}
                  @endif
                </tr>
                @if($secConcedente->nmResponsavel != NULL)
                <tr>
                  <td colspan="4"><label>Responsável:</label>  {{$secConcedente->nmResponsavel}}</td>
                  <td colspan="4"><label>Cargo:</label>  {{$secConcedente->dsResponsavelCargo}}</td>
                </tr>
                @endif
                @if($secConcedente->nmAdministrador != NULL)
                <tr>
                  <td colspan="4"><label>Nome Administrador:</label>  {{$secConcedente->nmAdministrador}}</td>
                  <td colspan="4"><label>Cargo:</label>  {{$secConcedente->dsAdministradorCargo}}</td>
                </tr>
                @endif

                <tr>
                  <td colspan="8"><label>Endereço: </label> {{$secConcedente->dsEndereco}}, <strong>N°</strong> {{$secConcedente->nnNumero}} </td>
                </tr>
                <tr>
                  <td colspan="2"><label>Bairro:</label>  {{$secConcedente->nmBairro}}</td>
                  <td colspan="2"><label>Cidade:</label>  {{$secConcedente->concedente->cidade->nmCidade}}</td>
                  <td colspan="1"><label>Estado:</label> {{$secConcedente->concedente->cidade->estado->nmEstado}}</td>
                  <td colspan="2"><label>Cep:</label> {{$secConcedente->cdCEP}}</td>
                </tr>
                <tr>
                  <td colspan="2"><label>Telefone:</label> {{$secConcedente->dsFone}}</td>
                  <td colspan="2"><label>Outro:</label>  {{$secConcedente->dsOutroFone}}</td>
                  <td colspan="3"><label>E-mail:</label> {{$secConcedente->dsEmail}}</td>
                </tr>
                @if($secConcedente->dtFimContrato != NULL)
                <tr>
                  <td colspan="2"><label>Data Inicio Convênio:</label>  {{$secConcedente->dtInicioContrato->format('d/m/Y')}}</td>
                  <td colspan="2"><label>Data Fim Convênio:</label>  {{$secConcedente->dtFimContrato->format('d/m/Y')}}</td>
                  <td colspan="2"><label>Valor CI:</label>  R$ {{number_format($secConcedente->vlEstagio, 2, ',', '.')}}</td>
                </tr>
                @endif
              </tbody>
            </table>

          </div>
          <!-- /.box-body -->

          <div class="box-footer text-center">
            <a href="{{route('concedente.secretaria.edit', ['idConcedente' => $secConcedente->concedente_id, 'id' => $secConcedente->id])}}" class="btn btn-warning ad-click-event">Editar</a>
            <a href="{{route('concedente.secretaria.usuario.create', ['idConcedente' => $secConcedente->concedente_id, 'id' => $secConcedente->id])}}" class="btn btn-success ad-click-event">Cadastrar User</a>
          </div>
        </div>

      </div>
    </div>

    @if ($usersConcedente->count())
    <h3><strong>Usuarios da Secretaria</strong>({{$usersConcedente->count()}})</h3>
    @endif
    <!--Aqui parte dos usuarios da secConcedente-->
    @if(session('successUser'))
    <div class="alert alert-success alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h4><i class="icon fa fa-check"></i> Alerta!</h4>
      {{session('successUser')}}
    </div>
    @endif

    @if(session('errorUser'))
    <div class="alert alert-danger alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h4><i class="icon fa fa-ban"></i> Alerta!</h4>
      {{session('errorUser')}}
    </div>
    @endif
    @if($usersConcedente->count() > 0)
    <div class="row">
      <div class="col-md-12">
        <div class="box box-default">
          <div class="box-body">
            <table class="table table-bordered">
              <tbody>
                <tr>
                  <th style="width: 5%;">#</th>
                  <th style="width: 22px;">Nome</th>
                  <th style="width: 90px;">E-Mail</th>
                  <th style="width: 90px;">Cadastrado por</th>
                  <!--<th>Outro</th>-->
                  <th style="width: 90px;">Ações</th>
                </tr>
                @foreach($usersConcedente as $user)

                <tr>
                  <td>{{$loop->iteration}}</td>
                  <td>{{$user->name}}</td>
                  <td>{{$user->email}}</td>
                  <td>{{$user->userCadastro->name}}</td>
                  <!--<td>          </td>-->
                  <td>
                    <a href="{{route('concedente.secretaria.usuario.edit', ['idConcedente' => $secConcedente->concedente_id, 'id' => $secConcedente->id,'user' => $user->id])}}" class="btn btn-warning btn-xs ad-click-event"><i class="fa fa-edit"></i> Editar</a>
                  </td>
                </tr>
                @endforeach

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    @endif

    @section('post-script')

    <script type="text/javascript">
    $(document).ready(function() {
      //maskara nova de telefones
      var SPMaskBehavior = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
      },
      spOptions = {
        onKeyPress: function(val, e, field, options) {
          field.mask(SPMaskBehavior.apply({}, arguments), options);
        }
      };

      $('#dsFone').mask(SPMaskBehavior, spOptions);
      $('#dsOutroFone').mask(SPMaskBehavior, spOptions);
    });

    jQuery(function($){
      $("#cdCEP").mask("99999-999");
      $("#cdCnpjCpf").mask("99.999.999/9999-99");
    });
    /*
    jQuery("#dsFone, #dsOutroFone")
    .mask("(99) 9999-9999?9")
    .focusout(function (event) {
    var target, phone, element;
    target = (event.currentTarget) ? event.currentTarget : event.srcElement;
    phone = target.value.replace(/\D/g, '');
    element = $(target);
    element.unmask();
    if(phone.length > 10) {
    element.mask("(99) 99999-999?9");
  } else {
  element.mask("(99) 9999-9999?9");
}
});
*/
</script>
<!-- Adicionando Javascript -->
<script type="text/javascript" >

$(document).ready(function() {

  function limpa_formulário_cep() {
    // Limpa valores do formulário de cep.
    $("#dsEndereco").val("");
    $("#nmBairro").val("");
  }

  //Quando o campo cep perde o foco.
  $("#cdCEP").blur(function() {

    //Nova variável "cep" somente com dígitos.
    var cep = $(this).val().replace(/\D/g, '');

    //Verifica se campo cep possui valor informado.
    if (cep != "") {

      //Expressão regular para validar o CEP.
      var validacep = /^[0-9]{8}$/;

      //Valida o formato do CEP.
      if(validacep.test(cep)) {

        //Preenche os campos com "..." enquanto consulta webservice.
        $("#dsEndereco").val("...");
        $("#nmBairro").val("...");

        //Consulta o webservice viacep.com.br/
        $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

          if (!("erro" in dados)) {
            //Atualiza os campos com os valores da consulta.
            $("#dsEndereco").val(dados.logradouro);
            $("#nmBairro").val(dados.bairro);
          } //end if.
          else {
            //CEP pesquisado não foi encontrado.
            limpa_formulário_cep();
            alert("CEP não encontrado.");
          }
        });
      } //end if.
      else {
        //cep é inválido.
        limpa_formulário_cep();
        alert("Formato de CEP inválido.");
      }
    } //end if.
    else {
      //cep sem valor, limpa formulário.
      limpa_formulário_cep();
    }
  });
});
</script>
@endsection
@stop
