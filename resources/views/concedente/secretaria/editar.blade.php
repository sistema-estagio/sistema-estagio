@extends('adminlte::page') 
@if(!empty($titulo))
	@section('title_postfix', ' - '.$titulo)
@endif
@section('content_header')

<h1>Edição de Secretaria da Concedente</h1>
<ol class="breadcrumb">
	<li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="{{route('concedente.index')}}"><i class="fa fa-industry"></i> Concedentes</a></li>
	<li><a href="{{route('concedente.show', ['concedente' => $concedente->id])}}"><i class="fa fa-industry"></i> {{$concedente->nmRazaoSocial}}</a></li>
	<li class="active"><i class="fa fa-pencil"></i> Secretaria {{$secConcedente->nmSecretaria}}</li>
</ol>
@stop 
@section('content') 
@if(session('success'))
<div class="alert alert-success alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h4>
		<i class="icon fa fa-check"></i> Alerta!</h4>
	{{session('success')}}
</div>
@endif 
@if(session('error'))
<div class="alert alert-danger alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h4>
		<i class="icon fa fa-ban"></i> Alerta!</h4>
	{{session('error')}}
</div>
@endif
@if(isset($errors)&& $errors->any())
<div class="alert alert-danger alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<ul>
		@foreach($errors->all() as $error)
		<li>{{$error}}</li>
		@endforeach
	</ul>
</div>
@endif
<div class="row">
	<div class="col-md-12">
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">{{$concedente->nmRazaoSocial}}</h3>
			</div>
			<!-- /.box-header -->
			<!-- form start -->
			<form action="{{route('concedente.secretaria.update', ['concedente' => $concedente->id, '' => $secConcedente->id])}}" method="POST">
				{!! csrf_field() !!} {!! method_field('PUT') !!}
				<div class="box-body">
                <div class="row">
					<div class="col-md-8">
						<div class="form-group">
							<label for="nmSecretaria">*Nome da Secretaria</label>
							<input class="form-control" id="nmSecretaria" name="nmSecretaria" value="{{$secConcedente->nmSecretaria}}" placeholder="Nome da Secretaria"
							 type="text">
						</div>
					</div>	
					<div class="col-md-4">
						<div class="form-group">
							<label for="cdcnpjCpf">*CNPJ</label>
							<input class="form-control" id="cdCnpjCpf" name="cdCnpjCpf" value="{{$secConcedente->cdCnpjCpf}}" placeholder="CNPJ" type="text" readonly>
						</div>
					</div>
                </div>
                <div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="nmResponsavel">Nome Responsável</label>
							<input class="form-control" id="nmResponsavel" name="nmResponsavel" value="{{$secConcedente->nmResponsavel}}" placeholder="Nome Responsável"
							 type="text">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="dsResponsavelCargo">Cargo Responsável</label>
							<input class="form-control" id="dsResponsavelCargo" name="dsResponsavelCargo" value="{{$secConcedente->dsResponsavelCargo}}"
							 placeholder="Cargo do Resposável" type="text">
						</div>
					</div>
                </div>
                <div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="nmAdministrador">Nome Administrador</label>
							<input class="form-control" id="nmAdministrador" name="nmAdministrador" value="{{$secConcedente->nmAdministrador}}" placeholder="Nome Administrador"
							 type="text">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="dsAdministradorCargo">Cargo do Administrador</label>
							<input class="form-control" id="dsAdministradorCargo" name="dsAdministradorCargo" value="{{$secConcedente->dsAdministradorCargo}}"
							 placeholder="Cargo do Reitor" type="text">
						</div>
					</div>
                </div>

				<legend>Endereço e Contatos</legend>
                <div class="row">
					<div class="col-md-2">
						<div class="form-group">
							<label for="cdCEP">*CEP</label>
							<input class="form-control" id="cdCEP" name="cdCEP" value="{{$secConcedente->cdCEP}}" placeholder="CEP" type="text">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="dsEndereco">*Endereço</label>
							<input class="form-control" id="dsEndereco" name="dsEndereco" value="{{$secConcedente->dsEndereco}}" placeholder="Endereço"
							 type="text">
						</div>
					</div>
					<div class="col-md-1">
						<div class="form-group">
							<label for="nnNumero">*Numero</label>
							<input class="form-control" id="nnNumero" name="nnNumero" value="{{$secConcedente->nnNumero}}" placeholder="000" type="text">
						</div>
					</div>
                    <div class="col-md-3">
						<div class="form-group">
							<label for="nmBairro">*Bairro</label>
							<input class="form-control" id="nmBairro" name="nmBairro" value="{{$secConcedente->nmBairro}}" placeholder="Bairro" type="text">
						</div>
					</div>
                </div>
                <div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label for="dsComplemento">Complemento</label>
							<input class="form-control" id="dsComplemento" name="dsComplemento" value="{{$secConcedente->dsComplemento}}" placeholder="Complemento"
							 type="text">
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">
							<label for="dsFone">*Telefone</label>
							<input class="form-control" id="dsFone" name="dsFone" value="{{$secConcedente->dsFone}}" placeholder="(88)3535-12345"
							 type="text">
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">
							<label for="dsOutroFone">Outro Telefone</label>
							<input class="form-control" id="dsOutroFone" name="dsOutroFone" value="{{$secConcedente->dsOutroFone}}" placeholder="(88)98801-1234"
							 type="text">
						</div>
					</div>
                </div>
                <div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<label for="dsEmail">*E-mail</label>
							<input class="form-control" id="dsEmail" name="dsEmail" value="{{$secConcedente->dsEmail}}" placeholder="email@email.com.br"
							 type="text">
						</div>
					</div>
					<!--<div class="col-md-4">
						<div class="form-group">
							<label for="unidade_id">Unidade Upa</label>
							<input class="form-control" id="unidade_id" name="unidade_id" value="{{$concedente->unidade_id}}" placeholder="Unidade" type="text">
						</div>
					</div>-->
                </div>
				</div>
				<!-- /.box-body -->

				<div class="box-footer text-center">
					<button type="submit" class="btn btn-success">Atualizar</button>
				</div>
			</form>
		</div>

	</div>
</div>
@section('post-script')
<script type="text/javascript">
	$(document).ready(function() {
		$('#cidade_id').select2();
		//maskara nova de telefones
        var SPMaskBehavior = function (val) {
            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
          },
          spOptions = {
            onKeyPress: function(val, e, field, options) {
                field.mask(SPMaskBehavior.apply({}, arguments), options);
              }
          };
        
          $('#dsFone').mask(SPMaskBehavior, spOptions);
		  $('#dsOutroFone').mask(SPMaskBehavior, spOptions);
	});
jQuery(function($){
		   $("#cdCEP").mask("99999-999");
    });
    /*jQuery("#dsFone, #dsOutroFone")
        .mask("(99) 9999-9999?9")
        .focusout(function (event) {  
            var target, phone, element;  
            target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
            phone = target.value.replace(/\D/g, '');
            element = $(target);  
            element.unmask();  
            if(phone.length > 10) {  
                element.mask("(99) 99999-999?9");  
            } else {  
                element.mask("(99) 9999-9999?9");  
            }  
		});
		*/
</script>
<!-- Adicionando Javascript -->
<script type="text/javascript" >
    
            $(document).ready(function() {
    
                function limpa_formulário_cep() {
                    // Limpa valores do formulário de cep.
                    $("#dsEndereco").val("");
                    $("#nmBairro").val("");
                }
                
                //Quando o campo cep perde o foco.
                $("#cdCEP").blur(function() {
    
                    //Nova variável "cep" somente com dígitos.
                    var cep = $(this).val().replace(/\D/g, '');
    
                    //Verifica se campo cep possui valor informado.
                    if (cep != "") {
    
                        //Expressão regular para validar o CEP.
                        var validacep = /^[0-9]{8}$/;
    
                        //Valida o formato do CEP.
                        if(validacep.test(cep)) {
    
                            //Preenche os campos com "..." enquanto consulta webservice.
                            $("#dsEndereco").val("...");
                            $("#nmBairro").val("...");
    
                            //Consulta o webservice viacep.com.br/
                            $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {
    
                                if (!("erro" in dados)) {
                                    //Atualiza os campos com os valores da consulta.
                                    $("#dsEndereco").val(dados.logradouro);
                                    $("#nmBairro").val(dados.bairro);
                                } //end if.
                                else {
                                    //CEP pesquisado não foi encontrado.
                                    limpa_formulário_cep();
                                    alert("CEP não encontrado.");
                                }
                            });
                        } //end if.
                        else {
                            //cep é inválido.
                            limpa_formulário_cep();
                            alert("Formato de CEP inválido.");
                        }
                    } //end if.
                    else {
                        //cep sem valor, limpa formulário.
                        limpa_formulário_cep();
                    }
                });
            });
        </script>
@endsection 
@stop