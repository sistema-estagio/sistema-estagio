@extends('adminlte::page')
@if(!empty($titulo))
	@section('title_postfix', ' - '.$titulo)
@endif
@section('content_header')

<h1>Cadastrar Supervisor em Secretaria</h1>
<ol class="breadcrumb">
	<li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="{{route('concedente.index')}}"><i class="fa fa-industry"></i> Concedentes</a></li>
	<li><a href="{{route('concedente.show', ['concedente' => $secConcedente->concedente->id])}}"><i class="fa fa-industry"></i> {{$secConcedente->concedente->nmRazaoSocial}}</a></li>
	<li><a href="{{route('concedente.secretaria.show', ['idConcedente' => $secConcedente->concedente->id, 'id' => $secConcedente->id])}}"<i class="fa fa-file"></i> {{$secConcedente->nmSecretaria}}</a></li>
  <li><a href="{{route('concedente.secretaria.show.supervisor', ['idConcedente' => $secConcedente->concedente->id, 'id' => $secConcedente->id])}}"<i class="fa fa-file"></i> Supervisores</a></li>
  <li class="active"><i class="fa fa-pencil"></i> Novo Supervisor</li>
</ol>
@stop
@section('content')
  @if(session('success'))
  <div class="alert alert-success alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h4><i class="icon fa fa-check"></i> Alerta!</h4>
          {{session('success')}}
  </div>
  @endif

  @if(session('error'))
  <div class="alert alert-warning alert alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h4><i class="icon fa fa-ban"></i> Alerta!</h4>
          {{session('error')}}
  </div>
  @endif

  @if($errors->any())
  <div class="alert alert-warning alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h4><i class="icon fa fa-ban"></i> Alerta!</h4>
      <ul class="alert-warning">
          <h3>:( Whoops, Houve algum erro! </h3>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
      </ul>
  </div>
  @endif
  <form action="{{ route('concedente.secretaria.show.supervisor.store', ['idConcedente' => $secConcedente->concedente->id, 'id' => $secConcedente->id]) }}" method="post">
		{{ csrf_field() }}
    <div class="box box-succes">
        <div class="box-header with-border">
            <h3 class="box-title text-yellow" data-widget="collapse">Cadastro de Supervisor</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
    <div class="form-group row">
      <div class="col-md-6">
        <label for="">Nome</label>
        <input type="text" name="nome" value="" class="form-control" placeholder="Digite o nome" required>
      </div>
      <div class="col-md-6">
        <label for="">CPF</label>
        <input type="tel" name="cpf" value="" class="form-control" id="cpf" placeholder="Digite o cpf" onchange="" required>
      </div>
    </div>
		<div class="form-group row">
      <div class="col-md-4">
        <label for="">E-mail</label>
        <input type="email" name="email" value="" class="form-control" placeholder="Digite o e-mail">
      </div>
			<div class="col-md-4">
        <label for="">Cargo</label>
        <input type="text" name="cargo" value="" class="form-control" placeholder="Digite o cargo" required>
      </div>
      <div class="col-md-4">
        <label for="">Telefone</label>
        <input type="text" name="telefone" value="" class="form-control" id="telefone" placeholder="Digite o telefone">
      </div>
    </div>
    <center><button type="submit" class="btn btn-success">Cadastrar</button></center>
  </div>
  </div>
</form>
@endsection
@section('post-script')
<script>
		$("#cpf").mask("000.000.000-00");
		$("#telefone").mask("(99) 9 9999-9999");
</script>
@endsection
