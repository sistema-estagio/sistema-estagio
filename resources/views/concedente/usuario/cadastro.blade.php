@extends('adminlte::page')
@if(!empty($titulo))
    @section('title_postfix', ' - '.$titulo)
@endif
@section('content_header')
<h1>Usuário da Concedente</h1>
<ol class="breadcrumb">
    <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="{{route('concedente.index')}}"><i class="fa fa-industry"></i> Concedentes</a></li>    
    <li><a href="{{route('concedente.show', ['concedente' => $concedente->id])}}"><i class="fa fa-industry"></i> {{$concedente->nmRazaoSocial}}</a></li>
    <li class="active"><i class="fa fa-file"></i> Cadastro de Usuário</li>
</ol>
@stop 
@section('content') 

@if(session('success'))
<div class="alert alert-success alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h4>
		<i class="icon fa fa-check"></i> Alerta!</h4>
	{{session('success')}}
</div>
@endif 

@if(session('error'))
<div class="alert alert-danger alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h4>
		<i class="icon fa fa-ban"></i> Alerta!</h4>
	{{session('error')}}
</div>
@endif

@if($errors->any())
<div class="alert alert-warning alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h4><i class="icon fa fa-ban"></i> Alerta!</h4>
	<ul class="alert-warning">
		<h3>:( Whoops, Houve algum erro! </h3>
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif

<div class="row">
	<div class="col-md-12">
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">Formulário de Cadastro de Usuario</h3>
			</div>
			<!-- /.box-header -->
			<!-- form start -->
			<form action="{{route('concedente.usuario.store')}}" method="POST">
                <input id="concedente_id" name="concedente_id" type="hidden" value="{{$concedente->id}}">                     
				{!! csrf_field() !!}
				<div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">*Nome Usuario</label>
                                <input class="form-control" id="name" name="name" placeholder="Nome do Usuário" type="text" value="{{old('name')}}">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="email">*E-mail de Acesso</label>
                                <input class="form-control" id="email" name="email" placeholder="E-mail de Acesso" type="email" value="{{old('email')}}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="password">*Senha</label>
                                <input class="form-control" required="true" id="password" name="password" type="password">
                            </div>
                        </div><!-- col-md-4-->

                    </div><!-- Row -->

				</div>
				<!-- /.box-body -->
				<div class="box-footer text-center">
					<button type="submit" class="btn btn-primary">Cadastrar</button>
				</div>
			</form>
		</div>

	</div>
</div>

@stop