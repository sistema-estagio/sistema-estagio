@extends('adminlte::page')

@if(!empty($titulo))
@section('title_postfix', ' - '.$titulo)
@endif

@section('content_header')
<h1>Concedente</h1>
<ol class="breadcrumb">
  <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
  <li><a href="{{route('concedente.index')}}"><i class="fa fa-industry"></i> Concedentes</a></li>
  <li class="active"><i class="fa fa-file"></i> {{$concedente->nmRazaoSocial}}</li>
</ol>
@stop

@section('content')

@if(session('success'))
<div class="alert alert-success alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
  <h4><i class="icon fa fa-check"></i> Alerta!</h4>
  {{session('success')}}
</div>
@endif

@if(session('error'))
<div class="alert alert-danger alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
  <h4><i class="icon fa fa-ban"></i> Alerta!</h4>
  {{session('error')}}
</div>
@endif

<div class="row">
  @if($secretariasConcedente->count() == 0)
  <div class="col-md-4 col-sm-3 col-xs-12">
    <div class="info-box">
      <span class="info-box-icon bg-aqua"><i class="fa fa-file-text-o"></i></span>
      <div class="info-box-content">
        <span class="info-box-text">TCES Pendentes</span>
        <span class="info-box-number">{{$tcesAtivos->where('dt4Via', null)->count()}}</span>
        <a href="{{route('concedente.tces.pendentes.show', $concedente->id)}}">
          <span class="label label-info">Listar</span>
        </a>
        {{-- <a href="#">Relatório</a> --}}
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>

  <div class="col-md-4 col-sm-3 col-xs-12">
    <div class="info-box">
      <span class="info-box-icon bg-green"><i class="fa fa-check-square-o"></i></span>
      <div class="info-box-content">
        <span class="info-box-text">TCES ATIVOS</span>
        <span class="info-box-number">{{$tcesAtivos->where('dt4Via', '<>', null)->count()}}</span>
        <a href="{{route('concedente.tces.ativos.show', $concedente->id)}}">
          <span class="label label-success">Listar</span>
        </a>
        {{-- <a href="#">Relatório</a> --}}
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
  <!-- /.col -->

  <div class="col-md-4 col-sm-3 col-xs-12">
    <div class="info-box">
      <span class="info-box-icon bg-red"><i class="fa fa-times"></i></span>
      <div class="info-box-content">
        <span class="info-box-text">TCES CANCELADOS</span>
        <span class="info-box-number">{{$tcesCancelados->count()}}</span>
        <a href="{{ route('concedente.tces.cancelados.show', $concedente->id) }}" class="label label-success">Listar</a>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
  <!-- /.col -->

  <div class="col-md-4 col-sm-3 col-xs-12">
    <div class="info-box">
      <span class="info-box-icon bg-blue"><i class="fa fa-money"></i></span>

      <div class="info-box-content">
        <span class="info-box-text">Folhas de Pagamento</span>
        <span class="info-box-number">{{$concedente->folhasPagamento()->count()}}</span>
        <a href="{{route('concedente.financeiro', $concedente->id)}}" class="ad-click-event">Acessar</a>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
  <!-- /.col -->

  <div class="col-md-4 col-sm-3 col-xs-12">
    <div class="info-box">
      <span class="info-box-icon bg-orange"><i class="fa fa-graduation-cap"></i></span>

      <div class="info-box-content">
        <span class="info-box-text">Supervisores</span>
        <span class="info-box-number">{{$supervisores}}</span>
        <a href="{{route('concedente.show.supervisor', ['concedente_id' => $concedente->id])}}" target="_blank">Ver</a>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
  <!-- /.col -->
  <div class="col-md-4 col-sm-3 col-xs-12">
    <div class="info-box">
      <span class="info-box-icon bg-default"><i class="fa fa-files-o"></i></span>
      <div class="info-box-content">
        <span class="info-box-text">Aditivos</span>
        <span class="info-box-number">{{$aditivos}}</span>
        <a href="{{route('concedente.show.aditivos', ['idConcedente'=>$concedente->id])}}" target="_blank">Ver</a>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
  <div class="col-md-4 col-sm-3 col-xs-12">
    <div class="info-box">
      <span class="info-box-icon bg-black"><i class="fa fa-files-o"></i></span>
      <div class="info-box-content">
        <span class="info-box-text">Vencidos</span>
        <span class="info-box-number">{{\App\Models\Tce::getVencidos($concedente->id, null)->count()}}</span>
        <a href="{{route('concedente.tces.vencidos.show', ['idConcedente'=>$concedente->id])}}" target="_blank">Ver</a>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
  <!-- /.col -->
  @endif
  @if(Auth::user()->unidade_id == null)
  <div class="col-md-4 col-sm-3 col-xs-12">
    <div class="info-box">
      <span class="info-box-icon bg-white"><i class="fa fa-file-text-o"></i></span>
      <div class="info-box-content">
        <span class="info-box-text">Relatório de Pagamento</span>
        <span class="info-box-number">{{$concedente->folhasPagamento()->count()}}</span>
        <a href="javascript:void(0);" onclick="gerarRelatorioFolhas()">
          <span class="label label-default" style="color:grey;">Gerar</span>
        </a>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
  @endif
</div>

@if(Auth::user()->unidade_id == null)
<div class="row">
  <div class="col-md-12">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Histórico</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <div class="btn-group">
            <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-wrench"></i></button>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#"></a></li>
              </ul>
            </div>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-8">
              <p class="text-center">
                <strong>Sales: 1 Jan, 2014 - 30 Jul, 2014</strong>
              </p>

              <div class="chart">
                <!-- Sales Chart Canvas -->
                <canvas id="salesChart" style="height: 55px; width: 330px;" width="660" height="110"></canvas>
              </div>
              <!-- /.chart-responsive -->
            </div>
            <!-- /.col -->
            <div class="col-md-4">
              <p class="text-center">
                <strong>Goal Completion</strong>
              </p>
              {{--
              @php
                $dt = \Carbon\Carbon::now();
              @endphp
              <div class="progress-group">
                <span class="progress-text">TCE's validados</span>
                <span class="progress-number"><b>{{ $tcesAtivos->where('dt4Via', '!=', null)->where('dt4Via_at', '>=', $dt->startOfWeek->format('Y-m-d'))->count() }}</b>/{{ $tcesAtivos->where('dt4Via', '!=', null)->count() }}</span>

                <div class="progress sm">
                  <div class="progress-bar progress-bar-aqua" style="width: 80%"></div>
                </div>
              </div>
              <!-- /.progress-group -->
              <div class="progress-group">
                <span class="progress-text">TCE's cancelados</span>
                <span class="progress-number"><b>{{ $tcesAtivos->where('dt4Via', '!=', null)->where('dt4Via_at', '>=', $dt->startOfWeek->format('Y-m-d'))->count() }}</b>/{{ $tcesAtivos->where('dt4Via', '!=', null)->count() }}</span>

                <div class="progress sm">
                  <div class="progress-bar progress-bar-red" style="width: 80%"></div>
                </div>
              </div>
              --}}
              <!-- /.progress-group -->
              <div class="progress-group">
                <span class="progress-text">TCE's </span>
                <span class="progress-number"><b>480</b>/800</span>

                <div class="progress sm">
                  <div class="progress-bar progress-bar-green" style="width: 80%"></div>
                </div>
              </div>
              <!-- /.progress-group -->
              <div class="progress-group">
                <span class="progress-text"></span>
                <span class="progress-number"><b>250</b>/500</span>

                <div class="progress sm">
                  <div class="progress-bar progress-bar-yellow" style="width: 80%"></div>
                </div>
              </div>
              <!-- /.progress-group -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- ./box-body -->
        <div class="box-footer">
          <div class="row">
            <div class="col-sm-3 col-xs-6">
              <div class="description-block border-right">
                <span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 17%</span>
                <h5 class="description-header">$35,210.43</h5>
                <span class="description-text">TOTAL REVENUE</span>
              </div>
              <!-- /.description-block -->
            </div>
            <!-- /.col -->
            <div class="col-sm-3 col-xs-6">
              <div class="description-block border-right">
                <span class="description-percentage text-yellow"><i class="fa fa-caret-left"></i> 0%</span>
                <h5 class="description-header">$10,390.90</h5>
                <span class="description-text">TOTAL COST</span>
              </div>
              <!-- /.description-block -->
            </div>
            <!-- /.col -->
            <div class="col-sm-3 col-xs-6">
              <div class="description-block border-right">
                <span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 20%</span>
                <h5 class="description-header">$24,813.53</h5>
                <span class="description-text">TOTAL PROFIT</span>
              </div>
              <!-- /.description-block -->
            </div>
            <!-- /.col -->
            <div class="col-sm-3 col-xs-6">
              <div class="description-block">
                <span class="description-percentage text-red"><i class="fa fa-caret-down"></i> 18%</span>
                <h5 class="description-header">1200</h5>
                <span class="description-text">GOAL COMPLETIONS</span>
              </div>
              <!-- /.description-block -->
            </div>
          </div>
          <!-- /.row -->
        </div>
        <!-- /.box-footer -->
      </div>
    </div>
  </div>
  @endif


  <div class="row">
    <div class="col-md-12">
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">{{$concedente->nmRazaoSocial}}</h3>
          <div class="box-tools pull-right">
            <b>Codigo:</b> {{$concedente->id}}
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table class="table table-condensed table-striped">
            <tbody>
              <tr>
                <td colspan="4"><label>Nome Fantasia:</label> {{$concedente->nmFantasia}}</td>
                <td colspan="2"><label>@if($concedente->flTipo == "PF") CPF: @else CNPJ: @endif</label> {{$concedente->cdCnpjCpf}}
                  @if($concedente->flTipo == "PF")
                  <td colspan="2"><label>Orgão Regulador:</label> {{$concedente->dsOrgaoRegulador}}
                    @endif
                  </tr>
                  @if($concedente->nmResponsavel != NULL)
                  <tr>
                    <td colspan="4"><label>Responsável:</label>  {{$concedente->nmResponsavel}}</td>
                    <td colspan="4"><label>Cargo:</label>  {{$concedente->dsResponsavelCargo}}</td>
                  </tr>
                  @endif
                  @if($concedente->nmAdministrador != NULL)
                  <tr>
                    <td colspan="4"><label>Nome Administrador:</label>  {{$concedente->nmAdministrador}}</td>
                    <td colspan="4"><label>Cargo:</label>  {{$concedente->dsAdministradorCargo}}</td>
                  </tr>
                  @endif

                  <tr>
                    <td colspan="8"><label>Endereço: </label> {{$concedente->dsEndereco}}, <strong>N°</strong> {{$concedente->nnNumero}} </td>
                  </tr>
                  <tr>
                    <td colspan="2"><label>Bairro:</label>  {{$concedente->nmBairro}}</td>
                    <td colspan="2"><label>Cidade:</label>  {{$concedente->cidade->nmCidade}}</td>
                    <td colspan="1"><label>Estado:</label> {{$concedente->cidade->estado->nmEstado}}</td>
                    <td colspan="2"><label>Cep:</label> {{$concedente->cdCEP}}</td>
                  </tr>
                  <tr>
                    <td colspan="2"><label>Telefone:</label> {{$concedente->dsFone}}</td>
                    <td colspan="2"><label>Outro:</label>  {{$concedente->dsOutroFone}}</td>
                    <td colspan="3"><label>E-mail:</label> {{$concedente->dsEmail}}</td>
                  </tr>
                  @if($concedente->dtFimContrato != NULL)
                  <tr>
                    <td colspan="2"><label>Data Inicio Convênio:</label>  {{$concedente->dtInicioContrato->format('d/m/Y')}}</td>
                    <td colspan="2"><label>Data Fim Convênio:</label>  {{$concedente->dtFimContrato->format('d/m/Y')}}</td>
                    <td colspan="2"><label>Valor CI:</label>  R$ {{number_format($concedente->vlEstagio, 2, ',', '.')}}</td>
                  </tr>
                  @endif
                  <tr style="background-color: beige;">
                    <td colspan="2">
                      <label>Usuário Concedente pode Cancelar TCE ?</label>
                      @if($concedente->opCancelamento == "SIM")
                      <span class="label label-success label-xs">Sim</span>
                      @else
                      <span class="label label-danger label-xs">Não</span>
                      @endif
                    </td>
                    <td colspan="2">
                      <label>Assinatura UPA no TCE ?</label>
                      @if($concedente->assUpaTCE != "N")
                      <span class="label label-success label-xs">Sim</span>
                      @else
                      <span class="label label-danger label-xs">Não</span>
                      @endif
                    </td>
                    <td colspan="2">
                      <label>Assinatura UPA no Aditivo ?</label>
                      @if($concedente->assUpaADITIVO != NULL)
                      <span class="label label-success label-xs">Sim</span>
                      @else
                      <span class="label label-danger label-xs">Não</span>
                      @endif
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">
              <a href="{{route('concedente.edit', $concedente->id)}}" class="btn btn-warning ad-click-event">Editar</a>
              <a href="{{route('concedente.usuario.create', $concedente->id)}}" class="btn btn-success ad-click-event">Cadastrar User</a>
            </div>
          </div>
        </div>
      </div>
      <h3><strong>Secretarias</strong> ({{$secretariasConcedente->count()}})</h3>
      <!--Formulario de cadastro de secretarias da concedente-->
      @if(session('successSecretaria'))
      <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> Alerta!</h4>
        {{session('successSecretaria')}}
      </div>
      @endif

      @if(session('errorSecretaria'))
      <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Alerta!</h4>
        {{session('errorSecretaria')}}
      </div>
      @endif
      <div class="box box-default collapsed-box">
        <div class="box-header with-border">
          <h3 class="box-title" data-widget="collapse">Cadastrar Secretaria</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse">
              <i class="fa fa-plus"></i>
            </button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body" style="display: none;">
          <form action="{{route('concedente.secretaria.store', ['concedente' => $concedente->id])}}" method="POST" id="formSecretariaStore">
            {!! csrf_field() !!}

            <div class="row">
              <div class="col-md-8">
                <div class="form-group">
                  <label for="nmSecretaria">*Nome da Secretaria</label>
                  <input class="form-control" id="nmSecretaria" name="nmSecretaria" placeholder="Ex.: Secretaria de Saude" type="text">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="cdCnpjCpf">*CNPJ</label>
                  <input class="form-control" id="cdCnpjCpf" name="cdCnpjCpf" placeholder="Documento" type="text">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="nmResponsavel">*Nome Responsável</label>
                  <input class="form-control" id="nmResponsavel" name="nmResponsavel" placeholder="Nome Responsável" type="text">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="dsResponsavelCargo">*Cargo</label>
                  <input class="form-control" id="dsResponsavelCargo" name="dsResponsavelCargo" placeholder="Cargo do Resposável" type="text">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="nmAdministrador">Nome Administrador</label>
                  <input class="form-control" id="nmAdministrador" name="nmAdministrador" placeholder="Nome Administrador" type="text">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="dsAdministradorCargo">Cargo</label>
                  <input class="form-control" id="dsAdministradorCargo" name="dsAdministradorCargo" placeholder="Cargo do Administrador" type="text">
                </div>
              </div>
            </div>
            <legend>Endereço e Contatos</legend>
            <div class="row">
              <div class="col-md-2">
                <div class="form-group">
                  <label for="cdCEP">*CEP</label>
                  <input class="form-control" id="cdCEP" name="cdCEP" placeholder="CEP" type="text">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="dsEndereco">*Endereço</label>
                  <input class="form-control" id="dsEndereco" name="dsEndereco" placeholder="Endereço" type="text">
                </div>
              </div>
              <div class="col-md-1">
                <div class="form-group">
                  <label for="nnNumero">*Numero</label>
                  <input class="form-control" id="nnNumero" name="nnNumero" placeholder="000" type="text">
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label for="nmBairro">*Bairro</label>
                  <input class="form-control" id="nmBairro" name="nmBairro" placeholder="Bairro" type="text">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="dsComplemento">Complemento</label>
                  <input class="form-control" id="dsComplemento" name="dsComplemento" placeholder="Complemento" type="text">
                </div>
              </div>

              <div class="col-md-2">
                <div class="form-group">
                  <label for="dsFone">*Telefone</label>
                  <input class="form-control" id="dsFone" name="dsFone" placeholder="(88)3535-12345" type="text">
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <label for="dsOutroFone">Outro Telefone</label>
                  <input class="form-control" id="dsOutroFone" name="dsOutroFone" placeholder="(88)98801-1234" type="text">
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <label for="dsEmail">*E-mail</label>
                  <input class="form-control" id="dsEmail" name="dsEmail" placeholder="email@email.com.br" type="text">
                </div>
              </div>
            </div>

            <div class="box-footer text-center">
              <button type="submit" class="btn btn-primary" id="btn-submit-secretaria">Cadastrar</button>
            </div>
            <!-- /.row -->
          </form>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box box-default collapsed-box -->
      <!-- Fim formulario de secretaria de concedente -->
      @if ($secretariasConcedente->count())
      <div class="row">
        <div class="col-md-12">
          <div class="box box-default">
            <div class="box-body">
              <div class="table-resposive">
                <table class="table table-bordered">
                  <thead>
                    <col>
                    <colgroup span="1"></colgroup>
                    <colgroup span="1"></colgroup>
                    <colgroup span="3"></colgroup>
                    <colgroup span="1"></colgroup>
                    <tr>
                      <th colspan="2" style="width:60%;"></th>
                      <th colspan="1" style="width:5%;" class="text-center">Supervisor</th>
                      <th colspan="3" style="width:20%;" class="text-center">TCE's</th>
                      <th colspan="1" style="width:10%;"></th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th scope="col">Nome</th>
                      <th scope="col">CNPJ</th>
                      <th class="text-center" scope="col">Quantidade</th>
                      <th class="text-center" scope="col" style="color:blue;">Pendentes</th>
                      <th class="text-center" scope="col" style="color:green;">Ativos</th>
                      <th class="text-center" scope="col" style="color:red;">Cancelados</th>
                      <th scope="col" class="text-center">Ações</th>
                    </tr>
                    @php $supervisores = 0; @endphp
                    @foreach($secretariasConcedente as $secretaria)
                    @php
                    $supervisores += \App\Models\Supervisor::where('secConcedente_id',$secretaria->id)->count();
                    @endphp
                    <tr>
                      <td><a href="{{ route('concedente.secretaria.show', ['idConcedente' => $concedente->id, 'id' => $secretaria->id]) }}">{{$secretaria->nmSecretaria}}</a></td>
                      <td>{{$secretaria->cdCnpjCpf}}</td>
                      <td class="text-center">{{\App\Models\Supervisor::where('secConcedente_id',$secretaria->id)->count()}}</td>
                      <td class="text-center pendentes">{{\App\Models\Tce::where('sec_conc_id',$secretaria->id)->where('dtCancelamento', null)->where('dt4Via',null)->count()}}</td>
                      <td class="text-center ativos">{{\App\Models\Tce::where('sec_conc_id',$secretaria->id)->where('dtCancelamento', null)->whereNotNull('dt4Via')->count()}}</td>
                      <td class="text-center cancelados">{{\App\Models\Tce::where('sec_conc_id',$secretaria->id)->whereNotNull('dtCancelamento')->count()}}</td>
                      <td class="text-center">
                        <a href="{{route('concedente.secretaria.edit', ['idConcedente' => $concedente->id, 'id' => $secretaria->id])}}" class="btn btn-warning btn-xs ad-click-event"><i class="fa fa-edit"></i> Editar</a>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                  <tfoot>
                    <tr>
                      <th>Totais</th>
                      <th></th>
                      <th id="tdSupervisores" class="text-center">{{$supervisores}}</th>
                      <th id="tdPendentes" class="text-center">{{$tcesAtivos->where('dt4Via', null)->count()}}</th>
                      <th id="tdAtivos" class="text-center">{{$tcesAtivos->where('dt4Via', '<>', null)->count()}}</th>
                      <th id="tdCancelado" class="text-center">{{$tcesCancelados->count()}}</th>
                      <th></th>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      @endif
      @if ($secretariasConcedente->count() == 0)
      @if ($usersConcedente->count())
      <h3><strong>Usuarios da Concedete</strong>({{$usersConcedente->count()}})</h3>
      @endif
      <!--Aqui parte dos usuarios da concedente-->
      @if(session('successUser'))
      <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> Alerta!</h4>
        {{session('successUser')}}
      </div>
      @endif

      @if(session('errorUser'))
      <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Alerta!</h4>
        {{session('errorUser')}}
      </div>
      @endif
      @if ($usersConcedente->count())
      <div class="row">
        <div class="col-md-12">
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Usuarios da Concedete</h3>
            </div>
            <div class="box-body">
              <table class="table table-bordered">
                <tbody>
                  <tr>
                    <th style="width: 22px;">Nome</th>
                    <th style="width: 90px;">E-Mail</th>
                    <th style="width: 90px;">Cadastrado por</th>
                    <!--<th>Outro</th>-->
                    <th style="width: 90px;">Ações</th>
                  </tr>
                  @foreach($usersConcedente as $user)

                  <tr>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->userCadastro->name}}</td>
                    <!--<td>          </td>-->
                    <td>
                      <a href="{{route('concedente.usuario.edit', $user->id)}}" class="btn btn-warning btn-xs ad-click-event"><i class="fa fa-edit"></i> Editar</a>
                    </td>
                  </tr>
                  @endforeach

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      @endif
      @endif

      <div class="modal fade" tabindex="-1" id="modelRelatorioFolhas" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Configurar Relatório</h4>
            </div>
            <form action="{{ route('financeiro.relatorio.geral', ['id'=>$concedente->id]) }}" method="post">
              {!! csrf_field() !!}
              <div class="modal-body">
                <div class="form-group row">
                  <div class="col-md-4">
                    <label for="">Referência Mês</label>
                    <select class="form-control" id="referenciaMes" name="referenciaMes" required>
                      <option value="" selected>escolha o mês</option>
                      <option value="01-JANEIRO">JANEIRO</option>
                      <option value="02-FEVEREIRO">FEVEREIRO</option>
                      <option value="03-MARÇO">MARÇO</option>
                      <option value="04-ABRIL">ABRIL</option>
                      <option value="05-MAIO">MAIO</option>
                      <option value="06-JUNHO">JUNHO</option>
                      <option value="07-JULHO">JULHO</option>
                      <option value="08-AGOSTO">AGOSTO</option>
                      <option value="09-SETEMBRO">SETEMBRO</option>
                      <option value="10-OUTUBRO">OUTUBRO</option>
                      <option value="11-NOVEMBRO">NOVEMBRO</option>
                      <option value="12-DEZEMBRO">DEZEMBRO</option>
                    </select>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="referenciaAno">*Ano</label>
                      <input class="form-control" id="referenciaAno" name="referenciaAno" type="tel" value="{{ \Carbon\Carbon::now()->format('Y')}}" required>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <label for="">Forma</label>
                    <select class="form-control" name="modelo" required>
                      <option value="visao">Visão</option>
                      <option value="pdf">PDF</option>
                    </select>
                  </div>

                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                <button type="submit" class="btn btn-primary">Gerar</button>
              </div>
            </form>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
      @section('post-script')

      <script type="text/javascript">

      $('#thPendentes').html($('.pendentes').data('total'));
      $('#thAtivos').html($('.ativos').data('total'));
      $('#thCancelados').html($('.cancelados').data('total'));

      $(document).ready(function() {
        //maskara nova de telefones
        var SPMaskBehavior = function (val) {
          return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
        },
        spOptions = {
          onKeyPress: function(val, e, field, options) {
            field.mask(SPMaskBehavior.apply({}, arguments), options);
          }
        };

        $('#dsFone').mask(SPMaskBehavior, spOptions);
        $('#dsOutroFone').mask(SPMaskBehavior, spOptions);
      });

      jQuery(function($){
        $("#cdCEP").mask("99999-999");
        $("#cdCnpjCpf").mask("99.999.999/9999-99");
      });
      /*
      jQuery("#dsFone, #dsOutroFone")
      .mask("(99) 9999-9999?9")
      .focusout(function (event) {
      var target, phone, element;
      target = (event.currentTarget) ? event.currentTarget : event.srcElement;
      phone = target.value.replace(/\D/g, '');
      element = $(target);
      element.unmask();
      if(phone.length > 10) {
      element.mask("(99) 99999-999?9");
    } else {
    element.mask("(99) 9999-9999?9");
  }
});
*/
</script>
<!-- Adicionando Javascript -->
<script type="text/javascript" >

$(document).ready(function() {

  function limpa_formulário_cep() {
    // Limpa valores do formulário de cep.
    $("#dsEndereco").val("");
    $("#nmBairro").val("");
  }

  //Quando o campo cep perde o foco.
  $("#cdCEP").blur(function() {

    //Nova variável "cep" somente com dígitos.
    var cep = $(this).val().replace(/\D/g, '');

    //Verifica se campo cep possui valor informado.
    if (cep != "") {

      //Expressão regular para validar o CEP.
      var validacep = /^[0-9]{8}$/;

      //Valida o formato do CEP.
      if(validacep.test(cep)) {

        //Preenche os campos com "..." enquanto consulta webservice.
        $("#dsEndereco").val("...");
        $("#nmBairro").val("...");

        //Consulta o webservice viacep.com.br/
        $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

          if (!("erro" in dados)) {
            //Atualiza os campos com os valores da consulta.
            $("#dsEndereco").val(dados.logradouro);
            $("#nmBairro").val(dados.bairro);
          } //end if.
          else {
            //CEP pesquisado não foi encontrado.
            limpa_formulário_cep();
            alert("CEP não encontrado.");
          }
        });
      } //end if.
      else {
        //cep é inválido.
        limpa_formulário_cep();
        alert("Formato de CEP inválido.");
      }
    } //end if.
    else {
      //cep sem valor, limpa formulário.
      limpa_formulário_cep();
    }
  });
});
$('#formSecretariaStore').submit(function(){
  $("#btn-submit-secretaria").attr('disabled','disabled');
});

function gerarRelatorioFolhas(){
  $('#modelRelatorioFolhas').modal('show');
}
</script>
@endsection
@stop
