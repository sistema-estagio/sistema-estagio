@extends('adminlte::page')

@if(!empty($titulo))
    @section('title_postfix', ' - '.$titulo)
@endif

@section('content_header')
    <h1>Financeiro Concedente</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('concedente.index')}}"><i class="fa fa-industry"></i> Concedentes</a></li>
        <li><a href="{{route('concedente.show', ['concedente' => $folha->concedente->id])}}"><i class="fa fa-file"></i> {{$folha->concedente->nmRazaoSocial}}</a></li>
        <li><a href="{{route('concedente.financeiro', ['concedente' => $folha->concedente->id])}}"><i class="fa fa-calculator"></i> Folha de Pagamento</a></li>
        <li><a href="{{route('concedente.financeiro.show', ['concedente' => $folha->concedente->id, 'folha' => $folha->id])}}"><i class="fa fa-calculator"></i> {{$folha->referenciaMes}}/{{$folha->referenciaAno}}</a></li>
        <li class="active"><i class="fa fa-plus"></i> Adicionar</li>        
    </ol>
@stop

@section('content')

{{--  <!--detalhes acima do conteudo-->
<div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-green"><i class="fa fa-file-text-o"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">TCES ATIVOS</span>
          <span class="info-box-number">5</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->
</div>
<!--FIM detalhes acima do conteudo-->  --}}

@if(session('success'))
<div class="alert alert-success alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h4>
		<i class="icon fa fa-check"></i> Alerta!</h4>
	{{session('success')}}
</div>
@endif 

@if(session('error'))
<div class="alert alert-danger alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h4>
		<i class="icon fa fa-ban"></i> Alerta!</h4>
	{{session('error')}}
</div>
@endif

@if($errors->any())
<div class="alert alert-warning alert-dismissible">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-ban"></i> Alerta!</h4>
    <ul class="alert-warning">
        <h3>:( Whoops, Houve algum erro! </h3>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

    <div class="row">
        <div class="col-md-12">
            <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Adicionar TCE {{$tce->estudante->nmEstudante}} na Folha de Pagamento {{$folha->referenciaMes}}/{{$folha->referenciaAno}}</h3>
            </div>
            <!-- /.box-header -->
                    <div class="box-body">
                        <!-- form start -->
                        <form action="{{route('concedente.financeiro.item.store', ['concedente' => $tce->concedente->id])}}" method="POST">
                                <input type="hidden" name="folha_id" value="{{$folha->id}}">
                                <input type="hidden" name="concedente_id" value="{{$tce->concedente->id}}">
                                <input type="hidden" name="secConcedente_id" value="{{$tce->concedente->secConcedenteid}}">
                                <input type="hidden" name="referenciaMes" value="{{$folha->referenciaMes}}">
                                <input type="hidden" name="referenciaAno" value="{{$folha->referenciaAno}}">
                                <input type="hidden" name="tipo_tce" value="{{$tce->relatorio_id}}">
                                <input type="hidden" name="tce_id" value="{{$tce->id}}">
                                {!! csrf_field() !!}
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="dtPagamento">Data Pagamento:</label>
                                                <input class="form-control" id="dtPagamento" name="dtPagamento" placeholder="Data Pagamento" type="text">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="mesesPropRecesso">Meses Prop. Recesso</label>
                                                <input class="form-control" id="mesesPropRecesso" name="mesesPropRecesso" placeholder="Qtd Meses" type="text">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="diasBaseEstagiados">*Dias base Estágio</label>
                                                <input class="form-control" id="diasBaseEstagiados" name="diasBaseEstagiados" placeholder="Qtd Dias" type="text">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for="diasTrabalhados">*Dias Trabahados</label>
                                                    <input class="form-control" id="diasTrabalhados" name="diasTrabalhados" placeholder="Qtd Dias" type="text">
                                                </div>
                                            </div>
                                    </div>
                                    <div class="row">

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="vlAuxilioMensal">*Auxilio Mensal</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-money"></i></span>                                                
                                                    @if($tce->aditivos->count() > 0)
                                                        @if($tce->aditivos()->latest()->first()->vlAuxilioMensal != Null)
                                                            <input class="form-control" id="vlAuxilioMensal" name="vlAuxilioMensal" type="text" value="{{number_format($tce->aditivos()->latest()->first()->vlAuxilioMensal, 2, ',', '.')}}">
                                                        @else
                                                        <input class="form-control" id="vlAuxilioMensal" name="vlAuxilioMensal" type="text" value="{{number_format($tce->vlAuxilioMensal, 2, ',', '.')}}">                                                        
                                                        @endif
                                                    @else
                                                    <input class="form-control" id="vlAuxilioMensal" name="vlAuxilioMensal" type="text" value="{{number_format($tce->vlAuxilioMensal, 2, ',', '.')}}">                                                    
                                                    @endif
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for="vlAuxilioMensal">*Auxilio Transporte/Dia</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-money"></i></span> 
                                                        <input class="form-control" id="vlAuxilioTransporteDia" name="vlAuxilioTransporteDia" type="text" value="{{number_format($tce->concedente->vlTransporteDia, 2, ',', '.')}}">                                                    
                                                    </div>
                                                </div>
                                            </div>
                                        
                                    </div>
                                    <div class="box-footer text-center">
                                        <button type="submit" class="btn btn-success">Adicionar</button>
                                    </div>
                        </form>

                    </div>
                    <!-- /.box-body -->
          </div>    
  
        </div>
    </div>

@section('post-script')

<script type="text/javascript">
jQuery(function($){
        $("#dtPagamento").mask("99/99/9999");
        $('#vlAuxilioMensal').maskMoney({thousands:'.',decimal:','});
    });
    /*
    jQuery("#dsFone, #dsOutroFone")
        .mask("(99) 9999-9999?9")
        .focusout(function (event) {  
            var target, phone, element;  
            target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
            phone = target.value.replace(/\D/g, '');
            element = $(target);  
            element.unmask();  
            if(phone.length > 10) {  
                element.mask("(99) 99999-999?9");  
            } else {  
                element.mask("(99) 9999-9999?9");  
            }  
        });
        */
</script>
@endsection 
@stop