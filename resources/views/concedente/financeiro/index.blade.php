@extends('adminlte::page')

@if(!empty($titulo))
@section('title_postfix', ' - '.$titulo)
@endif

@section('content_header')
<h1>Financeiro Concedente</h1>
<ol class="breadcrumb">
  <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
  <li><a href="{{route('concedente.index')}}"><i class="fa fa-industry"></i> Concedentes</a></li>
  <li><a href="{{route('concedente.show', ['concedente' => $concedente->id])}}"><i class="fa fa-file"></i> {{$concedente->nmRazaoSocial}}</a></li>
  <li class="active"><i class="fa fa-calculator"></i> Folha de Pagamento</li>
</ol>
@stop

@section('content')

@if(session('success'))
<div class="alert alert-success alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
  <h4>
    <i class="icon fa fa-check"></i> Alerta!</h4>
    {{session('success')}}
  </div>
  @endif

  @if(session('error'))
  <div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4>
      <i class="icon fa fa-ban"></i> Alerta!</h4>
      {{session('error')}}
    </div>
    @endif

    @if($errors->any())
    <div class="alert alert-warning alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h4><i class="icon fa fa-ban"></i> Alerta!</h4>
      <ul class="alert-warning">
        <h3>:( Whoops, Houve algum erro! </h3>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
    @endif

    <div class="box box-default collapsed-box">
      <div class="box-header with-border">
        <h3 class="box-title" data-widget="collapse">Criar Folha de Pagamento</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse">
            <i class="fa fa-plus"></i>
          </button>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body" style="display: none;">
        <form action="{{route('concedente.financeiro.store', ['concedente' => $concedente->id])}}" method="POST">
          {!! csrf_field() !!}

          <div class="row">

            <div class="col-md-2">
              <div class="form-group">
                <label for="referenciaMes">*Mês Referência</label>
                <select class="form-control" id="referenciaMes" name="referenciaMes">
                  <option value="" selected>escolha o mês</option>
                  <option value="01-JANEIRO">JANEIRO</option>
                  <option value="02-FEVEREIRO">FEVEREIRO</option>
                  <option value="03-MARÇO">MARÇO</option>
                  <option value="04-ABRIL">ABRIL</option>
                  <option value="05-MAIO">MAIO</option>
                  <option value="06-JUNHO">JUNHO</option>
                  <option value="07-JULHO">JULHO</option>
                  <option value="08-AGOSTO">AGOSTO</option>
                  <option value="09-SETEMBRO">SETEMBRO</option>
                  <option value="10-OUTUBRO">OUTUBRO</option>
                  <option value="11-NOVEMBRO">NOVEMBRO</option>
                  <option value="12-DEZEMBRO">DEZEMBRO</option>
                </select>
              </div>
            </div>

            <div class="col-md-2">
              <div class="form-group">
                <label for="referenciaAno">*Ano</label>
                <input class="form-control" id="referenciaAno" name="referenciaAno" type="text" value="{{ \Carbon\Carbon::now()->format('Y')}}">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="referenciaAno">Deseja que a folha seja criada já alimentada?</label>
                <input type="hidden" name="alimentada" value="não" id="alimentada"><br>
                <button type="button" name="btnYes" class="btn btn-default">Sim</button> <button type="button" name="btnNot" class="btn btn-primary">Não</button>
              </div>
            </div>
            <div id="formDias" style="display:none;">
              <div class="col-md-2">
                <div class="form-group">
                  <label for="diasBaseEstagiados">Dias Base</label>
                  <input type="tel" name="diasBaseEstagiados" id="diasBaseEstagiados" value="30" class="form-control">
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <label for="diasTrabalhados">Dias Trabalhados</label>
                  <input type="tel" name="diasTrabalhados" id="diasTrabalhados" value="22" class="form-control">
                </div>
              </div>
            </div>
          </div>

          <legend>Observação</legend>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <textarea class="form-control" id="obs" name="obs"rows="3" placeholder="escreva se houver alguma observação..."></textarea>
              </div>
            </div>


          </div>

          <div class="box-footer text-center">
            <button type="submit" class="btn btn-success" onclick="return confirm('Deseja Realmente Criar a folha de pagamento?');">Criar</button>
          </div>
          <!-- /.row -->
        </form>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box box-default collapsed-box -->

    <div class="row">
      <div class="col-md-12">
        <div class="box box-default">
          <div class="box-header with-border">
            <h3 class="box-title">Lista das Folhas de Pagamentos</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table class="table table-condensed table-striped">
              <tbody>
                <tr>
                  <th style="width:5%;">Status</th>
                  <th style="width:10%;">Competência</th>
                  <th style="width:25%;">Concedente</th>
                  <th style="width:15%;">Operador</th>
                  <th style="width:15%;">Criado em</th>
                  <th style="width:15%;">Fechada</th>
                </tr>
                @forelse($folhas as $folha)
                <tr>
                  <td>
                    @if($folha->fechado == 'S')
                    <span class="btn btn-success" data-toggle="tooltip" data-placement="right" title="Fechada"><i class="fa fa-check"></i></span>
                    @else
                    <span class="btn btn-primary" data-toggle="tooltip" data-placement="right" title="Aberta"><i class="fa fa-folder-open"></i></span>
                    @endif
                  </td>
                  <td>{{$folha->referenciaMes}}/{{$folha->referenciaAno}}</td>
                  <td><a href="{{route('financeiro.show', ['id'=>$folha->hash])}}">{{$concedente->nmRazaoSocial}}</a></td>
                  <td>{{$folha->userCadastro->name}}</td>
                  <td>{{$folha->created_at}}</td>
                  <td>
                    @if($folha->fechado_at != null)
                    <strong>por:</strong> {{\App\Models\User::find($folha->fechado_user)->name}} | <b>em:</b> {{\Carbon\Carbon::parse($folha->fechado_at)->format('d/m/Y')}}
                    @else
                    Ainda em aberto
                    @endif
                  </td>
                </tr>
                @empty
                <tr>
                  <td colspan="6" align="center">
                    <span class="badge bg-red text-center">Sem registros no banco de dados!</span>
                  </td>
                </tr>
                @endforelse
              </tbody>
            </table>

          </div>
          <!-- /.box-body -->
        </div>

      </div>
    </div>



    @section('post-script')

    <script type="text/javascript">

    $(document).ready(function() {
      //maskara nova de telefones
      var SPMaskBehavior = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
      },
      spOptions = {
        onKeyPress: function(val, e, field, options) {
          field.mask(SPMaskBehavior.apply({}, arguments), options);
        }
      };

      $('#dsFone').mask(SPMaskBehavior, spOptions);
      $('#dsOutroFone').mask(SPMaskBehavior, spOptions);
    });

    jQuery(function($){
      $("#cdCEP").mask("99999-999");
      $("#cdCnpjCpf").mask("99.999.999/9999-99");
    });

    $("button[name=btnYes]").click(function(){
      $('#alimentada').val("sim");
      $("button[name=btnNot]").removeClass('btn-primary').addClass('btn-default');
      $(this).removeClass('btn-default').addClass('btn-primary');
      $("#formDias").show();
    });

    $("button[name=btnNot]").click(function(){
      $('#alimentada').val("não");
      $("button[name=btnYes]").removeClass('btn-primary').addClass('btn-default');
      $(this).removeClass('btn-default').addClass('btn-primary');
      $("#formDias").hide();
    });
    /*
    jQuery("#dsFone, #dsOutroFone")
    .mask("(99) 9999-9999?9")
    .focusout(function (event) {
    var target, phone, element;
    target = (event.currentTarget) ? event.currentTarget : event.srcElement;
    phone = target.value.replace(/\D/g, '');
    element = $(target);
    element.unmask();
    if(phone.length > 10) {
    element.mask("(99) 99999-999?9");
  } else {
  element.mask("(99) 9999-9999?9");
}
});
*/
</script>
@endsection
@stop
