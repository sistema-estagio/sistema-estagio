@extends('adminlte::page') 
@if(!empty($titulo))
	@section('title_postfix', ' - '.$titulo)
@endif
@section('content_header')
<h1>Unidades</h1>
<ol class="breadcrumb">
	<li>
		<a href="{{route('home')}}">
			<i class="fa fa-dashboard"></i> Home</a>
	</li>
	<li class="active">
		<i class="fa fa-building-o"></i> Unidades</li>
</ol>
@stop 
@section('content') 
@if(session('success'))
<div class="alert alert-success alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h4>
		<i class="icon fa fa-check"></i> Alerta!</h4>
	{{session('success')}}
</div>
@endif 
@if(session('error'))
<div class="alert alert-danger alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h4>
		<i class="icon fa fa-ban"></i> Alerta!</h4>
	{{session('error')}}
</div>
@endif
<a href="{{route('unidade.create')}}" class="btn btn-success ad-click-event">Nova Unidade</a>
</br>
</br>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Listagem de Unidades</h3>
				<div class="box-tools">
					<form action="{{route('unidade.search')}}" method="POST">
						{!! csrf_field() !!}
						<div class="input-group input-group-sm" style="width: 200px;">
							<input name="key_search" class="form-control pull-right" placeholder="Pesquisar" type="text">

							<div class="input-group-btn">
								<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<table class="table table-bordered">
					<tbody>
						<tr>
							<th style="width: 40px">#</th>
							<th>Unidade</th>
							<th>Estado</th>
						</tr>
						@forelse($Unidades as $unidade)
						<tr>
							<td>{{$unidade->id}}</td>
							<td><a href="{{route('unidade.show',['unidade'=>$unidade->id])}}"> {{$unidade->nmUnidade}}</a></td>
							<td>
								@foreach($unidade->estado as $estado)
									<span class="label label-primary" style="padding: 7px;">{{$estado->nmEstado}}</span>
								@endforeach
							</td>
						</tr>
						@empty
						<tr>
							<td colspan="3" align="center">
								<span class="badge bg-red">NADA NO BANCO</span>
							</td>
						</tr>
						@endforelse
					</tbody>
				</table>
			</div>
			<div class="box-footer clearfix">

			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
				@if(isset($dataForm)) {!! $Unidades->appends($dataForm)->links() !!} @else {!! $Unidades->links() !!} @endif
			</div>
		</div>
		<!-- /.box -->
	</div>
	@stop