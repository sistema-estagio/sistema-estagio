@extends('adminlte::page')

@if(!empty($titulo))
@section('title_postfix', ' - '.$titulo)
@endif

@section('content_header')
<h1>Unidade</h1>
<ol class="breadcrumb">
	<li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="{{route('unidade.index')}}"><i class="fa fa-building-o"></i> Unidades</a></li>
	<li><a href="{{route('unidade.show', ['unidade' => $Unidade->id])}}"><i class="fa fa-building-o"></i>{{$Unidade->nmUnidade}}</a></li>
    <li class="active"><i class="fa fa-pencil"></i> Edição</li>
</ol>
@stop 
@section('content') 

@if(session('success'))
<div class="alert alert-success alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h4>
		<i class="icon fa fa-check"></i> Alerta!</h4>
	{{session('success')}}
</div>
@endif 

@if(session('error'))
<div class="alert alert-danger alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h4>
		<i class="icon fa fa-ban"></i> Alerta!</h4>
	{{session('error')}}
</div>
@endif

@if(isset($errors)&& $errors->any())
<div class="alert alert-danger alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<ul>
		@foreach($errors->all() as $error)
		<li>{{$error}}</li>
		@endforeach
	</ul>
</div>
@endif

<div class="row">
	<div class="col-md-12">
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">Formulário de Cadastro</h3>
			</div>
			<!-- /.box-header -->
			<!-- form start -->
            <form action="{{route('unidade.update', ['unidade' => $Unidade->id])}}" method="POST">
                {!! csrf_field() !!}
                {!! method_field('PUT') !!}
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">*Nome da Unidade</label>
                                <input class="form-control" required="true" id="nmUnidade" name="nmUnidade" value="{{$Unidade->nmUnidade}}" type="text">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="estado_id">*Estado</label>
                                <select class="form-control" name="estado_id[]" id="estado_id" multiple>
                                    @foreach($Estado as $estado)
                                        <option value="{{$estado->id}}" @if(in_array($estado->id,$Unidade->estado->pluck('id')->toArray())) selected="selected" @endif>{{$estado->nmEstado}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div><!-- col-md-6-->
                    </div>
                </div>
				<!-- /.box-body -->
				<div class="box-footer text-center">
					<button type="submit" class="btn btn-primary">Atualizar</button>
				</div>
			</form>
		</div>

	</div>
</div>
@stop
@section('post-script')
	<script type="text/javascript">
        $(document).ready(function(){
            $('#estado_id').select2();
        });
	</script>
@endsection