@extends('adminlte::page')

@if(!empty($titulo))
    @section('title_postfix', ' - '.$titulo)
@endif


@section('content_header')
    <h1>Unidade</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('unidade.index')}}"><i class="fa fa-building-o"></i> Unidades</a></li>
        <li class="active"><i class="fa fa-file"></i> {{$Unidade->nmUnidade}}</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">{{$Unidade->nmUnidade}}</h3>
                    <div class="box-tools pull-right">
                        <b>Codigo:</b> {{$Unidade->id}}
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-condensed table-striped">
                        <tbody>
                        <tr>
                            <td colspan="4"><label>Nome Unidade:</label> {{$Unidade->nmUnidade}}</td>
                            <td colspan="4">
                                <label>Estado(s):</label>
                                @foreach($Unidade->estado as $estado)
                                    <span class="label label-primary" style="padding: 7px;">{{$estado->nmEstado}}</span>
                                @endforeach
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer text-center">
                    <a href="{{route('unidade.edit', $Unidade->id)}}" class="btn btn-warning ad-click-event">Editar</a>
                </div>            </div>
        </div>
    </div>
    </br>


@stop