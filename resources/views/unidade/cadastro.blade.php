@extends('adminlte::page')
@section('title', 'AdminLTE') 
@section('content_header')
<h1>Unidade</h1>
<ol class="breadcrumb">
	<li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="{{route('unidade.index')}}"><i class="fa fa-industry"></i> Unidade</a></li>
    <li class="active"><i class="fa fa-file"></i> Cadastro de Unidade</li>
</ol>
@stop 
@section('content') 

@if(session('success'))
<div class="alert alert-success alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h4>
		<i class="icon fa fa-check"></i> Alerta!</h4>
	{{session('success')}}
</div>
@endif 

@if(session('error'))
<div class="alert alert-danger alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h4>
		<i class="icon fa fa-ban"></i> Alerta!</h4>
	{{session('error')}}
</div>
@endif

@if(isset($errors)&& $errors->any())
<div class="alert alert-danger alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<ul>
		@foreach($errors->all() as $error)
		<li>{{$error}}</li>
		@endforeach
	</ul>
</div>
@endif

<div class="row">
	<div class="col-md-12">
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">Formulário de Cadastro</h3>
			</div>
			<!-- /.box-header -->
			<!-- form start -->
			<form action="{{route('unidade.store')}}" method="POST">
				{!! csrf_field() !!}
				<div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">*Nome Unidade</label>
                                <input class="form-control" required="true" id="nmUnidade" name="nmUnidade" placeholder="Nome da Unidade" type="text">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="estado_id">*Estado</label>
                                <select class="form-control" name="estado_id[]" id="estado_id" multiple>
                                    <option value="">Escolha um Estado</option>
                                        @foreach($Estado as $estado)
                                        <option value="{{$estado->id}}">{{$estado->nmEstado}}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div><!-- col-md-6-->
                    </div>
				</div>
				<!-- /.box-body -->
				<div class="box-footer text-center">
					<button type="submit" class="btn btn-primary">Cadastrar</button>
				</div>
			</form>
		</div>

	</div>
</div>
@stop

@section('post-script')
	<script type="text/javascript">
        $(document).ready(function(){
            $('#estado_id').select2();
        });
	</script>
@endsection