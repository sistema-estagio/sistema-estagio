@extends('adminlte::page')
@section('title') 
@section('content_header')
    <h1>Sem Permissão</h1>
@stop
@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
                <h3>Desculpe, Você não tem permissão suficiente para essa área, ou sem tempo de login expirou!</h3>
                <h4>Volte para o Painel</h4>
                <br>
                <a href="{{route('login')}}">
                    <button class="btn btn-primary btn-lg">Ir ao Painel</button>
                </a>

            </div>

        </div>
        <!-- /.box -->
    </div>
</div>
@stop