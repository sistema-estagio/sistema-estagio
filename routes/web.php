<?php

//######################################################
//######## INICIO - GRUPO - ROTA ADMINISTRAÇÃO #########
//######################################################
/* FUNÇÂO COLOCAR EM MANUTENÇÃO
Route::get('/admin/down', function()
{
return Artisan::call('down');
});
*/
Route::get('phpinfo', function () {
    return dump(phpinfo());
});
Route::view('/', 'layouts.home');
Route::auth();
Route::get('admin/login', 'Auth\LoginController@showLoginForm');
//Rota de Permissão
Route::get('permissao', function () {
    return view('layouts.permissao');
    //return route('home');
})->name('permissao');

Route::group(['prefix' => '/home', 'middleware' => ['role:admin']], function () {
    //Rota Painel
    Route::get('/', 'HomeController@index')->name('home');

    //Rotas Cursos e Atividades de Curso
    Route::group(['prefix' => '/curso'], function () {
        Route::any('/search', [
      'middleware' => 'permission:Listar Curso',
      'as' => 'curso.search',
      'uses' => 'CursoController@search',
    ]);
        Route::get('/', [
      'middleware' => 'permission:Listar Curso',
      'as' => 'curso.index',
      'uses' => 'CursoController@index',
    ]);
        Route::get('/create', [
      'middleware' => 'permission:Criar Curso',
      'as' => 'curso.create',
      'uses' => 'CursoController@create',
    ]);
        Route::post('/store', [
      'middleware' => 'permission:Criar Curso',
      'as' => 'curso.store',
      'uses' => 'CursoController@store',
    ]);
        Route::get('/show/{id}', [
      'middleware' => 'permission:Ver Curso',
      'as' => 'curso.show',
      'uses' => 'CursoController@show',
    ]);
        Route::get('/edit/{id}', [
      'middleware' => 'permission:Editar Curso',
      'as' => 'curso.edit',
      'uses' => 'CursoController@edit',
    ]);
        Route::put('/update/{id}', [
      'middleware' => 'permission:Editar Curso',
      'as' => 'curso.update',
      'uses' => 'CursoController@update',
    ]);
        //Rota Atividade
        Route::group(['prefix' => '{idCurso}/atividade'], function () {
            Route::get('/', [
        'middleware' => 'permission:Listar Curso',
        'as' => 'curso.atividade.index',
        'uses' => 'AtividadeController@index',
      ]);
            Route::get('/create', [
        'middleware' => 'permission:Criar Curso',
        'as' => 'curso.atividade.create',
        'uses' => 'AtividadeController@create',
      ]);
            Route::post('/store', [
        'middleware' => 'permission:Criar Curso',
        'as' => 'curso.atividade.store',
        'uses' => 'AtividadeController@store',
      ]);
            Route::get('/show/{id}', [
        'middleware' => 'permission:Ver Curso',
        'as' => 'curso.atividade.show',
        'uses' => 'AtividadeController@show',
      ]);
            Route::get('/edit/{}', [
        'middleware' => 'permission:Editar Curso',
        'as' => 'curso.atividade.edit',
        'uses' => 'AtividadeController@edit',
      ]);
            Route::put('/update/{id}', [
        'middleware' => 'permission:Editar Curso',
        'as' => 'curso.atividade.update',
        'uses' => 'AtividadeController@update',
      ]);
        });
        //Route::resource('curso.atividade', 'AtividadeController');
    });

    //Rotas Polos
    Route::group(['prefix' => '/polo'], function () {
        //Route::resource('concedente','ConcedenteController');
        Route::any('polo/search', [
      'middleware' => 'permission:Listar Polo',
      'as' => 'polo.search',
      'uses' => 'PoloController@search',
    ]);
        Route::get('/', [
      'middleware' => 'permission:Listar Polo',
      'as' => 'polo.index',
      'uses' => 'PoloController@index',
    ]);
        Route::get('/create', [
      'middleware' => 'permission:Criar Polo',
      'as' => 'polo.create',
      'uses' => 'PoloController@create',
    ]);
        Route::post('/store', [
      'middleware' => 'permission:Criar Polo',
      'as' => 'polo.store',
      'uses' => 'PoloController@store',
    ]);
        Route::get('/show/{id}', [
      'middleware' => 'permission:Ver Polo',
      'as' => 'polo.show',
      'uses' => 'PoloController@show',
    ]);
        Route::get('/historico/{id}', [
      'middleware' => 'permission:Ver Polo',
      'as' => 'polo.historico',
      'uses' => 'PoloController@historico',
    ]);
        Route::get('/edit/{id}', [
      'middleware' => 'permission:Editar Polo',
      'as' => 'polo.edit',
      'uses' => 'PoloController@edit',
    ]);
        Route::put('/update/{id}', [
      'middleware' => 'permission:Editar Polo',
      'as' => 'polo.update',
      'uses' => 'PoloController@update',
    ]);
        //Relatorios
        Route::get('/relatorio/polo', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.polo', 'uses' => 'RelatorioController@polo']);
        Route::get('/relatorio/polo/pdf', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.polo.pdf', 'uses' => 'RelatorioController@poloPDF']);
    });
    //Rota Polo

    //Rotas Instituições e Cursos de Instituições
    Route::group(['prefix' => '/instituicao'], function () {
        //Route::resource('instituicao', 'InstituicaoController');
        Route::any('instituicao/search', [
      'middleware' => 'permission:Listar Instituicao',
      'as' => 'instituicao.search',
      'uses' => 'InstituicaoController@search',
    ]);
        Route::get('/', [
      'middleware' => 'permission:Listar Instituicao',
      'as' => 'instituicao.index',
      'uses' => 'InstituicaoController@index',
    ]);
        Route::get('/create', [
      'middleware' => 'permission:Criar Instituicao',
      'as' => 'instituicao.create',
      'uses' => 'InstituicaoController@create',
    ]);
        Route::post('/store', [
      'middleware' => 'permission:Criar Instituicao',
      'as' => 'instituicao.store',
      'uses' => 'InstituicaoController@store',
    ]);
        Route::get('/show/{id}', [
      'middleware' => 'permission:Ver Instituicao',
      'as' => 'instituicao.show',
      'uses' => 'InstituicaoController@show',
    ]);
        Route::get('/historico/{id}', [
      'middleware' => 'permission:Ver Instituicao',
      'as' => 'instituicao.historico',
      'uses' => 'InstituicaoController@historico',
    ]);
        Route::get('/editar/{id}', [
      'middleware' => 'permission:Editar Instituicao',
      'as' => 'instituicao.edit',
      'uses' => 'InstituicaoController@edit',
    ]);
        Route::put('/update/{id}', [
      'middleware' => 'permission:Editar Instituicao',
      'as' => 'instituicao.update',
      'uses' => 'InstituicaoController@update',
    ]);

        //teste relatorio instituicao
        //Instituicao
        Route::get('/relatorio/instituicao', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.instituicao', 'uses' => 'RelatorioController@instituicao']);
        Route::get('/relatorio/instituicao/pdf', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.instituicao.pdf', 'uses' => 'RelatorioController@instituicaoPDF']);
        //Curso
        Route::get('/relatorio/curso', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.curso', 'uses' => 'RelatorioController@curso']);
        Route::get('/relatorio/curso/pdf', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.curso.pdf', 'uses' => 'RelatorioController@cursoPDF']);
        //Curso Instituicao
        Route::get('/relatorio/cursoinstituicao', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.curso.instituicao', 'uses' => 'RelatorioController@cursoInstituicao']);
        Route::get('/relatorio/cursoinstituicao/pdf', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.curso.instituicao.pdf', 'uses' => 'RelatorioController@cursoInstituicaoPDF']);
        //
        //Rota Gerar Convenio Insituicao
        Route::post('/convenio', [
      'middleware' => 'permission:Editar Instituicao',
      'as' => 'instituicao.convenio',
      'uses' => 'ConvenioIeController@store',
    ]);

        Route::get('/{instituicao}/convenio/{convenio}', [
      'middleware' => 'permission:Editar Instituicao',
      'as' => 'instituicao.convenio.show',
      'uses' => 'ConvenioIeController@show',
    ]);

        //Rota /instituicao/{idIntituicao}/Curso
        Route::group(['prefix' => '{idIntituicao}/curso'], function () {
            Route::get('/', [
        'middleware' => 'permission:Listar Instituicao',
        'as' => 'instituicao.curso.index',
        'uses' => 'CursoInstituicaoController@index',
      ]);
            Route::get('/create', [
        'middleware' => 'permission:Criar Instituicao',
        'as' => 'instituicao.curso.create',
        'uses' => 'CursoInstituicaoController@create',
      ]);
            Route::post('/store', [
        'middleware' => 'permission:Criar Instituicao',
        'as' => 'instituicao.curso.store',
        'uses' => 'CursoInstituicaoController@store',
      ]);
            Route::get('/show/{id}', [
        'middleware' => 'permission:Ver Instituicao',
        'as' => 'instituicao.curso.show',
        'uses' => 'CursoInstituicaoController@show',
      ]);
            Route::get('/edit/{id}', [
        'middleware' => 'permission:Editar Instituicao',
        'as' => 'instituicao.curso.edit',
        'uses' => 'CursoInstituicaoController@edit',
      ]);
            Route::put('/update/{id}', [
        'middleware' => 'permission:Editar Instituicao',
        'as' => 'instituicao.curso.update',
        'uses' => 'CursoInstituicaoController@update',
      ]);
            //Route::resource('instituicao.curso', 'CursoInstituicaoController');
        }); //fim /Instituicao/{id}/cursp
    }); // fim /Instituicao

  //Rotas Concedentes
    Route::group(['prefix' => '/concedente'], function () {
        //Route::resource('concedente','ConcedenteController');
        Route::any('concedente/search', [
      'middleware' => 'permission:Listar Concedente',
      'as' => 'concedente.search',
      'uses' => 'ConcedenteController@search',
    ]);
        Route::get('/', [
      'middleware' => 'permission:Listar Concedente',
      'as' => 'concedente.index',
      'uses' => 'ConcedenteController@index',
    ]);
        Route::get('/create', [
      'middleware' => 'permission:Criar Concedente',
      'as' => 'concedente.create',
      'uses' => 'ConcedenteController@create',
    ]);
        Route::post('/store', [
      'middleware' => 'permission:Criar Concedente',
      'as' => 'concedente.store',
      'uses' => 'ConcedenteController@store',
    ]);
        Route::get('/show/{id}', [
      'middleware' => 'permission:Ver Concedente',
      'as' => 'concedente.show',
      'uses' => 'ConcedenteController@show',
    ]);
        //Listar Tces da concedente
        //Ativos
        Route::get('/tcesativos/{id}', [
      'middleware' => 'permission:Listar Tce',
      'as' => 'concedente.tces.ativos.show',
      'uses' => 'ConcedenteController@showTcesAtivos',
    ]);
        //Pendentes
        Route::get('/tcespendentes/{id}', [
      'middleware' => 'permission:Listar Tce',
      'as' => 'concedente.tces.pendentes.show',
      'uses' => 'ConcedenteController@showTcesPendentes',
    ]);
        //Cancelados
        Route::get('/tcescancelados/{id}', [
      'middleware' => 'permission:Listar Tce',
      'as' => 'concedente.tces.cancelados.show',
      'uses' => 'ConcedenteController@showTcesCancelados',
    ]);
        //Cancelados
        Route::post('/tcescancelados/{id}/pdf', [
      'middleware' => 'permission:Listar Tce',
      'as' => 'concedente.tces.cancelados.pdf',
      'uses' => 'ConcedenteController@downloadRelatorioPDF',
    ]);

        Route::get('/tcesvencidos/{id}', [
      'middleware' => 'permission:Listar Tce',
      'as' => 'concedente.tces.vencidos.show',
      'uses' => 'ConcedenteController@showTcesVencidos',
    ]);

        Route::post('tces/vencidos/cancelar/lote', [
      'as' => 'tces.vencidos.cancelar.submit',
      'uses' => 'API\TceController@cancelLote',
    ]);

        Route::get('/show/{idConcedente}/aditivos', [
      'middleware' => 'permission:Editar Concedente',
      'as' => 'concedente.show.aditivos',
      'uses' => 'AditivoController@showConcedenteSecretaria',
    ]);

        Route::get('/show/{id}/supervisores', [
      'middleware' => 'permission:Ver Concedente',
      'as' => 'concedente.show.supervisor',
      'uses' => 'SupervisorController@index',
    ]);

        Route::get('/show/{id}/supervisores/create', [
      'middleware' => 'permission:Ver Concedente',
      'as' => 'concedente.show.supervisor.create',
      'uses' => 'SupervisorController@create',
    ]);
        Route::post('/show/{id}/supervisores/store', [
      'middleware' => 'permission:Ver Concedente',
      'as' => 'concedente.show.supervisor.store',
      'uses' => 'SupervisorController@store',
    ]);
        Route::get('/edit/{id}', [
      'middleware' => 'permission:Editar Concedente',
      'as' => 'concedente.edit',
      'uses' => 'ConcedenteController@edit',
    ]);
        Route::put('/update/{id}', [
      'middleware' => 'permission:Editar Concedente',
      'as' => 'concedente.update',
      'uses' => 'ConcedenteController@update',
    ]);
        //Administrar Usuario da Concedente
        Route::get('usuario/create/{id}', [
      'middleware' => 'permission:Criar Concedente',
      'as' => 'concedente.usuario.create',
      'uses' => 'UsuarioConcedenteController@create',
    ]);
        Route::post('usuario/store', [
      'middleware' => 'permission:Criar Concedente',
      'as' => 'concedente.usuario.store',
      'uses' => 'UsuarioConcedenteController@store',
    ]);
        Route::get('usuario/edit/{id}', [
      'middleware' => 'permission:Editar Concedente',
      'as' => 'concedente.usuario.edit',
      'uses' => 'UsuarioConcedenteController@edit',
    ]);
        Route::put('usuario/update/{id}', [
      'middleware' => 'permission:Editar Concedente',
      'as' => 'concedente.usuario.update',
      'uses' => 'UsuarioConcedenteController@update',
    ]);
        //Fim Administrar Usuario da Concedente

        //Secretaria da Concedente
        Route::group(['prefix' => '{idConcedente}/secretaria'], function () {
            //Listar Tces da secretaria concedente
            //Ativos
            Route::get('/tcesativos/{id}', [
        'middleware' => 'permission:Listar Tce',
        'as' => 'concedente.secretaria.tces.ativos.show',
        'uses' => 'SecConcedenteController@showTcesAtivos',
      ]);
            //Pendentes
            Route::get('/tcespendentes/{id}', [
        'middleware' => 'permission:Listar Tce',
        'as' => 'concedente.secretaria.tces.pendentes.show',
        'uses' => 'SecConcedenteController@showTcesPendentes',
      ]);
            //Cancelados
            Route::get('/tcescancelados/{id}', [
        'middleware' => 'permission:Listar Tce',
        'as' => 'concedente.secretaria.tces.cancelados.show',
        'uses' => 'SecConcedenteController@showTcesCancelados',
      ]);

            //vencidos
            Route::get('/tcesvencidos/{id}', [
          'middleware' => 'permission:Listar Tce',
          'as' => 'concedente.secretaria.tces.vencidos.show',
          'uses' => 'SecConcedenteController@showTcesVencidos',
        ]);

            Route::post('/tcescancelados/pdf/{id}', [
        'middleware' => 'permission:Listar Tce',
        'as' => 'concedente.secretaria.tces.cancelados.pdf',
        'uses' => 'SecConcedenteController@downloadRelatorioPDF',
      ]);

            Route::post('/store', [
        'middleware' => 'permission:Editar Concedente',
        'as' => 'concedente.secretaria.store',
        'uses' => 'SecConcedenteController@store',
      ]);
            Route::get('/show/{id}', [
        'middleware' => 'permission:Editar Concedente',
        'as' => 'concedente.secretaria.show',
        'uses' => 'SecConcedenteController@show',
      ]);
            Route::get('/show/{id}/aditivos', [
        'middleware' => 'permission:Editar Concedente',
        'as' => 'concedente.secretaria.show.aditivos',
        'uses' => 'AditivoController@showConcedenteSecretaria',
      ]);
            Route::get('/show/{id}/supervisores', [
        'middleware' => 'permission:Editar Concedente',
        'as' => 'concedente.secretaria.show.supervisor',
        'uses' => 'SupervisorController@index',
      ]);
            Route::get('/show/{id}/supervisores/create', [
        'middleware' => 'permission:Editar Concedente',
        'as' => 'concedente.secretaria.show.supervisor.create',
        'uses' => 'SupervisorController@create',
      ]);
            Route::post('/show/{id}/supervisores/store', [
        'middleware' => 'permission:Editar Concedente',
        'as' => 'concedente.secretaria.show.supervisor.store',
        'uses' => 'SupervisorController@store',
      ]);
            Route::get('/edit/{id}', [
        'middleware' => 'permission:Editar Concedente',
        'as' => 'concedente.secretaria.edit',
        'uses' => 'SecConcedenteController@edit',
      ]);
            Route::get('/{id}/usuario/criar', [
        'middleware' => 'permission:Editar Concedente',
        'as' => 'concedente.secretaria.usuario.create',
        'uses' => 'UsuarioSecretariaController@create',
      ]);
            Route::post('/{id}/usuario/store', [
        'middleware' => 'permission:Editar Concedente',
        'as' => 'concedente.secretaria.usuario.store',
        'uses' => 'UsuarioSecretariaController@store',
      ]);
            Route::get('/{id}/usuario/{user}/edit', [
        'middleware' => 'permission:Editar Concedente',
        'as' => 'concedente.secretaria.usuario.edit',
        'uses' => 'UsuarioSecretariaController@edit',
      ]);
            Route::put('/{id}/usuario/{user}/update', [
        'middleware' => 'permission:Editar Concedente',
        'as' => 'concedente.secretaria.usuario.update',
        'uses' => 'UsuarioSecretariaController@update',
      ]);
            Route::put('/update/{id}', [
        'middleware' => 'permission:Editar Concedente',
        'as' => 'concedente.secretaria.update',
        'uses' => 'SecConcedenteController@update',
      ]);

            Route::get('/financeiro/{id}', [
        'middleware' => 'permission:Editar Concedente',
        'as' => 'concedente.secretaria.financeiro',
        'uses' => 'SecConcedenteController@financeiroIndex',
      ]);

            Route::post('/financeiro/{id}/store', [
        'middleware' => 'permission:Editar Concedente',
        'as' => 'concedente.secretaria.financeiro.store',
        'uses' => 'SecConcedenteController@financeiroStore',
      ]);

            Route::get('/financeiro/{id}/folha/{folha}', [
        'middleware' => 'permission:Editar Concedente',
        'as' => 'concedente.secretaria.financeiro.show',
        'uses' => 'SecConcedenteController@financeiroVerFolha',
      ]);

            Route::get('/financeiro/{id}/folha/{folha}/create/{tce}', [
        'middleware' => 'permission:Editar Concedente',
        'as' => 'concedente.secretaria.financeiro.item.create',
        'uses' => 'SecConcedenteController@financeiroCriarItem',
      ]);
            Route::post('/financeiro/{id}/folha/{folha}/store', [
        'middleware' => 'permission:Editar Concedente',
        'as' => 'concedente.secretaria.financeiro.item.store',
        'uses' => 'SecConcedenteController@financeiroItemStore',
      ]);

            Route::post('/financeiro/{id}/folha/{folha}/addItem', [
        'middleware' => 'permission:Editar Concedente',
        'as' => 'concedente.secretaria.financeiro.item.add',
        'uses' => 'API\SecConcedenteController@itemsStore',
      ]);

            Route::post('/financeiro/{id}/folha/{folha}/removerItems', [
        'middleware' => 'permission:Editar Concedente',
        'as' => 'concedente.secretaria.financeiro.item.removeall',
        'uses' => 'API\SecConcedenteController@itemsRemove',
      ]);

            Route::get('/financeiro/{id}/folha/{folha}/edit/{idItem}', [
        'middleware' => 'permission:Editar Concedente',
        'as' => 'concedente.secretaria.financeiro.item.editar.form',
        'uses' => 'SecConcedenteController@financeiroEditarItem',
      ]);

            Route::delete('/financeiro/{id}/folha/{folha}/delete', [
        'middleware' => 'permission:Editar Concedente',
        'as' => 'concedente.secretaria.financeiro.item.delete',
        'uses' => 'SecConcedenteController@financeiroDeletarItem',
      ]);

            Route::put('/financeiro/{id}/folha/{folha}/item/{idItem}/update', [
        'middleware' => 'permission:Editar Concedente',
        'as' => 'concedente.secretaria.financeiro.item.update',
        'uses' => 'SecConcedenteController@financeiroUpdateItem',
      ]);

            Route::get('/show/{id}/relatorio/{folha}', [
        'middleware' => 'permission:Editar Concedente',
        'as' => 'concedente.secretaria.financeiro.relatorio',
        'uses' => 'SecConcedenteController@financeiroVerFolhaPDF', ]);

            Route::get('/show/{id}/relatorio/{folha}/recibo', [
          'middleware' => 'permission:Editar Concedente',
          'as' => 'concedente.secretaria.financeiro.relatorio.recibo',
          'uses' => 'SecConcedenteController@financeiroVerFolhaPDF', ]);

            Route::get('/show/{id}/relatorio/{folha}/fatura', [
            'middleware' => 'permission:Editar Concedente',
            'as' => 'concedente.secretaria.financeiro.relatorio.fatura',
            'uses' => 'SecConcedenteController@financeiroVerFolhaPDF', ]);
        });
        //Fim Secretaria da Concedente
        Route::group(['prefix' => 'financeiro'], function () {
            //
            //Mudança URL
            //
            Route::post('/folha/{id}/store', [
              'middleware' => 'permission:Editar Concedente',
              'as' => 'financeiro.item.store',
              'uses' => 'FinanceiroController@store',
            ]);

            Route::post('/folha/{id}/lote/store', [
              'middleware' => 'permission:Editar Concedente',
              'as' => 'financeiro.lote.item.store',
              'uses' => 'API\FinanceiroController@itemsStore',
            ]);

            Route::post('/folha/{id}/removeItems', [
              'middleware' => 'permission:Editar Concedente',
              'as' => 'financeiro.lote.items.remove',
              'uses' => 'API\FinanceiroController@itemsRemove',
            ]);

            Route::post('/folha/{id}/edit', [
              'middleware' => 'permission:Editar Concedente',
              'as' => 'financeiro.item.edit',
              'uses' => 'FinanceiroController@store',
            ]);

            Route::get('folha/{id}', [
              'middleware' => 'permission:Editar Concedente',
              'as' => 'financeiro.show',
              'uses' => 'FinanceiroController@show',
            ]);

            Route::put('folha/{id}/atualizar', [
              'middleware' => 'permission:Editar Concedente',
              'as' => 'financeiro.folha.observacoes.atualizar',
              'uses' => 'FinanceiroController@updateFolha',
            ]);

            Route::post('folha/{id}/relatorio/geral', [
              'middleware' => 'permission:Editar Concedente',
              'as' => 'financeiro.relatorio.geral',
              'uses' => 'FinanceiroController@relatorioGeral',
            ]);

            Route::delete('folha/{id}/delete', [
              'middleware' => 'permission:Editar Concedente',
              'as' => 'financeiro.delete.item',
              'uses' => 'FinanceiroController@financeiroDeletarItem',
            ]);

            Route::get('folha/{id}/fechar', [
              'middleware' => 'permission:Editar Concedente',
              'as' => 'financeiro.folha.fechar',
              'uses' => 'FinanceiroController@fecharFolha',
            ]);

            //relatorios financeiros
            Route::post('/folha/relatorio', [
              'middleware' => 'permission:Editar Concedente',
              'as' => 'financeiro.relatorio',
              'uses' => 'FinanceiroController@folhaPDF', ]);
            //
              //Mudança URL
              //
        });
        //Inicio Finaneiro Concedente
        Route::group(['prefix' => '{idConcedente}/financeiro'], function () {
            Route::get('/', [
                'middleware' => 'permission:Editar Concedente',
                'as' => 'concedente.financeiro',
                'uses' => 'ConcedenteController@financeiroIndex',
              ]);

            Route::post('/store', [
                'middleware' => 'permission:Editar Concedente',
                'as' => 'concedente.financeiro.store',
                'uses' => 'ConcedenteController@financeiroCriarFolha',
              ]);

            Route::get('/folha/{id}', [
                'middleware' => 'permission:Editar Concedente',
                'as' => 'concedente.financeiro.show',
                'uses' => 'ConcedenteController@financeiroVerFolha',
              ]);

            Route::get('/folha/{id}/create/{tce}', [
                'middleware' => 'permission:Editar Concedente',
                'as' => 'concedente.financeiro.item.create',
                'uses' => 'ConcedenteController@financeiroCriarItem',
              ]);
            Route::post('/folha/store', [
                'middleware' => 'permission:Editar Concedente',
                'as' => 'concedente.financeiro.item.store',
                'uses' => 'ConcedenteController@financeiroItemStore',
              ]);

            Route::post('/folha/addItem', [
                'middleware' => 'permission:Editar Concedente',
                'as' => 'concedente.financeiro.item.add',
                'uses' => 'API\ConcedenteController@itemsStore',
              ]);

            Route::post('/folha/removerItems', [
                'middleware' => 'permission:Editar Concedente',
                'as' => 'concedente.financeiro.item.remove',
                'uses' => 'API\ConcedenteController@itemsRemove',
              ]);

            Route::get('folha/{folha}/edit/{id}', [
                'middleware' => 'permission:Editar Concedente',
                'as' => 'concedente.financeiro.item.editar.form',
                'uses' => 'ConcedenteController@financeiroEditarItem',
              ]);

            Route::delete('folha/{folha}/delete', [
                'middleware' => 'permission:Editar Concedente',
                'as' => 'concedente.financeiro.item.delete',
                'uses' => 'ConcedenteController@financeiroDeletarItem',
              ]);

            Route::put('folha/{folha}/item/{id}/update', [
                'middleware' => 'permission:Editar Concedente',
                'as' => 'concedente.financeiro.item.update',
                'uses' => 'ConcedenteController@financeiroUpdateItem',
              ]);

            //relatorios financeiros
            Route::get('/show/{id}/relatorio', [
                'middleware' => 'permission:Editar Concedente',
                'as' => 'concedente.financeiro.relatorio',
                'uses' => 'ConcedenteController@financeiroVerFolhaPDF', ]);

            Route::get('/show/{id}/relatorio/recibo', [
                  'middleware' => 'permission:Editar Concedente',
                  'as' => 'concedente.financeiro.relatorio.recibo',
                  'uses' => 'FinanceiroController@concedenteReciboPDF', ]);

            Route::get('/show/{id}/relatorio/fatura', [
                    'middleware' => 'permission:Editar Concedente',
                    'as' => 'concedente.financeiro.relatorio.fatura',
                    'uses' => 'FinanceiroController@concedenteFaturaPDF', ]);

            //Route::get('/show/{id}/relatoriopagamento', ['middleware' => 'permission:Editar Concedente', 'as' => 'concedente.financeiro.relatorio.pagamento', 'uses' => 'ConcedenteController@financeiroVerFolhaPgPDF']);
        }); //fim /{id}/financeiro

                  //Fim Financeiro Concedente

        //Relatorios
        Route::get('/relatorio/concedente', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.concedente', 'uses' => 'RelatorioController@concedente']);
        Route::get('/relatorio/concedente/pdf', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.concedente.pdf', 'uses' => 'RelatorioController@concedentePDF']);
    });

    //Rotas Estudantes
    Route::group(['prefix' => '/estudante'], function () {
        //Route::resource('estudante','EstudanteController');
        Route::any('estudante/search', [
                    'middleware' => 'permission:Listar Estudante',
                    'as' => 'estudante.search',
                    'uses' => 'EstudanteController@search',
                  ]);
        Route::get('/', [
                    'middleware' => 'permission:Listar Estudante',
                    'as' => 'estudante.index',
                    'uses' => 'EstudanteController@index',
                  ]);
        Route::get('/create', [
                    'middleware' => 'permission:Criar Estudante',
                    'as' => 'estudante.create',
                    'uses' => 'EstudanteController@create',
                  ]);
        Route::get('/indexJson', [
                    'middleware' => 'permission:Listar Estudante',
                    'as' => 'estudante.index.json',
                    'uses' => 'EstudanteController@indexJson',
                  ]);
        Route::post('/store', [
                    'middleware' => 'permission:Criar Estudante',
                    'as' => 'estudante.store',
                    'uses' => 'EstudanteController@store',
                  ]);
        Route::get('/show/{id}', [
                    'middleware' => 'permission:Ver Estudante',
                    'as' => 'estudante.show',
                    'uses' => 'EstudanteController@show',
                  ]);
        Route::get('/edit/{id}', [
                    'middleware' => 'permission:Editar Estudante',
                    'as' => 'estudante.edit',
                    'uses' => 'EstudanteController@edit',
                  ]);
        Route::put('/update/{id}', [
                    'middleware' => 'permission:Editar Estudante',
                    'as' => 'estudante.update',
                    'uses' => 'EstudanteController@update',
                  ]);
        //teste copiar estudante pra outra tabela
        Route::get('{id}/copiar', [
                    'middleware' => 'permission:Editar Estudante',
                    'as' => 'estudante.copiar',
                    'uses' => 'EstudanteController@copiar',
                  ]);
        //teste copiar estudante pra outra tabela
        Route::get('/historico/{id}', [
                    'middleware' => 'permission:Ver Estudante',
                    'as' => 'estudante.historico',
                    'uses' => 'EstudanteController@historico',
                  ]);

        //Rota Tces dentro de Estudante
        Route::group(['prefix' => '{idEstudante}/tce'], function () {
            //Route::resource('estudante.tce', 'TceController');
            Route::get('/', [
                      'middleware' => 'permission:Listar Tce',
                      'as' => 'estudante.tce.index',
                      'uses' => 'TceController@index',
                    ]);
            Route::get('/create', [
                      'middleware' => 'permission:Criar Tce',
                      'as' => 'estudante.tce.create',
                      'uses' => 'TceController@create',
                    ]);
            Route::post('/store', [
                      'middleware' => 'permission:Criar Tce',
                      'as' => 'estudante.tce.store',
                      'uses' => 'TceController@store',
                    ]);
            Route::get('/show/{id}', [
                      'middleware' => 'permission:Ver Tce',
                      'as' => 'estudante.tce.show',
                      'uses' => 'TceController@show',
                    ]);
            Route::get('/edit/{id}', [
                      'middleware' => 'permission:Editar Tce',
                      'as' => 'estudante.tce.edit',
                      'uses' => 'TceController@edit',
                    ]);
            Route::put('/update/{id}', [
                      'middleware' => 'permission:Editar Tce',
                      'as' => 'estudante.tce.update',
                      'uses' => 'TceController@update',
                    ]);
        });
        //Estudante
        Route::get('/relatorio/estudante', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.estudante', 'uses' => 'RelatorioController@estudante']);
        Route::get('/relatorio/estudante/pdf', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.estudante.pdf', 'uses' => 'RelatorioController@estudantePDF']);
        //Estudante Instituicao
        Route::get('/relatorio/estudanteinstituicao', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.estudante.instituicao', 'uses' => 'RelatorioController@estudanteInstituicao']);
        Route::get('/relatorio/estudanteinstituicao/pdf', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.estudante.instituicao.pdf', 'uses' => 'RelatorioController@estudanteInstituicaoPDF']);
        //Estudante Concedente
        Route::get('/relatorio/estudanteconcedente', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.estudante.concedente', 'uses' => 'RelatorioController@estudanteConcedente']);
        Route::get('/relatorio/estudanteconcedente/csv', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.estudante.concedente.csv', 'uses' => 'RelatorioController@estudanteConcedenteCSV']);
        Route::get('/verify/{cpf}', ['middleware' => 'permission:Criar Estudante', 'as' => 'estudante.verify', 'uses' => 'EstudanteController@verify']);
    });

    //Rota Tce
    Route::group(['prefix' => '/tce'], function () {
        //Route::resource('tce', 'TceController');
        Route::any('/search', [
                    'middleware' => 'permission:Listar Tce',
                    'as' => 'tce.search',
                    'uses' => 'TceController@search',
                  ]);
        Route::any('/searchCancelados', [
                    'middleware' => 'permission:Listar Tce',
                    'as' => 'tce.search.cancelados',
                    'uses' => 'TceController@searchCancelados',
                  ]);
        Route::any('/{tce}/relatorio/{relatorio}', [
                    'middleware' => 'permission:Ver Tce',
                    'as' => 'tce.relatorio.show',
                    'uses' => 'TceController@showRelatorio',
                  ]);
        Route::any('/{tce}/recisao', [
                    'middleware' => 'permission:Ver Tce',
                    'as' => 'tce.recisao.show',
                    'uses' => 'TceController@showRecisao',
                  ]);
        Route::any('/{tce}/declaracao', [
                    'middleware' => 'permission:Ver Tce',
                    'as' => 'tce.declaracao.show',
                    'uses' => 'TceController@showDeclaracao',
                  ]);
        Route::get('/', [
                    'middleware' => 'permission:Listar Tce',
                    'as' => 'tce.index',
                    'uses' => 'TceController@index',
                  ]);
        Route::get('/cancelados', [
                    'middleware' => 'permission:Listar Tce',
                    'as' => 'tce.index.cancelados',
                    'uses' => 'TceController@indexCancelados',
                  ]);
        //rotas validação tce
        Route::get('/validar', [
                    'middleware' => 'permission:Editar Tce',
                    'as' => 'tce.validar',
                    'uses' => 'TceController@validar',
                  ]);
        Route::any('/validar/tce', [
                    'middleware' => 'permission:Editar Tce',
                    'as' => 'tce.validar.tce',
                    'uses' => 'TceController@searchValidar',
                  ]);
        Route::put('/validar/{id}', [
                    'middleware' => 'permission:Editar Tce',
                    'as' => 'tce.validando',
                    'uses' => 'TceController@validando',
                  ]);

        Route::get('/create', [
                    'middleware' => 'permission:Criar Tce',
                    'as' => 'tce.create',
                    'uses' => 'TceController@create',
                  ]);
        Route::post('/store', [
                    'middleware' => 'permission:Criar Tce',
                    'as' => 'tce.store',
                    'uses' => 'TceController@store',
                  ]);
        Route::get('/show/{id}', [
                    'middleware' => 'permission:Ver Tce',
                    'as' => 'tce.show',
                    'uses' => 'TceController@show',
                  ]);
        Route::get('/edit/{id}', [
                    'middleware' => 'permission:Editar Tce',
                    'as' => 'tce.edit',
                    'uses' => 'TceController@edit',
                  ]);
        Route::put('/update/{id}', [
                    'middleware' => 'permission:Editar Tce',
                    'as' => 'tce.update',
                    'uses' => 'TceController@update',
                  ]);
        //teste cancelar tce
        Route::put('/cancel/{id}', [
                    'middleware' => 'permission:Cancelar Tce',
                    'as' => 'tce.cancelar',
                    'uses' => 'TceController@cancel',
                  ]);
        Route::get('/cancelar/{id}', [
                    'middleware' => 'permission:Cancelar Tce',
                    'as' => 'tce.cancelar.form',
                    'uses' => 'TceController@cancelar',
                  ]);
        //
        //Rota Aditivo
        Route::group(['prefix' => '{idTce}/aditivo'], function () {
            //Route::resource('tce.aditivo', 'AditivoController');
            Route::get('/', [
                      'middleware' => 'permission:Listar Aditivo',
                      'as' => 'tce.aditivo.index',
                      'uses' => 'AditivoController@index',
                    ]);
            Route::get('/create', [
                      'middleware' => 'permission:Criar Aditivo',
                      'as' => 'tce.aditivo.create',
                      'uses' => 'AditivoController@create',
                    ]);
            Route::post('/store', [
                      'middleware' => 'permission:Criar Aditivo',
                      'as' => 'tce.aditivo.store',
                      'uses' => 'AditivoController@store',
                    ]);
            Route::get('/show/{id}', [
                      'middleware' => 'permission:Ver Aditivo',
                      'as' => 'tce.aditivo.show',
                      'uses' => 'AditivoController@show',
                    ]);
            Route::get('/edit/{id}', [
                      'middleware' => 'permission:Editar Aditivo',
                      'as' => 'tce.aditivo.edit',
                      'uses' => 'AditivoController@edit',
                    ]);
            Route::put('/update/{id}', [
                      'middleware' => 'permission:Editar Aditivo',
                      'as' => 'tce.aditivo.update',
                      'uses' => 'AditivoController@update',
                    ]);
            //PDF Aditivo
            Route::any('/relatorio/{id}', [
                      'middleware' => 'permission:Ver Aditivo',
                      'as' => 'aditivo.relatorio.show',
                      'uses' => 'AditivoController@showRelatorio',
                    ]);
        });
        //Concedente Tce
        Route::get('/relatorios/concedentetce', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.concedente.tce', 'uses' => 'RelatorioController@concedenteTce']);
        Route::get('/relatorios/concedentetce/pdf', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.concedente.tce.pdf', 'uses' => 'RelatorioController@concedenteTcePDF']);
        //Concedente Secretaria Tce
        Route::get('/relatorios/concedentesectce/pdf', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.concedente.sec.tce.pdf', 'uses' => 'RelatorioController@concedenteSecTcePDF']);
        //Tce
        Route::get('/relatorios/tce', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.tce', 'uses' => 'RelatorioController@tce']);
        Route::get('/relatorios/tce/pdf', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.tce.pdf', 'uses' => 'RelatorioController@tcePDF']);
        //Tces cancelados por concedente
        Route::get('/relatorios/concedentetcecancelado', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.concedente.cancelado.tce', 'uses' => 'RelatorioController@concedenteTceCancelado']);
        Route::get('/relatorios/concedentetcecancelado/pdf', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.concedente.cancelado.tce.pdf', 'uses' => 'RelatorioController@concedenteTceCanceladoPDF']);
        //Concedente Tce DesValidados
        Route::get('/relatorios/tcedesvalidado', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.tce.desvalidado', 'uses' => 'RelatorioController@tceDesValidado']);
        Route::get('/relatorios/tcedesvalidado/pdf', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.tce.desvalidado.pdf', 'uses' => 'RelatorioController@tceDesValidadoPDF']);
        //Concedente Tce Validados
        Route::get('/relatorios/concedentetcevalidado', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.concedente.tce.validado', 'uses' => 'RelatorioController@concedenteTceValidado']);
        Route::get('/relatorios/concedentetcevalidado/pdf', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.concedente.tce.validado.pdf', 'uses' => 'RelatorioController@concedenteTceValidadoPDF']);
    });

    //Rota Usuario
    Route::group(['prefix' => '/usuario'], function () {
        //Route::resource('usuario','UsuarioController');
        Route::any('usuario/search', [
                    'middleware' => 'permission:Listar Usuario',
                    'as' => 'usuario.search',
                    'uses' => 'UsuarioController@search',
                  ]);
        Route::get('/', [
                    'middleware' => 'permission:Listar Usuario',
                    'as' => 'usuario.index',
                    'uses' => 'UsuarioController@index',
                  ]);
        Route::get('/create', [
                    'middleware' => 'permission:Criar Usuario',
                    'as' => 'usuario.create',
                    'uses' => 'UsuarioController@create',
                  ]);
        Route::post('/store', [
                    'middleware' => 'permission:Criar Usuario',
                    'as' => 'usuario.store',
                    'uses' => 'UsuarioController@store',
                  ]);
        Route::get('/show/{id}', [
                    'middleware' => 'permission:Ver Usuario',
                    'as' => 'usuario.show',
                    'uses' => 'UsuarioController@show',
                  ]);
        Route::get('/edit/{id}', [
                    'middleware' => 'permission:Editar Usuario',
                    'as' => 'usuario.edit',
                    'uses' => 'UsuarioController@edit',
                  ]);
        Route::put('/update/{id}', [
                    'middleware' => 'permission:Editar Usuario',
                    'as' => 'usuario.update',
                    'uses' => 'UsuarioController@update',
                  ]);
    });

    //Rota Unidades
    Route::group(['prefix' => '/unidade'], function () {
        //Route::resource('unidade','UnidadeController');
        Route::any('unidade/search', [
                    'middleware' => 'permission:Listar Unidade',
                    'as' => 'unidade.search',
                    'uses' => 'UnidadeController@search',
                  ]);
        Route::get('/', [
                    'middleware' => 'permission:Listar Unidade',
                    'as' => 'unidade.index',
                    'uses' => 'UnidadeController@index',
                  ]);
        Route::get('/create', [
                    'middleware' => 'permission:Criar Unidade',
                    'as' => 'unidade.create',
                    'uses' => 'UnidadeController@create',
                  ]);
        Route::post('/store', [
                    'middleware' => 'permission:Criar Unidade',
                    'as' => 'unidade.store',
                    'uses' => 'UnidadeController@store',
                  ]);
        Route::get('/show/{id}', [
                    'middleware' => 'permission:Ver Unidade',
                    'as' => 'unidade.show',
                    'uses' => 'UnidadeController@show',
                  ]);
        Route::get('/edit/{id}', [
                    'middleware' => 'permission:Editar Unidade',
                    'as' => 'unidade.edit',
                    'uses' => 'UnidadeController@edit',
                  ]);
        Route::put('/update/{id}', [
                    'middleware' => 'permission:Editar Unidade',
                    'as' => 'unidade.update',
                    'uses' => 'UnidadeController@update',
                  ]);
    });

    //Rota Relatorio
    Route::group(['prefix' => '/relatorio'], function () {
        //Concedente
        //Route::get('/concedente', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.concedente', 'uses' => 'RelatorioController@concedente']);
        //Route::get('/concedente/pdf', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.concedente.pdf', 'uses' => 'RelatorioController@concedentePDF']);
        //Concedente Tce
        //Route::get('/concedentetce', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.concedente.tce', 'uses' => 'RelatorioController@concedenteTce']);
        //Route::get('/concedentetce/pdf', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.concedente.tce.pdf', 'uses' => 'RelatorioController@concedenteTcePDF']);
        //Concedente Tce Cancelado
        Route::get('/concedentetcecancelado', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.concedente.cancelado.tce', 'uses' => 'RelatorioController@concedenteTceCancelado']);
        Route::get('/concedentetcecancelado/pdf', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.concedente.cancelado.tce.pdf', 'uses' => 'RelatorioController@concedenteTceCanceladoPDF']);
        //Instituicao
                  //Route::get('/instituicao', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.instituicao', 'uses' => 'RelatorioController@instituicao']);
                  //Route::get('/instituicao/pdf', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.instituicao.pdf', 'uses' => 'RelatorioController@instituicaoPDF']);
                  //Curso
                  //Route::get('/curso', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.curso', 'uses' => 'RelatorioController@curso']);
                  //Route::get('/curso/pdf', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.curso.pdf', 'uses' => 'RelatorioController@cursoPDF']);
                  //Curso Instituicao
                  //Route::get('/cursoinstituicao', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.curso.instituicao', 'uses' => 'RelatorioController@cursoInstituicao']);
                  //Route::get('/cursoinstituicao/pdf', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.curso.instituicao.pdf', 'uses' => 'RelatorioController@cursoInstituicaoPDF']);
                  //Estudante
                  //Route::get('/estudante', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.estudante', 'uses' => 'RelatorioController@estudante']);
                  //Route::get('/estudante/pdf', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.estudante.pdf', 'uses' => 'RelatorioController@estudantePDF']);
                  //Estudante Instituicao
                  //Route::get('/estudanteinstituicao', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.estudante.instituicao', 'uses' => 'RelatorioController@estudanteInstituicao']);
                  //Route::get('/estudanteinstituicao/pdf', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.estudante.instituicao.pdf', 'uses' => 'RelatorioController@estudanteInstituicaoPDF']);
                  //Tce
                  //Route::get('/tce', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.tce', 'uses' => 'RelatorioController@tce']);
                  //Route::get('/tce/pdf', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.tce.pdf', 'uses' => 'RelatorioController@tcePDF']);
    });

    ////----- GETS -----/////
    //Get Nivel/Tuno/Curso - Instituicao Curso
    Route::get('/get-nivel-curso-instituicao/{idCurso}', 'CursoController@getNivelCursoInstituicao');
    Route::get('/get-cursos-instituicao/{idInstituicao}', 'InstituicaoController@getCursosInstituicao');
    Route::get('/get-turno-curso-instituicao/{idCurso}', 'CursoController@getTurnoCursoInstituicao');
    //Get polo da instituição
    Route::get('/get-polo-instituicao/{idInstituicao}', 'InstituicaoController@getPoloInstituicao');
    //Get instituicoes por polo
    Route::get('/get-instituicoes-polo/{idPolo}', 'PoloController@getInstituicoesPolo');
    //Rota Cidades
    Route::get('/get-cidades/{idEstado}', 'CidadeController@getCidades');
    //Rota Atividades
    Route::get('/get-atividades/{idCurso}', 'CursoController@getAtividades');
    //Rota Secretaria Concedente
    Route::get('/get-secretarias/{idConcedente}', 'SecConcedenteController@getSecretarias');

    Route::get('/get-supervisor/{idConcedente}/{secretaria?}', 'SupervisorController@getConcedenteSecretaria');

    Route::get('/get-tces', 'API\TceController@getTces')->name('api.get.tces');

    Route::post('folha/editar/item', 'API\FinanceiroController@editItem');
    // Route::get('/get-items/{idConcedente}/secretaria/financeiro/{id}/folha/{folha}/ativos','API\FinanceiroController@getAtivos');
                //
                // Route::get('/get-items/{idConcedente}/secretaria/financeiro/{id}/folha/{folha}/cancelados','API\FinanceiroController@getCancelados');
});

              Route::get('supervisor/{id}/show', [
                'middleware' => 'permission:Editar Concedente',
                'as' => 'supervisor.show',
                'uses' => 'SupervisorController@show',
              ]);

              Route::get('supervisor/{id}/edit', [
                'middleware' => 'permission:Editar Concedente',
                'as' => 'supervisor.edit',
                'uses' => 'SupervisorController@edit',
              ]);

              Route::put('supervisor/{id}/update', [
                'middleware' => 'permission:Editar Concedente',
                'as' => 'supervisor.update',
                'uses' => 'SupervisorController@update',
              ]);

              //######################################################
              //########## FIM - GRUPO - ROTA concedenteISTRAÇÃO ##########
              //######################################################

              /*
              Route::group(['prefix' => '/estudante','middleware' => ['role:estudante']], function () {

              Route::get('/', [
              'middleware' => 'permission:Listar Curso',
              'as' => 'testes',
              'uses' => 'estudante\CursoController@index',
            ]);

          });
          */

          //Route::get('painel', function () {
          //    return 'Hello World';
          //});

          //TESTE ROTA USUARIO CONCEDENTE

          //######################################################
          //##### INICIO - GRUPO - ROTA PAINEL CONCEDENTE ########
          //######################################################

          //Auth::routes();
          Route::group(['prefix' => '/painel'], function () {
              Route::prefix('/concedente')->group(function () {
                  Route::post('/registro/folhas/criar', 'Concedente\FolhaPagamentoController@store')->name('painel.concedente.folhas.store');
                  Route::post('/registro/folha/{hash}/item/update', 'Concedente\FolhaPagamentoController@updateItem')->name('painel.concedente.folha.item.update');
                  Route::get('/registro/folha/{hash}', 'Concedente\FolhaPagamentoController@show')->name('painel.concedente.folha.show');
                  Route::post('/registro/folha/{hash}/fechar', 'Concedente\FolhaPagamentoController@fechar')->name('painel.concedente.folha.fechar');
                  Route::get('/registro/folhas', 'Concedente\FolhaPagamentoController@index')->name('painel.concedente.folhas.index');

                  Route::get('/oportunidades', 'Concedente\OportunidadeController@index')->name('painel.concedente.oportunidade.index');
                  Route::get('/oportunidade/criar', 'Concedente\OportunidadeController@create')->name('painel.concedente.oportunidade.criar');
                  Route::post('/oportunidade/store', 'Concedente\OportunidadeController@store')->name('painel.concedente.oportunidade.store');
                  Route::get('/oportunidade/{hash}', 'Concedente\OportunidadeController@show')->name('painel.concedente.oportunidade.show');
                  Route::get('/oportunidade/{code}/editar', 'Concedente\OportunidadeController@edit')->name('painel.concedente.oportunidade.editar');
                  Route::post('/oportunidade/{code}/update', 'Concedente\OportunidadeController@update')->name('painel.concedente.oportunidade.update');
                  Route::delete('/oportunidade/{code}/delete', 'Concedente\OportunidadeController@destroy')->name('painel.concedente.oportunidade.delete');
                  //
                  //Requisitos
                  Route::post('/requisito/store', 'Concedente\RequisitoController@store')->name('painel.concedente.requisito.store');
                  //
                  //Atividades Oportunidades
                  Route::post('/atividade/store', 'Concedente\AtividadeController@store')->name('painel.concedente.atividade.store');
                  //
                  //
                  //Mediador
                  Route::get('/mediadores', 'Concedente\MediadorController@index')->name('painel.concedente.mediador.index');
                  Route::post('/mediador/store', 'Concedente\MediadorController@store')->name('painel.concedente.mediador.store');
                  Route::put('/mediador/update', 'Concedente\MediadorController@update')->name('painel.concedente.mediador.update');
                  Route::post('/mediador/delete', 'Concedente\MediadorController@destroy')->name('painel.concedente.mediador.destroy');

                  Route::get('/supervisores', 'Concedente\SupervisorController@index')->name('painel.concedente.supervisor.index');
                  Route::post('/supervisor/store', 'Concedente\SupervisorController@store')->name('painel.concedente.supervisor.store');
                  Route::put('/supervisor/update', 'Concedente\SupervisorController@update')->name('painel.concedente.supervisor.update');
                  Route::get('/supervisor/{id}/ver', 'Concedente\SupervisorController@show')->name('painel.concedente.supervisor.show');

                  Route::get('/', 'Concedente\HomeConcedenteController@index')->name('painel.concedente.dashboard');
                  /*Route::get('/show/{id}', [
                  'middleware' => 'permission:Ver Tce',
                  'as' => 'tce.show',
                  'uses' => 'TceController@show',
            ]);*/
                  //estudante
                  Route::get('/estudante/{id}', 'Concedente\HomeConcedenteController@estudante')->name('painel.concedente.estudante');
                  //Instituicao de ensino
                  Route::get('/instituicao/{id}', 'Concedente\HomeConcedenteController@instituicao')->name('painel.concedente.instituicao');
                  //TCES
                  Route::get('/tce/{id}', 'Concedente\HomeConcedenteController@tce')->name('painel.concedente.tce');
                  Route::get('/tcesAtivos', 'Concedente\HomeConcedenteController@tcesAtivos')->name('painel.concedente.tces.ativos');
                  Route::get('/tcesPendentes', 'Concedente\HomeConcedenteController@tcesAtivos')->name('painel.concedente.tces.pendentes');
                  Route::get('/tces', 'Concedente\HomeConcedenteController@tces')->name('painel.concedente.tces');
                  Route::get('/tcesCancelados', 'Concedente\HomeConcedenteController@tcesCancelados')->name('painel.concedente.tces.cancelados');

                  Route::any('/{tce}/relatorio/{relatorio}', [
            'as' => 'painel.concedente.modelos.relatorio.show',
            'uses' => 'Concedente\HomeConcedenteController@showRelatorio',
            ]);
                  Route::any('/{tce}/recisao', [
              'as' => 'painel.concedente.tce.recisao.show',
              'uses' => 'Concedente\HomeConcedenteController@showRecisao',
            ]);
                  //rota relatorio de atividade semestral
                  Route::any('/{tce}/relatorioAtividadeSemestral', [
              'as' => 'painel.concedente.relatorioAtividades',
              'uses' => 'Concedente\HomeConcedenteController@showRelatorioAtividades',
            ]);
                  //rota relatorio de atividade semestral
                  //teste rota folha de pagamento do TCE
                  Route::any('/{tce}/folha', [
              'as' => 'painel.concedente.folha',
              'uses' => 'Concedente\HomeConcedenteController@showFolha',
            ]);
                  //teste rota aditivo do TCE
                  Route::any('/{tce}/aditivo/{aditivo}', [
              'as' => 'painel.concedente.tce.aditivo',
              'uses' => 'Concedente\HomeConcedenteController@aditivo',
            ]);
                  //teste cancelar tce nova funcionalidade 14/06/2018
                  Route::put('/cancel/{id}', [
              'as' => 'painel.concedente.tce.cancelar',
              'uses' => 'Concedente\HomeConcedenteController@cancel',
            ]);

                  Route::any('/{tce}/declaracaoCancelado', [
              'as' => 'painel.concedente.tce.cancelado.declaracao.show',
              'uses' => 'Concedente\HomeConcedenteController@declaracaoCancelado',
            ]);

                  Route::get('/financeiro', [
              'as' => 'painel.concedente.financeiro',
              'uses' => 'Concedente\HomeConcedenteController@financeiro',
            ]);
                  Route::get('/financeiro/{id}/', [
              'as' => 'painel.concedente.financeiro.show',
              'uses' => 'Concedente\HomeConcedenteController@financeiroVerFolhaPDF',
            ]);

                  Route::get('/relatorio', [
              'as' => 'painel.concedente.relatorio',
              'uses' => 'Concedente\RelatorioController@index',
            ]);

                  Route::get('/relatorio/pendentes/{tipo}', [
              'as' => 'painel.concedente.relatorio.pendentes',
              'uses' => 'Concedente\RelatorioController@pendentes',
            ]);

                  Route::get('/relatorio/ativos/{tipo}', [
              'as' => 'painel.concedente.relatorio.ativos',
              'uses' => 'Concedente\RelatorioController@ativos',
            ]);

                  Route::get('/relatorio/cancelados/{tipo}', [
              'as' => 'painel.concedente.relatorio.cancelados',
              'uses' => 'Concedente\RelatorioController@cancelados',
            ]);

                  Route::get('/relatorio/supervisores/{tipo}', [
              'as' => 'painel.concedente.relatorio.supervisores',
              'uses' => 'Concedente\RelatorioController@supervisores',
            ]);

                  Route::get('/relatorio/mediadores/{tipo}', [
              'as' => 'painel.concedente.relatorio.mediadores',
              'uses' => 'Concedente\RelatorioController@mediadores',
            ]);

                  Route::get('/relatorio/estudantes/{tipo}', [
              'as' => 'painel.concedente.relatorio.estudantes',
              'uses' => 'Concedente\RelatorioController@estudantes',
            ]);

                  Route::get('/relatorio1PDF', [
              'as' => 'painel.concedente.relatorio1PDF',
              'uses' => 'Concedente\HomeConcedenteController@relatorioTce1PDF',
            ]);
                  Route::get('/relatorio2PDF', [
              'as' => 'painel.concedente.relatorio2PDF',
              'uses' => 'Concedente\HomeConcedenteController@relatorioTce2PDF',
            ]);
                  Route::post('/alterar-senha', 'Concedente\UserController@changePassword')->name('painel.concedente.alterarSenha.submit');

                  Route::get('/login', 'Auth\ConcedenteLoginController@showLoginForm')->name('painel.concedente.login');
                  Route::post('/login', 'Auth\ConcedenteLoginController@login')->name('painel.concedente.login.submit');
                  Route::post('/logout', 'Auth\ConcedenteLoginController@logout')->name('painel.concedente.logout');
              });
          });

        //######################################################
        //###### FIM - GRUPO - ROTA PAINEL CONCEDENTE ##########
        //######################################################

        //######################################################
        //##### INICIO - GRUPO - ROTA PAINEL MEDIADOR ########
        //######################################################

        Route::group(['prefix' => 'painel'], function () {
            Route::prefix('/mediador')->group(function () {
                Route::get('', 'Mediador\FaltaController@show')->name('painel.mediador.index');
                Route::get('/registro/faltas', 'Mediador\FaltaController@show')->name('painel.mediador.index');
                Route::post('/logout', 'Auth\ConcedenteLoginController@logout')->name('painel.mediador.logout');
            });
        });

        //######################################################
        //###### FIM - GRUPO - ROTA PAINEL MEDIADOR ##########
        //######################################################

        //######################################################
        //##### INICIO - GRUPO - ROTA PAINEL ESTUDANTE ########
        //######################################################
        Route::group(['prefix' => 'painel/estudante', 'middleware' => 'auth:web_estudante'], function () {
            Route::get('', 'Estudante\HomeController@index')->name('painel.estudante.home');
            Route::get('home', 'Estudante\HomeController@index')->name('painel.estudante.home');
            Route::get('vaga/{code}', 'Estudante\OportunidadeController@show')->name('painel.estudante.vaga');
            Route::get('tce/show/{tce}', 'Estudante\TceController@show')->name('painel.estudante.tce');
            Route::get('perfil', 'Estudante\PerfilController@index')->name('painel.estudante.perfil');
            Route::put('perfil/atualizar', 'Estudante\PerfilController@update')->name('painel.estudante.perfil.submit');
            Route::get('relatorio/baixar', 'Estudante\HomeController@downloadRelatorioSemestralPDF')->name('painel.estudante.relatorio.download');

            Route::get('sair', function () {
                Auth::logout();

                return redirect()->route('painel.estudante.login');
            })->name('painel.estudante.sair');
        });

        //Cadastro Externo
        ////----- GETS -----/////
        //Get Instituição - por estado
        Route::get('/get-instituicao-estado/{idEstado}', 'InstituicaoController@getInstituicaoEstado');
        //Get Nivel/Tuno/Curso - Instituicao Curso
        Route::get('/get-nivel-curso-instituicao/{idCurso}', 'CursoController@getNivelCursoInstituicao');
        Route::get('/get-cursos-instituicao/{idInstituicao}', 'InstituicaoController@getCursosInstituicao');
        Route::get('/get-turno-curso-instituicao/{idCurso}', 'CursoController@getTurnoCursoInstituicao');
        //Rota Cidades
        Route::get('/get-cidades/{idEstado}', 'CidadeController@getCidades');
        //######################################################
        //##### INICIO - GRUPO - ROTA API ESTUDANTE ########
        //######################################################
        Route::get('verify/cpf/{cpf}', 'API\EstudanteController@verifyCPF');

        Route::get('verify/cpf/{cpf}/dtnascimento/{dtnascimento}', 'API\EstudanteController@verifydtNascimento');

        //######################################################
        //###### FIM - GRUPO - ROTA API ESTUDANTE ##########
        //######################################################

        Route::get('painel/estudante/primeiro-acesso', 'Auth\EstudanteLoginController@showLoginForm')->name('painel.estudante.primeiro-acesso');
        Route::get('painel/estudante/login', 'Auth\EstudanteLoginController@showLoginForm')->name('painel.estudante.login');
        Route::get('painel/estudante/primeiro-acesso', 'Auth\EstudanteLoginController@showPrimeiroAcessoForm')->name('painel.estudante.primeiro-acesso');
        Route::post('painel/estudante/primeiro-acesso/cadastrar', 'Auth\EstudanteLoginController@registerPrimeiroAcesso')->name('painel.estudante.primeiro-acesso.submit');
        Route::get('painel/estudante/cadastrar-se', 'Auth\EstudanteLoginController@showRegisterForm')->name('painel.estudante.cadastrar');
        Route::post('painel/estudante/cadastrar', 'Auth\EstudanteLoginController@register')->name('painel.estudante.cadastrar.submit');

        Route::post('painel/estudante/login', 'Auth\EstudanteLoginController@login')->name('painel.estudante.login.submit');
        Route::post('painel/estudante/logout', 'Auth\EstudanteLoginController@logout')->name('painel.estudante.logout');
