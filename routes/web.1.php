<?php
#######################################################
######### INICIO - GRUPO - ROTA ADMINISTRAÇÃO #########
#######################################################
Route::auth();

//Rota de Permissão
Route::get('permissao', function () {
    return view('layouts.permissao');
})->name('permissao');

Route::group(['prefix' => '/sysEstagio','middleware' => ['role:admin']], function () {

    //Rota Painel
    Route::get('/', 'HomeController@index')->name('home');

    //Rotas Cursos e Atividades de Curso
    Route::group(['prefix' => '/curso'], function () {
        Route::any('/search',[
            'middleware' => 'permission:Listar Curso',
            'as'    => 'curso.search',
            'uses'  => 'CursoController@search',
        ]);
        Route::get('/', [
            'middleware' => 'permission:Listar Curso',
            'as' => 'curso.index',
            'uses' => 'CursoController@index',
        ]);
        Route::get('/create', [
            'middleware' => 'permission:Criar Curso',
            'as' => 'curso.create',
            'uses' => 'CursoController@create',
        ]);
        Route::post('/store', [
            'middleware' => 'permission:Criar Curso',
            'as' => 'curso.store',
            'uses' => 'CursoController@store',
        ]);
        Route::get('/show/{id}', [
            'middleware' => 'permission:Ver Curso',
            'as' => 'curso.show',
            'uses' => 'CursoController@show',
        ]);
        Route::get('/edit/{id}', [
            'middleware' => 'permission:Editar Curso',
            'as' => 'curso.edit',
            'uses' => 'CursoController@edit',
        ]);
        Route::put('/update/{id}', [
            'middleware' => 'permission:Editar Curso',
            'as' => 'curso.update',
            'uses' => 'CursoController@update',
        ]);
        //Rota Atividade
        Route::group(['prefix' => '{idCurso}/atividade'], function () {
            Route::get('/', [
                'middleware' => 'permission:Listar Curso',
                'as' => 'curso.atividade.index',
                'uses' => 'AtividadeController@index',
            ]);
            Route::get('/create', [
                'middleware' => 'permission:Criar Curso',
                'as' => 'curso.atividade.create',
                'uses' => 'AtividadeController@create',
            ]);
            Route::post('/store', [
                'middleware' => 'permission:Criar Curso',
                'as' => 'curso.atividade.store',
                'uses' => 'AtividadeController@store',
            ]);
            Route::get('/show/{id}', [
                'middleware' => 'permission:Ver Curso',
                'as' => 'curso.atividade.show',
                'uses' => 'AtividadeController@show',
            ]);
            Route::get('/edit/{}', [
                'middleware' => 'permission:Editar Curso',
                'as' => 'curso.atividade.edit',
                'uses' => 'AtividadeController@edit',
            ]);
            Route::put('/update/{id}', [
                'middleware' => 'permission:Editar Curso',
                'as' => 'curso.atividade.update',
                'uses' => 'AtividadeController@update',
            ]);

        });
        //Route::resource('curso.atividade', 'AtividadeController');
    });

    //Rotas Instituições e Cursos de Instituições
    Route::group(['prefix' => '/instituicao'], function () {
        //Route::resource('instituicao', 'InstituicaoController');
        Route::any('instituicao/search',[
            'middleware' => 'permission:Listar Instituicao',
            'as' => 'instituicao.search',
            'uses' => 'InstituicaoController@search'
        ]);
        Route::get('/', [
            'middleware' => 'permission:Listar Instituicao',
            'as' => 'instituicao.index',
            'uses' => 'InstituicaoController@index',
        ]);
        Route::get('/create', [
            'middleware' => 'permission:Criar Instituicao',
            'as' => 'instituicao.create',
            'uses' => 'InstituicaoController@create',
        ]);
        Route::post('/store', [
            'middleware' => 'permission:Criar Instituicao',
            'as' => 'instituicao.store',
            'uses' => 'InstituicaoController@store',
        ]);
        Route::get('/show/{id}', [
            'middleware' => 'permission:Ver Instituicao',
            'as' => 'instituicao.show',
            'uses' => 'InstituicaoController@show',
        ]);
        Route::get('/editar/{id}', [
            'middleware' => 'permission:Editar Instituicao',
            'as' => 'instituicao.edit',
            'uses' => 'InstituicaoController@edit',
        ]);
        Route::put('/update/{id}', [
            'middleware' => 'permission:Editar Instituicao',
            'as' => 'instituicao.update',
            'uses' => 'InstituicaoController@update',
        ]);

        //teste relatorio instituicao
        //Instituicao
        Route::get('/relatorio/instituicao', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.instituicao', 'uses' => 'RelatorioController@instituicao']);
        Route::get('/relatorio/instituicao/pdf', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.instituicao.pdf', 'uses' => 'RelatorioController@instituicaoPDF']);
        //Curso
        Route::get('/relatorio/curso', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.curso', 'uses' => 'RelatorioController@curso']);
        Route::get('/relatorio/curso/pdf', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.curso.pdf', 'uses' => 'RelatorioController@cursoPDF']);
        //Curso Instituicao
        Route::get('/relatorio/curso/instituicao', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.curso.instituicao', 'uses' => 'RelatorioController@cursoInstituicao']);
        Route::get('/relatorio/curso/instituicao/pdf', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.curso.instituicao.pdf', 'uses' => 'RelatorioController@cursoInstituicaoPDF']);
        //
            //Rota Gerar Convenio Insituicao
            Route::get('/{instituicao}/convenio',[
                'middleware' => 'permission:Editar Instituicao',
                'as' => 'instituicao.convenio',
                'uses' => 'ConvenioIeController@store'
            ]);
            Route::get('/{instituicao}/convenio/{convenio}',[
                'middleware' => 'permission:Editar Instituicao',
                'as' => 'instituicao.convenio.show',
                'uses' => 'ConvenioIeController@show'
            ]);

        //Rota /instituicao/{idIntituicao}/Curso
        Route::group(['prefix' => '{idIntituicao}/curso'], function () {
            Route::get('/', [
                'middleware' => 'permission:Listar Instituicao',
                'as' => 'instituicao.curso.index',
                'uses' => 'CursoInstituicaoController@index',
            ]);
            Route::get('/create', [
                'middleware' => 'permission:Criar Instituicao',
                'as' => 'instituicao.curso.create',
                'uses' => 'CursoInstituicaoController@create',
            ]);
            Route::post('/store', [
                'middleware' => 'permission:Criar Instituicao',
                'as' => 'instituicao.curso.store',
                'uses' => 'CursoInstituicaoController@store',
            ]);
            Route::get('/show/{id}', [
                'middleware' => 'permission:Ver Instituicao',
                'as' => 'instituicao.curso.show',
                'uses' => 'CursoInstituicaoController@show',
            ]);
            Route::get('/edit/{id}', [
                'middleware' => 'permission:Editar Instituicao',
                'as' => 'instituicao.curso.edit',
                'uses' => 'CursoInstituicaoController@edit',
            ]);
            Route::put('/update/{id}', [
                'middleware' => 'permission:Editar Instituicao',
                'as' => 'instituicao.curso.update',
                'uses' => 'CursoInstituicaoController@update',
            ]);
            //Route::resource('instituicao.curso', 'CursoInstituicaoController');

        });//fim /Instituicao/{id}/cursp

    });// fim /Instituicao

    //Rotas Concedentes
    Route::group(['prefix' => '/concedente'], function () {
        //Route::resource('concedente','ConcedenteController');
        Route::any('concedente/search',[
            'middleware' => 'permission:Listar Concedente',
            'as' => 'concedente.search',
            'uses' => 'ConcedenteController@search'
        ]);
        Route::get('/', [
            'middleware' => 'permission:Listar Concedente',
            'as' => 'concedente.index',
            'uses' => 'ConcedenteController@index',
        ]);
        Route::get('/create', [
            'middleware' => 'permission:Criar Concedente',
            'as' => 'concedente.create',
            'uses' => 'ConcedenteController@create',
        ]);
        Route::post('/store', [
            'middleware' => 'permission:Criar Concedente',
            'as' => 'concedente.store',
            'uses' => 'ConcedenteController@store',
        ]);
        Route::get('/show/{id}', [
            'middleware' => 'permission:Ver Concedente',
            'as' => 'concedente.show',
            'uses' => 'ConcedenteController@show',
        ]);
        Route::get('/edit/{id}', [
            'middleware' => 'permission:Editar Concedente',
            'as' => 'concedente.edit',
            'uses' => 'ConcedenteController@edit',
        ]);
        Route::put('/update/{id}', [
            'middleware' => 'permission:Editar Concedente',
            'as' => 'concedente.update',
            'uses' => 'ConcedenteController@update',
        ]);
         //Relatorios
         Route::get('/relatorio/concedente', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.concedente', 'uses' => 'RelatorioController@concedente']);
         Route::get('/relatorio/concedente/pdf', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.concedente.pdf', 'uses' => 'RelatorioController@concedentePDF']);
         
    });

    //Rotas Estudantes
    Route::group(['prefix' => '/estudante'], function () {
        //Route::resource('estudante','EstudanteController');
        Route::any('estudante/search',[
            'middleware' => 'permission:Listar Estudante',
            'as' => 'estudante.search',
            'uses' => 'EstudanteController@search'
        ]);
        Route::get('/', [
            'middleware' => 'permission:Listar Estudante',
            'as' => 'estudante.index',
            'uses' => 'EstudanteController@index',
        ]);
        Route::get('/create', [
            'middleware' => 'permission:Criar Estudante',
            'as' => 'estudante.create',
            'uses' => 'EstudanteController@create',
        ]);
        Route::post('/store', [
            'middleware' => 'permission:Criar Estudante',
            'as' => 'estudante.store',
            'uses' => 'EstudanteController@store',
        ]);
        Route::get('/show/{id}', [
            'middleware' => 'permission:Ver Estudante',
            'as' => 'estudante.show',
            'uses' => 'EstudanteController@show',
        ]);
        Route::get('/edit/{id}', [
            'middleware' => 'permission:Editar Estudante',
            'as' => 'estudante.edit',
            'uses' => 'EstudanteController@edit',
        ]);
        Route::put('/update/{id}', [
            'middleware' => 'permission:Editar Estudante',
            'as' => 'estudante.update',
            'uses' => 'EstudanteController@update',
        ]);
        

        //Rota Tces dentro de Estudante
        Route::group(['prefix' => '{idEstudante}/tce'], function () {
            //Route::resource('estudante.tce', 'TceController');
            Route::get('/', [
                'middleware' => 'permission:Listar Tce',
                'as' => 'estudante.tce.index',
                'uses' => 'TceController@index',
            ]);
            Route::get('/create', [
                'middleware' => 'permission:Criar Tce',
                'as' => 'estudante.tce.create',
                'uses' => 'TceController@create',
            ]);
            Route::post('/store', [
                'middleware' => 'permission:Criar Tce',
                'as' => 'estudante.tce.store',
                'uses' => 'TceController@store',
            ]);
            Route::get('/show/{id}', [
                'middleware' => 'permission:Ver Tce',
                'as' => 'estudante.tce.show',
                'uses' => 'TceController@show',
            ]);
            Route::get('/edit/{id}', [
                'middleware' => 'permission:Editar Tce',
                'as' => 'estudante.tce.edit',
                'uses' => 'TceController@edit',
            ]);
            Route::put('/update/{id}', [
                'middleware' => 'permission:Editar Tce',
                'as' => 'estudante.tce.update',
                'uses' => 'TceController@update',
            ]);
            
        });
        //Estudante
        Route::get('/relatorio/estudante', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.estudante', 'uses' => 'RelatorioController@estudante']);
        Route::get('/relatorio/estudante/pdf', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.estudante.pdf', 'uses' => 'RelatorioController@estudantePDF']);
        //Estudante Instituicao
        Route::get('/relatorio/estudanteinstituicao', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.estudante.instituicao', 'uses' => 'RelatorioController@estudanteInstituicao']);
        Route::get('/relatorio/estudanteinstituicao/pdf', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.estudante.instituicao.pdf', 'uses' => 'RelatorioController@estudanteInstituicaoPDF']);

    });

    //Rota Tce
    Route::group(['prefix' => '/tce'], function () {
        //Route::resource('tce', 'TceController');
        Route::any('/search',[
            'middleware' => 'permission:Listar Tce',
            'as' => 'tce.search',
            'uses' => 'TceController@search'
        ]);
        Route::any('/{tce}/relatorio/{relatorio}', [
            'middleware' => 'permission:Ver Tce',
            'as' => 'tce.relatorio.show',
            'uses' => 'TceController@showRelatorio'
        ]);
        Route::any('/{tce}/recisao', [
            'middleware' => 'permission:Ver Tce',
            'as' => 'tce.recisao.show',
            'uses' => 'TceController@showRecisao'
        ]);

        Route::get('/', [
            'middleware' => 'permission:Listar Tce',
            'as' => 'tce.index',
            'uses' => 'TceController@index',
        ]);
        Route::get('/cancelados', [
            'middleware' => 'permission:Listar Tce',
            'as' => 'tce.index.cancelados',
            'uses' => 'TceController@indexCancelados',
        ]);
        Route::get('/create', [
            'middleware' => 'permission:Criar Tce',
            'as' => 'tce.create',
            'uses' => 'TceController@create',
        ]);
        Route::post('/store', [
            'middleware' => 'permission:Criar Tce',
            'as' => 'tce.store',
            'uses' => 'TceController@store',
        ]);
        Route::get('/show/{id}', [
            'middleware' => 'permission:Ver Tce',
            'as' => 'tce.show',
            'uses' => 'TceController@show',
        ]);
        Route::get('/edit/{id}', [
            'middleware' => 'permission:Editar Tce',
            'as' => 'tce.edit',
            'uses' => 'TceController@edit',
        ]);
        Route::put('/update/{id}', [
            'middleware' => 'permission:Editar Tce',
            'as' => 'tce.update',
            'uses' => 'TceController@update',
        ]);
        //teste cancelar tce
        Route::put('/cancel/{id}', [
            'middleware' => 'permission:Cancelar Tce',
            'as' => 'tce.cancelar',
            'uses' => 'TceController@cancel',
        ]);
        Route::get('/cancelar/{id}', [
            'middleware' => 'permission:Cancelar Tce',
            'as' => 'tce.cancelar.form',
            'uses' => 'TceController@cancelar',
        ]);
        //
            //Rota Aditivo
            Route::group(['prefix' => '{idTce}/aditivo'], function () {
                //Route::resource('tce.aditivo', 'AditivoController');
                Route::get('/', [
                    'middleware' => 'permission:Listar Aditivo',
                    'as' => 'tce.aditivo.index',
                    'uses' => 'AditivoController@index',
                ]);
                Route::get('/create', [
                    'middleware' => 'permission:Criar Aditivo',
                    'as' => 'tce.aditivo.create',
                    'uses' => 'AditivoController@create',
                ]);
                Route::post('/store', [
                    'middleware' => 'permission:Criar Aditivo',
                    'as' => 'tce.aditivo.store',
                    'uses' => 'AditivoController@store',
                ]);
                Route::get('/show/{id}', [
                    'middleware' => 'permission:Ver Aditivo',
                    'as' => 'tce.aditivo.show',
                    'uses' => 'AditivoController@show',
                ]);
                Route::get('/edit/{id}', [
                    'middleware' => 'permission:Editar Aditivo',
                    'as' => 'tce.aditivo.edit',
                    'uses' => 'AditivoController@edit',
                ]);
                Route::put('/update/{id}', [
                    'middleware' => 'permission:Editar Aditivo',
                    'as' => 'tce.aditivo.update',
                    'uses' => 'AditivoController@update',
                ]);
                //PDF Aditivo
                Route::any('/relatorio/{id}', [
                    'middleware' => 'permission:Ver Aditivo',
                    'as' => 'aditivo.relatorio.show',
                    'uses' => 'AditivoController@showRelatorio'
                ]);

            });
        //Concedente Tce
        Route::get('/relatorios/concedentetce', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.concedente.tce', 'uses' => 'RelatorioController@concedenteTce']);
        Route::get('/relatorios/concedentetce/pdf', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.concedente.tce.pdf', 'uses' => 'RelatorioController@concedenteTcePDF']);
        //Tce
        Route::get('/relatorios/tce', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.tce', 'uses' => 'RelatorioController@tce']);
        Route::get('/relatorios/tce/pdf', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.tce.pdf', 'uses' => 'RelatorioController@tcePDF']);
    });

    //Rota Usuario
    Route::group(['prefix' => '/usuario'], function () {
        //Route::resource('usuario','UsuarioController');
        Route::any('usuario/search',[
            'middleware' => 'permission:Listar Usuario',
            'as' => 'usuario.search',
            'uses' => 'UsuarioController@search'
        ]);
        Route::get('/', [
            'middleware' => 'permission:Listar Usuario',
            'as' => 'usuario.index',
            'uses' => 'UsuarioController@index',
        ]);
        Route::get('/create', [
            'middleware' => 'permission:Criar Usuario',
            'as' => 'usuario.create',
            'uses' => 'UsuarioController@create',
        ]);
        Route::post('/store', [
            'middleware' => 'permission:Criar Usuario',
            'as' => 'usuario.store',
            'uses' => 'UsuarioController@store',
        ]);
        Route::get('/show/{id}', [
            'middleware' => 'permission:Ver Usuario',
            'as' => 'usuario.show',
            'uses' => 'UsuarioController@show',
        ]);
        Route::get('/edit/{id}', [
            'middleware' => 'permission:Editar Usuario',
            'as' => 'usuario.edit',
            'uses' => 'UsuarioController@edit',
        ]);
        Route::put('/update/{id}', [
            'middleware' => 'permission:Editar Usuario',
            'as' => 'usuario.update',
            'uses' => 'UsuarioController@update',
        ]);

    });

    //Rota Unidades
    Route::group(['prefix' => '/unidade'], function () {
        //Route::resource('unidade','UnidadeController');
        Route::any('unidade/search',[
            'middleware' => 'permission:Listar Unidade',
            'as' => 'unidade.search',
            'uses' => 'UnidadeController@search'
        ]);
        Route::get('/', [
            'middleware' => 'permission:Listar Unidade',
            'as' => 'unidade.index',
            'uses' => 'UnidadeController@index',
        ]);
        Route::get('/create', [
            'middleware' => 'permission:Criar Unidade',
            'as' => 'unidade.create',
            'uses' => 'UnidadeController@create',
        ]);
        Route::post('/store', [
            'middleware' => 'permission:Criar Unidade',
            'as' => 'unidade.store',
            'uses' => 'UnidadeController@store',
        ]);
        Route::get('/show/{id}', [
            'middleware' => 'permission:Ver Unidade',
            'as' => 'unidade.show',
            'uses' => 'UnidadeController@show',
        ]);
        Route::get('/edit/{id}', [
            'middleware' => 'permission:Editar Unidade',
            'as' => 'unidade.edit',
            'uses' => 'UnidadeController@edit',
        ]);
        Route::put('/update/{id}', [
            'middleware' => 'permission:Editar Unidade',
            'as' => 'unidade.update',
            'uses' => 'UnidadeController@update',
        ]);

    });

    //Rota Relatorio
    Route::group(['prefix' => '/relatorio'], function () {
        //Concedente
        //Route::get('/concedente', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.concedente', 'uses' => 'RelatorioController@concedente']);
        //Route::get('/concedente/pdf', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.concedente.pdf', 'uses' => 'RelatorioController@concedentePDF']);
        //Concedente Tce
        //Route::get('/concedentetce', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.concedente.tce', 'uses' => 'RelatorioController@concedenteTce']);
        //Route::get('/concedentetce/pdf', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.concedente.tce.pdf', 'uses' => 'RelatorioController@concedenteTcePDF']);
        //Concedente Tce Cancelado
        Route::get('/concedentetcecancelado', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.concedente.cancelado.tce', 'uses' => 'RelatorioController@concedenteTceCancelado']);
        Route::get('/concedentetcecancelado/pdf', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.concedente.cancelado.tce.pdf', 'uses' => 'RelatorioController@concedenteTceCanceladoPDF']);
        //Instituicao
            //Route::get('/instituicao', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.instituicao', 'uses' => 'RelatorioController@instituicao']);
            //Route::get('/instituicao/pdf', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.instituicao.pdf', 'uses' => 'RelatorioController@instituicaoPDF']);
        //Curso
            //Route::get('/curso', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.curso', 'uses' => 'RelatorioController@curso']);
            //Route::get('/curso/pdf', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.curso.pdf', 'uses' => 'RelatorioController@cursoPDF']);
        //Curso Instituicao
            //Route::get('/cursoinstituicao', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.curso.instituicao', 'uses' => 'RelatorioController@cursoInstituicao']);
            //Route::get('/cursoinstituicao/pdf', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.curso.instituicao.pdf', 'uses' => 'RelatorioController@cursoInstituicaoPDF']);
        //Estudante
        //Route::get('/estudante', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.estudante', 'uses' => 'RelatorioController@estudante']);
        //Route::get('/estudante/pdf', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.estudante.pdf', 'uses' => 'RelatorioController@estudantePDF']);
        //Estudante Instituicao
        //Route::get('/estudanteinstituicao', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.estudante.instituicao', 'uses' => 'RelatorioController@estudanteInstituicao']);
        //Route::get('/estudanteinstituicao/pdf', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.estudante.instituicao.pdf', 'uses' => 'RelatorioController@estudanteInstituicaoPDF']);
        //Tce
        //Route::get('/tce', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.tce', 'uses' => 'RelatorioController@tce']);
        //Route::get('/tce/pdf', ['middleware' => 'permission:Relatorios', 'as' => 'relatorio.tce.pdf', 'uses' => 'RelatorioController@tcePDF']);
    });

    ////----- GETS -----/////
    //Get Nivel/Tuno/Curso - Instituicao Curso
    Route::get('/get-nivel-curso-instituicao/{idCurso}', 'CursoController@getNivelCursoInstituicao');
    Route::get('/get-cursos-instituicao/{idInstituicao}', 'InstituicaoController@getCursosInstituicao');
    Route::get('/get-turno-curso-instituicao/{idCurso}', 'CursoController@getTurnoCursoInstituicao');
    //Rota Cidades
    Route::get('/get-cidades/{idEstado}', 'CidadeController@getCidades');
    //Rota Atividades
    Route::get('/get-atividades/{idCurso}', 'CursoController@getAtividades');

});

#######################################################
########### FIM - GRUPO - ROTA ADMINISTRAÇÃO ##########
#######################################################

/*
Route::group(['prefix' => '/estudante','middleware' => ['role:estudante']], function () {

 Route::get('/', [
            'middleware' => 'permission:Listar Curso',
            'as' => 'testes',
            'uses' => 'estudante\CursoController@index',
        ]);

});
*/